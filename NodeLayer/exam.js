const puppeteer = require('puppeteer');

(async () => {
  const browser = await puppeteer.launch(
    {
      slowMo: 2500
    });
  const page = await browser.newPage();
  await page.goto('google.com');
  
  // Get the "viewport" of the page, as reported by the page.
  const dimensions = await page.evaluate(() => {
    return {
      width: document.documentElement.clientWidth,
      height: 1360,
      deviceScaleFactor: window.devicePixelRatio
    };
  });
  // await page.pdf({path: 'hn.pdf', format: 'A4'});
 await page.screenshot({path: 'full.png', fullPage: true});
  console.log('Dimensions:', dimensions);

  await browser.close();
})();
