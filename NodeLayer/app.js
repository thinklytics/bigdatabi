var express=require('express');
var app=express();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var $=require('underscore');
var bodyParser=require('body-parser');
var session = require('express-session');
global.dataMetaWise = {};
var Client=require('./modules/ClientUtility/Client');
var Tooltip=require('./modules/ClientUtility/Tooltip');
var DataStore=require('./modules/DataProcessor/DataStore');
var CalculationOnData=require('./modules/DataProcessor/CalculationOnData');
var GroupOnData=require('./modules/DataProcessor/GroupOnData');
var ClientRegister=require('./modules/ClientUtility/ClientRegister');
var DataConfigUtility=require('./modules/ClientUtility/DataConfigureUtility');
var DatatableDataConfigUtility=require('./modules/ClientUtility/SpecialReports/DatatableDataConfigureUtility');
var NumberWidgetDataConfigureUtility=require('./modules/ClientUtility/SpecialReports/NumberWidgetDataConfigureUtility');
var MapDataConfigUtility=require('./modules/ClientUtility/SpecialReports/MapDataConfigureUtility');
var WordCloudDataConfigUtility=require('./modules/ClientUtility/SpecialReports/WordCloudDataConfigUtility');
var MaleFemaleChartConfigUtility=require('./modules/ClientUtility/SpecialReports/MaleFemaleChartConfigUtility');
var Report=require('./modules/ClientUtility/Report');
var SocketManager=require('./modules/ClientUtility/SocketManager');
var MapReport=require('./modules/ClientUtility/SpecialReports/MapReport');
var DatatableReport=require('./modules/ClientUtility/SpecialReports/DatatableReport');
var NumberWidgetReport=require('./modules/ClientUtility/SpecialReports/NumberWidgetReport');
var MaleFemaleReport=require('./modules/ClientUtility/SpecialReports/MaleFemaleChartReport');
var Calculate=require('./modules/DataProcessor/CalculateOnData');
var PivotUtility=require('./modules/DataProcessor/PivotUtility');
var TextImageDataConfigureUtility=require('./modules/ClientUtility/SpecialReports/TextImageDataConfigureUtility');
var TextImagetReport=require('./modules/ClientUtility/SpecialReports/TextImageReport');
const args = process.argv;
var fs = require('fs');
var util = require('util');
var Request = require("request");
/*
 * Metadata and company wise data and column
 */


const puppeteer = require('puppeteer');
//var capture = require('capture-screenshot'); 
//const numeral=require('numeral');
//var puppeteer = require('puppeteer');  

/** bodyParser.urlencoded(options)
 * Parses the text as URL encoded data (which is how browsers tend to send form data from regular forms set to POST)
 * and exposes the resulting object (containing the keys and values) on req.body
 */

app.use(bodyParser.urlencoded({
    extended: true,
    parameterLimit: 10000,
    limit: '100mb'
}));

/**bodyParser.json(options)
 * Parses the text as JSON and exposes the resulting object on req.body.
 */
app.use(bodyParser.json());
app.use(session({secret: 'ssshhhhh'}));
// set headers
app.use(function (req, res, next) {

    // Website you wish to allow to connect
    res.setHeader('Access-Control-Allow-Origin', '*');

    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    res.setHeader('Access-Control-Allow-Credentials', true);

    // Pass to next layer of middleware
    next();
});

//setInterval(()=>{
//  const a=process.memoryUsage();
//},5000);
var connectionCallback;
connectionCallback=function(socket){
    //   socket.join(socket.id);'
    //socket.emit("test", 'heello');
    socket.on('disconnect',function(){
        //console.log("disconnect");
    });
}
io.on('connection',connectionCallback);


//RedisHelper.clearRedis();
app.post("/initializeClient",function(req,res){
    console.log("initializeClient");
    var ClientInfo=req.body.ClientInfo;
    try{
        ClientInfo=JSON.parse(ClientInfo);
    }catch(e) {

    }
    var realtime=false;
    try{
        var ClientInfoRealtime=ClientInfo.id.split("-");
        if(ClientInfoRealtime && ClientInfoRealtime[1]!=undefined && ClientInfoRealtime[1]=="realtime"){
            realtime=true;
        }
    }catch (e){

    }
    ClientInfo.realtime=realtime;
    var client=ClientRegister.getMetadataClient(ClientInfo.sessionId,ClientInfo.metadataId);

    /*
     * Role level check
     */
    DataStore.checkRoleLevelCondition(ClientInfo).then(function (resolveObj) {
        var department=resolveObj.department;
        var userType=resolveObj.userType;
        var roleLevel=resolveObj.roleLevelCondition;
        if(!client){
            var metadataKey="";
            if(resolveObj.userType){
                metadataKey=ClientInfo.metadataId+":"+ClientInfo.companyId+":"+ClientInfo.id;
            }else if(realtime){
                var tempKey=ClientInfo.metadataId+":"+ClientInfo.companyId;
                if(resolveObj.userType && resolveObj.roleLevelCondition && resolveObj.roleLevelCondition.length){
                    tempKey=tempKey+":"+ClientInfo.id;
                }else if(department && resolveObj.roleLevelCondition && resolveObj.roleLevelCondition.length){
                    tempKey=tempKey+":"+department;
                }
                metadataKey=tempKey+"-realtime";
            }else if(department && resolveObj.roleLevelCondition && resolveObj.roleLevelCondition.length){
                metadataKey=ClientInfo.metadataId+":"+ClientInfo.companyId+":"+department;
            }else{
                metadataKey=ClientInfo.metadataId+":"+ClientInfo.companyId;
            }
            if(global.dataMetaWise[metadataKey]==undefined){
                DataStore.getClientData(ClientInfo).then(function(dataObj,departmentN){
                    try{
                        var tableColumn=dataObj['tableColumn'];
                        var data=Object.values(dataObj['dataObj']);

                        var dataObj=dataObj['dataObj'];
                        if(department!=undefined && department!=''){
                            global.dataMetaWise[metadataKey]={};
                            global.dataMetaWise[metadataKey]['data']=dataObj;
                            global.dataMetaWise[metadataKey]['tableColumn']=tableColumn;
                        }else{
                            global.dataMetaWise[metadataKey]={};
                            global.dataMetaWise[metadataKey]['data']=dataObj;
                            global.dataMetaWise[metadataKey]['tableColumn']=tableColumn;
                        }
                        client= new Client(ClientInfo.id,ClientInfo.metadataId,ClientInfo.sessionId,data,tableColumn,ClientInfo.companyId);
                        client.setRealtime(realtime);
                        client.setUsertype(userType);
                        if(department && resolveObj.roleLevelCondition && resolveObj.roleLevelCondition.length){
                            client.setDepartment(department);
                            client.setRoleLevel(true);
                        }
                        client.setDataObj(dataObj['dataObj']);
                        ClientRegister.register(client);
                        var result={
                            status:true
                        };
                        res.json(result);
                    }catch(e){
                        console.log(e);
                    }
                });
            }else{
                var data =Object.values(global.dataMetaWise[metadataKey]['data']);
                var tableColumn=global.dataMetaWise[metadataKey]['tableColumn'];
                client= new Client(ClientInfo.id,ClientInfo.metadataId,ClientInfo.sessionId,data,tableColumn,ClientInfo.companyId);
                client.setRealtime(realtime);
                if(department && resolveObj.roleLevelCondition && resolveObj.roleLevelCondition.length){
                    client.setDepartment(department);
                    client.setRoleLevel(true);
                }
                client.setDataObj(global.dataMetaWise[metadataKey]);
                client.setUsertype(userType);
                ClientRegister.register(client);
                var result={
                    status:true
                };
                res.json(result);
            }
        }else{
            DataStore.checkIncrementObj(ClientInfo).then(function (incrementObj) {
                if(incrementObj){
                    DataStore.redisCheckLength(ClientInfo).then(function (lenght) {
                        if (lenght > 100000) {
                            res.json(result);
                        } else {
                            DataStore.redisLimitData("", "", ClientInfo, client, true).then(function () {
                                client.removeFilters();
                                client.changeCossfilterData();
                                var result = {
                                    status: true
                                };
                                res.json(true);
                            });
                        }
                    });
                }else{
                    /*
                     * With out increment object
                     */
                    var result={
                        status:true
                    };
                    DataStore.redisCheckLength(ClientInfo).then(function (length) {
                        var clientDataLength=client.getData().length;
                        if(length>100000){
                            return res.json(true);
                        }
                        console.log(length,clientDataLength);
                        if(length>clientDataLength){
                            DataStore.redisLimitData(clientDataLength,length,ClientInfo,client,false).then(function () {
                                client.removeFilters();
                                client.changeCossfilterData();
                                res.json(true);
                            });
                        }else{
                            client.removeFilters();
                            client.changeCossfilterData();
                            res.json(true);
                        }
                    });
                    res.json(true);
                }
            });
        }
    });

});
/*app.post("/deleteClinet",function(req,res){
    console.log("deleteClient")
    res.json(true);
});*/
app.post("/metadataDelete",function(req,res){
    var metadataId=req.body.metadataId;
    var status=ClientRegister.deleteClient(metadataId);
    if(status){
        var result={
            status:true
        };
    }else{
        var result={
            status:false
        };
    }
    res.json(result);
});

app.post("/realtimeDraw",function(req,res){
    var sessionId=req.body.sessionId;
    var metadataId=req.body.metadataId;
    var sharedviewId=req.body.sharedviewId;
    var companyId=req.body.companyId;
    var id=req.body.id;
    var time=req.body.time;
    var ClientInfo={}
    ClientInfo.id=id;
    ClientInfo.metadataId=metadataId;
    ClientInfo.companyId=companyId;
    ClientInfo.sessionId=sessionId;
    var client= ClientRegister.getMetadataClient(sessionId,metadataId);
    client.setSharedviewId(sharedviewId);
    SocketManager.run(io,client,ClientInfo,DataStore,time);
    res.json(true);
});

app.post("/calculation",function(req,res){
    var sessionId=req.body.sessionId;
    var metadataId=req.body.metadataId;
    var column=req.body.column;
    var activeKey=req.body.activeKey;
    var client=ClientRegister.getMetadataClient(sessionId,metadataId);
    client.applyCalculation(column, activeKey, client.getFilterData(),client);
    res.json(true);
});

app.post("/drawPivot_Row",function(req,res){
    var sessionId=req.body.sessionId;
    var metadataId=req.body.metadataId;
    var rowAttribute=req.body.rowAttribute;
    var reportId=req.body.reportId;
    var dummyDimensionObject=req.body.dummyDimensionObj;
    var exclude_Row=req.body.exclude_Row;
    var reOrder_Row=req.body.reOrder_Row;
    var client=ClientRegister.getMetadataClient(sessionId,metadataId);
    var Pivot=new PivotUtility(client);
    var data=Pivot.getRowCreationData(rowAttribute,dummyDimensionObject,reportId,exclude_Row,reOrder_Row);
    client.registerPivot(Pivot,reportId);
    res.json(data);
});

app.post("/drawPivot_Column",function(req,res){
    var sessionId=req.body.sessionId;
    var metadataId=req.body.metadataId;
    var columnAttribute=req.body.columnAttribute;
    var reportId=req.body.reportId;
    var dummyDimensionObject=req.body.dummyDimensionObj;
    var exclude_Col=req.body.exclude_Col;
    var reOrder_Col=req.body.reOrder_Col;
    var client=ClientRegister.getMetadataClient(sessionId,metadataId);
    var data=(new PivotUtility(client)).getColumnCreationData(columnAttribute,dummyDimensionObject,reportId,exclude_Col,reOrder_Col);
    res.json(data);
});

app.post("/drawPivot_Data",function(req,res){
    var sessionId=req.body.sessionId;
    var metadataId=req.body.metadataId;
    var rowAttribute=req.body.rowAttribute;
    var reportId=req.body.reportId;
    var columnAttribute=req.body.columnAttribute;
    var dataAttribute=req.body.dataAttribute;
    var dummyDimensionObject=req.body.dummyDimensionObj;
    var exclude_Row=req.body.exclude_Row;
    var exclude_Col=req.body.exclude_Col;
    var client=ClientRegister.getMetadataClient(sessionId,metadataId);
    //client.addReports(_report);
    var data=(new PivotUtility(client)).getCreationData(dataAttribute,rowAttribute,columnAttribute,dummyDimensionObject,reportId,exclude_Row,exclude_Col);
    res.json(data);
});

app.post("/filterPivotRowColumn",function(req,res){
    var sessionId=req.body.sessionId;
    var metadataId=req.body.metadataId;
    var dimensionAttribute=req.body.dimensionAttribute;
    var filterItem=JSON.parse(req.body.filterItem);
    var client=ClientRegister.getMetadataClient(sessionId,metadataId);
    var dimensionList = client.getPivotDimensionList();
    var Pivot=PivotUtility(client);
    var _dimension=client.createDimension(dimensionAttribute);
    client.addPivotDimension(_dimension,dimensionAttribute);
    var pivotDataStatus=Pivot.filter(_dimension,filterItem,dimensionAttribute);
    var data={};
    client.getReports(function(report,reportId){
        data[reportId]=report.getDataGroups(client);
    });
    data['status']=pivotDataStatus;
    res.json(data);
});

app.post("/filterPivotData",function(req,res){
    var sessionId=req.body.sessionId;
    var metadataId=req.body.metadataId;
    var dimensionAttribute=req.body.dimensionAttribute;
    var filterItem=req.body.filterItem;
    var client= ClientRegister.getMetadataClient(sessionId,metadataId);
    var Pivot=new PivotUtility(client);
    var _dimension=client.createDimension(dimensionAttribute);
    $.each(dimensionAttribute,function(dimensionObj,dimensionKey){
        var _dimension=client.createDimension(dimensionObj);
        client.addPivotDimension(_dimension,dimensionObj);
        Pivot.filter(_dimension,filterItem);
    });
    var data={};
    client.getReports(function(report,reportId){
        data[reportId]=report.getDataGroups(client);
    });
    res.json(data);
});

app.post("/draw", function (req, res){
    var sessionId = req.body.sessionId;
    var chartType = req.body.chartType;
    var reportId = req.body.reportId;
    try{
        var metadataId = JSON.parse(req.body.axisConfig['queryObj'])['metadataId']; //for old saved object
    }catch (e){
        var metadataId = req.body.axisConfig['queryObj'];
    }
    var client = ClientRegister.getMetadataClient(sessionId,metadataId);
    // Tootip exist then check for calculation
    var activeKey=[];
    Object.keys(req.body.axisConfig.checkboxModelDimension).forEach(function (k,v) {
        activeKey.push(req.body.axisConfig.checkboxModelDimension[k].columnName);
    });

    if(req.body.axisConfig.dataFormat && req.body.axisConfig.dataFormat.tooltipSelector){
        CalculationOnData(client.getData()).tooltipCal(req.body.axisConfig.dataFormat.tooltipSelector,client,activeKey);
    }
    // End grp tooltip
    var dataConfig = new DataConfigUtility(req.body.axisConfig,client.getCrossfilter(),client,reportId);
    var report = new Report(reportId,dataConfig,chartType);
    client.addReport(report);
    data = report.getDataGroups(client);
    res.json(data);
});
app.post("/redraw", function (req, res){
    var sessionId = req.body.sessionId;
    var chartType = req.body.chartType;
    var reportId = req.body.reportId;
    var axisConfig=req.body.axisConfig;
    try{
        var metadataId = JSON.parse(req.body.axisConfig['queryObj'])['metadataId']; //for old saved object
    }catch (e){
        var metadataId = req.body.axisConfig['queryObj'];
    }
    var client = ClientRegister.getMetadataClient(sessionId,metadataId);
    // Tootip exist then check for calculation
    // End grp tooltip
    var report = client.getReportById(reportId);
    data = report.getDataGroups(client,axisConfig);
    res.json(data);
});



app.post("/drawMap_Center",function(req,res){
    try{
        var sessionId=req.body.sessionId;
        var chartType=req.body.chartType;
        var reportId=req.body.reportId;
        var metadataId=req.body.metadataId;
        var mapConfig=req.body.mapConfig;
        var client = ClientRegister.getMetadataClient(sessionId,metadataId);
        var dataConfig=new MapDataConfigUtility(mapConfig,client.getCrossfilter(),client.getData(),client);
        var report=new MapReport(reportId,dataConfig,chartType);
        client.addReport(report);
        var data={};
        data['center']=dataConfig.getCenterFromDegrees();
        res.json(data);
    }catch(e){

    }
});

app.post("/drawMap_LatLng",function(req,res){
    //try{
    var sessionId=req.body.sessionId;
    var chartType=req.body.chartType;
    var reportId=req.body.reportId;
    var metadataId=req.body.metadataId;
    var mapConfig=req.body.mapConfig;
    var excludeKey=req.body.excludeKey;
    var measureObject=JSON.parse(req.body.measureObject);
    mapConfig['measureObject']=measureObject;
    var client= ClientRegister.getMetadataClient(sessionId,metadataId);
    var dataConfig=new MapDataConfigUtility(mapConfig,client.getCrossfilter(),client.getData(),client);
    var report=new MapReport(reportId,dataConfig,chartType);
    client.addReport(report);
    var data=report.getDataGroups(client,excludeKey);
    //data['center']=dataConfig.getCenterFromDegrees();
    res.json(data);
    //  }catch(e){

    // }
});

app.post("/drawMap_Radius",function(req,res){
    // try{
    var sessionId=req.body.sessionId;
    var chartType=req.body.chartType;
    var reportId=req.body.reportId;
    var metadataId=req.body.metadataId;


    //var mapConfig=req.body.config['mapConfig'];
    var measureObj=req.body.measureObj;
    var dimensionObj=req.body.dimensionObj;
    var tooltipArr=req.body.tooltipArr;
    var tooltip=req.body.tooltip;
    //   var index=groupConfig.index;
    var client = ClientRegister.getMetadataClient(sessionId,metadataId);

    var report=client.getReportById(reportId);

    var dataConfig=report.getConfiguration();

    var groupData=dataConfig.getMapGroup(dimensionObj,measureObj,tooltipArr,tooltip);
    return res.json(groupData);
    // }catch(e){

    // }
});

app.post("/drawTextImage", function (req, res) {
    var chartType="_TextImage";
    var reportId=req.body.config.reportId;
    var sessionId=req.body.sessionId;
    var metadataId=req.body.metadataId;
    var config=req.body.config;
    var client= ClientRegister.getMetadataClient(sessionId,metadataId);
    var dataConfig=new TextImageDataConfigureUtility(config,client.getCrossfilter(),client);
    var TextImageReport=new TextImagetReport(reportId,dataConfig,chartType);
    client.addReport(TextImageReport);
    res.json(TextImageReport.processData());
});

app.post("/drawNumberWidget", function (req, res) {
    try{
        var sessionId=req.body.sessionId;
        var reportId=req.body.reportId;
        var metadataId=req.body.metadataId;
        var chartType="_numberWidget";
        var axisConfig=req.body.axisConfig;
        try{
            var axisConfig=JSON.parse(axisConfig);
        }catch(e){

        }
        var client= ClientRegister.getMetadataClient(sessionId,metadataId);
        var dataConfig=new NumberWidgetDataConfigureUtility(axisConfig,client.getCrossfilter(),client,reportId);
        var report=new NumberWidgetReport(reportId,dataConfig,chartType);
        client.addReport(report);
        var data=report.processData();
        res.json(data);
    }catch(e){

    }
});

app.post("/drawSpeedoMeter", function (req, res) {
    try{
        var sessionId=req.body.sessionId;
        var reportId=req.body.reportId;
        var metadataId=req.body.metadataId;
        var chartType="_numberWidget";
        var client= ClientRegister.getMetadataClient(sessionId,metadataId);
        var dataConfig=new NumberWidgetDataConfigureUtility(req.body.axisConfig,client.getCrossfilter(),client);
        var report=new NumberWidgetReport(reportId,dataConfig,chartType);
        client.addReport(report);
        var data=report.processData();
        res.json(data);
    }catch(e){
        console.log(e);
    }
});

app.post("/filter",function(req,res){
    try{
        var sessionId=req.body.sessionId;
        var activeReportId=req.body.filter.reportId;
        var metadataId=req.body.metadataId;
        var filter=req.body.filter;
        var customFormat=req.body.customFormat; //for date format check
        var client= ClientRegister.getMetadataClient(sessionId,metadataId);
        var report=(client.getReportsObject())[activeReportId];
        var flag=report.applyFilter(filter,customFormat);
        var data={};
        var i=0;
        client.getReports(function(report,reportId){
            if(activeReportId!=reportId)
                data[reportId]=report.getDataGroups(client);
            i++;
        });
        data['status']=flag;
        res.json(data);
    }catch(e){

    }

});

app.post("/filterAll",function(req,res){
    try{
        var sessionId = req.body.sessionId;
        var metadataId = req.body.metadataId;
        var client = ClientRegister.getMetadataClient(sessionId,metadataId);
        /*
         * For external filter
         */
        var data={};
        try{
            client.filterAllPivotDimension();
            client.filterAllDatatableDimension();

            client.getReports(function(report,reportId){
                try{
                    report.filterAll();
                }catch(e){
                    console.log(e);
                }
            });
            client.getReports(function(report,reportId){
                try{
                    data[reportId]=report.getDataGroups(client);
                }catch(e){
                    console.log(e);
                }
            });
        }catch(e){

        }
        res.json(data);
    }catch(e){
        console.log(e);
        res.json([]);
    }
});

app.post("/populateMaleFemaleOptions",function(req,res){
    try{
        var sessionId=req.body.sessionId;
        var metadataId=req.body.metadataId;
        var dimensionObject=req.body.dimensionObject;
        var client= ClientRegister.getMetadataClient(sessionId,metadataId);
        var list=client.getUniqueDataOfColumn(dimensionObject);
        res.json(list);
    }catch(e){
        res.json([]);
    }


});

app.post("/deleteReport",function(req,res){
    try{
        var sessionId=req.body.sessionId;
        var metadataId=req.body.metadataId;
        var reportId=req.body.reportId;
        var client= ClientRegister.getMetadataClient(sessionId,metadataId);
        client.deleteReport(reportId);
        res.json({success:true});
    }catch(e){
        res.json({success:false});
    }

});

app.post("/maleFemaleChartDraw",function(req,res){
    try{
        var sessionId=req.body.sessionId;
        var metadataId=req.body.metadataId;

        var dimensionObject=req.body.dimensionObject;
        var measureObject=req.body.measureObject;

        var reportId=req.body.reportId;
        var config={
            dimensionObject:dimensionObject,
            measureObject:measureObject,
        };
        var chartType=null;
        var client= ClientRegister.getMetadataClient(sessionId,metadataId);
        var dataConfig=new MaleFemaleChartConfigUtility(config,client.getCrossfilter(),client,reportId);
        var report=new MaleFemaleReport(reportId,dataConfig,chartType);
        client.addReport(report);
        var data=report.getDataGroups(client);
        res.json(data);
    }catch(e){
        res.json([]);
    }
});



app.post("/getUniqueDataFieldsOfColumn",function(req,res){
    try{
        var sessionId=req.body.sessionId;
        var metadataId=req.body.metadataId;
        var columnObject=req.body.columnObject;
        var client = ClientRegister.getMetadataClient(sessionId,metadataId);
        var list = client.getUniqueDataOfColumn(columnObject);
        res.json(list);
    }catch(e){
        res.json([]);
    }
});
app.post("/getUniqueDataFieldsOfColumnAfterCascade",function(req,res){
    //try{
    var sessionId=req.body.sessionId;
    var metadataId=req.body.metadataId;
    var columnObject=req.body.columnObject;
    var totalLength=req.body.totalLength;
    var filterIndex=req.body.filterIndex;
    var client= ClientRegister.getMetadataClient(sessionId,metadataId);
    var list=client.getUniqueDataOfColumnAfterCascade(columnObject,totalLength,filterIndex);
    res.json(list);
    // }catch(e){
    //  res.json([]);
    // }
});


app.post("/applyCascadeFilters",function(req,res){
    // try{
    var sessionId=req.body.sessionId;
    var metadataId=req.body.metadataId;
    var dimensionObject=req.body.dimensionObject;
    var item=req.body.item;
    var totalLength=totalLength;
    var client = ClientRegister.getMetadataClient(sessionId,metadataId);
    client.addCascadeFilter(item,dimensionObject);
    res.json([]);
    // }catch(e){
    // }
});
/*
  Cascade redefine
 */
app.post("/cascadeReinitialize",function(req,res){
    var sessionId=req.body.sessionId;
    var metadataId=req.body.metadataId;
    var client = ClientRegister.getMetadataClient(sessionId,metadataId);
    client.removeFilters();
    client.cascadeReinitialize();
    res.json(true);
});
app.post("/applyExternalFilters",function(req,res){
    try{
        var sessionId=req.body.sessionId;
        var metadataId=req.body.metadataId;
        var dimensionObject=req.body.dimensionObject;
        var item=req.body.item;
        var client= ClientRegister.getMetadataClient(sessionId,metadataId);
        dimensionObject['columnName']=dimensionObject.key;
        client.addExternalFilterDimension(dimensionObject);
        //Cross filter to filter data
        client.applyFilter(item,dimensionObject);
        //After Filter Calculation execute

        var data={};
        client.getReports(function(report,reportId){
            try{
                report.filterAll();
                data[reportId]=report.getDataGroups(client);
            }catch(e){

            }

        });
        res.json(data);
    }catch(e){
        res.json([]);
    }
});


app.post("/applyExternalAllFilters",function(req,res){
    try{
        var sessionId=req.body.sessionId;
        var metadataId=req.body.metadataId;
        var columnObjFilter=req.body.columnObjFilter;
        var client= ClientRegister.getMetadataClient(sessionId,metadataId);
        Object.keys(columnObjFilter).forEach(function(d){
            var dimensionObject=columnObjFilter[d]['columnObj'];
            dimensionObject['columnName']=columnObjFilter[d]['columnObj']['key'];
            var externalDimension=client.addExternalFilterDimension(dimensionObject);
            externalDimension.filterAll();
            //Cross filter to filter data
            client.applyFilter(columnObjFilter[d]['filterValue'],dimensionObject);
        });
        //After Filter Calculation execute
        var data={};
        client.getReports(function(report,reportId){
            try{
                report.filterAll();
                data[reportId]=report.getDataGroups(client);
            }catch(e){

            }

        });
        res.json(data);
    }catch(e){
        res.json([]);
    }
});
app.post("/processDataForDatatable",function(req,res){
    //try{
    var chartType="_dataTable";
    var reportId=req.body.config.id;
    var sessionId=req.body.sessionId;
    var metadataId=req.body.config.metadataId;
    var groupParams=req.body.groupParams;
    var groupKeys=req.body.groupKeys;
    var pageNo=req.body.pageNo;
    var client = ClientRegister.getMetadataClient(sessionId,metadataId);
    var topN = req.body.config.topN;
    var topnMeasure = req.body.config.topnMeasure;
    // if(groupParams && groupParams.length>0){
    //     Calculate=Calculate(client.getData());
    //     groupParams.forEach(function(e){
    //         Calculate.groupCalculation(groupKeys,e['formula'],e['columnName'],e['columnObject']);
    //     });
    // }
    var dataConfig = new DatatableDataConfigUtility(req.body.config,client.getCrossfilter(),client);
    var datatableReport = new DatatableReport(reportId,dataConfig,chartType);
    client.addReport(datatableReport);
    // res.json(datatableReport.processData(pageNo));
    res.json(datatableReport.processData(pageNo, topN, topnMeasure));
    // }catch(e){
    //    res.json([]);
    //}
});


app.post("/groupData",function(req,res){
    var sessionId=req.body.sessionId;
    var metadataId=req.body.metadataId;
    if(req.body.columnName.createdby!=undefined){
        var columnName=JSON.parse(req.body.columnName.createdby);
    }else{
        var columnName=req.body.columnName;
    }
    var client= ClientRegister.getMetadataClient(sessionId,metadataId);
    var list=client.getUniqueDataOfColumn(columnName);
    res.json(list);
});
app.post("/groupDataEditList",function(req,res){
    var sessionId=req.body.sessionId;
    var metadataId=req.body.metadataId;
    var columnName=req.body.columnName;
    var selectedGroupKey=req.body.selectedGroupKey;
    var client = ClientRegister.getMetadataClient(sessionId,metadataId);
    var list=client.getUniqueDataOfColumnEditList(columnName,selectedGroupKey);
    res.json(list);
});
app.post("/groupColumnCreate",function(req,res){
    var sessionId=req.body.sessionId;
    var metadataId=req.body.metadataId;
    var columnName=req.body.columnName;
    var categoryData=req.body.categoryData;
    var categoryName=req.body.categoryName;
    var client= ClientRegister.getMetadataClient(sessionId,metadataId);
    res.json(GroupOnData(client.getData()).createColumnInData(columnName,categoryData,categoryName));
});
app.post("/groupColumnUpdate",function(req,res){
    var sessionId=req.body.sessionId;
    var metadataId=req.body.metadataId;
    var columnName=req.body.columnName;
    var categoryData=req.body.categoryData;
    var categoryName=req.body.categoryName;
    var lastObj=req.body.categoryName;
    var client= ClientRegister.getMetadataClient(sessionId,metadataId);
    res.json(GroupOnData(client.getData()).updateColumnInData(columnName,categoryData,categoryName,lastObj));
});
app.post("/drawWordCloud",function(req,res){
    try{
        var sessionId=req.body.sessionId;
        var metadataId=req.body.metadataId;
        var reportId=req.body.reportId;

        var client= ClientRegister.getMetadataClient(sessionId,metadataId);
        var dataConfig=new WordCloudDataConfigUtility(req.body.axisConfig,client.getCrossfilter(),client.getData(),client,reportId);

        var report=new Report(reportId,dataConfig);

        client.addReport(report);
        var data=report.getDataGroups(client);

        res.json(data);

    }catch(e){
        res.json([]);
    }
});
app.post("/dataGroupCalculation",function(req,res){
    try{
        var sessionId=req.body.sessionId;
        var metadataId=req.body.metadataId;
        var oldColumn=req.body.oldColumn;
        var newColumn=req.body.newColumn;
        var categoryGroupObject=req.body.categoryGroupObject;
        var client= ClientRegister.getMetadataClient(sessionId,metadataId);
        client.addNewColumn(newColumn);
        CalculationOnData(client.getData(),oldColumn,newColumn,client)
            .uniqueArray()
            .columnCal()
            .groupObject(categoryGroupObject)
            .columnCategory();
        res.json(true);
    }
    catch(e){

    }
});
app.post("/dataRangeFilter",function(req,res){
    // try{
    var sessionId=req.body.sessionId;
    var metadataId=req.body.metadataId;
    var timelineObj=req.body.timelineObj;
    var client= ClientRegister.getMetadataClient(sessionId,metadataId).timeLineFilter(timelineObj);
    res.json(true);
});
app.post("/tooltip",function(req,res){

    var sessionId=req.body.sessionId;
    var metadataId=req.body.metadataId;
    var title=req.body.title;
    var tooltipFormat=req.body.tooltipFormat;
    var tooltipObj=req.body.tooltipObj;
    var client= ClientRegister.getMetadataClient(sessionId,metadataId);
    var tooltip=Tooltip(client.getData()).tooltipRender(title,tooltipFormat,tooltipObj);
    res.json(tooltip);

});
app.post("/takeSnapSort",function(req,res) {
    var publicView=req.body.publicView;
    var name=req.body.name;
    (async() => {
        try{
            //   const args=['--no-sandbox', '--disable-setuid-sandbox'];
            // const browser = await puppeteer.launch({args:args,slowMo: 7000});
            const browser = await puppeteer.launch({
            args: ['--no-sandbox','--disable-setuid-sandbox'],
            executablePath: '/usr/bin/google-chrome',
            slowMo: 7000
        });
    const page = await browser.newPage();
    //await page.setViewport({ width: 1366, height: 768});
    await page.setViewport({
        width: 1280,
        height: await page.evaluate(() => parseInt(document.documentElement.scrollHeight))
});
    /*const override = Object.assign(page.viewport(), {width: 1280});
    await page.setViewport(override);*/
    //, {waitUntil: 'networkidle2'}
    await page.goto(publicView);
    //await page.emulateMedia('screen');
    console.log("takeSnapSort");
    await page.screenshot({path: "/var/www/html/demo2_thinklytics_io/api/storage/app/snapImage/"+name+".png",fullPage: true});
    fs.chmodSync("/var/www/html/demo2_thinklytics_io/api/storage/app/snapImage/"+name+".png", 0o777);
    browser.close();
    return true;
}catch(e){
        console.log(e);
    }
})();
});
/*
Add Table column
 */
app.post("/addTableColumn",function(req,res){
    try{
        var sessionId=req.body.sessionId;
        var metadataId=req.body.metadataId;
        var tableColumn=req.body.tableColumn;
        var client= ClientRegister.getMetadataClient(sessionId,metadataId);

        client.addNewColumn(tableColumn);
        res.json(true);
    }catch(e){

    }
});

app.post("/getColumnData",function(req,res){
    var metadataId=req.body.metadataId;
    var sessionId=req.body.sessionId;
    var client=ClientRegister.getMetadataClient(sessionId,metadataId);
    var result={
        "tableColumn":client.getTableColumn(),
        "tableData":client.getData()
    };
    res.json(result);
});

var logFile = fs.createWriteStream('log.txt', { flags: 'a' });
// Or 'w' to truncate the file every time the process starts.
var logStdout = process.stdout;

console.log = function () {
    logFile.write(util.format.apply(null, arguments) + '\n');
    logStdout.write(util.format.apply(null, arguments) + '\n');
}
console.error = console.log;

http.listen(args[2],function(){
    console.log("listening "+args[2]);
});
