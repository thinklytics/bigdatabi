var funcD = require('./CalculateFunctionDefination');
var d3 = require('d3');
var $ = require('underscore');
var moment = require('moment');
var concatVar = "";
var FixedCalculation = require('./CalculationModule/FixedCalculationModule');
var AggregateModule = require('./CalculationModule/AggregateModule');
(function () {
    function _calculateOnData(data) {
        var _data = data;
        var funcDef = funcD();
        var _dataGroupsParams = {};
        var _abstractExpression = {};
        var _activeKey = [];
        var _total = 0;
        var _originalData = data;
        var i = 0;
        var minGroup = {};
        var maxGroup = {};

        function parseFormula(expression) {

            const regex = /(sum|SUM|count|COUNT|avg|AVG)\(\[([a-z|A-Z|0-9|_|' '|'('|')']*)\]\)/g;
            const str = expression;
            var params = {};
            var alteredFormula = expression;
            while ((m = regex.exec(str)) !== null) {
                if (m.index === regex.lastIndex) {
                    regex.lastIndex++;
                }
                params[m[2]] = m[1];
                alteredFormula = alteredFormula.replace(m[0], 'parseFloat(g[\'' + m[1] + '\'][\'' + m[2] + '\'])');
            }
            return {formula: alteredFormula, params: params};
        }

        function isAggregateField(formula) {
            var flag = !$.isEmpty((parseFormula(formula)).params);
            return flag;
        }

        function hasMultiLevelCalculation(formula, client) {
            var flag = false;
            //const regex = /\[([\w|' ']*)\]/g;
            const regex = /(sum|SUM|count|COUNT|avg|AVG)\(\[([a-z|A-Z|0-9|_|' '|'('|')']*)\]\)/g;
            var m;
            while ((m = regex.exec(formula)) !== null) {
                try {
                    if (m[1]) {
                        var tableColumns = client.getTableColumn();
                        var fieldObject = tableColumns[m[2]];
                        if (fieldObject && fieldObject.type == "custom" && isAggregateField(fieldObject.formula)) {
                            flag = true;
                        }
                    }
                } catch (e) {
                    return false;
                }
            }
            return flag;
        }

        function reduceMultilevelFormula(formula, client) {
            //    var hasMultiLevelCalculation=this.hasMultiLevelCalculation;
            const regex = /(?:sum|SUM|count|COUNT|avg|AVG)\(\[([\w|' ']*)\]\)/g;
            var reducedFormula = formula;
            var runFormula = formula;
            var m;
            while ((m = regex.exec(formula)) !== null) {
                try {
                    var fieldObject = client.getTableColumn()[m[1]];
                    if (fieldObject.type == "custom" && isAggregateField(fieldObject.formula)) {
                        reducedFormula = reducedFormula.replace(m[0], "(" + fieldObject.formula + ")", client);
                    }
                } catch (e) {
                    return reducedFormula;
                }
            }
            if (hasMultiLevelCalculation(reducedFormula, client)) {
                reducedFormula = reduceMultilevelFormula(reducedFormula, client);
            } else {
                const checkDivide = /\(?(?:sum|SUM|count|COUNT|avg|AVG)?\(?\[[\w|' ']*\]\)?\)\/\(?(?:sum|SUM|count|COUNT|avg|AVG)?\(?\[[\w|' ']*\]\)?\)/g;
                var o;
                var i = 1;
                var replacements = {};
                while ((o = checkDivide.exec(reducedFormula)) !== null) {

                    replacements[o[0]] = "(isNaN(" + o[0] + ")?0:" + o[0] + ")";

                    i++;
                }

                $.each(replacements, function (key, val) {
                    reducedFormula = reducedFormula.replace(key, val);
                });

                return reducedFormula;
            }
            return reducedFormula;
        }
        /*
         * For dcount
         */
        var hasMultiLevelCalculationForDcount = function (formula, client) {
            var flag = false;
            const regex = /\[([\w|' ']*)\]/g;
            //const regex = /(sum|SUM|count|COUNT|avg|AVG)\(\[([a-z|A-Z|0-9|_|' '|'('|')']*)\]\)/g;
            var m;
            while ((m = regex.exec(formula)) !== null) {
                try {
                    if (m[1]) {
                        var tableColumns = client.getTableColumn();
                        var fieldObject = tableColumns[m[1]];
                        if (fieldObject.type == "custom") {
                            flag = true;
                        }
                    }
                } catch (e) {
                    return false;
                }
            }
            return flag;
        };
        var isAggregateFieldForDcount=function (formula) {
            var flag = !$.isEmpty((parseFormulaForDcount(formula)).params);
            return flag;
        }
        var parseFormulaForDcount=function (formula) {
            const regex = /(sum|SUM|count|COUNT|avg|AVG)\(\[([a-z|A-Z|0-9|_|' '|'('|')']*)\]\)/g;
            const str = expression;
            var params = {};
            var alteredFormula = expression;
            while ((m = regex.exec(str)) !== null) {
                if (m.index === regex.lastIndex) {
                    regex.lastIndex++;
                }
                params[m[2]+"@@_"+m[1]] = m[1];
                alteredFormula = alteredFormula.replace(m[0], 'parseFloat(HelperModule.snap_to_zero_singleval(g[\'' + m[1] + '\'][\'' + m[2] + '\']))');
            }
            return {formula: alteredFormula, params: params};
        }
        var reduceMultilevelFormulaForDcount = function (formula, client) {
            //const regex = /(?:sum|SUM|count|COUNT|avg|AVG)\(\[([\w|' ']*)\]\)/g;
            const regex = /(?:count|avg|sum)[(]\[([\w|\' \'()]*)\][)]|\[([\w|\' \'()]*)\]/g;
            var reducedFormula = formula;
            var runFormula = formula;
            var m;
            while ((m = regex.exec(formula)) !== null) {
                try {
                    var fieldObject = client.getTableColumn()[m[2]];
                    if (fieldObject.type == "custom") {
                        reducedFormula = reducedFormula.replace(m[0], "(" + fieldObject.formula + ")", client);
                    }
                } catch (e) {
                    return reducedFormula;
                }
            }
            if (hasMultiLevelCalculationForDcount(reducedFormula, client)) {
                reducedFormula = reduceMultilevelFormulaForDcount(reducedFormula, client);
            }else {
                const checkDivide = /\(?(?:sum|SUM|count|COUNT|avg|AVG)?\(?\[[\w|' ']*\]\)?\)\/\(?(?:sum|SUM|count|COUNT|avg|AVG)?\(?\[[\w|' ']*\]\)?\)/g;
                var o;
                var i = 1;
                var replacements = {};
                while ((o = checkDivide.exec(reducedFormula)) !== null) {
                    replacements[o[0]] = "(isNaN(" + o[0] + ")?0:" + o[0] + ")";
                    i++;
                }
                $.each(replacements, function (key, val) {
                    reducedFormula = reducedFormula.replace(key, val);
                });
                return reducedFormula;
            }
            return reducedFormula;
        };
        /*
         * End dcount
         */
        return {
            processDataExpression: function (expr, column, activeKey, filterData, _client) {
                var column=Object.assign({},column);
                if (expr) {
                    var flagDcount=true;
                    if(hasMultiLevelCalculationForDcount(expr,_client) && column.dataKey=="Measure"){
                        var reduceFormula=reduceMultilevelFormulaForDcount(expr,_client)
                        if(reduceFormula.indexOf("dcount")!=-1 || reduceFormula.indexOf("AGGR")!=-1){
                            flagDcount=false;
                            expr=reduceFormula;
                        }
                    }
                    if(flagDcount)
                    expr = reduceMultilevelFormula(expr, _client);
                    if (expr)
                        expr = expr.replace("return", "");
                    _activeKey = activeKey;
                    if (filterData) {
                        _data = filterData;
                    } else {
                        _data = data;
                    }
                    if(expr.indexOf("dcount") != -1 || expr.indexOf("AGGR") != -1 && column.dataKey=="Measure"){
                        if(expr.indexOf("dcount") != -1){
                            column.formula=this.performDcountCalculation(expr, column,_activeKey);
                            expr=column.formula;
                        }

                        if(expr.indexOf("AGGR") != -1){
                            column.formula=this.performAggrCalculation(expr, column,_activeKey);
                            expr=column.formula;
                        }
                        this.calculatedAggrDcount(expr, column);
                    }else if (!this.isGroupExist(expr)) {
                        this.performNormalCalculation(expr, column);
                    }
                    else if (FixedCalculation.hasFixedCalculation(column.formula)) {
                        var dataGroup = FixedCalculation.processFormula(column.formula, _client.getData(), _client);
                        var form = FixedCalculation.genrateFormulaWithoutTooltip(column.formula);
                        var isKeyThere = {};
                        var k = _activeKey[0];
                        _data.forEach(function (d) {
                            if (!isKeyThere[d[k]]) {
                                d[column.columnName] = eval(form);
                                isKeyThere[d[k]] = true;
                            }
                            else {
                                d[column.columnName] = 0;
                            }
                        });
                    } else if (this.isAggrExist(expr)) {
                        this.processAggr(expr, column, activeKey, _client);
                    }
                    else {

                        this.groupCalculation(activeKey, expr, column.columnName, column)
                    }
                }
            },
            performNormalCalculation: function (expr, column) {
                var executingExpression = this.processDimensionStatements(expr);
                executingExpression = this.preProcessToken(executingExpression);
                try {
                    _data.forEach(function (d) {
                        var res = eval(executingExpression);
                        if(res==undefined){res=0;}
                        if (res != null)
                            d[column.columnName] = res;
                    });
                } catch (e) {
                    console.log(e);
                }
            },
            calculatedAggrDcount:function (expr, column) {
                try{
                    var columName = column.columnName;
                    /*
                     * If condition check
                     */
                    try{
                        //if([a-z|A-Z|0-9|=|.|,|-|*|\/|!|%|{|}| |_|(|)|[\]]*)else{[\w]:?[=|.|,|-|*|\/|!|%|{|}| |+|_|(|)|[\]]*}
                        const regex = /if([a-z|A-Z|0-9|=|.|,|-|*|\/|!|%|{|}| |_|(|)|[\]]*)else{[\w]:?[=|.|,|-|*|\/|!|%|{|}| |+|_|(|)|[\]]*}/gm;
                        var m;
                        while ((m = regex.exec(expr)) !== null) {
                            if (m.index === regex.lastIndex)
                                regex.lastIndex++;
                            expr=expr.replace(m[0],'eval("'+m[0]+'")');
                        }
                    }catch (e){
                        console.log(e);
                    }
                    expr = this.preProcessToken(expr);
                    _data.forEach(function (d) {
                        var val= eval(expr);
                        if(expr=="(funcDef.checkValue(d['Students not placed']))-(eval(\"if(funcDef.checkValue(d['job_status(sms_ims)'])==1){funcDef.checkValue(d['Students not placed'])}else{0}\"))"){
                            //console.log(val,d['Students not placed'],d['job_status(sms_ims)']);
                        }
                        d[columName] = val;
                    });
                }catch (e){
                    console.log(e);
                }
            },
            performDcountCalculation:function (expr, column,activeDimension) {
                //var executingExpression = this.processDimensionStatements(expr);
                //executingExpression = this.preProcessToken(executingExpression);
                var executingExpression=expr;
                try {
                    var grp={};
                    //(dcount)\(\['([([a-z|A-Z|0-9|_| |]*)\'\]\)
                    const regex = /(dcount)\(\[([([a-z|A-Z|0-9|_| |()]*)\]\)/g;
                    while ((m = regex.exec(executingExpression)) !== null) {
                        if(activeDimension==undefined){
                            activeDimension=[];
                            activeDimension.push(m[2]);
                        }
                        _data.forEach(function (d) {
                            var newExp=0;
                            if(grp[d[m[2]]+""+d[activeDimension[0]]]==undefined){
                                newExp=1;
                                grp[d[m[2]]+""+d[activeDimension[0]]]=true;
                            }else{
                                newExp=0;
                            }
                            var res = newExp;
                            if (res != null)
                                d[column.columnName] = res;
                        });
                        executingExpression=executingExpression.replace(m[0],"["+column.columnName+"]");
                    };
                    return executingExpression;
                } catch (e) {
                    console.log(e);
                }
            },
            performAggrCalculation:function (expr, column,activeDimension) {
                //var executingExpression = this.processDimensionStatements(expr);
                //executingExpression = this.preProcessToken(executingExpression);
                var executingExpression=expr;
                var columnName=column.columnName
                try {
                    var grp={};
                    //(sum|SUM|count|COUNT|avg|AVG|min|max|dcount|DCOUNT|AGGR)[(]|(funcDef.checkValue\(d\[')([a-z|A-Z|0-9|_| |(|).|]*)[']]\)*|,
                    //const regex = /(sum|SUM|count|COUNT|avg|AVG|min|max|Aggr|aggr|AGGR)[(]|(funcDef.checkValue\(d\[')([a-z|A-Z|0-9|_| |(|).|]*)[']]\)*|,/gm;
                    const regex = /(AGGR)\(\[([([a-z|A-Z|0-9|_| |()]*)\]\)*, *(count|sum|avg|min|max)\(\[([([a-z|A-Z|0-9|_| |()]*)\]\)*,(\[([([a-z|A-Z|0-9|_| |()]*)\])\)/g;
                    var tempObj={};

                    while ((m = regex.exec(executingExpression)) !== null) {
                        if(activeDimension=="" || activeDimension==undefined){
                            activeDimension=[];
                            activeDimension.push(m[2]);
                        }
                        if(column.formulaObj && column.formulaObj.length){
                            column.formulaObj.forEach(function (dObj) {
                                if(dObj.formula==m[0]){
                                    columnName=dObj.columnName;
                                }
                            });
                        }
                        var dimGrp={};
                        var totalGrpAggr=0;
                        //if(_data[0][columnName]==undefined) {
                            _data.forEach(function (d) {
                                var newExp = 0;
                                if (m[3] == 'count') {
                                    if (grp[d[m[2]] + "" + d[activeDimension[0]] + "" + d[m[6]]] == undefined) {
                                        newExp = 1;
                                        grp[d[m[2]] + "" + d[activeDimension[0]] + "" + d[m[6]]] = true;
                                    } else {
                                        newExp = 0;
                                    }
                                } else if (m[3] == 'sum' || m[3] == 'avg') {
                                    if (grp[d[m[2]] + "" +d[activeDimension] + "" + d[m[6]]] == undefined) {
                                        newExp = d[m[4]];
                                        grp[d[m[2]] + "" +d[activeDimension] + "" + d[m[6]]] = true;
                                        if (dimGrp[d[activeDimension]] == undefined) {
                                            dimGrp[d[activeDimension]] = 0;
                                        }
                                        dimGrp[d[activeDimension]]++;
                                        totalGrpAggr++;
                                    } else {
                                        newExp = 0;
                                    }
                                }
                                var res = newExp;
                                if (res != null)
                                    d[column.columnName] = res;
                            });
                            _data.forEach(function (d) {
                                if (m[3] == 'avg') {
                                    var avgData=0;
                                    if(d[columnName]){
                                        avgData=parseFloat(d[columnName])/parseFloat(dimGrp[d[activeDimension]]);
                                    }
                                    if (avgData != null){
                                        d[columnName] = avgData;
                                    }
                                }
                            });
                       // }
                        tempObj[columnName]=m[0];
                    }
                    $.each(tempObj,function (value,key) {
                        executingExpression=executingExpression.replace(value,"["+key+"]");
                    });
                    return executingExpression;
                } catch (e) {
                    console.log(e);
                }
            },
            processDimensionStatements: function (expr) {
                const regex = /DimensionSum\((?:(?:\[([\w+|\(|\|\s)]+)\])|(all|ALL|All)),\[([\w+|\(|\)|\s]+)\]\)/gm;
                var m;
                while ((m = regex.exec(expr)) !== null) {
                    // This is necessary to avoid infinite loops with zero-width matches
                    if (m.index === regex.lastIndex) {
                        regex.lastIndex++;
                    }
                    // The result can be accessed through the `m`-variable.
                    if (m[2] != "ALL" && m[2] != "all" && m[2] != "All") {
                        expr = expr.replace(m[0], "[" + m[3] + "]");
                    } else {
                        _total = 0;
                        _data.forEach(function (d, index) {
                            _total += parseFloat(d[m[3]]);
                        });
                        expr = expr.replace(m[0], "_total");
                    }
                }
                return expr;

            },

            preProcessToken: function (expr) {
                this.preProcessFunctions(expr);
                var expression = expr.replace(/isNull[(]/g,
                    "funcDef.isEmpty(")
                    .replace(/isNotNull[(]/g,
                        "funcDef.isNotEmpty(")
                    .replace(/floor[(]/g,
                        "floor(")
                    .replace(/ceil[(]/g,
                        "ceil(")
                    .replace(/decimalToInt[(]/g,
                        "parseInt(")
                    .replace(/concat[(]/g,
                        "concatVar.concat(")
                    .replace(/strlen[(]/g,
                        "funcDef.strlen(")
                    .replace(/dateDiff[(]/g,
                        "funcDef.dateDiff(")
                    .replace(/toLower[(]/g,
                        "funcDef.toLower(")
                    .replace(/toString[(]/g,
                        "funcDef.toString(")
                    .replace(/trim[(]/g,
                        "funcDef.trim(")
                    .replace(/replace[(]/g,
                        "funcDef.replace(")
                    .replace(/round[(]/g,
                        "funcDef.round(")
                    .replace(/parseDate[(]/g,
                        "funcDef.parseDate(")
                    .replace(/formatDate[(]/g,
                        "funcDef.formatDate(")
                    .replace(/toUpper[(]/g,
                        "funcDef.toUpper(")
                    .replace(/DateAdd[(]/g,
                        "funcDef.DateAdd(")
                    .replace(/return/g, "")
                    .replace(/\[/g, "funcDef.checkValue(d['")
                    .replace(/dateTimeDiff[(]/g, "funcDef.dateTimeDiff(")
                    .replace(/\]/g, "'])")
                    .replace(/currDate[(]/g, "funcDef.currentDate(");
                return expression.trim();
            },
            processAggr: function (expression, column, activeKey, client) {
                if (activeKey) {
                    if(Array.isArray(activeKey)){
                        activeKey=activeKey[1];
                    }
                    const regex = /(min|max)\(\[([a-z|A-Z|0-9|_|' '|'('|')']*)\]\)?/g;
                    var str = expression;
                    var flagGroup = false;
                    var tableColumn = client.getTableColumn();
                    var strGrp=[];
                    var i=0;
                    var minGroup={};
                    var maxGroup={};
                    while ((m = regex.exec(expression)) !== null) {
                        // This is necessary to avoid infinite loops with zero-width matches
                        if (!(tableColumn[m[2]].columType == "date" || tableColumn[m[2]].columType == "datetime")) {
                            if (!minGroup[m[2]]) {
                                minGroup[m[2]] = {};
                            }
                            if (!maxGroup[m[2]]) {
                                maxGroup[m[2]] = {};
                            }
                            _data.forEach(function (d, index) {
                                if(d[m[2]]){
                                    if (!maxGroup[m[2]][d[activeKey]]) {
                                        maxGroup[m[2]][d[activeKey]] = parseInt(d[m[2]]);
                                    }
                                    if (!minGroup[m[2]][d[activeKey]]) {
                                        minGroup[m[2]][d[activeKey]] = parseInt(d[m[2]]);
                                    }
                                    if (parseInt(d[m[2]]) > maxGroup[m[2]][d[activeKey]]) {
                                        maxGroup[m[2]][d[activeKey]] = d[m[2]]
                                    }
                                    else if (parseInt(d[m[2]]) < minGroup[m[2]][d[activeKey]]) {
                                        minGroup[m[2]][d[activeKey]] = d[m[2]]
                                    }
                                }
                            });
                            if (m[1] == 'min') {
                                str = str.replace(m[0], "minMaxGrp_"+i);
                                strGrp.push("minGroup['" + m[2] + "'][d[activeKey]]");

                            }
                            if (m[1] == 'max') {
                                str = str.replace(m[0], "minMaxGrp_"+i);
                                strGrp.push("maxGroup['" + m[2] + "'][d[activeKey]]");

                            }
                        } else if (tableColumn[m[2]].columType == "date" || tableColumn[m[2]].columType == "datetime") {
                            if (!minGroup[m[2]]) {
                                minGroup[m[2]] = {};
                            }
                            if (!maxGroup[m[2]]) {
                                maxGroup[m[2]] = {};
                            }
                            _data.forEach(function (d, index) {
                                if(d[m[2]]){
                                    if (!maxGroup[m[2]][d[activeKey]]) {
                                        maxGroup[m[2]][d[activeKey]] = moment(d[m[2]]);
                                    }
                                    if (!minGroup[m[2]][d[activeKey]]) {
                                        minGroup[m[2]][d[activeKey]] = moment(d[m[2]]);
                                    }

                                    if (moment(d[m[2]]) > maxGroup[m[2]][d[activeKey]]) {
                                        maxGroup[m[2]][d[activeKey]] = moment(d[m[2]])
                                    }
                                    else if (moment(d[m[2]]) < minGroup[m[2]][d[activeKey]]) {
                                        minGroup[m[2]][d[activeKey]] = moment(d[m[2]]);
                                    }
                                }
                            });
                            if (m[1] == 'min') {
                                str = str.replace(m[0], "minMaxGrp_"+i);
                                strGrp.push("minGroup['" + m[2] + "'][d[activeKey]]");
                            }
                            if (m[1] == 'max') {
                                str = str.replace(m[0], "minMaxGrp_"+i);
                                strGrp.push("maxGroup['" + m[2] + "'][d[activeKey]]");
                            }
                        }
                        if (m.index === regex.lastIndex) {
                            regex.lastIndex++;
                        }
                        flagGroup = true;
                        str=this.preProcessToken(str);
                        strGrp.forEach(function (d,index) {
                            str=str.replace("minMaxGrp_"+index,d);
                        });
                        var isValued = {};
                        var d = _data[0];
                        var checkVal=eval(str);
                        _data.forEach(function (d) {
                            if(!isValued[d[activeKey]] || checkVal instanceof moment){
                                var dataStr=eval(str);
                                if(dataStr instanceof moment){
                                    dataStr=dataStr.format('YYYY-MM-DD HH:MM:SS');
                                }
                                d[column.columnName] = dataStr;
                                isValued[d[activeKey]]=true;
                            }
                        });
                        return str;
                    }
                    return true;
                }

            },
            isAggrExist: function (expression) {
                const regex = /(min|max)\(\[([a-z|A-Z|0-9|_|' '|'('|')']*)\]\)?/g;
                const str = expression;
                var flagGroup = false;
                var m;

                while ((m = regex.exec(str)) !== null) {

                    // This is necessary to avoid infinite loops with zero-width matches
                    if (m.index === regex.lastIndex) {
                        regex.lastIndex++;
                    }
                    flagGroup = true;
                    return true;
                }
                return false;
            },
            isGroupExist: function (expression) {
                const regex = /(sum|SUM|count|COUNT|avg|AVG|min|max|dcount|DCOUNT)\(\[([a-z|A-Z|0-9|_|' '|'('|')']*)\]\)?/g;
                const str = expression;
                var flagGroup = false;
                var m;
                while ((m = regex.exec(str)) !== null) {
                    // This is necessary to avoid infinite loops with zero-width matches
                    if (m.index === regex.lastIndex) {
                        regex.lastIndex++;
                    }
                    flagGroup = true;
                    return true;
                }
                return false;
            },
            groupCalculation: function (_activeKey, expression, keyName, column) {
                var executingExpression = this.preProcessToken(expression);
                var groupedData = {};
                var dataGroupParams = this.getDataGroupParams();
                function getGroupedKey(d) {
                    var tempString = undefined;
                    if (_activeKey && _activeKey.length > 0) {
                        tempString = "";
                        _activeKey.forEach(function (key, index) {
                            tempString += "||" + d[key];
                        });
                    }

                    return tempString;
                }

                groupedData = d3.nest().key(function (d) {
                    return getGroupedKey(d);
                });

                function checkValIntegrity(val) {
                    if (!val || isNaN(val)) {
                        val = 0;
                    }
                    else {
                        val = parseFloat(val);
                    }
                    return val;
                }

                function processedReturnObj(v) {
                    var tempObj = {};
                    $.each(dataGroupParams, function (value, key) {
                        var tempKey=key.split("@@_")[0];
                        if (value == "sum") {
                            tempObj[key] = d3.sum(v, function (d) {
                                return checkValIntegrity(d[tempKey]);
                            });
                        }
                        if (value == "count") {
                            tempObj[key] = d3.sum(v, function (d) {
                                if (d[tempKey])
                                    return 1;
                                else
                                    return 0;
                            });
                        }
                        if (value == "avg") {
                            tempObj[key] = d3.mean(v, function (d) {
                                return checkValIntegrity(d[tempKey]);
                            });
                        }
                    });
                    return tempObj;
                }

                groupedData = groupedData.rollup(function (v) {
                    return processedReturnObj(v);
                });
                groupedData = groupedData.entries(_data);
                var testData = {};
                groupedData.forEach(function (d) {
                    testData[d.key] = d.values;
                });
                groupedData = testData;
                var _tempAvgKey = null;
                // executingExpression=this.processAggr(executingExpression,column,_activeKey);
                $.each(groupedData, function (d, key) {
                    try {
                        var res = eval(_abstractExpression);
                        d['newCalculation'] = res;
                    } catch (e) {

                    }
                });
                var tempSearchObj = {};
                if (column && column.dataKey == "Dimension") {
                    _data.forEach(function (d) {
                        var groupKey = getGroupedKey(d);
                        if (!tempSearchObj[groupKey]) {
                            tempSearchObj[groupKey] = true;
                            d[keyName] = groupedData[groupKey]['newCalculation'];
                        } else {
                            d[keyName] = groupedData[groupKey]['newCalculation'];
                        }
                    });
                } else {
                    _data.forEach(function (d) {
                        var groupKey = getGroupedKey(d);
                        if (!tempSearchObj[groupKey]) {
                            tempSearchObj[groupKey] = true;
                            d[keyName] = groupedData[groupKey]['newCalculation'];
                        } else {
                            d[keyName] = 0;
                        }
                    });
                }
            },
            applyDataGroupOn: function (key, operator) {
                _dataGroupsParams[key+"@@_"+operator] = operator;
            },
            getDataGroupParams: function () {
                return _dataGroupsParams;
            },
            preProcessFunctions: function (expression) {
                const regex = /(sum|SUM|count|COUNT|avg|AVG|min|max)\(\[([a-z|A-Z|0-9|_|' '|'('|')']*)\]\)?/g;
                const str = expression;
                var flagGroup = false;
                _abstractExpression = expression;
                var m;
                while ((m = regex.exec(str)) !== null) {
                    // This is necessary to avoid infinite loops with zero-width matches
                    if (m.index === regex.lastIndex) {
                        regex.lastIndex++;
                    }
                    this.applyDataGroupOn(m[2], m[1]);
                    _abstractExpression = _abstractExpression.replace(m[0], 'd[\'' + m[2] + "@@_" + m[1] + '\']');
                    flagGroup = true;
                }
                if (!flagGroup) {
                    _dataGroupsParams = {};
                }
            }
        }
    }

    this.CalculateOnData = _calculateOnData;
})();
module.exports = CalculateOnData;