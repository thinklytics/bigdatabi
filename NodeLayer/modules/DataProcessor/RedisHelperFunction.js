var redis = require("redis");
var snappy = require('snappy');
var Promise = require('es6-promise').Promise;
var ioRedis = require('ioredis');
var ioRed=new ioRedis();
var _=require('underscore');
var calculate = require('./CalculateOnData');
var zlib = require('zlib');
var RedisHelper =(function () {
    var _client=redis.createClient();
    var _incrementObj;
    return {
        printAllKeys: function () {
            _client.keys('*', function (err, keys) {
                if (err) return console.log(err);
                for (var i = 0, len = keys.length; i < len; i++) {

                }
            });
        },
        roleLevelCondition:function (metadataKey,callback) {
            _client.hgetall("metadataDetails", function (metaVal, metaResult) {
                var userType=false;
                try{
                    if(JSON.parse(metaResult[metadataKey]).userType){
                        userType=JSON.parse(metaResult[metadataKey]).userType;
                    }
                    var roleLevelCondition=JSON.parse(metaResult[metadataKey]).roleLevelCondition;
                    var department=JSON.parse(metaResult[metadataKey]).department;
                }catch (e){

                }

                callback(roleLevelCondition,department,userType);
            });
        },
        decryptRadisData:function (data) {
            var decrptData = zlib.inflateSync(new Buffer(data, 'base64')).toString();
            decrptData=JSON.parse(JSON.parse(decrptData));
            return decrptData;
        },
        getKeyData: function (key,metadataKey,ClientInfo, callback) {
            try{
                _client.hgetall("metadataDetails", function (metaVal, metaResult) {
                    var dataLength=metaResult[key];
                    if(ClientInfo.realtime){
                        key=key+"-realtime";
                    }
                    _client.hgetall(key, function (val, result) {
                        if(result){
                            var tableColumn=[];
                            try{
                                tableColumn  = result['tableColumn'];
                                //delete result['tableColumn'];
                            }catch(e){

                            }
                            var data=[];
                            var dataObj={};
                            _.each(result,function(val,key){
                                if(key!="tableColumn"){
                                    if(dataLength>=100000){
                                        RedisHelper.decryptRadisData(val).forEach(function (d,index) {
                                            data.push(d);
                                            dataObj[index+key]=d;
                                        });
                                    }else{
                                        data.push(JSON.parse(val));
                                        dataObj[key]=JSON.parse(val);
                                    }
                                }
                            });
                            callback(data,tableColumn,dataObj);
                        }
                    });
                });
            }catch (e){
                callback([],[],{});
            }

        },
        setIncrementObj:function (incrementObj) {
            _incrementObj=incrementObj
        },
        getLength:function (key,metadataKey,callback) {
            _client.hgetall(key, function (val, result) {
                if(result && result[metadataKey]){
                    callback(JSON.parse(result[metadataKey]).length);
                }else{
                    callback(false);
                }
            });
        },
        incrementObj:function (key,metadataKey,callback) {
            _client.hgetall(key, function (val, result) {
                if(result && result[metadataKey]){
                    result[metadataKey]=JSON.parse(result[metadataKey]);
                    if(result[metadataKey]['incrementObj']){
                        _incrementObj=result[metadataKey];
                        callback(true);
                    }else{
                        callback(false);
                    }
                }else{
                    callback(false);
                }
            });
        },
        clearRedis: function () {
            _client.flushdb( function (err, succeeded) {
                 // will be true if successfull
            });
        },
        testIoRedis:function(){
            ioRed.get(key, function (val, result) {
                callback(result);
            });
        },
        limitData:function (from,to,key,client,incrementObj,callback) {
            if(incrementObj){
                var clientKey = client.getMetadataKey();
                _client.hgetall("metadataDetails", function (metaVal, metaResult) {
                    var redisClientDetails = JSON.parse(metaResult[clientKey]);
                    _client.hgetall(key, function (val, result) {
                        var clientDepartmentWise=client.getMetadataId()+":"+client.getCompanyId();
                        //console.log(redisClientDetails,client.getUsertype());
                        /*if(client.getUsertype() && redisClientDetails.roleLevelCondition!=undefined && redisClientDetails.roleLevelCondition!=""){
                            //Nothing
                        }else*/
                        if(redisClientDetails.roleLevelCondition!=undefined && redisClientDetails.roleLevelCondition!="" && client.getUsertype()==false && client.getDepartment()){
                            clientDepartmentWise=client.getMetadataId()+":"+client.getCompanyId()+":"+client.getDepartment();
                        }
                        if (_incrementObj["updated_key"]) {
                            if (_incrementObj["updated_key"]) {
                                if(client.getUsertype()){
                                    clientDepartmentWise=clientDepartmentWise+":"+client.getId();
                                }else if(client.getRealtime()){
                                    clientDepartmentWise=clientDepartmentWise+"-realtime";
                                }
                                _incrementObj["updated_key"].forEach(function (d) {
                                    if (result && result[d]) {
                                        var data = JSON.parse(result[d]);
                                        /*
                                         * Need to recheck
                                         */
                                        global.dataMetaWise[clientDepartmentWise]['data'][d] = data;
                                    }
                                });
                            }
                        }
                        callback();
                    });
                });
            }else{
                 var clientKey = client.getMetadataKey();
                _client.hgetall("metadataDetails", function (metaVal, metaResult) {
                    var redisClientDetails=JSON.parse(metaResult[clientKey]);
                    _client.hgetall(key, function (val, result) {
                        if(redisClientDetails.newCreatedKey && redisClientDetails.newCreatedKey.length){
                            var clientDepartmentWise=client.getMetadataId()+":"+client.getCompanyId();
                            if(redisClientDetails.roleLevelCondition!=undefined && redisClientDetails.roleLevelCondition!=""){
                                clientDepartmentWise=client.getMetadataId()+":"+client.getCompanyId()+":"+client.getDepartment();
                            }
                            redisClientDetails.newCreatedKey.forEach(function (d) {
                                var data = JSON.parse(result[d]);
                                global.dataMetaWise[clientDepartmentWise]['data'][d]=data;
                            });
                        }
                        callback();
                    });
                });
            }
        },
        setMetadataLength:function (length,metadataKey) {
            _client.hgetall("metadataDetails", function (metaVal, metaResult) {
                var redisClientDetails=JSON.parse(metaResult[metadataKey]);
                redisClientDetails.length=length;
                //redisClientDetails.roleLevelCondition=JSON.parse(redisClientDetails.roleLevelCondition);
                redisClientDetails=JSON.stringify(redisClientDetails);
                _client.hset("metadataDetails",metadataKey,redisClientDetails);
            });
        }
    }
})();
module.exports=RedisHelper;
