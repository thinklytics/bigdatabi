var TooltipModule = (function () {
    var module = {};
    module.processTooltipText = function (d,g,v) {
        var val = v[d.columnName];
        if (isNaN(val) && val == null || isNaN(val) || val == 'NaN' || val == undefined) {
            val = 1;
        }else {
            val = Math.round(val);
        }
        if(isNaN(val)){
            val=0;
        }
        if (!g["<"+d.aggregate+"(" + d.reName + ")>"])
            g["<"+d.aggregate+"(" + d.reName + ")>"] = 0;
        if(d.aggregate==undefined){
            d.aggregate="sum";
        }else if(d.aggregate=="avg"){
            if(!g.tooltipAvg){
                g.tooltipAvg={};
                g.tooltipAvg.sum =0;
                g.tooltipAvg.count=0;
            }
        }
        switch (d.aggregate) {
            case "sum":
                g["<sum(" + d.reName + ")>"] += val;
                break;
            case "count":
                g["<count(" + d.reName + ")>"]++;
                break;
            case "avg":
                g.tooltipAvg.sum += val;
                g.tooltipAvg.count++;
                g["<avg(" + d.reName + ")>"]=g.tooltipAvg.sum/g.tooltipAvg.count;
                break;
            default :
                g["<sum(" + d.reName + ")>"] += val;
        }
    };

    module.processTooltipTextReduce = function (d,g,v) {
        var val = v[d.columnName];
        if (isNaN(val) && val == null || isNaN(val) || val == 'NaN' || val == undefined) {
            val = 1;
        }else {
            val = Math.round(val);
        }
        if(isNaN(val)){
            val=0;
        }
        if (!g["<"+d.aggregate+"(" + d.reName + ")>"])
            g["<"+d.aggregate+"(" + d.reName + ")>"] = 0;
        if(d.aggregate==undefined){
            d.aggregate="sum";
        }else if(d.aggregate=="avg"){
            if(!g.tooltipAvg){
                g.tooltipAvg={};
                g.tooltipAvg.sum =0;
                g.tooltipAvg.count=0;
            }
        }
        switch (d.aggregate) {
            case "sum":
                g["<sum(" + d.reName + ")>"] -= val;
                break;
            case "count":
                g["<count(" + d.reName + ")>"]--;
                break;
            case "avg":
                g.tooltipAvg.sum -= val;
                g.tooltipAvg.count--;
                g["<avg(" + d.reName + ")>"]=g.tooltipAvg.sum/g.tooltipAvg.count;
                break;
            default :
                g["<sum(" + d.reName + ")>"] -= val;
        }
    };

    module.processTooltipGrp = function (d,g,v) {
        var val = v[d.columnName];
        if (isNaN(val) && val == null || isNaN(val) || val == 'NaN' || val == undefined) {
            val = 0;
        }else {
            val = parseFloat(val);
        }
        if(d.aggregate=="sum"){
           return val;
        }else if(d.aggregate=="count"){
            return 1;
        }else if(d.aggregate=="avg"){
            return {"sum":val,"count":1};
        }
        return val;
    };

    module.processTooltipTextWithFixedKeyword = function (d,g,v,tooltipData,formulaObject) {
        var formula = formulaObject[d.columnName];
        var val=eval(formula);
        if(val!=0){
            if (!g["<sum(" + d.reName + ")>"])
                g["<sum(" + d.reName + ")>"] = 0;
            if (!g["<count(" + d.reName + ")>"])
                g["<count(" + d.reName + ")>"] = 0;
            switch (d.aggregate) {
                case "sum":
                    g["<sum(" + d.reName + ")>"] = val;
                    break;
                case "count":
                    g["<count(" + d.reName + ")>"];
                    break;
                default :
                    g["<sum(" + d.reName + ")>"] = val;
            }
        }
        return g;
    };

    module.processTooltipTextWithGroupCalculation=function (d, g, v, groupColor, client) {
        if (g[d.columnName] == undefined) {
            g[d.columnName] = {};
            var formula = reduceMultilevelFormula(d.formula, client);
            var parsedFormulaObj = parseFormula(formula, client);
            g[d.columnName]['formula'] = parsedFormulaObj['formula'];
            g[d.columnName]['params'] = parsedFormulaObj['params'];
        }
        var params = g[d.columnName]['params'];
        var formula = g[d.columnName]['formula'];
        $.each(params, function (val, key) {
            var value = v[key];
            if (!value || isNaN(value)) {
                value = 0;
            } else {
                value = parseFloat(value);
            }
            if (val == 'sum' || val == 'avg') {
                if (!g['sum'][key])
                    g['sum'][key] = 0;
                g['sum'][key] += value;
            }
            if (val == 'count' || val == 'avg') {
                if (!g['count'][key])
                    g['count'][key] = 0;
                g['count'][key] += 1;
            }
            if (val == 'avg') {
                if (!g['avg'][key]) {
                    g['avg'][key] = 0;
                }

                g['avg'][key] = g['sum'][key] / g['count'][key];
                if (isNaN(g['avg'][key])) {
                    g['avg'][key] = 0;
                }
            }
        });
        val = parseFloat(eval(formula));
        if (isNaN(val) && val == null || isNaN(val) || val == 'NaN' || val == undefined) {
            val = 1;
        } else {
            val = Math.round(val);

        }
        if (isNaN(val)) {
            val = 0;
        }
        if (!g["<sum(" + d.reName + ")>"])
            g["<sum(" + d.reName + ")>"] = 0;
        if (!g["<count(" + d.reName + ")>"])
            g["<count(" + d.reName + ")>"] = 0;

        switch (d.aggregate) {
            case "sum":
                g["<sum(" + d.reName + ")>"] = val;
                break;
            case "count":
                g["<count(" + d.reName + ")>"];
                break;
            default :
                g["<sum(" + d.reName + ")>"] = val;
        }
        return g;
    };
    return module;
}());
module.exports = TooltipModule;