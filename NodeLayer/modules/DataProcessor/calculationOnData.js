var d3 = require('d3');
var $ = require('underscore');
var calculate = require('./CalculateOnData');

(function () { 
    function _CalculationOnData(data,oldColumn,newColumn,client) {
        //var draw={};
        var _data = data;
        try{
            var _oldColumn=JSON.parse(oldColumn);
        }catch(e) {
            var _oldColumn=oldColumn;
        }

        var _newColumn=newColumn;
        var _calObject={};
        var _newKeysObj={};
        var _categoryArray=[];
        var _calArray=[];
        var _categories={};
        var _categoryObject={};
        var _client=client;
        var calculateData=calculate(_data);
        return {
            uniqueArray : function () {

                $.each(_newColumn, function (key, value) {
                    _newColumn[key.reName] = key;
                    if (key.type == "custom") {
                        if (key.formula != undefined) {
                            _calObject[key.reName] = key;
                            _calArray.push(key);
                        }
                        else {
                            _categoryArray.push(key);
                        }
                    }
                });
                return this;
            },
            columnCal:function(){
                client.setCalArray(_calArray);
                _calArray.forEach(function(e,index){
                    var columnName=e.columnName;
                    var formula=e.formula;
                    //formula=reduceMultilevelFormula(formula,_client);

                    calculateData.processDataExpression(formula,e,'','',_client);
                });
                return this;
            },
            groupObject:function(groupObj){
                if(groupObj!=undefined){
                    _categoryObject=JSON.parse(groupObj);
                    if(Object.keys(_categoryObject).length){
                        $.each(_categoryObject,function(key,value){
                            _categories[value.categoryName]=value;
                        });
                    }
                }
                return this;
            },
            columnCategory:function(){
                if(_categoryObject!=undefined && Object.keys(_categoryObject).length){
                    Object.keys(_categoryObject).forEach(function(d){
                        var columnName=_categoryObject[d].columnName;
                        var categoryName=_categoryObject[d].categoryName;
                        var categoryData=_categoryObject[d].groupData;
                        _data.forEach(function(e){
                            var keysVal="";
                            Object.keys(categoryData).forEach(function(r){
                                var index=categoryData[r].indexOf(e[columnName]);
                                if(index!=-1){
                                    keysVal=r;
                                }
                            });
                            if(keysVal==""){keysVal=e[columnName];}
                            e[categoryName]=keysVal;
                        });
                    });
                }
                return this;
            }
        }
    }
    this.CalculationOnData = _CalculationOnData;
})();
module.exports = CalculationOnData;