var CalculationHelperModule = (function () {
    var module = {};
    module.reduceMultilevelFormula = function (formula, client) {
        const regex = /(?:sum|SUM|count|COUNT|avg|AVG)\(\[([\w|' ']*)\]\)/g;
        var reducedFormula = formula;
        var runFormula = formula;

        var m;
        while ((m = regex.exec(formula)) !== null) {
            try {
                var fieldObject = client.getTableColumn()[m[1]];
                if (fieldObject.type == "custom" && isAggregateField(fieldObject.formula)) {
                    reducedFormula = reducedFormula.replace(m[0], "(" + fieldObject.formula + ")", client);
                }

            } catch (e) {
                return reducedFormula;
            }
        }

        if (hasMultiLevelCalculation(reducedFormula, client)) {
            reducedFormula = reduceMultilevelFormula(reducedFormula, client);

        } else {
            const checkDivide = /\(?(?:sum|SUM|count|COUNT|avg|AVG)?\(?\[[\w|' ']*\]\)?\)\/\(?(?:sum|SUM|count|COUNT|avg|AVG)?\(?\[[\w|' ']*\]\)?\)/g;
            var o;
            var i = 1;
            var replacements = {};
            while ((o = checkDivide.exec(reducedFormula)) !== null) {

                replacements[o[0]] = "(isNaN(" + o[0] + ")?0:" + o[0] + ")";

                i++;
            }

            $.each(replacements, function (key, val) {
                reducedFormula = reducedFormula.replace(key, val);
            });

            return reducedFormula;
        }
        return reducedFormula;
    };

    module.hasMultiLevelCalculation = function (formula, client) {


        var flag = false;
        const regex = /\[([\w|' ']*)\]/g;


        var m;
        while ((m = regex.exec(formula)) !== null) {
            try {
                if (m[1]) {
                    var tableColumns = client.getTableColumn();
                    var fieldObject = tableColumns[m[1]];
                    if (fieldObject.type == "custom" && isAggregateField(fieldObject.formula)) {
                        flag = true;
                    }
                }
            } catch (e) {
                return false;
            }
        }
        return flag;
    };

    module.isCalculated = function (formula, client) {

        var flag = hasMultiLevelCalculation(formula, client) || isAggregateField(formula);
        return flag;
    };

    module.parseFormula = function (expression) {
        const regex = /(sum|SUM|count|COUNT|avg|AVG)\(\[([a-z|A-Z|0-9|_|' '|'('|')']*)\]\)/g;
        const str = expression;
        var params = {};
        var alteredFormula = expression;
        while ((m = regex.exec(str)) !== null) {
            if (m.index === regex.lastIndex) {
                regex.lastIndex++;
            }
            params[m[2]] = m[1];
            alteredFormula = alteredFormula.replace(m[0], 'parseFloat(g[\'' + m[1] + '\'][\'' + m[2] + '\'])');
        }

        return {formula: alteredFormula, params: params};
    };
    return module;
}());
module.exports = CalculationHelperModule;