var TooltipModule=require('../TooltipModule');
var FixedCalculationModule = (function () {
 var module = {};
//............................... Helper Functions........................................

    module.checkForSingleArgs=function(formula){
        const regex =/fixed\((sum|SUM|count|COUNT|avg|AVG)\(\[([\w+|\(|\)|\s]+)\]\)\)/gm;
        var m;
        while ((m = regex.exec(formula)) !== null) {
            return true;
        }
    };
    module.processForSingleArgs=function(formula,_data,client){
        const regex =/fixed\((sum|SUM|count|COUNT|avg|AVG)\(\[([\w+|\(|\)|\s]+)\]\)\)/gm;
        var m;
        var group={};
        while ((m = regex.exec(formula)) !== null) {
            if (m[1] == 'sum') {
                _data.forEach(function (d) {
                    if(!group[m[2]])
                        group[m[2]]=0;

                    group[m[2]] += parseFloat(d[m[2]]);
                });
                group[m[2]]=group[m[2]].toFixed(2);
            }
        }
        return group;
    }
    module.genrateFormulaForSingleArgsWithTooltip=function(d) {
        formula=d.formula;
        const regex =/fixed\((sum|SUM|count|COUNT|avg|AVG)\(\[([\w+|\(|\)|\s]+)\]\)\)/gm;
        var m;
        var newForm=formula;
        while ((m = regex.exec(formula)) !== null) {
            newForm = newForm.replace(m[0], "tooltipData['" + d.columnName + "']['" + m[2] + "']");
        }

        var reg = /\[([\w+|\(|\)|\s]+)\]/gm;
        var n;
        while ((n = reg.exec(newForm)) !== null) {
            // This is necessary to avoid infinite loops with zero-width matches
            if (n.index === regex.lastIndex) {
                reg.lastIndex++;

            }
            newForm=newForm.replace(n[0],"v['"+n[1]+"']");
        }
        return newForm;
    }

    module.genrateFormulaForSingleArgsWithoutTooltip=function(expr) {
        formula=expr;
        const regex =/fixed\((sum|SUM|count|COUNT|avg|AVG)\(\[([\w+|\(|\)|\s]+)\]\)\)/gm;
        var m;
        var newForm=formula;
        while ((m = regex.exec(formula)) !== null) {
            newForm = newForm.replace(m[0],"dataGroup['" + m[2] + "']");
        }

        var reg = /\[([\w+|\(|\)|\s]+)\]/gm;
        var n;
        while ((n = reg.exec(newForm)) !== null) {
            // This is necessary to avoid infinite loops with zero-width matches
            if (n.index === regex.lastIndex) {
                reg.lastIndex++;
            }
            newForm=newForm.replace(n[0],"d['"+n[1]+"']");
        }
        return newForm;
    }

    module.checkForDimensionArgs=function(formula){
        const regex = /fixed\(\[([\w+|\(|\)|\s]+)\]\,(sum|SUM|count|COUNT|avg|AVG)\(\[([\w+|\(|\)|\s]+)\]\)\)/gm;
        const str = formula;
        var m;


        while ((m = regex.exec(str)) !== null) {
            // This is necessary to avoid infinite loops with zero-width matches

            if (m.index === regex.lastIndex) {
                regex.lastIndex++;
            }
            return true;
            // The result can be accessed through the `m`-variable.
        }


    };

    module.processForDimensionArgs=function(formula,_data,client){
        const regex = /fixed\(\[([\w+|\(|\)|\s]+)\]\,(sum|SUM|count|COUNT|avg|AVG)\(\[([\w+|\(|\)|\s]+)\]\)\)/gm;
        var m;
        var group={};
        while ((m = regex.exec(formula)) !== null) {
            if (m[2] == 'sum') {
                _data.forEach(function (d) {
                    if(!group[d[m[1]]])
                        group[d[m[1]]]={};
                    if(!group[d[m[1]]][m[3]])
                        group[d[m[1]]][m[3]]=0;
                    group[d[m[1]]][m[3]] += parseFloat(d[m[3]]);
                });


            }
        }
        return group;
    }

    module.genrateFormulaForDimensionArgsWithTooltip=function(d) {
        formula=d.formula;
        const regex = /fixed\(\[([\w+|\(|\)|\s]+)\]\,(sum|SUM|count|COUNT|avg|AVG)\(\[([\w+|\(|\)|\s]+)\]\)\)/gm;
        var m;
        var newForm=formula;
        while ((m = regex.exec(formula)) !== null) {
            newForm = newForm.replace(m[0], "tooltipData['" + d.columnName + "']['" + m[3] + "']");
        }

        var reg = /\[([\w+|\(|\)|\s]+)\]/gm;
        var n;
        while ((n = reg.exec(newForm)) !== null) {
            // This is necessary to avoid infinite loops with zero-width matches
            if (n.index === regex.lastIndex) {
                reg.lastIndex++;

            }
            newForm=newForm.replace(n[0],"v['"+n[1]+"']");


        }
        return newForm;
    }

    module.genrateFormulaForDimensionArgsWithoutTooltip=function(expr) {
        formula=expr;
        const regex = /fixed\(\[([\w+|\(|\)|\s]+)\]\,(sum|SUM|count|COUNT|avg|AVG)\(\[([\w+|\(|\)|\s]+)\]\)\)/gm;
        var m;
        var newForm=formula;
        while ((m = regex.exec(formula)) !== null) {
            newForm = newForm.replace(m[0], "dataGroup['" + m[3] + "']");
        }

        var reg = /\[([\w+|\(|\)|\s]+)\]/gm;
        var n;
        while ((n = reg.exec(newForm)) !== null) {
            // This is necessary to avoid infinite loops with zero-width matches
            if (n.index === regex.lastIndex) {
                reg.lastIndex++;

            }
            newForm=newForm.replace(n[0],"d['"+n[1]+"']");


        }
        return newForm;
    }

    module.hasFixedCalculation = function (formula) {
        if(module.checkForDimensionArgs(formula) || module.checkForSingleArgs(formula)){
                return true;
        }
        return false;
    };

   module.processTooltipFormula=function(tooltip,dimension,client){
        var processedTooltipFormula={};
        tooltip.datakeys.forEach(function (d) {
            if(FixedCalculationModule.hasFixedCalculation(d.formula)){
                processedTooltipFormula[d.columnName]=module.genrateFormula(d);
            }
        });

        return processedTooltipFormula;
    }

    module.processTooltipData=function(tooltip,dimension,client){
        var processedDataOfTooltip={};
        tooltip.datakeys.forEach(function (d) {
            if(FixedCalculationModule.hasFixedCalculation(d.formula)){
                processedDataOfTooltip[d.columnName] =module.processFormula(d.formula,dimension.top(Infinity),client);
            }
        });
        return processedDataOfTooltip;
    }

    module.genrateFormula=function(d){
        if(module.checkForSingleArgs(formula)){
            return module.genrateFormulaForSingleArgsWithTooltip(d);
        }else{
            return module.genrateFormulaForDimensionArgsWithTooltip(d);
        }
    }
    module.genrateFormulaWithoutTooltip=function(expr){
        if(module.checkForSingleArgs(expr)){
            return module.genrateFormulaForSingleArgsWithoutTooltip(expr);
        }else{
            return module.genrateFormulaForDimensionArgsWithoutTooltip(expr);
        }
    }
    module.processFormula = function(formula,_data,client) {
        if(module.checkForSingleArgs(formula)){
          return module.processForSingleArgs(formula,_data,client);
        }else{
          return module.processForDimensionArgs(formula,_data,client);
        }
    }

    module.createGroup=function (dimension, measureObj, groupColorName,customTooltip,client) {
        var measureColumnName = measureObj.columnName;
        var grp = dimension.group().reduceSum(function (d) {
            return parseFloat(d[measureColumnName]);
        });
        return grp;
    }
    module.createGroupWithFixedTooltip=function(dimension, measureObj, groupColor,customTooltip,client){
        var tooltipData= module.processTooltipData(customTooltip,dimension,client);
        var formulaObject=module.processTooltipFormula(customTooltip,dimension,client);
        if(groupColor)
            var groupColorName=groupColor.columnName;

        return dimension.group().reduce(
            /* callback for when data is added to the current filter results */
            function (g, v) {
                ++g.count;
                var val = v[measureObj.columnName];
                if (!val || isNaN(val)) {
                    val = 0;
                } else {
                    val = parseFloat(val);
                }
                g.sumIndex += val;
                g.customTooltip=true;
                if (customTooltip.datakeys) {
                    customTooltip.datakeys.forEach(function (d) {
                        if (d.dataKey == "Measure") {
                            if(module.hasFixedCalculation(d.formula)){
                                g=TooltipModule.processTooltipTextWithFixedKeyword(d,g,v,tooltipData,formulaObject);
                            }else{
                                TooltipModule.processTooltipText(d, g, v);
                            }
                        } else {
                            if(!(g["<" + d.reName + ">"])){
                                g["<" + d.reName + ">"] = v[d.columnName];
                            }
                            else if(g["<" + d.reName + ">"]!=v[d.columnName]){
                                g["<" + d.reName + ">"]="Multiple Values..";

                            }
                        }
                    });
                }
                if (groupColorName) {
                    var valTemp = (v[groupColorName]);

                    if (!g.groupRepeatCheck[valTemp])
                        g.groupRepeatCheck[valTemp] = val;
                    else
                        g.groupRepeatCheck[valTemp] += val;

                    g.groupColor = g.groupRepeatCheck;
                }
                return g;
            },
            /*
             * callback for when data is removed from the current filter
             * results
             */

            function (g, v) {
                --g.count;
                var val = v[measureObj.columnName];
                if (!val || isNaN(val)) {
                    val = 0;
                } else {
                    val = parseFloat(val);
                }
                g.customTooltip=true;
                if (customTooltip.dataKeys) {
                    customTooltip.dataKeys.forEach(function (d) {
                        if (d.dataKey == "Measure") {
                            if(isCalculated(d.formula,client)){
                                g=TooltipModule.processTooltipTextWithFixedKeyword(d,g,v,tooltipData,formulaObject);
                            }else{
                                TooltipModule.processTooltipText(d, g, v);
                            }
                        } else {
                            if(!g["<" + d.reName + ">"])
                                g["<" + d.reName + ">"] = v[d.columnName];
                            else if(g["<" + d.reName + ">"]!=v[d.columnName]){
                                g["<" + d.reName + ">"]="Multiple Values..";
                            }
                        }

                    });
                }
                var valTemp = (v[groupColorName]);
                if (!g.groupRepeatCheck[valTemp]) {
                    g.groupRepeatCheck[valTemp] = val;

                } else {

                    g.groupRepeatCheck[valTemp] = g.groupRepeatCheck[valTemp] - val;

                }
                g.groupColor = g.groupRepeatCheck;
                g.sumIndex -= val;
                return g;
            },
            /* initialize p */
            function () {
                return {
                    count: 0,
                    sumIndex: 0,
                    avgIndex: 0,
                    groupColor: {},
                    groupRepeatCheck: {},
                    sum:{},
                    avg:{}
                };
            });
    }

    return module;
}());
module.exports = FixedCalculationModule;