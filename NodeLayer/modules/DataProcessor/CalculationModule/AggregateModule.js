var TooltipModule=require('../TooltipModule');
var AggregateModule = (function () {
    var module = {};
//............................... Helper Functions........................................
    module.isAggrExist=function (expression) {
        const regex = /(min|max)\(\[([a-z|A-Z|0-9|_|' '|'('|')']*)\]\)?/g;
        const str = expression;
        var flagGroup = false;
        var m;
        while ((m = regex.exec(str)) !== null) {

            // This is necessary to avoid infinite loops with zero-width matches
            if (m.index === regex.lastIndex) {
                regex.lastIndex++;
            }
            flagGroup = true;
            return true;
        }
        return false;
    }
    module.createGroup=function (dimension, measureObj, groupColorName,customTooltip,client,dimensionObj) {
        var measureColumnName = measureObj.columnName;
        const regex = /(min|max)\(\[([a-z|A-Z|0-9|_|' '|'('|')']*)\]\)?/g;
        const str = measureObj.formula;
        var group={};
        return dimension.group().reduce(
            /* callback for when data is added to the current filter results */
            function (g, v) {
                ++g.count;
                var val = v[measureColumnName];
                if (!val || isNaN(val)) {
                    val = 0;
                } else {
                    val = parseFloat(val);
                }
                while ((m = regex.exec(str)) !== null) {
                    if(group[v[dimensionObj.columnName]]==undefined){
                        group[v[dimensionObj.columnName]]={};
                        group[v[dimensionObj.columnName]]['key']=0;
                        group[v[dimensionObj.columnName]]['keyLength']=0;
                    }
                    if(group[v[dimensionObj.columnName]]['key']<v[m[2]]){
                        group[v[dimensionObj.columnName]]['key']=v[m[2]];
                    }
                    group[v[dimensionObj.columnName]]['keyLength']++;
                }
                var grpVal=parseFloat(group[v[dimensionObj.columnName]]['key']);
                g.sumIndex = grpVal;
                return g;
            },
            /*
             * callback for when data is removed from the current filter
             * results
             */
            function (g, v) {
                --g.count;
                var val = v[measureColumnName];
                if (!val || isNaN(val)) {
                    val = 0;
                } else {
                    val = parseFloat(val);
                }
                while ((m = regex.exec(str)) !== null) {
                    if(group[v[dimensionObj.columnName]]==undefined){
                        group[v[dimensionObj.columnName]]={};
                        group[v[dimensionObj.columnName]]['key']=0;
                        group[v[dimensionObj.columnName]]['keyLength']=0;
                    }
                    if(group[v[dimensionObj.columnName]]['key']<v[m[2]]){
                        group[v[dimensionObj.columnName]]['key']=v[m[2]];
                    }
                    group[v[dimensionObj.columnName]]['keyLength']++;
                }
                if(group[v[dimensionObj.columnName]]['key'])
                    g.sumIndex = group[v[dimensionObj.columnName]]['key'];
                return g;
            },
            /* initialize p */
            function () {
                return {
                    count: 0,
                    sumIndex: 0,
                    avgIndex: 0,
                    groupColor: {},
                    groupRepeatCheck: {},
                    tooltipObj:{},
                    sum: {},
                    avg: {}
                };
            });
    }
    return module;
}());
module.exports = AggregateModule;