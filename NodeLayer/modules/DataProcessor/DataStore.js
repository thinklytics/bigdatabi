var RedisHelper=require('./RedisHelperFunction');
var Promise = require('es6-promise').Promise;
var $=require('underscore');
var DataStore=(function(){
    return {
      getClientData:function(ClientInfo){
          return new Promise(function(resolve,reject){
              var metadataKey=ClientInfo.id+":"+ClientInfo.metadataId+":"+ClientInfo.companyId+":"+ClientInfo.sessionId;
              var metadataCompanyIdKey=ClientInfo.metadataId+":"+ClientInfo.companyId;//metadata and company id only
              RedisHelper.getKeyData(metadataCompanyIdKey,metadataKey,ClientInfo,function(data,tableColumn,dataObj){
                  RedisHelper.roleLevelCondition(metadataKey,function (roleLevelCondition,department,userType) {
                      var tempData={};
                      tempData['tableColumn']=tableColumn;
                      if(roleLevelCondition && roleLevelCondition.length){
                          var Obj=JSON.parse(roleLevelCondition);
                          var columnName="";
                          var tableVal="";
                          //const regex = /([a-z|A-Z|0-9|_|-| ]*).([a-z|A-Z|0-9|_|-| ]*)=([a-z|A-Z|0-9|'|"| ]*)([or|and|OR|AND|{|}|]*)/gm;
                          const regex = /([a-z|A-Z|_|-| ]*).([a-z|A-Z|_|-| ]*)=([0-9|'|:|\-| ]*)([or|and|OR|AND|{|}|]*)/gm;
                          var compareValue="";
                          var countLength=Object.keys(Obj).length;
                          var j=1;
                          Object.keys(Obj).forEach(function(key,index){
                              Obj[key]=Obj[key].replace(' or ',' {or} ');
                              Obj[key]=Obj[key].replace(' and ',' {and} ');
                              while ((m = regex.exec(Obj[key])) !== null) {
                                  // This is necessary to avoid infinite loops with zero-width matches
                                  if (m.index === regex.lastIndex) {
                                      regex.lastIndex++;
                                  }
                                  m[2]=m[2].replace(/\s/g,'');
                                  // m[3]=m[3].replace(/\s/g,'');
                                  m[1]=m[1].replace(/\s/g,'');
                                  compareValue +="d['"+m[2]+"("+m[1]+")"+"']=="+m[3];
                                  if(m[4]){
                                      if(m[4]=="{and}"){
                                          compareValue +=" && ";
                                      }else if(m[4]=="{or}"){
                                          compareValue +=" || ";
                                      }
                                  }
                              }
                              if(j<countLength){
                                  compareValue +=" && ";
                              }
                              j++;
                          });
                          /*data = data.filter(function (d) {
                              return eval(compareValue);
                          });*/
                          var filtedDataObj={};
                          Object.keys(dataObj).forEach(function (p) {
                                var d=dataObj[p];
                                if(eval(compareValue)) {
                                    filtedDataObj[p]=d;
                                }
                          });

                          /*
                           * Change redis length
                           */
                          RedisHelper.setMetadataLength(Object.keys(filtedDataObj).length,metadataKey);
                      }
                      if(filtedDataObj){
                          dataObj=filtedDataObj;
                      }
                      tempData['data']=data;
                      tempData['dataObj']=dataObj;
                      tempData['department']=department;
                      resolve(tempData);
                  });
              });
          });
      },
      checkRoleLevelCondition:function (ClientInfo) {
          return new Promise(function(resolve,reject){
              var metadataKey=ClientInfo.id+":"+ClientInfo.metadataId+":"+ClientInfo.companyId+":"+ClientInfo.sessionId;
              RedisHelper.roleLevelCondition(metadataKey,function (roleLevelCondition,department,userType) {
                  var tempObj={};
                  tempObj['roleLevelCondition']=roleLevelCondition;
                  tempObj['department']=department;
                  tempObj['userType']=userType;
                  if(department!=undefined && department!='' && roleLevelCondition!=undefined && roleLevelCondition!=''){
                        resolve(tempObj);
                    }
                    resolve(tempObj);
              });
          });
      },
      redisCheckLength:function (ClientInfo) {
        return new Promise(function(resolve,reject) {
            var key = ClientInfo.id + ":" + ClientInfo.metadataId+":"+ClientInfo.companyId+":"+ClientInfo.sessionId;
            RedisHelper.getLength("metadataDetails", key, function (length) {
                resolve(length);
            });
        });
      },
      redisCheckLengthRealtime:function (ClientInfo) {
            return new Promise(function(resolve,reject) {
                var key = ClientInfo.id + "-realtime:" + ClientInfo.metadataId+":"+ClientInfo.companyId+":"+ClientInfo.sessionId+"-realtime";
                RedisHelper.getLength("metadataDetails", key, function (length) {
                    resolve(length);
                });
            });
        },
      redisLimitData:function (fromLength,toLength,ClientInfo,client,incrementObj) {
          if(incrementObj){
              return new Promise(function(resolve,reject) {
                  var key = ClientInfo.id + ":" + ClientInfo.metadataId+":"+ClientInfo.companyId;
                  var metadataCompanyIdKey=ClientInfo.metadataId+":"+ClientInfo.companyId;//metadata and company id only
                  RedisHelper.limitData(fromLength,toLength,metadataCompanyIdKey,client,incrementObj,function (data) {
                      resolve();
                  });
                  /*
                   * For realtime
                   */
                  var key = ClientInfo.metadataId+":"+ClientInfo.companyId+"-realtime";
                  RedisHelper.limitData(fromLength,toLength,key,client,incrementObj,function (data) {
                      resolve();
                  });
              });
          }else{
              return new Promise(function(resolve,reject) {
                  var key = ClientInfo.id + ":" + ClientInfo.metadataId+":"+ClientInfo.companyId;
                  var metadataCompanyIdKey=ClientInfo.metadataId+":"+ClientInfo.companyId;
                  RedisHelper.limitData(fromLength,toLength,metadataCompanyIdKey,client,incrementObj,function (data) {
                      resolve();
                  });
              });
          }
      },
      checkIncrementObj:function (ClientInfo) {
          return new Promise(function(resolve,reject) {
              if(ClientInfo.realtime){
                  var key = ClientInfo.id + "-realtime:" + ClientInfo.metadataId+":"+ClientInfo.companyId+":"+ClientInfo.sessionId+"-realtime";
              }else{
                  var key = ClientInfo.id + ":" + ClientInfo.metadataId+":"+ClientInfo.companyId+":"+ClientInfo.sessionId;
              }
              RedisHelper.incrementObj("metadataDetails", key, function (incrementObj) {
                  resolve(incrementObj);
              });
          });
      }
    };
})();
module.exports=DataStore;