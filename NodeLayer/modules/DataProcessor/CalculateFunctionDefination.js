var moment= require('moment'); 
(function () {
    function _calculateFunctionDefination() {
        return {
            dateTimeDiff: function (format, date1, date2) {
                var date1=moment(date1, "YYYY-MM-DD HH:mm:ss");
                var date2=moment(date2, "YYYY-MM-DD HH:mm:ss");
                var ms = date2.diff(date1);
                //var d = moment.duration(ms);
                var d = moment.duration(ms, 'milliseconds');
                if(format=="hh")
                    var s = d.asHours();
                else if(format=="mm")
                    var s = d.asMinutes() - hours * 60;
                else if(format=="ss")
                    var s=d/1000;
                return s;
            }, 
            DateAdd: function (type, interval, date, dateFormat) {
                var dateObj = new Date(date); 
                if (!dateFormat) {
                    var dateFormat = 'DD-MM-YYYY';
                }
                var momentObj = moment(dateObj).add(interval, type);
                return momentObj.format(dateFormat);
            },
            round: function (value) {
                return Math.round(value);
            },
            isEmpty: function (val) {
                return (val === undefined || val == null || val.length <= 0) ? true : false;
            },
            isNotEmpty : function(val){
                return (val === undefined || val == null || val.length <= 0) ? false : true ;
            }, 
            replace: function (expression, substring, replacement) {
                return expression.replace(
                    substring,
                    replacement);
            },
            parseDate: function (dateVal, format) {
                if (format == null) {
                    return moment(dateVal).toDate();
                } else {
                    return moment(dateVal, format).toDate();
                }
            },
            formatDate: function (date, format) { 
                if (format == null) {
                    return moment(date).format('YYYY-MM-DD');
                } else {
                    return moment(date).format(format);
                }
            },
            currentDate: function (dateFormat) {
                return moment(new Date()).format(dateFormat);
            },
            checkValue: function (value) {

                if (value == null) {
                    return 0;
                }
                return value;
            },
            strlen: function (expression) {
                if (expression != 0) {
                    return expression.length;
                } else {
                    return 0;
                }
            },
            toLower: function (expression) {
                try {
                    if (!Number(expression) && expression != null && expression != "") {
                        return expression.toLowerCase();
                    } else {
                        return "na";
                    }
                } catch (e) {

                }

            },
            toUpper: function (expression) {
                if (!Number(expression) && expression != null && expression != "") {
                    var newString = expression.toUpperCase();
                    return newString;
                } else {
                    return "NA";
                }

            },
            toString: function (expression) {
                return expression.toString();
            },
            trim: function (expression) {
                if (!Number(expression) && expression != null && expression != "") {
                    var newString = expression.trim();
                    return newString; 
                } else {
                    return "NA";
                }
            },
            dateDiff: function (getDate1, getDate2, format) {
                if (format != undefined) {
                    var startDate = moment(getDate1, format);
                    var endDate = moment(getDate2, format);
                } else {
                    var startDate = moment(getDate1);
                    var endDate = moment(getDate2);
                }
                return endDate.diff(startDate, 'days');
            }
        }
    }

    this.CalculateFunctionDefination = _calculateFunctionDefination;
})();
module.exports = CalculateFunctionDefination;