var d3 = require('d3');
var $=require('underscore');
(function () {
    function _GroupOnData(data) {
        var _data = data;
        return {   
            createColumnInData:function(columnName,categoryData,categoryName){
                _data.forEach(function(d){
                    var keysVal = "";
                    Object.keys(categoryData).forEach(function (r) {
                        var index = categoryData[r].indexOf(d[columnName]);
                        if (index != -1) {
                            keysVal = r;
                        }
                    });
                    if (keysVal == "") {
                        keysVal = d[columnName];
                    }
                    d[categoryName] = keysVal;
                });
                return true;
            },
            updateColumnInData:function(columnName,categoryData,categoryName,lastObj){
                _data.forEach(function(d){
                   delete  d[lastObj];
                });
                _data.forEach(function(d){
                    var keysVal = "";
                    Object.keys(categoryData).forEach(function (r) {
                        var index = categoryData[r].indexOf(d[columnName]);
                        if (index != -1) {
                            keysVal = r;
                        }
                    });
                    if (keysVal == "") {
                        keysVal = d[columnName];
                    }
                    d[categoryName] = keysVal;
                });
                return true;
            }
        }
    }
    this.GroupOnData = _GroupOnData;
})();
module.exports = GroupOnData;