    var $=require('underscore');
    var lineChart = (function () {

        var _chart = {};
        _chart.groupHashList = {};
        _chart.dimensionHashList = {};
        return {
            /**
             * Determine if a given chart instance resides in the registry.
             *
             * @method   Add crossfilter group in group hashList
             *
             * @returns Object again for chaining
             */
            reset:function(){
                _chart.groupHashList = {};
                _chart.dimensionHashList = {};
                return this;
            },
            addGroup: function (group, groupKey) {
                try {
                    _chart.groupHashList[groupKey] = group;
                } catch (e) {
                    return this;
                }


                return this;
            },

            /**
             * helps adding multiple dimension
             *
             */
            addDimension: function (dimension, dimensionKey) {
                try {
                    _chart.dimensionHashList[dimensionKey] = dimension;
                } catch (e) {
                    return this;
                }
                return this;
            },


            listGroups: function (callback) {
                try {

                    $.each(_chart.groupHashList, callback);
                } catch (e) {
                    return false;
                }
            },

            listDimension: function (callback) {
                try {
                    $.each(_chart.dimensionHashList, callback);
                } catch (e) {
                    return false;
                }
            },
            render: function () {
                try {
                    var labels = [];
                    var datasets = [];
                    var j = 0;
                    this.listGroups(function (group, groupKey) {
                                var data=[];
                                var finalData = group.all();
                                finalData.forEach(function (d) {
                                    if(j==0)
                                    labels.push(d.key);
                                    data.push(d.value);
                                });

                                var dt = {
                                    label: groupKey,
                                    data: data
                                };

                                datasets.push(dt);
                                j++;

                    });
                    var dataInFormat = {
                                labels: labels,
                                datasets: datasets
                    };
                    return JSON.stringify(dataInFormat);

                } catch (e) {

                }
            }


        };
    })();
    module.exports=lineChart;
