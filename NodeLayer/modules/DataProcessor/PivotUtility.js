var $=require('underscore');
var d3=require('d3');
(function() {
    function PivotUtility(client) {
        var client=client;
        var _dimension=null;
        var rollUpData=function(ptCustTableData_Arr,dataGroup,dataKeyName,type,data){
            if(type == 'average'){
                dataGroup = dataGroup.rollup(function(d) {
                    return d3.mean(d, function(e){
                        return e[dataKeyName];
                    });
                });
            }else if(type == 'count'){
                dataGroup.rollup(function(d) {
                    return d.length;
                });
            }else if(type == 'sum'){
                dataGroup.rollup(function(d) {
                    return d3.sum(d, function(e){
                        return e[dataKeyName];
                    });
                });
            }
            ptCustTableData_Arr.push(dataGroup.entries(data));
        };
        return {
            getDimension:function(){
                 return _dimension;
            },
            getRowCreationData:function(rowAttr,dummyDimension,_reportId,exclude_Row,reOrder_Row){
                if(!_dimension){
                    _dimension = client.createDimension(dummyDimension,_reportId);
                }
                var data = _dimension.top(Infinity);
                if(exclude_Row && Object.keys(exclude_Row).length){
                    var keys=Object.keys(exclude_Row);
                    data = data.filter(function(d){
                        var flag=true;
                        for(var i=0;i<keys.length;i++){
                            if(exclude_Row[keys[i]].indexOf(d[keys[i]])!=-1){
                                flag=false;
                            }
                        }
                        if(flag){
                            return d;
                        }
                    });
                }
                
                var dataGroupRow = d3.nest();
                rowAttr.forEach(function(dataKeyName){
                    dataGroupRow = dataGroupRow.key(function(d) {
                        return d[dataKeyName];
                    });
                });
                dataGroupRow = dataGroupRow.rollup(function(d){ return [{}];}).entries(data);
                dataGroupRow.forEach(function(d,i){
                    if(d.key == null || d.key == 'null'){
                        dataGroupRow.splice(i,1);
                    }
                });

                if(reOrder_Row && reOrder_Row.length){
                    var dataArr = [];
                    reOrder_Row.forEach(function(d){
                        dataGroupRow.forEach(function(dd){
                            if(d == dd.key){
                                dataArr.push(dd);
                            }
                        });
                    });
                    return dataArr;
                }
                return dataGroupRow;
            },

            getColumnCreationData:function(columnAttr,dummyDimension,_reportId,exclude_Col,reOrder_Col){
                if(!_dimension){
                    _dimension=client.createDimension(dummyDimension,_reportId);
                }
                var data=_dimension.top(Infinity);
                if(exclude_Col && Object.keys(exclude_Col).length){
                    var keys=Object.keys(exclude_Col);
                    data = data.filter(function(d){
                        var flag=true;
                        for(var i=0;i<keys.length;i++){
                            if(exclude_Col[keys[i]].indexOf(d[keys[i]])!=-1){
                                flag=false;
                            }
                        }
                        if(flag){
                            return d;
                        }
                    });
                }

                var dataGroupColumn = d3.nest();
                columnAttr.forEach(function(dataKeyName){
                    dataGroupColumn = dataGroupColumn.key(function(d) {
                        return d[dataKeyName];
                    });
                });
                dataGroupColumn = dataGroupColumn.rollup(function(){return [{}];}).entries(data);
                dataGroupColumn.forEach(function(d,i){
                    if(d.key == null || d.key == 'null'){
                        dataGroupColumn.splice(i,1);
                    }
                });

                if(reOrder_Col && reOrder_Col.length){
                    var dataArr = [];
                    reOrder_Col.forEach(function(d){
                        dataGroupColumn.forEach(function(dd){
                            if(d == dd.key){
                                dataArr.push(dd);
                            }
                        });
                    });
                    return dataArr;
                }

                return dataGroupColumn;
            },
            getCreationData:function(dataAttr,rowAttr,columnAttr,dummyDimension,_reportId,exclude_Row,exclude_Col){
                if(!_dimension){
                    _dimension = client.createDimension(dummyDimension,_reportId);
                }
                var data=_dimension.top(Infinity);

                if(exclude_Row && Object.keys(exclude_Row).length){
                    var keys=Object.keys(exclude_Row);
                    data = data.filter(function(d){
                        var flag=true;
                        for(var i=0;i<keys.length;i++){
                            if(exclude_Row[keys[i]].indexOf(d[keys[i]])!=-1){
                                flag=false;
                            }
                        }
                        if(flag){
                            return d;
                        }
                    });
                }
                if(exclude_Col && Object.keys(exclude_Col).length){
                    var keys=Object.keys(exclude_Col);
                    data = data.filter(function(d){
                        var flag=true;
                        for(var i=0;i<keys.length;i++){
                            if(exclude_Col[keys[i]].indexOf(d[keys[i]])!=-1){
                                flag=false;
                            }
                        }
                        if(flag){
                            return d;
                        }
                    });
                }

                var dataGroup = d3.nest();
                rowAttr.forEach(function(dataKeyName){
                    dataGroup = dataGroup.key(function(d) {
                        return d[dataKeyName];
                    });
                });
                columnAttr.forEach(function(dataKeyName){
                    dataGroup = dataGroup.key(function(d) {
                        return d[dataKeyName];
                    });
                });
                var ptCustTableData_Arr = [];
                $.each(dataAttr, function(v,k){
                   rollUpData(ptCustTableData_Arr,dataGroup,k,v,data);
                });
                dataGroup = dataGroup.entries(data);
                return ptCustTableData_Arr;
            },
            filter:function(_dimension,item,selectedDimension){
                var dimensionList = client.getPivotDimensionList();
                $.each(item, function(val,key){
                    if(val.length == 0 || !val){
                        if(dimensionList[key]){
                            dimensionList[key].filterAll();
                        }
                    }
                });
                var filters=item;
                var filter=false;
                if(filters && filters.length>0){
                  filter=true;
                }else{
                    filter=false;
                }
                /*
                 * Flag for data exist or not
                 */
                var flag=true;
                if (filters && !(filter==='true')){
                    $.each(item, function(val,key){
                        if(val.length == 0 || !val){
                            if(dimensionList[key]){
                                dimensionList[key].filterAll();
                            }
                        }else{
                            dimensionList[key].filter(function(d){
                                if(val.indexOf(d.toString()) != -1){
                                    return true;
                                }else{
                                    return false;
                                }
                            });
                        }
                    });
                    /*
                     * For notification test
                     */
                    if(dimensionList[selectedDimension['columnName']]){
                        if(dimensionList[selectedDimension['columnName']].top(Infinity).length){
                            flag=false;
                        }
                    }
                } else{
                     flag=false;
                    _dimension.filterAll();
                }
                return flag;
            }
        }
    }
    this.PivotUtility=PivotUtility;
})();
module.exports=PivotUtility;