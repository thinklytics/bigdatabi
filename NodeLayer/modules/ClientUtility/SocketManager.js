var Request = require("request");
var RedisHelper=require('../DataProcessor/RedisHelperFunction');
var SocketManager=(function(){
     var activeClient={};
     return {
         run:function(io,client,ClientInfo,DataStore,sessionId,time){
             var ClientInfo=ClientInfo;
             var client=client;
             var id=ClientInfo.id;
             var metadataId=ClientInfo.metadataId;
             var companyId=ClientInfo.companyId;
             setInterval(function () {
                      ClientInfo.sessionId=client.getSessionId();
                      var sessionId=client.getSessionId();
                      var currentdate = new Date();
                      var datetime = "Last Sync: " + currentdate.getDate() + "/"
                          + (currentdate.getMonth()+1)  + "/"
                          + currentdate.getFullYear() + " @ "
                          + currentdate.getHours() + ":"
                          + currentdate.getMinutes() + ":"
                          + currentdate.getSeconds();
                      
                     Request.post({
                         "headers": { "content-type": "application/json","Connection":"keep-alive",'accept-encoding': 'gzip, deflate'},
                         "url": "http://bigdataapi.thinklytics.io/api/redisUpdateNode",
                         "body": JSON.stringify({
                             "client": id+"-realtime"+":"+metadataId+":"+companyId,
                             "metadataId": metadataId,
                             "sharedviewId":client.getSharedviewId(),
                             "sessionId":client.getSessionId(),
                             "companyId":companyId
                         })
                     }, (error, response, body) => {
                         if(error) {
                             return console.dir(error);
                         }
                         if(body){
                             body=JSON.parse(body);
                             if(body.flag){
                                 if(body.incrementObjFlag){
                                     if(body.incrementObj && body.incrementObj.updated_key){
                                         RedisHelper.setIncrementObj(body.incrementObj);
                                         DataStore.redisLimitData("","",ClientInfo,client,true).then(function () {
                                             client.removeFilters();
                                             client.changeCossfilterData();
                                             io.emit(id+metadataId+sessionId,'heello');
                                         });
                                     }
                                 }else{
                                     /*
                                      * With out increment object
                                      */
                                     DataStore.redisCheckLengthRealtime(ClientInfo).then(function (lenght) {
                                         if(lenght) {
                                             var clientDataLength = client.getData().length;
                                             if (lenght > clientDataLength) {
                                                 DataStore.redisLimitData(clientDataLength, lenght, ClientInfo, client, false).then(function () {
                                                     client.removeFilters();
                                                     client.changeCossfilterData();
                                                     io.emit(id + metadataId+sessionId, 'heello');
                                                 });
                                             }
                                         }
                                     });
                                 }
                             }
                         }
                     });
             },10000);
     }
  }
})();
module.exports=SocketManager;