var $=require('underscore');
Object.assign=require("object-assign");
(function(){
    function report(id,dataConfig,chartType){
        var _chartType=chartType;
        var _id=id;
        var _dataConfig=dataConfig;
        var moment = require('moment');
        return {
            getDataConfig:function(){
                return _dataConfig;
            },
            getDimension:function(){
                return _dataConfig.getDimension();
            },
            getGroups:function(){
                return _dataConfig.getGroups();
            },
            getGroupColorDimension:function(){
                return _dataConfig.getGroupDimension();
            },
            setConfigObj:function(dataConfig){
                var axisConfig=_dataConfig.getconfigObj();
                axisConfig=dataConfig;
            },
            getDataGroups:function(client,axisConfig){
                /*
                 * axisConfig only use for redraw charts other wise undefined
                 */
                var data={};
                var allData={};
                var delFlag=false;
                var dataConfigDualX="";
                var _dataObj={};
                var startPage=0;
                var endPage=50;
                if(axisConfig!=undefined){
                    startPage=axisConfig.startPagination;
                    endPage=axisConfig.endPagination;
                    _dataConfig.getconfigObj().dataFormat= axisConfig.dataFormat;
                }else if(_dataConfig.getconfigObj() && _dataConfig.getconfigObj().startPagination){
                    startPage=_dataConfig.getconfigObj().startPagination;
                    endPage=_dataConfig.getconfigObj().endPagination;
                }
                if(_dataConfig.getconfigObj() && _dataConfig.getconfigObj().dataFormat && _dataConfig.getconfigObj().dataFormat.xaxis && _dataConfig.getconfigObj().dataFormat.xaxis.dualX && _dataConfig.getconfigObj().dataFormat.xaxis.dualX!='no'){
                    dataConfigDualX= _dataConfig.getconfigObj().dataFormat.xaxis;
                    var dualXaxisDimension=JSON.parse(dataConfigDualX.dualXDimension).columnName;
                    var dualXaxisDimensionType=JSON.parse(dataConfigDualX.dualXDimension).columType;
                    var _data=client.getData();
                    var _dataObject={};
                    var dimension=Object.values(_dataConfig.getconfigObj().checkboxModelDimension)[0].columnName;
                    var dimensionColumnType=Object.values(_dataConfig.getconfigObj().checkboxModelDimension)[0].columType;
                    _data.forEach(function (d) {
                        if(dimensionColumnType=='date' || dimensionColumnType=='datetime'){
                            if(dataConfigDualX.format && dataConfigDualX.format!='custom'){
                                _dataObj[moment(d[dimension]).format(dataConfigDualX.format)]=d;
                            }else if(dataConfigDualX.format && dataConfigDualX.customFormat){
                                _dataObj[moment(d[dimension]).format(dataConfigDualX.customFormat)]=d;
                            }else{
                                _dataObj[moment(new Date(d[dimension]).toISOString())]=d;
                            }
                        }else{
                            _dataObj[d[dimension]]=d;
                        }
                    });
                }
                // $.each(_dataConfig.getconfigObj()['checkboxModelMeasure'],function(value,key){
                //     if(value.formula && (value.formula.includes("DimensionSum") || value.formula.includes("avg"))){
                //         var activeKey=[Object.keys(_dataConfig.getconfigObj()['checkboxModelDimension'])[0]];
                //         client.applyCalculation(value,activeKey);
                //     }
                // });
                $.each(this.getGroups(),function(group,key){
                    data[key] = group.all();
                    var newDataInstance=data[key];
                    allData[key]=[];
                    var deleteIndex=[];
                    if(dataConfig.getconfigObj().excludeKey){
                        var tempData=[];
                        newDataInstance.forEach(function (p,index) {
                            var indexMatch = dataConfig.getconfigObj().excludeKey.indexOf(p.key);
                            if(indexMatch==-1){
                                tempData.push(p);
                            }
                        });
                        newDataInstance=tempData;
                    }

                    newDataInstance.forEach(function(d,index){
                        var e=Object.assign({},d);
                        if(dataConfigDualX && dataConfigDualX.dualX=='yes'){
                            if(dualXaxisDimensionType=='date' || dualXaxisDimensionType=='datetime'){
                                if(dataConfigDualX.dualXformat=='custom'){
                                    e['dualXKey']=moment(_dataObj[e.key][dualXaxisDimension]).format(dataConfigDualX.dualXcustomFormat);
                                }else{
                                    e['dualXKey']=moment(_dataObj[e.key][dualXaxisDimension]).format(dataConfigDualX.dualXformat);
                                }
                            }else{
                                if((dimensionColumnType=='date' || dimensionColumnType=='datetime') && dataConfigDualX.format==undefined){
                                    e['dualXKey']=_dataObj[moment(e.key)][dualXaxisDimension];
                                }else{
                                    e['dualXKey']=_dataObj[e.key][dualXaxisDimension];
                                }
                            }
                        }
                        if($.isObject(e['value']) && $.isEmpty(e['value']['groupColor']) && !e['value']["customTooltip"] && !e['value']["calculated"]){
                            if(e['value']['val']!=null && !isNaN(e['value']['val']))
                                e['value']=e['value']['val'];
                            else
                                e['value'] = 0;
                            delFlag=true;
                        }
                        allData[key].push(e);
                    });
                    if(dataConfigDualX && dataConfigDualX.dualX=='yes'){
                        $.each(allData, function(k,v){
                            k.sort(function(a, b) {
                                if(a.dualXKey && b.dualXKey){
                                    a.dualXKey=a.dualXKey.toString();
                                    b.dualXKey=b.dualXKey.toString();
                                    var textA = a.dualXKey.toUpperCase();
                                    var textB = b.dualXKey.toUpperCase();
                                    return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
                                }
                            });
                        });
                    }
                });
                // if(delFlag)
                /*
                 * Zero keys remove
                 */
                /*
                 * Sorting data if sorting apply sortMeasure
                 */
                if(_dataConfig.getconfigObj().dataFormat && _dataConfig.getconfigObj().dataFormat.topN){
                    /*
                     * Sort data desc order topnMeasure
                     */
                    allData = this.sortingData(allData,_dataConfig.getconfigObj().dataFormat,_dataConfig.getconfigObj().aggregateModel,"topN");
                    allData = this.topN(allData,_dataConfig.getconfigObj().dataFormat,_dataConfig.getconfigObj().aggregateModel);
                }
                if(_dataConfig.getconfigObj().dataFormat && _dataConfig.getconfigObj().dataFormat.sort){
                    if(_dataConfig.getconfigObj().dataFormat.sortMeasure==undefined || _dataConfig.getconfigObj().dataFormat.sortMeasure==''){
                        _dataConfig.getconfigObj().dataFormat.sortMeasure=Object.keys(_dataConfig.getconfigObj().checkboxModelMeasure)[0];
                    }
                    allData = this.sortingData(allData,_dataConfig.getconfigObj().dataFormat,_dataConfig.getconfigObj().aggregateModel,"sorting");
                }

                var WithoutZeroKeys={};
                Object.keys(allData).forEach(function (d) {
                    WithoutZeroKeys[d]=[];
                    allData[d].forEach(function (val){
                        if(typeof val.value === 'object'){
                            var flag = false;
                            if($.isEmpty(val.value.groupColor)){
                                WithoutZeroKeys[d].push(val);
                            }else{
                                $.each(val.value.groupColor, function(k, v){
                                    if(k != 0){
                                        flag = true;
                                    }
                                });
                                if(flag){
                                    WithoutZeroKeys[d].push(val);
                                }
                            }
                        }else if(val.value!=0){
                            WithoutZeroKeys[d].push(val);
                        }
                    });
                });

                var limitedData={};
                Object.keys(WithoutZeroKeys).forEach(function (d) {
                    limitedData['totalKey']=WithoutZeroKeys[d].length;
                    limitedData['reportId']=_id;
                    limitedData[d]=WithoutZeroKeys[d].slice(startPage,endPage);
                });
                return limitedData;
            },
            getId:function(){
                return _id;
            },
            applyFilter:function(filter,customFormat){
                var filters=filter.filters;
                var filterTemp=[];
                var _dimension=this.getDimension();
                try{
                    if(dataConfig.isDateApplied() && !dataConfig.isDateFormatApplied() && customFormat=="false"){
                        filters.forEach(function (e) {
                            filterTemp.push((moment(e)).toString());
                        });
                        filters= filterTemp;
                    }
                }catch (e){

                }
                if (!(filter.reset==='true')) {
                    try{
                        if(dataConfig.isDateApplied() && !dataConfig.isDateFormatApplied()){
                            _dimension.filter(function (d) {
                                if (filters.indexOf(d.toString()) != -1) {
                                    return true;
                                } else {
                                    return false;
                                }
                            });
                        } else{
                            _dimension.filter(function (d) {
                                if (filters.indexOf(d.toString()) != -1) {
                                    return true;
                                } else {
                                    return false;
                                }
                            });
                        }
                    }catch (e){
                        _dimension.filter(function (d) {
                            if (d && filters.indexOf(d.toString()) != -1) {
                                return true;
                            } else {
                                return false;
                            }
                        });
                    }


                } else{
                    _dimension.filterAll();
                }

                if(_dataConfig.isGroupColorExist()){
                    var groupFilter=filter.datasetFilters;
                    try{
                        this.getGroupColorDimension().filter(function (d) {
                            if (groupFilter.indexOf(d.toString()) != -1) {
                                return true;
                            } else {
                                return false;
                            }
                        });
                    }catch(e){
                        this.getGroupColorDimension().filterAll();
                    }
                }
            },
            filterAll:function(){
                var _dimension=this.getDimension();
                _dimension.filter(null);
                if(_dataConfig.isGroupColorExist()){
                    this.getGroupColorDimension().filterAll();
                }
            },
            sortingData:function (allData,dataFormat,aggrModal,type) {
                var sorting=dataFormat.sort;
                var sortingMeasure= dataFormat.sortMeasure;
                /*
                 * If topN sorting
                 */
                if(type=="topN"){
                    sorting="desc";
                    sortingMeasure=dataFormat.topnMeasure;
                }
                /*
                 * End top N sorting
                 */
                if(sorting=='desc'){
                    allData[sortingMeasure] = allData[sortingMeasure].sort(function(a,b) {
                        if(typeof a.value === 'object'){
                            return (b.value[aggrModal[sortingMeasure].key])-(a.value[aggrModal[sortingMeasure].key]);
                        }else{
                            return (b.value)-(a.value);
                        }
                    });
                }else{
                    allData[sortingMeasure] = allData[sortingMeasure].sort(function(a,b) {
                        if(typeof a.value === 'object'){
                            return (a.value[aggrModal[sortingMeasure].key])-(b.value[aggrModal[sortingMeasure].key]);
                        }else{
                            return (a.value)-(b.value);
                        }
                    });
                }
                if(Object.keys(allData).length>1){
                    /*
                     * Sort key change for other measure keys also(If multiple measure)
                     */
                    var finalGrpObj={};
                    finalGrpObj[sortingMeasure]=allData[sortingMeasure];
                    var sortedLabelArr=[];
                    finalGrpObj[sortingMeasure].forEach(function(d){
                        sortedLabelArr.push(d.key.toString());
                    });
                    $.each(allData,function(value,key){
                        var newArr = [];
                        if(key!=sortingMeasure){
                            value.forEach(function (d,i){
                                var index = sortedLabelArr.indexOf(d.key.toString());
                                if(index!=-1)
                                    newArr[index] = d;
                            });
                            finalGrpObj[key]=newArr;
                        }
                    });
                    allData=finalGrpObj;
                }
                return allData;
            },
            topN:function (allData,dataFormat,aggrModal) {
                var topNData={};
                $.each(allData,function(value,key){
                    topNData[key]=value.slice(0,dataFormat.topN);
                    /*
                     * Other key
                     */
                    if(dataFormat.topNOther!=undefined){
                        var otherKey={key:'other',value:0};
                        if(typeof topNData[key][0].value === 'object'){
                            otherKey={key:'other',value:{}};
                        }
                        value.slice(dataFormat.topN,value.length).forEach(function (d) {
                            if(typeof otherKey.value === 'object'){
                                if(dataFormat.groupColor!=undefined){
                                    Object.keys(d.value).forEach(function (p) {
                                        if(p=='groupColor' || p=='groupRepeatCheck' || p=='tooltipObj'){
                                            if(otherKey.value[p]==undefined){
                                                otherKey.value[p]={};
                                            }
                                            Object.keys(d.value[p]).forEach(function (k) {
                                                if(otherKey.value[p][k]==undefined){
                                                    otherKey.value[p][k]=0;
                                                }
                                                otherKey.value[p][k] +=d.value[aggrModal[key].key]
                                            });
                                        }else{
                                            if(otherKey.value[p]==undefined){
                                                otherKey.value[p]=0;
                                            }
                                            otherKey.value[p] +=d.value[aggrModal[key].key]
                                        }
                                    });
                                }else{
                                    otherKey.value[aggrModal[key].key] +=d.value[aggrModal[key].key];
                                }
                            }else{
                                otherKey.value +=d.value;
                            }
                        });
                        topNData[key].push(otherKey);
                    }
                    /*
                     * End of other keys
                     */
                });
                return topNData;
            }
        }
    }
    this.Report=report;
})();
module.exports=Report;