var $ = require('underscore');
var moment = require('moment');
var fs = require('fs');
(function () {
    function report(id, dataConfig, chartType) {
        var _chartType = chartType;
        var _id = id;
        var _dataConfig = dataConfig;
        var _aggregate = _dataConfig.getAggregate();
        var _dim = _dataConfig.getDimension();
        var _client = _dataConfig.getClient();

        return {
            getDimension: function () {
                return _dataConfig.getDimension();
            },

            getId: function () {
                return _id;
            },

            applyFilter: function (filter) {
                var filters = filter.filters;
                var filtersTemp={};
                $.each(filters,function(value,key){
                    filtersTemp[key.replace('|',"")]=value;
                });
                filters=filtersTemp;
                var chartKeyOrder = filter.chartKeyOrder;
                var _dimensionList = this.getDimension();
                $.each(_dimensionList,function(value,key){
                    value.filterAll();
                });
                var filterTemp={};
                $.each(filters,function(value,key){
                    if (dataConfig.isDateApplied(chartKeyOrder[key]) && !dataConfig.isDateFormatApplied()) {
                        value.forEach(function (e) {
                            if (!filterTemp[key]) {
                                filterTemp[key] = [];
                            }
                            filterTemp[key].push((moment(e)).toString());
                        });
                        filters[key] = filterTemp[key];
                    }
                });
                var flag=true;
                if($.isEmpty(filters)){
                    flag=false;
                    $.each(_dimensionList,function(value,key){
                        value.filterAll();
                    });
                }else{
                    $.each(filters,function(value,key){
                        if(value.length!=0){
                            if (dataConfig.isDateApplied(chartKeyOrder[key]) && !dataConfig.isDateFormatApplied()) {
                                (_dimensionList[chartKeyOrder[key].reName]).filter(function (d) {
                                    if (value.indexOf(d.toString()) != -1) {
                                        return true;
                                    } else {
                                        return false;
                                    }
                                });
                            }else {
                                (_dimensionList[chartKeyOrder[key].reName]).filter(function (d) {
                                    if (value.indexOf(d.toString()) != -1) {
                                        return true;
                                    } else {
                                        return false;
                                    }
                                });
                            }
                        }else{
                            _dimensionList[chartKeyOrder[key].reName].filterAll();
                        }
                        /*
                         * For notification condition check
                         */
                        flag=true;
                        if(_dimensionList[chartKeyOrder[key].reName].top(Infinity).length){
                            flag=false;
                        }
                    });
                }
                return flag;
            },

            filterAll: function () {
                var _dimension = this.getDimension();
                Object.keys(_dimension).forEach(function (d) {
                    _dimension[d].filter(null);
                })
            },

            getDataGroups: function () {
                return {};
            },

            processDataWithD3: function () {
                var _chart = _dataConfig.getConfiguration();
                var keyOfObj = "";
                var data = [];
                data = _dim[0].top(Infinity);
                var tableHeaders = "";
                var k = 0;
                var ColumnArrayForDataTable = [];
                var keys = [];
                if (_dataConfig.getTableSettings() && _dataConfig.getTableColumnOrder()) {
                    keys = [];
                    var simpleKey = [];
                    $.each(_dataConfig.getTableColumnOrder(), function (value, key) {
                        simpleKey.push(JSON.parse(value).reName);
                        keys.push(JSON.parse(value));
                    });
                    var commonArray = [];
                    var commonArrayNames = [];
                    $.each(_chart._measure, function (value, key) {
                        commonArrayNames.push(key);
                        commonArray.push(value);
                    });
                    $.each(_chart._dimension, function (value, key) {
                        commonArrayNames.push(key);
                        commonArray.push(value);
                    });
                    if (simpleKey.length < commonArrayNames.length) {
                        commonArrayNames.forEach(function (d, i) {
                            if (simpleKey.indexOf(d) === -1) {
                                keys.push(commonArray[i]);
                            }
                        });
                    } else if (simpleKey.length === commonArrayNames.length) {
                        for (var j = commonArrayNames.length - 1; j >= 0; j--) {
                            if (simpleKey.indexOf(commonArrayNames[j]) === -1) {
                                keys.push(commonArray[j]);
                                simpleKey.push(commonArray[j].reName);
                            }
                        }
                        for (var j = simpleKey.length - 1; j >= 0; j--) {
                            if (commonArrayNames.indexOf(simpleKey[j]) === -1) {
                                keys.splice(j, 1);
                            }
                        }
                    } else {
                        var temp = simpleKey;
                        for (var j = simpleKey.length - 1; j >= 0; j--) {
                            if (commonArrayNames.indexOf(simpleKey[j]) === -1) {
                                keys.splice(j, 1);
                                //simpleKey.splice(j,1);
                            }
                        }
                    }
                } else {
                    try {
                        $.each(_chart._dimension, function (value, key) {
                            keys.push(value);
                        });
                    } catch (e) {

                    }
                    try {
                        $.each(_chart._measure, function (value, key) {
                            keys.push(value);
                        });
                    } catch (e) {

                    }
                }
                var _tableSetting = null;
                var columnNames = [];
                var _activeKey = [];
                var initialKey = columnNames[0];
                var fieldsArray = keys;
                var keyIndex = 0;
                var TotalObject = {};
            },

            processData: function (pageNo, topN, topnMeasure) {
                // try {
                var _chart = _dataConfig.getConfiguration();
                var keyOfObj = "";
                var data = [];
                if(pageNo==undefined){
                    pageNo=1;
                }
                var page = pageNo;
                data = _dim[Object.keys(_dim)[0]].top(Infinity);

                $.each(_chart._axisConfig.checkboxModelMeasure, function (k,v) {
                    if(k.columnName == topnMeasure || k.reName == topnMeasure){
                        topnMeasure = k.columnName;
                    }
                });

                var GTotalObj = {};
                $.each(_chart._axisConfig.checkboxModelMeasure, function (k,v) {
                    GTotalObj[k.columnName] = {};
                    GTotalObj[k.columnName]['avgIndex'] = 0;
                    GTotalObj[k.columnName]['count'] = 0;
                    GTotalObj[k.columnName]['sumIndex'] = 0;
                });

                //data = allData.slice((page*500)-500,page*500);
                var tableHeaders = "";
                var k = 0;
                var ColumnArrayForDataTable = [];
                var keys = [];
                if (_dataConfig.getTableSettings() && _dataConfig.getTableColumnOrder()) {
                    keys = [];
                    var simpleKey = [];
                    $.each(_dataConfig.getTableColumnOrder(), function (value, key) {
                        simpleKey.push(JSON.parse(value).reName);
                        keys.push(JSON.parse(value));
                    });
                    var commonArray = [];
                    var commonArrayNames = [];
                    $.each(_chart._measure, function (value, key) {
                        commonArrayNames.push(key);
                        commonArray.push(value);
                    });
                    $.each(_chart._dimension, function (value, key) {
                        commonArrayNames.push(key);
                        commonArray.push(value);
                    });
                    if (simpleKey.length < commonArrayNames.length) {
                        commonArrayNames.forEach(function (d, i) {
                            if (simpleKey.indexOf(d) === -1) {
                                keys.push(commonArray[i]);
                            }
                        });
                    } else if (simpleKey.length === commonArrayNames.length) {
                        for (var j = commonArrayNames.length - 1; j >= 0; j--) {
                            if (simpleKey.indexOf(commonArrayNames[j]) === -1) {
                                keys.push(commonArray[j]);
                                simpleKey.push(commonArray[j].reName);
                            }
                        }
                        for (var j = simpleKey.length - 1; j >= 0; j--) {
                            if (commonArrayNames.indexOf(simpleKey[j]) === -1) {
                                keys.splice(j, 1);
                            }
                        }
                    } else {
                        var temp = simpleKey;
                        for (var j = simpleKey.length - 1; j >= 0; j--) {
                            if (commonArrayNames.indexOf(simpleKey[j]) === -1) {
                                keys.splice(j, 1);
                                //simpleKey.splice(j,1);
                            }
                        }
                    }
                } else {
                    try {
                        $.each(_chart._dimension, function (value, key) {
                            keys.push(value);
                        });
                    } catch (e) {

                    }
                    try {
                        $.each(_chart._measure, function (value, key) {
                            keys.push(value);
                        });
                    } catch (e) {

                    }
                }

                var _tableSetting = null;
                var columnNames = [];
                var _activeKey = [];
                var initialKey = columnNames[0];
                var fieldsArray = keys;
                var keyIndex = 0;
                var TotalObject = {};

                function sum(key, processedObj, d) {
                    if (processedObj["total_" + key] === undefined) {
                        processedObj["total_" + key] = 0;
                    }
                    processedObj["total"] = {123: {}};
                }

                aggergateKeyIndexes = [];
                if (_chart._tableSetting) {
                    $.each(_chart._tableSetting.footer, function (k, v) {
                        if (columnNames.indexOf(k) !== -1) {
                            aggergateKeyIndexes.push(columnNames.indexOf(k));
                        }
                    });
                }
                var aggregates = {};
                var subAggregates = {};
                function doAggregate(operation, key, d) {
                    if (operation === "sum") {
                        if (!aggregates[key]) {
                            aggregates[key] = 0;
                            if (parseFloat(d[key]))
                                aggregates[key] += parseFloat(d[key]);
                        } else {
                            if (parseFloat(d[key]))
                                aggregates[key] += parseFloat(d[key]);
                        }
                    } else if (operation === "count") {
                        if (!aggregates[key]) {
                            aggregates[key] = 0;
                            aggregates[key] += 1;
                        } else {
                            aggregates[key] += 1;
                        }
                    } else if (operation === "avg") {
                        if (!aggregates[key]) {
                            aggregates[key] = 0;
                            if (parseFloat(d[key])) {
                                aggregates[key] += parseFloat(d[key]);
                            }
                        } else {
                            if (parseFloat(d[key]))
                                aggregates[key] += parseFloat(d[key]);
                        }
                    }
                }

                var countAggregates = {};
                var sumAggregates = {};

                //Sub string Calculate
                function doSubAggregate(operation, d, initialKey, columnNames, index) {
                    if (operation === "sum") {
                        if (!subAggregates[d[initialKey]]) {
                            subAggregates[d[initialKey]] = {};
                            if (parseFloat(d[columnNames[index]]))
                                subAggregates[d[initialKey]][columnNames[index]] = parseFloat(d[columnNames[index]]);
                            else
                                subAggregates[d[initialKey]][columnNames[index]] = 0;
                        } else {
                            if (parseFloat(d[columnNames[index]])) {
                                if (!subAggregates[d[initialKey]][columnNames[index]]) {
                                    subAggregates[d[initialKey]][columnNames[index]] = 0;
                                }
                                subAggregates[d[initialKey]][columnNames[index]] += parseFloat(d[columnNames[index]]);
                            }
                        }
                    } else if (operation == "count") {
                        if (!subAggregates[d[initialKey]]) {
                            subAggregates[d[initialKey]] = {};
                            if (parseFloat(d[columnNames[index]]))
                                subAggregates[d[initialKey]][columnNames[index]] = 1;
                            else
                                subAggregates[d[initialKey]][columnNames[index]] = 1;
                        } else {
                            if (parseFloat(d[columnNames[index]])) {
                                if (!subAggregates[d[initialKey]][columnNames[index]]) {
                                    subAggregates[d[initialKey]][columnNames[index]] = 0;
                                }
                                subAggregates[d[initialKey]][columnNames[index]] += 1;
                            }
                        }
                    } else if (operation == "avg") {
                        if (!sumAggregates[d[initialKey]]) {
                            sumAggregates[d[initialKey]] = {};
                            if (parseFloat(d[columnNames[index]]))
                                sumAggregates[d[initialKey]][columnNames[index]] = parseFloat(d[columnNames[index]]);
                            else
                                sumAggregates[d[initialKey]][columnNames[index]] = 0;
                        } else {
                            if (parseFloat(d[columnNames[index]])) {
                                if (!sumAggregates[d[initialKey]][columnNames[index]]) {
                                    sumAggregates[d[initialKey]][columnNames[index]] = 0;
                                }
                                sumAggregates[d[initialKey]][columnNames[index]] += parseFloat(d[columnNames[index]]);
                            }
                        }
                        if (!countAggregates[d[initialKey]]) {
                            countAggregates[d[initialKey]] = {};
                            if (parseFloat(d[columnNames[index]]))
                                countAggregates[d[initialKey]][columnNames[index]] = 1;
                            else
                                countAggregates[d[initialKey]][columnNames[index]] = 0;
                        } else {
                            if (parseFloat(d[columnNames[index]])) {
                                if (!countAggregates[d[initialKey]][columnNames[index]]) {
                                    countAggregates[d[initialKey]][columnNames[index]] = 0;
                                }
                                countAggregates[d[initialKey]][columnNames[index]] += 1;
                            }
                        }
                        if (!subAggregates[d[initialKey]]) {
                            subAggregates[d[initialKey]] = {};
                            subAggregates[d[initialKey]][columnNames[index]] = parseFloat(sumAggregates[d[initialKey]][columnNames[index]]) / parseFloat(countAggregates[d[initialKey]][columnNames[index]]);
                        } else {
                            if (!subAggregates[d[initialKey]][columnNames[index]]) {
                                subAggregates[d[initialKey]][columnNames[index]] = 0;
                            }
                            subAggregates[d[initialKey]][columnNames[index]] = parseFloat(sumAggregates[d[initialKey]][columnNames[index]]) / parseFloat(countAggregates[d[initialKey]][columnNames[index]]);
                        }
                    }
                }

                var countBuffer = {};
                var sumBuffer = {};
                var dimensionList = [];
                for (var keyNames in keys) {
                    if (keys[keyNames].dataKey == "Dimension") {
                        dimensionList.push(keys[keyNames]['columnName']);
                    }
                }
                var grandTotal={};

                function aggregateObject(processedObj, currentKey, d, key, lastKeyType, type, index) {
                    try {
                        if (_aggregate[fieldsArray[index]['reName']].key == "sumIndex") {
                            var lastKey = Object.keys(processedObj)[0];
                            var newKey = parseFloat(lastKey) + parseFloat(d[key]);
                            currentKey = newKey;
                            if (newKey != lastKey) {
                                processedObj[newKey] = processedObj[lastKey];
                                delete processedObj[lastKey];
                            }
                        } else if (_aggregate[fieldsArray[index]['reName']].key == "count") {
                            /* //not working with multiple level
                             if (type == "repeated") {
                                var lastKey = Object.keys(processedObj)[0];
                                var newKey = parseInt(lastKey)+ 1;
                                currentKey = newKey;
                                if (newKey != lastKey) {
                                    processedObj[newKey] = processedObj[lastKey];
                                    delete processedObj[lastKey];
                                }
                            } else {
                                var lastKey = Object.keys(processedObj)[0];
                                processedObj[1] = processedObj[lastKey];
                                currentKey = 1;
                                newKey = 1;
                                if (newKey != lastKey) {
                                    processedObj[newKey] = processedObj[lastKey];
                                    delete processedObj[lastKey];
                                }
                            }*/
                            if (!countBuffer[key]) {
                                countBuffer[key] = {};
                            }
                            tempCount=createLevels(dimensionList,countBuffer[key],0,dimensionList.length,d,1);
                            if (!sumBuffer[key]) {
                                sumBuffer[key] = {};
                            }
                            tempSum=createLevels(dimensionList,sumBuffer[key],0,dimensionList.length,d,parseFloat(d[key]));
                            var lastKey = Object.keys(processedObj)[0];
                            var newKey = parseFloat(tempCount);
                            currentKey = newKey;
                            lastKey = parseFloat(lastKey);
                            if (newKey !== lastKey) {
                                processedObj[newKey] = processedObj[lastKey];
                                delete processedObj[lastKey];
                            }
                        }
                        else //if(_aggregate[fieldsArray[index]['reName']].key == "avg")
                        {
                            if (!countBuffer[key]) {
                                countBuffer[key] = {};
                            }
                             tempCount=createLevels(dimensionList,countBuffer[key],0,dimensionList.length,d,1);
                            if (!sumBuffer[key]) {
                                sumBuffer[key] = {};
                            }
                            tempSum=createLevels(dimensionList,sumBuffer[key],0,dimensionList.length,d,parseFloat(d[key]));
                            var lastKey = Object.keys(processedObj)[0];
                            var newKey = parseFloat(tempSum) / parseFloat(tempCount);
                            currentKey = newKey;
                            lastKey = parseFloat(lastKey);
                            if (newKey !== lastKey) {
                                processedObj[newKey] = processedObj[lastKey];
                                delete processedObj[lastKey];
                            }
                        }

                    } catch (e) {

                    }
                    return currentKey;
                }
                var createLevels=function(list,object,i,totalLength,d,val){
                    if(i<totalLength-1){
                        if(!object[d[list[i]]]){
                            object[d[list[i]]]={};
                        }
                       return createLevels(list,object[d[list[i]]],++i,totalLength,d,val);
                    }else{
                        if(!object[d[list[i]]])
                            object[d[list[i]]]=0;
                        object[d[list[i]]]+=val;
                        return object[d[list[i]]];
                    }
                };
                var initializeBuffers = function (key, d, keyType) {
                    if (keyType == "Measure") {
                        if (!countBuffer[key]) {
                            countBuffer[key] = {};
                        }
                        createLevels(dimensionList,countBuffer,0,dimensionList.length,d,1);
                        if (!sumBuffer[key]) {
                            sumBuffer[key] = {};
                        }
                        createLevels(dimensionList,sumBuffer,0,dimensionList.length,d,parseFloat(d[key]));
                    }
                };

                function group(d, index, processedObj, lastKeyType, type) {
                    var newIndex = index + 1;
                    var key = fieldsArray[index]['columnName'];
                    var runningTotal = fieldsArray[index]['runningTotal'];
                    var keyType = fieldsArray[index]['dataKey'].trim();
                    if (keyType === 'Measure') {
                        if (d[key] === null || isNaN(d[key])) {
                            d[key] = 0;
                        }
                    }

                    if (!lastKeyType){
                        lastKeyType = keyType;
                    }
                    if (!type){
                        type = "notrepeated";
                    }
                    if (!processedObj[d[key]]) {
                        var currentKey = d[key];
                        if (keyType === 'Measure') {
                            currentKey = parseFloat(currentKey);
                            if ($.isEmpty(processedObj)) {
                                if(!processedObj[currentKey]){
                                    processedObj[currentKey] = {};
                                }
                                if (_aggregate[fieldsArray[index]['columnName']]) {
                                    if (_aggregate[fieldsArray[index]['columnName']].key == "count" || _aggregate[fieldsArray[index]['columnName']].key == "avgIndex") {
                                        currentKey = aggregateObject(processedObj, currentKey, d, key, lastKeyType, type, index, runningTotal);
                                    }
                                }
                            } else {
                                currentKey = aggregateObject(processedObj, currentKey, d, key, lastKeyType, type, index, runningTotal);
                            }
                        } else {
                            processedObj[currentKey] = {};
                        }
                        if (index < fieldsArray.length - 1) {
                            initializeBuffers(key, d, keyType);
                            processedObj[currentKey] = group(d, newIndex, processedObj[currentKey], lastKeyType, "notrepeated");
                            lastKeyType = keyType;
                            return processedObj;
                        } else {
                            lastKeyType = keyType;
                            return processedObj;
                        }
                    } else {
                        var currentKey = d[key];
                        if (index < fieldsArray.length - 1) {
                            if (keyType === 'Measure') {
                                currentKey = parseFloat(currentKey);
                                currentKey = aggregateObject(processedObj, currentKey, d, key, lastKeyType, type, index, runningTotal);
                            }
                            processedObj[currentKey] = group(d, newIndex, processedObj[currentKey], lastKeyType, "repeated");
                            lastKeyType = keyType;
                            return processedObj;
                        } else {
                            if (keyType === 'Measure') {
                                currentKey = parseFloat(currentKey);
                                currentKey = aggregateObject(processedObj, currentKey, d, key, lastKeyType, type, index, runningTotal);
                            }
                        }

                        lastKeyType = keyType;
                        return processedObj;
                    }
                }

                var finalObj = {};

                data.forEach(function (d, i) {

                    $.each(GTotalObj, function (k, v) {
                        // GTotalObj[v]['avgIndex'] += parseFloat(d[v])/data.length;
                        GTotalObj[v]['count'] = data.length;
                        GTotalObj[v]['sumIndex'] += parseFloat(d[v]);
                        GTotalObj[v]['avgIndex'] = parseFloat(GTotalObj[v]['sumIndex'])/parseInt(data.length);
                    });
                    group(d, 0, finalObj);
                });
                /*
                 * Check lenght last key of object
                 */
                var ObjectLength=0;
                function findObjectlength(obj) {
                    for(var i in obj) {
                        if(obj.hasOwnProperty(i)){
                            if(Object.keys(obj[i]).length==0) { ObjectLength++;}
                            findObjectlength(obj[i]);
                        }
                    }
                }
                findObjectlength(finalObj);
                var count=0;
                var lastkey="";
                var findObjectByLabel = function(obj,finalObjLimit) {
                    for(var i in obj) {
                        if(obj.hasOwnProperty(i)){
                            if(count>=limitTo && count<limitFrom){
                                finalObjLimit[i]={};
                            }
                            if(count>=limitTo && count>=limitFrom){return count;}
                            if(Object.keys(obj[i]).length==0) { count++;}
                            if(finalObjLimit[i]!=undefined)
                                findObjectByLabel(obj[i],finalObjLimit[i]);
                            else
                                findObjectByLabel(obj[i],finalObjLimit);
                        }
                    }
                    return null;
                };
                var finalNewLimitObj={};
                var flag=true;
                var limitTo =(page*100)-100;
                var limitFrom=page*100;
                $.each(finalObj,function (value,key) {
                    if(flag){
                        finalNewLimitObj[key]={};
                        var countObj = findObjectByLabel(value,finalNewLimitObj[key]);
                    }
                    if(count<=limitTo || count>limitFrom){
                        finalNewLimitObj={};
                    }
                    if(count>=limitTo && count>=limitFrom){
                        flag=false;
                    }
                });
                /*
                 * Final limited object sort
                 */
                recurObj(finalNewLimitObj);
                var obj = sortObj(finalNewLimitObj);
                function recurObj(tempObj){
                    $.each(tempObj, function(v,k){
                        if(!$.isEmpty(v)){
                            v = sortObj(v);
                            tempObj[k] = v;
                            recurObj(v);
                        }
                    });
                    return tempObj;
                }
                function sortObj(tempObj){
                    var obj = {};
                    Object.keys(tempObj).sort((a, b) => {
                        return a.toLowerCase().localeCompare(b.toLowerCase());
                    }).forEach(function(key){
                        obj[key] = tempObj[key];
                    });
                    return obj;
                }
                
                var dataInfo={};
                dataInfo['data']=obj;
                dataInfo['info']={};
                dataInfo['info']['totalData'] = ObjectLength;
                dataInfo['info']['GrandTotal'] = GTotalObj;
                return dataInfo;
                //  }catch (e){
                // }
            }
        }
    }
    this.DatatableReport = report;
})();
module.exports = DatatableReport;