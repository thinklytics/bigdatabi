var $=require('underscore');
var d3=require('d3');
(function() {
    function PivotUtility(client) {
          var client=client;
          var _dimension=null;
          var rollUpData=function(ptCustTableData_Arr,dataGroup,dataKeyName,type){
              if(type == 'average'){
                  dataGroup = dataGroup.rollup(function(d) {
                      return d3.mean(d, function(e){
                          return e[dataKeyName];
                      });
                  });
              }else if(type == 'count'){
                  dataGroup.rollup(function(d) {
                      return d.length;
                  });
              }else if(type == 'sum'){
                  dataGroup.rollup(function(d) {
                      return d3.sum(d, function(e){
                          return e[dataKeyName];
                      });
                  });
              }
            ptCustTableData_Arr.push(dataGroup.entries(client.getData()));
        };
        return {
            getClient:function(){
                return client;
            },
	        getDimension:function(){
                 return _dimension;
            },
            getRowCreationData:function(rowAttr,dummyDimension,_reportId){
                if(!_dimension)
                    _dimension=client.createDimension(dummyDimension,_reportId);
                var data=_dimension.top(Infinity);
                var dataGroupRow = d3.nest();
                rowAttr.forEach(function(dataKeyName){
                    dataGroupRow = dataGroupRow.key(function(d) {
                        return d[dataKeyName];
                    });
                });
                dataGroupRow = dataGroupRow.rollup(function(){return [{}];}).entries(data);
                return dataGroupRow;
            },
            getColumnCreationData:function(columnAttr,dummyDimension,_reportId){
                if(!_dimension)
                    _dimension=client.createDimension(dummyDimension,_reportId);
                var data=_dimension.top(Infinity);
                var dataGroupColumn = d3.nest();
                columnAttr.forEach(function(dataKeyName){
                    dataGroupColumn =dataGroupColumn.key(function(d) {
                        return d[dataKeyName];
                    });
                });
                dataGroupColumn = dataGroupColumn.rollup(function(){return [{}];}).entries(data);
                return dataGroupColumn;
            },
            getCreationData:function(dataAttr,rowAttr,columnAttr,dummyDimension,_reportId){
                if(!_dimension)
                    _dimension=client.createDimension(dummyDimension,_reportId);
                var data=_dimension.top(Infinity);
                var dataGroup = d3.nest();
                rowAttr.forEach(function(dataKeyName){
                    dataGroup =dataGroup.key(function(d) {
                        return d[dataKeyName];
                    });
                });
                columnAttr.forEach(function(dataKeyName){
                    dataGroup =dataGroup.key(function(d) {
                        return d[dataKeyName];
                    });
                });
                var ptCustTableData_Arr = [];
                    $.each(dataAttr, function(v,k){
                       rollUpData(ptCustTableData_Arr,dataGroup,k,v);
                    });
                dataGroup=dataGroup.entries(data);
                return ptCustTableData_Arr;
            },
            getDimension:function(){
                return _dimenson;
            },
            filter:function(_dimension,item){
                var filters=item;
                var filter=false;
                if(filters && filters.length>0){
                  filter=true;
                }else{
                    filter=false;
                }
                if ( filters && !(filter==='true')) {
                    _dimension.filter(function (d) {
                        if (filters.indexOf(d) != -1) {
                            return true;
                        } else {
                            return false;
                        }
                    });
                } else{
                    _dimension.filterAll();
                }
            }
        }
    }
    this.PivotUtility=PivotUtility;
})();
module.exports=PivotUtility;