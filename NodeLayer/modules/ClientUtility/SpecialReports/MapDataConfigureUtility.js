/**
 * Created by mitesh.panchal.
 */
var $=require('underscore');
var CrossfilterUtility=require("./../crossfilterUtility");
(function(){
    function config(configuration,_crossfilter,_data,client) {
        var latitude = JSON.parse(configuration.lat_map);
        var longitude = JSON.parse(configuration.long_map);
        //LatLng
        _data.forEach(function (d) {
            d['location'] = parseFloat(d[latitude.columnName]).toFixed(6) + "," + parseFloat(d[longitude.columnName]).toFixed(6);
        });
        var locationObj = JSON.parse(configuration.lat_map);
        locationObj.columnName = 'location';
        var utility=CrossfilterUtility(_crossfilter);
        var  _dimension=utility.createDimension(locationObj,'location');
        //var measureName=configuration.measure_map.columnName;
        var measureObject=configuration.measureObject;
        if(measureObject){
            var measureName=measureObject.columnName;
            var aggregate =  {};
            aggregate[measureName]={
                "key": "sumIndex",
                "value": "Sum",
                "type": "Aggregate"
            };
        }else{
            measureObject={};
        }

        var _initialGroup=utility.createGroup(_dimension,measureObject,aggregate);
        //var _initialGroup=utility.createGroup(_dimension,measureObject,aggregate);
        var  _groups={};
        _groups['location']=_initialGroup;

        var _latLongArray=[];
        _initialGroup.all().forEach(function(latlong){
            _latLongArray.push(latlong.key.split(","));
        });
        return {
            process: function () {
                var aggregate = configuration['aggregateModel'];
            },
            getDimension:function(){
                return _dimension;
            },
            getGroups:function(){
                return _groups;
            },
            // isGroupColorExist:function(){
            //     if(_groupColor)
            //         return true;
            //     return false;
            // },
            // getGroupColor:function(){
            //     return _groupColor;
            // },
            // getGroupDimension:function(){
            //
            //     return _groupDimension;
            // },

            getCenterFromDegrees:function(){
                var data = _latLongArray;
                if (!(data.length > 0)) {
                    return false;
                }
                var num_coords = data.length, X = 0.0, Y = 0.0, Z = 0.0;
                for (i = 0; i < data.length; i++) {
                    var lat = data[i][0] * Math.PI / 180;
                    var lon = data[i][1] * Math.PI / 180;
                    var a = Math.cos(lat) * Math.cos(lon);
                    var b = Math.cos(lat) * Math.sin(lon);
                    var c = Math.sin(lat);
                    X += a;
                    Y += b;
                    Z += c;
                }
                X /= num_coords;
                Y /= num_coords;
                Z /= num_coords;
                var lon = Math.atan2(Y, X);
                var hyp = Math.sqrt(X * X + Y * Y);
                var lat = Math.atan2(Z, hyp);
                var newX = (lat * 180 / Math.PI);
                var newY = (lon * 180 / Math.PI);
                return new Array(newX, newY);
            },
            processTooltipText:function (d, g, v) {
                var val = v[d.columnName];
                if (val == null || isNaN(val) || val == 'NaN' || val == undefined) {
                    val = 0;
                }
                else {
                    val = parseFloat(val);
                }
                if (!g["<"+d.aggregate+"(" + d.reName + ")>"])
                    g["<"+d.aggregate+"("  + d.reName + ")>"] = 0;
                switch (d.aggregate) {
                    case "sum":
                        g["<sum(" + d.reName + ")>"] += val;
                        break;
                    case "count":
                        g["<count(" + d.reName + ")>"]++;
                        break;
                    case "avg":
                        g.avgTooltip.sum +=val;
                        g.avgTooltip.count++;

                        g["<avg(" + d.reName + ")>"]=parseFloat(g.avgTooltip.sum)/parseFloat(g.avgTooltip.count);
                        break;
                    default :
                        //g["<sum(" + d.reName + ")>"] += val;
                }
               // console.log(g);
            },

            getMapGroup:function(dimension, measure, _TooltipArr, tooltipObj){
                // if(_TooltipArr){
                //     _TooltipArr=JSON.parse(_TooltipArr);
                // }
                var processTooltipText=_TooltipArr;
                var columnName=measure.columnName;
                if(dimension)
                var _groupColor=dimension.columnName;
                var process=this.processTooltipText;
                var grp = _dimension.group().reduce(
                    function (g, v) {
                        ++g.count;
                        var val = v[columnName];
                        if (val === null || isNaN(val) || val === undefined) {
                            val = 0;
                        } else {
                            val = Math.round(val);
                        }
                        g.sumIndex += val;

                        if (_TooltipArr) {
                            _TooltipArr.forEach(function (d) {
                                if (d.dataKey === "Measure") {
                                   process(d, g, v);
                                } else {
                                    g["<" + d.reName + ">"] = v[d.columnName];
                                }
                            });
                        }
                        if (_groupColor){
                            var valTemp = (v[_groupColor]);
                            if (!g.groupRepeatCheck[valTemp])
                                g.groupRepeatCheck[valTemp] = val;
                            else
                                g.groupRepeatCheck[valTemp] += val;
                            g.groupColor = g.groupRepeatCheck;
                        }
                        return g;
                    },
                    function (g, v) {
                        var val;
                        val = v[columnName];
                        if (val === null || isNaN(val) || val === undefined) {
                            val = 0;
                        }else {
                            val = Math.round(val);
                        }
                        if (_TooltipArr) {
                          //  _TooltipArr.forEach(function (d) {
                                if (_TooltipArr.dataKey === "Measure") {
                                    this.processTooltipTextDecrement(_TooltipArr, g, v);
                                } else {
                                    g["<" + _TooltipArr.reName + ">"] = v[_TooltipArr.columnName];
                                }
                           // });
                        }
                        if (_groupColor) {
                            var valTemp = (v[_groupColor]);
                            if (!g.groupRepeatCheck[valTemp]) {
                                g.groupRepeatCheck[valTemp] = val;
                            } else {
                                g.groupRepeatCheck[valTemp] = g.groupRepeatCheck[valTemp] - val;
                            }
                            g.groupColor = g.groupRepeatCheck;
                        }
                        g.sumIndex -= parseFloat(val);

                        return g;
                    }, 
                    /* initialize p */
                    function () {
                        return { 
                            sumIndex: 0,
                            groupColor: {},
                            groupRepeatCheck: {},
                            avgTooltip:{
                                sum:0,
                                count:0
                            }
                        };
                    });
                return grp.all();
            },
            applyFilter:function(filter){

            },
            createMapGroup:function(selectedKey,_TooltipArr,index){
                var columnName = selectedKey.columnName;
                var _groupColor;
                if(configuration.multiGroupColor_map){
                    if(configuration.multiGroupColor_map[index] != undefined && configuration.multiGroupColor_map[index] == ''){
                        _groupColor = JSON.parse(configuration.multiGroupColor_map[index]);
                        var groupColorColName = _groupColor.columnName;
                    }else{
                      _groupColor = '';
                    }
                }else{
                    _chart._groupColor = '';
                }

                var grp= _dimension.group().reduce(
                     function (g, v) {
                        ++g.count;
                        var val = v[columnName];
                        if (val === null || isNaN(val) || val === undefined) {
                            val = 0;
                        } else {
                            val = Math.round(val);
                        }
                        g.sumIndex += val;

                        g.toolTipObj = {};

                        if (_TooltipArr) {
                            _TooltipArr.forEach(function (d) {
                                if (d.dataKey === "Measure") {
                                  this.proccessTooltipText(d, g, v);
                                } else {
                                    g["<" + d.reName + ">"] = v[d.reName];
                                }
                            });
                        }
                        if (_groupColor){
                            var valTemp = (v[groupColorColName]);
                            if (!g.groupRepeatCheck[valTemp])
                                g.groupRepeatCheck[valTemp] = val;
                            else
                                g.groupRepeatCheck[valTemp] += val;
                            g.groupColor = g.groupRepeatCheck;
                        }
                        return g;
                    },
                    function (g, v) {

                        var val;
                        val = v[columnName];
                        if (val === null || isNaN(val) || val === undefined) {
                            val = 0;
                        }else {
                            val = Math.round(val);
                        }
                        if (_TooltipArr) {
                            _TooltipArr.forEach(function (d) {
                                if (d.dataKey === "Measure") {
                                   this.proccessTooltipText(d, g, v);
                                } else {
                                    g["<" + d.reName + ">"] = v[d.reName];
                                }
                            });
                        }
                        if (_groupColor) {
                            var valTemp = (v[groupColorColName]);
                            if (!g.groupRepeatCheck[valTemp]) {
                                g.groupRepeatCheck[valTemp] = val;
                            } else {
                                g.groupRepeatCheck[valTemp] = g.groupRepeatCheck[valTemp] - val;
                            }
                            g.groupColor = g.groupRepeatCheck;
                        }
                        g.sumIndex -= parseFloat(val);

                        return g;
                    },
                    /* initialize p */
                    function () {
                        return {
                            sumIndex: 0,
                            groupColor: {},
                            groupRepeatCheck: {}
                        };
                    });
                _groups.push(grp);
                return grp.all();
            }

        }

    }
    this.MapDataConfig=config;
})();
module.exports=MapDataConfig;