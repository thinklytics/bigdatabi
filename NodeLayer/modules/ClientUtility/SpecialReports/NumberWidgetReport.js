var $=require('underscore');
(function(){
    function report(id,dataConfig,chartType){
        var _chartType=chartType;
        var _id=id;
        var _dataConfig=dataConfig;

        return {
            processData:function(){
                try {
                   return _dataConfig.groupValue();
                }catch(e){
                    console.log(e);
                }
            },
            getId:function(){
                return _id;
            },
            getDataGroups:function(){
                  try{
                      return {"numberWidget":_dataConfig.groupValue()};
                  }catch (e){

                  }
            },
            filterAll:function(){}
        }
    }
    this.NumberWidgetReport=report;
})();
module.exports=NumberWidgetReport;