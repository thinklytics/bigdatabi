var $=require('underscore');
var CrossfilterUtility=require("./../crossfilterUtility");
(function(){
    function config(configuration,_crossfilter,client,reportId) {
        var _dimension = null;
        var _groupColor=null;
        var _reportId=reportId;
        var utility=CrossfilterUtility(_crossfilter);
        var dimensionObject=configuration.dimensionObject;

        var measureObject=configuration.measureObject;
        var _groups={};
        var _groupDimension=null;
        var aggregates={};
        aggregates[measureObject['columnName']]= { "key": "sumIndex",
            "value":"Sum",
            "type":"Aggregate"
        };
        var _dateFormat=null;

        if(!client.dimensionExist(dimensionObject)){
            _dimension=utility.createDimension(dimensionObject,_dateFormat);
            client.addDimension(dimensionObject,_dimension,_reportId);
        }
        else{
            _dimension=client.getDimension(dimensionObject,_reportId);
        }
        _groups[measureObject['columnName']]=utility.createGroup(_dimension,measureObject,aggregates);

        return {
            process: function () {
                var aggregate = configuration['aggregateModel'];
            },
            getDimension:function(){
                return _dimension;
            },
            getGroups:function(){
                return _groups;
            },
            isGroupColorExist:function(){
                if(_groupColor)
                    return true;
                return false;
            },
            getGroupColor:function(){
                return _groupColor;
            },
            getGroupDimension:function(){

                return _groupDimension;
            }
        }
    }
    this.MaleFemaleChartConfigUtility=config;
})();
module.exports=MaleFemaleChartConfigUtility;