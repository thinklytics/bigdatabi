var $ = require('underscore');
(function () {
    function report(id, dataConfig, chartType) {
        var _chartType = chartType;
        var _id = id;
        var _dataConfig = dataConfig;
        var _aggregate = _dataConfig.getAggregate();
        var moment = require('moment');
        var _dim = _dataConfig.getDimension();

        var _client = _dataConfig.getClient();
        return {
            getDimension: function () {
                return _dataConfig.getDimension();
            },
            getId: function () {
                return _id;
            },
            applyFilter: function (filter) {
                var filters = filter.filters;
                var filtersTemp={};
                $.each(filters,function(value,key){
                    filtersTemp[key.replace('|',"")]=value;
                });
                filters=filtersTemp;
                var chartKeyOrder = filter.chartKeyOrder;
                var _dimensionList = this.getDimension();
                $.each(_dimensionList,function(value,key){
                    value.filterAll();
                });
                var filterTemp={};
                $.each(filters,function(value,key){
                    if (dataConfig.isDateApplied(chartKeyOrder[key]) && !dataConfig.isDateFormatApplied()) {
                        value.forEach(function (e) {
                            if (!filterTemp[key]) {
                                filterTemp[key] = [];
                            }
                            filterTemp[key].push((moment(e)).toString());
                        });
                        filters[key] = filterTemp[key];
                    }
                });
                var flag=true;
                if($.isEmpty(filters)){
                    flag=false;
                    $.each(_dimensionList,function(value,key){
                        value.filterAll();
                    });
                }else{
                    $.each(filters,function(value,key){
                        if(value.length!=0){
                            if (dataConfig.isDateApplied(chartKeyOrder[key]) && !dataConfig.isDateFormatApplied()) {
                                (_dimensionList[chartKeyOrder[key].reName]).filter(function (d) {
                                    if (value.indexOf(d.toString()) != -1) {
                                        return true;
                                    } else {
                                        return false;
                                    }
                                });
                            }else {
                                (_dimensionList[chartKeyOrder[key].reName]).filter(function (d) {
                                    if (value.indexOf(d.toString()) != -1) {
                                        return true;
                                    } else {
                                        return false;
                                    }
                                });
                            }
                        }else{
                            _dimensionList[chartKeyOrder[key].reName].filterAll();
                        }
                        /*
                         * For notification condition check
                         */
                        flag=true;
                        if(_dimensionList[chartKeyOrder[key].reName].top(Infinity).length){
                            flag=false;
                        }
                    });
                }
                return flag;
            },

            filterAll: function () {
                var _dimension = this.getDimension();
                Object.keys(_dimension).forEach(function (d) {
                    _dimension[d].filter(null);
                })

            },

            getDataGroups: function () {
                return {};
            },

            processDataWithD3: function () {
                var _chart = _dataConfig.getConfiguration();
                var keyOfObj = "";
                var data = [];

                data = _dim[0].top(Infinity);
                var tableHeaders = "";
                var k = 0;
                var ColumnArrayForDataTable = [];
                var keys = [];

                if (_dataConfig.getTableSettings() && _dataConfig.getTableColumnOrder()) {
                    keys = [];
                    var simpleKey = [];
                    $.each(_dataConfig.getTableColumnOrder(), function (value, key) {
                        simpleKey.push(JSON.parse(value).reName);
                        keys.push(JSON.parse(value));
                    });
                    var commonArray = [];
                    var commonArrayNames = [];
                    $.each(_chart._measure, function (value, key) {
                        commonArrayNames.push(key);
                        commonArray.push(value);
                    });

                    $.each(_chart._dimension, function (value, key) {
                        commonArrayNames.push(key);
                        commonArray.push(value);
                    });

                    if (simpleKey.length < commonArrayNames.length) {
                        commonArrayNames.forEach(function (d, i) {
                            if (simpleKey.indexOf(d) === -1) {
                                keys.push(commonArray[i]);
                            }
                        });
                    } else if (simpleKey.length === commonArrayNames.length) {
                        for (var j = commonArrayNames.length - 1; j >= 0; j--) {
                            if (simpleKey.indexOf(commonArrayNames[j]) === -1) {
                                keys.push(commonArray[j]);
                                simpleKey.push(commonArray[j].reName);
                            }
                        }
                        for (var j = simpleKey.length - 1; j >= 0; j--) {
                            if (commonArrayNames.indexOf(simpleKey[j]) === -1) {
                                keys.splice(j, 1);
                            }
                        }
                    } else {
                        var temp = simpleKey;
                        for (var j = simpleKey.length - 1; j >= 0; j--) {
                            if (commonArrayNames.indexOf(simpleKey[j]) === -1) {
                                keys.splice(j, 1);
                                //simpleKey.splice(j,1);
                            }
                        }
                    }
                } else {
                    try {
                        $.each(_chart._dimension, function (value, key) {
                            keys.push(value);
                        });
                    } catch (e) {

                    }
                    try {
                        $.each(_chart._measure, function (value, key) {
                            keys.push(value);
                        });
                    } catch (e) {

                    }
                }
                var _tableSetting = null;
                var columnNames = [];
                var _activeKey = [];
                var initialKey = columnNames[0];
                var fieldsArray = keys;
                var keyIndex = 0;
                var TotalObject = {};

            },

            processData: function () {
                var _chart = _dataConfig.getConfiguration();
                var _data = _dim[Object.keys(_dim)[0]].top(Infinity);
                /*
                 * Regex to get Column
                 */
                var found = {},          // an array to collect the strings that are found
                rxp = /{{([^}]+)}}/g,
                str = _chart.String,
                curMatch;
                var dateFormat={};
                while( curMatch = rxp.exec( str ) ) {
                    var stringVariable=curMatch[1].split(",");
                    found[stringVariable[0]+"@@!"+stringVariable[1]]=stringVariable[1];
                    if(stringVariable[2]!=undefined)
                    dateFormat[stringVariable[0]+","+stringVariable[1]]=stringVariable[2];
                }
                function findMeasureDimension(column) {
                    if(_chart._measure!=undefined){
                        var measureArr=Object.keys(_chart._measure);
                        if(measureArr.indexOf(column)!=-1){
                            return "measure";
                        }
                    }
                    var dimObj=_chart._dimension[column];
                    if(dimObj.columType=="date" || dimObj.columType=="datetime"){
                        return "date";
                    }
                    return "dimension";

                }
                String.prototype.replaceAll = function(a, b) {
                    return this.replace(new RegExp(a.replace(/([.?*+^$[\]\\(){}|-])/ig, "\\$1"), 'ig'), b)
                }
                var newObj={};
                var countDisCal={};
                _data.forEach(function (d) {
                    $.each(found,function (v,k) {
                        k=k.split("@@!")[0];
                        var type=findMeasureDimension(k);
                        if(v=="sum"){
                            if(type=="measure"){
                                if(newObj[k+","+v]==undefined){
                                    newObj[k+","+v]=0;
                                }
                                newObj[k+","+v] += parseFloat(d[k]);
                            }else{
                                if(newObj[k+","+v]==undefined){
                                    newObj[k+","+v]=d[k];
                                }
                                if(newObj[k+","+v]!=d[k])
                                    newObj[k+","+v] = "Multiple Values...";
                            }
                        }else if(v=="count"){
                            if(type=="measure"){
                                if(newObj[k+","+v]==undefined){
                                    newObj[k+","+v]=0;
                                }
                                newObj[k+","+v] += 1;
                            }else{
                                if(newObj[k+","+v]==undefined){
                                    newObj[k+","+v]=0;
                                }
                                newObj[k+","+v] += 1;
                            }
                        }else if(v=="avg"){
                            if(type=="measure"){
                                if(newObj[k+","+v]==undefined){
                                    newObj[k+","+v]={};
                                    newObj[k+","+v]['sum']=0;
                                    newObj[k+","+v]['count']=0;
                                }
                                newObj[k+","+v]['sum'] +=parseFloat(d[k]);
                                ++newObj[k+","+v]['count'];
                                newObj[k+","+v]['avg'] = newObj[k+","+v]['sum']/newObj[k+","+v]['count'];
                            }else{
                                if(newObj[k+","+v]==undefined){
                                    newObj[k+","+v]=d[k];
                                }
                                if(newObj[k+","+v]!=d[k])
                                    newObj[k+","+v] = "Multiple Values...";
                            }
                        }else if(v=="min"){
                            var type=findMeasureDimension(k);
                            if(type=="measure"){
                                if(newObj[k+","+v]==undefined){
                                    newObj[k+","+v]=d[k];
                                }
                                if(newObj[k+","+v]>d[k])
                                newObj[k+","+v]=d[k];
                            }else if(type=="dimension"){
                                if(newObj[k+","+v]==undefined){
                                    newObj[k+","+v]=d[k];
                                }
                                if(newObj[k+","+v]>=d[k])
                                    newObj[k+","+v] =d[k];
                            }else if(type=="date"){
                                if(newObj[k+","+v]==undefined){
                                    newObj[k+","+v]=moment(d[k]);
                                }
                                if(newObj[k+","+v]>moment(d[k]))
                                newObj[k+","+v] = moment(d[k]);
                            }
                        }else if(v=="max"){
                            if(type=="measure"){
                                if(newObj[k+","+v]==undefined){
                                    newObj[k+","+v]=d[k];
                                }
                                if(newObj[k+","+v]<d[k])
                                  newObj[k+","+v]=d[k];
                            }else if(type=="dimension"){
                                if(newObj[k+","+v]==undefined){
                                    newObj[k+","+v]=d[k];
                                }
                                if(newObj[k+","+v]<d[k])
                                newObj[k+","+v] = d[k];
                            }else if(type=="date"){
                                if(newObj[k+","+v]==undefined){
                                    newObj[k+","+v]=moment(d[k]);
                                }
                                if(newObj[k+","+v]<moment(d[k]))
                                    newObj[k+","+v] = moment(d[k]);
                            }
                        }else if(v=="all"){
                             if(type=="dimension"){
                                if(newObj[k+","+v]==undefined){
                                    newObj[k+","+v]=d[k];
                                }
                                if(newObj[k+","+v]!=d[k])
                                    newObj[k+","+v] = "Multiple Values...";
                            }else {
                                 newObj[k+","+v]="Invalid";
                             }
                        }else if(v=="countDistinct"){
                            if(newObj[k+","+v]==undefined){
                                newObj[k+","+v]=0;
                            }
                            if(countDisCal[k+d[k]]==undefined){
                                countDisCal[k+d[k]]=true;
                                newObj[k+","+v] += 1;
                            }
                        }
                    });
                });
                $.each(newObj,function (v,k) {
                    if(v instanceof moment){
                        str = str.replaceAll("{{"+k+","+dateFormat[k]+"}}",v.format(dateFormat[k]));
                    }else if(v instanceof Object ){
                        str = str.replaceAll("{{"+k+"}}",v.avg);
                    }
                    else{
                        str = str.replaceAll("{{"+k+"}}",v);
                    }
                });
                var dataInfo={};
                dataInfo['data']=str;
                return dataInfo;
            }
        }
    }
    this.DatatableReport = report;
})();
module.exports = DatatableReport;