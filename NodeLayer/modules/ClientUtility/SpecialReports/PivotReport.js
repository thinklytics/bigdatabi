var $=require('underscore');
(function(){
    function report(id,dataConfig,chartType){
        var _chartType=chartType;
        var _id=id;
        var _dataConfig=dataConfig;

        return {
            getDimension:function(){
                return _dataConfig.getDimension();
            },
            getGroups:function(){

                return _dataConfig.getGroups();
            },
            getGroupColorDimension:function(){
                return _dataConfig.getGroupDimension();
            },
            getDataGroups:function(){
                var data={};
                $.each(this.getGroups(),function(group,key){
                    data[key]=group.all();
                    data[key].forEach(function(e){
                        if($.isObject(e['value']) && $.isEmpty(e['value']['groupColor'])){
                            e['value']=e['value']['val'];
                        }
                    });
                });
                return data;
            },
            getId:function(){
                return _id;
            },
            applyFilter:function(filter){
                var filters=filter.filters;
                var _dimension=this.getDimension();
                if (!(filter.reset==='true')) {
                    _dimension.filter(function (d) {
                        if (filters.indexOf(d) != -1) {
                            return true;
                        } else {
                            return false;
                        }
                    });
                } else{
                    _dimension.filterAll();
                }
                if(_dataConfig.isGroupColorExist()){
                    var groupFilter=filter.datasetFilters;
                    try{
                        this.getGroupColorDimension().filter(function (d) {
                            if (groupFilter.indexOf(d) != -1) {
                                return true;
                            } else {
                                return false;
                            }
                        });
                    }catch(e){
                        this.getGroupColorDimension().filterAll();
                    }
                }
            },
            filterAll:function(){
                var _dimension=this.getDimension();
                _dimension.filterAll();
                if(_dataConfig.isGroupColorExist()){
                    this.getGroupColorDimension().filterAll();
                }
            }
        }
    }
    this.PivotReport=report;
})();
module.exports=PivotReport;