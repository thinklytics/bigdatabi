var $=require('underscore');
(function(){
    function report(id,dataConfig,chartType){
        var _chartType=chartType;
        var _id=id;
        var _dataConfig=dataConfig;

        return {
            getDimension:function(){
                return _dataConfig.getDimension();
            },
            getGroups:function(){

                return _dataConfig.getGroups();
            },
            getGroupColorDimension:function(){
                return _dataConfig.getGroupDimension();
            },
            getDataGroups:function(){
               var grps=this.getGroups();
               var data={};
               $.each(grps,function(v,k){
                   data[k] =v.all();
               });


               return data;
            },
            getId:function()
            {
                return _id;
            },
            applyFilter:function(filter){

                var filters=filter.filters;
                var _dimension=this.getDimension();
                               if (!(filter.reset==='true')) {
                    _dimension.filter(function (d) {

                        if (filters.indexOf(d.toString()) != -1) {

                            return true;
                        } else {

                            return false;
                        }
                    });
                } else{
                    _dimension.filterAll();
                }
            },
            filterAll:function(){
                var _dimension=this.getDimension();
                _dimension.filter(null);
            }
        }
    }
    this.MaleFemaleReport=report;
})();
module.exports=MaleFemaleReport;