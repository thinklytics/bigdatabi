var $=require('underscore');
var CrossfilterUtility=require("./../crossfilterUtility");
(function(){
    function config(configuration,_crossfilter,client,reportId) {
        var utility=CrossfilterUtility(_crossfilter);
        var measureKey=Object.keys(configuration)[0];
        var _dimension;
        var dimensionObj=Object.values(configuration['dimension']);
        //console.log(client.dimensionExist(dimensionObj,reportId));
        if(!client.dimensionExist(dimensionObj,reportId) || dimensionObj.key=="datetime" || dimensionObj.key=="date"){
            _dimension=utility.createDimension(dimensionObj,"","");
            client.addDimension(dimensionObj,_dimension,reportId);
        }else{
            _dimension=client.getDimension(dimensionObj,reportId);
        }
        var group=utility.numberGroup(configuration[measureKey],client,dimensionObj[0],_dimension);
        return {
            groupValue: function () {
              var d=group;
              return (Math.abs(d.value())<1e-6) ? 0 : d.value();
            }
        }
    }
    this.NumberWidgetDataConfigUtility=config;
})();
module.exports=NumberWidgetDataConfigUtility;