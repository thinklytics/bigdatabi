var $=require('underscore');
var CrossfilterUtility=require("./../crossfilterUtility");
(function(){
    function config(configuration,_crossfilter,data,client,reportId) {
        var _dimension = null;
        var _reportId=reportId;
        var utility=CrossfilterUtility(_crossfilter);
        var _dateFormat=null;
        var _groups={};
        var _groupColor;
        var dimensionObject;
        var aggregates=configuration['aggregateModel'];
        $.each(configuration['checkboxModelDimension'], function (dimensionObj) {
            dimensionObject=dimensionObj;
            if(!client.dimensionExist(dimensionObj,_reportId)){
                _dimension=utility.createDimension(dimensionObj,_dateFormat);
                client.addDimension(dimensionObj,_dimension,_reportId);
            }
            else{
                _dimension=client.getDimension(dimensionObj,_reportId);
            }
        });
        var measure = {};
        measure['columType'] = 'int';
        measure['columnName'] = 'dummyMeasureForWordCloud';
        measure['dataKey'] = 'Measure';
        measure['key'] = 'int';
        measure['reName'] = 'dummyMeasureForWordCloud';
        measure['tableName'] = '1111';
        measure['type'] = 'defined';
        measure['value'] = 1;
        data.forEach(function(d){
            d[measure['columnName']]=1;
        });
        aggregates[measure['columnName']]=aggregates[Object.keys(aggregates)[0]];
        _groups[measure['columnName']]=utility.createGroup(_dimension,measure,aggregates);

        return {
            getconfigObj:function() {
                return configuration;
            },
            isDateApplied:function(){
                if(dimensionObject.key=="datetime" || dimensionObject.key=="date"){
                    return true;
                }
                return false;
            },
            isDateFormatApplied:function(){
                if(configuration.dataFormate){
                    var format=configuration.dataFormate;
                    if(format.xAxis){
                        return true;
                    }
                }
                return false;
            },
            process: function () {
                var aggregate = configuration['aggregateModel'];
            },
            getDimension:function(){
                return _dimension;
            },
            getGroups:function(){
                return _groups;
            },
            isGroupColorExist:function(){
                if(_groupColor)
                    return true;
                return false;
            },
            getGroupColor:function(){
                return _groupColor;
            },
            getGroupDimension:function(){

                return _groupDimension;
            }
        }
    }
    this.WordCloudDataConfigUtility=config;
})();
module.exports=WordCloudDataConfigUtility;