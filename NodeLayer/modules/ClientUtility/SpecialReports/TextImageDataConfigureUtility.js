var $=require('underscore');
var CrossfilterUtility=require("./../crossfilterUtility");
(function(){
    function config(configuration,_crossfilter,client,_reportId) {
        var _dimensionArray = [];

        var _reportId=_reportId;
        var utility=CrossfilterUtility(_crossfilter);

        // var dimensionKey=configuration.keysOrder[0];
        // var dimensionObj=configuration._dimension[dimensionKey];
        var _dateFormat=null;
        var _dimensionList={};
        var _dimensionObjList={};
        $.each(configuration._dimension,function(dimensionObj,key){
            if(!client.datatableDimensionExist(dimensionObj,_reportId)){
                var _dimension=utility.createDimension(dimensionObj,_dateFormat);
                _dimensionList[key]=_dimension;
                _dimensionObjList[key]=dimensionObj;
                client.addDatatableDimension(dimensionObj,_dimension);
            }
            else{
                _dimensionList[key]=client.getDatatableDimension(dimensionObj);
                _dimensionObjList[key]=dimensionObj;
            }
        });
        return {
            getAggregate: function () {
                var aggregate = configuration['_aggregate'];
                return aggregate;
            },
            isDateApplied:function(dimObj){

                if(dimObj.key=="datetime" || dimObj.key=="date"){
                    return true;
                }
                return false;
            },
            getClient:function(){
                return client;
            },
            isDateFormatApplied:function(){

                if(configuration.dataFormate){
                    var format=configuration.dataFormate;

                    if(format.xAxis){



                        return true;


                    }
                }
                return false;
            },
            getDimension:function(){
                return _dimensionList;
            },
            getTableSettings:function(){
                return configuration._tableSetting;
            },
            getTableColumnOrder:function(){
                if(configuration._tableSetting){
                    return configuration._tableSetting.tableColumnOrder;
                }
                return false;
            },
            getConfiguration:function(){
                return configuration;
            }

        }
    }
    this.DatatableDataConfig=config;
})();
module.exports=DatatableDataConfig;