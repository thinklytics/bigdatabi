var $=require('underscore');
var CrossfilterUtility=require("./crossfilterUtility");
(function(){
    function config(configuration,_crossfilter,client,reportId) {
        var _dimension = null;
        var _groups={};
        var _groupColor=null;
        var _groupDimension=null;
        var utility=CrossfilterUtility(_crossfilter);
        var _dateFormat=null;
        var _reportId=reportId;
        var _customTooltip=false;
        var _customDateFormat=null;
        var configuration=configuration;
        if(configuration.dataFormat){
            var format=configuration.dataFormat;
            if(format.groupColor){
                _groupColor=JSON.parse(configuration.dataFormat.groupColor);
                _groupDimension=utility.createDimension(_groupColor);
            }
            if(format.tooltip){
                _customTooltip={};
                _customTooltip["text"]=format.tooltip;
                _customTooltip["datakeys"]=format.tooltipSelector;
            }
            if(format.xaxis && format.xaxis.format){
                _dateFormat=format.xaxis.format;
                if(format.xaxis && format.xaxis.customFormat)
                  _customDateFormat=format.xaxis.customFormat;
            }
        }
        var aggregates=configuration['aggregateModel'];
        var dimensionObject=null;
        $.each(configuration['checkboxModelDimension'], function (dimensionObj,dimensionKey) {
            dimensionObject=dimensionObj;
            if(!client.dimensionExist(dimensionObj,_reportId) || dimensionObj.key=="datetime" || dimensionObj.key=="date"){
                 _dimension=utility.createDimension(dimensionObj,_dateFormat,_customDateFormat);
                 client.addDimension(dimensionObj,_dimension,_reportId);
            }else{
                 _dimension=client.getDimension(dimensionObj,_reportId);
            }
        });
        
        $.each(configuration['checkboxModelMeasure'],function (measureObj,measureKey) {
            _groups[measureKey]=utility.createGroup(_dimension,measureObj,aggregates,_groupColor,_dateFormat,_customTooltip,client,dimensionObject);
        });

        return{
            getconfigObj:function() {
                return configuration;
            },
            isDateApplied:function(){
                if(dimensionObject.key=="datetime" || dimensionObject.key=="date"){
                    return true;
                }
                return false;
            },
            isDateFormatApplied:function(){
                if(configuration.dataFormat){
                    var format=configuration.dataFormat;
                    if(format.xAxis){
                        return true;
                    }
                }
                return false;
            },
            process: function () {
                var aggregate = configuration['aggregateModel'];
            },
            getDimension:function(){
                return _dimension;
            },
            getGroups:function(){
                return _groups;
            },
            isGroupColorExist:function(){
                if(_groupColor)
                    return true;
                return false;
            },
            getGroupColor:function(){
                return _groupColor;
            },
            isCustomTooltipExist:function(){
                 return _customTooltip;
            },
            getGroupDimension:function(){
                return _groupDimension;
            }
        }
    }
    this.DataConfig=config;
})();
module.exports=DataConfig;