var crossfilter = require('crossfilter');
var Enumerable = require('linq');
var _ = require('underscore');
var moment = require('moment');
var crossfilterUtility = require('./../ClientUtility/crossfilterUtility');
(function () {
    function Tooltip(data) {
        var _data=data;
        return {
            tooltipRender:function (title,tooltipFormat,tooltipObj) {
                //var data=0;
                //var tooltipObj=JSON.parse(tooltipFormat).tooltipSelector;
                /*_data.forEach(function(d){
                    if(d['campaign_name']==title){
                        data=parseFloat(data)+parseFloat(d[tooltipObj[0].columnName]);
                    }
                });
                return tooltipObj[0].columnName+" : "+data;*/
                if(tooltipFormat && JSON.parse(tooltipFormat).tooltipSelector && JSON.parse(tooltipFormat).tooltipSelector.length){
                    JSON.parse(tooltipFormat).tooltipSelector.forEach(function(p){
                        tooltipObj=tooltipObj.replace("<" + p.aggregate + "(" + p.columnName + ")" + ">", "10");
                    });
                }
                return tooltipObj;
            }
        }
    }
    this.Tooltip = Tooltip;
})();
module.exports = Tooltip;