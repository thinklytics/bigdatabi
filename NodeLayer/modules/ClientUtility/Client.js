var crossfilter = require('crossfilter2');
var CalculateOnData = require('./../DataProcessor/CalculateOnData');
var Enumerable = require('linq');
var _ = require('underscore');
var moment = require('moment');
var crossfilterUtility = require('./../ClientUtility/crossfilterUtility');
var DataStore=require('./../DataProcessor/DataStore');
(function () {
    function Client(clientId, metadataId, sessionId, data, tableColumn,company_id) {
        var _id = clientId;
        var _metadataId = metadataId;
        var _sessionId = sessionId;
        var _companyId=company_id;
        var _tableColumn=tableColumn;
        var _department="";
        var _realtime=false;
        //var _data = data;
        var _crossfilter = crossfilter(data);
        var _crossfilterUtility = crossfilterUtility(_crossfilter);
        var _reports = {};
        var _externalFilterDimensions = {};
        var _tableColumn = JSON.parse(JSON.parse(tableColumn));
        var _calculateOnData = new CalculateOnData(data);
        var _dimensionFactory = {};
        var _pivotFactory={};
        var _pivotDimensionList={};
        var _datatableDimensionList={};
        var _filterDataDumyDimension=null;
        var _cascadeFilters={};
        var _dataObj={};
        var _calArray=null;
        var _sharedviewId;
        var _dimensionList={};
        var _userType=false;
        var _roleLevel=false;
        return {
            //All datatable handling code..........................................................
            addDatatableDimension:function(_dimensionAttr,_dimension){
                _datatableDimensionList[_dimensionAttr.columnName]=_dimension;
            },
            filterAllDatatableDimension:function(){
                _.each(_datatableDimensionList,function(val,key){
                    val.filterAll();
                });
            },
            getDatatableDimensionList:function(){
                return _datatableDimensionList;
            },
            getPivotDimensionList:function(){
                return _dimensionList;
            },
            id: function (clientId) {
                _id = clientId;
                return this;
            },
            getTableColumn:function(){
                return _tableColumn;
            },
            getCrossfilter:function(){
                return _crossfilter;
            },
            setRoleLevel:function (roleLevel) {
                _roleLevel=roleLevel;
            },
            setCalArray:function(calArr){
                _calArray=calArr;
            },
            getCalArray:function(){
                return _calArray;
            },
            addNewColumn:function(newColumn){
                newColumn.forEach(function(d){
                    _tableColumn[d.columnName]=d;
                });
            },
            getId: function (clientId) {
                return _id;
            },
            getDataColumn: function () {
                return _dataColumn;
            },
            setDataObj:function(dataObj){
                _dataObj=dataObj;
            },
            setRealtime:function(flag){
                _realtime=flag;
            },
            getRealtime:function(){
                return _realtime;
            },
            getDataObj:function () {
                return _dataObj;
            },
            reconstructAlldata:function () {
                _data=[];
                _.each(_dataObj,function (v,k) {
                    _data.push(v);
                });
            },
            setSharedviewId:function (sharedviewId) {
                _sharedviewId=sharedviewId;
            },
            getSharedviewId:function () {
                return _sharedviewId;
            },
            metadataId: function (metadataId) {
                _metadataId = metadataId;
                return this;
            },
            getMetadataId: function () {
                return _metadataId;
            },
            getCompanyId:function () {
              return _companyId;
            },
            getMetadataKey:function(){
                return _id+":"+_metadataId+":"+_companyId+":"+_sessionId;
            },
            filterDimension:function(reportId){
                _.each(_dimensionFactory[reportId],function(val){
                    val.filterAll();
                });
            },
            registerPivot:function(PivotUtility,reportId){
                _pivotFactory[reportId]=PivotUtility;
            },
            applyPivotFilter:function(){
                _.each(_pivotFactory,function(val,key){
                    //.log("pivot apply", val.getDimension(),key);
                    val.getDimension().filterAll();
                });

            },
            removeFilters:function(){
                _.each(_externalFilterDimensions,function(dim,key){dim.filterAll()});
                this.filterAllPivotDimension();
                this.filterAllDatatableDimension();
                this.getReports(function(report,reportId){
                    try{
                        report.filterAll();
                    }catch(e){
                        console.log(e);
                    }
                });
            },
            sessionId: function (sessionId) {
                _sessionId = sessionId;
                return this;
            },
            getSessionId: function () {
                return _sessionId;
            },
            setDepartment:function (department) {
                _department=department;
            },
            getDepartment:function () {
                return _department;
            },
            getData: function() {
                if(_userType){
                    return Object.values(global.dataMetaWise[_metadataId+":"+_companyId+":"+_id]['data']);
                }else if(_realtime){
                    var tempKey=_metadataId+":"+_companyId;
                    if(_userType && _roleLevel){
                        tempKey=tempKey+":"+_id;
                    }else if(_department!=undefined && _department!='' && _roleLevel){
                        tempKey=tempKey+":"+_department;
                    }
                    return Object.values(global.dataMetaWise[tempKey+"-realtime"]['data']);
                }else if(_department!=undefined && _department!='' && _roleLevel){
                    return Object.values(global.dataMetaWise[_metadataId+":"+_companyId+":"+_department]['data']);
                }
                return Object.values(global.dataMetaWise[_metadataId+":"+_companyId]['data']);
            },
            setUsertype:function (type) {
                _userType=type;
            },
            getUsertype:function () {
                return _userType;
            },
            getUniqueDataOfColumn: function (columnObject) {
                var columnDataList = [];
                Enumerable.from(this.getData())
                    .distinct(function (x) {
                        return x[columnObject.columnName];
                    })
                    .select(function (x) {
                        return x[columnObject.columnName];
                    })
                    .toArray()
                    .forEach(function (e) {
                        columnDataList.push(e);
                    });
                return columnDataList;
            },
            getUniqueDataOfColumnAfterCascade: function (columnObject,totalLength,filterIndex) {
                var columnDataList = [];
                var filterData=[];
                var numOfFilter = parseInt(parseInt(filterIndex)+1);
                this.getData().forEach(function(d){
                    var flag=true;
                    var i=0;
                    _.each(_cascadeFilters, function (val, key){
                        i++;
                        if((i<=numOfFilter ) && numOfFilter!=totalLength ){
                            if(key){
                                var item=val['item'];
                                var filter=val['filter'];
                                if (filter.value == "int" || filter.value == "bigint" || filter.value == "float" || filter.value == "decimal" || filter.value == "double") {
                                    var f = item;
                                    if (isNaN(d[key])) {
                                        flag=false;
                                    }
                                    if ((parseFloat(f[0]) <= parseFloat(d[key])) && (parseFloat(f[1]) >= parseFloat(d[key]))) {

                                    } else {
                                        flag=false;
                                    }
                                } else if ((filter.value).toLowerCase() == "date") {
                                    var startDate = moment(item['start'], "YYYY-MM-DD");
                                    var endDate = moment(item['end'], "YYYY-MM-DD");
                                    var dateInData = moment(d[key]);
                                    if (startDate <= dateInData && endDate >= dateInData) {

                                    } else {
                                        flag=false;
                                    }
                                }else if((filter.value).toLowerCase() == "datetime"){
                                    var startDate = moment(item['start'], "YYYY-MM-DD HH:mm:ss");
                                    var endDate = moment(item['end'], "YYYY-MM-DD HH:mm:ss");
                                    var dateInData = moment(d[key]);
                                    if (startDate <= dateInData && endDate >= dateInData) {

                                    } else {
                                        flag=false;
                                    }
                                } else {
                                    if(item){
                                        if(d[key]==null || item.indexOf(d[key].toString())== -1){
                                            flag=false;
                                        }
                                    }
                                }
                            }
                        }
                    });
                    if(flag){
                        filterData.push(d);
                    }
                });
                if(columnObject){
                    Enumerable.from(filterData)
                        .distinct(function (x) {
                            return x[columnObject.key];
                        })
                        .select(function (x) {
                            return x[columnObject.key];
                        })
                        .toArray()
                        .forEach(function (e) {
                            columnDataList.push(e);
                        });
                }
                return columnDataList;
            },

            getUniqueDataOfColumnEditList: function (columnObject,selectedGroupKey) {
                var columnDataList = [];
                Enumerable.from(this.getData())
                    .distinct(function (x) {
                        return x[columnObject.columnName];
                    })
                    .select(function (x) {
                        return x[columnObject.columnName];
                    })
                    .toArray()
                    .forEach(function (e) {
                        if(selectedGroupKey[e]==undefined)
                            columnDataList.push(e);
                    });
                return columnDataList;
            },
            addDimension: function (_dimensionAttr, _dimension,_reportId) {
                var columnName = _dimensionAttr.columnName;
                if(!_dimensionFactory[columnName])
                    _dimensionFactory[columnName] ={};
                _dimensionFactory[columnName][_reportId]= _dimension;
            },
            addPivotDimension:function(_dimension,dimObj){
                _dimensionList[dimObj.columnName]=_dimension;
            },
            dimensionExist: function (_dimensionAttr,_reportId){
                var columnName = _dimensionAttr.columnName;
                if(_dimensionFactory[columnName] && _dimensionFactory[columnName][_reportId]){
                    return true;
                } else {
                    return false;
                }
            },
            datatableDimensionExist: function (_dimensionAttr){
                var columnName = _dimensionAttr.columnName;
                if (_datatableDimensionList[columnName]) {
                    return true;
                } else {
                    return false;
                }
            },
            filterAllPivotDimension:function(){
                _.each(_dimensionList,function(val,key){
                    val.filterAll();
                });
            },
            createDimension: function (_dimensionAttr,_reportId) {
                var columnName = _dimensionAttr.columnName;
                if (!this.dimensionExist(_dimensionAttr,_reportId)) {
                    var _dimension=_crossfilterUtility.createDimension(_dimensionAttr);
                    this.addDimension(_dimensionAttr,_dimension,_reportId);
                    return _dimensionFactory[columnName][_reportId];
                } else {
                    return _dimensionFactory[columnName][_reportId];
                }
            },
            getDatatableDimension:function(_dimensionAttr){
                var columnName = _dimensionAttr.columnName;
                return _datatableDimensionList[columnName];
            },
            getDimension: function (_dimensionAttr,reportId) {
                var columnName = _dimensionAttr.columnName;
                return _dimensionFactory[columnName][reportId];
            },
            removeDimension: function (_dimensionAttr) {
                var columnName;
                if (_dimensionAttr instanceof Object) {
                    columnName = _dimensionAttr.columnName;
                } else {
                    columnName = _dimensionAttr;
                }
                if (this.dimensionExist(_dimensionAttr)) {
                    delete _dimensionFactory[columnName];
                }
            },

            addReport: function (report) {
                _reports[report.getId()] = report;
            },
            getReports: function (callback) {
                _.each(_reports, function (report, reportKey) {
                    callback(report, reportKey);
                });
            },

            getReportsObject: function () {
                return _reports;
            },

            getReportById: function (id) {
                return _reports[id];

            },

            deleteReport: function (id) {
                delete _reports[id];
            },
            addCascadeFilter:function(item,dimensionObj){
                _cascadeFilters[dimensionObj.key]={item:item,filter:dimensionObj};
            },
            cascadeReinitialize:function (dimensionObj) {
                _cascadeFilters={};
            },
            addExternalFilterDimension: function (dimensionObject) {
                if (_externalFilterDimensions[dimensionObject.key] == undefined)
                    _externalFilterDimensions[dimensionObject.key] = _crossfilterUtility.createDimension(dimensionObject);
                return _externalFilterDimensions[dimensionObject.key];
            },
            applyFilter: function (item, filter) {
                if (item == '' || !item) {
                    _externalFilterDimensions[filter.key].filterAll();
                } else {
                    if (filter.value == "int" || filter.value == "bigint" || filter.value == "float" || filter.value == "decimal" || filter.value == "double") {
                        var f = item;
                        _externalFilterDimensions[filter.key].filter(function (d) {
                            if (!isNaN(d)) {
                            } else {
                                return false;
                            }
                            if ((parseFloat(f[0]) <= parseFloat(d)) && (parseFloat(f[1]) >= parseFloat(d))) {
                                return true;
                            } else {
                                return false;
                            }
                        });
                    } else if ((filter.value).toLowerCase() == "date") {
                        var startDate = moment(item['start'], "YYYY-MM-DD");
                        var endDate = moment(item['end'], "YYYY-MM-DD");
                        _externalFilterDimensions[filter.key].filter(function (d) {
                            var dateInData = moment(d);

                            if (startDate <= dateInData && endDate >= dateInData) {
                                return true;
                            } else {
                                return false;
                            }
                        });
                    }else if((filter.value).toLowerCase() == "datetime"){
                        var startDate = moment(item['start'], "YYYY-MM-DD HH:mm:ss");
                        var endDate = moment(item['end'], "YYYY-MM-DD HH:mm:ss");
                        _externalFilterDimensions[filter.key].filter(function (d) {
                            var dateInData = moment(d);
                            if (startDate <= dateInData && endDate >= dateInData) {
                                return true;
                            } else {
                                return false;
                            }
                        });
                    } else {
                        _externalFilterDimensions[filter.key].filter(function (d) {
                            return item.indexOf(d.toString()) > -1;
                        });
                    }
                }
            },
            getFilterData:function(){
                if(_filterDataDumyDimension==null){
                    _filterDataDumyDimension=_crossfilter.dimension(function (d) {
                        return 'abc';
                        // return d[1];
                    });
                }
                return _filterDataDumyDimension.top(Infinity);
            },
            applyCalculation: function (column,activeKey,filteredData,client) {
                /*_.each(_dimensionFactory[reportId],function(val){
                    val.filterAll();
                });*/
                _calculateOnData.processDataExpression(column.formula, column,activeKey,filteredData,client);
            },
            timeLineFilter: function (timelineObj) {
                var objectKey = {};
                if (!_.isEmpty(timelineObj)) {
                    _.each(timelineObj, function (key, value) {
                        objectKey['key'] = key;
                        objectKey['value'] = value;
                    });
                }
                if (this.getData()[0]['periodType']) {
                    this.getData().forEach(function (d, i) {
                        delete d['periodType'];
                    });
                }
                var filteredData = this.getData().filter(function (row) {
                    var value = moment(row[objectKey['key']], "YYYY-MM-DD");
                    var flag = false;
                    timelineObj.forEach(function (d, i) {
                        var periodStart = moment(d.start, "YYYY-MM-DD");
                        var periodEnd = moment(d.end, "YYYY-MM-DD");
                        if (periodStart <= value && periodEnd >= value) {
                            if (row['periodType'])
                                row['periodType'] += 'period' + i;
                            else
                                row['periodType'] = 'period' + i;
                            flag = true;
                        }
                    });
                    return flag;
                });
            },
            changeCossfilterData:function () {
                var calculateData=CalculateOnData(this.getData());
                if(_calArray){
                    _calArray.forEach(function(e,index){
                        var columnName=e.columnName;
                        var formula=e.formula;
                        //formula=reduceMultilevelFormula(formula,_client);

                        calculateData.processDataExpression(formula,e,'','',this);
                    });

                }
                _crossfilter.remove();
                _crossfilter.add(this.getData());
            },
            preProcessToken(expr) {
                var expression = expr.replace(/isNull[(]/g,
                    "funcDef.isEmpty(")
                    .replace(/isNotNull[(]/g,
                        "funcDef.isNotEmpty(")
                    .replace(/floor[(]/g,
                        "Math.floor(")
                    .replace(/ceil[(]/g,
                        "ceil(")
                    .replace(/decimalToInt[(]/g,
                        "parseInt(")
                    .replace(/concat[(]/g,
                        "concatVar.concat(")
                    .replace(/strlen[(]/g,
                        "funcDef.strlen(")
                    .replace(/dateDiff[(]/g,
                        "funcDef.dateDiff(")
                    .replace(/toLower[(]/g,
                        "funcDef.toLower(")
                    .replace(/toString[(]/g,
                        "funcDef.toString(")
                    .replace(/trim[(]/g,
                        "funcDef.trim(")
                    .replace(/replace[(]/g,
                        "funcDef.replace(")
                    .replace(/round[(]/g,
                        "funcDef.round(")
                    .replace(/parseDate[(]/g,
                        "funcDef.parseDate(")
                    .replace(/formatDate[(]/g,
                        "funcDef.formatDate(")
                    .replace(/toUpper[(]/g,
                        "funcDef.toUpper(")
                    .replace(/DateAdd[(]/g,
                        "funcDef.DateAdd(")
                    .replace(/return/g, "")
                    .replace(/currDate[(]/g, "funcDef.currentDate(")
                    .replace(/\[/g, "funcDef.checkValue(d['")
                    .replace(/dateTimeDiff[(]/g, "funcDef.dateTimeDiff(")
                    .replace(/\]/g, "'])");
                return expression.trim();
            }
        }
    }

    this.Client = Client;
})();
module.exports = Client;