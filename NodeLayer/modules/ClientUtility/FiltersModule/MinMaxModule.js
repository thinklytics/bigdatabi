// ===============================================================================================================
//
//                                             Min Max MODULE
//
// ===============================================================================================================

var $ = require('underscore');
var TooltipModule=require('../../DataProcessor/TooltipModule');
var HelperModule=require('./HelperModule');
var SumModule = (function () {
    var module = {};

    module.processMinMax=function(dimension, measureObj, client, dimensionObj, groupColor, dateFormat, customTooltip){
        if(!groupColor && !customTooltip){
            console.log('1111111111')
            return  module.minMaxWithoutGroupAndTooltip(dimension, measureObj, groupColor, dateFormat, customTooltip, client, dimensionObj);
        }else if(groupColor && !customTooltip){
            console.log('2222222222222')
            return module.minMaxWithGroupNoTooltip(dimension, measureObj, groupColor, dateFormat, customTooltip, client, dimensionObj);
        }else if(!groupColor && customTooltip){
            console.log('333333333333')
            return module.minMaxWithTooltipNoGroup(dimension, measureObj, groupColor, dateFormat, customTooltip, client, dimensionObj);
        }else{
            console.log('444444444444')
            return module.minMaxWithGroupTooltip(dimension, measureObj, groupColor, dateFormat, customTooltip, client, dimensionObj);
        }
    };



    module.minMaxWithoutGroupAndTooltip=function(dimension, measureObj, groupColor, dateFormat, customTooltip, client, dimensionObj){
        var group = {};
        var expression=measureObj.formula;
        group['all']=function () {
            var activeKey=dimensionObj.columnName;
            var _data = dimension.top(Infinity);
            if (activeKey) {
                if(Array.isArray(activeKey)){
                    activeKey=activeKey[0];
                }
                const regex = /(min|max)\(\[([a-z|A-Z|0-9|_|' '|'('|')']*)\]\)?/g;
                var str = expression;
                var flagGroup = false;
                var tableColumn = client.getTableColumn();
                var minGroup={};
                var maxGroup={};
                var strGrp=[];
                var i=0;
                while ((m = regex.exec(expression)) !== null) {
                    if (!(tableColumn[m[2]].columType == "date" || tableColumn[m[2]].columType == "datetime")) {
                        if (!minGroup[m[2]]) {
                            minGroup[m[2]] = {};
                        }
                        if (!maxGroup[m[2]]) {
                            maxGroup[m[2]] = {};
                        }
                        _data.forEach(function (d, index) {
                            if(d[m[2]]){
                                if (!maxGroup[m[2]][d[activeKey]]) {
                                    maxGroup[m[2]][d[activeKey]] = parseInt(d[m[2]]);
                                }
                                if (!minGroup[m[2]][d[activeKey]]) {
                                    minGroup[m[2]][d[activeKey]] = parseInt(d[m[2]]);
                                }
                                if (parseInt(d[m[2]]) > maxGroup[m[2]][d[activeKey]]) {
                                    maxGroup[m[2]][d[activeKey]] = d[m[2]];
                                }
                                else if (parseInt(d[m[2]]) < minGroup[m[2]][d[activeKey]]) {
                                    minGroup[m[2]][d[activeKey]] = d[m[2]];
                                }
                            }
                        });
                        if (m[1] == 'min') {
                            str = str.replace(m[0], "minMaxGrp_"+i);
                            strGrp.push("minGroup['" + m[2] + "'][d[activeKey]]");
                        }
                        if (m[1] == 'max') {
                            str = str.replace(m[0], "minMaxGrp_"+i);
                            strGrp.push("maxGroup['" + m[2] + "'][d[activeKey]]");
                        }
                    } else if (tableColumn[m[2]].columType == "date" || tableColumn[m[2]].columType == "datetime") {
                        if (!minGroup[m[2]]) {
                            minGroup[m[2]] = {};
                        }
                        if (!maxGroup[m[2]]) {
                            maxGroup[m[2]] = {};
                        }
                        _data.forEach(function (d, index) {
                            if(d[m[2]]){
                                if (!maxGroup[m[2]][d[activeKey]]) {
                                    maxGroup[m[2]][d[activeKey]] = moment(d[m[2]]);
                                }
                                if (!minGroup[m[2]][d[activeKey]]) {
                                    minGroup[m[2]][d[activeKey]] = moment(d[m[2]]);
                                }
                                if (moment(d[m[2]]) > maxGroup[m[2]][d[activeKey]]) {
                                    maxGroup[m[2]][d[activeKey]] = moment(d[m[2]]);
                                }
                                else if (moment(d[m[2]]) < minGroup[m[2]][d[activeKey]]) {
                                    minGroup[m[2]][d[activeKey]] = moment(d[m[2]]);
                                }
                            }
                        });
                        if (m[1] == 'min') {
                            str = str.replace(m[0], "minMaxGrp_"+i);
                            strGrp.push("minGroup['" + m[2] + "'][d[activeKey]]");
                        }
                        if (m[1] == 'max') {
                            str = str.replace(m[0], "minMaxGrp_"+i);
                            strGrp.push("maxGroup['" + m[2] + "'][d[activeKey]]");
                        }
                    }
                    if (m.index === regex.lastIndex) {
                        regex.lastIndex++;
                    }
                    i++;
                }
                str=client.preProcessToken(str);
                strGrp.forEach(function (d,index) {
                    str=str.replace("minMaxGrp_"+index,d);
                });
                flagGroup = true;
                var isValued = {};
                newData=[];
                function isValidDate(date) {
                    return date && Object.prototype.toString.call(date) === "[object Date]" && !isNaN(date);
                }
                _data.forEach(function (d) {
                    if(!isValued[d[activeKey]]){
                        var dataStr=eval(str);
                        try{
                            if(dataStr instanceof moment){
                                dataStr=moment(dataStr).format('YYYY-MM-DD HH:MM:SS');
                            }
                        }catch (e){

                        }
                        newData.push({key: d[activeKey], value: parseFloat(dataStr)});
                        isValued[d[activeKey]]=true;
                    }
                });
                newData = newData.sort(function (a, b) {
                    return a.key === b.key ? 0 : a.key < b.key ? -1 : 1;
                });
                return newData;
            }
            return [];
        }
        return group;
    };

    module.minMaxWithGroupNoTooltip=function(dimension, measureObj, groupColor, dateFormat, customTooltip, client, dimensionObj){
        var group = {};
        var expression=measureObj.formula;
        var groupColorName = groupColor.columnName;
        var type, col;
        group['all']=function () {
            var activeKey=dimensionObj.columnName;
            var _data = dimension.top(Infinity);
            if (activeKey) {
                if(Array.isArray(activeKey)){
                    activeKey=activeKey[0];
                }
                const regex = /(min|max)\(\[([a-z|A-Z|0-9|_|' '|'('|')']*)\]\)?/g;
                var str = expression;
                var flagGroup = false;
                var tableColumn = client.getTableColumn();
                var minGroup={};
                var maxGroup={};
                var strGrp=[];
                var i=0;
                while ((m = regex.exec(expression)) !== null) {
                    type = m[1];
                    col = m[2];
                    if (!(tableColumn[m[2]].columType == "date" || tableColumn[m[2]].columType == "datetime")) {
                        if (!minGroup[m[2]]) {
                            minGroup[m[2]] = {};
                        }
                        if (!maxGroup[m[2]]) {
                            maxGroup[m[2]] = {};
                        }
                        _data.forEach(function (d, index) {
                            if(d[m[2]]){
                                if (!maxGroup[m[2]][d[activeKey]+d[groupColorName]]) {
                                    maxGroup[m[2]][d[activeKey]+d[groupColorName]] = parseInt(d[m[2]]);
                                }
                                if (!minGroup[m[2]][d[activeKey]+d[groupColorName]]) {
                                    minGroup[m[2]][d[activeKey]+d[groupColorName]] = parseInt(d[m[2]]);
                                }
                                if (parseInt(d[m[2]]) > maxGroup[m[2]][d[activeKey]+d[groupColorName]]) {
                                    maxGroup[m[2]][d[activeKey]+d[groupColorName]] = d[m[2]];
                                }
                                else if (parseInt(d[m[2]]) < minGroup[m[2]][d[activeKey]+d[groupColorName]]) {
                                    minGroup[m[2]][d[activeKey]+d[groupColorName]] = d[m[2]];
                                }
                            }
                        });
                        if (m[1] == 'min') {
                            str = str.replace(m[0], "minMaxGrp_"+i);
                            strGrp.push("minGroup['" + m[2] + "'][d[activeKey]+d[groupColorName]]");
                        }
                        if (m[1] == 'max') {
                            str = str.replace(m[0], "minMaxGrp_"+i);
                            strGrp.push("maxGroup['" + m[2] + "'][d[activeKey]+d[groupColorName]]");
                        }
                    } else if (tableColumn[m[2]].columType == "date" || tableColumn[m[2]].columType == "datetime") {
                        if (!minGroup[m[2]]) {
                            minGroup[m[2]] = {};
                        }
                        if (!maxGroup[m[2]]) {
                            maxGroup[m[2]] = {};
                        }
                        _data.forEach(function (d, index) {
                            if(d[m[2]]){
                                if (!maxGroup[m[2]][d[activeKey]+d[groupColorName]]) {
                                    maxGroup[m[2]][d[activeKey]+d[groupColorName]] = moment(d[m[2]]);
                                }
                                if (!minGroup[m[2]][d[activeKey]+d[groupColorName]]) {
                                    minGroup[m[2]][d[activeKey]+d[groupColorName]] = moment(d[m[2]]);
                                }
                                if (moment(d[m[2]]) > maxGroup[m[2]][d[activeKey]+d[groupColorName]]) {
                                    maxGroup[m[2]][d[activeKey]+d[groupColorName]] = moment(d[m[2]]);
                                }
                                else if (moment(d[m[2]]) < minGroup[m[2]][d[activeKey]+d[groupColorName]]) {
                                    minGroup[m[2]][d[activeKey]+d[groupColorName]] = moment(d[m[2]]);
                                }
                            }
                        });
                        if (m[1] == 'min') {
                            str = str.replace(m[0], "minMaxGrp_"+i);
                            strGrp.push("minGroup['" + m[2] + "'][d[activeKey]]");
                        }
                        if (m[1] == 'max') {
                            str = str.replace(m[0], "minMaxGrp_"+i);
                            strGrp.push("maxGroup['" + m[2] + "'][d[activeKey]]");
                        }
                    }
                    if (m.index === regex.lastIndex) {
                        regex.lastIndex++;
                    }
                    i++;
                }
                str=client.preProcessToken(str);
                strGrp.forEach(function (d,index) {
                    str=str.replace("minMaxGrp_"+index,d);
                });
                flagGroup = true;
                var isValued = {};
                var newData=[];
                function isValidDate(date) {
                    return date && Object.prototype.toString.call(date) === "[object Date]" && !isNaN(date);
                }
                var dataStr={};
                _data.forEach(function (d) {
                    var dimension=d[activeKey];
                    var valTemp = d[groupColorName];
                    if(dataStr[dimension]==undefined){
                        dataStr[dimension]={
                            count: 0,
                            sumIndex: 0,
                            avgIndex: 0,
                            groupColor: {},
                            groupRepeatCheck: {},
                        };
                    }
                    if(!isValued[d[groupColorName]+dimension]){
                        var val = eval(str);
                        dataStr[dimension].sumIndex += parseFloat(val);
                        dataStr[dimension].count++;
                        if(groupColorName) {
                            if(dataStr[dimension].groupRepeatCheck[valTemp] == undefined){
                                dataStr[dimension].groupRepeatCheck[valTemp] = parseFloat(val);
                                dataStr[dimension].groupColor = dataStr[dimension].groupRepeatCheck;
                            }
                            if(type == 'min'){
                                if(dataStr[dimension].groupRepeatCheck[valTemp] > parseFloat(val)){
                                    dataStr[dimension].groupRepeatCheck[valTemp] = parseFloat(val);
                                    dataStr[dimension].groupColor = dataStr[dimension].groupRepeatCheck;
                                }
                            }else if(type == 'max'){
                                if(dataStr[dimension].groupRepeatCheck[valTemp] < parseFloat(val)){
                                    dataStr[dimension].groupRepeatCheck[valTemp] = parseFloat(val);
                                    dataStr[dimension].groupColor = dataStr[dimension].groupRepeatCheck;
                                }
                            }
                        }
                        isValued[d[groupColorName]+dimension]=true;
                    }
                });
                $.each(dataStr, function (val, key) {
                    newData.push({key: key, value: val});
                });
                newData = newData.sort(function (a, b) {
                    return a.key === b.key ? 0 : a.key < b.key ? -1 : 1;
                });
                return newData;
            }
            return [];
        }
        return group;
    };

    module.minMaxWithTooltipNoGroup=function(dimension, measureObj, groupColor, dateFormat, customTooltip, client, dimensionObj){
        var group = {};
        var expression=measureObj.formula;
        var processTooltipText = TooltipModule.processTooltipText;
        var processTooltipTextReduce = TooltipModule.processTooltipTextReduce;
        group['all'] = function () {
            var activeKey=dimensionObj.columnName;
            var _data = dimension.top(Infinity);
            if (activeKey) {
                if(Array.isArray(activeKey)){
                    activeKey=activeKey[0];
                }
                const regex = /(min|max)\(\[([a-z|A-Z|0-9|_|' '|'('|')']*)\]\)?/g;
                var str = expression;
                var flagGroup = false;
                var tableColumn = client.getTableColumn();
                var minGroup={};
                var maxGroup={};
                var strGrp=[];
                var i=0;
                while ((m = regex.exec(expression)) !== null) {
                    if (!(tableColumn[m[2]].columType == "date" || tableColumn[m[2]].columType == "datetime")) {
                        if (!minGroup[m[2]]) {
                            minGroup[m[2]] = {};
                        }
                        if (!maxGroup[m[2]]) {
                            maxGroup[m[2]] = {};
                        }
                        _data.forEach(function (d, index) {
                            if(d[m[2]]){
                                if (!maxGroup[m[2]][d[activeKey]]) {
                                    maxGroup[m[2]][d[activeKey]] = parseInt(d[m[2]]);
                                }
                                if (!minGroup[m[2]][d[activeKey]]) {
                                    minGroup[m[2]][d[activeKey]] = parseInt(d[m[2]]);
                                }
                                if (parseInt(d[m[2]]) > maxGroup[m[2]][d[activeKey]]) {
                                    maxGroup[m[2]][d[activeKey]] = d[m[2]];
                                }
                                else if (parseInt(d[m[2]]) < minGroup[m[2]][d[activeKey]]) {
                                    minGroup[m[2]][d[activeKey]] = d[m[2]];
                                }
                            }
                        });
                        if (m[1] == 'min') {
                            str = str.replace(m[0], "minMaxGrp_"+i);
                            strGrp.push("minGroup['" + m[2] + "'][d[activeKey]]");
                        }
                        if (m[1] == 'max') {
                            str = str.replace(m[0], "minMaxGrp_"+i);
                            strGrp.push("maxGroup['" + m[2] + "'][d[activeKey]]");
                        }
                    } else if (tableColumn[m[2]].columType == "date" || tableColumn[m[2]].columType == "datetime") {
                        if (!minGroup[m[2]]) {
                            minGroup[m[2]] = {};
                        }
                        if (!maxGroup[m[2]]) {
                            maxGroup[m[2]] = {};
                        }
                        _data.forEach(function (d, index) {
                            if(d[m[2]]){
                                if (!maxGroup[m[2]][d[activeKey]]) {
                                    maxGroup[m[2]][d[activeKey]] = moment(d[m[2]]);
                                }
                                if (!minGroup[m[2]][d[activeKey]]) {
                                    minGroup[m[2]][d[activeKey]] = moment(d[m[2]]);
                                }

                                if (moment(d[m[2]]) > maxGroup[m[2]][d[activeKey]]) {
                                    maxGroup[m[2]][d[activeKey]] = moment(d[m[2]]);
                                }
                                else if (moment(d[m[2]]) < minGroup[m[2]][d[activeKey]]) {
                                    minGroup[m[2]][d[activeKey]] = moment(d[m[2]]);
                                }
                            }
                        });
                        if (m[1] == 'min') {
                            str = str.replace(m[0], "minMaxGrp_"+i);
                            strGrp.push("minGroup['" + m[2] + "'][d[activeKey]]");
                        }
                        if (m[1] == 'max') {
                            str = str.replace(m[0], "minMaxGrp_"+i);
                            strGrp.push("maxGroup['" + m[2] + "'][d[activeKey]]");
                        }
                    }
                    if (m.index === regex.lastIndex) {
                        regex.lastIndex++;
                    }
                    i++;
                }
                str=client.preProcessToken(str);
                strGrp.forEach(function (d,index) {
                    str=str.replace("minMaxGrp_"+index,d);
                });
                flagGroup = true;
                var isValued = {};
                newData=[];
                function isValidDate(date) {
                    return date && Object.prototype.toString.call(date) === "[object Date]" && !isNaN(date);
                }

                var dataStr = {};
                _data.forEach(function (d,i) {
                    var dimension = d[activeKey];
                    if(dataStr[dimension]==undefined){
                        dataStr[dimension]={
                            count: 0,
                            sumIndex: 0,
                            avgIndex: 0,
                            customTooltip: true,
                        };
                    }
                    if(!isValued[d[activeKey]]){
                        var val = eval(str);
                        dataStr[dimension].sumIndex += parseFloat(val);
                        ++dataStr[dimension].count;
                        dataStr[dimension].sumIndex = parseFloat(dataStr[dimension].sumIndex)/parseFloat(dataStr[dimension].count);
                        if (customTooltip.datakeys) {
                            customTooltip.datakeys.forEach(function (dd) {
                                if (dd.dataKey == "Measure") {
                                    processTooltipText(dd, dataStr, d);
                                } else {
                                    if (!(dataStr[dimension]["<" + dd.reName + ">"])) {
                                        dataStr[dimension]["<" + dd.reName + ">"] = d[dd.columnName];
                                    }else if (dataStr[dimension]["<" + dd.reName + ">"] != d[dd.columnName]) {
                                        dataStr[dimension]["<" + dd.reName + ">"] = "Multiple Values..";
                                    }
                                }
                            });
                        }
                        isValued[d[activeKey]]=true;
                    }else{
                        customTooltip.datakeys.forEach(function (dd) {
                            console.log(dimension, d[dd.columnName])
                            if (dd.dataKey == "Measure") {
                                processTooltipText(dd, dataStr, d);
                            } else {
                                if (!(dataStr[dimension]["<" + dd.reName + ">"])) {
                                    dataStr[dimension]["<" + dd.reName + ">"] = d[dd.columnName];
                                }else if (dataStr[dimension]["<" + dd.reName + ">"] != d[dd.columnName]) {
                                    dataStr[dimension]["<" + dd.reName + ">"] = "Multiple Values..";
                                }
                            }
                        });
                    }
                    // dataStr[dimension].sumIndex += parseFloat(val);
                    // ++dataStr[dimension].count;
                    // dataStr[dimension].avgIndex = parseFloat(dataStr[dimension].sumIndex)/parseFloat(dataStr[dimension].count);
                });

                $.each(dataStr, function (val, key) {
                    newData.push({key: key, value: val});
                });
                newData = newData.sort(function (a, b) {
                    return a.key === b.key ? 0 : a.key < b.key ? -1 : 1;
                });
                return newData;
            }
            return [];
        }
        return group;
    };

    module.minMaxWithGroupTooltip=function(dimension, measureObj, groupColor, dateFormat, customTooltip, client, dimensionObj){
        var group = {};
        var expression=measureObj.formula;
        var groupColorName = groupColor.columnName;
        var type, col;
        var processTooltipText = TooltipModule.processTooltipGrp;
        group['all']=function () {
            var activeKey=dimensionObj.columnName;
            var _data = dimension.top(Infinity);
            if (activeKey) {
                if(Array.isArray(activeKey)){
                    activeKey=activeKey[0];
                }
                const regex = /(min|max)\(\[([a-z|A-Z|0-9|_|' '|'('|')']*)\]\)?/g;
                var str = expression;
                var flagGroup = false;
                var tableColumn = client.getTableColumn();
                var minGroup={};
                var maxGroup={};
                var strGrp=[];
                var i=0;
                while ((m = regex.exec(expression)) !== null) {
                    type = m[1];
                    col = m[2];
                    if (!(tableColumn[m[2]].columType == "date" || tableColumn[m[2]].columType == "datetime")) {
                        if (!minGroup[m[2]]) {
                            minGroup[m[2]] = {};
                        }
                        if (!maxGroup[m[2]]) {
                            maxGroup[m[2]] = {};
                        }
                        _data.forEach(function (d, index) {
                            if(d[m[2]]){
                                if (!maxGroup[m[2]][d[activeKey]+d[groupColorName]]) {
                                    maxGroup[m[2]][d[activeKey]+d[groupColorName]] = parseInt(d[m[2]]);
                                }
                                if (!minGroup[m[2]][d[activeKey]+d[groupColorName]]) {
                                    minGroup[m[2]][d[activeKey]+d[groupColorName]] = parseInt(d[m[2]]);
                                }
                                if (parseInt(d[m[2]]) > maxGroup[m[2]][d[activeKey]+d[groupColorName]]) {
                                    maxGroup[m[2]][d[activeKey]+d[groupColorName]] = d[m[2]];
                                }
                                else if (parseInt(d[m[2]]) < minGroup[m[2]][d[activeKey]+d[groupColorName]]) {
                                    minGroup[m[2]][d[activeKey]+d[groupColorName]] = d[m[2]];
                                }
                            }
                        });
                        if (m[1] == 'min') {
                            str = str.replace(m[0], "minMaxGrp_"+i);
                            strGrp.push("minGroup['" + m[2] + "'][d[activeKey]+d[groupColorName]]");
                        }
                        if (m[1] == 'max') {
                            str = str.replace(m[0], "minMaxGrp_"+i);
                            strGrp.push("maxGroup['" + m[2] + "'][d[activeKey]+d[groupColorName]]");
                        }
                    } else if (tableColumn[m[2]].columType == "date" || tableColumn[m[2]].columType == "datetime") {
                        if (!minGroup[m[2]]) {
                            minGroup[m[2]] = {};
                        }
                        if (!maxGroup[m[2]]) {
                            maxGroup[m[2]] = {};
                        }
                        _data.forEach(function (d, index) {
                            if(d[m[2]]){
                                if (!maxGroup[m[2]][d[activeKey]+d[groupColorName]]) {
                                    maxGroup[m[2]][d[activeKey]+d[groupColorName]] = moment(d[m[2]]);
                                }
                                if (!minGroup[m[2]][d[activeKey]+d[groupColorName]]) {
                                    minGroup[m[2]][d[activeKey]+d[groupColorName]] = moment(d[m[2]]);
                                }
                                if (moment(d[m[2]]) > maxGroup[m[2]][d[activeKey]+d[groupColorName]]) {
                                    maxGroup[m[2]][d[activeKey]+d[groupColorName]] = moment(d[m[2]]);
                                }
                                else if (moment(d[m[2]]) < minGroup[m[2]][d[activeKey]+d[groupColorName]]) {
                                    minGroup[m[2]][d[activeKey]+d[groupColorName]] = moment(d[m[2]]);
                                }
                            }
                        });
                        if (m[1] == 'min') {
                            str = str.replace(m[0], "minMaxGrp_"+i);
                            strGrp.push("minGroup['" + m[2] + "'][d[activeKey]]");
                        }
                        if (m[1] == 'max') {
                            str = str.replace(m[0], "minMaxGrp_"+i);
                            strGrp.push("maxGroup['" + m[2] + "'][d[activeKey]]");
                        }
                    }
                    if (m.index === regex.lastIndex) {
                        regex.lastIndex++;
                    }
                    i++;
                }
                str=client.preProcessToken(str);
                strGrp.forEach(function (d,index) {
                    str=str.replace("minMaxGrp_"+index,d);
                });
                flagGroup = true;
                var isValued = {};
                newData=[];
                function isValidDate(date) {
                    return date && Object.prototype.toString.call(date) === "[object Date]" && !isNaN(date);
                }
                var dataStr={};
                _data.forEach(function (d) {
                    var dimension=d[activeKey];
                    var valTemp = d[groupColorName];
                    if(dataStr[dimension]==undefined){
                        dataStr[dimension]={
                            count: 0,
                            sumIndex: 0,
                            avgIndex: 0,
                            groupColor: {},
                            groupRepeatCheck: {},
                            tooltipObj:{},
                            avgAggrTooltip:{
                                sum:0,
                                count:0
                            },
                        };
                    }
                    if(!isValued[d[groupColorName]+dimension]){
                        var val = eval(str);
                        dataStr[dimension].sumIndex += parseFloat(val);
                        dataStr[dimension].count++;
                        dataStr[dimension].customTooltip = true;

                        if (groupColorName) {
                            if(dataStr[dimension].groupRepeatCheck[valTemp] == undefined){
                                dataStr[dimension].groupRepeatCheck[valTemp] = parseFloat(val);
                                dataStr[dimension].groupColor = dataStr[dimension].groupRepeatCheck;
                            }
                            if(type == 'min'){
                                if(dataStr[dimension].groupRepeatCheck[valTemp] > parseFloat(val)){
                                    dataStr[dimension].groupRepeatCheck[valTemp] = parseFloat(val);
                                    dataStr[dimension].groupColor = dataStr[dimension].groupRepeatCheck;
                                }
                            }else if(type == 'max'){
                                if(dataStr[dimension].groupRepeatCheck[valTemp] < parseFloat(val)){
                                    dataStr[dimension].groupRepeatCheck[valTemp] = parseFloat(val);
                                    dataStr[dimension].groupColor = dataStr[dimension].groupRepeatCheck;
                                }
                            }
                        }
                        isValued[d[groupColorName]+dimension]=true;
                    }
                    if (customTooltip.datakeys) {
                        customTooltip.datakeys.forEach(function (c) {
                            if(dataStr[dimension].tooltipObj[c.reName+"@@!"+valTemp+"@@#"+c.aggregate] == undefined){
                                dataStr[dimension].tooltipObj[c.reName+"@@!"+valTemp+"@@#"+c.aggregate] = 0;
                            }
                            if (c.dataKey == "Measure") {
                                if(c.aggregate=="avg"){
                                    var avgObj=processTooltipText(c, dataStr[dimension], d)
                                    dataStr[dimension].avgAggrTooltip.sum = avgObj.sum;
                                    dataStr[dimension].avgAggrTooltip.count = avgObj.count;
                                    dataStr[dimension].tooltipObj[c.reName+"@@!"+valTemp+"@@#"+c.aggregate] = dataStr[dimension].avgAggrTooltip.sum/dataStr[dimension].avgAggrTooltip.count;
                                }else{
                                    dataStr[dimension].tooltipObj[c.reName+"@@!"+valTemp+"@@#"+c.aggregate] += processTooltipText(c, dataStr[dimension], d);
                                }

                            } else {
                                if (!(dataStr[dimension]["<" + c.reName + ">"])){
                                    dataStr[dimension].tooltipObj[c.reName+"@@!"+valTemp+"@@#"+c.aggregate] = d[c.columnName];
                                }
                                else if (dataStr[dimension]["<" + c.reName + ">"] != d[c.columnName]) {
                                    dataStr[dimension].tooltipObj[c.reName+"@@!"+valTemp+c.aggregate] = "Multiple Values..";
                                }
                            }
                        });
                    }
                });
                $.each(dataStr, function (val, key) {
                    newData.push({key: key, value: val});
                });
                newData = newData.sort(function (a, b) {
                    return a.key === b.key ? 0 : a.key < b.key ? -1 : 1;
                });
                return newData;
            }
            return [];
        }
        return group;
    };


    return module;
}());

module.exports = SumModule;