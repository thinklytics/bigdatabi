// ===============================================================================================================
//
//                                             Count MODULE
//
// ===============================================================================================================

var TooltipModule=require('../../DataProcessor/TooltipModule');
var HelperModule=require('./HelperModule');
var CountModule = (function () {
    var module = {};

    module.processAverage=function(dimension, measureObj, groupColor,dateFormat,customTooltip,client)
    {
        if(!groupColor && !customTooltip){
            return  module.countWithoutGroupAndTooltip(dimension, measureObj, groupColor,dateFormat,customTooltip,client);
        }else if(groupColor && !customTooltip){
            return module.countWithGroupNoTooltip(dimension, measureObj, groupColor,dateFormat,customTooltip,client);
        }else if(!groupColor && customTooltip){
            return module.countWithTooltipNoGroup(dimension, measureObj, groupColor,dateFormat,customTooltip,client);
        }else{
            return module.countWithGroupTooltip(dimension, measureObj, groupColor,dateFormat,customTooltip,client);
        }
    };

    module.countWithoutGroupAndTooltip=function(dimension, measureObj, groupColor,dateFormat,customTooltip,client){
        var measureColumnName = measureObj.columnName;
        var grp = dimension.group().reduceCount(function (d) {
            return parseInt(d[measureColumnName]);
        });
        //return HelperModule.snap_to_zero(grp);
        return grp;
    };

    module.countWithGroupNoTooltip=function(dimension, measureObj, groupColor,dateFormat,customTooltip,client){
        var measureColumnName = measureObj.columnName;
        var processTooltipText = TooltipModule.processTooltipText;
        var groupColorName=groupColor.columnName;
        return dimension.group().reduce(
            /* callback for when data is added to the current filter results */
            function (g, v) {
                ++g.count;
                if (groupColorName) {
                    var valTemp = (v[groupColorName]);
                    if (!g.groupRepeatCheck[valTemp])
                        g.groupRepeatCheck[valTemp] = 1;
                    else
                        g.groupRepeatCheck[valTemp] += 1;
                    g.groupColor = g.groupRepeatCheck;
                }
                return g;
            },
            /*
             * callback for when data is removed from the current filter
             * results
             */

            function (g, v) {
                --g.count;
                var valTemp = (v[groupColorName]);
                if (!g.groupRepeatCheck[valTemp]) {
                    g.groupRepeatCheck[valTemp] = 1;
                } else {
                    g.groupRepeatCheck[valTemp] = g.groupRepeatCheck[valTemp] - 1;
                }
                g.groupColor = g.groupRepeatCheck;
                return g;
            },
            /* initialize p */
            function () {
                return {
                    count: 0,
                    sumIndex: 0,
                    avgIndex: 0,
                    groupColor: {},
                    groupRepeatCheck: {},
                    sum: {},
                    avg: {}
                };
            }
        );
    };

    module.countWithTooltipNoGroup=function(dimension, measureObj, groupColor,dateFormat,customTooltip,client){
        var columName = measureObj.columnName;
        var processTooltipText = TooltipModule.processTooltipText;
        // var columType = measureObj.columType;
        var grp = dimension.group().reduce(
            /* callback for when data is added to the current filter results */
            function (g, v) {
                ++g.count;
                g.customTooltip = true;
                if (customTooltip.datakeys) {
                    customTooltip.datakeys.forEach(function (d) {
                        if (d.dataKey == "Measure") {
                            processTooltipText(d, g, v);
                        } else {
                            if (!(g["<" + d.reName + ">"])) {
                                g["<" + d.reName + ">"] = v[d.columnName];
                            }
                            else if (g["<" + d.reName + ">"] != v[d.columnName]) {
                                g["<" + d.reName + ">"] = "Multiple Values..";

                            }
                        }
                    });
                }
                return g;
            }
            ,
            /*
             * callback for when data is removed from the current filter
             * results
             */
            function (g, v) {
                --g.count;
                g.customTooltip = true;
                if (customTooltip.dataKeys) {
                    customTooltip.dataKeys.forEach(function (d) {
                        if (d.dataKey == "Measure") {
                            processTooltipText(d, g, v);
                        } else {
                            if (!g["<" + d.reName + ">"])
                                g["<" + d.reName + ">"] = v[d.columnName];
                            else if (g["<" + d.reName + ">"] != v[d.columnName]) {
                                g["<" + d.reName + ">"] = "Multiple Values..";
                            }
                        }

                    });
                }
                return g;
            },
            /* initialize p */
            function () {
                return {
                    count: 0,
                    sumIndex: 0,
                    avgIndex: 0
                };
            });
        return grp;
    };
    module.countWithGroupTooltip=function(dimension, measureObj, groupColor,dateFormat,customTooltip,client){
        var measureColumnName = measureObj.columnName;
        var groupColorName=groupColor.columnName;
        var processTooltipText = TooltipModule.processTooltipGrp;
        return dimension.group().reduce(
            /* callback for when data is added to the current filter results */
            function (g, v) {
                ++g.count;
                g.customTooltip = true;
                if (groupColorName) {
                    var valTemp = (v[groupColorName]);
                    if (!g.groupRepeatCheck[valTemp]){
                        g.groupRepeatCheck[valTemp] = 1;
                        if (customTooltip.datakeys) {
                            customTooltip.datakeys.forEach(function (d) {
                                if (d.dataKey == "Measure") {
                                    if(d.aggregate=="avg"){
                                        var avgObj=processTooltipText(d, g, v)
                                        g.avgAggrTooltip.sum = avgObj.sum;
                                        g.avgAggrTooltip.count = avgObj.count;
                                        g.tooltipObj[d.reName+"@@!"+valTemp+"@@#"+d.aggregate] = g.avgAggrTooltip.sum/g.avgAggrTooltip.count;
                                    }else{
                                        g.tooltipObj[d.reName+"@@!"+valTemp+"@@#"+d.aggregate] = processTooltipText(d, g, v);
                                    }
                                } else {
                                    if (!(g["<" + d.reName + ">"])) {
                                        g.tooltipObj[d.reName+"@@!"+valTemp] = v[d.columnName];
                                    }
                                    else if (g["<" + d.reName + ">"] != v[d.columnName]) {
                                        g.tooltipObj[d.reName+"@@!"+valTemp] = "Multiple Values..";

                                    }
                                }
                            });
                        }
                    }
                    else{
                        g.groupRepeatCheck[valTemp] += 1;
                        if (customTooltip.datakeys) {
                            customTooltip.datakeys.forEach(function (d) {
                                if (d.dataKey == "Measure") {
                                    if(d.aggregate=="avg"){
                                        var avgObj=processTooltipText(d, g, v)
                                        g.avgAggrTooltip.sum += avgObj.sum;
                                        g.avgAggrTooltip.count += avgObj.count;
                                        g.tooltipObj[d.reName+"@@!"+valTemp+"@@#"+d.aggregate] = g.avgAggrTooltip.sum/g.avgAggrTooltip.count;
                                    }else{
                                        g.tooltipObj[d.reName+"@@!"+valTemp+"@@#"+d.aggregate] += processTooltipText(d, g, v);
                                    }
                                }else {
                                    if (!(g["<" + d.reName + ">"])) {
                                        g.tooltipObj[d.reName+"@@!"+valTemp] = v[d.columnName];
                                    }
                                    else if (g["<" + d.reName + ">"] != v[d.columnName]) {
                                        g.tooltipObj[d.reName+"@@!"+valTemp] = "Multiple Values..";

                                    }
                                }
                            });
                        }
                    }
                    g.groupColor = g.groupRepeatCheck;
                }
                return g;
            },
            /*
             * callback for when data is removed from the current filter
             * results
             */

            function (g, v) {
                --g.count;
                var val = v[measureColumnName];
                if (!val || isNaN(val)) {
                    val = 0;
                } else {
                    val = parseFloat(val);
                }
                g.customTooltip = true;
                var valTemp = (v[groupColorName]);
                if (!g.groupRepeatCheck[valTemp]) {
                    g.groupRepeatCheck[valTemp] = 1;
                    if (customTooltip.dataKeys) {
                        customTooltip.dataKeys.forEach(function (d) {
                            if (d.dataKey == "Measure") {
                                if(d.aggregate=="avg"){
                                    var avgObj=processTooltipText(d, g, v)
                                    g.avgAggrTooltip.sum = avgObj.sum;
                                    g.avgAggrTooltip.count = avgObj.count;
                                    g.tooltipObj[d.reName+"@@!"+valTemp+"@@#"+d.aggregate] = g.avgAggrTooltip.sum/g.avgAggrTooltip.count;
                                }else{
                                    g.tooltipObj[d.reName+"@@!"+valTemp+"@@#"+d.aggregate] = processTooltipText(d, g, v);
                                }
                            } else {
                                if (!g["<" + d.reName + ">"])
                                    g.tooltipObj[d.reName+"@@!"+valTemp] = v[d.columnName];
                                else if (g["<" + d.reName + ">"] != v[d.columnName]) {
                                    g.tooltipObj[d.reName+"@@!"+valTemp] = "Multiple Values..";
                                }
                            }

                        });
                    }
                } else {
                    g.groupRepeatCheck[d.reName+"@@!"+valTemp] = g.groupRepeatCheck[valTemp] - 1;
                    if (customTooltip.dataKeys) {
                        customTooltip.dataKeys.forEach(function (d) {
                            if (d.dataKey == "Measure") {
                                if(d.aggregate=="avg"){
                                    var avgObj=processTooltipText(d, g, v)
                                    g.avgAggrTooltip.sum -= avgObj.sum;
                                    g.avgAggrTooltip.count -= avgObj.count;
                                    g.tooltipObj[d.reName+"@@!"+valTemp+"@@#"+d.aggregate] = g.avgAggrTooltip.sum/g.avgAggrTooltip.count;
                                }else{
                                    g.tooltipObj[d.reName+"@@!"+valTemp+"@@#"+d.aggregate] -= processTooltipText(d, g, v);
                                }
                            }else {
                                if (!g["<" + d.reName + ">"])
                                    g.tooltipObj[d.reName+"@@!"+valTemp] = v[d.columnName];
                                else if (g["<" + d.reName + ">"] != v[d.columnName]) {
                                    g.tooltipObj[d.reName+"@@!"+valTemp] = "Multiple Values..";
                                }
                            }
                        });
                    }
                }
                g.groupColor = g.groupRepeatCheck;
                return g;
            },
            /* initialize p */
            function () {
                return {
                    count: 0,
                    sumIndex: 0,
                    avgIndex: 0,
                    groupColor: {},
                    groupRepeatCheck: {},
                    tooltipObj:{},
                    sum: {},
                    avg: {},
                    avgAggrTooltip:{
                        sum:0,
                        count:0
                    }
                };
            });
    };
    return module;
}());
module.exports = CountModule;