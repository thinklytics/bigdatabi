// ===============================================================================================================
//
//                                             AVERAGE MODULE
//
// ===============================================================================================================

var TooltipModule=require('../../DataProcessor/TooltipModule');
var TooltipModule=require('./HelperModule');
var AverageModule = (function () {
    var module = {};

    module.processAverage=function(dimension, measureObj, groupColor,dateFormat,customTooltip,client)
    {
        if(!groupColor && !customTooltip){
           return  module.avgWithoutGroupAndTooltip(dimension, measureObj, groupColor,dateFormat,customTooltip,client);
        }else if(groupColor && !customTooltip){
           return module.avgWithGroupNoTooltip(dimension, measureObj, groupColor,dateFormat,customTooltip,client);
        }else if(!groupColor && customTooltip){
           return module.avgWithTooltipNoGroup(dimension, measureObj, groupColor,dateFormat,customTooltip,client);
        }else{
            return module.avgWithGroupTooltip(dimension, measureObj, groupColor,dateFormat,customTooltip,client);
        }
    };

    module.avgWithoutGroupAndTooltip=function(dimension, measureObj, groupColor,dateFormat,customTooltip,client){
        var measureColumnName=measureObj.columnName;
        return dimension.group().reduce(function(p, v) { // add
                var val= v[measureColumnName];
                if (!val || isNaN(val)) {
                    val = 0;
                } else {
                    val=parseFloat(val);
                }
                p.sumIndex+=parseFloat(val);
                p.count++;
                p.val=parseFloat(p.sumIndex/p.count);
                return p;
            },
            function(p, v) {
                p.count--;
                var val= v[measureColumnName];
                if (!val || isNaN(val)) {
                    val = 0;
                } else {
                    val=parseFloat(val);
                }
                p.sumIndex-=val;
                p.val=parseFloat(p.sumIndex/p.count);

                return p;
            },
            function() {
                p={sumIndex:0,count:0,val:0};

                return p;
            }
        );
    };

    module.avgWithGroupNoTooltip=function(dimension, measureObj, groupColor,dateFormat,customTooltip,client){
        var measureColumnName=measureObj.columnName;
        var groupColorName=groupColor.columnName;
        return dimension.group().reduce(
            /* callback for when data is added to the current filter results */
            function (g, v) {
                ++g.count;
                var val = v[measureColumnName];
                if (!val || isNaN(val)) {
                    val = 0;
                } else {
                    val = parseFloat(val);
                }

                g.sumIndex += val;

                if (groupColorName) {

                    var valTemp = (v[groupColorName]);

                    if (!g.groupRepeatCheck[valTemp])
                        g.groupRepeatCheck[valTemp] = {};
                    if(!g.groupRepeatCheck[valTemp]['sum']){
                        g.groupRepeatCheck[valTemp]['sum']=0;
                    }
                    if(!g.groupRepeatCheck[valTemp]['count']){
                        g.groupRepeatCheck[valTemp]['count']=0;
                    }
                    g.groupRepeatCheck[valTemp]['sum'] += val;
                    g.groupRepeatCheck[valTemp]['count']+=val;

                    g.groupColor[valTemp] = g.groupRepeatCheck[valTemp]['sum']/g.groupRepeatCheck[valTemp]['count'];
                }
                return g;
            },
            /*
             * callback for when data is removed from the current filter
             * results
             */

            function (g, v) {
                --g.count;
                var val = v[measureColumnName];
                if (!val || isNaN(val)) {
                    val = 0;
                } else {
                    val = parseFloat(val);
                }
                g.sumIndex -= val;
                var valTemp = (v[groupColorName]);
                if (groupColorName) {

                    var valTemp = (v[groupColorName]);

                    if (!g.groupRepeatCheck[valTemp])
                        g.groupRepeatCheck[valTemp] = {};
                    if(!g.groupRepeatCheck[valTemp]['sum']){
                        g.groupRepeatCheck[valTemp]['sum']=0;
                    }
                    if(!g.groupRepeatCheck[valTemp]['count']){
                        g.groupRepeatCheck[valTemp]['count']=0;
                    }
                    g.groupRepeatCheck[valTemp]['sum'] -= val;
                    g.groupRepeatCheck[valTemp]['count']-=1;

                    g.groupColor[valTemp] = g.groupRepeatCheck[valTemp]['sum']/g.groupRepeatCheck[valTemp]['count'];
                }

                return g;
            },
            /* initialize p */
            function () {
                return {
                    count: 0,
                    sumIndex: 0,
                    avgIndex: 0,
                    groupColor: {},
                    groupRepeatCheck: {},
                    sum: {},
                    avg: {}
                };
            });
    };

    module.avgWithTooltipNoGroup=function(dimension, measureObj, groupColor,dateFormat,customTooltip,client){
        var columName = measureObj.columnName;
        var grp= dimension.group().reduce(
            function (g, v) {
                ++g.count;
                var val = v[columName];
                if(!val || isNaN(val)) {
                    val = 0;
                }else {
                    val=parseFloat(val);
                }
                g.sumIndex += val;
                g.avgIndex = parseFloat(g.sumIndex / g.count);
                g.customTooltip=true;
                if (customTooltip.datakeys) {
                    customTooltip.datakeys.forEach(function (d) {

                        if (d.dataKey == "Measure") {
                            TooltipModule.processTooltipText(d,g,v);
                        } else {
                            if(!(g["<" + d.reName + ">"])){
                                g["<" + d.reName + ">"] = v[d.columnName];

                            }
                            else if(g["<" + d.reName + ">"]!=v[d.columnName]){
                                g["<" + d.reName + ">"]="Multiple Values..";

                            }
                        }
                    });
                }
                return g;
            }
            ,
            /*
             * callback for when data is removed from the current filter
             * results
             */
            function (g, v) {

                --g.count;
                var val;
                val = v[columName];
                if(!val || isNaN(val)) {
                    val = 0;
                }else {
                    val=parseFloat(val);
                }
                g.customTooltip=true;


                if (customTooltip.dataKeys) {
                    customTooltip.dataKeys.forEach(function (d) {
                        if (d.dataKey == "Measure") {
                            TooltipModule.processTooltipText(d,g,v);
                        } else {
                            if(!g["<" + d.reName + ">"])
                                g["<" + d.reName + ">"] = v[d.columnName];
                            else if(g["<" + d.reName + ">"]!=v[d.columnName]){
                                g["<" + d.reName + ">"]="Multiple Values..";
                            }
                        }

                    });
                }
                g.sumIndex -= val;
                g.avgIndex = g.count ? g.sumIndex / g.count : 0;
                return g;
            },
            function () {
                return {
                    count: 0,
                    sumIndex: 0,
                    avgIndex: 0,
                    groupColor: {},
                    groupRepeatCheck: {},
                    sum: {},
                    avg: {}
                };
            });


        return grp;
    };

    module.avgWithGroupNoTooltip=function(dimension, measureObj, groupColor,dateFormat,customTooltip,client){
        var measureColumnName=measureObj.columnName;
        var groupColorName=groupColor.columnName;
        return dimension.group().reduce(
            /* callback for when data is added to the current filter results */
            function (g, v) {
                ++g.count;
                var val = v[measureColumnName];
                if (!val || isNaN(val)) {
                    val = 0;
                } else {
                    val = parseFloat(val);
                }

                g.sumIndex += val;

                if (groupColorName) {

                    var valTemp = (v[groupColorName]);

                    if (!g.groupRepeatCheck[valTemp])
                        g.groupRepeatCheck[valTemp] = {};
                    if(!g.groupRepeatCheck[valTemp]['sum']){
                        g.groupRepeatCheck[valTemp]['sum']=0;
                    }
                    if(!g.groupRepeatCheck[valTemp]['count']){
                        g.groupRepeatCheck[valTemp]['count']=0;
                    }
                    g.groupRepeatCheck[valTemp]['sum'] += val;
                    g.groupRepeatCheck[valTemp]['count']+=1;

                    g.groupColor[valTemp] = g.groupRepeatCheck[valTemp]['sum']/g.groupRepeatCheck[valTemp]['count'];
                }
                return g;
            },
            /*
             * callback for when data is removed from the current filter
             * results
             */

            function (g, v) {
                --g.count;
                var val = v[measureColumnName];
                if (!val || isNaN(val)) {
                    val = 0;
                } else {
                    val = parseFloat(val);
                }
                g.sumIndex -= val;
                var valTemp = (v[groupColorName]);
                if (groupColorName) {

                    var valTemp = (v[groupColorName]);

                    if (!g.groupRepeatCheck[valTemp])
                        g.groupRepeatCheck[valTemp] = {};
                    if(!g.groupRepeatCheck[valTemp]['sum']){
                        g.groupRepeatCheck[valTemp]['sum']=0;
                    }
                    if(!g.groupRepeatCheck[valTemp]['count']){
                        g.groupRepeatCheck[valTemp]['count']=0;
                    }
                    g.groupRepeatCheck[valTemp]['sum'] -= val;
                    g.groupRepeatCheck[valTemp]['count']-=1;

                    g.groupColor[valTemp] = g.groupRepeatCheck[valTemp]['sum']/g.groupRepeatCheck[valTemp]['count'];
                }

                return g;
            },
            /* initialize p */
            function () {
                return {
                    count: 0,
                    sumIndex: 0,
                    avgIndex: 0,
                    groupColor: {},
                    groupRepeatCheck: {},
                    sum: {},
                    avg: {}
                };
            });
    };
    module.avgWithGroupTooltip=function(dimension, measureObj, groupColor,dateFormat,customTooltip,client){
        var measureColumnName=measureObj.columnName;
        var groupColorName=groupColor.columnName;
        var processTooltipText = TooltipModule.processTooltipGrp; // for avg tooltip pending
        var avgGroup =dimension.group().reduce(
            /* callback for when data is added to the current filter results */
            function (g, v) {
                ++g.count;
                var val = v[measureColumnName];
                if (!val || isNaN(val)) {
                    val = 0;
                } else {
                    val = parseFloat(val);
                }

                g.sumIndex += val;

                if (groupColorName) {

                    var valTemp = (v[groupColorName]);

                    if (!g.groupRepeatCheck[valTemp])
                        g.groupRepeatCheck[valTemp] = {};
                    if(!g.groupRepeatCheck[valTemp]['sum']){
                        g.groupRepeatCheck[valTemp]['sum']=0;
                    }
                    if(!g.groupRepeatCheck[valTemp]['count']){
                        g.groupRepeatCheck[valTemp]['count']=0;
                    }
                    if(!g.groupRepeatCheck[valTemp]['avg']){
                        g.groupRepeatCheck[valTemp]['avg']=0;
                    }
                    g.groupRepeatCheck[valTemp]['sum'] += val;
                    g.groupRepeatCheck[valTemp]['count']+=1;

                    g.groupColor[valTemp] = g.groupRepeatCheck[valTemp]['sum']/g.groupRepeatCheck[valTemp]['count'];
                    g.avgIndex=g.groupRepeatCheck[valTemp]['sum']/g.groupRepeatCheck[valTemp]['count'];
                    g.groupRepeatCheck[valTemp]['avg']=g.avgIndex;
                }
                return g;
            },
            /*
             * callback for when data is removed from the current filter
             * results
             */

            function (g, v) {
                --g.count;
                var val = v[measureColumnName];
                if (!val || isNaN(val)) {
                    val = 0;
                } else {
                    val = parseFloat(val);
                }
                g.sumIndex -= val;
                var valTemp = (v[groupColorName]);
                if (groupColorName) {

                    var valTemp = (v[groupColorName]);

                    if (!g.groupRepeatCheck[valTemp])
                        g.groupRepeatCheck[valTemp] = {};
                    if(!g.groupRepeatCheck[valTemp]['sum']){
                        g.groupRepeatCheck[valTemp]['sum']=0;
                    }
                    if(!g.groupRepeatCheck[valTemp]['count']){
                        g.groupRepeatCheck[valTemp]['count']=0;
                    }
                    g.groupRepeatCheck[valTemp]['sum'] -= val;
                    g.groupRepeatCheck[valTemp]['count']-=1;
                    if(g.groupRepeatCheck[valTemp]['count'])
                    {
                        g.groupColor[valTemp] = g.groupRepeatCheck[valTemp]['sum']/g.groupRepeatCheck[valTemp]['count'];
                        g.avgIndex=g.groupRepeatCheck[valTemp]['sum']/g.groupRepeatCheck[valTemp]['count'];
                        g.groupRepeatCheck[valTemp]['avg']=g.avgIndex;
                    }else{
                        g.groupColor[valTemp] = 0;
                        g.avgIndex=0;
                        g.groupRepeatCheck[valTemp]['avg']=0;
                    }
                }

                return g;
            },
            /* initialize p */
            function () {
                return {
                    count: 0,
                    sumIndex: 0,
                    avgIndex: 0,
                    groupColor: {},
                    groupRepeatCheck: {},
                    sum: {},
                    avg: {},
                    avgAggrTooltip:{
                        sum:0,
                        count:0
                    }
                };
            });
        return avgGroup;
    };
    return module;
}());
module.exports = AverageModule;