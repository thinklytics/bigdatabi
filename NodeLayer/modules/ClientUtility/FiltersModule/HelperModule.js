var HelperModule = (function () {
    var module = {};
    //DEALS WITH THE ISSUE OF GETTING 0 VALUES IN JAVASCRIPT SUBTRACTION AND ADDITION WITH FLOATING POINT VALUES
    module.snap_to_zero_singleval=function(val) {
        return (Math.abs(val) < 1e-6) ? 0 : val;
    };
    module.snap_to_zero=function(source_group) {
        return {
            all: function () {
                return source_group.all().map(function (d) {
                    return {
                        key: d.key,
                        value: (Math.abs(d.value) < 1e-6) ? 0 : d.value
                    };
                });
            }
        };
    };
    module.snap_to_zero_Group=function(source_group) {
        return {
            all: function () {
                return source_group.all().map(function (d) {
                    d.value.sumIndex = (Math.abs(d.value.sumIndex) < 1e-6) ? 0 : d.value.sumIndex;
                    return d;
                });
            }
        };
    };
    //...........................................................................................................
    return module;
}());
module.exports = HelperModule;