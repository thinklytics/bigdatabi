// ===============================================================================================================
//
//                                             SUM MODULE
//
// ===============================================================================================================

var TooltipModule=require('../../DataProcessor/TooltipModule');
var HelperModule=require('./HelperModule');
var SumModule = (function () {
    var module = {};

    module.processAverage=function(dimension, measureObj, groupColor,dateFormat,customTooltip,client){
        if(!groupColor && !customTooltip){
            return  module.sumWithoutGroupAndTooltip(dimension, measureObj, groupColor,dateFormat,customTooltip,client);
        }else if(groupColor && !customTooltip){
            return module.sumWithGroupNoTooltip(dimension, measureObj, groupColor,dateFormat,customTooltip,client);
        }else if(!groupColor && customTooltip){
            return module.sumWithTooltipNoGroup(dimension, measureObj, groupColor,dateFormat,customTooltip,client);
        }else{
            return module.sumWithGroupTooltip(dimension, measureObj, groupColor,dateFormat,customTooltip,client);
        }
    };

    module.sumWithoutGroupAndTooltip=function(dimension, measureObj, groupColor,dateFormat,customTooltip,client){
        var measureColumnName = measureObj.columnName;
        var grp = dimension.group().reduceSum(function (d) {
            return parseFloat(d[measureColumnName]);
        });
        return HelperModule.snap_to_zero(grp);
    };

    module.sumWithGroupNoTooltip=function(dimension, measureObj, groupColor,dateFormat,customTooltip,client){
        var measureColumnName = measureObj.columnName;
        var processTooltipText = TooltipModule.processTooltipText;
        var groupColorName=groupColor.columnName;
        var grp = dimension.group().reduce(
            /* callback for when data is added to the current filter results */
            function (g, v) {
                ++g.count;
                var val = v[measureColumnName];
                if (!val || isNaN(val)) {
                    val = 0;
                }else {
                    val = parseFloat(val);
                }

                g.sumIndex += val;
                if (groupColorName) {
                    var valTemp = (v[groupColorName]);
                    if (!g.groupRepeatCheck[valTemp])
                        g.groupRepeatCheck[valTemp] = val;
                    else
                        g.groupRepeatCheck[valTemp] += val;
                    g.groupColor = g.groupRepeatCheck;
                }
                return g;
            },
            /*
             * callback for when data is removed from the current filter
             * results
             */
            function (g, v) {
                --g.count;
                var val = v[measureColumnName];
                if (!val || isNaN(val)) {
                    val = 0;
                }else {
                    val = parseFloat(val);
                }

                var valTemp = (v[groupColorName]);
                if (!g.groupRepeatCheck[valTemp]) {
                    g.groupRepeatCheck[valTemp] = val;
                } else {
                    g.groupRepeatCheck[valTemp] = g.groupRepeatCheck[valTemp] - val;
                }
                g.groupColor = g.groupRepeatCheck;
                g.sumIndex -= val;
                return g;
            },
            /* initialize p */
            function () {
                return {
                    count: 0,
                    sumIndex: 0,
                    avgIndex: 0,
                    groupColor: {},
                    groupRepeatCheck: {},
                    sum: {},
                    avg: {}
                };
            }
        );
        return HelperModule.snap_to_zero_Group(grp);
    };

    module.sumWithTooltipNoGroup=function(dimension, measureObj, groupColor,dateFormat,customTooltip,client){
        var columName = measureObj.columnName;
        var processTooltipText = TooltipModule.processTooltipText;
        var processTooltipTextReduce = TooltipModule.processTooltipTextReduce;
        // var columType = measureObj.columType;
        var grp = dimension.group().reduce(
            /* callback for when data is added to the current filter results */
            function (g, v) {
                ++g.count;
                var val = v[columName];
                if (!val || isNaN(val)) {
                    val = 0;
                } else {
                    val = parseFloat(val);
                }
                g.sumIndex += val;
                g.avgIndex = parseFloat(g.sumIndex / g.count);
                g.customTooltip = true;
                if (customTooltip.datakeys) {
                    customTooltip.datakeys.forEach(function (d) {
                        if (d.dataKey == "Measure") {
                            processTooltipText(d, g, v);
                        } else {
                            if (!(g["<" + d.reName + ">"])) {
                                g["<" + d.reName + ">"] = v[d.columnName];
                            }
                            else if (g["<" + d.reName + ">"] != v[d.columnName]) {
                                g["<" + d.reName + ">"] = "Multiple Values..";

                            }
                        }
                    });
                }
                return g;
            }
            ,
            /*
             * callback for when data is removed from the current filter
             * results
             */
            function (g, v) {
                --g.count;
                var val;
                val = v[columName];
                if (!val || isNaN(val)) {
                    val = 0;
                } else {
                    val = parseFloat(val);
                }
                g.customTooltip = true;
                if (customTooltip.datakeys) {
                    customTooltip.datakeys.forEach(function (d) {
                        if (d.dataKey == "Measure") {
                            processTooltipTextReduce(d, g, v);
                        } else {
                            if (!g["<" + d.reName + ">"])
                                g["<" + d.reName + ">"] = v[d.columnName];
                            else if (g["<" + d.reName + ">"] != v[d.columnName]) {
                                g["<" + d.reName + ">"] = "Multiple Values..";
                            }
                        }
                    });
                }


                g.sumIndex -= val;

                g.avgIndex = g.count ? g.sumIndex / g.count : 0;
                return g;
            },
            /* initialize p */
            function () {
                return {
                    count: 0,
                    sumIndex: 0,
                    avgIndex: 0
                };
            });
        return grp;
    };
    
    module.sumWithGroupTooltip=function(dimension, measureObj, groupColor,dateFormat,customTooltip,client){
        var measureColumnName = measureObj.columnName;
        var groupColorName=groupColor.columnName;
        var processTooltipText = TooltipModule.processTooltipGrp;
        return dimension.group().reduce(
            /* callback for when data is added to the current filter results */
            function (g, v) {
                ++g.count;
                var val = v[measureColumnName];
                if (!val || isNaN(val)) {
                    val = 0;
                } else {
                    val = parseFloat(val);
                }
                g.sumIndex += val;
                g.customTooltip = true;
                if (groupColorName) {
                    var valTemp = (v[groupColorName]);
                    if (!g.groupRepeatCheck[valTemp]){
                        g.groupRepeatCheck[valTemp] = val;
                        if (customTooltip.datakeys) {
                            customTooltip.datakeys.forEach(function (d) {
                                if (d.dataKey == "Measure") {
                                    if(d.aggregate=="avg"){
                                        var avgObj=processTooltipText(d, g, v)
                                        g.avgAggrTooltip.sum = avgObj.sum;
                                        g.avgAggrTooltip.count = avgObj.count;
                                        g.tooltipObj[d.reName+"@@!"+valTemp+"@@#"+d.aggregate] = g.avgAggrTooltip.sum/g.avgAggrTooltip.count;
                                    }else{
                                        g.tooltipObj[d.reName+"@@!"+valTemp+"@@#"+d.aggregate] = processTooltipText(d, g, v);
                                    }
                                } else {
                                    if (!(g["<" + d.reName + ">"])) {
                                        g.tooltipObj[d.reName+"@@!"+valTemp]  = v[d.columnName];
                                    }
                                    else if (g["<" + d.reName + ">"] != v[d.columnName]) {
                                        g.tooltipObj[d.reName+"@@!"+valTemp]= "Multiple Values..";
                                    }
                                }
                            });
                        }
                    }
                    else{
                        g.groupRepeatCheck[valTemp] += val;  
                        if (customTooltip.datakeys) {
                            customTooltip.datakeys.forEach(function (d) {
                                if (d.dataKey == "Measure") {
                                    if(d.aggregate=="avg"){
                                        var avgObj=processTooltipText(d, g, v)
                                        g.avgAggrTooltip.sum += avgObj.sum;
                                        g.avgAggrTooltip.count += avgObj.count;
                                        g.tooltipObj[d.reName+"@@!"+valTemp+"@@#"+d.aggregate] = g.avgAggrTooltip.sum/g.avgAggrTooltip.count;
                                    }else{
                                        g.tooltipObj[d.reName+"@@!"+valTemp+"@@#"+d.aggregate] += processTooltipText(d, g, v);
                                    }

                                }else {
                                    if (!(g["<" + d.reName + ">"])) {
                                        g.tooltipObj[d.reName+"@@!"+valTemp]  = v[d.columnName];
                                    }
                                    else if (g["<" + d.reName + ">"] != v[d.columnName]) {
                                        g.tooltipObj[d.reName+"@@!"+valTemp]= "Multiple Values..";
                                    }
                                }
                            });
                        }
                    }
                    g.groupColor = g.groupRepeatCheck;
                }
                return g;
            },
            /*
             * callback for when data is removed from the current filter
             * results
             */
            function (g, v) {
                --g.count;
                var val = v[measureColumnName];
                if (!val || isNaN(val)) {
                    val = 0;
                } else {
                    val = parseFloat(val);
                }
                g.customTooltip = true;
                var valTemp = (v[groupColorName]);
                if (!g.groupRepeatCheck[valTemp]) {
                    g.groupRepeatCheck[valTemp] = val;
                    if (customTooltip.datakeys) {
                        customTooltip.datakeys.forEach(function (d) {
                            if (d.dataKey == "Measure") {
                                if(d.aggregate=="avg"){
                                    var avgObj=processTooltipText(d, g, v)
                                    g.avgAggrTooltip.sum = avgObj.sum;
                                    g.avgAggrTooltip.count = avgObj.count;
                                    g.tooltipObj[d.reName+"@@!"+valTemp+"@@#"+d.aggregate] = g.avgAggrTooltip.sum/g.avgAggrTooltip.count;
                                }else{
                                    g.tooltipObj[d.reName+"@@!"+valTemp+"@@#"+d.aggregate] = processTooltipText(d, g, v);
                                }

                            } else {
                                if (!(g["<" + d.reName + ">"])) {
                                    g.tooltipObj[d.reName+"@@!"+valTemp+"@@#"+d.aggregate] = v[d.columnName];
                                }
                                else if (g["<" + d.reName + ">"] != v[d.columnName]) {
                                    g.tooltipObj[d.reName+"@@!"+valTemp+d.aggregate] = "Multiple Values..";

                                }
                            }
                        });
                    }
                } else {
                    g.groupRepeatCheck[valTemp] = g.groupRepeatCheck[valTemp] - val;
                    if (customTooltip.datakeys) {
                        customTooltip.datakeys.forEach(function (d) {
                            if (d.dataKey == "Measure") {
                                if(d.aggregate=="avg"){
                                    var avgObj=processTooltipText(d, g, v)
                                    g.avgAggrTooltip.sum -= avgObj.sum;
                                    g.avgAggrTooltip.count -= avgObj.count;
                                    g.tooltipObj[d.reName+"@@!"+valTemp+"@@#"+d.aggregate] = g.avgAggrTooltip.sum/g.avgAggrTooltip.count;
                                }else{
                                    g.tooltipObj[d.reName+"@@!"+valTemp+"@@#"+d.aggregate] -= processTooltipText(d, g, v);
                                }
                            }else {
                                if (!(g["<" + d.reName + ">"])) {
                                    g.tooltipObj[d.reName+"@@!"+valTemp] = v[d.columnName];
                                }
                                else if (g["<" + d.reName + ">"] != v[d.columnName]) {
                                    g.tooltipObj[d.reName+"@@!"+valTemp] = "Multiple Values..";
                                }
                            }
                        });
                    }
                }
                g.groupColor = g.groupRepeatCheck;
                g.sumIndex -= val;
                return g;
            },
            /* initialize p */
            function () {
                return {
                    count: 0,
                    sumIndex: 0,
                    avgIndex: 0,
                    groupColor: {},
                    groupRepeatCheck: {},
                    tooltipObj:{},
                    sum: {},
                    avg: {},
                    avgAggrTooltip:{
                        sum:0,
                        count:0
                    }
                };
            });
    };
    return module;
}());
module.exports = SumModule;