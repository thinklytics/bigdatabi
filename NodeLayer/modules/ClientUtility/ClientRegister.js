var $ = require('underscore');
var ClientRegistry=(function(){
    var ClientRegister={};
    return {
        register:function(client){
            if(!ClientRegister[client.getSessionId()]){
                ClientRegister[client.getSessionId()]={};
                ClientRegister[client.getSessionId()][client.getMetadataId()]=client;
            }else{
                ClientRegister[client.getSessionId()][client.getMetadataId()]=client;
            }

            return client;
        },
        deleteRegisterObj:function (sessionId) {
            delete ClientRegister[sessionId];
        },
        hasClient:function(sessionId){
            if(ClientRegister[sessionId])
                return true;
            return false;
        },
        getClient:function(sessionId){
          return  ClientRegister[sessionId];
        },
        getMetadataClient:function(sessionId,metadataId){
            if(ClientRegister[sessionId]){
                return ClientRegister[sessionId][metadataId];
            }
            return false;
        },
        deleteClient:function(metadataId){
            var tempDeleteMetadata=[];
            $.each(ClientRegister,function (value,key) {
                $.each(value,function (v,k) {
                    if(k==metadataId){
                        tempDeleteMetadata.push(key);
                    }
                });
            });
            var globalDataDelete=[];
            Object.keys(global.dataMetaWise).forEach(function (d) {
                  var splitArr=d.split(":");
                  if(splitArr[0]==metadataId || splitArr[0]+"-realtime"==metadataId+"-realtime"){
                      globalDataDelete.push(d);
                  }
            });
            globalDataDelete.forEach(function (d) {
                delete global.dataMetaWise[d];
            });
            if(tempDeleteMetadata.length){
                tempDeleteMetadata.forEach(function (d) {
                    delete ClientRegister[d];
                });
                return true;
            }else{
                return false;
            }

        },
        deleteClinetSessionWise:function(){
           console.log("deleteClinetSessionWise");
        }
     }
})();
module.exports=ClientRegistry;
