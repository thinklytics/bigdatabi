// ===============================================================================================================
//
//                                             CROSSFILTER MODULE
//
// ===============================================================================================================
//Change parseFormula function  so @@_ so make possiable so make issue for other functions
var funcD = require('../DataProcessor/CalculateFunctionDefination');
var $ = require('underscore');
var d3 = require('d3');
var $ = require('underscore');
var TooltipModule=require('../DataProcessor/TooltipModule');
var moment = require('moment');
var FixedCalculationModule = require('../DataProcessor/CalculationModule/FixedCalculationModule');
var AggregateModule = require('../DataProcessor/CalculationModule/AggregateModule');
var AverageModule = require('../ClientUtility/FiltersModule/AverageModule');
var SumModule = require('../ClientUtility/FiltersModule/SumModule');
var MinMaxModule = require('../ClientUtility/FiltersModule/MinMaxModule');
var CountModule = require('../ClientUtility/FiltersModule/CountModule');
var DistinctModule = require('../ClientUtility/FiltersModule/DistinctModule');
var HelperModule=require('./FiltersModule/HelperModule');
(function () {
    function crossfilterUtility(_crossfilter) {
        // THIS VARIABLE HOLDS THE EXPRESSION AFTER REDUCE
        var _abstractExpression = {};
        var funcDef = funcD();

        function snap_to_zero(source_group) {
            return {
                all: function () {
                    return source_group.all().map(function (d) {
                        return {
                            key: d.key,
                            value: (Math.abs(d.value) < 1e-6) ? 0 : d.value
                        };
                    });
                }
            };
        };
        //.............................................................................................................................................

        var _dataGroupsParams = {};
        function hasCalGroup(formula) {
            const regex = /(?:sum|SUM|count|COUNT|avg|AVG)\(\[([\w+|\(|\)|\s]+)\]\)/gm;
            var reducedFormula = formula;
            var runFormula = formula;
            var m;
            while ((m = regex.exec(formula)) !== null) {
                return true;
            }
        }

        var isAggregateField = function (formula) {
            var flag = !$.isEmpty((parseFormula(formula)).params);
            return flag;
        };


        var reduceMultilevelFormula = function (formula, client) {
            const regex = /(?:sum|SUM|count|COUNT|avg|AVG)\(\[([\w|' ']*)\]\)/g;
            var reducedFormula = formula;
            var runFormula = formula;
            var m;
            while ((m = regex.exec(formula)) !== null) {
                try {
                    var fieldObject = client.getTableColumn()[m[1]];
                    if (fieldObject.type == "custom" && isAggregateField(fieldObject.formula)) {
                        reducedFormula = reducedFormula.replace(m[0], "(" + fieldObject.formula + ")", client);
                    }

                } catch (e) {
                    return reducedFormula;
                }
            }

            if (hasMultiLevelCalculation(reducedFormula, client)) {
                reducedFormula = reduceMultilevelFormula(reducedFormula, client);
            } else {
                const checkDivide = /\(?(?:sum|SUM|count|COUNT|avg|AVG)?\(?\[[\w|' ']*\]\)?\)\/\(?(?:sum|SUM|count|COUNT|avg|AVG)?\(?\[[\w|' ']*\]\)?\)/g;
                var o;
                var i = 1;
                var replacements = {};
                while ((o = checkDivide.exec(reducedFormula)) !== null) {
                    replacements[o[0]] = "(isNaN(" + o[0] + ")?0:" + o[0] + ")";
                    i++;
                }
                $.each(replacements, function (key, val) {
                    reducedFormula = reducedFormula.replace(key, val);
                });
                return reducedFormula;
            }
            return reducedFormula;
        };
        var hasMultiLevelCalculation = function (formula, client) {
            var flag = false;
            const regex = /\[([\w|' ']*)\]/g;
            //const regex = /(sum|SUM|count|COUNT|avg|AVG)\(\[([a-z|A-Z|0-9|_|' '|'('|')']*)\]\)/g;
            var m;
            while ((m = regex.exec(formula)) !== null) {
                try {
                    if (m[1]) {
                        var tableColumns = client.getTableColumn();
                        var fieldObject = tableColumns[m[1]];
                        if (fieldObject.type == "custom" && isAggregateField(fieldObject.formula)) {
                            flag = true;
                        }
                    }
                } catch (e) {
                    return false;
                }
            }
            return flag;
        };
        var hasMultiLevelCalculationNumber = function (formula, client) {
            var flag = false;
            const regex = /\[([\w|' ']*)\]/g;
            //const regex = /(sum|SUM|count|COUNT|avg|AVG)\(\[([a-z|A-Z|0-9|_|' '|'('|')']*)\]\)/g;
            var m;
            while ((m = regex.exec(formula)) !== null) {
             //   console.log(m);
                try {
                    if (m[1]) {
                        var tableColumns = client.getTableColumn();
                        var fieldObject = tableColumns[m[1]];
                        if (fieldObject.type == "custom") {
                            flag = true;
                        }
                    }
                } catch (e) {
                    return false;
                }
            }
            return flag;
        };
        var reduceMultilevelFormulaNumber = function (formula, client) {
            const regex = /(?:sum|SUM|count|COUNT|avg|AVG)|\[([\w|\' \']*)\]/g;
            var reducedFormula = formula;
            var runFormula = formula;
            var m;
            while ((m = regex.exec(formula)) !== null) {
                try {
                    var fieldObject = client.getTableColumn()[m[1]];
                    if (fieldObject.type == "custom") {
                        reducedFormula = reducedFormula.replace(m[0], "(" + fieldObject.formula + ")", client);
                    }

                } catch (e) {
                    return false;
                }
            }

            if (hasMultiLevelCalculationNumber(reducedFormula, client)) {
                reducedFormula = reduceMultilevelFormulaNumber(reducedFormula, client);
            } else {
                const checkDivide = /\(?(?:sum|SUM|count|COUNT|avg|AVG)?\(?\[[\w|' ']*\]\)?\)\/\(?(?:sum|SUM|count|COUNT|avg|AVG)?\(?\[[\w|' ']*\]\)?\)/g;
                var o;
                var i = 1;
                var replacements = {};
                while ((o = checkDivide.exec(reducedFormula)) !== null) {
                    replacements[o[0]] = "(isNaN(" + o[0] + ")?0:" + o[0] + ")";
                    i++;
                }
                $.each(replacements, function (key, val) {
                    reducedFormula = reducedFormula.replace(key, val);
                });
                return reducedFormula;
            }
            return reducedFormula;
        };

        /*
         * For dcount
         */
        var hasMultiLevelCalculationForDcount = function (formula, client) {
            var flag = false;
            const regex = /\[([\w|' ']*)\]/g;
            //const regex = /(sum|SUM|count|COUNT|avg|AVG)\(\[([a-z|A-Z|0-9|_|' '|'('|')']*)\]\)/g;
            var m;
            while ((m = regex.exec(formula)) !== null) {
                try {
                    if (m[1]) {
                        var tableColumns = client.getTableColumn();
                        var fieldObject = tableColumns[m[1]];
                        if (fieldObject.type == "custom") {
                            flag = true;
                        }
                    }
                } catch (e) {
                    return false;
                }
            }
            return flag;
        };
        var isAggregateFieldForDcount=function (formula) {
            var flag = !$.isEmpty((parseFormulaForDcount(formula)).params);
            return flag;
        }
        var parseFormulaForDcount=function (formula) {
            const regex = /(sum|SUM|count|COUNT|avg|AVG)\(\[([a-z|A-Z|0-9|_|' '|'('|')']*)\]\)/g;
            const str = expression;
            var params = {};
            var alteredFormula = expression;
            while ((m = regex.exec(str)) !== null) {
                if (m.index === regex.lastIndex) {
                    regex.lastIndex++;
                }
                params[m[2]+"@@_"+m[1]] = m[1];
                alteredFormula = alteredFormula.replace(m[0], 'parseFloat(HelperModule.snap_to_zero_singleval(g[\'' + m[1] + '\'][\'' + m[2] + '\']))');
            }
            return {formula: alteredFormula, params: params};
        }
        var reduceMultilevelFormulaForDcount = function (formula, client) {
            //const regex = /(?:sum|SUM|count|COUNT|avg|AVG)\(\[([\w|' ']*)\]\)/g;
            const regex = /(?:count|avg|sum)[(]\[([\w|\' \'()]*)\][)]|\[([\w|\' \'()]*)\]/g;
            var reducedFormula = formula;
            var runFormula = formula;
            var m;
            while ((m = regex.exec(formula)) !== null) {
                try {
                    var fieldObject = client.getTableColumn()[m[2]];
                    if (fieldObject.type == "custom") {
                        reducedFormula = reducedFormula.replace(m[0], "(" + fieldObject.formula + ")", client);
                    }
                } catch (e) {
                    return reducedFormula;
                }
            }
            if (hasMultiLevelCalculationForDcount(reducedFormula, client)) {
                reducedFormula = reduceMultilevelFormulaForDcount(reducedFormula, client);
            }else {
                const checkDivide = /\(?(?:sum|SUM|count|COUNT|avg|AVG)?\(?\[[\w|' ']*\]\)?\)\/\(?(?:sum|SUM|count|COUNT|avg|AVG)?\(?\[[\w|' ']*\]\)?\)/g;
                var o;
                var i = 1;
                var replacements = {};
                while ((o = checkDivide.exec(reducedFormula)) !== null) {
                    replacements[o[0]] = "(isNaN(" + o[0] + ")?0:" + o[0] + ")";
                    i++;
                }
                $.each(replacements, function (key, val) {
                    reducedFormula = reducedFormula.replace(key, val);
                });
                return reducedFormula;
            }
            return reducedFormula;
        };
        /*
         * End dcount
         */

        var isCalculated = function (formula, client) {
            var flag = hasMultiLevelCalculation(formula, client) || isAggregateField(formula);
            return flag;
        };
        /*
         * This for same name mutiple tooltip
         */
        var parseFormula = function (expression) {
            const regex = /(sum|SUM|count|COUNT|avg|AVG)\(\[([a-z|A-Z|0-9|_|' '|'('|')']*)\]\)/g;
            const str = expression;
            var params = {};
            var alteredFormula = expression;
            while ((m = regex.exec(str)) !== null) {
                if (m.index === regex.lastIndex) {
                    regex.lastIndex++;
                }
                //params[m[2]+"@@_"+m[1]] = m[1];
                params[m[2]+"@@_"+m[1]] = m[1];
                alteredFormula = alteredFormula.replace(m[0], 'parseFloat(HelperModule.snap_to_zero_singleval(g[\'' + m[1] + '\'][\'' + m[2] + '\']))');
            }
            return {formula: alteredFormula, params: params};
        };
        /*
         * This for charts
         */
        var parseFormulaCharts = function (expression) {
            const regex = /(sum|SUM|count|COUNT|avg|AVG)\(\[([a-z|A-Z|0-9|_|' '|'('|')']*)\]\)/g;
            const str = expression;
            var params = {};
            var alteredFormula = expression;
            while ((m = regex.exec(str)) !== null) {
                if (m.index === regex.lastIndex) {
                    regex.lastIndex++;
                }
                //params[m[2]+"@@_"+m[1]] = m[1];
                params[m[2]] = m[1];
                alteredFormula = alteredFormula.replace(m[0], 'parseFloat(HelperModule.snap_to_zero_singleval(g[\'' + m[1] + '\'][\'' + m[2] + '\']))');
            }
            return {formula: alteredFormula, params: params};
        };
        function applyDataGroupOn(key, operator) {
            _dataGroupsParams[key] = operator;
        };

        function getDataGroupParams() {
            return _dataGroupsParams;
        };

        function processSimpleExpr(expr) {
            const regex = /\[([\w+|\(|\)|\s]+)\]/gm;
            var m;
            while ((m = regex.exec(expr)) !== null) {
                // This is necessary to avoid infinite loops with zero-width matches
                if (m.index === regex.lastIndex) {
                    regex.lastIndex++;
                }
                expr = expr.replace(m[0], "d['" + m[1] + "']");
                // The result can be accessed through the `m`-variable.
            }
            return expr;
        }

        function preProcessFunctions(expression) {
            const regex = /(sum|SUM|count|COUNT|avg|AVG)\(\[([a-z|A-Z|0-9|_|' '|'('|')']*)\]\)?/g;
            _dataGroupsParams = {};
            const str = expression;
            var flagGroup = false;
            _abstractExpression = expression;
            var m;
            while ((m = regex.exec(str)) !== null) {
                // This is necessary to avoid infinite loops with zero-width matches
                if (m.index === regex.lastIndex) {
                    regex.lastIndex++;
                }
                applyDataGroupOn(m[2], m[1]);
                _abstractExpression = _abstractExpression.replace(m[0], 'd[\'' + m[2] + '\']');
                flagGroup = true;
            }
            if (!flagGroup) {
                _dataGroupsParams = {};
            }
        };
        function preProcessToken(expr) {
            var expression = expr.replace(/isNull[(]/g,
                "funcDef.isEmpty(")
                .replace(/isNotNull[(]/g,
                    "funcDef.isNotEmpty(")
                .replace(/floor[(]/g,
                    "Math.floor(")
                .replace(/ceil[(]/g,
                    "ceil(")
                .replace(/decimalToInt[(]/g,
                    "parseInt(")
                .replace(/concat[(]/g,
                    "concatVar.concat(")
                .replace(/strlen[(]/g,
                    "funcDef.strlen(")
                .replace(/dateDiff[(]/g,
                    "funcDef.dateDiff(")
                .replace(/toLower[(]/g,
                    "funcDef.toLower(")
                .replace(/toString[(]/g,
                    "funcDef.toString(")
                .replace(/trim[(]/g,
                    "funcDef.trim(")
                .replace(/replace[(]/g,
                    "funcDef.replace(")
                .replace(/round[(]/g,
                    "funcDef.round(")
                .replace(/parseDate[(]/g,
                    "funcDef.parseDate(")
                .replace(/formatDate[(]/g,
                    "funcDef.formatDate(")
                .replace(/toUpper[(]/g,
                    "funcDef.toUpper(")
                .replace(/DateAdd[(]/g,
                    "funcDef.DateAdd(")
                .replace(/return/g, "")
                .replace(/currDate[(]/g, "funcDef.currentDate(")
                .replace(/\[/g, "funcDef.checkValue(d['")
                .replace(/dateTimeDiff[(]/g, "funcDef.dateTimeDiff(")
                .replace(/\]/g, "'])");
            return expression.trim();
        }

        var _total = 0;
        return {
            createDimension: function (object, dateFormat, customFormat) {
                var columnName = object.columnName;
                if (object.key == 'date' || object.key == 'datetime') {
                    if (dateFormat) {
                        return _crossfilter.dimension(function (d) {
                            var returnVal="Not Known";
                            if (dateFormat == "custom" && customFormat) {
                                if (object.dateFormat) {
                                    returnVal=  moment((d[columnName]), object.dateFormat).format(customFormat);
                                }
                                returnVal= moment(d[columnName]).format(customFormat);
                            }else {
                                if (object.dateFormat) {
                                    returnVal= moment(d[columnName], object.dateFormat);
                                }
                                returnVal= moment(d[columnName]).format(dateFormat);
                            }
                            if(!returnVal)
                                return "Something Broke";
                            return returnVal;
                        });
                    }
                    else {
                        return _crossfilter.dimension(function (d) {
                            if (d[columnName])
                                return moment(d[columnName]);
                            else
                                return null;
                        });
                    }
                }
                return _crossfilter.dimension(function (d) {
                    return d[columnName] || "";
                });
            },
            createGroup: function (dimension, measureObj, aggregates, groupColor, dateFormat, customTooltip, client, dimensionObj) {
                var measureColumnName = measureObj.columnName;
                var aggregate;
                if (aggregates)
                    aggregate = (aggregates[measureColumnName]).key;
                else {
                    aggregate = "sumIndex";
                }
                /*
                 * For dcount and aggr
                 */

                try{
                    var flagDcount=true;
                    if(hasMultiLevelCalculationForDcount(measureObj.formula,client)){
                        var reduceFormula=reduceMultilevelFormulaForDcount(measureObj.formula,client)
                        if(reduceFormula.indexOf("dcount")!=-1 || reduceFormula.indexOf("AGGR")!=-1){
                            flagDcount=false;
                            measureObj.formula=reduceFormula;
                        }
                    }
                    if (hasMultiLevelCalculation(measureObj.formula, client) && flagDcount) {
                        measureObj.formula = reduceMultilevelFormula(measureObj.formula, client);
                    }
                    if(measureObj.formula && (measureObj.formula.indexOf("dcount")!=-1 || measureObj.formula.indexOf("AGGR")!=-1)){
                        if(measureObj.formula && measureObj.formula.indexOf("dcount")!=-1){
                            //Create calculated column for dcount
                            measureObj.formula=this.createDataForDcount(measureObj,dimension,dimensionObj);
                        }
                        if(measureObj.formula && measureObj.formula.indexOf("AGGR")!=-1){
                            //Create calculated column for aggr
                            measureObj.formula=this.createDataForAggr(measureObj,dimension,dimensionObj);
                        }
                        if(!groupColor && !customTooltip) {
                            return this.dcountAggrNormalGrp(dimension, measureObj, groupColor, dateFormat, client, dimensionObj,customTooltip,aggregate);
                        }else if(!groupColor && customTooltip) {
                            return this.dcountAggrWithTooltip(dimension, measureObj, groupColor, dateFormat, client, dimensionObj,customTooltip,aggregate);
                        }else if(groupColor && !customTooltip){
                            return this.dcountAggrWithGrpColor(dimension, measureObj, groupColor, dateFormat, client, dimensionObj,customTooltip,aggregate);
                        }else if(groupColor && customTooltip){
                            return this.dcountAggrWithGrpColorTooltip(dimension, measureObj, groupColor, dateFormat, client, dimensionObj,customTooltip,aggregate);
                        }
                    }
                }catch (e){
                    console.log(e);
                }
                /*
                 * End dount aggr
                 */
                if (measureObj.formula && isCalculated(measureObj.formula, client)) {
                    aggregate = "calculated";
                }
                if (measureObj.formula && client!=undefined) {
                    if(measureObj.formula.indexOf("DimensionSum") != -1){
                        if(!groupColor && !customTooltip) {
                            return this.customDimensionSumGroup(dimension, measureObj,groupColor, dateFormat , client, dimensionObj);
                        }else if(!groupColor && customTooltip) {
                            return this.customDimensionSumGroupWithTooltip(dimension, measureObj,customTooltip, client, dimensionObj,dateFormat);
                        }else if(groupColor && !customTooltip){
                            return this.customDimensionSumGroupColor(dimension, measureObj,groupColor, client, dimensionObj,dateFormat);
                        }else if(groupColor && customTooltip){
                            return this.customDimensionSumGroupColorWithTooltip(dimension, measureObj,groupColor, client, dimensionObj,dateFormat,customTooltip);
                        }
                    }
                    if (FixedCalculationModule.hasFixedCalculation(measureObj.formula)) {
                        return FixedCalculationModule.createGroup(dimension, measureObj, groupColor,customTooltip, client);
                    }
                }
                var customFlag = true;
                if (customTooltip) {
                    var flag = false;
                    var grp;
                    if (customTooltip.datakeys)
                        customTooltip.datakeys.forEach(function (d, index) {
                            if (FixedCalculationModule.hasFixedCalculation(d.formula)) {
                                grp = FixedCalculationModule.createGroupWithFixedTooltip(dimension, measureObj, groupColor, customTooltip, client);
                                flag = true;
                            }
                        });
                    if (flag)
                        return grp;
                }
                /*
                    Min and max calculation
                 */
                if(measureObj.formula && AggregateModule.isAggrExist(measureObj.formula)){
                    return MinMaxModule.processMinMax(dimension, measureObj, client, dimensionObj, groupColor, dateFormat, customTooltip);
                }
                switch (aggregate) {
                    case 'sumIndex':
                        return this.sumGroup(dimension, measureObj, groupColor, dateFormat, customTooltip, client);
                    case 'count':
                        return this.countGroup(dimension, measureObj, groupColor, dateFormat, customTooltip, client);
                    case 'avgIndex':
                        return this.avgGroup(dimension, measureObj, groupColor, dateFormat, customTooltip, client);
                    case 'runTotal':
                        return this.sumGroup(dimension, measureObj, groupColor, dateFormat, customTooltip, client);
                    case 'countDistinct':
                        return this.countDistinctGroup(dimension, measureObj, groupColor, dateFormat, customTooltip, client,dimensionObj);
                    case 'totalDistinct':
                        return this.totalDistinctGroup(dimension, measureObj, groupColor, dateFormat, customTooltip, client,dimensionObj);
                    case 'calculated':
                        return this.calculatedGroup(dimension, measureObj, aggregates, groupColor, dateFormat, customTooltip, client);
               }
            },
            dcountAggrNormalGrp: function (dimension, measureObj,groupColor, dateFormat , client, dimensionObj,customTooltip) {
                var group = {};
                group['all'] = function () {
                    var _data = dimension.top(Infinity);
                    //var dimensionVal=moment(d[dimensionObj.columnName], object.dateFormat)
                    var newData = [];
                    /*
                     * If condition check
                     */
                    var expr = measureObj.formula;
                    try{
                        //if([a-z|A-Z|0-9|=|.|,|-|*|\/|!|%|{|}| |_|(|)|[\]]*)else{[\w]:?[=|.|,|-|*|\/|!|%|{|}| |+|_|(|)|[\]]*}
                        const regex = /if([a-z|A-Z|0-9|=|.|,|-|*|\/|!|%|{|}| |_|(|)|[\]]*)else{[\w]:?[=|.|,|-|*|\/|!|%|{|}| |+|_|(|)|[\]]*}/gm;
                        var m;
                        while ((m = regex.exec(expr)) !== null) {
                            if (m.index === regex.lastIndex)
                                regex.lastIndex++;
                            expr=expr.replace(m[0],'eval("'+m[0]+'")');
                        }
                    }catch (e){
                        console.log(e);
                    }
                    var expr = preProcessToken(expr);
                    _data.forEach(function (d) {
                        var val=eval(expr);
                        if (isNaN(val) && val == null || isNaN(val) || val == 'NaN' || val == undefined) {
                            val = 0;
                        } else {
                            val = parseFloat(val);
                            //val= val.toFixed(2)
                        }
                        d[measureObj['columnName']] = val;
                    });
                    var keyObj = {};
                    _data.forEach(function (d) {
                        var dimensionVal=d[dimensionObj.columnName];
                        /*
                         * Date format
                         */
                        if(dateFormat){
                            dimensionVal=moment(d[dimensionObj.columnName]).format(dateFormat);
                        }else if(dimensionObj.columType=='date' || dimensionObj.columType=='datetime'){
                            var dimensionDate = new Date(d[dimensionObj.columnName]);
                            dimensionVal=dimensionDate.toISOString();
                        }
                        if (!keyObj[dimensionVal])
                            keyObj[dimensionVal] = {
                                count: 0,
                                sumIndex: 0,
                                avgIndex: 0,
                                customTooltip:true
                            };
                        var val = parseFloat(d[measureObj.columnName]);
                        if(val){
                            ++keyObj[dimensionVal].count;
                        }
                        keyObj[dimensionVal].sumIndex += val;
                        if(keyObj[dimensionVal].count)
                            keyObj[dimensionVal].avgIndex = keyObj[dimensionVal].sumIndex/keyObj[dimensionVal].count;
                        else
                            keyObj[dimensionVal].avgIndex = 0;
                    });
                    $.each(keyObj, function (val, key) {
                        newData.push({key: key, value: val});
                    });
                    newData = newData.sort(function (a, b) {
                        return a.key === b.key ? 0 : a.key < b.key ? -1 : 1;
                    });
                    return newData;
                }
                return group;
            },
            dcountAggrWithTooltip:function (dimension, measureObj,groupColor, dateFormat , client, dimensionObj,tooltip) {
                var group = {};
                var processTooltipText = TooltipModule.processTooltipText;
                group['all'] = function () {
                    var _data = dimension.top(Infinity);
                    var newData = [];
                    /*
                     * If condition check
                     */
                    var expr = measureObj.formula;
                    try{
                        //if([a-z|A-Z|0-9|=|.|,|-|*|\/|!|%|{|}| |_|(|)|[\]]*)else{[\w]:?[=|.|,|-|*|\/|!|%|{|}| |+|_|(|)|[\]]*}
                        const regex = /if([a-z|A-Z|0-9|=|.|,|-|*|\/|!|%|{|}| |_|(|)|[\]]*)else{[\w]:?[=|.|,|-|*|\/|!|%|{|}| |+|_|(|)|[\]]*}/gm;
                        var m;
                        while ((m = regex.exec(expr)) !== null) {
                            if (m.index === regex.lastIndex)
                                regex.lastIndex++;
                            expr=expr.replace(m[0],'eval("'+m[0]+'")');
                        }
                    }catch (e){
                        console.log(e);
                    }
                    var expr = preProcessToken(expr);
                    _data.forEach(function (d) {
                        var val=eval(expr);
                        if (isNaN(val) && val == null || isNaN(val) || val == 'NaN' || val == undefined) {

                            val = 0;
                        } else {
                            val = parseFloat(val);
                            //val= val.toFixed(2)
                        }
                        d[measureObj['columnName']] = val;
                    });
                    var keyObj = {};
                    _data.forEach(function (p) {
                        /*
                         * Date format
                         */
                        var dimensionVal=p[dimensionObj.columnName];
                        if(dateFormat){
                            dimensionVal=moment(p[dimensionObj.columnName]).format(dateFormat);
                        }else if(dimensionObj.columType=='date' || dimensionObj.columType=='datetime'){
                            var dimensionDate = new Date(p[dimensionObj.columnName]);
                            dimensionVal=dimensionDate.toISOString();
                        }
                        if (!keyObj[dimensionVal])
                            keyObj[dimensionVal] = {
                                count: 0,
                                sumIndex: 0,
                                avgIndex: 0,
                                customTooltip:true
                            };
                        var val=parseFloat(p[measureObj.columnName]);
                        if (tooltip.datakeys) {
                            tooltip.datakeys.forEach(function (d) {
                                if (d.dataKey == "Measure") {
                                    processTooltipText(d, keyObj[dimensionVal], p);
                                } else {
                                    if (!(keyObj[dimensionVal]["<" + d.reName + ">"])) {
                                        keyObj[dimensionVal]["<" + d.reName + ">"] = p[d.columnName];
                                    }
                                    else if (keyObj[dimensionVal]["<" + d.reName + ">"] != p[d.columnName]) {
                                        keyObj[dimensionVal]["<" + d.reName + ">"] = "Multiple Values..";
                                    }
                                }
                            });
                        }
                        if(val){
                            ++keyObj[dimensionVal].count;
                        }
                        keyObj[dimensionVal].sumIndex += val;
                        if(keyObj[dimensionVal].count)
                            keyObj[dimensionVal].avgIndex = keyObj[dimensionVal].sumIndex/keyObj[dimensionVal].count;
                        else
                            keyObj[dimensionVal].avgIndex=0;
                    });
                    $.each(keyObj, function (val, key) {
                        newData.push({key: key, value: val});
                    });
                    newData = newData.sort(function (a, b) {
                        return a.key === b.key ? 0 : a.key < b.key ? -1 : 1;
                    });
                    return newData;
                }
                return group;
            },
            dcountAggrWithGrpColor:function (dimension, measureObj,groupColor, dateFormat , client, dimensionObj,customTooltip,aggregate) {
                var group = {};
                var processTooltipText = TooltipModule.processTooltipText;
                var groupColorName=groupColor.columnName;
                group['all'] = function () {
                    var _data = dimension.top(Infinity);
                    var newData = [];
                    /*
                     * If condition check
                     */
                    var expr = measureObj.formula;
                    try{
                        //if([a-z|A-Z|0-9|=|.|,|-|*|\/|!|%|{|}| |_|(|)|[\]]*)else{[\w]:?[=|.|,|-|*|\/|!|%|{|}| |+|_|(|)|[\]]*}
                        const regex = /if([a-z|A-Z|0-9|=|.|,|-|*|\/|!|%|{|}| |_|(|)|[\]]*)else{[\w]:?[=|.|,|-|*|\/|!|%|{|}| |+|_|(|)|[\]]*}/gm;
                        var m;
                        while ((m = regex.exec(expr)) !== null) {
                            if (m.index === regex.lastIndex)
                                regex.lastIndex++;
                            expr=expr.replace(m[0],'eval("'+m[0]+'")');
                        }
                    }catch (e){
                        console.log(e);
                    }
                    var expr = preProcessToken(expr);
                    _data.forEach(function (d) {
                        var val=eval(expr);
                        if (isNaN(val) && val == null || isNaN(val) || val == 'NaN' || val == undefined) {

                            val = 0;
                        } else {
                            val = parseFloat(val);
                            //val= val.toFixed(2)
                        }
                        d[measureObj['columnName']] = val;
                    });
                    var keyObj = {};
                    _data.forEach(function (p) {
                        /*
                         * Group color
                         */
                        var valTemp = (p[groupColorName]);
                        var dimensionVal=p[dimensionObj.columnName];
                        if(dateFormat){
                            dimensionVal=moment(p[dimensionObj.columnName]).format(dateFormat);
                        }else if(dimensionObj.columType=='date' || dimensionObj.columType=='datetime'){
                            var dimensionDate = new Date(p[dimensionObj.columnName]);
                            dimensionVal=dimensionDate.toISOString();
                        }
                        if (!keyObj[dimensionVal])
                            keyObj[dimensionVal] = {
                                count: 0,
                                sumIndex: 0,
                                avgIndex: 0,
                                groupColor: {},
                                groupRepeatCheck: {},
                                sum: {},
                                avg: {}
                            };
                        var val=parseFloat(p[measureObj.columnName]);
                        keyObj[dimensionVal].sumIndex += val;
                        if(val){
                            keyObj[dimensionVal].count += 1;
                        }
                        if(keyObj[dimensionVal].count)
                            keyObj[dimensionVal].avgIndex = keyObj[dimensionVal].sumIndex/keyObj[dimensionVal].count;
                        else
                            keyObj[dimensionVal].avgIndex = 0;
                        if(keyObj[dimensionVal].groupRepeatCheck[valTemp]==undefined){
                            keyObj[dimensionVal].groupRepeatCheck[valTemp] = {
                                'sumIndex':0,
                                'avgIndex':0,
                                'count':0
                            };
                        }
                        if(val){
                            ++keyObj[dimensionVal].groupRepeatCheck[valTemp].count;
                        }
                        keyObj[dimensionVal].groupRepeatCheck[valTemp].sumIndex +=val;
                        if(keyObj[dimensionVal].groupRepeatCheck[valTemp].count){
                            keyObj[dimensionVal].groupRepeatCheck[valTemp].avgIndex =keyObj[dimensionVal].groupRepeatCheck[valTemp].sumIndex/keyObj[dimensionVal].groupRepeatCheck[valTemp].count;
                        }
                        $.each(keyObj[dimensionVal].groupRepeatCheck,function (val,key) {
                            if(aggregate=="avgIndex"){
                                keyObj[dimensionVal].groupColor[key] = val.avgIndex;
                            }else if(aggregate=="count"){
                                keyObj[dimensionVal].groupColor[key] = val.count;
                            }else{
                                keyObj[dimensionVal].groupColor[key] = val.sumIndex;
                            }
                        });
                    });
                    $.each(keyObj, function (val, key) {
                        newData.push({key: key, value: val});
                    });
                    newData = newData.sort(function (a, b) {
                        return a.key === b.key ? 0 : a.key < b.key ? -1 : 1;
                    });
                    return newData;
                }
                return group;
            },
            dcountAggrWithGrpColorTooltip:function (dimension, measureObj,groupColor, dateFormat , client, dimensionObj,customTooltip,aggregate) {
                var group = {};
                var processTooltipText = TooltipModule.processTooltipText;
                var groupColorName=groupColor.columnName;
                var processTooltipText = TooltipModule.processTooltipGrp;
                group['all'] = function () {
                    var _data = dimension.top(Infinity);
                    var newData = [];
                    /*
                     * If condition check
                     */
                    var expr = measureObj.formula;
                    try{
                        //if([a-z|A-Z|0-9|=|.|,|-|*|\/|!|%|{|}| |_|(|)|[\]]*)else{[\w]:?[=|.|,|-|*|\/|!|%|{|}| |+|_|(|)|[\]]*}
                        const regex = /if([a-z|A-Z|0-9|=|.|,|-|*|\/|!|%|{|}| |_|(|)|[\]]*)else{[\w]:?[=|.|,|-|*|\/|!|%|{|}| |+|_|(|)|[\]]*}/gm;
                        var m;
                        while ((m = regex.exec(expr)) !== null) {
                            if (m.index === regex.lastIndex)
                                regex.lastIndex++;
                            expr=expr.replace(m[0],'eval("'+m[0]+'")');
                        }
                    }catch (e){
                        console.log(e);
                    }
                    var expr = preProcessToken(expr);
                    _data.forEach(function (d) {
                        var val=eval(expr);
                        if (isNaN(val) && val == null || isNaN(val) || val == 'NaN' || val == undefined) {

                            val = 0;
                        } else {
                            val = parseFloat(val);
                            //val= val.toFixed(2)
                        }
                        d[measureObj['columnName']] = val;
                    });
                    var keyObj = {};
                    _data.forEach(function (p) {
                        /*
                         * Group color
                         */
                        var valTemp = (p[groupColorName]);
                        var dimensionVal=p[dimensionObj.columnName];
                        if(dateFormat){
                            dimensionVal=moment(p[dimensionObj.columnName]).format(dateFormat);
                        }else if(dimensionObj.columType=='date' || dimensionObj.columType=='datetime'){
                            var dimensionDate = new Date(p[dimensionObj.columnName]);
                            dimensionVal=dimensionDate.toISOString();
                        }
                        if (!keyObj[dimensionVal])
                            keyObj[dimensionVal] = {
                                count: 0,
                                sumIndex: 0,
                                avgIndex: 0,
                                groupColor: {},
                                groupRepeatCheck: {},
                                tooltipObj:{},
                                sum: {},
                                avg: {}
                            };
                        var val=parseFloat(p[measureObj.columnName]);
                        keyObj[dimensionVal].sumIndex += val;
                        if(val){
                            keyObj[dimensionVal].count += 1;
                        }
                        if(keyObj[dimensionVal].count)
                            keyObj[dimensionVal].avgIndex = keyObj[dimensionVal].sumIndex/keyObj[dimensionVal].count;
                        else
                            keyObj[dimensionVal].avgIndex = 0;
                        if(keyObj[dimensionVal].groupRepeatCheck[valTemp]==undefined){
                            keyObj[dimensionVal].groupRepeatCheck[valTemp] = {
                                'sumIndex':0,
                                'avgIndex':0,
                                'count':0,
                                avgAggrTooltip:{
                                    sum:0,
                                    count:0
                                }
                            };
                        }
                        if(val){
                            ++keyObj[dimensionVal].groupRepeatCheck[valTemp].count;
                        }
                        keyObj[dimensionVal].groupRepeatCheck[valTemp].sumIndex +=val;
                        if(keyObj[dimensionVal].groupRepeatCheck[valTemp].count){
                            keyObj[dimensionVal].groupRepeatCheck[valTemp].avgIndex =keyObj[dimensionVal].groupRepeatCheck[valTemp].sumIndex/keyObj[dimensionVal].groupRepeatCheck[valTemp].count;
                        }
                        $.each(keyObj[dimensionVal].groupRepeatCheck,function (val,key) {
                            if(aggregate=="avgIndex"){
                                keyObj[dimensionVal].groupColor[key] = val.avgIndex;
                            }else if(aggregate=="count"){
                                keyObj[dimensionVal].groupColor[key] = val.count;
                            }else{
                                keyObj[dimensionVal].groupColor[key] = val.sumIndex;
                            }
                        });
                        if (customTooltip.datakeys) {
                            customTooltip.datakeys.forEach(function (d) {
                                if (d.dataKey == "Measure") {
                                    if(keyObj[dimensionVal].tooltipObj[d.reName+"@@!"+valTemp+"@@#"+d.aggregate]){
                                        keyObj[dimensionVal].tooltipObj[d.reName+"@@!"+valTemp+"@@#"+d.aggregate]=0;
                                    }
                                    if(d.aggregate=="avg"){
                                        var avgObj=processTooltipText(d, keyObj[dimensionVal], p)
                                        keyObj.avgAggrTooltip.sum += avgObj.sum;
                                        keyObj.avgAggrTooltip.count += avgObj.count;
                                        keyObj.tooltipObj[d.reName+"@@!"+valTemp+"@@#"+d.aggregate] = keyObj.avgAggrTooltip.sum/keyObj.avgAggrTooltip.count;
                                    }else{
                                        keyObj[dimensionVal].tooltipObj[d.reName+"@@!"+valTemp+"@@#"+d.aggregate] += processTooltipText(d, keyObj[dimensionVal], p);
                                    }
                                } else {
                                    if (!(keyObj[dimensionVal]["<" + d.reName + ">"])) {
                                        keyObj[dimensionVal].tooltipObj[d.reName+"@@!"+valTemp] = p[d.columnName];
                                    }
                                    else if (keyObj[dimensionVal]["<" + d.reName + ">"] != p[d.columnName]) {
                                        keyObj[dimensionVal].tooltipObj[d.reName+"@@!"+valTemp] = "Multiple Values..";
                                    }
                                }
                            });
                        }
                    });
                    $.each(keyObj, function (val, key) {
                        newData.push({key: key, value: val});
                    });
                    newData = newData.sort(function (a, b) {
                        return a.key === b.key ? 0 : a.key < b.key ? -1 : 1;
                    });
                    return newData;
                }
                return group;
            },
            createDataForDcount :function (measureObj,dimension,dimensionObj) {
                var executingExpression=measureObj.formula;
                var activeDimension=dimensionObj.columnName;
                var columnName=measureObj.columnName;
                try {
                    var grp={};
                    //(dcount)\(\['([([a-z|A-Z|0-9|_| |]*)\'\]\)
                    const regex = /(dcount)\(\[([([a-z|A-Z|0-9|_| |()]*)\]\)/g;
                    var _data = dimension.top(Infinity);
                    var i=0;
                    while ((m = regex.exec(executingExpression)) !== null) {
                        /*if(measureObj.formulaObj && measureObj.formulaObj.length){
                            measureObj.formulaObj.forEach(function (dObj) {
                                if(dObj.formula==m[0]){
                                    columnName=dObj.columnName;
                                }
                            });
                        }*/
                        _data.forEach(function (d) {
                            var newExp=0;
                            if(grp[d[m[2]]+""+d[activeDimension[0]]]==undefined){
                                newExp=1;
                                grp[d[m[2]]+""+d[activeDimension[0]]]=true;
                            }else{
                                newExp=0;
                            }
                            var res = newExp;
                            if (res != null)
                                d[columnName] = res;
                        });
                        executingExpression=executingExpression.replace(m[0],"["+columnName+"]");
                    };
                    return executingExpression;
                } catch (e) {
                    console.log(e);
                }
            },
            createDataForAggr:function (measureObj,dimension,dimensionObj) {
                var executingExpression=measureObj.formula;
                var columnName=measureObj.columnName;
                var activeDimension=dimensionObj.columnName;
                try {
                    var grp={};
                    //(sum|SUM|count|COUNT|avg|AVG|min|max|dcount|DCOUNT|AGGR)[(]|(funcDef.checkValue\(d\[')([a-z|A-Z|0-9|_| |(|).|]*)[']]\)*|,
                    //const regex = /(sum|SUM|count|COUNT|avg|AVG|min|max|Aggr|aggr|AGGR)[(]|(funcDef.checkValue\(d\[')([a-z|A-Z|0-9|_| |(|).|]*)[']]\)*|,/gm;
                    const regex = /(AGGR)\(\[([([a-z|A-Z|0-9|_| |()]*)\]\)*, *(count|sum|avg|min|max)\(\[([([a-z|A-Z|0-9|_| |()]*)\]\)*,(\[([([a-z|A-Z|0-9|_| |()]*)\])\)/g;
                    var _data = dimension.top(Infinity);
                    var tempObj={};
                    while ((m = regex.exec(executingExpression)) !== null) {
                        if(measureObj.formulaObj && measureObj.formulaObj.length){
                            measureObj.formulaObj.forEach(function (dObj) {
                                if(dObj.formula==m[0]){
                                    columnName=dObj.columnName;
                                }
                            });
                        }
                       // if(_data[0][columnName]==undefined){
                           var dimGrp={};
                            _data.forEach(function (d) {
                                var newExp = 0;
                                if (m[3] == 'count') {
                                    if (grp[d[m[2]] + "" +d[activeDimension] + "" + d[m[6]]] == undefined) {
                                        newExp = 1;
                                        grp[d[m[2]] + "" +d[activeDimension] + "" + d[m[6]]] = true;
                                    } else {
                                        newExp = 0;
                                    }
                                } else if (m[3] == 'sum' || m[3] == 'avg') {
                                    if (grp[d[m[2]] + "" +d[activeDimension] + "" + d[m[6]]] == undefined) {
                                        newExp = d[m[4]];
                                        grp[d[m[2]] + "" +d[activeDimension] + "" + d[m[6]]] = true;
                                        if (dimGrp[d[activeDimension]] == undefined) {
                                            dimGrp[d[activeDimension]] = 0;
                                        }
                                        dimGrp[d[activeDimension]]++;
                                    } else {
                                        newExp = 0;
                                    }
                                }
                                var res = newExp;
                                if (res != null){
                                    d[columnName] = res;
                                }
                            });
                            _data.forEach(function (d) {
                                if (m[3] == 'avg') {
                                    var avgData=0;
                                    if(d[columnName]){

                                        avgData=parseFloat(d[columnName])/parseFloat(dimGrp[d[activeDimension]]);
                                    }
                                    if (avgData != null){
                                        d[columnName] = avgData;
                                    }
                                }
                            });
                       // }
                        if(m[3]=='avg'){
                            tempObj["avg(["+columnName+"])"]=m[0];
                        }else{
                            tempObj["["+columnName+"]"]=m[0];
                        }

                    }
                    $.each(tempObj,function (value,key) {
                        executingExpression=executingExpression.replace(value,key);
                    });
                    return executingExpression;
                } catch (e) {
                    console.log(e);
                }
            },
            numberWidgetMinMax:function (measureObj,client,dimension) {

                return {
                    value: function () {
                        const regex = /(min|max)\(\[([a-z|A-Z|0-9|_|' '|'('|')']*)\]\)?/g;
                        var expression=measureObj.formula
                        var str = measureObj.formula;
                        var flagGroup = false;
                        var tableColumn = client.getTableColumn();
                        var _data=dimension.top(Infinity);
                        var minGroup;
                        var maxGroup;
                        var strGrp=[];
                        var i=0;
                        while ((m = regex.exec(expression)) !== null) {
                            _data.forEach(function (d, index) {
                                if(d[m[2]]){
                                    if (!maxGroup) {
                                        maxGroup = parseInt(d[m[2]]);
                                    }
                                    if (!minGroup) {
                                        minGroup = parseInt(d[m[2]]);
                                    }
                                    if (parseInt(d[m[2]]) > maxGroup) {
                                        maxGroup = d[m[2]]
                                    }
                                    else if (parseInt(d[m[2]]) < minGroup) {
                                        minGroup = d[m[2]]
                                    }
                                }
                            });
                            if (m[1] == 'min') {
                                str = str.replace(m[0], "minMaxGrp_"+i);
                                strGrp.push(minGroup);
                            }
                            if (m[1] == 'max') {
                                str = str.replace(m[0], "minMaxGrp_"+i);
                                strGrp.push(maxGroup);
                            }
                            if (m.index === regex.lastIndex) {
                                regex.lastIndex++;
                            }
                            i++;
                        };
                        str=preProcessToken(str);
                        strGrp.forEach(function (d,index) {
                            str=str.replace("minMaxGrp_"+index,d);
                        });
                        var numberSum=0;
                        var isValued={};
                        _data.forEach(function (d) {
                                numberSum +=eval(str);
                        });
                        var data={};
                        data['sumIndex']=(Math.abs(numberSum)<1e-6) ? 0 : numberSum
                        return data;
                    }
                }
            },
            /*customMinMax: function (dimension, measureObj, client, dimensionObj) {

            },*/
            customDimensionSumGroupColor:function (dimension, measureObj,groupColor, client, dimensionObj,dateFormat) {
                var processExpression = this.processDimensionStatements;
                var group = {};
                var processTooltipText = TooltipModule.processTooltipText;
                var groupColorName=groupColor.columnName;
                group['all'] = function () {
                    var _data = dimension.top(Infinity);
                    var newData = [];
                    var expr = processExpression(measureObj.formula, _data, client, dimensionObj);
                    expr = processSimpleExpr(expr);
                    _data.forEach(function (d) {
                        d[measureObj['columnName']] = eval(expr);
                    });
                    var keyObj = {};
                    _data.forEach(function (p) {
                        /*
                         * Group color
                         */
                        var valTemp = (p[groupColorName]);
                        var dimensionVal=p[dimensionObj.columnName];
                        if(dateFormat){
                            dimensionVal=moment(p[dimensionObj.columnName]).format(dateFormat);
                        }else if(dimensionObj.columType=='date' || dimensionObj.columType=='datetime'){
                            var dimensionDate = new Date(p[dimensionObj.columnName]);
                            dimensionVal=dimensionDate.toISOString();
                        }
                        if (!keyObj[dimensionVal])
                            keyObj[dimensionVal] = {
                                count: 0,
                                sumIndex: 0,
                                avgIndex: 0,
                                groupColor: {},
                                groupRepeatCheck: {},
                                sum: {},
                                avg: {}
                            };
                        var val=parseFloat(p[measureObj.columnName]);
                        keyObj[dimensionVal].sumIndex += val;
                        keyObj[dimensionVal].count += 1;
                        keyObj[dimensionVal].avgIndex += keyObj[dimensionVal].sumIndex/keyObj[dimensionVal].count;
                        if(keyObj[dimensionVal].groupRepeatCheck[valTemp]==undefined){
                            keyObj[dimensionVal].groupRepeatCheck[valTemp] = 0;
                        }
                        keyObj[dimensionVal].groupRepeatCheck[valTemp] += val;
                        keyObj[dimensionVal].groupColor = keyObj[dimensionVal].groupRepeatCheck;
                    });
                    $.each(keyObj, function (val, key) {
                        newData.push({key: key, value: val});
                    });
                    newData = newData.sort(function (a, b) {
                        return b.value.sumIndex - a.value.sumIndex;
                    });
                    return newData;
                }
                return group;
            },
            customDimensionSumGroupColorWithTooltip:function (dimension, measureObj,groupColor, client, dimensionObj,dateFormat,customTooltip) {
                var processExpression = this.processDimensionStatements;
                var group = {};
                var processTooltipText = TooltipModule.processTooltipText;
                var groupColorName=groupColor.columnName;
                var processTooltipText = TooltipModule.processTooltipGrp;
                group['all'] = function () {
                    var _data = dimension.top(Infinity);
                    var newData = [];
                    var expr = processExpression(measureObj.formula, _data, client, dimensionObj);
                    expr = processSimpleExpr(expr);
                    _data.forEach(function (d) {
                        d[measureObj['columnName']] = eval(expr);
                    });
                    var keyObj = {};
                    _data.forEach(function (p) {
                        /*
                         * Group color
                         */
                        var valTemp = (p[groupColorName]);
                        var dimensionVal=p[dimensionObj.columnName];
                        if(dateFormat){
                            dimensionVal=moment(p[dimensionObj.columnName]).format(dateFormat);
                        }else if(dimensionObj.columType=='date' || dimensionObj.columType=='datetime'){
                            var dimensionDate = new Date(p[dimensionObj.columnName]);
                            dimensionVal=dimensionDate.toISOString();
                        }
                        if (!keyObj[dimensionVal])
                            keyObj[dimensionVal] = {
                                count: 0,
                                sumIndex: 0,
                                avgIndex: 0,
                                groupColor: {},
                                groupRepeatCheck: {},
                                tooltipObj:{},
                                sum: {},
                                avg: {},
                                avgAggrTooltip:{
                                    sum:0,
                                    count:0
                                }
                            };
                        var val=parseFloat(p[measureObj.columnName]);
                        keyObj[dimensionVal].sumIndex += val;
                        keyObj[dimensionVal].count =keyObj[dimensionVal].sumIndex;
                        keyObj[dimensionVal].avgIndex = keyObj[dimensionVal].sumIndex;
                        if(keyObj[dimensionVal].groupRepeatCheck[valTemp]==undefined){
                            keyObj[dimensionVal].groupRepeatCheck[valTemp] = 0;
                        }
                        keyObj[dimensionVal].groupRepeatCheck[valTemp] += val;
                        keyObj[dimensionVal].groupColor = keyObj[dimensionVal].groupRepeatCheck;
                        if (customTooltip.datakeys) {
                            customTooltip.datakeys.forEach(function (d) {
                                if (d.dataKey == "Measure") {
                                    if(keyObj[dimensionVal].tooltipObj[d.reName+"@@!"+valTemp+"@@#"+d.aggregate]){
                                        keyObj[dimensionVal].tooltipObj[d.reName+"@@!"+valTemp+"@@#"+d.aggregate]=0;
                                    }
                                    if(d.aggregate=="avg"){
                                        var avgObj=processTooltipText(d, keyObj[dimensionVal], p)
                                        keyObj.avgAggrTooltip.sum += avgObj.sum;
                                        keyObj.avgAggrTooltip.count += avgObj.count;
                                        keyObj.tooltipObj[d.reName+"@@!"+valTemp+"@@#"+d.aggregate] = keyObj.avgAggrTooltip.sum/keyObj.avgAggrTooltip.count;
                                    }else{
                                        keyObj[dimensionVal].tooltipObj[d.reName+"@@!"+valTemp+"@@#"+d.aggregate] += processTooltipText(d, keyObj[dimensionVal], p);
                                    }
                                } else {
                                    if (!(keyObj[dimensionVal]["<" + d.reName + ">"])) {
                                        keyObj[dimensionVal].tooltipObj[d.reName+"@@!"+valTemp] = p[d.columnName];
                                    }
                                    else if (keyObj[dimensionVal]["<" + d.reName + ">"] != p[d.columnName]) {
                                        keyObj[dimensionVal].tooltipObj[d.reName+"@@!"+valTemp] = "Multiple Values..";
                                    }
                                }
                            });
                        }
                    });
                    $.each(keyObj, function (val, key) {
                        newData.push({key: key, value: val});
                    });
                    newData = newData.sort(function (a, b) {
                        return b.value.sumIndex - a.value.sumIndex
                    });
                    return newData;
                }
                return group;
            },
            customDimensionSumGroup: function (dimension, measureObj,groupColor, dateFormat , client, dimensionObj) {
                var processExpression = this.processDimensionStatements;
                var group = {};
                group['all'] = function () {
                    var _data = dimension.top(Infinity);
                    //var dimensionVal=moment(d[dimensionObj.columnName], object.dateFormat)
                    var newData = [];
                    var expr = processExpression(measureObj.formula, _data, client, dimensionObj);
                    expr = processSimpleExpr(expr);
                    _data.forEach(function (d) {
                        d[measureObj['columnName']] = eval(expr);
                    });
                    var keyObj = {};
                    _data.forEach(function (d) {
                        var dimensionVal=d[dimensionObj.columnName];
                        /*
                         * Date format
                         */
                        if(dateFormat){
                            dimensionVal=moment(d[dimensionObj.columnName]).format(dateFormat);
                        }else if(dimensionObj.columType=='date' || dimensionObj.columType=='datetime'){
                            var dimensionDate = new Date(d[dimensionObj.columnName]);
                            dimensionVal=dimensionDate.toISOString();
                        }
                        if (!keyObj[dimensionVal])
                            keyObj[dimensionVal] = 0;
                        keyObj[dimensionVal] += parseFloat(d[measureObj.columnName]);
                    });
                    $.each(keyObj, function (val, key) {
                        newData.push({key: key, value: val});
                    });
                    newData = newData.sort(function (a, b) {
                        return b.value - a.value
                    });
                    return newData;
                }
                return group;
            },
            customDimensionSumGroupWithTooltip: function (dimension, measureObj,tooltip, client, dimensionObj,dateFormat) {
                var processExpression = this.processDimensionStatements;
                var group = {};
                var processTooltipText = TooltipModule.processTooltipText;
                group['all'] = function () {
                    var _data = dimension.top(Infinity);
                    var newData = [];
                    var expr = processExpression(measureObj.formula, _data, client, dimensionObj);
                    expr = processSimpleExpr(expr);
                    _data.forEach(function (d) {
                        d[measureObj['columnName']] = eval(expr);
                    });
                    var keyObj = {};
                    _data.forEach(function (p) {
                        /*
                         * Date format
                         */
                        var dimensionVal=p[dimensionObj.columnName];
                        if(dateFormat){
                            dimensionVal=moment(p[dimensionObj.columnName]).format(dateFormat);
                        }else if(dimensionObj.columType=='date' || dimensionObj.columType=='datetime'){
                            var dimensionDate = new Date(p[dimensionObj.columnName]);
                            dimensionVal=dimensionDate.toISOString();
                        }
                        if (!keyObj[dimensionVal])
                            keyObj[dimensionVal] = {
                                count: 0,
                                sumIndex: 0,
                                avgIndex: 0,
                                customTooltip:true
                            };
                        var val=parseFloat(p[measureObj.columnName]);
                        if (tooltip.datakeys) {
                            tooltip.datakeys.forEach(function (d) {
                                if (d.dataKey == "Measure") {
                                    processTooltipText(d, keyObj[dimensionVal], p);
                                } else {
                                    if (!(keyObj[dimensionVal]["<" + d.reName + ">"])) {
                                        keyObj[dimensionVal]["<" + d.reName + ">"] = p[d.columnName];
                                    }
                                    else if (keyObj[dimensionVal]["<" + d.reName + ">"] != p[d.columnName]) {
                                        keyObj[dimensionVal]["<" + d.reName + ">"] = "Multiple Values..";
                                    }
                                }
                            });
                        }
                        keyObj[dimensionVal].sumIndex += val;
                        keyObj[dimensionVal].count = keyObj[dimensionVal].sumIndex;
                        keyObj[dimensionVal].avgIndex = keyObj[dimensionVal].sumIndex;
                    });
                    $.each(keyObj, function (val, key) {
                        newData.push({key: key, value: val});
                    });
                    newData = newData.sort(function (a, b) {
                        return b.value.sumIndex - a.value.sumIndex
                    });
                    return newData;
                }
                return group;
            },
            processDimensionStatements: function (expr, _data, client, dimensionObj) {
                const regex = /DimensionSum\((?:(?:\[([\w+|\(|\|\s)]+)\])|(all|ALL|All)),\[([\w+|\(|\)|\s]+)\]\)/gm;
                var m;
                var tableColumns = client.getTableColumn();
                while ((m = regex.exec(expr)) !== null) {
                    // This is necessary to avoid infinite loops with zero-width matches
                    if (m.index === regex.lastIndex) {
                        regex.lastIndex++;
                    }
                    // The result can be accessed through the `m`-variable.
                    if (m[2] != "ALL" && m[2] != "all" && m[2] != "All") {


                        if (fieldObject.type == "custom" && hasCalGroup(fieldObject.formula)) {
                            var newFormula = fieldObject.formula;
                            expr = expr.replace(m[0], newFormula);
                        } else {
                            expr = expr.replace(m[0], "[" + m[3] + "]");
                        }

                    } else {
                        var fieldObject = tableColumns[m[3]];
                        var keyName = fieldObject.columnName;
                        if (fieldObject.type == "custom" && hasCalGroup(fieldObject.formula)) {
                            var newFormula = fieldObject.formula;
                            preProcessFunctions(newFormula);
                            var _activeKey = [];
                            _activeKey.push(dimensionObj.columnName);
                            var groupedData = {};
                            var dataGroupParams = getDataGroupParams();

                            function getGroupedKey(d) {
                                var tempString = undefined;
                                if (_activeKey && _activeKey.length > 0) {
                                    tempString = "";
                                    _activeKey.forEach(function (key, index) {
                                        tempString += "||" + d[key];
                                    });
                                }
                                return tempString;
                            }

                            groupedData = d3.nest().key(function (d) {
                                return getGroupedKey(d);
                            });

                            function checkValIntegrity(val) {
                                if (!val || isNaN(val)) {
                                    val = 0;
                                }
                                else {
                                    val = Math.round(val);
                                }
                                return val;
                            }

                            function processedReturnObj(v) {
                                var tempObj = {};
                                $.each(dataGroupParams, function (value, key) {
                                    if (value == "sum") {
                                        tempObj[key] = d3.sum(v, function (d) {
                                            return checkValIntegrity(d[key]);
                                        });
                                    }
                                    if (value == "count") {
                                        tempObj[key] = d3.sum(v, function (d) {
                                            if (d[key])
                                                return 1;
                                            else
                                                return 0;
                                        });
                                    }
                                    if (value == "avg") {
                                        tempObj[key] = d3.mean(v, function (d) {
                                            return checkValIntegrity(d[key]);
                                        });
                                    }
                                });
                                return tempObj;
                            }

                            groupedData = groupedData.rollup(function (v) {
                                return processedReturnObj(v);
                            });
                            groupedData = groupedData.entries(_data);

                            var testData = {};
                            groupedData.forEach(function (d) {
                                testData[d.key] = d.values;
                            });
                            groupedData = testData;
                            var _tempAvgKey = null;
                            $.each(groupedData, function (d, key) {
                                try {
                                    var res = eval(_abstractExpression);
                                    d['newCalculation'] = res;
                                } catch (e) {

                                }
                            });
                            var tempSearchObj = {};
                            _data.forEach(function (d) {
                                var groupKey = getGroupedKey(d);
                                //   if(groupKey){
                                if (!tempSearchObj[groupKey]) {
                                    tempSearchObj[groupKey] = true;
                                    d[keyName] = groupedData[groupKey]['newCalculation'];
                                } else {
                                    d[keyName] = 0;
                                }
                            });
                        } else {

                        }
                        _total = 0;
                        _data.forEach(function (d, index) {
                            _total += parseFloat(d[m[3]]);
                        });
                        expr = expr.replace(m[0], "_total");
                    }
                }
                return expr;
            },

            countGroup: function (dimension, measureObj, groupColor, dateFormat, customTooltip, client) {
                return CountModule.processAverage(dimension, measureObj, groupColor, dateFormat, customTooltip, client);
            },

            sumGroup: function (dimension, measureObj, groupColor, dateFormat, customTooltip, client) {
                return SumModule.processAverage(dimension, measureObj, groupColor, dateFormat, customTooltip, client);
            },

            avgGroup: function (dimension, measureObj, groupColor, dateFormat, customTooltip, client) {
               return AverageModule.processAverage(dimension, measureObj, groupColor, dateFormat, customTooltip, client);
            },
            countDistinctGroup :function (dimension, measureObj, groupColor, dateFormat, customTooltip, client,dimensionObj) {
                return DistinctModule.processAverage(dimension, measureObj, groupColor, dateFormat, customTooltip, client,dimensionObj);
            },
            processTooltipText: function (d, g, v) {
                var val = v[d.columnName];
                if (isNaN(val) && val == null || isNaN(val) || val == 'NaN' || val == undefined) {
                    val = 1;
                }
                // if(!(val!=null && !isNaN(val))){
                //     val=0;
                // }
                else {
                    val = Math.round(val);
                    //val= val.toFixed(2)
                }
                if (isNaN(val)) {
                    val = 0;
                }
                if (!g["<sum(" + d.reName + ")>"])
                    g["<sum(" + d.reName + ")>"] = 0;
                if (!g["<count(" + d.reName + ")>"])
                    g["<count(" + d.reName + ")>"] = 0;
                switch (d.aggregate) {
                    case "sum":
                        g["<sum(" + d.reName + ")>"] += val;
                        break;
                    case "count":
                        g["<count(" + d.reName + ")>"]++;
                        break;
                    default :
                        g["<sum(" + d.reName + ")>"] += val;
                }
            },
            processTooltipTextWithGroupCalculation: function (d, g, v, groupColor, client) {
                if (g[d.columnName] == undefined) {
                    g[d.columnName] = {};
                    var formula = reduceMultilevelFormula(d.formula, client);
                    var parsedFormulaObj = parseFormula(formula, client);
                    g[d.columnName]['formula'] = parsedFormulaObj['formula'];
                    g[d.columnName]['params'] = parsedFormulaObj['params'];
                }
                var params = g[d.columnName]['params'];
                var formula = g[d.columnName]['formula'];
                $.each(params, function (val, key) {
                    key=key.split("@@_")[0];
                    var value = v[key];
                    if (!value || isNaN(value)) {
                        value = 0;
                    } else {
                        value = parseFloat(value);
                    }
                    if (val == 'sum' || val == 'avg') {
                        if (!g['sum'][key])
                            g['sum'][key] = 0;
                        g['sum'][key] += value;

                    }
                    if (val == 'count' || val == 'avg') {
                        if (!g['count'][key])
                            g['count'][key] = 0;
                        g['count'][key] += 1;
                    }
                    if (val == 'avg') {
                        if (!g['avg'][key]) {
                            g['avg'][key] = 0;
                        }

                        g['avg'][key] = g['sum'][key] / g['count'][key];
                        if (isNaN(g['avg'][key])) {
                            g['avg'][key] = 0;
                        }
                    }
                });
                val = parseFloat(eval(formula));
                if (isNaN(val) && val == null || isNaN(val) || val == 'NaN' || val == undefined) {
                    val = 1;
                } else {
                    val = Math.round(val);
                    //val= val.toFixed(2)
                }
                if (isNaN(val)) {
                    val = 0;
                }
                if (!g["<sum(" + d.reName + ")>"])
                    g["<sum(" + d.reName + ")>"] = 0;
                if (!g["<count(" + d.reName + ")>"])
                    g["<count(" + d.reName + ")>"] = 0;
                switch (d.aggregate) {
                    case "sum":
                        g["<sum(" + d.reName + ")>"] = val;
                        break;
                    case "count":
                        g["<count(" + d.reName + ")>"];
                        break;
                    default :
                        g["<sum(" + d.reName + ")>"] = val;
                }

                return g;
            },
            processTooltipTextWithFixed: function (d, g, v, groupColor, client) {
                if (g[d.columnName] == undefined) {
                    g[d.columnName] = {};

                    var formula = reduceMultilevelFormula(d.formula, client);
                    var parsedFormulaObj = parseFormulaCharts(formula, client);
                    g[d.columnName]['formula'] = parsedFormulaObj['formula'];
                    g[d.columnName]['params'] = parsedFormulaObj['params'];
                }
                var params = g[d.columnName]['params'];
                var formula = g[d.columnName]['formula'];
                $.each(params, function (val, key) {
                    key=key;
                    var value = v[key];

                    if (!value || isNaN(value)) {
                        value = 0;
                    } else {
                        value = parseFloat(value);
                    }
                    if (val == 'sum' || val == 'avg') {
                        if (!g['sum'][key])
                            g['sum'][key] = 0;
                        g['sum'][key] += value;

                    }
                    if (val == 'count' || val == 'avg') {
                        if (!g['count'][key])
                            g['count'][key] = 0;
                        g['count'][key] += 1;
                    }
                    if (val == 'avg') {
                        if (!g['avg'][key]) {
                            g['avg'][key] = 0;
                        }

                        g['avg'][key] = g['sum'][key] / g['count'][key];
                        if (isNaN(g['avg'][key])) {
                            g['avg'][key] = 0;
                        }
                    }
                });
                val = parseFloat(eval(formula));

                if (isNaN(val) && val == null || isNaN(val) || val == 'NaN' || val == undefined) {
                    val = 1;
                } else {
                    val = Math.round(val);
                    //val= val.toFixed(2)
                }
                if (isNaN(val)) {
                    val = 0;
                }
                if (!g["<sum(" + d.reName + ")>"])
                    g["<sum(" + d.reName + ")>"] = 0;
                if (!g["<count(" + d.reName + ")>"])
                    g["<count(" + d.reName + ")>"] = 0;
                switch (d.aggregate) {
                    case "sum":
                        g["<sum(" + d.reName + ")>"] = val;
                        break;
                    case "count":
                        g["<count(" + d.reName + ")>"];
                        break;
                    default :
                        g["<sum(" + d.reName + ")>"] = val;
                }

                return g;
            },
            sumWithGroupColor: function (dimension, measureColumnName, groupColorName, customTooltip, client) {
                var processTooltipText = this.processTooltipText;
                return dimension.group().reduce(
                    /* callback for when data is added to the current filter results */
                    function (g, v) {
                        ++g.count;
                        var val = v[measureColumnName];
                        if (!val || isNaN(val)) {
                            val = 0;
                        } else {
                            val = parseFloat(val);
                        }

                        g.sumIndex += val;
                        g.customTooltip = true;
                        if (customTooltip.datakeys) {
                            customTooltip.datakeys.forEach(function (d) {
                                if (d.dataKey == "Measure") {
                                    processTooltipText(d, g, v);
                                } else {
                                    if (!(g["<" + d.reName + ">"])) {
                                        g["<" + d.reName + ">"] = v[d.columnName];
                                    }
                                    else if (g["<" + d.reName + ">"] != v[d.columnName]) {
                                        g["<" + d.reName + ">"] = "Multiple Values..";

                                    }
                                }
                            });
                        }
                        if (groupColorName) {

                            var valTemp = (v[groupColorName]);

                            if (!g.groupRepeatCheck[valTemp])
                                g.groupRepeatCheck[valTemp] = val;
                            else
                                g.groupRepeatCheck[valTemp] += val;

                            g.groupColor = g.groupRepeatCheck;
                        }
                        return g;
                    },
                    /*
                     * callback for when data is removed from the current filter
                     * results
                     */

                    function (g, v) {
                        --g.count;
                        var val = v[measureColumnName];
                        if (!val || isNaN(val)) {
                            val = 0;
                        } else {
                            val = parseFloat(val);
                        }
                        g.customTooltip = true;
                        if (customTooltip.dataKeys) {
                            customTooltip.dataKeys.forEach(function (d) {
                                if (d.dataKey == "Measure") {
                                    if (isCalculated(d.formula, client)) {
                                        processTooltipTextWithGroupCalculation(d, g, v, groupColorName, client);
                                    } else {
                                        processTooltipText(d, g, v);
                                    }
                                } else {
                                    if (!g["<" + d.reName + ">"])
                                        g["<" + d.reName + ">"] = v[d.columnName];
                                    else if (g["<" + d.reName + ">"] != v[d.columnName]) {
                                        g["<" + d.reName + ">"] = "Multiple Values..";
                                    }
                                }

                            });
                        }
                        var valTemp = (v[groupColorName]);
                        if (!g.groupRepeatCheck[valTemp]) {
                            g.groupRepeatCheck[valTemp] = val;

                        } else {

                            g.groupRepeatCheck[valTemp] = g.groupRepeatCheck[valTemp] - val;

                        }
                        g.groupColor = g.groupRepeatCheck;
                        g.sumIndex -= val;
                        return g;
                    },
                    /* initialize p */
                    function () {
                        return {
                            count: 0,
                            sumIndex: 0,
                            avgIndex: 0,
                            groupColor: {},
                            groupRepeatCheck: {},
                            sum: {},
                            avg: {}
                        };
                    });
            },
            countWithGroupColor: function (dimension, measureColumnName, groupColorName, customTooltip, client) {
                var processTooltipText = this.processTooltipText;
                return dimension.group().reduce(
                    /* callback for when data is added to the current filter results */
                    function (g, v) {
                        ++g.count;
                        g.customTooltip = true;
                        if (customTooltip.datakeys) {
                            customTooltip.datakeys.forEach(function (d) {
                                if (d.dataKey == "Measure") {
                                    processTooltipText(d, g, v);
                                } else {
                                    if (!(g["<" + d.reName + ">"])) {
                                        g["<" + d.reName + ">"] = v[d.columnName];
                                    }
                                    else if (g["<" + d.reName + ">"] != v[d.columnName]) {
                                        g["<" + d.reName + ">"] = "Multiple Values..";

                                    }
                                }
                            });
                        }
                        if (groupColorName) {

                            var valTemp = (v[groupColorName]);

                            if (!g.groupRepeatCheck[valTemp])
                                g.groupRepeatCheck[valTemp] = 1;
                            else
                                g.groupRepeatCheck[valTemp] += 1;

                            g.groupColor = g.groupRepeatCheck;
                        }
                        return g;
                    },
                    /*
                     * callback for when data is removed from the current filter
                     * results
                     */

                    function (g, v) {
                        --g.count;
                        g.customTooltip = true;
                        if (customTooltip.dataKeys) {
                            customTooltip.dataKeys.forEach(function (d) {
                                if (d.dataKey == "Measure") {
                                    if (isCalculated(d.formula, client)) {
                                        processTooltipTextWithGroupCalculation(d, g, v, groupColorName, client);
                                    } else {
                                        processTooltipText(d, g, v);
                                    }
                                } else {
                                    if (!g["<" + d.reName + ">"])
                                        g["<" + d.reName + ">"] = v[d.columnName];
                                    else if (g["<" + d.reName + ">"] != v[d.columnName]) {
                                        g["<" + d.reName + ">"] = "Multiple Values..";
                                    }
                                }

                            });
                        }
                        var valTemp = (v[groupColorName]);
                        if (!g.groupRepeatCheck[valTemp]) {
                            g.groupRepeatCheck[valTemp] = 1;

                        } else {

                            g.groupRepeatCheck[valTemp] = g.groupRepeatCheck[valTemp] - 1;

                        }
                        g.groupColor = g.groupRepeatCheck;

                        return g;
                    },
                    /* initialize p */
                    function () {
                        return {
                            count: 0,
                            sumIndex: 0,
                            avgIndex: 0,
                            groupColor: {},
                            groupRepeatCheck: {},
                            sum: {},
                            avg: {}
                        };
                    });
            },

            sumWithTooltip: function (dimension, measureObj, _groupColor, customTooltip) {
                var columName = measureObj;
                var processTooltipText = this.processTooltipText;
                // var columType = measureObj.columType;
                var grp = dimension.group().reduce(
                    /* callback for when data is added to the current filter results */
                    function (g, v) {
                        ++g.count;
                        var val = v[columName];
                        if (!val || isNaN(val)) {
                            val = 0;
                        } else {
                            val = parseFloat(val);
                        }
                        g.sumIndex += val;
                        g.avgIndex = parseFloat(g.sumIndex / g.count);
                        g.customTooltip = true;
                        if (customTooltip.datakeys) {
                            customTooltip.datakeys.forEach(function (d) {
                                if (d.dataKey == "Measure") {
                                    processTooltipText(d, g, v);
                                } else {
                                    if (!(g["<" + d.reName + ">"])) {
                                        g["<" + d.reName + ">"] = v[d.columnName];
                                    }
                                    else if (g["<" + d.reName + ">"] != v[d.columnName]) {
                                        g["<" + d.reName + ">"] = "Multiple Values..";

                                    }
                                }
                            });
                        }

                        // if (draw._groupColor) {

                        //     var valTemp = (v[draw._groupColor]);
                        //     if (!g.groupRepeatCheck[valTemp])
                        //         g.groupRepeatCheck[valTemp] = val;
                        //     else
                        //         g.groupRepeatCheck[valTemp] += val;

                        //     g.groupColor = g.groupRepeatCheck;
                        // }

                        return g;
                    }
                    ,
                    /*
                     * callback for when data is removed from the current filter
                     * results
                     */
                    function (g, v) {
                        --g.count;
                        var val;
                        val = v[columName];
                        if (!val || isNaN(val)) {
                            val = 0;
                        } else {
                            val = parseFloat(val);
                        }
                        g.customTooltip = true;
                        if (customTooltip.dataKeys) {
                            customTooltip.dataKeys.forEach(function (d) {
                                if (d.dataKey == "Measure") {
                                    processTooltipText(d, g, v);
                                } else {
                                    if (!g["<" + d.reName + ">"])
                                        g["<" + d.reName + ">"] = v[d.columnName];
                                    else if (g["<" + d.reName + ">"] != v[d.columnName]) {
                                        g["<" + d.reName + ">"] = "Multiple Values..";
                                    }
                                }

                            });
                        }
                        g.sumIndex -= val;
                        g.avgIndex = g.count ? g.sumIndex / g.count : 0;
                        return g;
                    },
                    /* initialize p */
                    function () {
                        return {
                            count: 0,
                            sumIndex: 0,
                            avgIndex: 0
                        };
                    });
                return grp;
            },


            reduceDimensionSum: function (formula) {
                const regex = /DimensionSum\((?:(?:\[([\w+|\(|\|\s)]+)\])|(all|ALL|All)),\[([\w+|\(|\)|\s]+)\]\)/gm;
                var m;
                while ((m = regex.exec(expr)) !== null) {
                    // This is necessary to avoid infinite loops with zero-width matches
                    if (m.index === regex.lastIndex) {
                        regex.lastIndex++;
                    }
                    // The result can be accessed through the `m`-variable.
                    if (m[2] != "ALL" && m[2] != "all" && m[2] != "All") {
                        expr = expr.replace(m[0], "[" + m[3] + "]");
                    } else {
                        _total = 0;
                        _data.forEach(function (d, index) {
                            _total += parseFloat(d[m[3]]);
                        });
                        expr = expr.replace(m[0], "_total");
                    }
                }
                return expr;
            },
            calculatedGroup: function (dimension, measureObj, aggregates, groupColorObject, dateFormat, customTooltip, client) {
                var columnName = measureObj.value;
                var formula = reduceMultilevelFormula(measureObj.formula, client);
                var parsedFormulaObj = parseFormula(formula, client);
                var formula = parsedFormulaObj['formula'];
                var params = parsedFormulaObj['params'];
                var processTooltipTextWithgrp = TooltipModule.processTooltipGrp;
                var processTooltipTextWithoutGrp = TooltipModule.processTooltipText;
                var processTooltipTextWithoutGrpReduce = TooltipModule.processTooltipTextReduce;
                if (groupColorObject) {
                    var groupColorObject = groupColorObject;
                    var groupColor = groupColorObject.columnName;
                }
                var dimensionFormula = undefined;
                if (groupColorObject)
                    dimensionFormula = reduceMultilevelFormula(groupColorObject.formula, client);
                if (dimensionFormula) {
                    var parsedDimensionFormulaObj = parseFormula(dimensionFormula, client);
                    if (!$.isEmpty(parsedDimensionFormulaObj['params'])) {
                        $.each(parsedDimensionFormulaObj['params'], function (key, val) {
                            params[key] = val;
                        });
                        dimensionFormula = parsedDimensionFormulaObj['formula'];
                    } else {
                        dimensionFormula = undefined;
                    }
                }
                var groupColor = groupColor;
                return dimension.group().reduce(function (g, v) {
                    //try {
                    if (!groupColor) {
                        g.calculated = true;
                        $.each(params, function (val, key) {
                            key=key.split("@@_")[0];
                            var value = v[key];
                            if (!value || isNaN(value)) {
                                value = 0;
                            } else {
                                value = parseFloat(value);
                            }
                            if (val == 'sum' || val == 'avg') {
                                if (!g['sum'][key])
                                    g['sum'][key] = 0;
                                g['sum'][key] += value;
                            }
                            if (val == 'count' || val == 'avg') {
                                if (!g['count'][key])
                                    g['count'][key] = 0;
                                g['count'][key] += 1;
                            }
                            if (val == 'avg') {
                                if (!g['avg'][key]) {
                                    g['avg'][key] = 0;
                                }

                                g['avg'][key] = g['sum'][key] / g['count'][key];
                                if (isNaN(g['avg'][key])) {
                                    g['avg'][key] = 0;
                                }
                            }
                        });
                        if (customTooltip.datakeys) {
                            customTooltip.datakeys.forEach(function (d) {
                                if (d.dataKey == "Measure") {
                                    processTooltipTextWithoutGrp(d, g, v);
                                } else {
                                    if (!(g["<" + d.reName + ">"])) {
                                        g["<" + d.reName + ">"] = v[d.columnName];
                                    }
                                    else if (g["<" + d.reName + ">"] != v[d.columnName]) {
                                        g["<" + d.reName + ">"] = "Multiple Values..";

                                    }
                                }
                            });
                        }
                        g.sumIndex = eval(formula);
                        g.val = g.sumIndex;

                    }
                    if (groupColor) {
                        if (!g.done) {
                            $.each(params, function (val, key) {
                                key=key.split("@@_")[0];
                                g.groupRepeatCheck['count'][key] = {};
                                g.groupRepeatCheck['sum'][key] = {};
                            });
                            g.done = true;
                        }
                        var valTemp = (v[groupColor]);
                        $.each(params, function (val, key) {
                            key=key.split("@@_")[0];
                            var value = v[key];
                            if (!value || isNaN(value)) {
                                value = 0;
                            }
                            if (!g.groupRepeatCheck['count'][key][valTemp]) {
                                g.groupRepeatCheck['count'][key][valTemp] = 1;
                                g.groupRepeatCheck['sum'][key][valTemp] = value;
                            } else {
                                g.groupRepeatCheck['count'][key][valTemp]++;
                                g.groupRepeatCheck['sum'][key][valTemp] += value;
                            }
                            //   g.groupRepeatCheck['avg'][key][valTemp]=g.groupRepeatCheck['sumIndex'][valTemp]/g.groupRepeatCheck['count'][valTemp];
                            value = g.groupRepeatCheck['sum'][key][valTemp];
                            if (!g['sum'][key])
                                g['sum'][key] = 0;
                            g['sum'][key] = value;
                            if (!g['count'][key])
                                g['count'][key] = 0;
                            g['count'][key] = g.groupRepeatCheck['count'][key][valTemp];
                            if (!g['avg'][key]) {
                                g['avg'][key] = 0;
                            }
                            g['avg'][key] = g['sum'][key] / g['count'][key];
                            if (isNaN(g['avg'][key])) {
                                g['avg'][key] = 0;
                            }
                        });
                        if (customTooltip.datakeys) {
                            customTooltip.datakeys.forEach(function (d) {
                                if (d.dataKey == "Measure") {
                                    g.tooltipObj[d.reName+"@@!"+valTemp] = processTooltipTextWithgrp(d, g, v);
                                } else {
                                    if (!(g["<" + d.reName + ">"])) {
                                        g.tooltipObj[d.reName+"@@!"+valTemp]  = v[d.columnName];
                                    }
                                    else if (g["<" + d.reName + ">"] != v[d.columnName]) {
                                        g.tooltipObj[d.reName+"@@!"+valTemp]= "Multiple Values..";
                                    }
                                }
                            });
                        }
                        g.sumIndex = parseFloat(eval(formula));
                        if (isNaN(g.sumIndex))
                            g.sumIndex = 0;
                        if (dimensionFormula) {
                            valTemp = eval(dimensionFormula);
                            g.groupRepeatCheck['val'] = {};
                        }
                        g.groupRepeatCheck['val'][valTemp] = g.sumIndex;
                        g.groupColor = g.groupRepeatCheck['val'];
                    }
                    return g;
                }, function (g, v) {
                    //try{

                    if (!groupColor) {
                        $.each(params, function (val, key) {
                            key=key.split("@@_")[0];
                            var value = v[key];
                            if (!value || isNaN(value)) {
                                value = 0;
                            } else {
                                value = parseFloat(value);
                            }
                            if (val == 'sum' || val == 'avg') {
                                if (!g['sum'][key])
                                    g['sum'][key] = 0;
                                g['sum'][key] -= parseFloat(value);
                            }
                            if (val == 'count' || val == 'avg') {
                                if (!g['count'][key])
                                    g['count'][key] = 0;
                                g['count'][key] -= parseFloat(1);
                            }
                            if (val == 'avg') {
                                if (!g['avg'][key]) {
                                    g['avg'][key] = 0;
                                }

                                g['avg'][key] = g['sum'][key] / g['count'][key];
                                if (isNaN(g['avg'][key])) {
                                    g['avg'][key] = 0;
                                }
                            }
                        });
                        if (customTooltip.datakeys) {
                            customTooltip.datakeys.forEach(function (d) {
                                if (d.dataKey == "Measure") {
                                    processTooltipTextWithoutGrpReduce(d, g, v);
                                } else {
                                    if (!g["<" + d.reName + ">"])
                                        g["<" + d.reName + ">"] = v[d.columnName];
                                    else if (g["<" + d.reName + ">"] != v[d.columnName]) {
                                        g["<" + d.reName + ">"] = "Multiple Values..";
                                    }
                                }
                            });
                        }
                        g.sumIndex = parseFloat(eval(formula));
                        g.val = g.sumIndex;
                    }
                    if (groupColor) {
                        if (!g.done) {
                            $.each(params, function (val, key) {
                                key=key.split("@@_")[0];
                                g.groupRepeatCheck['count'][key] = {};
                                g.groupRepeatCheck['sum'][key] = {};
                            });
                            g.done = true;
                        }
                        var valTemp = (v[groupColor]);
                        $.each(params, function (val, key) {
                            key=key.split("@@_")[0];
                            var value = v[key];
                            if (!value || isNaN(value)) {
                                value = 0;
                            } else {
                                value = Math.round(value);
                            }

                            if (!g.groupRepeatCheck['count'][key][valTemp]) {
                                g.groupRepeatCheck['count'][key][valTemp] = 1;
                                g.groupRepeatCheck['sum'][key][valTemp] = value;
                            } else {
                                g.groupRepeatCheck['count'][key][valTemp]--;
                                g.groupRepeatCheck['sum'][key][valTemp] -= parseFloat(value);
                            }
                            //   g.groupRepeatCheck['avg'][key][valTemp]=g.groupRepeatCheck['sumIndex'][valTemp]/g.groupRepeatCheck['count'][valTemp];

                            value = g.groupRepeatCheck['sum'][key][valTemp];
                            if (!g['sum'][key])
                                g['sum'][key] = 0;

                            g['sum'][key] = value;


                            if (!g['count'][key])
                                g['count'][key] = 0;
                            g['count'][key] = g.groupRepeatCheck['count'][key][valTemp];

                            if (!g['avg'][key]) {
                                g['avg'][key] = 0;
                            }

                            g['avg'][key] = g['sum'][key] / g['count'][key];
                            if (isNaN(g['avg'][key])) {
                                g['avg'][key] = 0;
                            }
                        });
                        if (customTooltip.datakeys) {
                            customTooltip.datakeys.forEach(function (d) {
                                if (d.dataKey == "Measure") {
                                    g.tooltipObj[d.reName+"@@!"+valTemp] = processTooltipTextWithgrp(d, g, v);
                                } else {
                                    if (!(g["<" + d.reName + ">"])) {
                                        g.tooltipObj[d.reName+"@@!"+valTemp] = v[d.columnName];
                                    }
                                    else if (g["<" + d.reName + ">"] != v[d.columnName]) {
                                        g.tooltipObj[d.reName+"@@!"+valTemp] = "Multiple Values..";

                                    }
                                }
                            });
                        }
                        g.sumIndex = parseFloat(eval(formula));
                        if (isNaN(g.sumIndex))
                            g.sumIndex = 0;

                        g.groupRepeatCheck['val'][valTemp] = g.sumIndex;

                        g.groupColor = g.groupRepeatCheck['val'];
                    }
                    return g;
                }, function (g, v) {
                    return {
                        sumIndex: 0,
                        tempSum: 0,
                        sum: {},
                        count: {},
                        avg: {},
                        groupColor: {},
                        groupRepeatCheck: {count: {}, sum: {}, avg: {}, val: {}},
                        tooltipObj:{},

                    };
                });

            },
            numberGroup: function (measureObj, client,dimensionObj,dimension) {

                if(hasMultiLevelCalculationForDcount(measureObj.formula,client)){
                    var reduceFormula=reduceMultilevelFormulaForDcount(measureObj.formula,client)
                    if(reduceFormula.indexOf("dcount")!=-1 || reduceFormula.indexOf("AGGR")!=-1){
                        measureObj.formula=reduceFormula;
                    }
                }
                if(measureObj.formula && (measureObj.formula.indexOf("dcount")!=-1 || measureObj.formula.indexOf("AGGR")!=-1)){
                    if(measureObj.formula && measureObj.formula.indexOf("dcount")!=-1){
                        //Create calculated column for dcount
                        measureObj.formula=this.createDataForDcount(measureObj,dimension,dimensionObj);
                    }
                    if(measureObj.formula && measureObj.formula.indexOf("AGGR")!=-1){
                        //Create calculated column for aggr
                        measureObj.formula=this.createDataForAggr(measureObj,dimension,dimensionObj);
                    }
                    if(measureObj.formula.indexOf("avg(")!=-1){
                        return this.calculatedNumberGroupsForAggr(measureObj, client,dimension,dimensionObj);
                    }else{
                        return this.calculatedNumberForAggrDcount(measureObj, client,dimension,dimensionObj);
                    }
                }
                /*
                 * Reduce formula
                 */
                if (hasMultiLevelCalculationNumber(measureObj.formula, client)) {
                    measureObj.formula = reduceMultilevelFormulaNumber(measureObj.formula, client);
                }

                //&& isAggregateField(measureObj.formula) && !FixedCalculationModule.hasFixedCalculation(measureObj.formula) && !AggregateModule.isAggrExist(measureObj.formula) &&  measureObj.formula.indexOf("dcount")==-1 && measureObj.formula.indexOf("AGGR")==-1
                if (measureObj.type == "custom" && measureObj.formula && isAggregateField(measureObj.formula) && !FixedCalculationModule.hasFixedCalculation(measureObj.formula) && !AggregateModule.isAggrExist(measureObj.formula) ) {
                    return this.calculatedNumberGroups(measureObj, client);
                }else if(measureObj.formula && AggregateModule.isAggrExist(measureObj.formula)){
                    return this.numberWidgetMinMax(measureObj,client,dimension);
                } else {
                    return this.normalNumberGroups(measureObj,client);
                }
            },
            calculatedNumberForAggrDcount:function (measureObj,client,dimension,dimensionObj) {
                try{
                    var data = dimension.top(Infinity);
                    var min = 0;//temp
                    var columName = measureObj.columnName;
                    var expr=measureObj.formula;
                    /*
                     * If condition check
                     */
                    try{
                        //if([a-z|A-Z|0-9|=|.|,|-|*|\/|!|%|{|}| |_|(|)|[\]]*)else{[\w]:?[=|.|,|-|*|\/|!|%|{|}| |+|_|(|)|[\]]*}
                        const regex = /if([a-z|A-Z|0-9|=|.|,|-|*|\/|!|%|{|}| |_|(|)|[\]]*)else{[\w]:?[=|.|,|-|*|\/|!|%|{|}| |+|_|(|)|[\]]*}/gm;
                        var m;
                        while ((m = regex.exec(expr)) !== null) {
                            if (m.index === regex.lastIndex)
                                regex.lastIndex++;
                            expr=expr.replace(m[0],'eval("'+m[0]+'")');
                        }
                    }catch (e){
                        console.log(e);
                    }
                    expr = preProcessToken(expr);
                    data.forEach(function (d) {
                        var val= eval(expr);
                        if (val < min) min = val;
                        d[columName] = val;
                    });
                    return _crossfilter.groupAll().reduce(function (g, v) {
                            ++g.count;
                            var val = v[columName];
                            if (!(val != null && !isNaN(val))) {
                                val = 0;
                            } else {
                                val = parseFloat(val);
                            }
                            g.sumIndex += parseFloat(val);
                            g.avgIndex = g.sumIndex / g.count;
                            return g;
                        },
                        /*
                         * callback for when data is removed from the current filter
                         * results
                         */
                        function (g, v) {
                            var val = v[columName];
                            if (!(val != null && !isNaN(val))) {
                                val = 0;
                            } else {
                                val = parseFloat(val);
                            }
                            --g.count;
                            g.sumIndex -= parseFloat(val);
                            g.avgIndex = g.count ? g.sumIndex / g.count : 0;
                            return g;
                        },
                        /* initialize p */
                        function () {
                            return {
                                count: 0,
                                sumIndex: parseFloat(0),
                                avgIndex: 0,
                                min:min
                            };
                        });
                    /*var keyObj={};
                    keyObj['dimensionVal'] = {
                        count: 0,
                        sumIndex: 0,
                        avgIndex: 0,
                    };*/
                    /*data.forEach(function (d) {
                        var val=parseFloat(d[measureObj.columnName]);
                        /!*
                         * Date format
                         *!/
                        var dimensionVal="dimensionVal";

                        if(val){
                            ++keyObj['dimensionVal'].count;
                        }
                        keyObj['dimensionVal'].sumIndex += val;
                        if(keyObj['dimensionVal'].count)
                            keyObj['dimensionVal'].avgIndex = keyObj[dimensionVal].sumIndex/keyObj[dimensionVal].count;
                        else
                            keyObj['dimensionVal'].avgIndex = 0;
                    });
                    return {
                        count: keyObj.dimensionVal.count,
                        sumIndex: keyObj.dimensionVal.sumIndex,
                        avgIndex: keyObj.dimensionVal.avgIndex
                    };*/

                }catch (e){
                    console.log(e);
                }
            },
            normalNumberGroups: function (measureObj,client) {
                var data = client.getData();
                console.log(data[0],measureObj.columnName);
                var min = data[0][measureObj.columnName];
                var columName = measureObj.columnName;
                data.forEach(function (d) {
                    var val= d[columName];
                    if (val < min) min = val;
                });
                return _crossfilter.groupAll().reduce(function (g, v) {
                    ++g.count;
                    var val = v[columName];
                    if (!(val != null && !isNaN(val))) {
                        val = 0;
                    } else {
                        val = parseFloat(val);
                    }
                    g.sumIndex += parseFloat(val);
                    g.avgIndex = g.sumIndex / g.count;
                    return g;
                },
                /*
                 * callback for when data is removed from the current filter
                 * results
                 */
                function (g, v) {
                    var val = v[columName];
                    if (!(val != null && !isNaN(val))) {
                        val = 0;
                    } else {
                        val = parseFloat(val);
                    }
                    --g.count;
                    g.sumIndex -= parseFloat(val);
                    g.avgIndex = g.count ? g.sumIndex / g.count : 0;
                    return g;
                },
                /* initialize p */
                function () {
                    return {
                        count: 0,
                        sumIndex: parseFloat(0),
                        avgIndex: 0,
                        min:min
                    };
                });
            },
            calculatedNumberGroups: function (measureObj, client) {
                var columName = measureObj.value;
                var formula = measureObj.formula;
                formula = reduceMultilevelFormula(measureObj.formula, client);
                var parsedFormulaObj = parseFormulaCharts(formula);
                formula = parsedFormulaObj['formula'];
                var params = parsedFormulaObj['params'];
                return _crossfilter.groupAll().reduce(function (g, v) {
                    $.each(params, function (val, key) {
                        key=key;
                        var value = v[key];
                        if (!(value != null && !isNaN(value))) {
                            value = 0;
                        } else {
                            value = parseFloat(value);
                        }
                        if (!g['sum'][key])
                            g['sum'][key] = 0;
                        g['sum'][key] += value;
                        if (!g['count'][key])
                            g['count'][key] = 0;
                        g['count'][key] += 1;
                        if (!g['avg'][key]) {
                            g['avg'][key] = 0;
                        }
                        g['avg'][key] = g['sum'][key] / g['count'][key];
                    });
                    g.sumIndex = parseFloat(eval(formula));
                    return g;
                }, function (g, v) {
                    $.each(params, function (val, key) {
                        key=key;
                        var value = v[key];
                        if (!(value != null && !isNaN(value))) {
                            value = 0;
                        } else {
                            value = parseFloat(value);
                        }
                        if (!g['sum'][key])
                            g['sum'][key] = 0;
                        g['sum'][key] -= value;
                        if (!g['count'][key])
                            g['count'][key] = 0;
                        g['count'][key] -= 1;
                        if (!g['avg'][key]) {
                            g['avg'][key] = 0;
                        }
                        g['avg'][key] = g['sum'][key] / g['count'][key];

                    });
                    g.sumIndex = parseFloat(eval(formula));

                    return g;
                }, function (g, v) {
                    return {
                        count: {},
                        sum: {},
                        avg: {}
                    };
                });
            },
            calculatedNumberGroupsForAggr: function (measureObj, client,dimension,dimensionObj) {
                var columName = measureObj.value;
                var formula = measureObj.formula;
                //formula = reduceMultilevelFormula(measureObj.formula, client);
                var parsedFormulaObj = parseFormula(formula);
                formula = parsedFormulaObj['formula'];
                var params = parsedFormulaObj['params'];
                var group={};
                group['value']=function () {
                    var _data=dimension.top(Infinity);
                    var g={
                        count: {},
                        sum: {},
                        avg: {},
                        countGrp:0,
                        grp:{}
                    };
                    _data.forEach(function (v) {
                        $.each(params, function (val, key) {
                            key=key.split("@@_")[0];
                            var value = v[key];
                            if (!(value != null && !isNaN(value))) {
                                value = 0;
                            } else {
                                value = parseFloat(value);
                            }
                            if (!g['sum'][key])
                                g['sum'][key] = 0;
                            g['sum'][key] += value;
                            if (!g['count'][key])
                                g['count'][key] = 0;
                            g['count'][key] += 1;
                            if (!g['avg'][key]) {
                                g['avg'][key] = 0;
                            }
                            if(g.grp[v[dimensionObj.columnName]]==undefined){
                                g.grp[v[dimensionObj.columnName]]=true;
                                g.countGrp++;
                            }
                            g['avg'][key] = parseFloat(g['sum'][key])/parseFloat(g.countGrp);
                        });
                        g.sumIndex = parseFloat(eval(formula));
                    });
                    return g;
                }
                return group;
            }
        }
    }
    this.CrossfilterUtility = crossfilterUtility;
})();
module.exports = CrossfilterUtility;