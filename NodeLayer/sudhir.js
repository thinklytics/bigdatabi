const puppeteer = require('puppeteer');
(async () => {
        const browser = await puppeteer.launch({
        headless: true,
        executablePath:'/usr/bin/google-chrome-stable',
        args: ['--no-sandbox', '--disable-setuid-sandbox'],
        });
        const page = await browser.newPage();
        await page.goto('https://www.google.com/', {waitUntil: 'networkidle2'});
        await page.pdf({path: 'hn.pdf', format: 'A4'});
    })();
