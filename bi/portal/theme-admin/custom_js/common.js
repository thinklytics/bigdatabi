var baseUrl = 'http://demo2api.thinklytics.io/public/';
var request = function(path , method , body){
    var url = baseUrl + path;
    var accessToken = getCookie('accessToken');
    accessToken = accessToken.replace(/%22/g,"");
    var config = {
        headers:{
            'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;',
            "Authorization" : "Bearer "+accessToken
        }
    };
    var response;
    if(method=== "post"){
        response = $.ajax({
            type        : method, // define the type of HTTP verb we want to use (POST for our form)
            url         : url, // the url where we want to POST
            data        : body, // our data object
            headers :{
                "Accept": "application/json, text/plain, /",
                'Content-Type':'application/x-www-form-urlencoded;charset=utf-8;',
                'Authorization':"Bearer "+accessToken
            }
        });
    }else if(method == "get"){
        response = $.ajax({
            type        : method, // define the type of HTTP verb we want to use (POST for our form)
            url         : url, // the url where we want to POST
            data        :  body, // our data object
            headers :{
                "Accept": "application/json, text/plain, /",
                'Content-Type':'application/x-www-form-urlencoded;charset=utf-8;',
                'Authorization':"Bearer "+accessToken
            }
        });
    }
    return response;
}

var setCookie=function(name,value,days) {
    var expires = "";
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days*24*60*60*1000));
        expires = "; expires=" + date.toUTCString();
    }
    document.cookie = name + "=" + (value || "")  + expires + "; path=/";
}
var getCookie=function(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for(var i=0;i < ca.length;i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1,c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
    }
    return null;
}
var eraseCookie=function(name) {
    document.cookie = name+'=; Max-Age=-99999999;';
}