<?php
    require_once('common.php');
?>


<!doctype html>
    <!--[if lte IE 9]> <html class="lte-ie9" lang="en"> <![endif]-->
    <!--[if gt IE 9]><!--> <html lang="en"> <!--<![endif]-->
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Remove Tap Highlight on Windows Phone IE -->
        <meta name="msapplication-tap-highlight" content="no"/>
        <title>Thinklytics</title>

        <?php
            common_CSS();
        ?>
    </head>

    <body class=" sidebar_main_open sidebar_main_swipe">

    <?php
        common_Header();
    ?>


    <div id="page_content" style="margin-left: 0px;">
        <div id="page_content_inner">
            <div id="sharedViewCont" class="gallery_grid uk-grid-width-medium-1-4 uk-grid-width-large-1-6" data-uk-grid="{gutter: 16}">

                <!-- Add all shared view list -->

            </div>
        </div>
    </div>

    <?php
        common_JS();
    ?>

    <script type="text/javascript">
//get machineID from url
        var url_string = window.location.href;
        var index = url_string.lastIndexOf('?') + 1;
        var selectedMachine = url_string.substring(index);

//get sharedview corresponding to machine
        var sharedViewList = [];
        var data = {
            'pageName' : 'sharedview-list',
        };
        request("api/universal/machineSharedviewList","post",data).done(function (response){
            if(response.errorCode == 1){
                var selected_SharedView = response.result;
                selected_SharedView.forEach(function(d){
                    if(d.machine_id == selectedMachine){
                        sharedViewList = JSON.parse(d.sharedview);
                    }
                });
                console.log(sharedViewList)
                var divStr = '';
                if(sharedViewList.length){
                    sharedViewList.forEach(function(d){
                        divStr += '<div onclick=getSharedView('+d.id+'); ><div class="md-card md-card-hover"><div class="gallery_grid_item md-card-content"><img class="imgIconView" src="http://devapi.thinklytics.io/storage/app/dashboardImage/'+d.image+'.png"><div class="gallery_grid_image_caption"><span class="textName gallery_image_title uk-text-truncate">'+d.name+'</span></div></div></div></div>';
                    });
                }else{
                    divStr = '<div class="emptyName">No Shared View Found</div>';
                }
                $('#sharedViewCont').append(divStr);
                if(sharedViewList.length){
                    $(window).resize();
                }
            }
        });

        function getSharedView(d){
            for(var i=0; i<sharedViewList.length; i++){
                if(d == sharedViewList[i]['id']){
                    setCookie('auth_key', sharedViewList[i]['auth_key'], 1);
                    window.open('http://dev.thinklytics.io/portal/theme/sharedView.php','_blank');
                }
            }
        }
    </script>

    </body>
</html>