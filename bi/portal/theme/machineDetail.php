<?php
    require_once('common.php');
?>


<!doctype html>
<!--[if lte IE 9]> <html class="lte-ie9" lang="en"> <![endif]-->
<!--[if gt IE 9]><!--> <html lang="en"> <!--<![endif]-->
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Remove Tap Highlight on Windows Phone IE -->
        <meta name="msapplication-tap-highlight" content="no"/>
        <title>Thinklytics</title>

        <?php
            common_CSS();
        ?>
    </head>

    <body class=" sidebar_main_open sidebar_main_swipe">

    <?php
        common_Header();
    ?>


    <div id="page_content" style="margin-left: 0px;">
        <div id="page_content_inner">

            <span class="heading_b uk-margin-bottom">Machine-Details</span>
            <button class="md-btn md-btn-primary md-btn-wave-light" style="float: right;" onclick="AddMachine();">Add</button>
            <div class="uk-width-medium-1-3">
                <div class="uk-modal" id="machine_Modal">
                    <div class="uk-modal-dialog">
                        <div class="uk-modal-header">
                            <h3 class="uk-modal-title">Machine Details</h3>
                        </div>

                        <form id="addForm" enctype="multipart/form-data" role="form" method="post" action="#">
                            <div class="uk-grid" data-uk-grid-margin>
                                <div class="uk-width-medium-1-2">
                                    <div class="uk-form-row">
                                        <div class="uk-grid">
                                            <div>
                                                <input type="text" id="machine_ID" name="id" class="md-input" placeholder="Machine ID" required />
                                                <input type="hidden" id="uniqueId" name="uniqueId" />
                                            </div>
                                            <div>
                                                <input type="text" id="machine_Desc" name="desc" class="md-input" placeholder="Machine Description" required />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="uk-width-medium-1-2">
                                    <div class="uk-form-row">
                                        <div class="uk-grid">
                                            <div>
                                                <input type="text" id="machine_Name" name="name" class="md-input" placeholder="Machine Name" required />
                                            </div>
                                            <div style="margin-top: 20px;">
                                                <input name="machine_File" id="machine_File" type="file" >
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="uk-modal-footer uk-text-right">
                                <button type="button" class="md-btn md-btn-danger md-btn-wave-light uk-modal-close">Close</button>
                                <button type="submit" class="md-btn md-btn-primary md-btn-wave-light" id="saveMacBtn">Save</button>
                                <button type="submit" class="md-btn md-btn-primary md-btn-wave-light" id="updateMacBtn" style="display: none;">Update</button>
                            </div>
                        </form>


                    </div>
                </div>
            </div>
            <br>

            <div class="md-card uk-margin-medium-bottom">
                <div class="md-card-content">
                    <div class="uk-overflow-container">
                        <table class="uk-table uk-table-striped">
                            <thead>
                                <tr>
                                    <th>S. No.</th>
                                    <th>Machine ID</th>
                                    <th>Machine Name</th>
                                    <th>Machine Description</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody id="machineTableBody">
                                <!--TableBody-->
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>
    </div>



    <?php
        common_JS();
    ?>

    <script type="text/javascript">

// open Add PopUp Start
        function AddMachine() {
            $('#machine_ID').val('');
            $('#machine_Name').val('');
            $('#machine_Desc').val('');
            UIkit.modal("#machine_Modal").show();
        }
// open Add PopUp End



//Bind Table Start
        function createTable() {
            $('#machineTableBody').html('');
            var machineList;
            var data = {
                'pageName' : 'sharedview-list',
            };
            request("api/universal/machineAddedList","post",data).done(function (response){
                if(response.errorCode == 1){
                    machineList = response.result;
                    var divStr = '';
                    machineList.forEach(function (d,i){
                        divStr += '<tr><td>'+(i+1)+'</td><td>'+d.machine_id+'</td><td>'+d.name+'</td><td>'+d.description+'</td><td><button type="button" class="md-btn md-btn-primary md-btn-wave-light" onclick="editMachine('+d.id+');"><i class="fa fa-edit"></i></button><button type="button" class="md-btn md-btn-danger md-btn-wave-light" onclick="deleteMachine('+d.id+');"><i class="fa fa-trash"></i></button></td></tr>';
                    });
                    $('#machineTableBody').append(divStr);
                }
            });
        }
        createTable();
//Bind Table End



// add machine details Start
        $('#addForm').on('submit',(function(e) {
            e.preventDefault();
            var formData = new FormData(this);
            var accessToken = getCookie('accessToken');
            accessToken = accessToken.replace(/%22/g,"");

            console.log(formData);

            $.ajax({
                type:'POST',
                url: 'http://devapi.thinklytics.io/public/api/universal/machineAdd',
                data:formData,
                cache:false,
                contentType: false,
                processData: false,
                enctype: 'multipart/form-data',
                headers :{
                    'Authorization':"Bearer "+accessToken
                },
                success:function(data){
                    console.log(data);
                    alert(data.message);
                    createTable();
                    UIkit.modal("#machine_Modal").hide();
                },
                error: function(data){
                    console.log(data);
                    alert(data.message);
                }
            });
        }));
// add machine details End



// edit machine details Start
        var machineData;
        function editMachine(id){
            $('#uniqueId').val(id);

            var data = {
                'pageName' : 'sharedview-list',
                'id' : id,
            };
            request("api/universal/getMachine","post",data).done(function (response){
                if(response.errorCode == 1){
                    machineData = response.result;
                    $('#machine_ID').val(machineData.machine_id);
                    $('#machine_Name').val(machineData.name);
                    $('#machine_Desc').val(machineData.description);
                }
            });
            $('#saveMacBtn').css('display', 'none');
            $('#updateMacBtn').css('display', 'inline');
            UIkit.modal("#machine_Modal").show();
        }
// edit machine details End



// delete machine details Start
        function deleteMachine(id){
            var data = {
                'pageName' : 'sharedview-list',
                'id' : id,
            };
            request("api/universal/machineDelete","post",data).done(function (response){
                if(response.errorCode == 1){
                    alert(response.message);
                    createTable();
                }
            });

        }
// delete machine details End


    </script>

    </body>
</html>



