<?php
    require_once('common.php');
?>


<!doctype html>
    <!--[if lte IE 9]> <html class="lte-ie9" lang="en"> <![endif]-->
    <!--[if gt IE 9]><!--> <html lang="en"> <!--<![endif]-->
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Remove Tap Highlight on Windows Phone IE -->
        <meta name="msapplication-tap-highlight" content="no"/>
        <title>Thinklytics</title>

        <?php
            common_CSS();
        ?>
    </head>

    <body class=" sidebar_main_open sidebar_main_swipe">

    <?php
        common_Header();
    ?>


    <div id="page_content" style="margin-left: 0px;">
        <div id="page_content_inner">

            <!-- Add Machine & Shared View List -->

        </div>
    </div>

    <!-- google web fonts -->

    <?php
        common_JS();
    ?>

    <script type="text/javascript">

// getList of selected sharedView & machine list
        var selected_SharedView;
        var data = {
            'pageName' : 'sharedview-list',
        };
        request("api/universal/machineSharedviewList","post",data).done(function(response){
            console.log(response)
            selected_SharedView = response.result;  
        });

// getList of sharedView list
        var sharedViewList, machineList;
        var data = {
            'pageName' : 'sharedview-list',
        };
        request("api/sharedview/folderList","post",data).done(function(response){
            if(response.errorCode == 1){
                sharedViewList = response.result;
            }
            var data = {
                'pageName' : '',
            };
// getList of machine list
            request("api/universal/machineAddedList","post",data).done(function(response) {
                console.log(response)
                if(response.errorCode == 1){
                    machineList = response.result;
                    var divStr = '';
                    machineList.forEach(function(d,i){
                        var div5 = '';
                        var div1 = '<div class="md-card"><div class="md-card-content"><div class="uk-grid" data-uk-grid-margin><div class="uk-width-large-1-4"><label>Machine Name</label><p class="textName">'+d.name+'</p></div><div class="uk-width-large-1-2"><form class="uk-form-stacked"><label for="kUI_multiselect_basic" class="uk-form-label">Shared View</label><select id="kUI_multiselect_basic_'+d.id+'" multiple="multiple" data-placeholder="Select Shared View">';
                       
                        $.each(sharedViewList, function(k,v){
                            var div2 = '<optgroup label="'+k+'">';
                            var div3 = '';
                            v.forEach(function(dd){
                                div3 += '<option>'+dd.name+'</option>';
                            });
                            selected_SharedView.forEach(function(a,aa){
                                if(d.id == a.machine_id){
                                    JSON.parse(a.sharedview).forEach(function(b,bb){
                                        var toStr = '<option>' + b.name + '</option>';
                                        var fromStr = '<option selected>' + b.name + '</option>';
                                        if(div3.includes(toStr)){
                                            div3 = div3.replace(toStr, fromStr)
                                        }
                                    });
                                }
                            });
                            var div4 = '</optgroup>';
                            div5 += div2 + div3 + div4;
                        });
                        var div6 = '</select></form></div><div class="uk-width-large-1-4"><button class="md-btn md-btn-primary md-btn-wave-light saveBtn" onclick=saveMachine(' + d.id + '); >Save</button></div></div></div></div>';
                        divStr+= div1 + div5 + div6;
                    });                
                    $('#page_content_inner').append(divStr);
                    machineList.forEach(function(d){
                        $("#kUI_multiselect_basic_"+d.id).kendoMultiSelect();
                    });
                    $(window).resize();
                }
            });
        });

        function saveMachine(id) {
            var tempSharedViewArr = [], dataArr = [];
            $.each(sharedViewList, function(k,v){
                v.forEach(function(d){
                    tempSharedViewArr.push(d);
                });
            });
            var multiselectData = $("#kUI_multiselect_basic_"+id).data("kendoMultiSelect")._old;
            multiselectData.forEach(function(d,i){
                for(var i=0; i<tempSharedViewArr.length; i++){
                    if(d == tempSharedViewArr[i].name){
                        dataArr.push(tempSharedViewArr[i]);
                        break;
                    }
                }
            });
            var data = {
                'pageName' : 'sharedview-list',
                'machineId' : id,
                'sharedViewArr' : JSON.stringify(dataArr),
            };
            request("api/universal/machineSharedviewSave","post",data).done(function (response){
                if(response.errorCode == 1){
                    alert(response.message)
                }
            });
        }
    </script>  
    
    </body>
</html>
