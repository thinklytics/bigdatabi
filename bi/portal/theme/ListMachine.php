<?php
require_once('common.php');
?>


<!doctype html>
    <!--[if lte IE 9]> <html class="lte-ie9" lang="en"> <![endif]-->
    <!--[if gt IE 9]><!--> <html lang="en"> <!--<![endif]-->
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Remove Tap Highlight on Windows Phone IE -->
        <meta name="msapplication-tap-highlight" content="no"/>
        <title>Thinklytics</title>

        <?php
            common_CSS();
        ?>
    </head>

    <body class=" sidebar_main_open sidebar_main_swipe">

    <?php
        common_Header();
    ?>

        <div id="page_content" style="margin-left: 0px;">
            <div id="page_content_inner">
                <div id="machineContainer" class="gallery_grid uk-grid-width-medium-1-4 uk-grid-width-large-1-6" data-uk-grid="{gutter: 16}">

                    <!-- Add all shared view list -->

                </div>
            </div>
        </div>

    <?php
        common_JS();
    ?>

        <script type="text/javascript">
            var data = {
                'pageName' : '',
            };
            request("api/universal/machineAddedList","post",data).done(function (response) {
                console.log(response)
                if(response.errorCode == 1){
                    var machineList = response.result;
                    var divStr = '';
                    machineList.forEach(function(d){
                        var div1 = '<div><div class="md-card md-card-hover"><div class="gallery_grid_item md-card-content"><a href="ListSharedView.php?' + d.id + '"><img class="imgIconView" src="http://devapi.thinklytics.io/storage/app/'+d.image+'"></a><div class="gallery_grid_image_caption"><span class="textName gallery_image_title uk-text-truncate">'+ d.name+'</span>';
                        if(d.status == 0){
                            var div2 = '<div title="In-Active" class="createCircle redCircle">';
                        }else if(d.status == 1){
                            var div2 = '<div title="Active" class="createCircle greenCircle">';
                        }else if(d.status == 2){
                            var div2 = '<div title="Warning" class="createCircle orangeCircle">';
                        }else{
                            var div2 = '<div title="Not-Found" class="createCircle blackCircle">';
                        }
                        var div3 = '</div></div></div></div></div>';
                        divStr += div1+div2+div3;
                    });
                    $('#machineContainer').append(divStr);
                    $(window).resize();
                }
            });
        </script>

    </body>
</html>