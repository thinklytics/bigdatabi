<!--Common CSS Start-->
<?php
    function common_CSS(){
?>

<!-- uikit -->
<link rel="stylesheet" href="bower_components/uikit/css/uikit.almost-flat.min.css" media="all">
<!-- flag icons -->
<link rel="stylesheet" href="assets/icons/flags/flags.min.css" media="all">
<!-- altair admin -->
<link rel="stylesheet" href="assets/css/main.min.css" media="all">
<link rel="icon" type="image/png" href="assets/img/favicon.png" sizes="16x16">
<link rel="icon" type="image/png" href="assets/img/favicon.png" sizes="32x32">
<!--<link rel="icon" type="image/png" href="assets/img/favicon-16x16.png" sizes="16x16">-->
<!--<link rel="icon" type="image/png" href="assets/img/favicon-32x32.png" sizes="32x32">-->
<!-- kendo UI -->
<link rel="stylesheet" href="bower_components/kendo-ui/styles/kendo.common-material.min.css"/>
<link rel="stylesheet" href="bower_components/kendo-ui/styles/kendo.material.min.css"/>
        
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">


<style type="text/css">
    .textName {
        font-size: 18px !important;
    }
    .emptyName{
        font-size: 19px !important;
        margin-top: 5% !important;
        margin-left: 40% !important;
    }
    .createCircle {
        width: 20px;
        height: 20px;
        border-radius: 25px;
        cursor: pointer;
        float: right;
        margin-top: -20px;
    }
    .redCircle {
        background: red;
    }
    .orangeCircle {
        background: orange;
    }
    .greenCircle {
        background: green;
    }
    .blackCircle {
        background: black;
    }
    .saveBtn{
        margin-top: 30px;
    }
    .headerDiv{
        color: white;
        font-size: 20px;
        padding-left: 10%;
    }
    .logoIcon{
        height: 40px;
        margin-top: 4px;
        margin-right: 80px;
        float: left;
    }
    .machineLi>a{
        height: 44px !important;
        line-height: 40px !important;
        display: block !important;
        color: white !important;
        padding: 3px 16px !important;
        min-width: 60px !important;
        text-align: center !important;
        font-size: 20px !important;
    }
    .machineHeaderLi{
        background-color: white !important;
        height: 48px !important;
    }
    .headerTitle{
        color: black !important;
    }
    .imgIconView{
        height: 105px !important;
        width: 100% !important;
    }

</style>

<?php
    }
?>
<!--Common CSS End-->



<!--Common Header Start-->
<?php
    function common_Header(){
?>

<header id="header_main" style="margin-left: 0px;">
    <div class="header_main_content">
        <nav class="uk-navbar">
            <div class="uk-navbar">
                <ul id="menu_top" class="uk-clearfix uk-float-left">
                    <img src="assets/img/universalLogo.gif" class="logoIcon">
                    <li class="machineLi <?php if(basename($_SERVER['PHP_SELF'])=='ListMachine.php'){echo 'machineHeaderLi';} ?>">
                        <a href="ListMachine.php"><span class="<?php if(basename($_SERVER['PHP_SELF'])=='ListMachine.php'){echo 'headerTitle';} ?>">Machine</span></a>
                    </li>
                </ul>
                <ul class="uk-navbar-nav user_actions uk-float-right">
                    <li data-uk-dropdown="{mode:'click',pos:'bottom-right'}">
                        <a href="#" class="user_action_image">
                            <img class="md-user-image" src="assets/img/userIcon.png"/>
                        </a>
                        <div class="uk-dropdown uk-dropdown-small">
                            <ul class="uk-nav js-uk-prevent">
                                <?php
                                if(roleCheck()){
                                ?>
                                <li><a href="machineDetail.php">Admin</a></li>
                                <li><a href="setting.php">Setting</a></li>
                                <?php }?>
                                <li><a href="index.php">Logout</a></li>
                            </ul>
                        </div>
                    </li>
                </ul>
            </div>
        </nav>
    </div>
    <div class="header_main_search_form">
        <i class="md-icon header_main_search_close material-icons">&#xE5CD;</i>
        <form class="uk-form">
            <input type="text" class="header_main_search_input"/>
            <button class="header_main_search_btn uk-button-link"><i class="md-icon material-icons">&#xE8B6;</i>
            </button>
        </form>
    </div>
</header>

<?php
    }
?>
<!--Common Header End-->




<!--Common JS Start-->
<?php
    function common_JS(){
?>

<!-- common functions -->
        <script src="assets/js/common.min.js"></script>
<!-- uikit functions -->
<script src="assets/js/uikit_custom.min.js"></script>
<!-- altair common functions/helpers -->
<!-- kendo UI -->
<script src="assets/js/kendoui_custom.min.js"></script>
<!--  kendoui functions -->
<script src="assets/js/pages/kendoui.min.js"></script>
<script src="assets/js/altair_admin_common.min.js"></script>

        <script src="custom_js/common.js"></script>
<!-- matchMedia polyfill for testing media queries in JS -->
<!--[if lte IE 9]>
<script type="text/javascript" src="bower_components/matchMedia/matchMedia.js"></script>
<script type="text/javascript" src="bower_components/matchMedia/matchMedia.addListener.js"></script>
<![endif]-->
        
<?php
    }
    function roleCheck(){
        $status=false;
        $accessToken=$_COOKIE['accessToken'];
        $accessToken=str_replace('"',"",$accessToken);
        $header = array();
        $header[] = 'Content-length: 0';
        $header[] = 'Content-type: application/json';
        $header[] = 'Authorization: Bearer '.$accessToken;
        $ch = curl_init();
        $vars="pageName=sharedview-list";
        curl_setopt($ch, CURLOPT_URL,"http://devapi.thinklytics.io/public/api/universal/role");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS,$vars);  //Post Fields
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        $server_output = curl_exec ($ch);
        curl_close ($ch);
        return json_decode($server_output)->result;
    }
?>
<!--Common JS End-->
