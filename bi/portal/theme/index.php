<!doctype html>
<!--[if lte IE 9]> <html class="lte-ie9" lang="en"> <![endif]-->
<!--[if gt IE 9]><!--> <html lang="en"> <!--<![endif]-->
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Remove Tap Highlight on Windows Phone IE -->
        <meta name="msapplication-tap-highlight" content="no"/>
        <link rel="icon" type="image/png" href="" sizes="16x16">
        <link rel="icon" type="image/png" href="" sizes="32x32">
        <title>Thinklytics</title>
        <link href='http://fonts.googleapis.com/css?family=Roboto:300,400,500' rel='stylesheet' type='text/css'>
        <!-- uikit -->
        <link rel="stylesheet" href="bower_components/uikit/css/uikit.almost-flat.min.css"/>
        <!-- altair admin login page -->
        <link rel="stylesheet" href="assets/css/login_page.min.css" />
        <style type="text/css">
            body{
                background-image: url("assets/img/loginBackground.jpg");
                background-size: cover;
            }
        </style>
    </head>

    <body class="login_page">
        <div class="login_page_wrapper">
            <div class="md-card" id="login_card">
                <div class="md-card-content large-padding" id="login_form">
                    <div class="login_heading">
                        <img src="assets/img/universalLogo.gif">
                    </div>
                    <form method="post" action="#">
                        <div class="uk-form-row">
                            <label for="login_username">Username</label>
                            <input class="md-input" type="email" id="login_username" name="login_username" required />
                        </div>
                        <div class="uk-form-row">
                            <label for="login_password">Password</label>
                            <input class="md-input" type="password" id="login_password" name="login_password" required />
                        </div>
                        <div class="uk-margin-medium-top">
                            <button type="submit" class="md-btn md-btn-primary md-btn-block md-btn-large">Sign In</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    <!-- common functions -->
    <script src="assets/js/common.min.js"></script>
    <!-- altair core functions -->
    <script src="assets/js/altair_admin_common.min.js"></script>
    <!-- altair login page functions -->
    <script src="assets/js/pages/login.min.js"></script>
    <script src="custom_js/common.js"></script>
    <script>
        $('form').submit(function(event) {
            var data = {
                "email":$('input[name=login_username]').val(),
                "password":$('input[name=login_password]').val()
            };
            request("api/login","post",data).done(function (response) {
                console.log(response);
                if(response.errorCode){
                    setCookie("accessToken",'%22'+response.result.access_token+'%22',1);
                    setCookie("companyId",'%22'+response.result.company_id+'%22',1);
                    setCookie("userId",response.result.id,1);
                    setCookie("userPort",response.result.port,1);
                    setCookie("accessPage",'%22'+"sharedView"+'%22',1);
                    window.location="http://dev.thinklytics.io/portal/theme/ListMachine.php";
                }
            });
            event.preventDefault();
        });
    </script>

    </body>
</html>