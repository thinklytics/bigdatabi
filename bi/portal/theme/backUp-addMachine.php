<?php
    require_once('common.php');
?>


<!doctype html>
<!--[if lte IE 9]> <html class="lte-ie9" lang="en"> <![endif]-->
<!--[if gt IE 9]><!--> <html lang="en"> <!--<![endif]-->
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Remove Tap Highlight on Windows Phone IE -->
        <meta name="msapplication-tap-highlight" content="no"/>
        <title>Thinklytics</title>

        <?php
            common_CSS();
        ?>
    </head>

    <body class=" sidebar_main_open sidebar_main_swipe">

    <?php
        common_Header();
    ?>


<div id="page_content" style="margin-left: 0px;">
    <div id="page_content_inner">


        <div class="uk-grid" data-uk-grid-margin>
            <div class="uk-width-medium-1-2">
                <div class="md-card">
                    <div class="md-card-content">
                        <h3 class="heading_a">Add Machine Details</h3>
                        <div class="uk-grid" data-uk-grid-margin>
                            <div class="uk-width-large-1-2 uk-width-medium-1-2">
                                <div class="uk-input-group">
                                    <label>Total Machines</label>
                                    <input type="number" min="0" class="md-input" id="totalMachine" />
                                </div>
                            </div>
                            <div class="uk-width-large-1-2 uk-width-medium-1-2">
                                <div class="uk-input-group">
                                    <button class="md-btn md-btn-primary md-btn-wave-light" onclick="addMachineRow();">Add</button>
                                    <button class="md-btn md-btn-primary md-btn-wave-light" id="saveMachine" onclick="saveMachine();">Save Machine Details</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <br>

        <div>
            <form id="MachineContainer" name="MachineContainer" enctype="multipart/form-data" role="form" method="POST" action="">

            <!-- Add Machine Detail -->
            </form>
        </div>

    </div>
</div>



    <?php
        common_JS();
    ?>

    <script type="text/javascript">
        var macCount;
        function addMachineRow() {
            $('#MachineContainer').html('');
            macCount = $('#totalMachine').val();
            var divStr = '';
            for(var i = 1; i<=macCount; i++){
                divStr += '<div class="md-card"><div class="md-card-content"><h3 class="heading_a">Machine ' + i + ' Details</h3><div class="uk-grid" data-uk-grid-margin><div class="uk-width-medium-1-2"><div class="uk-form-row"><div class="uk-grid"><div class="uk-width-medium-1-2"><label>Machine ID</label><input type="text" id="mac_ID_'+i+'" class="md-input" /></div><div class="uk-width-medium-1-2"><label>Machine Name</label><input type="text" id="mac_Name_'+i+'" class="md-input" /></div></div></div></div><div class="uk-width-medium-1-2"><div class="uk-form-row"><div class="uk-grid"><div class="uk-width-medium-1-2"><label>Machine Description</label><input type="text" id="mac_Desc_'+i+'" class="md-input" /></div><div class="uk-width-medium-1-2"><div class="uk-form-file md-btn md-btn-primary">Upload<input name="mac_File_'+i+'"  id="file_Upload_'+i+'" type="file"></div></div></div></div></div></div></div></div>';
            }
            console.log(divStr)
            $('#MachineContainer').append(divStr);
        }

        function saveMachine() {
            var macDetail = [];
            for(var i=1; i<=macCount; i++){
                var obj = {};
                obj['id']  = $('#mac_ID_'+i).val();
                obj['name'] = $('#mac_Name_'+i).val();
                obj['desc'] = $('#mac_Desc_'+i).val();
                macDetail.push(obj);
            }
            var data = {
                'pageName' : 'sharedview-list',
                'machineDetail' : macDetail,
            };
            // request("api/universal/machineAdd","post",data).done(function (response){
            //     if(response.errorCode == 1){
            //         alert(response.message);
            //     }
            // });


            for(var i=1; i<=macCount; i++){
                $("#file_Upload_"+i).click(function(){
                    var fd = new FormData();
                    console.log($('#mac_File_'+i))
                    var files = $('#mac_File_'+i)[0].files[0];
                    fd.append('file',files);
                    $.ajax({
                        url: 'upload.php',
                        type: 'post',
                        data: fd,
                        contentType: false,
                        processData: false,
                        success: function(response){
                            console.log(response)
                            if(response != 0){
                                $("#img").attr("src",response);
                                $(".preview img").show(); // Display image element
                            }else{
                                alert('file not uploaded');
                            }
                        },
                    });
                });
            }


        }


    </script>

    </body>
</html>
