var globalVar=0;
$("#toggleMenubar").on('click',function(){
	 if($('body').attr('class')=="animasition site-menubar-unfold"){
		 $('body').addClass("site-menubar-hide");
		 $('body').removeClass("site-menubar-unfold");
		 $('.navbar-toggler-left').addClass("hided");
		 
	 }
	 else{
		 $('body').addClass("site-menubar-unfold"); 
		 $('body').removeClass("site-menubar-hide");
		 $('.navbar-toggler-left').removeClass("hided");
	 }
	 globalVar=1;
	 setTimeout(function(){
		 $(window).resize();
	 },300);
});  
function resizeLogic() {
	
	if(globalVar==0){ 
		if($(window).width() < 820) {
	    	$('body').addClass("site-menubar-hide");
		 	$('body').removeClass("site-menubar-unfold");
		 	$('.navbar-toggler-left').addClass("hided");
	    } else {
	    	$('body').addClass("site-menubar-unfold"); 
		 	$('body').removeClass("site-menubar-hide");
		 	$('.navbar-toggler-left').removeClass("hided");
	    }
	} 
	 
	globalVar=0;
} 
resizeLogic(); // on load
$(window).resize(resizeLogic); // on window resize