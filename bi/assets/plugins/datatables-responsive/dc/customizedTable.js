// function customizedTable(data,dimensions,measures,container,crossfilter) {
//     console.log(crossfilter);
//     var keyOfObj = "";
//     var data = [];
//     if (!$.isEmptyObject(dimensions)) {
//
//         $.each(dimensions, function (key, val) {
//             keyOfObj = key;
//         });
//
//         var dim = crossfilter
//             .dimension(function (d) {
//                 return d[dimensions[keyOfObj].value];
//             });
//
//         data = jQuery.extend(true, [], dim.top(Infinity));
//
//     }
//     else {
//
//         data = jQuery.extend(true, [], draw._data);
//     }
//
//     var tableHeaders = "";
//     var k = 0;
//
//     var ColumnArrayForDataTable = [];
//     var keys = [];
//     try {
//         $.each(dimensions, function (key, value) {
//             keys.push(value);
//         });
//     }
//     catch (e) {
//         console.log(e);
//     }
//
//     try {
//         $.each(measures, function (key, value) {
//             keys.push(value);
//         });
//     }
//     catch (e) {
//         console.log(e);
//     }
//
// // data.forEach(function (d) {
// //     $.each(d, function (key, value) {
// //         if (keys.indexOf(key) === -1)
// //             delete d[key];
// //     });
// // });
//     var columnNames = [];
//
//     keys.forEach(function (d) {
//         columnNames.push(d['columnName']);
//         // var obj = new Object({
//         //     "mData": d,
//         //     "sDefaultContent": ""
//         // });
//
//         //  ColumnArrayForDataTable.push(obj);
//         tableHeaders += "<th>"
//             + '<a href="javascript:void(0)"  style="float:right" class="toggle-vis" data-column="'
//             + k + '"></a>' + d['columnName'] + "</th>";
//         k++;
//     });
//     var initialKey = columnNames[0];
//     $("#chart-" + container).html("");
//
//
//     $("#chart-" + container)
//         .append('<table id="table-'
//             + draw._container
//             + '" class="table table-bordered bordered table-striped table-condensed"    style="width: 100%;margin-left: 24px;"><thead><tr>'
//             + tableHeaders + '</tr></thead></table>');
//     var p = "<tr>";
//
//     var fieldsArray = keys;
//     var keyIndex = 0;
//     var TotalObject = {};
//
//     function sum(key, processedObj, d) {
//
//         if (processedObj["total_" + key] == undefined) {
//             processedObj["total_" + key] = 0;
//         }
//         processedObj["total"] = {123: {}};
//     }
//
//     aggergateKeyIndexes = [];
//     if (draw._tableSetting) {
//         $.each(draw._tableSetting.footer, function (k, v) {
//             if (columnNames.indexOf(k) != -1) {
//                 aggergateKeyIndexes.push(columnNames.indexOf(k));
//             }
//         });
//
//
//     }
//     var aggregates = {};
//     var subAggregates = {};
//
//     function doAggregate(operation, key, d) {
//         if (operation == "sum") {
//             if (!aggregates[key]) {
//                 aggregates[key] = 0;
//                 if (parseFloat(d[key]))
//                     aggregates[key] += parseFloat(d[key]);
//             } else {
//                 if (parseFloat(d[key]))
//                     aggregates[key] += parseFloat(d[key]);
//             }
//         } else if (operation == "count") {
//             if (!aggregates[key]) {
//                 aggregates[key] = 0;
//                 aggregates[key] += 1;
//             } else {
//                 aggregates[key] += 1;
//             }
//         } else if (operation == "avg") {
//             if (!aggregates[key]) {
//                 aggregates[key] = 0;
//                 if (parseFloat(d[key])) {
//                     aggregates[key] += parseFloat(d[key]);
//                 }
//             } else {
//                 if (parseFloat(d[key]))
//                     aggregates[key] += parseFloat(d[key]);
//             }
//         }
//
//     }
//
// //Sub string Calculate
//     function doSubAggregate(operation, d, initialKey, columnNames, index) {
//         if (operation == "sum") {
//             if (!subAggregates[d[initialKey]]) {
//                 subAggregates[d[initialKey]] = {};
//                 if (parseFloat(d[columnNames[index]]))
//                     subAggregates[d[initialKey]][columnNames[index]] = parseFloat(d[columnNames[index]]);
//                 else
//                     subAggregates[d[initialKey]][columnNames[index]] = 0;
//             }
//             else {
//                 if (parseFloat(d[columnNames[index]])) {
//
//                     if (!subAggregates[d[initialKey]][columnNames[index]]) {
//                         subAggregates[d[initialKey]][columnNames[index]] = 0;
//                     }
//                     subAggregates[d[initialKey]][columnNames[index]] += parseFloat(d[columnNames[index]]);
//                 }
//             }
//         } else if (operation == "count") {
//             if (!subAggregates[d[initialKey]]) {
//                 subAggregates[d[initialKey]] = {};
//
//                 if (parseFloat(d[columnNames[index]]))
//                     subAggregates[d[initialKey]][columnNames[index]] = 1;
//                 else
//                     subAggregates[d[initialKey]][columnNames[index]] = 0;
//             }
//             else {
//                 if (parseFloat(d[columnNames[index]])) {
//
//                     if (!subAggregates[d[initialKey]][columnNames[index]]) {
//                         subAggregates[d[initialKey]][columnNames[index]] = 0;
//                     }
//                     subAggregates[d[initialKey]][columnNames[index]] += 1;
//                 }
//             }
//         } else if (operation == "avg") {
//             if (!subAggregates[d[initialKey]]) {
//                 subAggregates[d[initialKey]] = {};
//
//                 if (parseFloat(d[columnNames[index]]))
//                     subAggregates[d[initialKey]][columnNames[index]] = parseFloat(d[columnNames[index]]);
//                 else
//                     subAggregates[d[initialKey]][columnNames[index]] = 0;
//             }
//             else {
//                 if (parseFloat(d[columnNames[index]])) {
//
//                     if (!subAggregates[d[initialKey]][columnNames[index]]) {
//                         subAggregates[d[initialKey]][columnNames[index]] = 0;
//                     }
//                     subAggregates[d[initialKey]][columnNames[index]] += parseFloat(d[columnNames[index]]);
//                 }
//             }
//         }
//     }
//
//     function group(d, index, processedObj) {
//
//         try {
//             var newIndex = index + 1;
//             var key = fieldsArray[index]['columnName'];
//             var keyType = fieldsArray[index]['dataKey'].trim();
//
//             if (draw._tableSetting) {
//                 if (aggergateKeyIndexes.indexOf(index) != -1) {
//                     $.each(draw._tableSetting.footer, function (k, v) {
//                         if (key === k) {
//                             doSubAggregate(v.operation, d, initialKey, columnNames, index);
//                         }
//                     });
//                 }
//                 $.each(draw._tableSetting.footer, function (k, v) {
//                     if (key === k) {
//                         doAggregate(v.operation, key, d);
//                     }
//                 });
//             }
//             console.log("Processing Started... with index ", index, " having current object ", Object.assign({}, processedObj));
//             if (!processedObj[d[key]]) {
//                 console.log("new value found... with index ", index, " having current object ", Object.assign({}, processedObj));
//                 if (index < fieldsArray.length - 1) {
//                     var currentKey = d[key];
//                     console.log("entered in fieldsArray... with index ", index, " having current object ", Object.assign({}, processedObj));
//                     if (keyType === 'Measure' && index != 0) {
//                         console.log("entered in fieldsArray , measure... with index ", index, " having current object ", Object.assign({}, processedObj));
//
//
//                         if (!$.isEmptyObject(processedObj)) {
//
//                             var lastKey = Object.keys(processedObj)[0];
//
//                             var newKey = parseInt(lastKey) + parseInt(d[key]);
//
//
//                             currentKey = newKey;
//
//                             processedObj[newKey] = processedObj[lastKey];
//                             delete processedObj[lastKey];
//
//
//                         } else {
//                             processedObj[currentKey] = {};
//                         }
//
//                     }
//                     else {
//                         processedObj[currentKey] = {};
//                     }
//                     processedObj[currentKey] = group(d, newIndex, processedObj[currentKey]);
//
//                     return processedObj;
//                 }
//                 else {
//                     if (keyType === 'Measure' && index != 0) {
//                         console.log("entered in fieldsArray , measure... with index ", index, " having current object ", Object.assign({}, processedObj));
//
//
//                         if (!$.isEmptyObject(processedObj)) {
//
//                             var lastKey = Object.keys(processedObj)[0];
//
//                             var newKey = parseInt(lastKey) + parseInt(d[key]);
//
//
//                             currentKey = newKey;
//
//                             processedObj[newKey] = processedObj[lastKey];
//                             delete processedObj[lastKey];
//
//
//                         } else {
//
//                             processedObj[d[key]] = {};
//                         }
//
//                     }
//                     else {
//                         processedObj[d[key]] = {};
//                     }
//                     console.log("else entered in fieldsArray... with index ", index, " having current object ", Object.assign({}, processedObj));
//                     return processedObj;
//                 }
//             } else {
//                 console.log("old key found... with index ", index, " having current object ", Object.assign({}, processedObj));
//                 if (index < fieldsArray.length - 1) {
//                     var currentKey = d[key];
//                     console.log("entered in fieldsArray , measure... with index ", index, " having current object ", Object.assign({}, processedObj));
//                     if (keyType === 'Measure' && index != 0) {
//
//                         if (!$.isEmptyObject(processedObj)) {
//                             var lastKey = Object.keys(processedObj)[0];
//
//                             var newKey = parseFloat(lastKey) + parseFloat(d[key]);
//
//                             currentKey = newKey;
//                             processedObj[newKey] = processedObj[lastKey];
//                             delete processedObj[lastKey];
//
//                         } else {
//                             console.log("else entered in fieldsArray... with index ", index, " having current object ", Object.assign({}, processedObj));
//                         }
//
//                     } else {
//                         console.log("else entered in fieldsArray , measure... with index ", index, " having current object ", Object.assign({}, processedObj));
//                         processedObj[currentKey] = {};
//
//                     }
//
//                     processedObj[currentKey] = group(d, newIndex, processedObj[currentKey]);
//
//                 }
//                 else {
//                     console.log("else entered in fieldsArray... with index ", index, " having current object ", Object.assign({}, processedObj));
//
//                     if (keyType === 'Measure') {
//
//
//                         if (!$.isEmptyObject(processedObj)) {
//
//                             var lastKey = Object.keys(processedObj)[0];
//
//                             var newKey = parseFloat(lastKey) + parseFloat(d[key]);
//                             currentKey = newKey;
//                             processedObj[currentKey] = {};
//                             delete processedObj[lastKey];
//
//                         } else {
//                             processedObj[currentKey] = {};
//                         }
//                     }
//                     return processedObj;
//                 }
//             }
//         } catch (e) {
//             console.log(e);
//         }
//     }
//
//     var finalObj = {};
//     data.forEach(function (d, i) {
//
//         group(d, 0, finalObj);
//     });
//     console.log(finalObj);
//     var isObject = function (a) {
//         if ($.isEmptyObject(a))
//             return false
//         else
//             return (!!a) && (a.constructor === Object);
//
//     };
//
//     function getAllChildren(group, children) {
//         children = children || [];
//         if (group && isObject(group)) {
//             $.each(group, function (key, child) {
//                 getAllChildren(child, children)
//             })
//         }
//         else {
//             children.push(group);
//         }
//         return children;
//     }
//
//     var p = "";
//     var r = "";
//     $.each(finalObj, function (key, value) {
//         r = "";
//         p += "<tr>";
//         var rowLength = getAllChildren(value).length;
//         var tableData = key.split('#');
//         p += "<td rowspan='" + rowLength + "'>" + tableData[0] + "</td>";
//         p += rowObj(value);
//         if ((!$.isEmptyObject(subAggregates[key])) && draw._tableSetting.footer.subtotal == 1) {
//             p += "<tr class='subtotal'>";
//             columnNames.forEach(function (d) {
//                 if (subAggregates[key][d])
//                     p += "<th> Sub" + draw._tableSetting.footer[d]['operation'] + " : " + subAggregates[key][d] + "</th>";
//                 else
//                     p += "<th>&nbsp;</th>";
//             });
//             p += "</tr>";
//         }
//     });
// //aggregates
//     if (!$.isEmptyObject(aggregates)) {
//         p += "<tr>";
//         columnNames.forEach(function (d) {
//             if (aggregates[d]) {
//                 console.log(draw._tableSetting.footer[d]['operation']);
//                 p += "<th> Total " + draw._tableSetting.footer[d]['operation'] + " : " + aggregates[d] + "</th>";
//             }
//             else
//                 p += "<th>&nbsp;</th>";
//         });
//         p += "</tr>";
//     }
//     function rowObj(value) {
//         var i = 0;
//         $.each(value, function (k, v) {
//             i++;
//             var rLength = 1;
//
//             if (isObject(v)) {
//
//                 rLength = getAllChildren(v).length;
//             }
//             if (i === 1) {
//                 var tableData = k.split('#');
//                 r += "<td rowspan='" + rLength + "'>" + tableData[0] + "</td>";
//                 if (isObject(v)) {
//
//                     rowObj(v);
//                 } else {
//                     r += "</tr>";
//                 }
//             }
//             else {
//                 var tableData = k.split('#');
//                 r += "<tr><td rowspan='" + rLength + "'>" + tableData[0] + "</td>";
//                 if (isObject(v)) {
//                     rowObj(v);
//                 } else {
//                     r += "</tr>";
//                 }
//             }
//         });
//         return r;
//     }
//
//     $("#table-" + draw._container).append("<tbody>" + p + "</tbody>");
//     var keyOfObj = "";
//     var data = [];
//     if (!$.isEmptyObject(draw._dimension)) {
//
//         $.each(draw._dimension, function (key, val) {
//             keyOfObj = key;
//         });
//
//         var dim = draw._crossfilter
//             .dimension(function (d) {
//                 return d[draw._dimension[keyOfObj].value];
//             });
//
//         data = jQuery.extend(true, [], dim.top(Infinity));
//
//     }
//
//
//     var tableHeaders = "";
//     var k = 0;
//
//     var ColumnArrayForDataTable = [];
//     var keys = [];
//     try {
//         $.each(draw._dimension, function (key, value) {
//             keys.push(value);
//         });
//     }
//     catch (e) {
//         console.log(e);
//     }
//
//     try {
//         $.each(draw._measure, function (key, value) {
//             keys.push(value);
//         });
//     }
//     catch (e) {
//         console.log(e);
//     }
//
// // data.forEach(function (d) {
// //     $.each(d, function (key, value) {
// //         if (keys.indexOf(key) === -1)
// //             delete d[key];
// //     });
// // });
//     var columnNames = [];
//
//     keys.forEach(function (d) {
//         columnNames.push(d['columnName']);
//         // var obj = new Object({
//         //     "mData": d,
//         //     "sDefaultContent": ""
//         // });
//
//         //  ColumnArrayForDataTable.push(obj);
//         tableHeaders += "<th>"
//             + '<a href="javascript:void(0)"  style="float:right" class="toggle-vis" data-column="'
//             + k + '"></a>' + d['columnName'] + "</th>";
//         k++;
//     });
//     var initialKey = columnNames[0];
//     $("#chart-" + draw._container).html("");
//
//
//     $("#chart-" + draw._container)
//         .append('<table id="table-'
//             + draw._container
//             + '" class="table table-bordered bordered table-striped table-condensed"    style="width: 100%;margin-left: 24px;"><thead><tr>'
//             + tableHeaders + '</tr></thead></table>');
//     var p = "<tr>";
//
//     var fieldsArray = keys;
//     var keyIndex = 0;
//     var TotalObject = {};
//
//     function sum(key, processedObj, d) {
//
//         if (processedObj["total_" + key] == undefined) {
//             processedObj["total_" + key] = 0;
//         }
//         processedObj["total"] = {123: {}};
//     }
//
//     aggergateKeyIndexes = [];
//     if (draw._tableSetting) {
//         $.each(draw._tableSetting.footer, function (k, v) {
//             if (columnNames.indexOf(k) != -1) {
//                 aggergateKeyIndexes.push(columnNames.indexOf(k));
//             }
//         });
//
//
//     }
//     var aggregates = {};
//     var subAggregates = {};
//
//     function doAggregate(operation, key, d) {
//         if (operation == "sum") {
//             if (!aggregates[key]) {
//                 aggregates[key] = 0;
//                 if (parseFloat(d[key]))
//                     aggregates[key] += parseFloat(d[key]);
//             } else {
//                 if (parseFloat(d[key]))
//                     aggregates[key] += parseFloat(d[key]);
//             }
//         } else if (operation == "count") {
//             if (!aggregates[key]) {
//                 aggregates[key] = 0;
//                 aggregates[key] += 1;
//             } else {
//                 aggregates[key] += 1;
//             }
//         } else if (operation == "avg") {
//             if (!aggregates[key]) {
//                 aggregates[key] = 0;
//                 if (parseFloat(d[key])) {
//                     aggregates[key] += parseFloat(d[key]);
//                 }
//             } else {
//                 if (parseFloat(d[key]))
//                     aggregates[key] += parseFloat(d[key]);
//             }
//         }
//
//     }
//
// //Sub string Calculate
//     function doSubAggregate(operation, d, initialKey, columnNames, index) {
//         if (operation == "sum") {
//             if (!subAggregates[d[initialKey]]) {
//                 subAggregates[d[initialKey]] = {};
//                 if (parseFloat(d[columnNames[index]]))
//                     subAggregates[d[initialKey]][columnNames[index]] = parseFloat(d[columnNames[index]]);
//                 else
//                     subAggregates[d[initialKey]][columnNames[index]] = 0;
//             }
//             else {
//                 if (parseFloat(d[columnNames[index]])) {
//
//                     if (!subAggregates[d[initialKey]][columnNames[index]]) {
//                         subAggregates[d[initialKey]][columnNames[index]] = 0;
//                     }
//                     subAggregates[d[initialKey]][columnNames[index]] += parseFloat(d[columnNames[index]]);
//                 }
//             }
//         } else if (operation == "count") {
//             if (!subAggregates[d[initialKey]]) {
//                 subAggregates[d[initialKey]] = {};
//
//                 if (parseFloat(d[columnNames[index]]))
//                     subAggregates[d[initialKey]][columnNames[index]] = 1;
//                 else
//                     subAggregates[d[initialKey]][columnNames[index]] = 0;
//             }
//             else {
//                 if (parseFloat(d[columnNames[index]])) {
//
//                     if (!subAggregates[d[initialKey]][columnNames[index]]) {
//                         subAggregates[d[initialKey]][columnNames[index]] = 0;
//                     }
//                     subAggregates[d[initialKey]][columnNames[index]] += 1;
//                 }
//             }
//         } else if (operation == "avg") {
//             if (!subAggregates[d[initialKey]]) {
//                 subAggregates[d[initialKey]] = {};
//
//                 if (parseFloat(d[columnNames[index]]))
//                     subAggregates[d[initialKey]][columnNames[index]] = parseFloat(d[columnNames[index]]);
//                 else
//                     subAggregates[d[initialKey]][columnNames[index]] = 0;
//             }
//             else {
//                 if (parseFloat(d[columnNames[index]])) {
//
//                     if (!subAggregates[d[initialKey]][columnNames[index]]) {
//                         subAggregates[d[initialKey]][columnNames[index]] = 0;
//                     }
//                     subAggregates[d[initialKey]][columnNames[index]] += parseFloat(d[columnNames[index]]);
//                 }
//             }
//         }
//     }
//
//     function group(d, index, processedObj) {
//
//         try {
//             var newIndex = index + 1;
//             var key = fieldsArray[index]['columnName'];
//             var keyType = fieldsArray[index]['dataKey'].trim();
//
//             if (draw._tableSetting) {
//                 if (aggergateKeyIndexes.indexOf(index) != -1) {
//                     $.each(draw._tableSetting.footer, function (k, v) {
//                         if (key === k) {
//                             doSubAggregate(v.operation, d, initialKey, columnNames, index);
//                         }
//                     });
//                 }
//                 $.each(draw._tableSetting.footer, function (k, v) {
//                     if (key === k) {
//                         doAggregate(v.operation, key, d);
//                     }
//                 });
//             }
//             console.log("Processing Started... with index ", index, " having current object ", Object.assign({}, processedObj));
//             if (!processedObj[d[key]]) {
//                 console.log("new value found... with index ", index, " having current object ", Object.assign({}, processedObj));
//                 if (index < fieldsArray.length - 1) {
//                     var currentKey = d[key];
//                     console.log("entered in fieldsArray... with index ", index, " having current object ", Object.assign({}, processedObj));
//                     if (keyType === 'Measure' && index != 0) {
//                         console.log("entered in fieldsArray , measure... with index ", index, " having current object ", Object.assign({}, processedObj));
//
//
//                         if (!$.isEmptyObject(processedObj)) {
//
//                             var lastKey = Object.keys(processedObj)[0];
//
//                             var newKey = parseInt(lastKey) + parseInt(d[key]);
//
//
//                             currentKey = newKey;
//
//                             processedObj[newKey] = processedObj[lastKey];
//                             delete processedObj[lastKey];
//
//
//                         } else {
//                             processedObj[currentKey] = {};
//                         }
//
//                     }
//                     else {
//                         processedObj[currentKey] = {};
//                     }
//                     processedObj[currentKey] = group(d, newIndex, processedObj[currentKey]);
//
//                     return processedObj;
//                 }
//                 else {
//                     if (keyType === 'Measure' && index != 0) {
//                         console.log("entered in fieldsArray , measure... with index ", index, " having current object ", Object.assign({}, processedObj));
//
//
//                         if (!$.isEmptyObject(processedObj)) {
//
//                             var lastKey = Object.keys(processedObj)[0];
//
//                             var newKey = parseInt(lastKey) + parseInt(d[key]);
//
//
//                             currentKey = newKey;
//
//                             processedObj[newKey] = processedObj[lastKey];
//                             delete processedObj[lastKey];
//
//
//                         } else {
//
//                             processedObj[d[key]] = {};
//                         }
//
//                     }
//                     else {
//                         processedObj[d[key]] = {};
//                     }
//                     console.log("else entered in fieldsArray... with index ", index, " having current object ", Object.assign({}, processedObj));
//                     return processedObj;
//                 }
//             } else {
//                 console.log("old key found... with index ", index, " having current object ", Object.assign({}, processedObj));
//                 if (index < fieldsArray.length - 1) {
//                     var currentKey = d[key];
//                     console.log("entered in fieldsArray , measure... with index ", index, " having current object ", Object.assign({}, processedObj));
//                     if (keyType === 'Measure' && index != 0) {
//
//                         if (!$.isEmptyObject(processedObj)) {
//                             var lastKey = Object.keys(processedObj)[0];
//
//                             var newKey = parseFloat(lastKey) + parseFloat(d[key]);
//
//                             currentKey = newKey;
//                             processedObj[newKey] = processedObj[lastKey];
//                             delete processedObj[lastKey];
//
//                         } else {
//                             console.log("else entered in fieldsArray... with index ", index, " having current object ", Object.assign({}, processedObj));
//                         }
//
//                     } else {
//                         console.log("else entered in fieldsArray , measure... with index ", index, " having current object ", Object.assign({}, processedObj));
//                         processedObj[currentKey] = {};
//
//                     }
//
//                     processedObj[currentKey] = group(d, newIndex, processedObj[currentKey]);
//
//                 }
//                 else {
//                     console.log("else entered in fieldsArray... with index ", index, " having current object ", Object.assign({}, processedObj));
//
//                     if (keyType === 'Measure') {
//
//
//                         if (!$.isEmptyObject(processedObj)) {
//
//                             var lastKey = Object.keys(processedObj)[0];
//
//                             var newKey = parseFloat(lastKey) + parseFloat(d[key]);
//                             currentKey = newKey;
//                             processedObj[currentKey] = {};
//                             delete processedObj[lastKey];
//
//                         } else {
//                             processedObj[currentKey] = {};
//                         }
//                     }
//                     return processedObj;
//                 }
//             }
//         } catch (e) {
//             console.log(e);
//         }
//     }
//
//     var finalObj = {};
//     data.forEach(function (d, i) {
//
//         group(d, 0, finalObj);
//     });
//     console.log(finalObj);
//     var isObject = function (a) {
//         if ($.isEmptyObject(a))
//             return false
//         else
//             return (!!a) && (a.constructor === Object);
//
//     };
//
//     function getAllChildren(group, children) {
//         children = children || [];
//         if (group && isObject(group)) {
//             $.each(group, function (key, child) {
//                 getAllChildren(child, children)
//             })
//         }
//         else {
//             children.push(group);
//         }
//         return children;
//     }
//
//     var p = "";
//     var r = "";
//     $.each(finalObj, function (key, value) {
//         r = "";
//         p += "<tr>";
//         var rowLength = getAllChildren(value).length;
//         var tableData = key.split('#');
//         p += "<td rowspan='" + rowLength + "'>" + tableData[0] + "</td>";
//         p += rowObj(value);
//         if ((!$.isEmptyObject(subAggregates[key])) && draw._tableSetting.footer.subtotal == 1) {
//             p += "<tr class='subtotal'>";
//             columnNames.forEach(function (d) {
//                 if (subAggregates[key][d])
//                     p += "<th> Sub" + draw._tableSetting.footer[d]['operation'] + " : " + subAggregates[key][d] + "</th>";
//                 else
//                     p += "<th>&nbsp;</th>";
//             });
//             p += "</tr>";
//         }
//     });
// //aggregates
//     if (!$.isEmptyObject(aggregates)) {
//         p += "<tr>";
//         columnNames.forEach(function (d) {
//             if (aggregates[d]) {
//                 console.log(draw._tableSetting.footer[d]['operation']);
//                 p += "<th> Total " + draw._tableSetting.footer[d]['operation'] + " : " + aggregates[d] + "</th>";
//             }
//             else
//                 p += "<th>&nbsp;</th>";
//         });
//         p += "</tr>";
//     }
//     function rowObj(value) {
//         var i = 0;
//         $.each(value, function (k, v) {
//             i++;
//             var rLength = 1;
//
//             if (isObject(v)) {
//
//                 rLength = getAllChildren(v).length;
//             }
//             if (i === 1) {
//                 var tableData = k.split('#');
//                 r += "<td rowspan='" + rLength + "'>" + tableData[0] + "</td>";
//                 if (isObject(v)) {
//
//                     rowObj(v);
//                 } else {
//                     r += "</tr>";
//                 }
//             }
//             else {
//                 var tableData = k.split('#');
//                 r += "<tr><td rowspan='" + rLength + "'>" + tableData[0] + "</td>";
//                 if (isObject(v)) {
//                     rowObj(v);
//                 } else {
//                     r += "</tr>";
//                 }
//             }
//         });
//         return r;
//     }
//
//     $("#table-" + draw._container).append("<tbody>" + p + "</tbody>");
//     var keyOfObj = "";
//     var data = [];
//     if (!$.isEmptyObject(draw._dimension)) {
//
//         $.each(draw._dimension, function (key, val) {
//             keyOfObj = key;
//         });
//
//         var dim = draw._crossfilter
//             .dimension(function (d) {
//                 return d[draw._dimension[keyOfObj].value];
//             });
//
//         data = jQuery.extend(true, [], dim.top(Infinity));
//
//     }
//     else {
//
//         data = jQuery.extend(true, [], draw._data);
//     }
//
//     var tableHeaders = "";
//     var k = 0;
//
//     var ColumnArrayForDataTable = [];
//     var keys = [];
//     try {
//         $.each(draw._dimension, function (key, value) {
//             keys.push(value);
//         });
//     }
//     catch (e) {
//         console.log(e);
//     }
//
//     try {
//         $.each(draw._measure, function (key, value) {
//             keys.push(value);
//         });
//     }
//     catch (e) {
//         console.log(e);
//     }
//
// // data.forEach(function (d) {
// //     $.each(d, function (key, value) {
// //         if (keys.indexOf(key) === -1)
// //             delete d[key];
// //     });
// // });
//     var columnNames = [];
//
//     keys.forEach(function (d) {
//         columnNames.push(d['columnName']);
//         // var obj = new Object({
//         //     "mData": d,
//         //     "sDefaultContent": ""
//         // });
//
//         //  ColumnArrayForDataTable.push(obj);
//         tableHeaders += "<th>"
//             + '<a href="javascript:void(0)"  style="float:right" class="toggle-vis" data-column="'
//             + k + '"></a>' + d['columnName'] + "</th>";
//         k++;
//     });
//     var initialKey = columnNames[0];
//     $("#chart-" + draw._container).html("");
//
//
//     $("#chart-" + draw._container)
//         .append('<table id="table-'
//             + draw._container
//             + '" class="table table-bordered bordered table-striped table-condensed"    style="width: 100%;margin-left: 24px;"><thead><tr>'
//             + tableHeaders + '</tr></thead></table>');
//     var p = "<tr>";
//
//     var fieldsArray = keys;
//     var keyIndex = 0;
//     var TotalObject = {};
//
//     function sum(key, processedObj, d) {
//
//         if (processedObj["total_" + key] == undefined) {
//             processedObj["total_" + key] = 0;
//         }
//         processedObj["total"] = {123: {}};
//     }
//
//     aggergateKeyIndexes = [];
//     if (draw._tableSetting) {
//         $.each(draw._tableSetting.footer, function (k, v) {
//             if (columnNames.indexOf(k) != -1) {
//                 aggergateKeyIndexes.push(columnNames.indexOf(k));
//             }
//         });
//
//
//     }
//     var aggregates = {};
//     var subAggregates = {};
//
//     function doAggregate(operation, key, d) {
//         if (operation == "sum") {
//             if (!aggregates[key]) {
//                 aggregates[key] = 0;
//                 if (parseFloat(d[key]))
//                     aggregates[key] += parseFloat(d[key]);
//             } else {
//                 if (parseFloat(d[key]))
//                     aggregates[key] += parseFloat(d[key]);
//             }
//         } else if (operation == "count") {
//             if (!aggregates[key]) {
//                 aggregates[key] = 0;
//                 aggregates[key] += 1;
//             } else {
//                 aggregates[key] += 1;
//             }
//         } else if (operation == "avg") {
//             if (!aggregates[key]) {
//                 aggregates[key] = 0;
//                 if (parseFloat(d[key])) {
//                     aggregates[key] += parseFloat(d[key]);
//                 }
//             } else {
//                 if (parseFloat(d[key]))
//                     aggregates[key] += parseFloat(d[key]);
//             }
//         }
//
//     }
//
// //Sub string Calculate
//     function doSubAggregate(operation, d, initialKey, columnNames, index) {
//         if (operation == "sum") {
//             if (!subAggregates[d[initialKey]]) {
//                 subAggregates[d[initialKey]] = {};
//                 if (parseFloat(d[columnNames[index]]))
//                     subAggregates[d[initialKey]][columnNames[index]] = parseFloat(d[columnNames[index]]);
//                 else
//                     subAggregates[d[initialKey]][columnNames[index]] = 0;
//             }
//             else {
//                 if (parseFloat(d[columnNames[index]])) {
//
//                     if (!subAggregates[d[initialKey]][columnNames[index]]) {
//                         subAggregates[d[initialKey]][columnNames[index]] = 0;
//                     }
//                     subAggregates[d[initialKey]][columnNames[index]] += parseFloat(d[columnNames[index]]);
//                 }
//             }
//         } else if (operation == "count") {
//             if (!subAggregates[d[initialKey]]) {
//                 subAggregates[d[initialKey]] = {};
//
//                 if (parseFloat(d[columnNames[index]]))
//                     subAggregates[d[initialKey]][columnNames[index]] = 1;
//                 else
//                     subAggregates[d[initialKey]][columnNames[index]] = 0;
//             }
//             else {
//                 if (parseFloat(d[columnNames[index]])) {
//
//                     if (!subAggregates[d[initialKey]][columnNames[index]]) {
//                         subAggregates[d[initialKey]][columnNames[index]] = 0;
//                     }
//                     subAggregates[d[initialKey]][columnNames[index]] += 1;
//                 }
//             }
//         } else if (operation == "avg") {
//             if (!subAggregates[d[initialKey]]) {
//                 subAggregates[d[initialKey]] = {};
//
//                 if (parseFloat(d[columnNames[index]]))
//                     subAggregates[d[initialKey]][columnNames[index]] = parseFloat(d[columnNames[index]]);
//                 else
//                     subAggregates[d[initialKey]][columnNames[index]] = 0;
//             }
//             else {
//                 if (parseFloat(d[columnNames[index]])) {
//
//                     if (!subAggregates[d[initialKey]][columnNames[index]]) {
//                         subAggregates[d[initialKey]][columnNames[index]] = 0;
//                     }
//                     subAggregates[d[initialKey]][columnNames[index]] += parseFloat(d[columnNames[index]]);
//                 }
//             }
//         }
//     }
//
//     function group(d, index, processedObj) {
//
//         try {
//             var newIndex = index + 1;
//             var key = fieldsArray[index]['columnName'];
//             var keyType = fieldsArray[index]['dataKey'].trim();
//
//             if (draw._tableSetting) {
//                 if (aggergateKeyIndexes.indexOf(index) != -1) {
//                     $.each(draw._tableSetting.footer, function (k, v) {
//                         if (key === k) {
//                             doSubAggregate(v.operation, d, initialKey, columnNames, index);
//                         }
//                     });
//                 }
//                 $.each(draw._tableSetting.footer, function (k, v) {
//                     if (key === k) {
//                         doAggregate(v.operation, key, d);
//                     }
//                 });
//             }
//             console.log("Processing Started... with index ", index, " having current object ", Object.assign({}, processedObj));
//             if (!processedObj[d[key]]) {
//                 console.log("new value found... with index ", index, " having current object ", Object.assign({}, processedObj));
//                 if (index < fieldsArray.length - 1) {
//                     var currentKey = d[key];
//                     console.log("entered in fieldsArray... with index ", index, " having current object ", Object.assign({}, processedObj));
//                     if (keyType === 'Measure' && index != 0) {
//                         console.log("entered in fieldsArray , measure... with index ", index, " having current object ", Object.assign({}, processedObj));
//
//
//                         if (!$.isEmptyObject(processedObj)) {
//
//                             var lastKey = Object.keys(processedObj)[0];
//
//                             var newKey = parseInt(lastKey) + parseInt(d[key]);
//
//
//                             currentKey = newKey;
//
//                             processedObj[newKey] = processedObj[lastKey];
//                             delete processedObj[lastKey];
//
//
//                         } else {
//                             processedObj[currentKey] = {};
//                         }
//
//                     }
//                     else {
//                         processedObj[currentKey] = {};
//                     }
//                     processedObj[currentKey] = group(d, newIndex, processedObj[currentKey]);
//
//                     return processedObj;
//                 }
//                 else {
//                     if (keyType === 'Measure' && index != 0) {
//                         console.log("entered in fieldsArray , measure... with index ", index, " having current object ", Object.assign({}, processedObj));
//
//
//                         if (!$.isEmptyObject(processedObj)) {
//
//                             var lastKey = Object.keys(processedObj)[0];
//
//                             var newKey = parseInt(lastKey) + parseInt(d[key]);
//
//
//                             currentKey = newKey;
//
//                             processedObj[newKey] = processedObj[lastKey];
//                             delete processedObj[lastKey];
//
//
//                         } else {
//
//                             processedObj[d[key]] = {};
//                         }
//
//                     }
//                     else {
//                         processedObj[d[key]] = {};
//                     }
//                     console.log("else entered in fieldsArray... with index ", index, " having current object ", Object.assign({}, processedObj));
//                     return processedObj;
//                 }
//             } else {
//                 console.log("old key found... with index ", index, " having current object ", Object.assign({}, processedObj));
//                 if (index < fieldsArray.length - 1) {
//                     var currentKey = d[key];
//                     console.log("entered in fieldsArray , measure... with index ", index, " having current object ", Object.assign({}, processedObj));
//                     if (keyType === 'Measure' && index != 0) {
//
//                         if (!$.isEmptyObject(processedObj)) {
//                             var lastKey = Object.keys(processedObj)[0];
//
//                             var newKey = parseFloat(lastKey) + parseFloat(d[key]);
//
//                             currentKey = newKey;
//                             processedObj[newKey] = processedObj[lastKey];
//                             delete processedObj[lastKey];
//
//                         } else {
//                             console.log("else entered in fieldsArray... with index ", index, " having current object ", Object.assign({}, processedObj));
//                         }
//
//                     } else {
//                         console.log("else entered in fieldsArray , measure... with index ", index, " having current object ", Object.assign({}, processedObj));
//                         processedObj[currentKey] = {};
//
//                     }
//
//                     processedObj[currentKey] = group(d, newIndex, processedObj[currentKey]);
//
//                 }
//                 else {
//                     console.log("else entered in fieldsArray... with index ", index, " having current object ", Object.assign({}, processedObj));
//
//                     if (keyType === 'Measure') {
//
//
//                         if (!$.isEmptyObject(processedObj)) {
//
//                             var lastKey = Object.keys(processedObj)[0];
//
//                             var newKey = parseFloat(lastKey) + parseFloat(d[key]);
//                             currentKey = newKey;
//                             processedObj[currentKey] = {};
//                             delete processedObj[lastKey];
//
//                         } else {
//                             processedObj[currentKey] = {};
//                         }
//                     }
//                     return processedObj;
//                 }
//             }
//         } catch (e) {
//             console.log(e);
//         }
//     }
//
//     var finalObj = {};
//     data.forEach(function (d, i) {
//
//         group(d, 0, finalObj);
//     });
//     console.log(finalObj);
//     var isObject = function (a) {
//         if ($.isEmptyObject(a))
//             return false
//         else
//             return (!!a) && (a.constructor === Object);
//
//     };
//
//     function getAllChildren(group, children) {
//         children = children || [];
//         if (group && isObject(group)) {
//             $.each(group, function (key, child) {
//                 getAllChildren(child, children)
//             })
//         }
//         else {
//             children.push(group);
//         }
//         return children;
//     }
//
//     var p = "";
//     var r = "";
//     $.each(finalObj, function (key, value) {
//         r = "";
//         p += "<tr>";
//         var rowLength = getAllChildren(value).length;
//         var tableData = key.split('#');
//         p += "<td rowspan='" + rowLength + "'>" + tableData[0] + "</td>";
//         p += rowObj(value);
//         if ((!$.isEmptyObject(subAggregates[key])) && draw._tableSetting.footer.subtotal == 1) {
//             p += "<tr class='subtotal'>";
//             columnNames.forEach(function (d) {
//                 if (subAggregates[key][d])
//                     p += "<th> Sub" + draw._tableSetting.footer[d]['operation'] + " : " + subAggregates[key][d] + "</th>";
//                 else
//                     p += "<th>&nbsp;</th>";
//             });
//             p += "</tr>";
//         }
//     });
// //aggregates
//     if (!$.isEmptyObject(aggregates)) {
//         p += "<tr>";
//         columnNames.forEach(function (d) {
//             if (aggregates[d]) {
//                 console.log(draw._tableSetting.footer[d]['operation']);
//                 p += "<th> Total " + draw._tableSetting.footer[d]['operation'] + " : " + aggregates[d] + "</th>";
//             }
//             else
//                 p += "<th>&nbsp;</th>";
//         });
//         p += "</tr>";
//     }
//     function rowObj(value) {
//         var i = 0;
//         $.each(value, function (k, v) {
//             i++;
//             var rLength = 1;
//
//             if (isObject(v)) {
//
//                 rLength = getAllChildren(v).length;
//             }
//             if (i === 1) {
//                 var tableData = k.split('#');
//                 r += "<td rowspan='" + rLength + "'>" + tableData[0] + "</td>";
//                 if (isObject(v)) {
//
//                     rowObj(v);
//                 } else {
//                     r += "</tr>";
//                 }
//             }
//             else {
//                 var tableData = k.split('#');
//                 r += "<tr><td rowspan='" + rLength + "'>" + tableData[0] + "</td>";
//                 if (isObject(v)) {
//                     rowObj(v);
//                 } else {
//                     r += "</tr>";
//                 }
//             }
//         });
//         return r;
//     }
//
//     $("#table-" + draw._container).append("<tbody>" + p + "</tbody>");
//     var keyOfObj = "";
//     var data = [];
//     if (!$.isEmptyObject(draw._dimension)) {
//
//         $.each(draw._dimension, function (key, val) {
//             keyOfObj = key;
//         });
//
//         var dim = draw._crossfilter
//             .dimension(function (d) {
//                 return d[draw._dimension[keyOfObj].value];
//             });
//
//         data = jQuery.extend(true, [], dim.top(Infinity));
//
//     }
//     else {
//
//         data = jQuery.extend(true, [], draw._data);
//     }
//
//     var tableHeaders = "";
//     var k = 0;
//
//     var ColumnArrayForDataTable = [];
//     var keys = [];
//     try {
//         $.each(draw._dimension, function (key, value) {
//             keys.push(value);
//         });
//     }
//     catch (e) {
//         console.log(e);
//     }
//
//     try {
//         $.each(draw._measure, function (key, value) {
//             keys.push(value);
//         });
//     }
//     catch (e) {
//         console.log(e);
//     }
//
// // data.forEach(function (d) {
// //     $.each(d, function (key, value) {
// //         if (keys.indexOf(key) === -1)
// //             delete d[key];
// //     });
// // });
//     var columnNames = [];
//
//     keys.forEach(function (d) {
//         columnNames.push(d['columnName']);
//         // var obj = new Object({
//         //     "mData": d,
//         //     "sDefaultContent": ""
//         // });
//
//         //  ColumnArrayForDataTable.push(obj);
//         tableHeaders += "<th>"
//             + '<a href="javascript:void(0)"  style="float:right" class="toggle-vis" data-column="'
//             + k + '"></a>' + d['columnName'] + "</th>";
//         k++;
//     });
//     var initialKey = columnNames[0];
//     $("#chart-" + draw._container).html("");
//
//
//     $("#chart-" + draw._container)
//         .append('<table id="table-'
//             + draw._container
//             + '" class="table table-bordered bordered table-striped table-condensed"    style="width: 100%;margin-left: 24px;"><thead><tr>'
//             + tableHeaders + '</tr></thead></table>');
//     var p = "<tr>";
//
//     var fieldsArray = keys;
//     var keyIndex = 0;
//     var TotalObject = {};
//
//     function sum(key, processedObj, d) {
//
//         if (processedObj["total_" + key] == undefined) {
//             processedObj["total_" + key] = 0;
//         }
//         processedObj["total"] = {123: {}};
//     }
//
//     aggergateKeyIndexes = [];
//     if (draw._tableSetting) {
//         $.each(draw._tableSetting.footer, function (k, v) {
//             if (columnNames.indexOf(k) != -1) {
//                 aggergateKeyIndexes.push(columnNames.indexOf(k));
//             }
//         });
//
//
//     }
//     var aggregates = {};
//     var subAggregates = {};
//
//     function doAggregate(operation, key, d) {
//         if (operation == "sum") {
//             if (!aggregates[key]) {
//                 aggregates[key] = 0;
//                 if (parseFloat(d[key]))
//                     aggregates[key] += parseFloat(d[key]);
//             } else {
//                 if (parseFloat(d[key]))
//                     aggregates[key] += parseFloat(d[key]);
//             }
//         } else if (operation == "count") {
//             if (!aggregates[key]) {
//                 aggregates[key] = 0;
//                 aggregates[key] += 1;
//             } else {
//                 aggregates[key] += 1;
//             }
//         } else if (operation == "avg") {
//             if (!aggregates[key]) {
//                 aggregates[key] = 0;
//                 if (parseFloat(d[key])) {
//                     aggregates[key] += parseFloat(d[key]);
//                 }
//             } else {
//                 if (parseFloat(d[key]))
//                     aggregates[key] += parseFloat(d[key]);
//             }
//         }
//
//     }
//
// //Sub string Calculate
//     function doSubAggregate(operation, d, initialKey, columnNames, index) {
//         if (operation == "sum") {
//             if (!subAggregates[d[initialKey]]) {
//                 subAggregates[d[initialKey]] = {};
//                 if (parseFloat(d[columnNames[index]]))
//                     subAggregates[d[initialKey]][columnNames[index]] = parseFloat(d[columnNames[index]]);
//                 else
//                     subAggregates[d[initialKey]][columnNames[index]] = 0;
//             }
//             else {
//                 if (parseFloat(d[columnNames[index]])) {
//
//                     if (!subAggregates[d[initialKey]][columnNames[index]]) {
//                         subAggregates[d[initialKey]][columnNames[index]] = 0;
//                     }
//                     subAggregates[d[initialKey]][columnNames[index]] += parseFloat(d[columnNames[index]]);
//                 }
//             }
//         } else if (operation == "count") {
//             if (!subAggregates[d[initialKey]]) {
//                 subAggregates[d[initialKey]] = {};
//
//                 if (parseFloat(d[columnNames[index]]))
//                     subAggregates[d[initialKey]][columnNames[index]] = 1;
//                 else
//                     subAggregates[d[initialKey]][columnNames[index]] = 0;
//             }
//             else {
//                 if (parseFloat(d[columnNames[index]])) {
//
//                     if (!subAggregates[d[initialKey]][columnNames[index]]) {
//                         subAggregates[d[initialKey]][columnNames[index]] = 0;
//                     }
//                     subAggregates[d[initialKey]][columnNames[index]] += 1;
//                 }
//             }
//         } else if (operation == "avg") {
//             if (!subAggregates[d[initialKey]]) {
//                 subAggregates[d[initialKey]] = {};
//
//                 if (parseFloat(d[columnNames[index]]))
//                     subAggregates[d[initialKey]][columnNames[index]] = parseFloat(d[columnNames[index]]);
//                 else
//                     subAggregates[d[initialKey]][columnNames[index]] = 0;
//             }
//             else {
//                 if (parseFloat(d[columnNames[index]])) {
//
//                     if (!subAggregates[d[initialKey]][columnNames[index]]) {
//                         subAggregates[d[initialKey]][columnNames[index]] = 0;
//                     }
//                     subAggregates[d[initialKey]][columnNames[index]] += parseFloat(d[columnNames[index]]);
//                 }
//             }
//         }
//     }
//
//     function group(d, index, processedObj) {
//
//         try {
//             var newIndex = index + 1;
//             var key = fieldsArray[index]['columnName'];
//             var keyType = fieldsArray[index]['dataKey'].trim();
//
//             if (draw._tableSetting) {
//                 if (aggergateKeyIndexes.indexOf(index) != -1) {
//                     $.each(draw._tableSetting.footer, function (k, v) {
//                         if (key === k) {
//                             doSubAggregate(v.operation, d, initialKey, columnNames, index);
//                         }
//                     });
//                 }
//                 $.each(draw._tableSetting.footer, function (k, v) {
//                     if (key === k) {
//                         doAggregate(v.operation, key, d);
//                     }
//                 });
//             }
//             console.log("Processing Started... with index ", index, " having current object ", Object.assign({}, processedObj));
//             if (!processedObj[d[key]]) {
//                 console.log("new value found... with index ", index, " having current object ", Object.assign({}, processedObj));
//                 if (index < fieldsArray.length - 1) {
//                     var currentKey = d[key];
//                     console.log("entered in fieldsArray... with index ", index, " having current object ", Object.assign({}, processedObj));
//                     if (keyType === 'Measure' && index != 0) {
//                         console.log("entered in fieldsArray , measure... with index ", index, " having current object ", Object.assign({}, processedObj));
//
//
//                         if (!$.isEmptyObject(processedObj)) {
//
//                             var lastKey = Object.keys(processedObj)[0];
//
//                             var newKey = parseInt(lastKey) + parseInt(d[key]);
//
//
//                             currentKey = newKey;
//
//                             processedObj[newKey] = processedObj[lastKey];
//                             delete processedObj[lastKey];
//
//
//                         } else {
//                             processedObj[currentKey] = {};
//                         }
//
//                     }
//                     else {
//                         processedObj[currentKey] = {};
//                     }
//                     processedObj[currentKey] = group(d, newIndex, processedObj[currentKey]);
//
//                     return processedObj;
//                 }
//                 else {
//                     if (keyType === 'Measure' && index != 0) {
//                         console.log("entered in fieldsArray , measure... with index ", index, " having current object ", Object.assign({}, processedObj));
//
//
//                         if (!$.isEmptyObject(processedObj)) {
//
//                             var lastKey = Object.keys(processedObj)[0];
//
//                             var newKey = parseInt(lastKey) + parseInt(d[key]);
//
//
//                             currentKey = newKey;
//
//                             processedObj[newKey] = processedObj[lastKey];
//                             delete processedObj[lastKey];
//
//
//                         } else {
//
//                             processedObj[d[key]] = {};
//                         }
//
//                     }
//                     else {
//                         processedObj[d[key]] = {};
//                     }
//                     console.log("else entered in fieldsArray... with index ", index, " having current object ", Object.assign({}, processedObj));
//                     return processedObj;
//                 }
//             } else {
//                 console.log("old key found... with index ", index, " having current object ", Object.assign({}, processedObj));
//                 if (index < fieldsArray.length - 1) {
//                     var currentKey = d[key];
//                     console.log("entered in fieldsArray , measure... with index ", index, " having current object ", Object.assign({}, processedObj));
//                     if (keyType === 'Measure' && index != 0) {
//
//                         if (!$.isEmptyObject(processedObj)) {
//                             var lastKey = Object.keys(processedObj)[0];
//
//                             var newKey = parseFloat(lastKey) + parseFloat(d[key]);
//
//                             currentKey = newKey;
//                             processedObj[newKey] = processedObj[lastKey];
//                             delete processedObj[lastKey];
//
//                         } else {
//                             console.log("else entered in fieldsArray... with index ", index, " having current object ", Object.assign({}, processedObj));
//                         }
//
//                     } else {
//                         console.log("else entered in fieldsArray , measure... with index ", index, " having current object ", Object.assign({}, processedObj));
//                         processedObj[currentKey] = {};
//
//                     }
//
//                     processedObj[currentKey] = group(d, newIndex, processedObj[currentKey]);
//
//                 }
//                 else {
//                     console.log("else entered in fieldsArray... with index ", index, " having current object ", Object.assign({}, processedObj));
//
//                     if (keyType === 'Measure') {
//
//
//                         if (!$.isEmptyObject(processedObj)) {
//
//                             var lastKey = Object.keys(processedObj)[0];
//
//                             var newKey = parseFloat(lastKey) + parseFloat(d[key]);
//                             currentKey = newKey;
//                             processedObj[currentKey] = {};
//                             delete processedObj[lastKey];
//
//                         } else {
//                             processedObj[currentKey] = {};
//                         }
//                     }
//                     return processedObj;
//                 }
//             }
//         } catch (e) {
//             console.log(e);
//         }
//     }
//
//     var finalObj = {};
//     data.forEach(function (d, i) {
//
//         group(d, 0, finalObj);
//     });
//     console.log(finalObj);
//     var isObject = function (a) {
//         if ($.isEmptyObject(a))
//             return false
//         else
//             return (!!a) && (a.constructor === Object);
//
//     };
//
//     function getAllChildren(group, children) {
//         children = children || [];
//         if (group && isObject(group)) {
//             $.each(group, function (key, child) {
//                 getAllChildren(child, children)
//             })
//         }
//         else {
//             children.push(group);
//         }
//         return children;
//     }
//
//     var p = "";
//     var r = "";
//     $.each(finalObj, function (key, value) {
//         r = "";
//         p += "<tr>";
//         var rowLength = getAllChildren(value).length;
//         var tableData = key.split('#');
//         p += "<td rowspan='" + rowLength + "'>" + tableData[0] + "</td>";
//         p += rowObj(value);
//         if ((!$.isEmptyObject(subAggregates[key])) && draw._tableSetting.footer.subtotal == 1) {
//             p += "<tr class='subtotal'>";
//             columnNames.forEach(function (d) {
//                 if (subAggregates[key][d])
//                     p += "<th> Sub" + draw._tableSetting.footer[d]['operation'] + " : " + subAggregates[key][d] + "</th>";
//                 else
//                     p += "<th>&nbsp;</th>";
//             });
//             p += "</tr>";
//         }
//     });
// //aggregates
//     if (!$.isEmptyObject(aggregates)) {
//         p += "<tr>";
//         columnNames.forEach(function (d) {
//             if (aggregates[d]) {
//                 console.log(draw._tableSetting.footer[d]['operation']);
//                 p += "<th> Total " + draw._tableSetting.footer[d]['operation'] + " : " + aggregates[d] + "</th>";
//             }
//             else
//                 p += "<th>&nbsp;</th>";
//         });
//         p += "</tr>";
//     }
//     function rowObj(value) {
//         var i = 0;
//         $.each(value, function (k, v) {
//             i++;
//             var rLength = 1;
//
//             if (isObject(v)) {
//
//                 rLength = getAllChildren(v).length;
//             }
//             if (i === 1) {
//                 var tableData = k.split('#');
//                 r += "<td rowspan='" + rLength + "'>" + tableData[0] + "</td>";
//                 if (isObject(v)) {
//
//                     rowObj(v);
//                 } else {
//                     r += "</tr>";
//                 }
//             }
//             else {
//                 var tableData = k.split('#');
//                 r += "<tr><td rowspan='" + rLength + "'>" + tableData[0] + "</td>";
//                 if (isObject(v)) {
//                     rowObj(v);
//                 } else {
//                     r += "</tr>";
//                 }
//             }
//         });
//         return r;
//     }
//
//     $("#table-" + draw._container).append("<tbody>" + p + "</tbody>");
//     var keyOfObj = "";
//     var data = [];
//     if (!$.isEmptyObject(draw._dimension)) {
//
//         $.each(draw._dimension, function (key, val) {
//             keyOfObj = key;
//         });
//
//         var dim = draw._crossfilter
//             .dimension(function (d) {
//                 return d[draw._dimension[keyOfObj].value];
//             });
//
//         data = jQuery.extend(true, [], dim.top(Infinity));
//
//     }
//     else {
//
//         data = jQuery.extend(true, [], draw._data);
//     }
//
//     var tableHeaders = "";
//     var k = 0;
//
//     var ColumnArrayForDataTable = [];
//     var keys = [];
//     try {
//         $.each(draw._dimension, function (key, value) {
//             keys.push(value);
//         });
//     }
//     catch (e) {
//         console.log(e);
//     }
//
//     try {
//         $.each(draw._measure, function (key, value) {
//             keys.push(value);
//         });
//     }
//     catch (e) {
//         console.log(e);
//     }
//
// // data.forEach(function (d) {
// //     $.each(d, function (key, value) {
// //         if (keys.indexOf(key) === -1)
// //             delete d[key];
// //     });
// // });
//     var columnNames = [];
//
//     keys.forEach(function (d) {
//         columnNames.push(d['columnName']);
//         // var obj = new Object({
//         //     "mData": d,
//         //     "sDefaultContent": ""
//         // });
//
//         //  ColumnArrayForDataTable.push(obj);
//         tableHeaders += "<th>"
//             + '<a href="javascript:void(0)"  style="float:right" class="toggle-vis" data-column="'
//             + k + '"></a>' + d['columnName'] + "</th>";
//         k++;
//     });
//     var initialKey = columnNames[0];
//     $("#chart-" + draw._container).html("");
//
//
//     $("#chart-" + draw._container)
//         .append('<table id="table-'
//             + draw._container
//             + '" class="table table-bordered bordered table-striped table-condensed"    style="width: 100%;margin-left: 24px;"><thead><tr>'
//             + tableHeaders + '</tr></thead></table>');
//     var p = "<tr>";
//
//     var fieldsArray = keys;
//     var keyIndex = 0;
//     var TotalObject = {};
//
//     function sum(key, processedObj, d) {
//
//         if (processedObj["total_" + key] == undefined) {
//             processedObj["total_" + key] = 0;
//         }
//         processedObj["total"] = {123: {}};
//     }
//
//     aggergateKeyIndexes = [];
//     if (draw._tableSetting) {
//         $.each(draw._tableSetting.footer, function (k, v) {
//             if (columnNames.indexOf(k) != -1) {
//                 aggergateKeyIndexes.push(columnNames.indexOf(k));
//             }
//         });
//
//
//     }
//     var aggregates = {};
//     var subAggregates = {};
//
//     function doAggregate(operation, key, d) {
//         if (operation == "sum") {
//             if (!aggregates[key]) {
//                 aggregates[key] = 0;
//                 if (parseFloat(d[key]))
//                     aggregates[key] += parseFloat(d[key]);
//             } else {
//                 if (parseFloat(d[key]))
//                     aggregates[key] += parseFloat(d[key]);
//             }
//         } else if (operation == "count") {
//             if (!aggregates[key]) {
//                 aggregates[key] = 0;
//                 aggregates[key] += 1;
//             } else {
//                 aggregates[key] += 1;
//             }
//         } else if (operation == "avg") {
//             if (!aggregates[key]) {
//                 aggregates[key] = 0;
//                 if (parseFloat(d[key])) {
//                     aggregates[key] += parseFloat(d[key]);
//                 }
//             } else {
//                 if (parseFloat(d[key]))
//                     aggregates[key] += parseFloat(d[key]);
//             }
//         }
//
//     }
//
// //Sub string Calculate
//     function doSubAggregate(operation, d, initialKey, columnNames, index) {
//         if (operation == "sum") {
//             if (!subAggregates[d[initialKey]]) {
//                 subAggregates[d[initialKey]] = {};
//                 if (parseFloat(d[columnNames[index]]))
//                     subAggregates[d[initialKey]][columnNames[index]] = parseFloat(d[columnNames[index]]);
//                 else
//                     subAggregates[d[initialKey]][columnNames[index]] = 0;
//             }
//             else {
//                 if (parseFloat(d[columnNames[index]])) {
//
//                     if (!subAggregates[d[initialKey]][columnNames[index]]) {
//                         subAggregates[d[initialKey]][columnNames[index]] = 0;
//                     }
//                     subAggregates[d[initialKey]][columnNames[index]] += parseFloat(d[columnNames[index]]);
//                 }
//             }
//         } else if (operation == "count") {
//             if (!subAggregates[d[initialKey]]) {
//                 subAggregates[d[initialKey]] = {};
//
//                 if (parseFloat(d[columnNames[index]]))
//                     subAggregates[d[initialKey]][columnNames[index]] = 1;
//                 else
//                     subAggregates[d[initialKey]][columnNames[index]] = 0;
//             }
//             else {
//                 if (parseFloat(d[columnNames[index]])) {
//
//                     if (!subAggregates[d[initialKey]][columnNames[index]]) {
//                         subAggregates[d[initialKey]][columnNames[index]] = 0;
//                     }
//                     subAggregates[d[initialKey]][columnNames[index]] += 1;
//                 }
//             }
//         } else if (operation == "avg") {
//             if (!subAggregates[d[initialKey]]) {
//                 subAggregates[d[initialKey]] = {};
//
//                 if (parseFloat(d[columnNames[index]]))
//                     subAggregates[d[initialKey]][columnNames[index]] = parseFloat(d[columnNames[index]]);
//                 else
//                     subAggregates[d[initialKey]][columnNames[index]] = 0;
//             }
//             else {
//                 if (parseFloat(d[columnNames[index]])) {
//
//                     if (!subAggregates[d[initialKey]][columnNames[index]]) {
//                         subAggregates[d[initialKey]][columnNames[index]] = 0;
//                     }
//                     subAggregates[d[initialKey]][columnNames[index]] += parseFloat(d[columnNames[index]]);
//                 }
//             }
//         }
//     }
//
//     function group(d, index, processedObj) {
//
//         try {
//             var newIndex = index + 1;
//             var key = fieldsArray[index]['columnName'];
//             var keyType = fieldsArray[index]['dataKey'].trim();
//
//             if (draw._tableSetting) {
//                 if (aggergateKeyIndexes.indexOf(index) != -1) {
//                     $.each(draw._tableSetting.footer, function (k, v) {
//                         if (key === k) {
//                             doSubAggregate(v.operation, d, initialKey, columnNames, index);
//                         }
//                     });
//                 }
//                 $.each(draw._tableSetting.footer, function (k, v) {
//                     if (key === k) {
//                         doAggregate(v.operation, key, d);
//                     }
//                 });
//             }
//             console.log("Processing Started... with index ", index, " having current object ", Object.assign({}, processedObj));
//             if (!processedObj[d[key]]) {
//                 console.log("new value found... with index ", index, " having current object ", Object.assign({}, processedObj));
//                 if (index < fieldsArray.length - 1) {
//                     var currentKey = d[key];
//                     console.log("entered in fieldsArray... with index ", index, " having current object ", Object.assign({}, processedObj));
//                     if (keyType === 'Measure' && index != 0) {
//                         console.log("entered in fieldsArray , measure... with index ", index, " having current object ", Object.assign({}, processedObj));
//
//
//                         if (!$.isEmptyObject(processedObj)) {
//
//                             var lastKey = Object.keys(processedObj)[0];
//
//                             var newKey = parseInt(lastKey) + parseInt(d[key]);
//
//
//                             currentKey = newKey;
//
//                             processedObj[newKey] = processedObj[lastKey];
//                             delete processedObj[lastKey];
//
//
//                         } else {
//                             processedObj[currentKey] = {};
//                         }
//
//                     }
//                     else {
//                         processedObj[currentKey] = {};
//                     }
//                     processedObj[currentKey] = group(d, newIndex, processedObj[currentKey]);
//
//                     return processedObj;
//                 }
//                 else {
//                     if (keyType === 'Measure' && index != 0) {
//                         console.log("entered in fieldsArray , measure... with index ", index, " having current object ", Object.assign({}, processedObj));
//
//
//                         if (!$.isEmptyObject(processedObj)) {
//
//                             var lastKey = Object.keys(processedObj)[0];
//
//                             var newKey = parseInt(lastKey) + parseInt(d[key]);
//
//
//                             currentKey = newKey;
//
//                             processedObj[newKey] = processedObj[lastKey];
//                             delete processedObj[lastKey];
//
//
//                         } else {
//
//                             processedObj[d[key]] = {};
//                         }
//
//                     }
//                     else {
//                         processedObj[d[key]] = {};
//                     }
//                     console.log("else entered in fieldsArray... with index ", index, " having current object ", Object.assign({}, processedObj));
//                     return processedObj;
//                 }
//             } else {
//                 console.log("old key found... with index ", index, " having current object ", Object.assign({}, processedObj));
//                 if (index < fieldsArray.length - 1) {
//                     var currentKey = d[key];
//                     console.log("entered in fieldsArray , measure... with index ", index, " having current object ", Object.assign({}, processedObj));
//                     if (keyType === 'Measure' && index != 0) {
//
//                         if (!$.isEmptyObject(processedObj)) {
//                             var lastKey = Object.keys(processedObj)[0];
//
//                             var newKey = parseFloat(lastKey) + parseFloat(d[key]);
//
//                             currentKey = newKey;
//                             processedObj[newKey] = processedObj[lastKey];
//                             delete processedObj[lastKey];
//
//                         } else {
//                             console.log("else entered in fieldsArray... with index ", index, " having current object ", Object.assign({}, processedObj));
//                         }
//
//                     } else {
//                         console.log("else entered in fieldsArray , measure... with index ", index, " having current object ", Object.assign({}, processedObj));
//                         processedObj[currentKey] = {};
//
//                     }
//
//                     processedObj[currentKey] = group(d, newIndex, processedObj[currentKey]);
//
//                 }
//                 else {
//                     console.log("else entered in fieldsArray... with index ", index, " having current object ", Object.assign({}, processedObj));
//
//                     if (keyType === 'Measure') {
//
//
//                         if (!$.isEmptyObject(processedObj)) {
//
//                             var lastKey = Object.keys(processedObj)[0];
//
//                             var newKey = parseFloat(lastKey) + parseFloat(d[key]);
//                             currentKey = newKey;
//                             processedObj[currentKey] = {};
//                             delete processedObj[lastKey];
//
//                         } else {
//                             processedObj[currentKey] = {};
//                         }
//                     }
//                     return processedObj;
//                 }
//             }
//         } catch (e) {
//             console.log(e);
//         }
//     }
//
//     var finalObj = {};
//     data.forEach(function (d, i) {
//
//         group(d, 0, finalObj);
//     });
//     console.log(finalObj);
//     var isObject = function (a) {
//         if ($.isEmptyObject(a))
//             return false
//         else
//             return (!!a) && (a.constructor === Object);
//
//     };
//
//     function getAllChildren(group, children) {
//         children = children || [];
//         if (group && isObject(group)) {
//             $.each(group, function (key, child) {
//                 getAllChildren(child, children)
//             })
//         }
//         else {
//             children.push(group);
//         }
//         return children;
//     }
//
//     var p = "";
//     var r = "";
//     $.each(finalObj, function (key, value) {
//         r = "";
//         p += "<tr>";
//         var rowLength = getAllChildren(value).length;
//         var tableData = key.split('#');
//         p += "<td rowspan='" + rowLength + "'>" + tableData[0] + "</td>";
//         p += rowObj(value);
//         if ((!$.isEmptyObject(subAggregates[key])) && draw._tableSetting.footer.subtotal == 1) {
//             p += "<tr class='subtotal'>";
//             columnNames.forEach(function (d) {
//                 if (subAggregates[key][d])
//                     p += "<th> Sub" + draw._tableSetting.footer[d]['operation'] + " : " + subAggregates[key][d] + "</th>";
//                 else
//                     p += "<th>&nbsp;</th>";
//             });
//             p += "</tr>";
//         }
//     });
// //aggregates
//     if (!$.isEmptyObject(aggregates)) {
//         p += "<tr>";
//         columnNames.forEach(function (d) {
//             if (aggregates[d]) {
//                 console.log(draw._tableSetting.footer[d]['operation']);
//                 p += "<th> Total " + draw._tableSetting.footer[d]['operation'] + " : " + aggregates[d] + "</th>";
//             }
//             else
//                 p += "<th>&nbsp;</th>";
//         });
//         p += "</tr>";
//     }
//     function rowObj(value) {
//         var i = 0;
//         $.each(value, function (k, v) {
//             i++;
//             var rLength = 1;
//
//             if (isObject(v)) {
//
//                 rLength = getAllChildren(v).length;
//             }
//             if (i === 1) {
//                 var tableData = k.split('#');
//                 r += "<td rowspan='" + rLength + "'>" + tableData[0] + "</td>";
//                 if (isObject(v)) {
//
//                     rowObj(v);
//                 } else {
//                     r += "</tr>";
//                 }
//             }
//             else {
//                 var tableData = k.split('#');
//                 r += "<tr><td rowspan='" + rLength + "'>" + tableData[0] + "</td>";
//                 if (isObject(v)) {
//                     rowObj(v);
//                 } else {
//                     r += "</tr>";
//                 }
//             }
//         });
//         return r;
//     }
//
//     $("#table-" + draw._container).append("<tbody>" + p + "</tbody>");
//     var keyOfObj = "";
//     var data = [];
//     if (!$.isEmptyObject(draw._dimension)) {
//
//         $.each(draw._dimension, function (key, val) {
//             keyOfObj = key;
//         });
//
//         var dim = draw._crossfilter
//             .dimension(function (d) {
//                 return d[draw._dimension[keyOfObj].value];
//             });
//
//         data = jQuery.extend(true, [], dim.top(Infinity));
//
//     }
//     else {
//
//         data = jQuery.extend(true, [], draw._data);
//     }
//
//     var tableHeaders = "";
//     var k = 0;
//
//     var ColumnArrayForDataTable = [];
//     var keys = [];
//     try {
//         $.each(draw._dimension, function (key, value) {
//             keys.push(value);
//         });
//     }
//     catch (e) {
//         console.log(e);
//     }
//
//     try {
//         $.each(draw._measure, function (key, value) {
//             keys.push(value);
//         });
//     }
//     catch (e) {
//         console.log(e);
//     }
//
// // data.forEach(function (d) {
// //     $.each(d, function (key, value) {
// //         if (keys.indexOf(key) === -1)
// //             delete d[key];
// //     });
// // });
//     var columnNames = [];
//
//     keys.forEach(function (d) {
//         columnNames.push(d['columnName']);
//         // var obj = new Object({
//         //     "mData": d,
//         //     "sDefaultContent": ""
//         // });
//
//         //  ColumnArrayForDataTable.push(obj);
//         tableHeaders += "<th>"
//             + '<a href="javascript:void(0)"  style="float:right" class="toggle-vis" data-column="'
//             + k + '"></a>' + d['columnName'] + "</th>";
//         k++;
//     });
//     var initialKey = columnNames[0];
//     $("#chart-" + draw._container).html("");
//
//
//     $("#chart-" + draw._container)
//         .append('<table id="table-'
//             + draw._container
//             + '" class="table table-bordered bordered table-striped table-condensed"    style="width: 100%;margin-left: 24px;"><thead><tr>'
//             + tableHeaders + '</tr></thead></table>');
//     var p = "<tr>";
//
//     var fieldsArray = keys;
//     var keyIndex = 0;
//     var TotalObject = {};
//
//     function sum(key, processedObj, d) {
//
//         if (processedObj["total_" + key] == undefined) {
//             processedObj["total_" + key] = 0;
//         }
//         processedObj["total"] = {123: {}};
//     }
//
//     aggergateKeyIndexes = [];
//     if (draw._tableSetting) {
//         $.each(draw._tableSetting.footer, function (k, v) {
//             if (columnNames.indexOf(k) != -1) {
//                 aggergateKeyIndexes.push(columnNames.indexOf(k));
//             }
//         });
//
//
//     }
//     var aggregates = {};
//     var subAggregates = {};
//
//     function doAggregate(operation, key, d) {
//         if (operation == "sum") {
//             if (!aggregates[key]) {
//                 aggregates[key] = 0;
//                 if (parseFloat(d[key]))
//                     aggregates[key] += parseFloat(d[key]);
//             } else {
//                 if (parseFloat(d[key]))
//                     aggregates[key] += parseFloat(d[key]);
//             }
//         } else if (operation == "count") {
//             if (!aggregates[key]) {
//                 aggregates[key] = 0;
//                 aggregates[key] += 1;
//             } else {
//                 aggregates[key] += 1;
//             }
//         } else if (operation == "avg") {
//             if (!aggregates[key]) {
//                 aggregates[key] = 0;
//                 if (parseFloat(d[key])) {
//                     aggregates[key] += parseFloat(d[key]);
//                 }
//             } else {
//                 if (parseFloat(d[key]))
//                     aggregates[key] += parseFloat(d[key]);
//             }
//         }
//
//     }
//
// //Sub string Calculate
//     function doSubAggregate(operation, d, initialKey, columnNames, index) {
//         if (operation == "sum") {
//             if (!subAggregates[d[initialKey]]) {
//                 subAggregates[d[initialKey]] = {};
//                 if (parseFloat(d[columnNames[index]]))
//                     subAggregates[d[initialKey]][columnNames[index]] = parseFloat(d[columnNames[index]]);
//                 else
//                     subAggregates[d[initialKey]][columnNames[index]] = 0;
//             }
//             else {
//                 if (parseFloat(d[columnNames[index]])) {
//
//                     if (!subAggregates[d[initialKey]][columnNames[index]]) {
//                         subAggregates[d[initialKey]][columnNames[index]] = 0;
//                     }
//                     subAggregates[d[initialKey]][columnNames[index]] += parseFloat(d[columnNames[index]]);
//                 }
//             }
//         } else if (operation == "count") {
//             if (!subAggregates[d[initialKey]]) {
//                 subAggregates[d[initialKey]] = {};
//
//                 if (parseFloat(d[columnNames[index]]))
//                     subAggregates[d[initialKey]][columnNames[index]] = 1;
//                 else
//                     subAggregates[d[initialKey]][columnNames[index]] = 0;
//             }
//             else {
//                 if (parseFloat(d[columnNames[index]])) {
//
//                     if (!subAggregates[d[initialKey]][columnNames[index]]) {
//                         subAggregates[d[initialKey]][columnNames[index]] = 0;
//                     }
//                     subAggregates[d[initialKey]][columnNames[index]] += 1;
//                 }
//             }
//         } else if (operation == "avg") {
//             if (!subAggregates[d[initialKey]]) {
//                 subAggregates[d[initialKey]] = {};
//
//                 if (parseFloat(d[columnNames[index]]))
//                     subAggregates[d[initialKey]][columnNames[index]] = parseFloat(d[columnNames[index]]);
//                 else
//                     subAggregates[d[initialKey]][columnNames[index]] = 0;
//             }
//             else {
//                 if (parseFloat(d[columnNames[index]])) {
//
//                     if (!subAggregates[d[initialKey]][columnNames[index]]) {
//                         subAggregates[d[initialKey]][columnNames[index]] = 0;
//                     }
//                     subAggregates[d[initialKey]][columnNames[index]] += parseFloat(d[columnNames[index]]);
//                 }
//             }
//         }
//     }
//
//     function group(d, index, processedObj) {
//
//         try {
//             var newIndex = index + 1;
//             var key = fieldsArray[index]['columnName'];
//             var keyType = fieldsArray[index]['dataKey'].trim();
//
//             if (draw._tableSetting) {
//                 if (aggergateKeyIndexes.indexOf(index) != -1) {
//                     $.each(draw._tableSetting.footer, function (k, v) {
//                         if (key === k) {
//                             doSubAggregate(v.operation, d, initialKey, columnNames, index);
//                         }
//                     });
//                 }
//                 $.each(draw._tableSetting.footer, function (k, v) {
//                     if (key === k) {
//                         doAggregate(v.operation, key, d);
//                     }
//                 });
//             }
//             console.log("Processing Started... with index ", index, " having current object ", Object.assign({}, processedObj));
//             if (!processedObj[d[key]]) {
//                 console.log("new value found... with index ", index, " having current object ", Object.assign({}, processedObj));
//                 if (index < fieldsArray.length - 1) {
//                     var currentKey = d[key];
//                     console.log("entered in fieldsArray... with index ", index, " having current object ", Object.assign({}, processedObj));
//                     if (keyType === 'Measure' && index != 0) {
//                         console.log("entered in fieldsArray , measure... with index ", index, " having current object ", Object.assign({}, processedObj));
//
//
//                         if (!$.isEmptyObject(processedObj)) {
//
//                             var lastKey = Object.keys(processedObj)[0];
//
//                             var newKey = parseInt(lastKey) + parseInt(d[key]);
//
//
//                             currentKey = newKey;
//
//                             processedObj[newKey] = processedObj[lastKey];
//                             delete processedObj[lastKey];
//
//
//                         } else {
//                             processedObj[currentKey] = {};
//                         }
//
//                     }
//                     else {
//                         processedObj[currentKey] = {};
//                     }
//                     processedObj[currentKey] = group(d, newIndex, processedObj[currentKey]);
//
//                     return processedObj;
//                 }
//                 else {
//                     if (keyType === 'Measure' && index != 0) {
//                         console.log("entered in fieldsArray , measure... with index ", index, " having current object ", Object.assign({}, processedObj));
//
//
//                         if (!$.isEmptyObject(processedObj)) {
//
//                             var lastKey = Object.keys(processedObj)[0];
//
//                             var newKey = parseInt(lastKey) + parseInt(d[key]);
//
//
//                             currentKey = newKey;
//
//                             processedObj[newKey] = processedObj[lastKey];
//                             delete processedObj[lastKey];
//
//
//                         } else {
//
//                             processedObj[d[key]] = {};
//                         }
//
//                     }
//                     else {
//                         processedObj[d[key]] = {};
//                     }
//                     console.log("else entered in fieldsArray... with index ", index, " having current object ", Object.assign({}, processedObj));
//                     return processedObj;
//                 }
//             } else {
//                 console.log("old key found... with index ", index, " having current object ", Object.assign({}, processedObj));
//                 if (index < fieldsArray.length - 1) {
//                     var currentKey = d[key];
//                     console.log("entered in fieldsArray , measure... with index ", index, " having current object ", Object.assign({}, processedObj));
//                     if (keyType === 'Measure' && index != 0) {
//
//                         if (!$.isEmptyObject(processedObj)) {
//                             var lastKey = Object.keys(processedObj)[0];
//
//                             var newKey = parseFloat(lastKey) + parseFloat(d[key]);
//
//                             currentKey = newKey;
//                             processedObj[newKey] = processedObj[lastKey];
//                             delete processedObj[lastKey];
//
//                         } else {
//                             console.log("else entered in fieldsArray... with index ", index, " having current object ", Object.assign({}, processedObj));
//                         }
//
//                     } else {
//                         console.log("else entered in fieldsArray , measure... with index ", index, " having current object ", Object.assign({}, processedObj));
//                         processedObj[currentKey] = {};
//
//                     }
//
//                     processedObj[currentKey] = group(d, newIndex, processedObj[currentKey]);
//
//                 }
//                 else {
//                     console.log("else entered in fieldsArray... with index ", index, " having current object ", Object.assign({}, processedObj));
//
//                     if (keyType === 'Measure') {
//
//
//                         if (!$.isEmptyObject(processedObj)) {
//
//                             var lastKey = Object.keys(processedObj)[0];
//
//                             var newKey = parseFloat(lastKey) + parseFloat(d[key]);
//                             currentKey = newKey;
//                             processedObj[currentKey] = {};
//                             delete processedObj[lastKey];
//
//                         } else {
//                             processedObj[currentKey] = {};
//                         }
//                     }
//                     return processedObj;
//                 }
//             }
//         } catch (e) {
//             console.log(e);
//         }
//     }
//
//     var finalObj = {};
//     data.forEach(function (d, i) {
//
//         group(d, 0, finalObj);
//     });
//     console.log(finalObj);
//     var isObject = function (a) {
//         if ($.isEmptyObject(a))
//             return false
//         else
//             return (!!a) && (a.constructor === Object);
//
//     };
//
//     function getAllChildren(group, children) {
//         children = children || [];
//         if (group && isObject(group)) {
//             $.each(group, function (key, child) {
//                 getAllChildren(child, children)
//             })
//         }
//         else {
//             children.push(group);
//         }
//         return children;
//     }
//
//     var p = "";
//     var r = "";
//     $.each(finalObj, function (key, value) {
//         r = "";
//         p += "<tr>";
//         var rowLength = getAllChildren(value).length;
//         var tableData = key.split('#');
//         p += "<td rowspan='" + rowLength + "'>" + tableData[0] + "</td>";
//         p += rowObj(value);
//         if ((!$.isEmptyObject(subAggregates[key])) && draw._tableSetting.footer.subtotal == 1) {
//             p += "<tr class='subtotal'>";
//             columnNames.forEach(function (d) {
//                 if (subAggregates[key][d])
//                     p += "<th> Sub" + draw._tableSetting.footer[d]['operation'] + " : " + subAggregates[key][d] + "</th>";
//                 else
//                     p += "<th>&nbsp;</th>";
//             });
//             p += "</tr>";
//         }
//     });
// //aggregates
//     if (!$.isEmptyObject(aggregates)) {
//         p += "<tr>";
//         columnNames.forEach(function (d) {
//             if (aggregates[d]) {
//                 console.log(draw._tableSetting.footer[d]['operation']);
//                 p += "<th> Total " + draw._tableSetting.footer[d]['operation'] + " : " + aggregates[d] + "</th>";
//             }
//             else
//                 p += "<th>&nbsp;</th>";
//         });
//         p += "</tr>";
//     }
//     function rowObj(value) {
//         var i = 0;
//         $.each(value, function (k, v) {
//             i++;
//             var rLength = 1;
//
//             if (isObject(v)) {
//
//                 rLength = getAllChildren(v).length;
//             }
//             if (i === 1) {
//                 var tableData = k.split('#');
//                 r += "<td rowspan='" + rLength + "'>" + tableData[0] + "</td>";
//                 if (isObject(v)) {
//
//                     rowObj(v);
//                 } else {
//                     r += "</tr>";
//                 }
//             }
//             else {
//                 var tableData = k.split('#');
//                 r += "<tr><td rowspan='" + rLength + "'>" + tableData[0] + "</td>";
//                 if (isObject(v)) {
//                     rowObj(v);
//                 } else {
//                     r += "</tr>";
//                 }
//             }
//         });
//         return r;
//     }
//
//     $("#table-" + draw._container).append("<tbody>" + p + "</tbody>");
//     var keyOfObj = "";
//     var data = [];
//     if (!$.isEmptyObject(draw._dimension)) {
//
//         $.each(draw._dimension, function (key, val) {
//             keyOfObj = key;
//         });
//
//         var dim = draw._crossfilter
//             .dimension(function (d) {
//                 return d[draw._dimension[keyOfObj].value];
//             });
//
//         data = jQuery.extend(true, [], dim.top(Infinity));
//
//     }
//     else {
//
//         data = jQuery.extend(true, [], draw._data);
//     }
//
//     var tableHeaders = "";
//     var k = 0;
//
//     var ColumnArrayForDataTable = [];
//     var keys = [];
//     try {
//         $.each(draw._dimension, function (key, value) {
//             keys.push(value);
//         });
//     }
//     catch (e) {
//         console.log(e);
//     }
//
//     try {
//         $.each(draw._measure, function (key, value) {
//             keys.push(value);
//         });
//     }
//     catch (e) {
//         console.log(e);
//     }
//
// // data.forEach(function (d) {
// //     $.each(d, function (key, value) {
// //         if (keys.indexOf(key) === -1)
// //             delete d[key];
// //     });
// // });
//     var columnNames = [];
//
//     keys.forEach(function (d) {
//         columnNames.push(d['columnName']);
//         // var obj = new Object({
//         //     "mData": d,
//         //     "sDefaultContent": ""
//         // });
//
//         //  ColumnArrayForDataTable.push(obj);
//         tableHeaders += "<th>"
//             + '<a href="javascript:void(0)"  style="float:right" class="toggle-vis" data-column="'
//             + k + '"></a>' + d['columnName'] + "</th>";
//         k++;
//     });
//     var initialKey = columnNames[0];
//     $("#chart-" + draw._container).html("");
//
//
//     $("#chart-" + draw._container)
//         .append('<table id="table-'
//             + draw._container
//             + '" class="table table-bordered bordered table-striped table-condensed"    style="width: 100%;margin-left: 24px;"><thead><tr>'
//             + tableHeaders + '</tr></thead></table>');
//     var p = "<tr>";
//
//     var fieldsArray = keys;
//     var keyIndex = 0;
//     var TotalObject = {};
//
//     function sum(key, processedObj, d) {
//
//         if (processedObj["total_" + key] == undefined) {
//             processedObj["total_" + key] = 0;
//         }
//         processedObj["total"] = {123: {}};
//     }
//
//     aggergateKeyIndexes = [];
//     if (draw._tableSetting) {
//         $.each(draw._tableSetting.footer, function (k, v) {
//             if (columnNames.indexOf(k) != -1) {
//                 aggergateKeyIndexes.push(columnNames.indexOf(k));
//             }
//         });
//
//
//     }
//     var aggregates = {};
//     var subAggregates = {};
//
//     function doAggregate(operation, key, d) {
//         if (operation == "sum") {
//             if (!aggregates[key]) {
//                 aggregates[key] = 0;
//                 if (parseFloat(d[key]))
//                     aggregates[key] += parseFloat(d[key]);
//             } else {
//                 if (parseFloat(d[key]))
//                     aggregates[key] += parseFloat(d[key]);
//             }
//         } else if (operation == "count") {
//             if (!aggregates[key]) {
//                 aggregates[key] = 0;
//                 aggregates[key] += 1;
//             } else {
//                 aggregates[key] += 1;
//             }
//         } else if (operation == "avg") {
//             if (!aggregates[key]) {
//                 aggregates[key] = 0;
//                 if (parseFloat(d[key])) {
//                     aggregates[key] += parseFloat(d[key]);
//                 }
//             } else {
//                 if (parseFloat(d[key]))
//                     aggregates[key] += parseFloat(d[key]);
//             }
//         }
//
//     }
//
// //Sub string Calculate
//     function doSubAggregate(operation, d, initialKey, columnNames, index) {
//         if (operation == "sum") {
//             if (!subAggregates[d[initialKey]]) {
//                 subAggregates[d[initialKey]] = {};
//                 if (parseFloat(d[columnNames[index]]))
//                     subAggregates[d[initialKey]][columnNames[index]] = parseFloat(d[columnNames[index]]);
//                 else
//                     subAggregates[d[initialKey]][columnNames[index]] = 0;
//             }
//             else {
//                 if (parseFloat(d[columnNames[index]])) {
//
//                     if (!subAggregates[d[initialKey]][columnNames[index]]) {
//                         subAggregates[d[initialKey]][columnNames[index]] = 0;
//                     }
//                     subAggregates[d[initialKey]][columnNames[index]] += parseFloat(d[columnNames[index]]);
//                 }
//             }
//         } else if (operation == "count") {
//             if (!subAggregates[d[initialKey]]) {
//                 subAggregates[d[initialKey]] = {};
//
//                 if (parseFloat(d[columnNames[index]]))
//                     subAggregates[d[initialKey]][columnNames[index]] = 1;
//                 else
//                     subAggregates[d[initialKey]][columnNames[index]] = 0;
//             }
//             else {
//                 if (parseFloat(d[columnNames[index]])) {
//
//                     if (!subAggregates[d[initialKey]][columnNames[index]]) {
//                         subAggregates[d[initialKey]][columnNames[index]] = 0;
//                     }
//                     subAggregates[d[initialKey]][columnNames[index]] += 1;
//                 }
//             }
//         } else if (operation == "avg") {
//             if (!subAggregates[d[initialKey]]) {
//                 subAggregates[d[initialKey]] = {};
//
//                 if (parseFloat(d[columnNames[index]]))
//                     subAggregates[d[initialKey]][columnNames[index]] = parseFloat(d[columnNames[index]]);
//                 else
//                     subAggregates[d[initialKey]][columnNames[index]] = 0;
//             }
//             else {
//                 if (parseFloat(d[columnNames[index]])) {
//
//                     if (!subAggregates[d[initialKey]][columnNames[index]]) {
//                         subAggregates[d[initialKey]][columnNames[index]] = 0;
//                     }
//                     subAggregates[d[initialKey]][columnNames[index]] += parseFloat(d[columnNames[index]]);
//                 }
//             }
//         }
//     }
//
//     function group(d, index, processedObj) {
//
//         try {
//             var newIndex = index + 1;
//             var key = fieldsArray[index]['columnName'];
//             var keyType = fieldsArray[index]['dataKey'].trim();
//
//             if (draw._tableSetting) {
//                 if (aggergateKeyIndexes.indexOf(index) != -1) {
//                     $.each(draw._tableSetting.footer, function (k, v) {
//                         if (key === k) {
//                             doSubAggregate(v.operation, d, initialKey, columnNames, index);
//                         }
//                     });
//                 }
//                 $.each(draw._tableSetting.footer, function (k, v) {
//                     if (key === k) {
//                         doAggregate(v.operation, key, d);
//                     }
//                 });
//             }
//             console.log("Processing Started... with index ", index, " having current object ", Object.assign({}, processedObj));
//             if (!processedObj[d[key]]) {
//                 console.log("new value found... with index ", index, " having current object ", Object.assign({}, processedObj));
//                 if (index < fieldsArray.length - 1) {
//                     var currentKey = d[key];
//                     console.log("entered in fieldsArray... with index ", index, " having current object ", Object.assign({}, processedObj));
//                     if (keyType === 'Measure' && index != 0) {
//                         console.log("entered in fieldsArray , measure... with index ", index, " having current object ", Object.assign({}, processedObj));
//
//
//                         if (!$.isEmptyObject(processedObj)) {
//
//                             var lastKey = Object.keys(processedObj)[0];
//
//                             var newKey = parseInt(lastKey) + parseInt(d[key]);
//
//
//                             currentKey = newKey;
//
//                             processedObj[newKey] = processedObj[lastKey];
//                             delete processedObj[lastKey];
//
//
//                         } else {
//                             processedObj[currentKey] = {};
//                         }
//
//                     }
//                     else {
//                         processedObj[currentKey] = {};
//                     }
//                     processedObj[currentKey] = group(d, newIndex, processedObj[currentKey]);
//
//                     return processedObj;
//                 }
//                 else {
//                     if (keyType === 'Measure' && index != 0) {
//                         console.log("entered in fieldsArray , measure... with index ", index, " having current object ", Object.assign({}, processedObj));
//
//
//                         if (!$.isEmptyObject(processedObj)) {
//
//                             var lastKey = Object.keys(processedObj)[0];
//
//                             var newKey = parseInt(lastKey) + parseInt(d[key]);
//
//
//                             currentKey = newKey;
//
//                             processedObj[newKey] = processedObj[lastKey];
//                             delete processedObj[lastKey];
//
//
//                         } else {
//
//                             processedObj[d[key]] = {};
//                         }
//
//                     }
//                     else {
//                         processedObj[d[key]] = {};
//                     }
//                     console.log("else entered in fieldsArray... with index ", index, " having current object ", Object.assign({}, processedObj));
//                     return processedObj;
//                 }
//             } else {
//                 console.log("old key found... with index ", index, " having current object ", Object.assign({}, processedObj));
//                 if (index < fieldsArray.length - 1) {
//                     var currentKey = d[key];
//                     console.log("entered in fieldsArray , measure... with index ", index, " having current object ", Object.assign({}, processedObj));
//                     if (keyType === 'Measure' && index != 0) {
//
//                         if (!$.isEmptyObject(processedObj)) {
//                             var lastKey = Object.keys(processedObj)[0];
//
//                             var newKey = parseFloat(lastKey) + parseFloat(d[key]);
//
//                             currentKey = newKey;
//                             processedObj[newKey] = processedObj[lastKey];
//                             delete processedObj[lastKey];
//
//                         } else {
//                             console.log("else entered in fieldsArray... with index ", index, " having current object ", Object.assign({}, processedObj));
//                         }
//
//                     } else {
//                         console.log("else entered in fieldsArray , measure... with index ", index, " having current object ", Object.assign({}, processedObj));
//                         processedObj[currentKey] = {};
//
//                     }
//
//                     processedObj[currentKey] = group(d, newIndex, processedObj[currentKey]);
//
//                 }
//                 else {
//                     console.log("else entered in fieldsArray... with index ", index, " having current object ", Object.assign({}, processedObj));
//
//                     if (keyType === 'Measure') {
//
//
//                         if (!$.isEmptyObject(processedObj)) {
//
//                             var lastKey = Object.keys(processedObj)[0];
//
//                             var newKey = parseFloat(lastKey) + parseFloat(d[key]);
//                             currentKey = newKey;
//                             processedObj[currentKey] = {};
//                             delete processedObj[lastKey];
//
//                         } else {
//                             processedObj[currentKey] = {};
//                         }
//                     }
//                     return processedObj;
//                 }
//             }
//         } catch (e) {
//             console.log(e);
//         }
//     }
//
//     var finalObj = {};
//     data.forEach(function (d, i) {
//
//         group(d, 0, finalObj);
//     });
//     console.log(finalObj);
//     var isObject = function (a) {
//         if ($.isEmptyObject(a))
//             return false
//         else
//             return (!!a) && (a.constructor === Object);
//
//     };
//
//     function getAllChildren(group, children) {
//         children = children || [];
//         if (group && isObject(group)) {
//             $.each(group, function (key, child) {
//                 getAllChildren(child, children)
//             })
//         }
//         else {
//             children.push(group);
//         }
//         return children;
//     }
//
//     var p = "";
//     var r = "";
//     $.each(finalObj, function (key, value) {
//         r = "";
//         p += "<tr>";
//         var rowLength = getAllChildren(value).length;
//         var tableData = key.split('#');
//         p += "<td rowspan='" + rowLength + "'>" + tableData[0] + "</td>";
//         p += rowObj(value);
//         if ((!$.isEmptyObject(subAggregates[key])) && draw._tableSetting.footer.subtotal == 1) {
//             p += "<tr class='subtotal'>";
//             columnNames.forEach(function (d) {
//                 if (subAggregates[key][d])
//                     p += "<th> Sub" + draw._tableSetting.footer[d]['operation'] + " : " + subAggregates[key][d] + "</th>";
//                 else
//                     p += "<th>&nbsp;</th>";
//             });
//             p += "</tr>";
//         }
//     });
// //aggregates
//     if (!$.isEmptyObject(aggregates)) {
//         p += "<tr>";
//         columnNames.forEach(function (d) {
//             if (aggregates[d]) {
//                 console.log(draw._tableSetting.footer[d]['operation']);
//                 p += "<th> Total " + draw._tableSetting.footer[d]['operation'] + " : " + aggregates[d] + "</th>";
//             }
//             else
//                 p += "<th>&nbsp;</th>";
//         });
//         p += "</tr>";
//     }
//     function rowObj(value) {
//         var i = 0;
//         $.each(value, function (k, v) {
//             i++;
//             var rLength = 1;
//
//             if (isObject(v)) {
//
//                 rLength = getAllChildren(v).length;
//             }
//             if (i === 1) {
//                 var tableData = k.split('#');
//                 r += "<td rowspan='" + rLength + "'>" + tableData[0] + "</td>";
//                 if (isObject(v)) {
//
//                     rowObj(v);
//                 } else {
//                     r += "</tr>";
//                 }
//             }
//             else {
//                 var tableData = k.split('#');
//                 r += "<tr><td rowspan='" + rLength + "'>" + tableData[0] + "</td>";
//                 if (isObject(v)) {
//                     rowObj(v);
//                 } else {
//                     r += "</tr>";
//                 }
//             }
//         });
//         return r;
//     }
//
//     $("#table-" + draw._container).append("<tbody>" + p + "</tbody>");
// }