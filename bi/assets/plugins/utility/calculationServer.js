(function(){
    function _calculation(){
        var draw={
            apiUrl: configData.nodeUrl,
            version: '1.0.0-dev',
            _oldkeysArray:{}, 
            _newkeysArray:{},
            _tabledata:[],
            _calArray:[],
            _categoryArray:[],
            _categoryObject:[]
        };
        draw.oldObject=function(obj){
            draw._oldkeysArray=obj;
            return draw;
        };
        draw.newObject=function(obj){

            draw._newkeysArray=obj;

            return draw;
        };
        draw.groupObject=function(groupObj){
            draw._categories={};
            draw._categoryObject=groupObj;
            $.each(draw._categoryObject,function(key,value){

                draw._categories[value.categoryName]=value;
            });


            return draw;
        };
        draw.tabledata=function(data){
            draw._tabledata=data;
            return draw;
        };
        draw.uniqueArray=function(){
            draw._calObject={};
            draw._newKeysObj={};
            $.each(draw._newkeysArray,function(key,value){
                //&&  (!value.form || value.form=="simple")
                draw._newKeysObj[value.reName]=value;
                if(value.type=="custom" ){
                    if(value.formula!=undefined)
                    {
                        draw._calObject[value.reName]=value;
                        draw._calArray.push(value);
                    }
                    else{
                        draw._categoryArray.push(value);
                    }
                }
            });




            /*draw._newkeysArray.forEach(function(d){
             if(d.type=="custom"){
             if(d.formula!=undefined)
             draw._calArray.push(d);
             else
             draw._categoryArray.push(d);
             }
             });*/
            return draw;
        }

        draw.processAndGetUnprocessedCat=function(){
            var allProcessed=false;
            var category=Object.keys(draw._categories);
            var currentCategoryIndex=0;
            var remainingCategories={};
            var newFieldProcessed=false;
            while(!allProcessed){
                if(draw._categoryObject!=null){

                    if(draw._oldkeysArray[(draw._categories[category[currentCategoryIndex]]).columnName]){
                        newFieldProcessed=true;
                        draw._oldkeysArray[(draw._categories[category[currentCategoryIndex]])["categoryName"]]= draw._newKeysObj[draw._categories[category[currentCategoryIndex]]["categoryName"]];
                        draw.processCategoryObject((draw._categories[category[currentCategoryIndex]]));
                        delete draw._categories[category[currentCategoryIndex]];
                    }else{
                        remainingCategories[draw._categories[category[currentCategoryIndex]]["categoryName"]]=draw._categories[category[currentCategoryIndex]];
                    }

                }
                if(currentCategoryIndex==(category.length-1)){
                    allProcessed=true;
                }
                currentCategoryIndex++;
            }
            console.log(remainingCategories);
            if(!newFieldProcessed || $.isEmptyObject(remainingCategories))
            {
                return remainingCategories;

            }else{

                return draw.processAndGetUnprocessedCat();

            }
        }

        draw.hasRequiredFields = function (formula) {
            var flag = true;
            const regex = /\[([\w|' ']*)\]/g;
            var m;
            while ((m = regex.exec(formula)) !== null) {
                try {
                    if(!draw._oldkeysArray[m[1]]){
                        flag=false;
                    }
                } catch (e) {
                    return false;
                }

            }
            return flag;
        }

        draw.render=function(){
            while(!$.isEmptyObject(draw.processAndGetUnprocessedCat()) && !$.isEmptyObject(draw.processCalculationFieldAndGetUnprocessedCal()));
        }
        draw.processCalculationFieldAndGetUnprocessedCal=function(){
            var allProcessed=false;
            var calc=Object.keys(draw._calObject);
            var currentCategoryIndex=0;
            var remainingCategories={};
            var newFieldProcessed=false;

            while(!allProcessed){
                var obj=draw._calObject[calc[currentCategoryIndex]];
                if(draw._calObject!=null){
                    console.log(obj);
                    if(draw.hasRequiredFields(obj.formula)){
                        newFieldProcessed=true;
                        draw._oldkeysArray[(draw._calObject[calc[currentCategoryIndex]])["columnName"]]= draw._newKeysObj[draw._calObject[calc[currentCategoryIndex]]["columnName"]];
                        draw.processCalculationObject(draw._calObject[calc[currentCategoryIndex]]);
                        delete draw._categories[category[currentCategoryIndex]];
                    }else{
                        remainingCategories[draw._calObject[calc[currentCategoryIndex]]["columnName"]]=draw._calc[calc[currentCategoryIndex]];
                    }

                }
                if(currentCategoryIndex==(calc.length-1)){
                    allProcessed=true;
                }
                currentCategoryIndex++;
            }

            if(!newFieldProcessed || $.isEmptyObject(remainingCategories))
            {
                return remainingCategories;

            }else{

                return draw.processCalculationFieldAndGetUnprocessedCal();

            }
        }
        draw.processCalculationObject=function(e){
            var columnName=e.columnName;
            var formula=e.formula;

            calculate.processExpression(formula, draw._tabledata, columnName,e);

        }
        draw.columnCal=function(){

            draw._calArray.forEach(function(e,index){
                //   setTimeout(function(){
                var columnName=e.columnName;
                var formula=e.formula;

                calculate.processExpression(formula, draw._tabledata, columnName,e);

                //  },3000*index);

            });
            return draw;
        }
        draw.columnCategory=function(){
            if(draw._categoryObject!=null){
                Object.keys(draw._categoryObject).forEach(function(d){
                    var columnName=draw._categoryObject[d].columnName;
                    var categoryName=draw._categoryObject[d].categoryName;
                    var categoryData=draw._categoryObject[d].groupData;

                    draw._tabledata.forEach(function(e){
                        var keysVal="";
                        Object.keys(categoryData).forEach(function(r){
                            //console.log(categoryData[r]);
                            var index=categoryData[r].indexOf(e[columnName]);
                            if(index!=-1){
                                keysVal=r;
                            }
                        });
                        if(keysVal==""){keysVal=e[columnName];}
                        e[categoryName]=keysVal;
                    });

                });
            }
            return draw;
        };
        draw.processCategoryObject=function(d){
            var columnName=d.columnName;
            var categoryName=d.categoryName;
            var categoryData=d.groupData;
            //Test

            draw._tabledata.forEach(function(e){
                var keysVal="";
                Object.keys(categoryData).forEach(function(r){
                    //console.log(categoryData[r]);
                    var index=categoryData[r].indexOf(e[columnName]);
                    if(index!=-1){
                        keysVal=r;
                    }
                });
                if(keysVal==""){keysVal=e[columnName];}
                e[categoryName]=keysVal;
            });
        };

        draw.processColumnCalculation=function(e){
            var columnName=e.columnName;
            var formula=e.formula;

            calculate.processExpression(formula, draw._tabledata, columnName,e);
        }
        /*
          Server side
         */
        draw.dataGroupCalculation=function(metadataId,accessToken,categoryGroupObject){
            var data={
                metadataId:metadataId,
                sessionId:accessToken,
                categoryGroupObject:JSON.stringify(categoryGroupObject),
                oldColumn:draw._oldkeysArray,
                newColumn:draw._newkeysArray
            }
            var url = configData.nodeUrl+"/dataGroupCalculation";
            $.post(url, data).done(function(data){
                return data;
            });
        }
        return draw;
    }
    this.calculationServer=_calculation();
})();