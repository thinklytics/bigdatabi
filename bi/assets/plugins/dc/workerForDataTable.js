var concatVar = "";
calculate._data = data;
try {
    var flag = false;


    if (!flag) {

        var executingExpression = calculate.preProcessToken(expression);
        var groupedData = {};



        if (!$.isEmptyObject(calculate.getDataGroupParams())) {

            var dataGroupParams=calculate.getDataGroupParams();
            function getGroupedKey(d){
                var tempString=undefined;
                if(sketch._activeKey && sketch._activeKey.length>0){
                    tempString="";
                    sketch._activeKey.forEach(function(key,index){
                        tempString+="||"+d[key];
                    });
                }
                return tempString;
            }


            //   if(index==0){
            groupedData=d3.nest().key(function(d){return getGroupedKey(d);});
            //    }else{
            //     groupedData=groupedData.key(function(d){return d[key];});
            //  }


            function checkValIntegrity(val){
                if(!val || isNaN(val)){
                    val=0;
                }
                else{
                    val=Math.round(val);
                }
                return val;
            }


            function processedReturnObj(v){
                var tempObj={};
                $.each(dataGroupParams,function(key,value){
                    if(value=="sum"){
                        tempObj[key]=d3.sum(v,function(d){
                            return checkValIntegrity(d[key]);
                        });
                    }
                    if(value=="count"){


                        tempObj[key]=d3.sum(v,function(d){
                            if(d[key])
                                return 1;
                            else
                                return 0;
                        });

                    }
                    if(value=="avg"){
                        tempObj[key]=d3.mean(v,function(d){
                            return checkValIntegrity(d[key]);
                        });
                    }


                });
                return tempObj;
            }


            groupedData=groupedData.rollup(function(v){
                return processedReturnObj(v);
            });

            groupedData=groupedData.entries(data);
            var testData={};

            groupedData.forEach(function(d){
                testData[d.key]=d.values;
            });

            groupedData=testData;

            calculate._tempAvgKey=null;

            $.each(groupedData,function(key,d){

                var res=eval(calculate._abstractExpression);

                d['newCalculation']=res;


            });
            var tempSearchObj={};

            if(column && column.dataKey=="Dimension"){
                data.forEach(function (d) {
                    var groupKey=getGroupedKey(d);
                    //   if(groupKey){
                    if(!tempSearchObj[groupKey]){
                        tempSearchObj[groupKey]=true;
                        d[keyName]=groupedData[groupKey]['newCalculation'];
                    }else{
                        d[keyName]=groupedData[groupKey]['newCalculation'];

                    }
                    // }
                });
            }else{
                data.forEach(function (d) {
                    var groupKey=getGroupedKey(d);
                    //   if(groupKey){
                    if(!tempSearchObj[groupKey]){
                        tempSearchObj[groupKey]=true;
                        d[keyName]=groupedData[groupKey]['newCalculation'];
                    }else{
                        d[keyName]=0;
                    }
                });
                // }
            }



        }else {
            //  setTimeout(function(d){
            data.forEach(function (d) {
                var res = eval(executingExpression);
                d[keyName] = res;
            });


        }

    }
    calculate._abstractExpression=null;
    calculate._dataGroupsParams={};
    calculate.cachedSum = null;
    calculate.cachedCount = null;
    calculate.cachedAvg = null;

} catch (e) {


}