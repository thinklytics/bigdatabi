(function () {
    function _eChart(Chart) {
        'use strict';
        var eChart = {
            VERSION: "1.0.0",
            //nodeApiUrl: "http://192.168.0.17:3002",
            nodeApiUrl: configData.nodeUrl,
            constants: {
                DELAY: 4,
                NEGLIGIBLE_NUMBER: 1e-10,
                DEFAULT_COLOR: "rgba(255, 99, 132, 0)",
                INACTIVE_COLOR: "rgba(255, 99, 132, 0)",
                LABEL: "label"
            }
        };

        eChart.rainbow = new Rainbow();

        //Utility Functions ...
        eChart.getIndexOfObj = function (array, attr, value) {
            for (var i = 0; i < array.length; i++) {
                if (array[i].hasOwnProperty(attr) && array[i][attr] === value) {
                    return i;
                }
            }
            return -1;
        };

        eChart.setSessionId = function(sessionId){
            eChart._sessionId = sessionId;
            return eChart;
        }

        eChart.setMetadataId = function(metadataId){
            eChart._metadataId = metadataId;
            return eChart;
        }

        eChart.monthWiseSortObject = {
            January: {},
            February: {},
            March: {},
            April: {},
            May: {},
            June: {},
            July: {},
            August: {},
            September: {},
            October: {},
            November: {},
            December: {}
        };

        eChart.chartRegistry = (function () {
            var _chartMap = {};
            var _chartAttributes = {};
            var _colorObject = {};
            return {
                /**
                 * Determine if a given chart instance resides in the registry.
                 *
                 * @method has
                 *
                 * @returns {Boolean}
                 */
                has: function (chart) {
                    if (_chartMap[chart.id]) {
                        return true;
                    }
                    return false;
                },
                get: function (chartId) {
                    if (_chartMap[chartId]) {
                        return _chartMap[chartId];
                    }
                    return false;
                },
                /**
                 * Add given chart instance .
                 *
                 */
                register: function (chart) {
                    if(sketchServer.chartRegistry.has(chart)){
                        sketchServer.chartRegistry.deregister(chart);
                    }
                    _chartMap[chart.id] = chart;
                    // if(sketch.chartRegistry.getRegisterdIds().indexOf(chart.id)!=-1){
                    //    sketch.chartRegistry.deregister(chart);
                    //  }
                },
                /**
                 *
                 * @method registerAttributes
                 * @memberof eCharts.chartRegistry
                 *
                 */
                registerColor: function (chart) {
                    if (_chartAttributes[chart.id]) {
                        _chartAttributes[chart.id]['colors'] = chart._colorSelection;
                    } else {
                        _chartAttributes[chart.id] = {colors: chart._colorSelection};
                    }
                },
                /**
                 *
                 * @method registerPublicViewColor
                 * @memberof eCharts.registerPublicViewColor
                 *
                 */
                registerPublicViewColor: function (id, color) {
                    _colorObject[id] = color;
                    _chartAttributes = _colorObject;
                    if(color && color.sortMeasure)
                        eChart.sortMeasureObj[id]=color.sortMeasure;
                },
                registerDataOrder: function (chart) {
                    if (_chartAttributes[chart.id]) {
                        _chartAttributes[chart.id]['order'] = chart._sort;
                        _chartAttributes[chart.id]['sortMeasure'] = eChart.sortMeasureObj[chart.id];
                    } else {
                        _chartAttributes[chart.id] = {order: chart._sort};
                        _chartAttributes[chart.id] = {sortMeasure: eChart.sortMeasureObj[chart.id]};
                    }
                },
                changeSortMeasure:function (id) {
                    _chartAttributes[id]['sortMeasure'] = eChart.sortMeasureObj[id];
                },
                /**
                 *
                 * @method registerAttributes
                 * @memberof eCharts.chartRegistry
                 *
                 */
                registerAllAttributes: function (chartAttributes) {
                    _chartAttributes = chartAttributes;
                },
                /**
                 *
                 * @method registerAttributes
                 * @memberof eCharts.chartRegistry
                 *
                 */
                listAttributes: function () {
                    return _chartAttributes;
                },
                /**
                 * Remove given chart instance
                 */
                deregister: function (chart) {
                    delete _chartMap[chart.id];
                },
                /**
                 *
                 * @method clear
                 * @memberof eCharts.chartRegistry
                 *
                 */
                clear: function () {
                    // delete _chartMap;
                },
                /**
                 *
                 * @method list
                 * @memberof eCharts.chartRegistry
                 *
                 */
                list: function () {
                    var list = [];
                    $.each(_chartMap, function (key, value) {
                        list.push(value);
                    });
                    return list;
                },
                /*
                 * @method check registry exist with other then delete
                 */
                otherRegistryCheck:function (id) {
                    sketchServer.chartRegistry.ifExist(id);
                },
                ifExist:function (id) {
                    if(_chartMap[id])
                        delete _chartMap[id];
                }
            };
        })();
        /*
         * Mark labels
         */
        eChart.markLabels =function (chart) {
            if(chart.type=="pieChart" && chart._dataFormateVal && chart._dataFormateVal.yaxis && chart._dataFormateVal.yaxis.markLabels){
                return {
                    duration: 0,
                    animationEasing: "easeOutBack",
                    onComplete: function () {
                        var ctx = this.chart.ctx;
                        ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontFamily, 'normal', Chart.defaults.global.defaultFontFamily);
                        ctx.textAlign = 'center';
                        ctx.textBaseline = 'bottom';
                        ctx.fillStyle=chart._dataFormateVal.yaxis.markLabelsColor;
                        this.data.datasets.forEach(function (dataset) {
                            for (var i = 0; i < dataset.data.length; i++) {
                                try{
                                    var metaKeys=Object.keys(dataset._meta);
                                    var selectedMetaKey=metaKeys[metaKeys.length-1];
                                    var model = dataset._meta[selectedMetaKey].data[i]._model,
                                    total = dataset._meta[selectedMetaKey].total,
                                    mid_radius = model.innerRadius + (model.outerRadius - model.innerRadius)/2,
                                    start_angle = model.startAngle,
                                    end_angle = model.endAngle,
                                    mid_angle = start_angle + (end_angle - start_angle)/2;
                                    var x = mid_radius * Math.cos(mid_angle);
                                    var y = mid_radius * Math.sin(mid_angle);
                                    ctx.fillText(dataset.data[i], model.x + x, model.y + y);
                                    // Display percent in another line, line break doesn't work for fillText
                                }catch (e){

                                }
                            }
                        });

                    }
                };
            }else if(chart.type=="bubbleChart" && chart._dataFormateVal && chart._dataFormateVal.yaxis && chart._dataFormateVal.yaxis.markLabels){
                return {
                    onComplete: function() {
                        var chartInstance = this.chart,
                        ctx = chartInstance.ctx;
                        ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
                        ctx.textAlign = 'center';
                        ctx.textBaseline = 'bottom';
                        ctx.fillStyle=chart._dataFormateVal.yaxis.markLabelsColor;
                        this.data.datasets.forEach(function(dataset, i) {
                            var meta = chartInstance.controller.getDatasetMeta(i);
                            meta.data.forEach(function(bar, index) {
                                var data = dataset.data[index];
                                if(data!=null){
                                    if(eChart.checkFloat(data.y)){
                                        data.y=(data.y).toFixed(2);
                                    }
                                    ctx.fillText(data.y, bar._model.x, bar._model.y + 7 );
                                }
                            });
                        });
                    }
                };
            }else if(chart._dataFormateVal && chart._dataFormateVal.yaxis && chart._dataFormateVal.yaxis.markLabels){
                return {
                    onComplete: function() {
                        var chartInstance = this.chart,
                        ctx = chartInstance.ctx;
                        ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
                        ctx.textAlign = 'center';
                        ctx.textBaseline = 'bottom';
                        ctx.fillStyle = chart._dataFormateVal.yaxis.markLabelsColor;
                        this.data.datasets.forEach(function(dataset, i) {
                            var meta = chartInstance.controller.getDatasetMeta(i);
                            meta.data.forEach(function(bar, index) {
                                var data = dataset.data[index];
                                if(data!=null){
                                    if(chart.type=="funnelChart"){
                                        ctx.fillText(data, bar._model.x, bar._model.y + 15 );
                                    }else if(chart.type=="horizontalBar"){
                                        // if(!eChart.isGroupColorApplied(chart)){
                                        //     ctx.fillText(data, bar._model.x + 5, bar._model.y + 7);
                                        // }else{
                                        //     ctx.fillText(data, bar._model.x - 7, bar._model.y + 7);
                                        // }
                                        ctx.fillText(data, bar._model.x - 18, bar._model.y + 7);
                                    }else{
                                        if(!eChart.isGroupColorApplied(chart) || chart.type=="lineChart" || chart.type=="areaChart"){
                                            ctx.fillText(data, bar._model.x, bar._model.y - 4 );
                                        }else{
                                            ctx.fillText(data, bar._model.x, bar._model.y + 15 );
                                        }
                                    }
                                }
                            });
                        });
                    }
                };
            }else{
                return {};
            }
        }
        /*
         * Common highlightSelectedFilter for if scroll come's
         */
        eChart.highlightSelectedFilter=function(_chart){
            if(_chart.filters==undefined){
                return true;
            }
            var labels = _chart._data.labels;
            var labelsLength = labels.length;
            var chartFiltersLength = 0;
            if (!_chart.reset) {
                chartFiltersLength = _chart.filters.length;
            }
            var pointRadiusNumber=2;
            if(/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
                pointRadiusNumber=20;
            }
            _chart._data.datasets.forEach(function (dataset, dsIndex) {
                _chart._data.datasets[dsIndex].backgroundColor = _chart._colorSelection[dsIndex];
                labels.forEach(function (filter, i) {
                    try {
                        _chart._data.datasets[dsIndex].pointRadius = pointRadiusNumber;
                        _chart._data.datasets[dsIndex].pointBorderColor[i] = _chart._colorSelection[dsIndex];
                    } catch (e) {
                        //_chart._data.datasets[dsIndex].pointBorderColor = _chart._colorSelection[dsIndex];
                    }

                });
            });
            if (labelsLength == chartFiltersLength || chartFiltersLength === 0) {
                _chart._data.datasets.forEach(function (dataset, dsIndex) {
                    _chart._data.datasets[dsIndex].backgroundColor = _chart._colorSelection[dsIndex];
                    labels.forEach(function (filter, i) {
                        try {
                            _chart._data.datasets[dsIndex].pointRadius = pointRadiusNumber;
                            _chart._data.datasets[dsIndex].pointBorderColor[i] = _chart._colorSelection[dsIndex];
                        } catch (e) {
                            //_chart._data.datasets[dsIndex].pointBorderColor = _chart._colorSelection[dsIndex];
                        }

                    });
                });
                _chart.filters = [];
            } else {
                var filterLookup = {};
                $.map(_chart.filters, function (value, index) {
                    if (!filterLookup[value['datasetIndex']]) {
                        filterLookup[value['datasetIndex']] = [];
                    }
                    filterLookup[value['datasetIndex']].push(value['label']);
                });
                labels.forEach(function (label, i) {
                    $.each(filterLookup, function (dsIndex, val) {
                        if (val.indexOf(label) != -1) {
                            try {
                                _chart._data.datasets[dsIndex].pointRadius = pointRadiusNumber;
                                _chart._data.datasets[dsIndex].pointBorderColor[i] = 'rgb(0, 0, 0)';
                            } catch (e) {
                                //_chart._data.datasets[dsIndex].pointBorderColor = 'rgb(0, 0, 0)';
                            }
                        }
                    });
                });
            }
        }
        //Xaxes Define
        eChart.renderxAxes = function (type, fontSize, dataFormateVal, chart) {
            var labelColor, axisLineColor;
            if(dataFormateVal && dataFormateVal.axisLabelColor){
                labelColor = dataFormateVal.axisLabelColor;
            }else{
                labelColor = '#666666';
            }
            if(dataFormateVal && dataFormateVal.axisLabelColor){
                axisLineColor = dataFormateVal.axisColor;
            }else{
                axisLineColor = '#e6e6e6';
            }
            /*
             * scale labels
             */
            var scaleLabels=false;
            if(dataFormateVal && dataFormateVal.xaxis && dataFormateVal.xaxis.axisLabel){
                scaleLabels=dataFormateVal.xaxis.axisLabel;
            }
            /*
             *  dimension value
             */
            var dimensionValue="";
            if(chart._dimension){
                dimensionValue=Object.keys(chart._dimension)[0];
            }
            /*
             *  Measures value
             */
            var measuresValue="";
            if(chart._dimension!=undefined){
                measuresValue=Object.keys(chart._dimension)[0];
            }
            var dualXaxisLabel=false;
            var dualXaxisLabelVal="";
            var axisLabelHeader=true;
            var allAxisHeader=false;
            var axisHeaderRotate=90;
            var autoSkip=false;
            if(chart._dataFormateVal && chart._dataFormateVal.xaxis){
                if(chart._dataFormateVal.xaxis.dualXaxisLabel){
                    dualXaxisLabel=chart._dataFormateVal.xaxis.dualXaxisLabel;
                    dualXaxisLabelVal=JSON.parse(chart._dataFormateVal.xaxis.dualXDimension).reName;
                }
                /*
                 * Axis label option
                 */
                if(chart._dataFormateVal.yaxis.axisHeader!=undefined){
                    axisLabelHeader=chart._dataFormateVal.xaxis.axisHeader;
                }
                /*
                 * All axis label
                 */
                if(chart._dataFormateVal.xaxis.allAxisHeader!=undefined){
                    allAxisHeader=chart._dataFormateVal.xaxis.allAxisHeader;
                    if(allAxisHeader==true){
                        allAxisHeader=false;
                        autoSkip=false;
                    }else{
                        allAxisHeader=10;
                        autoSkip=true;
                    }
                }
                /*
                 * Rotate label
                 */
                if(chart._dataFormateVal.xaxis.axisHeaderRotate!=undefined){
                    axisHeaderRotate=chart._dataFormateVal.xaxis.axisHeaderRotate;
                }
            }
            /*
             * Dual x axis flag
             */
            var dualXAxisFlag=false;
            if(chart._dataFormateVal && chart._dataFormateVal.xaxis && chart._dataFormateVal.xaxis.dualX=='yes'){
                dualXAxisFlag=true;
            }
            if(/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
                fontSize=30;
            }
            var dualXSkipLabel={};
            if((chart._dataType == "date" || chart._fontSize == "datetime") && chart._dataFormateVal.xAxis == undefined) {
                return [{
                    id:'xAxis1',
                    type:"category",
                    stacked: true,
                    scaleLabel: {
                        display: scaleLabels,
                        labelString: dimensionValue
                    },
                    gridLines: {
                        color: axisLineColor,
                        display: axisLabelHeader,
                    },
                    ticks: {
                        fontColor: labelColor,
                        fontSize: chart._xfontSize,
                        display: axisLabelHeader,
                        autoSkip: autoSkip,
                        maxRotation: axisHeaderRotate,
                        minRotation: axisHeaderRotate,
                        maxTicksLimit: allAxisHeader,
                        fontFamily: "sans-serif",
                        callback:function(label){
                            label=label.toString();
                            //return label;
                            return label.slice(0, 15) +(label.length > 15 ? "..." : "");
                        }
                    },
                    type: 'time',
                    unit: 'day',
                    unitStepSize: 1,
                    time: {
                        displayFormats: {
                            'day': 'MMM DD'
                        }
                    }
                },
                {
                    id:'xAxis2',
                    type:"category",
                    stacked: true,
                    display:dualXAxisFlag,
                    position: 'top',
                    scaleLabel: {
                        display: dualXaxisLabel,
                        labelString: dualXaxisLabelVal,
                    },
                    gridLines: {
                        color: axisLineColor,
                        display: axisLabelHeader,
                        drawOnChartArea: false, // only want the grid lines for one axis to show up
                        //tickMarkLength: 10,
                        offsetGridLines: false
                    },
                    ticks:{
                        fontSize: chart._xfontSize,
                        display: axisLabelHeader,
                        maxRotation: 0,
                        minRotation: 0,
                        fontFamily: "sans-serif",
                        callback:function(label){
                            label=label.toString();
                            if(dualXSkipLabel[chart._data.dualXLabel[label]]==undefined){
                                dualXSkipLabel[chart._data.dualXLabel[label]]=index;
                            }
                            if(dualXSkipLabel[chart._data.dualXLabel[label]]==index){
                                return chart._data.dualXLabel[label];
                            }
                            return "";
                        }
                    },
                    type: 'time',
                    unit: 'day',
                    unitStepSize: 1,
                    time: {
                        displayFormats: {
                            'day': 'MMM DD'
                        }
                    }
                }];
            } else {
                return [{
                    id:'xAxis1',
                    type:"category",
                    stacked: true,
                    scaleLabel: {
                        display: scaleLabels,
                        labelString: dimensionValue,
                    },
                    gridLines:{
                        color: axisLineColor,
                        display: axisLabelHeader,
                    },
                    ticks:{
                        fontColor: labelColor,
                        display: axisLabelHeader,
                        maxTicksLimit: allAxisHeader,
                        fontSize: chart._xfontSize,
                        autoSkip: autoSkip,
                        maxRotation: axisHeaderRotate,
                        minRotation: axisHeaderRotate,
                        fontFamily: "sans-serif",
                        callback:function(label){
                            if(label){
                                label=label.toString();
                                return label.slice(0, 15) +(label.length > 15 ? "..." : "");
                            }
                            return "";
                            return label.slice(0, 12) +(label.length > 12 ? "..." : "");
                        }
                    }
                },
                {
                    id:'xAxis2',
                    type:"category",
                    stacked: true,
                    position: 'top',
                    display:dualXAxisFlag,
                    scaleLabel: {
                        display: dualXaxisLabel,
                        labelString: dualXaxisLabelVal
                    },
                    gridLines: {
                        color: axisLineColor,
                        display: axisLabelHeader,
                        drawOnChartArea: false, // only want the grid lines for one axis to show up
                        //tickMarkLength: 10,
                        offsetGridLines: false
                    },
                    ticks:{
                        fontSize: chart._xfontSize,
                        display: axisLabelHeader,
                        maxRotation: 0,
                        minRotation: 0,
                        fontFamily: "sans-serif",
                        callback:function(label,index,labels){
                            if(label){
                                label=label.toString();
                                if(dualXSkipLabel[chart._data.dualXLabel[label]]==undefined){
                                    dualXSkipLabel[chart._data.dualXLabel[label]]=index;
                                }
                                if(dualXSkipLabel[chart._data.dualXLabel[label]]==index){
                                    return chart._data.dualXLabel[label];
                                }
                            }
                            return "";
                        }
                    }
                }];
            }
        }
        /*
          Y axis format
         */
        eChart.ranges = [
            { divider: 1e9, suffix: 'B' },
            { divider: 1e6, suffix: 'M' },
            { divider: 1e3, suffix: 'K' }
        ];

        eChart.formatNumber = function(n) {
            for (var i = 0; i < eChart.ranges.length; i++) {
                if (n >= eChart.ranges[i].divider) {
                    return (n / eChart.ranges[i].divider).toString() + eChart.ranges[i].suffix;
                }
            }
            return n;
        }

        // End y axis format
        eChart.renderyAxes = function (dataFormateVal, chart) {
            var labelColor, axisLineColor;
            if(dataFormateVal && dataFormateVal.axisLabelColor){
                labelColor = dataFormateVal.axisLabelColor;
            }else{
                labelColor = '#666666';
            }
            if(dataFormateVal && dataFormateVal.axisLabelColor){
                axisLineColor = dataFormateVal.axisColor;
            }else{
                axisLineColor = '#e6e6e6';
            }
            /*
             * scale labels
             */
            // Check data max value
            var maxVal=0;
            chart._data.datasets.forEach(function (d) {
                // var maxDatasetVal = Math.max(d.data);
                try{
                    var maxDatasetVal = Math.max.apply(null, d.data);
                    if(maxVal<maxDatasetVal){
                        maxVal=maxDatasetVal;
                    }
                }catch(e){
                    maxVal=0;
                }

            });
            var scaleLabels=false;
            var axisLabelHeader=true;
            if(chart._dataFormateVal && chart._dataFormateVal.yaxis){
                if(chart._dataFormateVal.yaxis.axisLabel){
                    scaleLabels=chart._dataFormateVal.yaxis.axisLabel;
                }
                /*
                 * Axis label option
                 */
                if(chart._dataFormateVal.yaxis.axisHeader!=undefined){
                    axisLabelHeader=chart._dataFormateVal.yaxis.axisHeader;
                }
            }
            /*
             *  Measures value
             */
            var measuresValue="";
            if(chart._measures){
                if(Object.keys(chart._measures).length>1){
                    measuresValue="Values";
                }else{
                    measuresValue=Object.keys(chart._measures)[0];
                }
            }
            var fontSize=chart._yfontSize;
            if(/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
                fontSize=30;
            }

            if(chart.type=="lineChart"){
                return [{
                    //Label formate
                    stacked: false,
                    scaleLabel: {
                        display:scaleLabels,
                        labelString:measuresValue
                    },
                    gridLines: {
                        color: axisLineColor,
                        display: axisLabelHeader,
                    },
                    ticks: {
                        fontColor: labelColor,
                        display: axisLabelHeader,
                        beginAtZero: true,
                        // Return an empty string to draw the tick line but hide the tick label
                        // Return `null` or `undefined` to hide the tick line entirely
                        userCallback: function (value, index, values) {
                            // Convert the number to a string and splite the string every 3 charaters from the end
                            // Convert the array to a string and format the output
                            //value = value.join('.');
                            if(maxVal<=10){
                                value = value.toFixed(2);
                            }
                            if (chart._dataFormateVal && chart._dataFormateVal.yaxis) {
                                var yAxisObj=chart._dataFormateVal.yaxis;
                                /*
                                 * Max value
                                 */
                                if(maxVal<=10){
                                    value = parseFloat(value).toFixed(2);
                                }
                                if(yAxisObj.decimal){
                                    value = parseFloat(value).toFixed(chart._dataFormateVal.yaxis.decimal);
                                }
                                if(yAxisObj.units=="yes"){
                                    value = eChart.formatNumber(value);
                                }
                                if(yAxisObj.separator){
                                    value = value.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
                                }
                                if(yAxisObj.prefix){
                                    value = yAxisObj.prefix+" "+value;
                                }
                                if(yAxisObj.suffix){
                                    value = value+" "+yAxisObj.suffix;
                                }
                                return value;
                            } else {
                                return eChart.formatNumber(value);
                            }
                        },
                        fontSize:fontSize,
                        fontFamily: 'sans-serif',
                    }
                }];

            }else if(chart.type=="dualAxisChart"){
                /*
                 * scale labels
                 */
                var scaleLabels2=false;
                if(chart._dataFormateVal && chart._dataFormateVal.y2axis && chart._dataFormateVal.y2axis.axisLabel){
                    scaleLabels2=chart._dataFormateVal.y2axis.axisLabel;
                }
                if(chart._dataFormateVal && chart._dataFormateVal.y2axis && chart._dataFormateVal.y2axis.fontSize){
                    var y2axisFontSize = chart._dataFormateVal.y2axis.fontSize;
                }else{
                    var y2axisFontSize = 13;
                }
                var measuresValueArr=[];
                if(chart._measures){
                    chart._data.datasets.forEach(function (d) {
                        measuresValueArr.push(d.label);
                    });
                }

                return [{
                    type: "linear", // only linear but allow scale type registration. This allows extensions to exist solely for log scale for instance
                    display: true,
                    position: "left",
                    id: "y-axis-1",
                    scaleLabel: {
                        display:scaleLabels,
                        labelString:Object.keys(chart._measures)[0]
                    },
                    gridLines: {
                        color: axisLineColor,
                        display: axisLabelHeader,
                    },
                    ticks: {
                        fontColor: labelColor,
                        display: axisLabelHeader,
                        fontSize: fontSize,
                        fontFamily: 'sans-serif',
                        beginAtZero: true,
                        userCallback: function (value, index, values) {
                            // Convert the number to a string and splite the string every 3 charaters from the end
                            // Convert the array to a string and format the output
                            //value = value.join('.');
                            if (chart._dataFormateVal && chart._dataFormateVal.yaxis) {
                                var yAxisObj=chart._dataFormateVal.yaxis;
                                if(yAxisObj.decimal){
                                    value = value.toFixed(chart._dataFormateVal.yaxis.decimal);
                                }
                                if(yAxisObj.units=="yes"){
                                    value = eChart.formatNumber(value);
                                }
                                if(yAxisObj.separator){
                                    value = value.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
                                }
                                if(yAxisObj.prefix){
                                    value = yAxisObj.prefix+" "+value;
                                }
                                if(yAxisObj.suffix){
                                    value = value+" "+yAxisObj.suffix;
                                }
                                return value;
                            } else {
                                return eChart.formatNumber(value);
                            }
                        }
                    }
                },
                {
                    type: "linear", // only linear but allow scale type registration. This allows extensions to exist solely for log scale for instance
                    display: true,
                    position: "right",
                    id: "y-axis-2",
                    scaleLabel: {
                        display:scaleLabels2,
                        labelString:Object.keys(chart._measures)[1]
                    },
                    gridLines: {
                        color: axisLineColor,
                        display: axisLabelHeader,
                    },
                    ticks: {
                        fontColor: labelColor,
                        fontSize: y2axisFontSize,
                        display: axisLabelHeader,
                        fontFamily: 'sans-serif',
                        beginAtZero: true,
                        userCallback: function (value, index, values) {
                            // Convert the number to a string and splite the string every 3 charaters from the end
                            // Convert the array to a string and format the output
                            //value = value.join('.');
                            if (chart._dataFormateVal && chart._dataFormateVal.y2axis) {
                                var yAxisObj=chart._dataFormateVal.y2axis;
                                if(yAxisObj.decimal){
                                    value = value.toFixed(chart._dataFormateVal.y2axis.decimal);
                                }
                                if(yAxisObj.units=="yes"){
                                    value = eChart.formatNumber(value)
                                }
                                if(yAxisObj.separator){
                                    value = value.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
                                }
                                if(yAxisObj.prefix){
                                    value = yAxisObj.prefix+" "+value;
                                }
                                if(yAxisObj.suffix){
                                    value = value+" "+yAxisObj.suffix;
                                }
                                return value;
                            } else {
                                return eChart.formatNumber(value);
                            }
                        }
                    }
                }];
            }else{
                return [{
                    //Label formate
                    stacked: true,
                    scaleLabel: {
                        display:scaleLabels,
                        labelString:measuresValue
                    },
                    gridLines: {
                        color: axisLineColor,
                        display: axisLabelHeader,
                    },
                    ticks: {
                        fontColor: labelColor,
                        display: axisLabelHeader,
                        beginAtZero: true,
                        padding: 0,
                        // Return an empty string to draw the tick line but hide the tick label
                        // Return `null` or `undefined` to hide the tick line entirely
                        userCallback: function (value, index, values) {
                            // Convert the number to a string and splite the string every 3 charaters from the end
                            // Convert the array to a string and format the output
                            //value = value.join('.');
                            if (chart._dataFormateVal && chart._dataFormateVal.yaxis) {
                                var yAxisObj=chart._dataFormateVal.yaxis;
                                if(yAxisObj.decimal){
                                    value = value.toFixed(chart._dataFormateVal.yaxis.decimal);
                                }
                                if(yAxisObj.units=="yes"){
                                    value = eChart.formatNumber(value)
                                }
                                if(yAxisObj.separator){
                                    value = value.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
                                }
                                if(yAxisObj.prefix){
                                    value = yAxisObj.prefix+" "+value;
                                }
                                if(yAxisObj.suffix){
                                    value = value+" "+yAxisObj.suffix;
                                }
                                return value;
                            } else {
                                return eChart.formatNumber(value);
                            }
                        },
                        fontSize:fontSize,
                        fontFamily: 'sans-serif'
                    }
                }];
            }
        }

        eChart.registerChart = function (chart){
            eChart.chartRegistry.otherRegistryCheck(chart.id);
            eChart.chartRegistry.register(chart);
        };

        eChart.registerColor = function (chart){
            eChart.chartRegistry.registerColor(chart);
        }

        eChart.registerDataOrder = function (chart){
            eChart.chartRegistry.registerDataOrder(chart);
        }

        eChart.getRunTotal = function(finalData, key, _chart){
            var sumKeeper=0;
            finalData.forEach(function(d){
                if (d.value!=null && _chart && d.value[_chart._operator[key].key]) {
                    if(!isNaN(d.value[_chart._operator[key].key]))
                        sumKeeper+=parseFloat(d.value[_chart._operator[key].key]);
                    d.value[_chart._operator[key].key]=sumKeeper;
                } else {
                    if(!isNaN(d.value))
                        sumKeeper+=parseFloat(d.value);
                    d.value=sumKeeper;
                }
            });
            return finalData;
        }
        eChart.colors = [
            'rgb(255, 99, 132)',
            'rgb(54, 162, 235)',
            'rgb(75, 192, 192)',
            'rgb(255, 159, 64)',
            'rgb(255, 205, 86)',
            'rgb(153, 102, 255)',
            // 'rgb(201, 203, 207)',
            '#4dc9f6',
            '#f67019',
            '#f53794',
            '#537bc4',
            '#acc236',
            '#166a8f',
            '#00a950',
            // '#58595b',
            '#8549ba',
            /*'#4676AC',
             '#FE8B00',
             '#F1464D',
             '#63BAB4',
             '#37A846',
             '#F1CD14',
             '#B972A3',
             '#FF95A3',
             '#A2745A',
             '#BCB0AB',*/
            /*'#0091D5','#EA6A47','#A5D8DD','#1C4E80','#7E909A','#202020','#F1F1F1',*/
            '#0073BA',
            '#FF7800',
            '#00A815',
            '#E70000',
            '#9E59C2',
            '#935346'
        ];

        var highlights = ["#1f88c5", "#aed8e9", "#ff8f3f", "#ffbb78", "#2ca02c", "#98df8a", "#d62728", "#ff9896", "#9467bd", "#c5b0d5", "#8c564b", "#c49c94"];

        // Used to remove already drawn canvas.........................
        eChart.clearContainer = function (id) {
            $(id).html("");
            $("#" + id).empty();
        };

        // exclude group keys
        eChart.groupExcludeKey = function (group,excludeKey) {
            var deleteIndex=[];
            Object.keys(excludeKey).forEach(function (d) {
                group[d].forEach(function (p,index) {
                    var indexMatch = excludeKey[d].indexOf(p.key);
                    if(indexMatch!=-1)
                        deleteIndex.push(index);
                });
                deleteIndex.forEach(function (k) {
                    group[d].splice(k,1);
                });
            });
            return group;
        }

        //Generate legend
        eChart.generateLegend = function (id, chart) {
            try {
                var instance = chart.chartInstance;
                //$("#"+id+"> ul").append(instance.generateLegend());
                document.getElementById(id + "select").innerHTML = instance.generateLegend();
                chart._datasetsLength = instance.config.data.datasets.length;
                chart._hiddenDatasets = 0;

                //Select
                $("#" + id + "select > ul > li").on("click", function(e){
                    if(chart.totalKey > 50){
                        return;
                    }
                    var index = $(this).index();
                    if (chart.type == "pieChart" || chart.type == "funnelChart") {
                        $(this).toggleClass("strike");
                        var curr = instance.config.data.datasets[0]._meta[instance.id].data[index];
                        curr.hidden = !curr.hidden;
                    } else {
                        if(eChart.isGroupColorApplied(chart)){
                            var clickLabel = instance.config.data.datasets[index].label;
                            instance.config.data.datasets.forEach(function(dObj){
                                if(dObj.label==clickLabel){
                                    if(dObj._meta[instance.id].hidden==null){
                                        dObj._meta[instance.id].hidden=true;
                                    }else{
                                        dObj._meta[instance.id].hidden=!dObj._meta[instance.id].hidden;
                                    }
                                }
                            });
                            $(this).toggleClass("strike");
                        }else{
                            var curr = instance.config.data.datasets[index]._meta[instance.id];
                            if (curr.hidden == null) {
                                curr.hidden = false;
                            }
                            if (!curr.hidden && chart._datasetsLength - 1 === chart._hiddenDatasets) {

                            } else {
                                curr.hidden = !curr.hidden;
                                $(this).toggleClass("strike");
                                if (curr.hidden) {
                                    chart._hiddenDatasets++;
                                } else {
                                    chart._hiddenDatasets--;
                                }
                            }
                        }
                    }
                    instance.update();
                });
            } catch (e) {

            }
        }

        // used to find add the canvas
        function addCanvas(_chart) {
//          var width = "651px";
            var height = "329px";
            if (_chart.type == 'barChart' || _chart.type == 'composite') {
                var width = "875px";
            } else if (_chart.type == 'horizontalBar') {
                var width = "850px";
            } else {
                var width = "651px";
            }

            if (_chart._dataFormateVal && _chart._dataFormateVal.canvas) {
                var element = $("<canvas id='canvas" + _chart.id + "' width='" + _chart._dataFormateVal.canvas.width + "' height='" + _chart._dataFormateVal.canvas.height + "'></canvas>").appendTo($("#" + _chart.id));
            } else {
                var element = $("<canvas id='canvas" + _chart.id + "' width='" + width + "' height='" + height + "'></canvas>").appendTo($("#" + _chart.id));
            }
            _chart.element = element;
            return element;

        }

        eChart.getCanvasContext = function (chart, callback) {
            var element = addCanvas(chart);
            setTimeout(function () {
                callback(element[0].getContext("2d"));
            }, eChart.constants.DELAY);
        };
        /*
         * Get common sort* data from server side
         */
        eChart.sortData = function (order) {
            /*
             * Check order alreay selected
             */
            if(eChart._activeChart && eChart._activeChart._sort && eChart._activeChart._sort==order){
                demo.showNotification('danger',order.charAt(0).toUpperCase() + order.slice(1)+" order already selected");
            }else{
                eChart._activeChart.sort(order);
            }
        }
        eChart.updateAttributes = function (_chart) {
            var attributes = eChart.chartRegistry.listAttributes();
            if(!$.isEmptyObject(attributes)){
                if (attributes[_chart.id]) {
                    if (attributes[_chart.id].colors){
                        _chart._colorSelection = attributes[_chart.id].colors;
                    }
                    if (attributes[_chart.id].order){
                        _chart._sort = attributes[_chart.id].order;
                    }
                }
            }
        }

        eChart.sortMeasureObj={};
        eChart.getSortMeasure = function(){
            return eChart.sortMeasureObj;
        };

        eChart.setSortMeasure = function(id,measure){
            eChart.sortMeasureObj[id] = measure;
            eChart.chartRegistry.changeSortMeasure(id);
        };
        /*
         * Crosfilter value call from server side | Click event bind
         */
        eChart.bindEvents = function (chart) {
            chart.element.on('click', callback);
            /*document.addEventListener('touchstart',function(e){
             alert("hello");
             });*/
            function callback(evt) {
                var filter = chart.getClickedElementLabel(evt);
                // var activePoints = chart.getElementAtEvent(evt);
                if (filter != null) {
                    if(filter.measure!=undefined){
                        eChart.sortMeasureObj[chart.id]=filter.measure;
                    }else{
                        eChart.sortMeasureObj[chart.id]=filter.datasetLabel;
                    }
                    chart._sortMeasure=eChart.sortMeasureObj[chart.id];
                    try{
                        var pathname = window.location.href;
                        if (pathname[pathname.length - 1] != 1 && pathname.search("publicView")==-1 && pathname.search("sharedView")==-1){
                            if (chart.type == "barChart" || chart.type == "lineChart" || chart.type == "composite" || chart.type == "areaChart" || chart.type == "horizontalBar" || chart.type == "dualAxisChart" || chart.type == "lineBarChart" || chart.type == "bubbleChart" || chart.type == "pieChart" || chart.type == "funnelChart") {
                                // $('.loadingBar').show();
                                var bodyOffsets = document.body.getBoundingClientRect();
                                var scroll = $(".sideSubBarWidth2").scrollTop();
                                var leftSideWidth = $(".sideSubBarWidth1").width() + $(".sidebar").width() + 20;
                                var tempX = evt.pageX - bodyOffsets.left - leftSideWidth + $(".sideSubBarWidth2").scrollLeft();
                                var tempY = evt.pageY + scroll - 55;
                                $('.colorPalette').css('left', tempX);
                                $('.colorPalette').css('top', tempY);
                                $('.colorPalette').show();
                            }else{
                                eChart.addFilter(chart, filter);
                                eChart.applyFiltersOnData(chart, filter, function () {
                                    eChart.updateAll(chart);
                                });
                            }
                        }else{
                            eChart.addFilter(chart, filter);
                            eChart.applyFiltersOnData(chart, filter, function () {
                                eChart.updateAll(chart);
                            });
                        }
                    }catch ($e) {

                    }
                    eChart.activeChart(chart, filter);
                }
            }
        };

        eChart.activeChart = function (chart, filter) {
            eChart._activeChart = chart;
            eChart._activeDataset = filter;
        }

        eChart.addColor = function (color) {
            eChart._activeChart.colorChange(eChart._activeDataset, color);
            $(".colorPalette").hide();
        };

        eChart.excludeKey = function(chartID, excludeKeys){
            var chart = eChart.chartRegistry.get(chartID);
            Object.keys(chart._group).forEach(function(grpKey){
                var deleteIndex=[];
                chart._group[grpKey].forEach(function(d,index){
                    var tempKey=excludeKeys.indexOf(d.key);
                    if(tempKey!=-1)
                        deleteIndex.push(index);
                });
                deleteIndex.sort(function(a,b){return b-a;});
                deleteIndex.forEach(function(index){
                    chart._group[grpKey].splice(index,1);
                });
            });
            eChart.updateChart(chart);
        }

        eChart.applyFilter = function () {
            eChart.addFilter(eChart._activeChart, eChart._activeDataset);
            eChart.applyFiltersOnData(eChart._activeChart, eChart._activeDataset, function () {
                eChart.updateAll(eChart._activeChart);
            });
            $(".colorPalette").hide();
        };
        //Add Filters To Filter List
        eChart.addFilter = function (chart, filter) {
            chart.reset = false;
            if (!chart.filters) {
                chart.filters = [];
                chart.filters.push(filter);
            } else {
                var flag = false;
                var foundIndex = -1;
                chart.filters.forEach(function (prevFilter, index) {
                    if (prevFilter.label == filter.label && prevFilter.datasetLabel == filter.datasetLabel) {
                        flag = true;
                        foundIndex = index;
                    }
                });
                if (flag) {
                    chart.filters.splice(foundIndex, 1);
                } else {
                    chart.filters.push(filter);
                }
                if (chart.filters.length == 0) {
                    chart.reset = true;
                    chart.filters = false;
                }
            }
        };
        eChart.isFiltersApplied = function (_chart, flag) {
            if (eChart._activeChart && eChart._activeChart.filters && eChart._activeChart.filters.length > 0){
                return true;
            } else {
                return false;
            }
        }
        eChart.resetDataToInitialState = function (data, chart, key) {
            var newData = [];
            var labelObj=chart._labelsObject;
            var labels = Object.keys(labelObj);
            data.forEach(function (dataObj, i) {
                if (dataObj.value && dataObj.value[chart._operator[key].key] && dataObj.value[chart._operator[key].key]!=0 && !isNaN(dataObj.value[chart._operator[key].key])) {
                    newData.push(dataObj.value[chart._operator[key].key]);
                }else if (dataObj.value != 0 && !isNaN(dataObj.value)) {
                    newData.push(dataObj.value);
                }else{
                    if(labelObj[dataObj.key])
                        newData.push(null);
                }
            });
            return {data: newData, labels: labels};
        };

        eChart.reConstructData = function (data, chart, key) {
            if(chart.type!='pieChart' && chart.type!='funnelChart'){
                var newData = [];
                var labelObj = chart._labelsObject;
                var labels = Object.keys(labelObj);
                // If runningTotal Applied For Getting wrong data Start
                if(chart._runningTotal && chart._runningTotal[key]){
                    data.forEach(function (dataObj, i) {
                        labels.forEach(function(d, index){
                            if(d == dataObj.key){
                                if (dataObj.value && dataObj.value[chart._operator[key].key] && dataObj.value[chart._operator[key].key]!=0 && !isNaN(dataObj.value[chart._operator[key].key])) {
                                    newData.push(dataObj.value[chart._operator[key].key]);
                                }else if (dataObj.value != 0 && !isNaN(dataObj.value)) {
                                    newData.push(dataObj.value);
                                }else{
                                    if(labelObj[dataObj.key]){
                                        newData.push(null);
                                    }
                                }
                            }
                        });
                    });
                    return {data: newData, labels: labels};
                }
// If runningTotal Applied For Getting wrong data End
                if(data){
                    data.forEach(function (dataObj, i) {
                        if (dataObj.value && dataObj.value[chart._operator[key].key] && dataObj.value[chart._operator[key].key]!=0 && !isNaN(dataObj.value[chart._operator[key].key])) {
                            newData.push(dataObj.value[chart._operator[key].key]);
                        }else if (dataObj.value != 0 && !isNaN(dataObj.value)) {
                            newData.push(dataObj.value);
                        }else{
                            if(labelObj[dataObj.key]){
                                newData.push(null);
                            }
                        }
                    });
                    return {data: newData, labels: labels};
                }else{
                    return {data: newData, labels: labels};
                }
            }else{
                var newData = [];
                var labels = [];
                if(data){
                    data.forEach(function (dataObj, i) {
                        if (dataObj.value && dataObj.value[chart._operator[key].key] && dataObj.value[chart._operator[key].key]!=0 && !isNaN(dataObj.value[chart._operator[key].key])) {
                            newData.push(dataObj.value[chart._operator[key].key]);
                            labels.push(dataObj.key);
                        }else if (dataObj.value != 0 && !isNaN(dataObj.value)) {
                            labels.push(dataObj.key);
                            newData.push(dataObj.value);
                        }
                    });
                    return {data: newData, labels: labels};
                }else{
                    return {data: newData, labels: labels};
                }
            }
        };

        eChart.updateDataset = function (chart, data, key, index, flag) {
            var finalData = {};
            if(eChart.isFiltersApplied(chart, flag)){
                finalData = eChart.reConstructData(data, chart, key);
            }else{
                finalData = eChart.resetDataToInitialState(data, chart, key);
            }
            if(chart._data.labels.length)
                chart.chartInstance.config.data.labels = chart._data.labels;
            if(finalData['data'].length){
                /*
                 * Index coming wrong from chart object
                 */
                var datasetIndex;
                chart.chartInstance.config.data.datasets.forEach(function(d,indexD){
                    if(d.label==key){
                        datasetIndex=indexD;
                    }
                });
                if(datasetIndex==undefined || datasetIndex==''){
                    datasetIndex=index;
                }
                chart.chartInstance.config.data.datasets[datasetIndex].data = finalData['data'];
            }else{
                /*
                 * Two time reset issue that's why i do it like that
                 */
                chart.chartInstance.config.data.datasets[index].data=[null];
            }
            setTimeout(function () {
                var res = chart.id.split("-");
                eChart.generateLegend("left" + res[1], chart);
            }, 1);
        };
        eChart.buildLabels = function(grp, chart){
            var labels={};
            if(grp){
                var keys = Object.keys(grp);
                var dataLength = grp[keys[0]].length;
                for(var i=0; i<dataLength; i++) {
                    var flag = false;
                    var keyOfData = null;
                    keys.forEach(function (key) {
                        var dataObj = grp[key][i];
                        if(dataObj && dataObj.value && chart._operator && dataObj.value[chart._operator[key].key] && dataObj.value[chart._operator[key].key] != 0 && !isNaN(dataObj.value[chart._operator[key].key])) {
                            flag = true;
                        }else if (dataObj && dataObj.value != 0 && !isNaN(dataObj.value)) {
                            flag = true;
                        }
                        if(dataObj){
                            keyOfData = dataObj.key;
                        }
                    });
                    if (flag) {
                        labels[keyOfData] = true;
                    }
                }
            }
            return labels;
        }

        eChart.updateChart = function (chart, flag){
            // try{
            if(chart.type!="bubbleChart"){
                //Because _group take's refrence
                if(chart._group == undefined && chart.tempGroupKey){
                    chart._group = chart.tempGroupKey;
                }
                var grp = chart._group;
                var indexDataset = 0;
                if(chart.type == 'maleFemaleChart'){
                    chart.updateDataset();
                    return;
                }
                chart._labelsObject = eChart.buildLabels(grp, chart);
                var labelsArr=[];
                if (!eChart.isGroupColorApplied(chart)){
                    if(!$.isEmptyObject(grp)){
                        var lastLabelCount=0;
                        $.each(grp, function (key, group){
                            var newData = [];
                            var data = [];
                            data = group;
                            /*if(eChart.isDataFormatApplied(chart)){
                                data = eChart.getDataByDateFormat(group, key, chart);
                            }*/
                            /*
                              Label Object rearrange
                             */
                            var labelsArr=[];
                            var tempData={};
                            data.forEach(function (d) {
                                if(!(d.value instanceof Object) && d.value){
                                    labelsArr.push(d.key.toString());
                                }else if(d.value && d.value[chart._operator[key].key]){
                                    labelsArr.push(d.key.toString());
                                }
                                if(chart._labelsObject[d.key]!=undefined){
                                    tempData[d.key]=true;
                                }
                            });
                            if(chart._runningTotal && chart._runningTotal[key]){
                                data = eChart.getRunTotal(group,key,chart);
                            }
                            if(labelsArr.length && lastLabelCount<labelsArr.length){
                                lastLabelCount=labelsArr.length;
                                chart._data.labels = labelsArr;
                                chart._labelsObject = tempData;
                            }
                            eChart.updateDataset(chart, data, key, indexDataset, flag);
                            indexDataset++;
                        });
                    }
                } else {
                    var tempDataList = {};
                    var dataAfterSort = [];
                    /*
                      stack index for composite chart
                     */
                    var grpMultipleMeasure=[];
                    var stackIndex=0;
                    /*
                        Dualaxis chart
                     */
                    var indexYaxis=1;
                    var lastLabelCount=0;
                    $.each(grp, function (key, group){
                        var data = group;
                        if (eChart.isDataFormatApplied(chart) && !chart.topN && !chart._sort) {
                            data = eChart.getDataByDateFormat(group, key, chart);
                        }
                        var labelsArr=[];
                        var tempData={};
                        data.forEach(function (d) {
                            if(d.value[chart._operator[key].key]){
                                labelsArr.push(d.key);
                            }
                            if(chart._labelsObject[d.key]!=undefined){
                                tempData[d.key]=true;
                            }
                        });
                        if(labelsArr.length && lastLabelCount<labelsArr.length){
                            lastLabelCount=labelsArr.length;
                            chart._labelsObject = tempData;
                            chart._data.labels = labelsArr;
                        }
                        /*
                          stack index for composite chart
                         */
                        if(chart.type=="composite"){
                            tempDataList = chart.updateDatasetByGroupColor(data, key, stackIndex);
                            stackIndex++;
                        }else if(chart.type=="lineBarChart"){
                            var type;
                            if (chart._chartType) {
                                if (chart._chartType[key]) {
                                    type = chart._chartType[key];
                                } else {
                                    var types = ['line', 'bar']
                                    type = types[(Math.random() * types.length) | 0];
                                }
                            } else {
                                var types = ['line', 'bar']
                                type = types[(Math.random() * types.length) | 0];
                            }
                            tempDataList = chart.updateDatasetByGroupColor(data, key,type);
                        }else if(chart.type=="dualAxisChart"){
                            var type;
                            if (chart._chartType) {
                                if (chart._chartType[key]) {
                                    type = chart._chartType[key]['type'];
                                } else {
                                    if(indexYaxis==1){
                                        type="line";
                                    }else{
                                        type="bar";
                                    }
                                }
                            } else {
                                if(indexYaxis==1){
                                    type="line";
                                }else{
                                    type="bar";
                                }
                            }
                            tempDataList = chart.updateDatasetByGroupColor(data, key, type, indexYaxis);
                            indexYaxis++;
                        }else{
                            tempDataList = chart.updateDatasetByGroupColor(data, key);
                        }
                        /*
                          Multiple measure with grp color
                         */
                        tempDataList.forEach(function (d) {
                            grpMultipleMeasure.push(d);
                        });
                    });
                    chart._data.datasets = grpMultipleMeasure;
                }
            }else{
                if(chart._group == undefined && chart.tempGroupKey){
                    chart._group = chart.tempGroupKey;
                }
                chart.updateDataset();
            }
            if(chart._dataFormateVal && chart._dataFormateVal.yaxis && chart._dataFormateVal.yaxis.referenceLine){
                var referenceLine = eChart.getReferenceLineData(chart);
                chart.chartInstance.options.annotation.annotations.forEach(function(d,index){
                    d.value=referenceLine[index].point;
                    /*d.label.content=referenceLine[index].tooltip;*/
                    d.onMouseover=function(e){
                        $("#refrenceLineTooltip").html(referenceLine[index].tooltip);
                        $("#refrenceLineTooltip").show();
                        $('#refrenceLineTooltip').css({'top' : e.clientY,'opacity':1,'left':e.clientX});
                    };
                    d.onMouseout=function(e) {
                        $("#refrenceLineTooltip").hide();
                        $('#refrenceLineTooltip').css({'opacity':0});
                    };
                });
            }
            chart.chartInstance.update();
        };

        eChart.updateAll = function (currentChart) {
            var idList = {};
            var length=eChart.chartRegistry.list().length;
            var timeTaken=length*100;
            var chartDrawPromise=new Promise(function(resolve,reject){
                eChart.chartRegistry
                    .list()
                    .forEach(
                        function (chart) {
                            idList[chart.id] = true;
                            if (chart.id != currentChart.id) {
                                eChart.updateChart(chart, true);
                            } else {
                                try {
                                    chart.highlightSelectedFilter();
                                    chart.chartInstance.update();
                                } catch (e) {

                                }
                            }
                        });
                setTimeout(function(){
                    resolve();
                },timeTaken);
            });
            var sketchChartDraw= new Promise(function(resolve,reject){
                sketchServer.chartRegistry
                    .list()
                    .forEach(function (_chart) {
                        if (!idList[_chart.id] && _chart.id!=currentChart.id && _chart.type!='_numberWidget'){
                            _chart.render();
                        }
                    });
                dc.redrawAll();
                setTimeout(function(){
                    resolve();
                },100);
            });
            chartDrawPromise.then(function(){
                sketchChartDraw.then(function(){
                    setTimeout(function(){
                        var id=$(".custNav li.active").attr('id');
                        $("#"+id).removeClass('active');
                        $("#"+id+" > a").trigger('click');
                        $(".loadingBar").hide();
                    },2000);
                    //$(".loadingBar").hide();
                });
            });
        };


        eChart.redrawEchartChartsOnly = function (id) {
            eChart.updateAll({id:id});
        }


        eChart.updateAllChartsColor = function () {
            var colorSelection = eChart.chartRegistry.listAttributes();
            eChart.chartRegistry.list().forEach(function (chart) {
                var labels = chart.getLabels();
                var colors = null;
                if (colorSelection[chart.id])
                    colors = colorSelection[chart.id].colors;

                if (chart.type != "lineChart" && chart.type != "areaChart" && colors != null) {
                    chart._data.datasets.forEach(function (dataset, dsIndex) {
                        var color = colors[dsIndex];
                        var tempData = [];
                        labels.forEach(function (filter, i) {
                            tempData.push(color);
                        });
                        chart._data.datasets[dsIndex].backgroundColor = tempData;
                        chart._data.datasets[dsIndex].borderColor = tempData;
                    });
                } else {
                    chart._data.datasets.forEach(function (dataset, dsIndex) {
                        var color = colors[dsIndex];
                        chart._data.datasets[dsIndex].backgroundColor = color;
                        chart._data.datasets[dsIndex].borderColor = color;

                    });
                }
                chart._colorSelection = colors;
                chart.chartInstance.update();
            });
        }

        //Function used to redraw chart on applying custom filters
        eChart.redrawAll = function () {
            var idList = {};
            $(".cust-tab-pane").addClass("active");
            eChart.chartRegistry.list().forEach(function (chart) {
                idList[chart.id] = true;
                eChart.updateChart(chart, true);
            });
            sketchServer.chartRegistry.list().forEach(function (_chart) {
                if(_chart.type!="_numberWidget")
                _chart.render();
                //  _chart.resetZoom();
            });
            setTimeout(function () {
                dc.redrawAll();
                var id=$(".custNav li.active").attr('id');
                $("#"+id).removeClass('active');
                $("#"+id+" > a").trigger('click');
                //$(".loadingBar").hide();
            }, 1000);
        };

        eChart.redrawChartsOnly = function () {
            eChart.chartRegistry.list().forEach(function (chart) {
                eChart.updateChart(chart, true);
            });
            setTimeout(function () {
                dc.redrawAll();
            }, 1000);
        };

        //chart data reset
        eChart.resetAll = function(metadataId) {
            sketchServer._pivotCust_Filter_Obj = {};
            sketchServer._pivotCust_Attr_Obj = {};
            sketchServer._pivotCust_Filter_Arr = [];
            // $(".loadingBar").show();
            try {
                eChart.server.filterAll(function(){
                    eChart.chartRegistry
                        .list()
                        .forEach(
                            function (chart) {
                                chart.reset = true;
                                chart.highlightSelectedFilter();
                                /*
                                 * For all legend show
                                 */
                                if(eChart.isGroupColorApplied(chart)){
                                    eChart.crossFilterLegendGrp(chart);
                                }else{
                                    eChart.crossFilterLegend(chart);
                                }
                            });
                    sketchServer.chartRegistry
                        .list()
                        .forEach(
                            function (_chart) {
                                _chart.filter = null;
                                $(".loadingBar").hide();
                            });
                    eChart.redrawAll();
                }, metadataId);
            }catch(e){

            }
        }

        eChart.resetChart=function(id){
            try{
                sketchServer.chartRegistry
                    .list()
                    .forEach(function (_chart) {
                        if(_chart.id == id){
                            (function(){
                                var chart = _chart;
                                chart.render();
                            })();
                        }
                    });
            }catch(e){

            }
        }

        //function to redraw all periods
        eChart.redrawAllP = function () {
            eChart.chartRegistry.list().forEach(function (chart) {
                var grp = chart._group;
                var index = 0;
                $.each(grp, function (key, group) {
                    var newData = [];
                    var labels = [];
                    group.all().forEach(function (dataObj) {
                        labels.push(dataObj.key);
                        newData.push(dataObj.value[chart._operator[key].key]);
                    });
                    chart.chartInstance.config.data.labels = labels;
                    chart.chartInstance.config.data.datasets[index].data = newData;
                    index++;
                });
                chart.chartInstance.update();
            });
        };


        //Create the group mappings
        eChart.createGroupMappings = function (finalData, _chart) {
            var operatorKey = Object.values(_chart._operator)[0].key;
            var groupMapppings = {};
            var data = Object.assign([], finalData);
            var labels = [];
            data.forEach(function (d) {
                if(d.key!=null){
                    labels.push((d.key).toString());
                    if (d.value && d.value.groupColor != undefined) {
                        var opValue = d.value[operatorKey];
                        $.each(d.value.groupColor, function (k, val) {
                            if (groupMapppings[k]) {
                                groupMapppings[k][d.key] = val;
                            } else {
                                groupMapppings[k] = {};
                                groupMapppings[k][d.key] = val;
                            }
                        });
                    }
                }
            });
            _chart._labels = labels;
            return groupMapppings;
        }

        eChart.filterOnServer = function(activeChart,filterObj,callback){
            var filterFlag=true;
            if(filterFlag){
                sketchServer._loadedReportCount = 0;
                sketchServer.chartLoading_Show();
                $(".cust-tab-pane").addClass("active");
                if(eChart._metadataId==undefined){
                    eChart._metadataId=sketchServer._axisConfig['queryObj'];
                }
                /*
                 * For date format check
                 */
                var customFormat=false;
                if(activeChart._dataFormateVal && activeChart._dataFormateVal.xaxis && activeChart._dataFormateVal.xaxis.format){
                    customFormat=true;
                }
                $.post(configData.nodeUrl + "/filter", {"filter":filterObj,"metadataId":eChart._metadataId,"sessionId":sketchServer.getAccessToken(),"customFormat":customFormat})
                    .done(function(data){

                        if(data.status){
                            demo.showNotification('danger',"No Relevant Data Found");
                        }
                        callback(data);
                    }).fail(function(response){
                    $(".loadingBar").hide();
                    demo.showNotification('danger',"Something went wrong reload browser");
                });
            }
        }

        eChart.filterAllOnServer=function(callback,metadataId){
            sketchServer._loadedReportCount = 0;
            if(eChart._metadataId==undefined){
                eChart._metadataId=metadataId;
            }
            $.post(configData.nodeUrl + "/filterAll", {"metadataId":eChart._metadataId,"sessionId":sketchServer.getAccessToken()}).done(function(data){

                callback(data);
            }).fail(function(response) {
                demo.showNotification('danger',"Something went wrong reload browser");
                $(".loadingBar").hide();
            });
        }
        /*
          Legend Object
         */
        eChart.legendChartObj={};
        eChart.crossFilterLegendGrp = function (chart) {
            var legendObj={};
            chart.chartInstance.legend.legendItems.forEach(function (d,index) {
                legendObj[d.text+index]=index;
            });
            chart._data.datasets.forEach(function (d,index) {
                var flag=false;
                d.data.forEach(function (p) {
                    if(p){
                        flag=true;
                    }
                });
                if(flag==false){
                    chart.chartInstance.legend.legendItems[legendObj[d.label+index]].hidden=true;
                }
            });
            eChart.legendChartObj[chart.id]=chart.chartInstance.legend.legendItems;
            //eChart.tempTest=chart.chartInstance.legend.legendItems;
        }

        eChart.crossFilterLegend = function (chart) {
            chart.chartInstance.config.data.datasets.forEach(function(d){
                var flag=false;
                d.data.forEach(function (d) {
                    flag=true;
                });
                $.each(d._meta,function(key,value){
                    if(flag){
                        value.hidden=false;
                    }else{
                        value.hidden=true;
                    }
                });
            });
        }

        eChart.server = (function(){
            return {
                filter:function(activeChart,filters,callback){
                    eChart.filterOnServer(activeChart,filters,function(data){
                        eChart.chartRegistry
                            .list()
                            .forEach(function (chart) {
                                if(activeChart.id!==chart.id){
                                    /* Running total */
                                    try {
                                        var TotalKey=data[chart.id]['totalKey'];
                                        var containerId=chart.id.split("-")[1];
                                        $("#rangeScroll_"+containerId).rangeSlider("destroy");
                                    }catch (e){

                                    }
                                    if(TotalKey>50 && chart.type!="funnelChart" && chart.type!="pieChart"){
                                        $("#rangeScroll_"+containerId).rangeSlider({
                                            bounds: {min: 0, max: TotalKey},
                                            defaultValues:{min: 0, max: 50},
                                            //range: {min: 0, max: 0},
                                            //step: 3,
                                            valueLabels:'hide',
                                            symmetricPositionning:true
                                        }).bind("userValuesChanged", function(e, data){
                                            //lineDrawAgain(parseInt(data.values.min),parseInt(data.values.max));
                                        });
                                    }
                                    try{
                                        delete data[chart.id]['totalKey'];
                                        delete data[chart.id]['reportId'];
                                    }catch(e){
                                        
                                    }

                                    var finalData = data[chart.id];
                                    chart._group = finalData;
                                }
                                sketchServer.reportLoadedCountCheck(chart);
                            });
                        sketchServer.numberWidgetRecordGroup.list().forEach(function(id){
                            sketchServer.numberWidgetRecordGroup.set(id,data[id]['numberWidget']);
                            sketchServer.reportLoadedCountCheck(id);
                        });
                        setTimeout(function () {
                            dc.redrawAll();
                            callback();
                            /*
                              For legend redraw chart again
                             */
                            eChart.chartRegistry
                                .list()
                                .forEach(function (chart) {
                                    if (activeChart.id !== chart.id && eChart.isGroupColorApplied(chart)) {
                                        eChart.crossFilterLegendGrp(chart);
                                        chart.render();
                                    }else if(activeChart.id !== chart.id){
                                        eChart.crossFilterLegend(chart);
                                        chart.render();
                                    }
                                    /*
                                     * Crossfilter data tooltip process
                                     */
                                    eChart.processTooltipData(chart);
                                });
                        },50);
                    });
                },

                filterAll:function(callback,metadataId){
                    eChart.filterAllOnServer(function(data){
                        eChart.chartRegistry
                            .list()
                            .forEach(
                                function (chart) {
                                    /* Running total */
                                    try {
                                        var TotalKey=data[chart.id]['totalKey'];
                                        var containerId=chart.id.split("-")[1];
                                        $("#rangeScroll_"+containerId).rangeSlider("destroy");
                                    }catch (e){

                                    }
                                    if(TotalKey>50 && chart.type!="funnelChart" && chart.type!="pieChart"){
                                        $("#rangeScroll_"+containerId).rangeSlider({
                                            bounds: {min: 0, max: TotalKey},
                                            defaultValues:{min: 0, max: 50},
                                            //range: {min: 0, max: 0},
                                            //step: 3,
                                            valueLabels:'hide',
                                            symmetricPositionning:true
                                        }).bind("userValuesChanged", function(e, data){
                                            //lineDrawAgain(parseInt(data.values.min),parseInt(data.values.max));
                                        });
                                    }
                                    try{
                                        delete data[chart.id]['totalKey'];
                                        delete data[chart.id]['reportId'];
                                    }catch (e){

                                    }
                                    var finalData = data[chart.id];
                                    chart._group=finalData;
                                });
                        sketchServer.numberWidgetRecordGroup.list().forEach(function(id){
                            if(data[id] && data[id]['numberWidget']){
                                sketchServer.numberWidgetRecordGroup.set(id,data[id]['numberWidget']);
                            }
                        });
                        setTimeout(function () {
                            dc.redrawAll();
                            callback();
                            /*
                              For legend redraw chart again
                             */
                            eChart.chartRegistry
                                .list()
                                .forEach(function (chart) {
                                    /*
                                     * Crossfilter data tooltip process
                                     */
                                    eChart.processTooltipData(chart);
                                    if(eChart.isGroupColorApplied(chart)){
                                        eChart.crossFilterLegendGrp(chart);
                                        chart.render();
                                    }
                                });
                        },50);
                    },metadataId);
                },

                externalFilterDataUpdate:function(data,callback){
                    eChart.chartRegistry
                        .list()
                        .forEach(
                            function (chart) {
                                /* Running total  */
                                try {
                                    var TotalKey=data[chart.id]['totalKey'];
                                    var containerId=chart.id.split("-")[1];
                                    $("#rangeScroll_"+containerId).rangeSlider("destroy");
                                }catch (e){

                                }
                                if(TotalKey>50 && chart.type!="funnelChart" && chart.type!="pieChart"){
                                    $("#rangeScroll_"+containerId).rangeSlider({
                                        bounds: {min: 0, max: TotalKey},
                                        defaultValues:{min: 0, max: 50},
                                        //range: {min: 0, max: 0},
                                        //step: 3,
                                        valueLabels:'hide',
                                        symmetricPositionning:true
                                    }).bind("userValuesChanged", function(e, data){
                                        //lineDrawAgain(parseInt(data.values.min),parseInt(data.values.max));
                                    });
                                }

                                if(data && data[chart.id]){
                                    delete data[chart.id]['totalKey'];
                                    delete data[chart.id]['reportId'];
                                }

                                var finalData = data[chart.id];
                                //        var keys = Object.keys(finalData);
                                //        keys.forEach(function(d){
                                // if(chart._measures[d]['runningTotal']){
                                //              var runingData=eChart.getRunTotal(finalData[d],d,chart);
                                //     finalData[d]=runingData;
                                //      }
                                //        });
                                /* End Running Total*/
                                chart._group=finalData;
                                /*
                                 * Top filter data tooltip process
                                 */
                                eChart.processTooltipData(chart);
                            });
                    sketchServer.numberWidgetRecordGroup.list().forEach(function(id){
                        if(data[id] && data[id]['numberWidget'])
                            sketchServer.numberWidgetRecordGroup.set(id,data[id]['numberWidget']);
                    });
                    setTimeout(function () {
                        callback();
                    },50);
                }
            }
        })();


        // ----------------------Apply Filters On Data----------------------------------------------------------//

        eChart.applyFiltersOnData = function (activeChart, currentFilter, callback) {
            try {
                var filters = [];
                var flagDimensionOther=0;
                if(activeChart.filters.length){
                    activeChart.filters.forEach(
                        function (filter) {
                            if(filter.label=="other"){
                                flagDimensionOther=1;
                            }else{
                                filters.push(filter.label);
                            }
                        }
                    );
                }
                var datasetFilters = [];
                if (eChart.isGroupColorApplied(activeChart)) {
                    if(activeChart.filters.length){
                        activeChart.filters.forEach(function (filter) {
                            datasetFilters.push(filter['datasetLabel']);
                        });
                    }
                }
            } catch (e) {

            }
            if(activeChart.otherDimension && flagDimensionOther){
                activeChart.otherDimension.forEach(function(d){
                    filters.push(d);
                });
            }
            var filterObj={
                filters:filters,
                datasetFilters:datasetFilters,
                reportId:activeChart.id,
                reset:activeChart.reset
            };
            eChart.server.filter(activeChart,filterObj,function(){
                callback();
            });
        };
        // ----------------------CHART DATA MANUPLATION FROM DIMENSION------------------------------------------//

        eChart.getDataInFormat = function (chart) {
            var colorsLength = colors.length;
            var grpObj = chart._group.all();
            var labels = [];
            var datasets = [];
            var data = [];
            var backgroundColor = [];
            var i = 0;
            grpObj.forEach(function (d) {
                labels.push(d.key);
                data.push(d.value[chart._operator]);
                backgroundColor.push(chart._colorSelection[i % colorsLength]);
                i++;
            });
            chart._allLabels = labels;
            var dt = {
                data: data,
                backgroundColor: backgroundColor
            };
            datasets.push(dt);
            var dataInFormat = {
                labels: labels,
                datasets: datasets
            };
            return dataInFormat;
        };

        //Check if Group Color Applied
        eChart.isGroupColorApplied = function (_chart) {
            if(_chart._dataFormateVal && _chart._dataFormateVal.groupColor){
                return true;
            }
            return false;
        }


        //eChart create options for chart...................................................

        eChart.createOption = function (chart) {
            var labelColor, axisLineColor;
            if(chart._dataFormateVal && chart._dataFormateVal.axisLabelColor){
                labelColor = chart._dataFormateVal.axisLabelColor;
            }else{
                labelColor = '#666666';
            }
            if(chart._dataFormateVal && chart._dataFormateVal.axisLabelColor){
                axisLineColor = chart._dataFormateVal.axisColor;
            }else{
                axisLineColor = '#e6e6e6';
            }


            /*
             * reference Line
             */
            var referenceLineData = eChart.getReferenceLineData(chart);
            chart.referenceLinePoint = referenceLineData.point;
            chart.referenceLineTooltip = referenceLineData.tooltip;
            var annotationData=[];
            var modeType, scale_ID;

            referenceLineData.forEach(function (d,index) {
                if(chart.type == 'horizontalBar'){
                    modeType = "vertical";
                    scale_ID = "x-axis-0";
                }else if(chart.type == 'dualAxisChart'){
                    modeType = "horizontal";
                    if(d.yaxis == 'yaxis1'){
                        scale_ID = "y-axis-1";
                    }else if(d.yaxis == 'yaxis2'){
                        scale_ID = "y-axis-2";
                    }
                }else{
                    modeType = "horizontal";
                    scale_ID = "y-axis-0";
                }

                var obj={
                    drawTime: "afterDatasetsDraw",
                    id: "hline"+index,
                    type: "line",
                    mode: modeType,
                    scaleID: scale_ID,
                    value: d.point,
                    borderColor: "black",
                    borderWidth: 2,
                    borderDash: [10, 10],
                    onMouseover:function(e){
                        /*var element = this;
                        element.options.borderWidth = 2;
                        element.options.label.enabled = true;
                        element.options.label.content = d.tooltip;
                        element.chartInstance.chart.canvas.style.cursor = 'pointer';
                        element.chartInstance.update();*/
                        $("#refrenceLineTooltip").html(d.tooltip);
                        $("#refrenceLineTooltip").show();
                        $('#refrenceLineTooltip').css({'top' : e.clientY,'opacity':1,'left':e.clientX});
                    },
                    onMouseleave: function(e) {
                        /*var element = this;
                        element.options.borderWidth = 2;
                        element.options.label.enabled = false;
                        element.chartInstance.chart.canvas.style.cursor = 'pointer';
                        element.chartInstance.update();*/
                        $("#refrenceLineTooltip").hide();
                        $('#refrenceLineTooltip').css({'opacity':0});
                    },
                }
                annotationData.push(obj);
            });
            /*
             * scale labels
             */
            var scaleLabels=false;
            var axisLabelHeader=true;
            if(chart._dataFormateVal && chart._dataFormateVal.yaxis){
                if(chart._dataFormateVal.yaxis.axisLabel){
                    scaleLabels=chart._dataFormateVal.yaxis.axisLabel;
                }
                if(chart._dataFormateVal.yaxis.axisHeader!=undefined){
                    axisLabelHeader=chart._dataFormateVal.yaxis.axisHeader;
                }
            }

            /*
             *  Measures value
             */
            var measuresValue="";
            if(chart._measures){
                if(Object.keys(chart._measures).length>1){
                    measuresValue="Values";
                }else{
                    measuresValue=Object.keys(chart._measures)[0];
                }
            }
            var fontSize = chart._yfontSize;
            if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
                fontSize = 30;
            }
            var option = {};
            option.title={
                display: false,
            };

            if(chart._dataFormateVal && chart._dataFormateVal.canvas && chart._dataFormateVal.canvas.width && chart._dataFormateVal.canvas.height){
                option.responsive = false;
            }else{
                option.responsive = true;
            }
            option.maintainAspectRatio = false;
            /*
             * crossfilter (Cannot read property 'transition' of null)
             */
            option.animation = {
                duration: 0
            };

            option.hover = {
                animationDuration: 0
            };
            option.responsiveAnimationDuration = 0;
            option.legend = {
                display: false
            };
            option.legendCallback = function (chartInstance) {
                // update legend not working when we go from edit to view
                // if(eChart.legendChartObj[chart.id]!=undefined){
                //     chartInstance.legend.legendItems=eChart.legendChartObj[chart.id];
                // }
                var html='';
                if(chart.groupColor)
                    html +="<li title="+JSON.parse(chart.groupColor).reName+"><p>&nbsp;&nbsp;"+JSON.parse(chart.groupColor).reName.slice(0, 16) + (JSON.parse(chart.groupColor).reName.length > 16 ? "..." : "") +"</p></li>";
                    html += "<ul>";
                if (chart._groupColor) {
                    $.each(chart._groupColor, function (key, value) {
                        html += '<li class=""><span style="background-color:' + value + '"></span>' + key + '</li>';
                    });
                } else {
                    var legends=[];
                    var legendsObj={};
                    /*
                       Duplicate legend remove
                     */
                    chartInstance.legend.legendItems.forEach(function (d) {
                        if(legendsObj[d.text]==undefined){
                            legendsObj[d.text]=true;
                            legends.push(d);
                        }
                    });
                    legends.forEach(function (d) {
                        if(d.hidden==false){
                            var title=d.text.replace(/ /g,"&nbsp;");
                            html += '<li class="" title='+title+'><span style="background-color:' + d.fillStyle + '"></span>' + d.text.slice(0, 12) + (d.text.length > 12 ? "..." : "") + '</li>';
                            //strokeStyle
                        }
                    });
                }
                html += "</ul>";
                return html;
            };
            option.annotation = {
                events: ['click', 'mouseover', 'mouseout'],
                annotations:annotationData
            };
            option.title = {
                display: false,
                text: ''
            };
            // if(eChart.isGroupColorApplied(chart)){
            //      option.scales = {
            //              xAxes: chart.xAxes(),
            //              yAxes: [{
            //                  stacked: true
            //              }]
            //             };
            //  }

            if (chart.type == 'composite') {
                if(eChart.isGroupColorApplied(chart)){
                    option.scales = {
                        xAxes: chart.xAxes(),
                        yAxes: [{
                            stacked: true,
                            scaleLabel: {
                                display:scaleLabels,
                                labelString:measuresValue
                            },
                            gridLines: {
                                color: axisLineColor,
                                display: axisLabelHeader,
                            },
                            ticks: {
                                fontColor: labelColor,
                                display: axisLabelHeader,
                                beginAtZero: true,
                                userCallback: function (value, index, values) {
                                    if (chart._dataFormateVal && chart._dataFormateVal.yaxis) {
                                        var yAxisObj=chart._dataFormateVal.yaxis;
                                        if(yAxisObj.decimal){
                                            value = value.toFixed(chart._dataFormateVal.yaxis.decimal);
                                        }
                                        if(yAxisObj.units=="yes"){
                                            value = eChart.formatNumber(value)
                                        }
                                        if(yAxisObj.separator){
                                            value = value.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
                                        }
                                        if(yAxisObj.prefix){
                                            value = yAxisObj.prefix+" "+value;
                                        }
                                        if(yAxisObj.suffix){
                                            value = value+" "+yAxisObj.suffix;
                                        }
                                        return value;
                                    } else {
                                        return eChart.formatNumber(value);
                                    }
                                },
                                fontSize: fontSize,
                                fontFamily: 'sans-serif'
                            }
                        }]
                    };
                }else{
                    option.scales = {
                        xAxes: chart.xAxes(),
                        yAxes: [{
                            stacked: false,
                            scaleLabel: {
                                display:scaleLabels,
                                labelString:measuresValue
                            },
                            gridLines: {
                                color: axisLineColor,
                                display: axisLabelHeader,
                            },
                            ticks: {
                                fontColor: labelColor,
                                display: axisLabelHeader,
                                beginAtZero: true,
                                userCallback: function (value, index, values) {
                                    if (chart._dataFormateVal && chart._dataFormateVal.yaxis) {
                                        var yAxisObj=chart._dataFormateVal.yaxis;
                                        if(yAxisObj.decimal){
                                            value = value.toFixed(chart._dataFormateVal.yaxis.decimal);
                                        }
                                        if(yAxisObj.units=="yes"){
                                            value = eChart.formatNumber(value)
                                        }
                                        if(yAxisObj.separator){
                                            value = value.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
                                        }
                                        if(yAxisObj.prefix){
                                            value = yAxisObj.prefix+" "+value;
                                        }
                                        if(yAxisObj.suffix){
                                            value = value+" "+yAxisObj.suffix;
                                        }
                                        return value;
                                    } else {
                                        return eChart.formatNumber(value);
                                    }
                                },
                                fontSize: fontSize,
                                fontFamily: 'sans-serif'
                            }
                        }]
                    };
                }
            }else if (chart.type == "horizontalBar") {
                /*
                 * scale labels
                 */
                var scaleLabels=false;
                if(chart._dataFormateVal && chart._dataFormateVal.xaxis && chart._dataFormateVal.xaxis.axisLabel){
                    scaleLabels=chart._dataFormateVal.xaxis.axisLabel;
                }
                var dualXaxisLabel=false;
                var dualXaxisLabelVal="";
                var axisLabelHeader=true;
                var allAxisHeader=false;
                var axisHeaderRotate=0;
                var autoSkip=false;
                var dualXAxisFlag=false;
                /*
                 * Dual x axis flag
                 */
                if(chart._dataFormateVal && chart._dataFormateVal.xaxis){
                    /*
                     * X axis name
                     */
                    if(chart._dataFormateVal.xaxis.dualX=='yes'){
                        dualXAxisFlag=true;
                    }
                    if(chart._dataFormateVal.xaxis.dualXaxisLabel){
                        dualXaxisLabel=chart._dataFormateVal.xaxis.dualXaxisLabel;
                        dualXaxisLabelVal=JSON.parse(chart._dataFormateVal.xaxis.dualXDimension).reName;
                    }
                    /*
                     * Axis label option
                     */
                    if(chart._dataFormateVal.xaxis.axisHeader!=undefined){
                        axisLabelHeader=chart._dataFormateVal.xaxis.axisHeader;
                    }
                    /*
                     * All axis label
                     */
                    if(chart._dataFormateVal.xaxis.allAxisHeader!=undefined){
                        allAxisHeader=chart._dataFormateVal.xaxis.allAxisHeader;
                        if(allAxisHeader==true){
                            allAxisHeader=false;
                            autoSkip=false;
                        }else{
                            allAxisHeader=10;
                            autoSkip=true;
                        }
                    }
                    /*
                     * Rotate label
                     */
                    if(chart._dataFormateVal.xaxis.axisHeaderRotate!=undefined){
                        axisHeaderRotate=chart._dataFormateVal.xaxis.axisHeaderRotate;
                    }
                }
                /*
                 *  dimension value
                 */
                var dimensionValue="";
                if(chart._dimension){
                    dimensionValue=Object.keys(chart._dimension)[0];
                }

                if(/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
                    chart._fontSize=30;
                }
                var dualXSkipLabel={};
                option.scales = {
                    xAxes: chart.xAxes(),
                    yAxes: [{
                        id:'xAxis1',
                        type:"category",
                        stacked: true,
                        scaleLabel: {
                            display: scaleLabels,
                            labelString: dimensionValue
                        },
                        gridLines: {
                            color: axisLineColor,
                            display: axisLabelHeader,
                        },
                        ticks:{
                            fontColor: labelColor,
                            display: axisLabelHeader,
                            fontSize: chart._xfontSize,
                            autoSkip: autoSkip,
                            maxRotation: axisHeaderRotate,
                            maxTicksLimit : allAxisHeader,
                            minRotation: axisHeaderRotate,
                            fontFamily: "sans-serif",
                            callback:function(label,index,lables){
                                label=label.toString();
                                if(allAxisHeader){
                                    if(index%Math.ceil(lables.length/10)==0 || lables.length-1==index){
                                        return label.slice(0, 15) +(label.length > 15 ? "..." : "");
                                    }else{
                                        return "";
                                    }
                                }
                                return label.slice(0, 15) +(label.length > 15 ? "..." : "");
                            }
                        }
                    },
                    {
                        id:'xAxis2',
                        type:"category",
                        stacked: true,
                        display:dualXAxisFlag,
                        position: 'right',
                        scaleLabel: {
                            display: dualXaxisLabel,
                            labelString: dualXaxisLabelVal
                        },
                        gridLines: {
                            color: axisLineColor,
                            drawOnChartArea: true, // only want the grid lines for one axis to show up
                            //tickMarkLength: 10,
                            offsetGridLines: true
                        },
                        ticks:{
                            fontColor: labelColor,
                            fontSize: chart._xfontSize,
                            maxRotation: 0,
                            minRotation: 0,
                            autoSkip: autoSkip,
                            fontFamily: "sans-serif",
                            callback:function(label,index,lables){
                                label=label.toString();
                                if(dualXSkipLabel[chart._data.dualXLabel[label]]==undefined){
                                    dualXSkipLabel[chart._data.dualXLabel[label]]=index;
                                }
                                if(dualXSkipLabel[chart._data.dualXLabel[label]]==index){
                                    return chart._data.dualXLabel[label];
                                }
                                return "";
                            }
                        }
                    }]
                };
            } else {
                option.scales = {
                    xAxes: chart.xAxes(),
                    yAxes: chart.yAxes(chart)
                };
            }
            if(chart.type=='boxPlotChart'){
                option.tooltips = {
                    enabled: true,
                    callbacks: {
                        title: function (t, d) {
                            return "";
                        },
                        label: function(tooltipItem, data) {
                            return "";
                            //return eChart.customTooltipTitle(tooltipItem, data,chart);
                        },
                        footer: function(t, d){
                            if(t.length){
                                return eChart.customTooltipFooter(t, d, chart);
                            }
                        }
                    }/*,
                    filter: function(item, data) {
                        /!*
                         * Null tooltip hide
                         *!/
                        var data = data.datasets[item.datasetIndex].data[item.index];
                        return !isNaN(data) && data !== null;
                    }*/
                };
            }else{
                option.tooltips = {
                    enabled: true,
                    callbacks: {
                        title: function (t, d) {
                            return "";
                        },
                        label: function(tooltipItem, data) {
                            return eChart.customTooltipTitle(tooltipItem, data,chart);
                        },
                        footer: function(t, d){
                            if(t.length){
                                return eChart.customTooltipFooter(t, d, chart);
                            }
                        }
                    },
                    filter: function(item, data) {
                        /*
                         * Null tooltip hide
                         */
                        var data = data.datasets[item.datasetIndex].data[item.index];
                        return !isNaN(data) && data !== null;
                    }
                };
            }


            //if (chart.type == "horizontalBar" || chart.type == "dualAxisChart" || chart.type == "lineBarChart" ) {
            option.pan = {
                // Boolean to enable panning
                enabled: false,
                // Panning directions. Remove the appropriate direction to disable
                // Eg. 'y' would only allow panning in the y direction
                mode: 'y'
            };
            option.zoom = {
                // Boolean to enable zooming
                enabled: false,
                /*drag: true,*/
                sensitivity: 0,
                // Zooming directions. Remove the appropriate direction to disable
                // Eg. 'y' would only allow zooming in the y direction
                mode: 'y',
            };
            option.animation = eChart.markLabels(chart);
            return option;
        }
        /*
         * Legend callback
         */
        eChart.legendCallback = function (chartInstance) {
            if(eChart.legendChartObj[chart.id]!=undefined){
                chartInstance.legend.legendItems=eChart.legendChartObj[chart.id];
            }
            var html='';
            if(chart.groupColor)
                html +="<li title="+JSON.parse(chart.groupColor).reName+"><p>&nbsp;&nbsp;"+JSON.parse(chart.groupColor).reName.slice(0, 16) + (JSON.parse(chart.groupColor).reName.length > 16 ? "..." : "") +"</p></li>";
            html += "<ul>";
            if (chart._groupColor) {
                $.each(chart._groupColor, function (key, value) {
                    html += '<li class=""><span style="background-color:' + value + '"></span>' + key + '</li>';
                });
            } else {
                var legends=[];
                var legendsObj={};
                /*
                   Duplicate legend remove
                 */
                chartInstance.legend.legendItems.forEach(function (d) {
                    if(legendsObj[d.text]==undefined){
                        legendsObj[d.text]=true;
                        legends.push(d);
                    }
                });
                legends.forEach(function (d) {
                    if(d.hidden==false){
                        var title=d.text.replace(/ /g,"&nbsp;");
                        html += '<li class="" title='+title+'><span style="background-color:' + d.fillStyle + '"></span>' + d.text.slice(0, 12) + (d.text.length > 12 ? "..." : "") + '</li>';
                        //strokeStyle
                    }
                });
            }
            html += "</ul>";
            return html;
        }

        /*
          Tooltip custom title
         */
        eChart.customTooltipTitle=function(t,d,chart){
            var dimension=Object.keys(chart._dimension)[0];
            if(chart.groupColor){
                return JSON.parse(chart.groupColor).reName+" : "+d.datasets[t.datasetIndex].label;
            }else{
                return dimension+" : "+d.labels[t.index]
            }
        }

        String.prototype.replaceAll = function(a, b) {
            return this.replace(new RegExp(a.replace(/([.?*+^$[\]\\(){}|-])/ig, "\\$1"), 'ig'), b)
        }

        eChart.checkFloat = function(n){
            return Number(n) === n && n % 1 !== 0;
        }

        eChart.isInt = function(n){
            return Number(n) === n && n % 1 === 0;
        }

        eChart.getLineReferenceData = function(r, chart){
            var dataArr = [];
            if(Object.keys(chart._measures).length <= 1){
                chart._data.datasets.forEach(function (d){
                    d.data.forEach(function (dd){
                        if(dd != null){
                            dataArr.push(dd);
                        }
                    });
                });
            }else{
                chart._data.datasets.forEach(function (d){
                    if(d.measure == r.measure || d.label == r.measure){
                        d.data.forEach(function (dd){
                            if(dd != null){
                                dataArr.push(dd);
                            }
                        });
                    }
                });
            }
            return dataArr;
        }

        eChart.getBarReferenceData = function(r, chart){
            var dataArr = [];
            if(Object.keys(chart._measures).length <= 1){
                chart._data.datasets.forEach(function (d, index){
                    if(eChart.isGroupColorApplied(chart)){
                        d.data.forEach(function (dd,i){
                            if(dataArr[i]==undefined){
                                dataArr[i]=0;
                            }
                            dataArr[i] += dd;
                        });
                    }else{
                        d.data.forEach(function (dd){
                            if(dd != null){
                                dataArr.push(dd);
                            }
                        });
                    }
                });
            }else{
                chart._data.datasets.forEach(function (d){
                    if(d.measure == r.measure || d.label == r.measure){
                        if(eChart.isGroupColorApplied(chart)){
                            d.data.forEach(function (dd,i){
                                if(dataArr[i]==undefined){
                                    dataArr[i]=0;
                                }
                                dataArr[i] += dd;
                            });
                        }else{
                            d.data.forEach(function (dd){
                                if(dd != null){
                                    dataArr.push(dd);
                                }
                            });
                        }
                    }
                });
            }
            return dataArr;
        }

        eChart.getReferenceLineData = function(chart){
            var referenceArr = [];
            if(chart._dataFormateVal && chart._dataFormateVal.yaxis && chart._dataFormateVal.yaxis.referenceLine){
                chart._dataFormateVal.yaxis.referenceLineArr.forEach(function (r) {
                    var dataArr = [];
                    var referenceObj = {};
                    var value;
                    if(chart.type == "lineChart"){
                        dataArr = eChart.getLineReferenceData(r, chart);
                    }else if(chart.type == 'dualAxisChart' || chart.type == 'lineBarChart'){
                        if(chart._chartType[r.measure].type == 'line'){
                            dataArr = eChart.getLineReferenceData(r, chart);
                        }else if(chart._chartType[r.measure].type == 'bar'){
                            dataArr = eChart.getBarReferenceData(r, chart);
                        }
                    }else{
                        dataArr = eChart.getBarReferenceData(r, chart);
                    }

                    if(r.aggregate == 'Average'){
                        var arrAvg = dataArr.reduce(function(a,b){
                            return a + b;
                        }, 0);
                        value = arrAvg/dataArr.length;
                        if(eChart.checkFloat(value)){
                            value = value.toFixed(2);
                        }
                    }else if(r.aggregate == 'Minimum'){
                        value = Math.min.apply(null, dataArr);
                        if(eChart.checkFloat(value)){
                            value = value.toFixed(2);
                        }
                    }else if(r.aggregate == 'Maximum'){
                        value = Math.max.apply(null, dataArr);
                        if(eChart.checkFloat(value)){
                            value = value.toFixed(2);
                        }
                    }else if(r.aggregate == 'Sum'){
                        value = dataArr.reduce(function(a,b){
                            return a + b;
                        }, 0);
                        if(eChart.checkFloat(value)){
                            value = value.toFixed(2);
                        }
                    }
                    if(r.yaxis != undefined){
                        referenceObj['yaxis'] = r.yaxis;
                    }
                    referenceObj['point'] = parseInt(value);
                    referenceObj['tooltip'] = r.aggregate + ' - ' + r.measure + ' : ' + value;
                    referenceArr.push(referenceObj);
                });
            }
            return referenceArr;
        }


        eChart.customTooltipFooter = function(t,d,chart){
            if(chart.type=="pieChart" || chart.type=="funnelChart"){
                chart._dataSetLabels=chart._data.labels;
            }
            var title=d.labels[t[0].index];
            var dimension=Object.keys(chart._dimension)[0];
            var tempArr=[];
            if(chart.groupColor) {
                tempArr.push(dimension + " : " + d.labels[t[0].index]);
            }
            var defaultValue = d.datasets[t[0].datasetIndex].data[t[0].index];

            var FixedVal = 2;
            if(chart._dataFormateVal && chart._dataFormateVal.yaxis && chart._dataFormateVal.yaxis.decimal!=undefined){
                FixedVal = chart._dataFormateVal.yaxis.decimal;
            }

            if(eChart.checkFloat(defaultValue)){
                defaultValue = defaultValue.toFixed(FixedVal);
            }

            /*
             * Dual  x axis tooltip
             */
            if(chart._dataFormateVal && chart._dataFormateVal.xaxis && chart._dataFormateVal.xaxis.dualX && chart._dataFormateVal.xaxis.dualXDimension){
                var dimensionDualX=JSON.parse(chart._dataFormateVal.xaxis.dualXDimension).reName;
                tempArr.push(dimensionDualX+" : "+ chart._data.dualXLabel[title]);
            }
            if(chart.type=="boxPlotChart"){
                $.each(chart._data.datasets[t[0].datasetIndex].data[t[0].datasetIndex].__stats,function (key,value) {
                    if(key=="min" || key=="max" || key=="median" || key=="q1" || key=="q3"){
                        tempArr.push(key+" : "+ value);
                    }
                });
            }

            if(defaultValue!=null){
                /*
                 * Tooltip format
                 */
                if (chart._dataFormateVal && chart._dataFormateVal.yaxis) {
                    var yAxisObj=chart._dataFormateVal.yaxis;
                    if(yAxisObj.decimal!=undefined){
                        defaultValue = Number(defaultValue);
                        defaultValue = defaultValue.toFixed(chart._dataFormateVal.yaxis.decimal);
                    }
                    if(yAxisObj.units=="yes"){
                        defaultValue = eChart.formatNumber(defaultValue);
                    }
                    if(yAxisObj.separator){
                        defaultValue = defaultValue.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
                    }
                    if(yAxisObj.prefix){
                        defaultValue = " "+yAxisObj.prefix+" "+defaultValue;
                    }
                    if(yAxisObj.suffix){
                        defaultValue = defaultValue+" "+yAxisObj.suffix;
                    }
                } else {
                    defaultValue = eChart.formatNumber(defaultValue);
                }
                /*
                 * Default value
                 */
                if(eChart.isGroupColorApplied(chart)){
                    if(chart.type!="boxPlotChart"){
                        tempArr.push(chart._data.datasets[t[0].datasetIndex].measure+" : "+defaultValue);
                    }
                }else{
                    if(chart.type=="pieChart" || chart.type=="funnelChart"){
                        tempArr.push(Object.keys(chart._measures)[0]+" : "+defaultValue);
                    }else{
                        if(chart._dataSetLabels[t[0].datasetIndex]==undefined){
                            tempArr.push(chart._dataSetLabels[chart._dataSetLabels.length-1]+" : "+defaultValue);
                        }else{
                            tempArr.push(chart._dataSetLabels[t[0].datasetIndex]+" : "+defaultValue);
                        }
                    }
                }
                /*
                 * Custom tooltip
                 */
                var tooltipObj = chart._dataFormateVal.tooltip;
                if(tooltipObj){
                    tooltipObj=tooltipObj.replace("{title}\n{xLabel}:{yLabel}","");
                    if(!eChart.isGroupColorApplied(chart)){
                        $.each(chart._tooltipData[title],function (k,v) {
                            if(k!='count' && k!='sum'&& k!='avg' && tooltipObj.includes(k)){
                                if(!isNaN(v)){
                                    v = v.toFixed(FixedVal);
                                }
                                tooltipObj = tooltipObj.replaceAll(k, v);
                            }
                        });
                        var tooltipArr = tooltipObj.split("\n");
                        tooltipArr.forEach(function (d) {
                            tempArr.push(d);
                        });
                    }else{
                        var grpkey = d.datasets[t[0].datasetIndex].label;
                        if(chart._tooltipData[title] && chart._tooltipData[title].tooltipObj){
                            $.each(chart._tooltipData[title].tooltipObj,function (k,v) {
                                try{
                                    if(!isNaN(v)){
                                        v = v.toFixed(FixedVal);
                                    }
                                }catch (e){

                                }

                                var aggr="";
                                if(k.includes("@@#")){
                                    aggr=k.split("@@#")[1];
                                }
                                if("<"+aggr+"("+k+")>"+"@@!"+grpkey+"@@#"+aggr || "<"+k+">"+"@@!"+grpkey){
                                    var aggr1="";
                                    if(aggr){
                                        aggr1="@@#"+aggr;
                                    }
                                    k=k.replace("@@!"+grpkey+aggr1,"");
                                    tooltipObj=tooltipObj.replaceAll("<"+aggr+"("+k+")>",v);
                                    tooltipObj=tooltipObj.replaceAll("<"+k+">",v);
                                }
                            });
                        }
                        var tooltipArr=tooltipObj.split("\n");
                        tooltipArr.forEach(function (d) {
                            tempArr.push(d);
                        });
                    }
                }
            }else{
                if(chart.type=="pieChart" || chart.type=="funnelChart"){
                    tempArr.push(Object.keys(chart._measures)[0]+" : "+defaultValue);
                }else{
                    if(chart._dataSetLabels[t[0].datasetIndex]==undefined){
                        tempArr.push(chart._dataSetLabels[chart._dataSetLabels.length-1]+" : "+defaultValue);
                    }else{
                        tempArr.push(chart._dataSetLabels[t[0].datasetIndex]+" : "+defaultValue);
                    }
                }
            }
            return tempArr;
        }


        eChart.decodeTimeLineObject = function () {
            return {
                time1: {
                    start: '2015-07-01',
                    end: '2016-07-01'
                },
                time2: {
                    start: '2016-07-01',
                    end: '2017-07-01'
                },
                time3: {
                    start: '2018-07-01',
                    end: '2019-07-01'
                }
            };
        }
        //Echart Parsing tooltips
        eChart.parseTooltip = function (format, data, tooltipItems) {
            try {
                var formatText = format.replace("{xLabel}", data.datasets[tooltipItems.datasetIndex].label);
                formatText = formatText.replace("{yLabel}", tooltipItems.yLabel);
                return formatText;
            } catch (e) {
                return "";
            }
        }

        eChart.isDataFormatApplied = function (_chart) {
            if (_chart._dataFormateVal && _chart._dataFormateVal.xaxis && _chart._dataFormateVal.xaxis.format) {
                return true;
            }
            return false;
        }

        eChart.getDataByDateFormat = function (grpObj, key, _chart) {
            if (_chart._dataFormateVal.xaxis.format == "MMMM") {
                var obj = Object.assign([], grpObj);
                var months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
                function sortObject(obj) {
                    return obj.sort(function (a, b) {
                        return months.indexOf(a.key) - months.indexOf(b.key);
                    });
                }
                return sortObject(obj);
            } else if (_chart._dataFormateVal.xaxis.format == "MMM") {
                var obj = grpObj;
                var months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
                function sortObject(obj) {
                    return obj.sort(function (a, b) {
                        return months.indexOf(a.key) - months.indexOf(b.key);
                    });
                }
                return sortObject(obj);
            } else{
                var obj = grpObj;
                function sortObject(obj) {
                    return obj.sort(function (a, b) {
                        return moment(a.key,_chart._dataFormateVal.xaxis.format) - moment(b.key,_chart._dataFormateVal.xaxis.format);
                    });
                }
                return sortObject(obj);
            }
            return grpObj;
        }

        //-------------Common Tooltip--------------------------------------
        eChart.processTooltipData = function (_chart) {
            _chart._tooltipData = {};
            var grpData;
            if(_chart._group){
                grpData = JSON.parse(JSON.stringify(_chart._group)); // find some solution for that taking some time
            }

            var i = 0;
            if(grpData){
                $.each(grpData, function (key, value) {
                    if (i == 0) {
                        _chart._tooltipData = {};
                        if(value.length)
                        value.forEach(function (d) {
                            _chart._tooltipData[d.key] = d.value;
                        });
                    }
                    i++;
                });
            }
        }

        //--------------------Common Tooltip--------------------------------------
        eChart.processGroupData = function (data) {
            var processedData = Object.create({});
            data.forEach(function (d) {
                processedData[d.key] = d.value;
            });
            return processedData;
        }

        eChart.sortArray = function(obj) {
            var months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
            return obj.sort(function (a, b) {
                return months.indexOf(a) - months.indexOf(b);
            });
        }

        //--------------BAR CHART DRAW CODE-----------------------------------
        eChart.barChart = function (id) {
            var _chart = {};

            _chart.id = id;
            _chart.type = 'barChart';
            _chart._sort = null;
            _chart._colorSelection = $.extend([], eChart.colors);

            _chart.tKey = function (totalKeys) {
                _chart.totalKey = totalKeys;
                return _chart;
            };

            _chart.group = function (serverData) {
                _chart._group = serverData;
                _chart.data();
                return _chart;
            };

            _chart.setExcludeKey = function (excludeKey) {
                _chart._excludeKey = excludeKey;
                return _chart;
            }

            _chart.measures = function(measures){
                var measure = {};
                $.each(measures,function(key,val){
                    measure[key] = val;
                });
                _chart._measures = measure;
                return _chart;
            };

            _chart.dimension = function (dimension) {
                _chart._dimension = dimension;
                return _chart;
            };

            _chart.dataFormat = function (dataFormat) {
                _chart._dataFormateVal = dataFormat;
                _chart.setDataFormat();
                return _chart;
            };

            _chart.dataSetLabels = function (labels) {
                _chart._dataSetLabels = labels;
                return _chart;
            };

            _chart.sort = function (order) {
                _chart._sort = order;
                eChart.registerDataOrder(_chart);
                _chart._metadataId=eChart._metadataId;
                sketchServer._barChartJs(_chart);
            };

            _chart.setDataFormat = function () {
                /* y axis font size
                */
                if (_chart._dataFormateVal.yaxis && _chart._dataFormateVal.yaxis.fontSize) {
                    _chart.yfontSize(_chart._dataFormateVal.yaxis.fontSize);
                } else {
                    _chart.yfontSize(12);
                }
                /*
                 * x axis font size
                 */
                if (_chart._dataFormateVal.xaxis && _chart._dataFormateVal.xaxis.fontSize) {
                    _chart.xfontSize(_chart._dataFormateVal.xaxis.fontSize);
                } else {
                    _chart.xfontSize(12);
                }
                if(_chart._dataFormateVal.sort){
                    _chart._sort=_chart._dataFormateVal.sort;
                    _chart._sortMeasure=_chart._dataFormateVal.sortMeasure;
                }
                if(_chart._dataFormateVal.topN){
                    _chart.topN = _chart._dataFormateVal.topN;
                    _chart.topnMeasure = _chart._dataFormateVal.topnMeasure;
                    _chart.topNOther = _chart._dataFormateVal.topNOther;
                }
                if(_chart._dataFormateVal.runningTotal!=undefined){
                    _chart._runningTotal=_chart._dataFormateVal.runningTotal;
                }
                _chart.groupColor = _chart._dataFormateVal.groupColor;
                _chart._dataType = _chart._dataFormateVal.dataType;
            }

            _chart.xAxes = function () {
                var labelColor, axisLineColor;
                if(_chart._dataFormateVal && _chart._dataFormateVal.axisLabelColor){
                    labelColor = _chart._dataFormateVal.axisLabelColor;
                }else{
                    labelColor = '#666666';
                }
                if(_chart._dataFormateVal && _chart._dataFormateVal.axisLabelColor){
                    axisLineColor = _chart._dataFormateVal.axisColor;
                }else{
                    axisLineColor = '#e6e6e6';
                }

                /*
                 * scale labels
                 */
                var scaleLabels=false;
                if(_chart._dataFormateVal && _chart._dataFormateVal.xaxis && _chart._dataFormateVal.xaxis.axisLabel){
                    scaleLabels=_chart._dataFormateVal.xaxis.axisLabel;
                }
                var dualXaxisLabel=false;
                var dualXaxisLabelVal="";
                var axisLabelHeader=true;
                var allAxisHeader=false;
                var axisHeaderRotate=90;
                var autoSkip=false;
                if(_chart._dataFormateVal && _chart._dataFormateVal.xaxis){
                    /*
                     * X axis name
                     */
                    if(_chart._dataFormateVal.xaxis.dualXaxisLabel){
                        dualXaxisLabel=_chart._dataFormateVal.xaxis.dualXaxisLabel;
                        dualXaxisLabelVal=JSON.parse(_chart._dataFormateVal.xaxis.dualXDimension).reName;
                    }
                    /*
                     * Axis label option
                     */
                    if(_chart._dataFormateVal.xaxis.axisHeader!=undefined){
                        axisLabelHeader=_chart._dataFormateVal.xaxis.axisHeader;
                    }
                    /*
                     * All axis label
                     */
                    if(_chart._dataFormateVal.xaxis.allAxisHeader!=undefined){
                        allAxisHeader=_chart._dataFormateVal.xaxis.allAxisHeader;
                        if(allAxisHeader==true){
                            allAxisHeader=false;
                            autoSkip=false;
                        }else{
                            allAxisHeader=10;
                            autoSkip=true;
                        }
                    }
                    /*
                     * Rotate label
                     */
                    if(_chart._dataFormateVal.xaxis.axisHeaderRotate!=undefined){
                        axisHeaderRotate=_chart._dataFormateVal.xaxis.axisHeaderRotate;
                    }
                }
                /*
                 * Dual x axis flag
                 */
                var dualXAxisFlag=false;
                if(_chart._dataFormateVal && _chart._dataFormateVal.xaxis && _chart._dataFormateVal.xaxis.dualX && _chart._dataFormateVal.xaxis.dualX=='yes'){
                    dualXAxisFlag=true;
                }
                /*
                 *  dimension value
                 */
                var dimensionValue="";
                if(_chart._dimension){
                    dimensionValue=Object.keys(_chart._dimension)[0];
                }
                
                if(/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
                    _chart._fontSize=30;
                }
                var dualXSkipLabel={};
                if ((_chart._dataType == "date" || _chart._fontSize == "datetime") && _chart._dataFormateVal.xAxis == undefined) {
                    return [
                        {
                            id:'xAxis1',
                            type:"category",
                            stacked: true,
                            scaleLabel: {
                                display: scaleLabels,
                                labelString: dimensionValue
                            },
                            ticks: {
                                fontColor: labelColor,
                                display:axisLabelHeader,
                                fontSize: _chart._xfontSize,
                                autoSkip: autoSkip,
                                maxTicksLimit: allAxisHeader,
                                maxRotation: axisHeaderRotate,
                                minRotation: axisHeaderRotate,
                                //maxTicksLimit: 10,
                                fontFamily: "sans-serif",
                                callback:function(label){
                                    label=label.toString();
                                    return label;
                                    //return label.slice(0, 12) +(label.length > 12 ? "..." : "");
                                }
                            },
                            type: 'time',
                            unit: 'day',
                            unitStepSize: 1,
                            time: {
                                displayFormats: {
                                    'day': 'MMM DD'
                                }
                            }
                        },
                        {
                            id:'xAxis2',
                            type:"category",
                            stacked: true,
                            position: 'top',
                            display:dualXAxisFlag,
                            scaleLabel: {
                                display: dualXaxisLabel,
                                labelString: dualXaxisLabelVal
                            },
                            gridLines: {
                                color: axisLineColor,
                                drawOnChartArea: true, // only want the grid lines for one axis to show up
                                //tickMarkLength: 10,
                                offsetGridLines: true
                            },
                            ticks:{
                                fontColor: labelColor,
                                display:axisLabelHeader,
                                fontSize: _chart._xfontSize,
                                maxRotation: 0,
                                minRotation: 0,
                                fontFamily: "sans-serif",
                                callback:function(label,index,labels){
                                    label=label.toString();
                                    if(dualXSkipLabel[_chart._data.dualXLabel[label]]==undefined){
                                        dualXSkipLabel[_chart._data.dualXLabel[label]]=index;
                                    }
                                    if(dualXSkipLabel[_chart._data.dualXLabel[label]]==index){
                                        return _chart._data.dualXLabel[label];
                                    }
                                    return "";
                                }
                            },
                            type: 'time',
                            unit: 'day',
                            unitStepSize: 1,
                            time: {
                                displayFormats: {
                                    'day': 'MMM DD'
                                }
                            }
                        }
                    ];
                } else {
                    return [{
                        id:'xAxis1',
                        type:"category",
                        stacked: true,
                        scaleLabel: {
                            display: scaleLabels,
                            labelString: dimensionValue
                        },
                        gridLines: {
                            color: axisLineColor,
                            display: axisLabelHeader,
                        },
                        ticks:{
                            fontColor: labelColor,
                            // display: false,
                            display:axisLabelHeader,
                            fontSize: _chart._xfontSize,
                            autoSkip: autoSkip,
                            maxRotation: axisHeaderRotate,
                            maxTicksLimit: allAxisHeader,
                            minRotation: axisHeaderRotate,
                            fontFamily: "sans-serif",
                            callback:function(label){
                                label=label.toString();
                                //return label;
                                return label.slice(0, 15) +(label.length > 15 ? "..." : "");
                            }
                        }
                    },
                    {
                        id:'xAxis2',
                        type:"category",
                        display:dualXAxisFlag,
                        stacked: true,
                        position: 'top',
                        scaleLabel: {
                            display: dualXaxisLabel,
                            labelString: dualXaxisLabelVal
                        },
                        gridLines: {
                            color: axisLineColor,
                            display:axisLabelHeader,
                            drawOnChartArea: true, // only want the grid lines for one axis to show up
                            //tickMarkLength: 10,
                            offsetGridLines: true
                        },
                        ticks:{
                            fontColor: labelColor,
                            display:axisLabelHeader,
                            fontSize: _chart._xfontSize,
                            maxRotation: 0,
                            minRotation: 0,
                            fontFamily: "sans-serif",
                            autoSkip: false,
                            callback:function(label,index,labels){
                                label=label.toString();
                                if(dualXSkipLabel[_chart._data.dualXLabel[label]]==undefined){
                                    dualXSkipLabel[_chart._data.dualXLabel[label]]=index;
                                }
                                if(dualXSkipLabel[_chart._data.dualXLabel[label]]==index){
                                    return _chart._data.dualXLabel[label];
                                }
                                return "";
                            }
                        }
                    }];
                }
            };

            _chart.yAxes = function(chart){
                return eChart.renderyAxes(_chart._dataFormateVal, chart);
            }

            _chart.dataType = function (type) {
                _chart._dataType = type;
            };

            _chart.fontFamily = function (fontFamily) {
                _chart._fontFamily = fontFamily;
            }

            _chart.yAxisPrefix = function (yAxis) {
                if (yAxis) {
                    _chart._yAxisPrefix = yAxis;
                } else {
                    _chart._yAxisPrefix = "";
                }
            }

            _chart.xfontSize = function (fontSize) {
                _chart._xfontSize = fontSize;
            };

            _chart.yfontSize = function (fontSize) {
                _chart._yfontSize = fontSize;
            };

            //---------------------------Group Color Start--------------------
            _chart.getDatasetByGroupColor = function (finalData, groupKey,grpIndex,measureLength) {
                var groupMappings = _chart.createGroupMappings(finalData, _chart);
                var initialData = [];
                _chart._labels.forEach(function (d, i) {
                    initialData[i] = null;
                });
                var p = 0;
                var datasets = [];
                _chart.groupColorDataOrder = [];
                $.each(groupMappings, function (key, value) {
                    var data = Object.assign([], initialData);
                    $.each(value, function (k, v) {
                        var labelIndexMatched = _chart._labels.indexOf(k);
                        var grpColorDataOrder = {};
                        var pushFlag = true;
                        grpColorDataOrder['order'] = labelIndexMatched;
                        grpColorDataOrder['key'] = key;
                        grpColorDataOrder['label'] = k;
                        grpColorDataOrder['val'] = v;
                        _chart.groupColorDataOrder.forEach(function(d,i){
                            if(labelIndexMatched == d.order){
                                pushFlag = false;
                            }
                        });
                        if(pushFlag){
                            _chart.groupColorDataOrder.push(grpColorDataOrder);
                        }
                        if (labelIndexMatched != -1) {
                            data[labelIndexMatched] = v;
                        }
                    });
                    /*
                      If single measure
                     */
                    /* if(measureLength==1){*/
                    var dt = {
                        measure:groupKey,
                        label: key,
                        data: data,
                        backgroundColor: Array(data.length).fill(_chart._colorSelection[p % (_chart._colorSelection.length)]),
                        borderWidth: 1
                    };
                    datasets.push(dt);
                    p++;
                });
                return datasets;
            }

            _chart.updateDatasetByGroupColor = function (finalData, groupKey) {
                var groupMappings = _chart.createGroupMappings(finalData, _chart);
                var initialData = [];
                _chart._data.labels.forEach(function (d, i) {
                    initialData[i] = null;
                });
                var labelsList = {};
                $.each(groupMappings, function (key, value) {
                    $.each(value, function (k, v) {
                        if (!(v == 0 || v < 0 || isNaN(v))) {
                            labelsList[k] = 1;
                        }
                    });
                });
                var labelsListOrdered = {};
                Object.keys(labelsList).forEach(function(key) {
                    labelsListOrdered[key] = labelsList[key];
                });
                var indexData = 0;
                var labelsListArray = [];
                $.each(labelsListOrdered, function (key, value) {
                    initialData[indexData] = null;
                    labelsListArray.push(key);
                    indexData++;
                });
                var p = 0;
                var datasets = [];
                $.each(groupMappings, function (key, value) {
                    var data = Object.assign([], initialData);
                    $.each(value, function (k, v) {
                        var labelIndexMatched = _chart._data.labels.indexOf(k);
                        if (labelIndexMatched != -1) {
                            if (!(v <= 0))
                                data[labelIndexMatched] = v;
                        }
                    });
                    var dt = {
                        measure:groupKey,
                        label: key,
                        data: data,
                        backgroundColor: Array(data.length).fill(_chart._colorSelection[p % (_chart._colorSelection.length)]),
                        borderWidth: 1
                    };
                    // _chart._labels = labels
                    datasets.push(dt);
                    p++;
                });
                return datasets;
            }
            //---------------------------Group Color End-----------------------


            _chart.createGroupMappings = function (finalData, _chart) {
                _chart._groupMappings = eChart.createGroupMappings(finalData, _chart);
                return _chart._groupMappings;
            }

            _chart.getDataByGroup = function () {
                var groups = _chart._group;
                var tempArray=[];
                var datasets = [];
                var measureLength=Object.keys(groups).length;
                var i=0;
                var j=0;
                var dualXLabel={};
                $.each(groups, function (key, grpObj) {
                    var finalData=grpObj;
                    if (eChart.isDataFormatApplied(_chart) && !_chart._sort && !_chart.topN) {
                        finalData = eChart.getDataByDateFormat(grpObj, key, _chart);
                    }
                    if(j==0)
                        grpObj.forEach(function (d) {
                            dualXLabel[d.key]=d.dualXKey;
                        });
                    datasets = _chart.getDatasetByGroupColor(finalData, key,i,measureLength);
                    datasets.forEach(function (d) {
                        tempArray.push(d);
                    });
                    j++;
                    i++;
                });
                var dataInFormat = {
                    labels: _chart._labels,
                    dualXLabel:dualXLabel,
                    datasets: tempArray
                };
                return dataInFormat;
            }

            _chart.getNormalData = function () {
                var colorsLength = eChart.colors.length;
                var groups = Object.assign({},_chart._group);

                var labels = [];
                var dualXLabel={};
                var datasets = [];
                var i = 0;
                var j = 0;
                var index = 0;
                /*
                 * Top n and reorder reinitialize
                 */
                _chart._dataSetLabels=[];
                $.each(groups, function (key, grpObj) {
                    var data = [];
                    var backgroundColor = [];
                    var borderColor = [];
                    var hoverBorderColor = [];
                    var finalData;
                    finalData = grpObj;
                    if (eChart.isDataFormatApplied(_chart) && !_chart._sort && !_chart.topN) {
                        finalData = eChart.getDataByDateFormat(grpObj, key, _chart);
                    }
                    if(_chart._runningTotal && _chart._runningTotal[key]){
                        finalData = eChart.getRunTotal(finalData,key,_chart);
                    }
                    _chart._dataSetLabels.push(key);
                    var dualXKeyCheck={};
                    finalData.forEach(function (d) {
                        if (j === 0){
                            labels.push(d.key);
                            /*if(dualXKeyCheck[d.dualXKey]==undefined){
                                dualXKeyCheck[d.dualXKey]=true;*/
                            dualXLabel[d.key]=d.dualXKey;
                            /* }else{
                                 dualXLabel[d.key]="";
                             }*/
                        }
                        if (_chart._operator && d.value!=null && d.value[_chart._operator[key].key]) {
                            data.push(d.value[_chart._operator[key].key]);
                        } else {
                            data.push(d.value);
                        }
                        backgroundColor.push(_chart._colorSelection[j]);
                        hoverBorderColor.push("#000");
                        i++;
                    });
                    var dt = {
                        label: key,
                        data: data,
                        backgroundColor: backgroundColor,
                        borderColor: backgroundColor,
                        borderWidth: 1
                    };
                    datasets.push(dt);
                    j++;
                });
                var dataInFormat = {
                    labels: labels,
                    datasets: datasets,
                    dualXLabel:dualXLabel
                };
                return dataInFormat;
            }

            _chart.getDataInFormat = function (order) {
                eChart.updateAttributes(_chart);
                var dataInFormat = {};
                if (eChart.isGroupColorApplied(_chart)) {
                    dataInFormat = _chart.getDataByGroup();
                } else {
                    dataInFormat = _chart.getNormalData();
                }
                try {
                    if (_chart._dataFormateVal && _chart._dataFormateVal.tooltipSelector) {
                        eChart.processTooltipData(_chart);
                    }
                } catch (e) {

                }
                return dataInFormat;
            }

            _chart.data = function () {
                _chart._data = _chart.getDataInFormat();
                return _chart;
            };

            _chart.render = function () {
                eChart.clearContainer(id);
                var bar = eChart.getCanvasContext(_chart, function (context) {
                    _chart.draw(context);
                    _chart.bindEvents();
                });
                sketchServer.reportLoadedCountCheck(_chart);
            };

            _chart.draw = function (context) {
                var instance = new Chart(context, {
                    type: 'bar',
                    data: _chart._data,
                    options: eChart.createOption(_chart)
                });
                var res = id.split("-");
                _chart.chartInstance = instance;
                eChart.generateLegend("left" + res[1], _chart);
                _chart['tempGroupKey'] = _chart._group;
                eChart.registerChart(_chart);
                sketchServer.globalRegistry.register(_chart);
                sketchServer.globalRegistry.syncLocalRegistries(_chart.id,"eChart");
            };

            _chart.operator = function (operator) {
                _chart._operator = operator;
                return _chart;
            };

            _chart.bindEvents = function () {
                eChart.bindEvents(this);
            };

            _chart.highlightSelectedFilter = function () {

                var labels = this.getLabels();
                var labelsLength = labels.length;
                var colorsLength = eChart.colors.length;
                var chartFiltersLength = 0;
                if (!_chart.reset) {
                    chartFiltersLength = this.filters.length;
                }
                if ((labelsLength == chartFiltersLength  && !eChart.isGroupColorApplied(_chart)) || chartFiltersLength === 0) {
                    _chart._data.datasets.forEach(function (dataset, dsIndex) {
                        labels.forEach(function (filter, i) {
                            try{
                                _chart._data.datasets[dsIndex].backgroundColor[i] = _chart._colorSelection[dsIndex % colorsLength];
                            }catch (e){

                            }
                        });
                    });
                    _chart.filters = [];
                } else {
                    var datasetsArray = [];
                    var datasetWiseFilters = {};
                    _chart.filters.forEach(function (filter) {
                        if (!datasetWiseFilters[filter.datasetIndex])
                            datasetWiseFilters[filter.datasetIndex] = [];
                        datasetWiseFilters[filter.datasetIndex].push(filter.label);
                    });
                    _chart._data.datasets.forEach(function (dataset, dsIndex) {
                        var filterLabelsArray = datasetWiseFilters[dsIndex];
                        if (filterLabelsArray) {
                            labels.forEach(function (label, index) {
                                if (filterLabelsArray.indexOf(label) == -1) {
                                    dataset.backgroundColor[index] = "#ddd";
                                } else {
                                    dataset.backgroundColor[index] = _chart._colorSelection[dsIndex % colorsLength];
                                }
                            });
                        } else {
                            dataset.backgroundColor = Array(labels.length).fill("#ddd");
                        }
                    });
                }
            };

            _chart.colorChange = function (selectedDataSet, color) {
                var labels = this.getLabels();
                _chart._data.datasets.forEach(function (dataset, dsIndex) {
                    if (dataset.label == selectedDataSet.datasetLabel) {
                        _chart._colorSelection[dsIndex] = color;
                        var tempData = [];
                        labels.forEach(function (filter, i) {
                            tempData.push(color);
                        });
                        _chart._data.datasets[dsIndex].backgroundColor = tempData;
                        _chart._data.datasets[dsIndex].borderColor = tempData;
                    }
                });
                setTimeout(function () {
                    _chart.chartInstance.update();
                    eChart.registerColor(_chart);
                    var res = _chart.id.split("-");
                    eChart.generateLegend("left" + res[1], _chart);
                }, 1);
            };

            _chart.getClickedElementLabel = function (evt) {
                var filter = {};
                try {
                    var eveObj=this.chartInstance.getElementAtEvent(evt)[0];
                    filter.label = eveObj._model.label;
                    filter.datasetLabel = eveObj._model.datasetLabel;
                    filter.datasetIndex = eveObj._datasetIndex;
                    if(eveObj._chart.config.data.datasets[eveObj._datasetIndex] && eveObj._chart.config.data.datasets[eveObj._datasetIndex].measure);
                    filter.measure = eveObj._chart.config.data.datasets[eveObj._datasetIndex].measure;
                    return filter;
                } catch (e) {
                    return null;
                }
            };

            _chart.getLabels = function () {
                return this.chartInstance.config.data.labels;
            };
            return _chart;
        };
        // -------------------------------------BAR CHART END---------------------------------------------------------------








        // -------------ROW CHART DRAW CODE-----------------------------------
        eChart.rowChart = function (id) {
            var _chart = {};
            _chart.id = id;
            _chart.type = 'horizontalBar';
            _chart._sort = null;

            _chart._colorSelection = $.extend([], eChart.colors);

            _chart.tKey = function (totalKeys) {
                _chart.totalKey = totalKeys;
                return _chart;
            };

            _chart.group = function (serverData) {
                _chart._group = serverData;
                _chart.data();
                return _chart;
            };

            _chart.setExcludeKey = function (excludeKey) {
                _chart._excludeKey = excludeKey;
                return _chart;
            }

            _chart.measures = function(measures){
                var measure={};
                $.each(measures,function(key,val){
                    measure[key]=val;
                });
                _chart._measures = measure;
                return _chart;
            };

            _chart.dimension = function (dimension) {
                _chart._dimension = dimension;
                return _chart;
            };

            _chart.dataFormat = function (dataFormat) {
                _chart._dataFormateVal = dataFormat;
                _chart.setDataFormat();
                return _chart;
            };

            _chart.dataSetLabels = function (labels) {
                _chart._dataSetLabels = labels;
                return _chart;
            };

            _chart.sort = function (order) {
                _chart._sort = order;
                eChart.registerDataOrder(_chart);
                _chart._metadataId=eChart._metadataId;
                sketchServer._rowChartJs(_chart);
            };

            _chart.xAxes = function () {
                var labelColor, axisLineColor;
                if(_chart._dataFormateVal && _chart._dataFormateVal.axisLabelColor){
                    labelColor = _chart._dataFormateVal.axisLabelColor;
                }else{
                    labelColor = '#666666';
                }
                if(_chart._dataFormateVal && _chart._dataFormateVal.axisLabelColor){
                    axisLineColor = _chart._dataFormateVal.axisColor;
                }else{
                    axisLineColor = '#e6e6e6';
                }
                /*
                 * scale labels
                 */
                var scaleLabels=false;
                var axisLabelHeader=true;
                if(_chart._dataFormateVal && _chart._dataFormateVal.xaxis){
                    if(_chart._dataFormateVal.xaxis.axisLabel){
                        scaleLabels=_chart._dataFormateVal.xaxis.axisLabel;
                    }
                    if(_chart._dataFormateVal.xaxis.axisHeader!=undefined){
                        axisLabelHeader=_chart._dataFormateVal.xaxis.axisHeader;
                    }
                }
                /*
                 *  dimension value
                 */
                var measuresValue="";
                if(_chart._measures){
                    if(Object.keys(_chart._measures).length>1){
                        measuresValue="Values";
                    }else{
                        measuresValue=Object.keys(_chart._measures)[0];
                    }
                }
                if(/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
                    _chart._xfontSize=30;
                }
                if ((_chart._dataType == "date" || _chart._dataType == "datetime") && _chart._dataFormateVal.xaxis == undefined) {
                    return [{
                        stacked: true,
                        scaleLabel: {
                            display: scaleLabels,
                            labelString: measuresValue
                        },
                        gridLines: {
                            color: axisLineColor,
                            display: axisLabelHeader,
                        },
                        ticks: {
                            fontColor: labelColor,
                            display: axisLabelHeader,
                            fontSize: _chart._xfontSize,
                            autoSkip: true,
                            maxTicksLimit: 10,
                            fontFamily: "sans-serif",
                            userCallback: function (value, index, values) {
                                // Convert the number to a string and splite the string every 3 charaters from the end
                                // Convert the array to a string and format the output
                                //value = value.join('.');
                                if (_chart._dataFormateVal && _chart._dataFormateVal.yaxis) {
                                    var yAxisObj=_chart._dataFormateVal.yaxis;
                                    if(yAxisObj.decimal){
                                        value = value.toFixed(_chart._dataFormateVal.yaxis.decimal);
                                    }
                                    if(yAxisObj.units=="yes"){
                                        value = eChart.formatNumber(value)
                                    }
                                    if(yAxisObj.separator){
                                        value = value.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
                                    }
                                    if(yAxisObj.prefix){
                                        value = yAxisObj.prefix+" "+value;
                                    }
                                    if(yAxisObj.suffix){
                                        value = value+" "+yAxisObj.suffix;
                                    }
                                    return value;
                                } else {
                                    return eChart.formatNumber(value);
                                }
                            }
                        },
                        type: 'time',
                        unit: 'day',
                        unitStepSize: 1,
                        time: {
                            displayFormats: {
                                'day': 'MMM DD'
                            }
                        }
                    }];
                } else {
                    return [{
                        stacked: true,
                        scaleLabel: {
                            display: scaleLabels,
                            labelString: measuresValue
                        },
                        gridLines: {
                            color: axisLineColor,
                            display: axisLabelHeader,
                        },
                        ticks: {
                            fontColor: labelColor,
                            display: axisLabelHeader,
                            fontSize: _chart._xfontSize,
                            autoSkip: true,
                            maxTicksLimit: 10,
                            fontFamily: "sans-serif",
                            userCallback: function (value, index, values) {
                                // Convert the number to a string and splite the string every 3 charaters from the end
                                // Convert the array to a string and format the output
                                //value = value.join('.');
                                if (_chart._dataFormateVal && _chart._dataFormateVal.yaxis) {
                                    var yAxisObj=_chart._dataFormateVal.yaxis;
                                    if(yAxisObj.decimal){
                                        value = value.toFixed(_chart._dataFormateVal.yaxis.decimal);
                                    }
                                    if(yAxisObj.units=="yes"){
                                        value = eChart.formatNumber(value)
                                    }
                                    if(yAxisObj.separator){
                                        value = value.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
                                    }
                                    if(yAxisObj.prefix){
                                        value = yAxisObj.prefix+" "+value;
                                    }
                                    if(yAxisObj.suffix){
                                        value = value+" "+yAxisObj.suffix;
                                    }
                                    return value;
                                } else {
                                    return eChart.formatNumber(value);
                                }
                            }
                        }
                    }];
                }
            };

            _chart.yAxes = function(chart){
                return eChart.renderyAxes(_chart._dataFormateVal, chart);
            }

            _chart.dataType = function (type) {
                _chart._dataType = type;
            };

            _chart.fontFamily = function (fontFamily) {
                _chart._fontFamily = fontFamily;
            }

            _chart.yAxisPrefix = function (yAxis) {
                if (yAxis) {
                    _chart._yAxisPrefix = yAxis;
                } else {
                    _chart._yAxisPrefix = "";
                }
            }

            _chart.setDataFormat = function () {
                /* y axis font size
                */
                if (_chart._dataFormateVal.yaxis && _chart._dataFormateVal.yaxis.fontSize) {
                    _chart.yfontSize(_chart._dataFormateVal.yaxis.fontSize);
                } else {
                    _chart.yfontSize(12);
                }
                /*
                 * x axis font size
                 */
                if (_chart._dataFormateVal.xaxis && _chart._dataFormateVal.xaxis.fontSize) {
                    _chart.xfontSize(_chart._dataFormateVal.xaxis.fontSize);
                } else {
                    _chart.xfontSize(12);
                }
                if(_chart._dataFormateVal.sort){
                    _chart._sort=_chart._dataFormateVal.sort;
                    _chart._sortMeasure=_chart._dataFormateVal.sortMeasure;
                }
                if(_chart._dataFormateVal.topN){
                    _chart.topN = _chart._dataFormateVal.topN;
                    _chart.topnMeasure = _chart._dataFormateVal.topnMeasure;
                    _chart.topNOther = _chart._dataFormateVal.topNOther;
                }
                if(_chart._dataFormateVal.runningTotal!=undefined){
                    _chart._runningTotal=_chart._dataFormateVal.runningTotal;
                }
                _chart.groupColor = _chart._dataFormateVal.groupColor;
                _chart._dataType = _chart._dataFormateVal.dataType;
            }

            _chart.xfontSize = function (fontSize) {
                _chart._xfontSize = fontSize;
            };

            _chart.yfontSize = function (fontSize) {
                _chart._yfontSize = fontSize;
            };

            //---------------------------Group Color Start--------------------
            _chart.getDatasetByGroupColor = function (finalData, groupKey,grpIndex,measureLength) {
                var groupMappings = _chart.createGroupMappings(finalData, _chart);
                var initialData = [];
                _chart._labels.forEach(function (d, i) {
                    initialData[i] = null;
                });
                var p = 0;
                var datasets = [];
                _chart.groupColorDataOrder = [];
                $.each(groupMappings, function (key, value) {
                    var data = Object.assign([], initialData);
                    $.each(value, function (k, v) {
                        var labelIndexMatched = _chart._labels.indexOf(k);

                        var grpColorDataOrder = {};
                        var pushFlag = true;
                        grpColorDataOrder['order'] = labelIndexMatched;
                        grpColorDataOrder['key'] = key;
                        grpColorDataOrder['label'] = k;
                        grpColorDataOrder['val'] = v;
                        _chart.groupColorDataOrder.forEach(function(d,i){
                            if(labelIndexMatched == d.order){
                                pushFlag = false;
                            }
                        });
                        if(pushFlag){
                            _chart.groupColorDataOrder.push(grpColorDataOrder);
                        }
                        if (labelIndexMatched != -1) {
                            data[labelIndexMatched] = v;
                        }
                    });
                    /*
                      If single measure
                     */
                    /* if(measureLength==1){*/
                    var dt = {
                        measure:groupKey,
                        label: key,
                        data: data,
                        backgroundColor: Array(data.length).fill(_chart._colorSelection[p % (_chart._colorSelection.length)]),
                        borderWidth: 1
                    };
                    datasets.push(dt);
                    p++;
                });
                return datasets;
            }

            _chart.updateDatasetByGroupColor = function (finalData, groupKey) {
                var groupMappings = _chart.createGroupMappings(finalData, _chart);
                var initialData = [];
                _chart._data.labels.forEach(function (d, i) {
                    initialData[i] = null;
                });
                var labelsList = {};
                $.each(groupMappings, function (key, value) {
                    $.each(value, function (k, v) {
                        if (!(v == 0 || v < 0 || isNaN(v))) {
                            labelsList[k] = 1;
                        }
                    });
                });
                var labelsListOrdered = {};
                Object.keys(labelsList).forEach(function(key) {
                    labelsListOrdered[key] = labelsList[key];
                });
                var indexData = 0;
                var labelsListArray = [];
                $.each(labelsListOrdered, function (key, value) {
                    initialData[indexData] = null;
                    labelsListArray.push(key);
                    indexData++;
                });
                var p = 0;
                var datasets = [];
                $.each(groupMappings, function (key, value) {
                    var data = Object.assign([], initialData);
                    $.each(value, function (k, v) {
                        var labelIndexMatched = _chart._data.labels.indexOf(k);
                        if (labelIndexMatched != -1) {
                            if (!(v <= 0))
                                data[labelIndexMatched] = v;
                        }
                    });
                    var dt = {
                        measure:groupKey,
                        label: key,
                        data: data,
                        backgroundColor: Array(data.length).fill(_chart._colorSelection[p % (_chart._colorSelection.length)]),
                        borderWidth: 1
                    };
                    // _chart._labels = labels
                    datasets.push(dt);
                    p++;
                });
                return datasets;
            }
            //---------------------------Group Color End-----------------------



            _chart.createGroupMappings = function (finalData, _chart) {
                _chart._groupMappings = eChart.createGroupMappings(finalData, _chart);
                return _chart._groupMappings;
            }

            _chart.getDataByGroup = function () {
                var groups = _chart._group;
                var tempArray=[];
                var datasets = [];
                var measureLength=Object.keys(groups).length;
                var i=0;
                var dualXLabel={};
                $.each(groups, function (key, grpObj) {
                    var finalData=grpObj;
                    if (eChart.isDataFormatApplied(_chart) && !_chart._sort && !_chart.topN) {
                        finalData = eChart.getDataByDateFormat(grpObj, key, _chart);
                    }
                    if(i==0)
                        grpObj.forEach(function (d) {
                            dualXLabel[d.key]=d.dualXKey;
                        });
                    datasets = _chart.getDatasetByGroupColor(finalData, key,i,measureLength);
                    datasets.forEach(function (d) {
                        tempArray.push(d);
                    });
                    i++;
                });
                var dataInFormat = {
                    labels: _chart._labels,
                    datasets: tempArray,
                    dualXLabel:dualXLabel
                };
                return dataInFormat;
            }

            _chart.getNormalData = function () {
                var colorsLength = eChart.colors.length;
                var groups = Object.assign({},_chart._group);

                /*
                 * Diension sum check
                 */
                var groupKeys=Object.keys(groups);
                var swappingVar=groupKeys[0];
                var sortingDimension=false;
                /*
                 * End Diension sum check
                 */
                var labels = [];
                var datasets = [];
                var i = 0;
                var j = 0;
                var index = 0;
                /*
                 * Top n and reorder reinitialize
                 */
                _chart._dataSetLabels=[];
                var dualXLabel={};
                $.each(groups, function (key, grpObj) {
                    var data = [];
                    var backgroundColor = [];
                    var borderColor = [];
                    var hoverBorderColor = [];
                    var finalData;
                    finalData = grpObj;
                    if (eChart.isDataFormatApplied(_chart) && !_chart._sort && !_chart.topN) {
                        finalData = eChart.getDataByDateFormat(grpObj, key, _chart);
                    }
                    if(_chart._runningTotal && _chart._runningTotal[key]){
                        finalData = eChart.getRunTotal(finalData,key,_chart);
                    }
                    _chart._dataSetLabels.push(key);
                    finalData.forEach(function (d) {
                        if (j === 0){
                            dualXLabel[d.key]=d.dualXKey;
                            labels.push(d.key);
                        }
                        if (_chart._operator && d.value!=null && d.value[_chart._operator[key].key]) {
                            data.push(d.value[_chart._operator[key].key]);
                        } else {
                            data.push(d.value);
                        }
                        backgroundColor.push(_chart._colorSelection[j]);
                        hoverBorderColor.push("#000");
                        i++;
                    });
                    var dt = {
                        label: key,
                        data: data,
                        backgroundColor: backgroundColor,
                        borderColor: backgroundColor,
                        borderWidth: 1
                    };
                    datasets.push(dt);
                    j++;
                });
                var dataInFormat = {
                    labels: labels,
                    datasets: datasets,
                    dualXLabel:dualXLabel
                };
                return dataInFormat;
            }

            _chart.getDataInFormat = function (order) {
                eChart.updateAttributes(_chart);
                var dataInFormat = {};
                if (eChart.isGroupColorApplied(_chart)) {
                    dataInFormat = _chart.getDataByGroup();
                } else {
                    dataInFormat = _chart.getNormalData();
                }
                try {
                    if (_chart._dataFormateVal && _chart._dataFormateVal.tooltipSelector) {
                        eChart.processTooltipData(_chart);
                    }
                } catch (e) {

                }
                return dataInFormat;
            }

            _chart.data = function () {
                _chart._data = _chart.getDataInFormat();
                return _chart;
            };

            _chart.render = function () {
                eChart.clearContainer(id);
                var bar = eChart.getCanvasContext(_chart, function (context) {
                    _chart.draw(context);
                    _chart.bindEvents();
                });
                sketchServer.reportLoadedCountCheck(_chart);
            };

            _chart.draw = function (context) {
                var instance = new Chart(context, {
                    type: 'horizontalBar',
                    data: _chart._data,
                    options: eChart.createOption(_chart)
                });
                var res = id.split("-");
                _chart.chartInstance = instance;
                eChart.generateLegend("left" + res[1], _chart);
                _chart['tempGroupKey'] = _chart._group;
                eChart.registerChart(_chart);
                sketchServer.globalRegistry.register(_chart);
                sketchServer.globalRegistry.syncLocalRegistries(_chart.id,"eChart");
            };

            _chart.operator = function (operator) {
                _chart._operator = operator;
                return _chart;
            };

            _chart.bindEvents = function () {
                eChart.bindEvents(this);
            };

            _chart.highlightSelectedFilter = function () {
                var labels = this.getLabels();
                var labelsLength = labels.length;
                var colorsLength = eChart.colors.length;
                var chartFiltersLength = 0;
                if (!_chart.reset) {
                    chartFiltersLength = this.filters.length;
                }
                if ((labelsLength == chartFiltersLength  && !eChart.isGroupColorApplied(_chart)) || chartFiltersLength === 0) {
                    _chart._data.datasets.forEach(function (dataset, dsIndex) {
                        labels.forEach(function (filter, i) {
                            try{
                                _chart._data.datasets[dsIndex].backgroundColor[i] = _chart._colorSelection[dsIndex % colorsLength];
                            }catch (e){

                            }
                        });
                    });
                    _chart.filters = [];
                } else {
                    var datasetsArray = [];
                    var datasetWiseFilters = {};
                    _chart.filters.forEach(function (filter) {
                        if (!datasetWiseFilters[filter.datasetIndex])
                            datasetWiseFilters[filter.datasetIndex] = [];
                        datasetWiseFilters[filter.datasetIndex].push(filter.label);
                    });
                    _chart._data.datasets.forEach(function (dataset, dsIndex) {
                        var filterLabelsArray = datasetWiseFilters[dsIndex];
                        if (filterLabelsArray) {
                            labels.forEach(function (label, index) {
                                if (filterLabelsArray.indexOf(label) == -1) {
                                    dataset.backgroundColor[index] = "#ddd";
                                } else {
                                    dataset.backgroundColor[index] = _chart._colorSelection[dsIndex % colorsLength];
                                }
                            });
                        } else {
                            dataset.backgroundColor = Array(labels.length).fill("#ddd");
                        }
                    });
                }
            };

            _chart.colorChange = function (selectedDataSet, color) {
                var labels = this.getLabels();
                _chart._data.datasets.forEach(function (dataset, dsIndex) {
                    if (dataset.label == selectedDataSet.datasetLabel) {
                        _chart._colorSelection[dsIndex] = color;
                        var tempData = [];
                        labels.forEach(function (filter, i) {
                            tempData.push(color);
                        });
                        _chart._data.datasets[dsIndex].backgroundColor = tempData;
                        _chart._data.datasets[dsIndex].borderColor = tempData;
                    }
                });
                setTimeout(function () {
                    _chart.chartInstance.update();
                    eChart.registerColor(_chart);
                    var res = _chart.id.split("-");
                    eChart.generateLegend("left" + res[1], _chart);
                }, 1);
            };

            _chart.getClickedElementLabel = function (evt) {
                var filter = {};
                try {
                    var eveObj=this.chartInstance.getElementAtEvent(evt)[0];
                    filter.label = eveObj._model.label;
                    filter.datasetLabel = eveObj._model.datasetLabel;
                    filter.datasetIndex = eveObj._datasetIndex;
                    if(eveObj._chart.config.data.datasets[eveObj._datasetIndex] && eveObj._chart.config.data.datasets[eveObj._datasetIndex].measure);
                    filter.measure = eveObj._chart.config.data.datasets[eveObj._datasetIndex].measure;
                    return filter;
                } catch (e) {
                    return null;
                }
            };

            _chart.getLabels = function () {
                return this.chartInstance.config.data.labels;
            };
            return _chart;
        };
        // -------------------------------------Row CHART END---------------------------------------------------------------





        // -------------------------------------Area CHART DRAW CODE--------------------------------------------------------

        eChart.areaChart = function (id) {
            var _chart = {};
            _chart.id = id;
            _chart.type = 'areaChart';
            _chart.dataBackup = [];

            //Color section changes
            _chart._colorSelection = $.extend([], eChart.colors);
            //End

            _chart.tKey = function (totalKeys) {
                _chart.totalKey = totalKeys;
                return _chart;
            };

            _chart.group = function (group) {
                _chart._group = group;
                _chart.data();
                return _chart;
            };

            _chart.dimension = function (dimension) {
                _chart._dimension = dimension;
                return _chart;
            };

            _chart.dataFormat = function (dataFormat) {
                _chart._dataFormateVal = dataFormat;
                _chart.setDataFormat();
                return _chart;
            };

            _chart.measures=function(measures){
                var measure={};
                $.each(measures,function(key,val){
                    measure[key]=val;
                });
                _chart._measures = measure;
                return _chart;
            };

            _chart.setDataFormat = function () {
                /* y axis font size
                */
                if (_chart._dataFormateVal.yaxis && _chart._dataFormateVal.yaxis.fontSize) {
                    _chart.yfontSize(_chart._dataFormateVal.yaxis.fontSize);
                } else {
                    _chart.yfontSize(12);
                }
                /*
                 * x axis font size
                 */
                if (_chart._dataFormateVal.xaxis && _chart._dataFormateVal.xaxis.fontSize) {
                    _chart.xfontSize(_chart._dataFormateVal.xaxis.fontSize);
                } else {
                    _chart.xfontSize(12);
                }
                if(_chart._dataFormateVal.topN){
                    _chart.topN = _chart._dataFormateVal.topN;
                    _chart.topnMeasure = _chart._dataFormateVal.topnMeasure;
                    _chart.topNOther = _chart._dataFormateVal.topNOther;
                }
                if(_chart._dataFormateVal.sort){
                    _chart._sort=_chart._dataFormateVal.sort;
                    _chart._sortMeasure=_chart._dataFormateVal.sortMeasure;
                }
                if(_chart._dataFormateVal.runningTotal!=undefined){
                    _chart._runningTotal=_chart._dataFormateVal.runningTotal;
                }
                _chart.groupColor = _chart._dataFormateVal.groupColor;
                _chart._dataType = _chart._dataFormateVal.dataType;
            }

            _chart.xfontSize = function (fontSize) {
                _chart._xfontSize = fontSize;
            };

            _chart.yfontSize = function (fontSize) {
                _chart._yfontSize = fontSize;
            };

            _chart.operator = function (operator) {
                _chart._operator = operator;
                return _chart;
            };

            _chart.data = function () {
                _chart._data = _chart.getDataInFormat();
                return _chart;
            };

            _chart.render = function () {
                eChart.clearContainer(id);
                var bar = eChart.getCanvasContext(_chart, function (context) {
                    _chart.draw(context);
                    _chart.bindEvents();
                });
                sketchServer.reportLoadedCountCheck(_chart);
                return true;
            };

            _chart.sort = function (order) {
                _chart._sort = order;
                eChart.registerDataOrder(_chart);
                _chart._metadataId=eChart._metadataId;
                sketchServer._areaChartJs(_chart);
            };


            //Style Setting
            _chart.styleSetting = function () {
                var ticks = {};
                ticks['fontSize'] = 20;
            }

            //Group Mapping...
            _chart.createGroupMappings = function (finalData, _chart) {
                _chart._groupMappings = eChart.createGroupMappings(finalData, _chart);
                return _chart._groupMappings;
            }

            //M -Start

            //---------------------------Group Color Start--------------------
            _chart.getDatasetByGroupColor = function (finalData, groupKey,grpIndex,measureLength) {
                var groupMappings = _chart.createGroupMappings(finalData, _chart);
                var initialData = [];
                _chart._labels.forEach(function (d, i) {
                    initialData[i] = null;
                });
                var p = 0;
                var datasets = [];
                _chart.groupColorDataOrder = [];
                $.each(groupMappings, function (key, value) {
                    var data = Object.assign([], initialData);
                    var pointRadius = Array(data.length).fill(2);
                    var pointBorderColor = Array(data.length).fill(_chart._colorSelection[p % (_chart._colorSelection.length)]);
                    var sumAdder=0;
                    $.each(value, function (k, v) {
                        var labelIndexMatched = _chart._labels.indexOf(k);
                        var grpColorDataOrder = {};
                        var pushFlag = true;
                        grpColorDataOrder['order'] = labelIndexMatched;
                        grpColorDataOrder['key'] = key;
                        grpColorDataOrder['label'] = k;
                        grpColorDataOrder['val'] = v;
                        _chart.groupColorDataOrder.forEach(function(d,i){
                            if(labelIndexMatched == d.order){
                                pushFlag = false;
                            }
                        });
                        if(pushFlag){
                            _chart.groupColorDataOrder.push(grpColorDataOrder);
                        }
                        if (labelIndexMatched != -1) {
                            if(_chart._operator[Object.keys(_chart._operator)[0]].key=="runTotal"){
                                sumAdder+=v;
                                if (!(v <= 0))
                                    data[labelIndexMatched] = sumAdder;
                            }else{
                                if (!(v <= 0))
                                    data[labelIndexMatched] += v;
                            }
                        }
                    });
                    var dt = {
                        measure:groupKey,
                        label: key,
                        data: data,
                        backgroundColor: _chart._colorSelection[p % (_chart._colorSelection.length)],
                        borderColor: _chart._colorSelection[p % (_chart._colorSelection.length)],
                        showLine: true,
                        fill: true,
                        borderWidth: 2,
                        lineTension: 0,
                        pointBorderColor: pointBorderColor,
                        pointRadius: 2
                    };
                    // _chart._labels=labels
                    datasets.push(dt);
                    p++;
                });
                return datasets;
            }

            _chart.updateDatasetByGroupColor = function (finalData, groupKey) {
                var groupMappings = _chart.createGroupMappings(finalData, _chart);
                var labelsList = {};
                $.each(groupMappings, function (key, value) {
                    $.each(value, function (k, v) {
                        if (!(v == 0 || v < 0 || isNaN(v))) {
                            labelsList[k] = 1;
                        }
                    });
                });
                var labelsListOrdered = {};
                Object.keys(labelsList).forEach(function(key) {
                    labelsListOrdered[key] = labelsList[key];
                });
                var initialData = [];
                _chart._data.labels.forEach(function (d, i) {
                    initialData[i] = null;
                });
                var initialData = [];
                var indexData = 0;
                var labelsListArray = [];
                $.each(labelsListOrdered, function (key, value) {
                    initialData[indexData] = null;
                    labelsListArray.push(key);
                    indexData++;
                });
                var p = 0;
                var datasets = [];
                var i=1;
                $.each(groupMappings, function (key, value) {
                    var data = Object.assign([], initialData);
                    var sumAdder=0;
                    $.each(value, function (k, v) {
                        var labelIndexMatched = _chart._data.labels.indexOf(k);
                        if (labelIndexMatched != -1) {
                            if(_chart._operator[Object.keys(_chart._operator)[0]].key=="runTotal"){
                                sumAdder+=v;
                                if (!(v <= 0))
                                    data[labelIndexMatched] = sumAdder;
                            }else{
                                if (!(v <= 0))
                                    data[labelIndexMatched] += v;
                            }
                        }
                    });
                    var pointBorder = "#ccc";
                    var dt = {
                        measure:groupKey,
                        label: key,
                        data: data,
                        backgroundColor: _chart._colorSelection[p % (_chart._colorSelection.length)],
                        borderColor: _chart._colorSelection[p % (_chart._colorSelection.length)],
                        showLine: true,
                        fill: true,
                        borderWidth: 2,
                        lineTension: 0,
                        /*
                          Change line chart for higlight filter not coming
                         */
                        //pointBorderColor: _chart._data.datasets[p].pointBorderColor,
                        //    pointRadius: _chart._data.datasets[p].pointRadius
                    };
                    //     _chart._labels=labels
                    datasets.push(dt);
                    p++;
                    i++;
                });
                return datasets;
            }
            //---------------------------Group Color End-----------------------
            //M-End
            _chart.colorChange = function (selectedDataSet, color) {
                var labels = this.getLabels();
                var labelsLength = labels.length;
                var colorsLength = eChart.colors.length;
                var chartFiltersLength = 0;
                _chart._data.datasets.forEach(function (dataset, dsIndex) {
                    if (dataset.label == selectedDataSet.datasetLabel) {
                        _chart._colorSelection[dsIndex] = color;
                        _chart._data.datasets[dsIndex].backgroundColor = color;
                        _chart._data.datasets[dsIndex].borderColor = color;
                        _chart._data.datasets[dsIndex].pointBorderColor = Array(_chart._data.datasets[dsIndex].data.length).fill(color);
                    }
                });
                setTimeout(function () {
                    eChart.registerColor(_chart);
                    _chart.chartInstance.update();
                    var res = _chart.id.split("-");
                    eChart.generateLegend("left" + res[1], _chart);
                }, 1);
            };

            _chart.xAxes = function () {
                return eChart.renderxAxes(_chart._dataType, _chart._xfontSize, _chart._dataFormateVal,_chart);
            };

            _chart.yAxes=function(chart){
                return eChart.renderyAxes(_chart._dataFormateVal, chart);
            }

            _chart.dataType = function (type) {
                _chart._dataType = type;
            };

            _chart.fontFamily = function (fontFamily) {
                _chart._fontFamily = fontFamily;
            }

            _chart.yAxisPrefix = function (yAxis) {
                if (yAxis) {
                    _chart._yAxisPrefix = yAxis;
                } else {
                    _chart._yAxisPrefix = "";
                }
            }

            _chart.getDatasetByMeasure = function (finalData) {
                var datasets = [];
                var backgroundColorIndex = 0;
                var GroupColor = {};
                var backgroundColor = [];
                finalData.forEach(function (d) {
                    // if (j === 0)
                    // labels.push(d.key);
                    if (_chart._operator) {
                        data.push(d.value[_chart._operator[key].key]);
                    } else {
                        data.push(d.value);
                    }
                    if (d.value.groupColor != undefined) {
                        if (GroupColor[d.value.groupColor]) {
                            backgroundColor.push(GroupColor[d.value.groupColor]);
                        } else {
                            backgroundColor.push(_chart._colorSelection[backgroundColorIndex % (_chart._colorSelection.length)]);
                            GroupColor[d.value.groupColor] = _chart._colorSelection[backgroundColorIndex % (_chart._colorSelection.length)];
                            backgroundColorIndex++;
                        }
                    } else {
                        backgroundColor.push(_chart._colorSelection[j % (_chart._colorSelection.length)]);
                    }
                    //backgroundColor.push(eChart.colors[j]);
                    i++;
                });

                var dt = {
                    label: _chart._dataSetLabels[j],
                    data: data,
                    backgroundColor: backgroundColor,
                    borderColor: backgroundColor,
                    borderWidth: 1,
                    pointBorderColor: backgroundColor,
                    pointRadius: 2,
                };
                datasets.push(dt);
                return datasets;
            }

            _chart.draw = function (context) {
                var referenceLineData = eChart.getReferenceLineData(_chart);
                var annotationData=[];
                referenceLineData.forEach(function (d,index) {
                    var obj={
                        drawTime: "afterDatasetsDraw",
                        id: "hline"+index,
                        type: "line",
                        mode: "horizontal",
                        scaleID: "y-axis-0",
                        value: d.point,
                        borderColor: "black",
                        borderWidth: 2,
                        borderDash: [10, 10],
                        onMouseover:function(e){
                            /*var element = this;
                            element.options.borderWidth = 2;
                            element.options.label.enabled = true;
                            element.options.label.content = d.tooltip;
                            element.chartInstance.chart.canvas.style.cursor = 'pointer';
                            element.chartInstance.update();*/
                            $("#refrenceLineTooltip").html(d.tooltip);
                            $("#refrenceLineTooltip").show();
                            $('#refrenceLineTooltip').css({'top' : e.clientY,'opacity':1,'left':e.clientX});
                        },
                        onMouseleave: function(e) {
                            /*var element = this;
                            element.options.borderWidth = 2;
                            element.options.label.enabled = false;
                            element.chartInstance.chart.canvas.style.cursor = 'pointer';
                            element.chartInstance.update();*/
                            $("#refrenceLineTooltip").hide();
                            $('#refrenceLineTooltip').css({'opacity':0});
                        },
                    }
                    annotationData.push(obj);
                });

                var responsiveData = true;
                if(_chart._dataFormateVal && _chart._dataFormateVal.canvas && _chart._dataFormateVal.canvas.width && _chart._dataFormateVal.canvas.height){
                    responsiveData = false;
                }

                var instance = new Chart(context, {
                    type: 'line',
                    data: _chart._data,
                    options: {
                        responsive: responsiveData,
                        maintainAspectRatio: false,
                        animation:{
                            duration: 0
                        },
                        hover:{
                            animationDuration: 0
                        },
                        responsiveAnimationDuration: 0,
                        legend:{
                            display: false
                        },
                        elements: {
                            line: {
                                tension: 0, // disables bezier curves
                            },
                            point: {
                                backgroundColor: "#999",
                            }
                        },
                        layout: {
                            padding: {
                                right: 10
                            }
                        },
                        bezierCurve: false,
                        legend: {
                            display: false,
                            /*labels: {
                                filter: function(item, chart) {
                                    // Logic to remove a particular legend item goes here
                                    return !item.text.includes('Legend 1');
                                }
                            }*/
                        },
                        legendCallback: function (chartInstance) {
                            if(eChart.legendChartObj[_chart.id]!=undefined){
                                chartInstance.legend.legendItems=eChart.legendChartObj[_chart.id];
                            }
                            var html='';
                            if(_chart.groupColor)
                                html +="<li title="+JSON.parse(_chart.groupColor).reName+"><p>&nbsp;&nbsp;"+JSON.parse(_chart.groupColor).reName.slice(0, 16) + (JSON.parse(_chart.groupColor).reName.length > 16 ? "..." : "") +"</p></li>";
                            html += "<ul>";

                            var chart=_chart;
                            if (chart._groupColor) {
                                $.each(chart._groupColor, function (key, value) {
                                    html += '<li class=""><span style="background-color:' + value + '"></span>' + key + '</li>';
                                });
                            } else {
                                var legends=[];
                                var legendsObj={};
                                /*
                                   Duplicate legend remove
                                 */
                                chartInstance.legend.legendItems.forEach(function (d) {
                                    if(legendsObj[d.text]==undefined){
                                        legendsObj[d.text]=true;
                                        legends.push(d);
                                    }
                                });
                                legends.forEach(function (d) {
                                    if(d.hidden==false){
                                        var title=d.text.replace(/ /g,"&nbsp;");
                                        html += '<li class="" title='+title+'><span style="background-color:' + d.fillStyle + '"></span>' + d.text.slice(0, 12) + (d.text.length > 12 ? "..." : "") + '</li>';
                                    }
                                    //strokeStyle
                                });
                            }
                            html += "</ul>"
                            return html;
                        },
                        tooltips: {
                            enabled: true,
                            callbacks: {
                                title: function (tooltipItem, data) {
                                    return "";
                                },
                                label: function(tooltipItem, data) {
                                    return eChart.customTooltipTitle(tooltipItem, data, _chart);
                                },footer: function(t, d){
                                    return eChart.customTooltipFooter(t,d,_chart);
                                }
                            }
                        },
                        scales: {
                            yAxes: _chart.yAxes(_chart),
                            xAxes: _chart.xAxes()
                            /*xAxes: xAxesArray*/
                        },
                        annotation: {
                            dblClickSpeed:0,
                            events: ['click', 'mouseover', 'mouseout'],
                            annotations:annotationData
                        },
                        pan: {
                            // Boolean to enable panning
                            enabled: false,
                            // Panning directions. Remove the appropriate direction to disable
                            // Eg. 'y' would only allow panning in the y direction
                            mode: 'xy'
                        },
                        // Container for zoom options
                        zoom: {
                            // Boolean to enable zooming
                            enabled: false,
                            sensitivity: 0,
                            // Zooming directions. Remove the appropriate direction to disable
                            // Eg. 'y' would only allow zooming in the y direction
                            mode: 'x'
                        },
                        stack:true,
                        animation: eChart.markLabels(_chart)
                    }
                });
                var res = id.split("-");
                _chart.chartInstance = instance;
                _chart['tempGroupKey'] = _chart._group;
                eChart.generateLegend("left" + res[1], _chart);
                eChart.registerChart(_chart);
            };

            _chart.bindEvents = function () {
                eChart.bindEvents(this);
            };

            _chart.highlightSelectedFilter = function () {
                var labels = this.getLabels();
                var labelsLength = labels.length;
                var chartFiltersLength = 0;
                if (!_chart.reset) {
                    chartFiltersLength = this.filters.length;
                }
                var pointRadiusNumber=2;
                if(/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
                    pointRadiusNumber=20;
                }
                if (labelsLength == chartFiltersLength || chartFiltersLength === 0) {
                    _chart._data.datasets.forEach(function (dataset, dsIndex) {
                        _chart._data.datasets[dsIndex].backgroundColor = _chart._colorSelection[dsIndex];
                        labels.forEach(function (filter, i) {
                            try {
                                _chart._data.datasets[dsIndex].pointRadius = pointRadiusNumber;
                                _chart._data.datasets[dsIndex].pointBorderColor[i] = _chart._colorSelection[dsIndex];
                            } catch (e) {
                                //_chart._data.datasets[dsIndex].pointBorderColor = _chart._colorSelection[dsIndex];
                            }

                        });
                    });
                    _chart.filters = [];
                } else {
                    var filterLookup = {};
                    $.map(_chart.filters, function (value, index) {
                        if (!filterLookup[value['datasetIndex']]) {
                            filterLookup[value['datasetIndex']] = [];
                        }
                        filterLookup[value['datasetIndex']].push(value['label']);
                    });
                    labels.forEach(function (label, i) {
                        $.each(filterLookup, function (dsIndex, val) {
                            if (val.indexOf(label) != -1) {
                                try {
                                    _chart._data.datasets[dsIndex].pointRadius = pointRadiusNumber;
                                    _chart._data.datasets[dsIndex].pointBorderColor[i] = 'rgb(0, 0, 0)';
                                } catch (e) {
                                    //_chart._data.datasets[dsIndex].pointBorderColor = 'rgb(0, 0, 0)';
                                }
                            }
                        });
                    });
                }
            };

            _chart.getLabels = function () {
                return this.chartInstance.config.data.labels;
            };

            _chart.dataSetLabels = function (labels) {
                _chart._dataSetLabels = labels;
                return _chart;
            };

            // _chart.highlightSelectedFilter = function () {
            //
            // };
            _chart.getClickedElementLabel = function (evt) {
                var filter = {};
                try {
                    var evtObject=this.chartInstance.getElementAtEvent(evt)[0];
                    filter.label = this.getLabels()[evtObject._index];
                    filter.datasetLabel = evtObject._chart.config.data.datasets[evtObject._datasetIndex].label;
                    filter.datasetIndex = evtObject._datasetIndex;
                    if(evtObject._chart.config.data.datasets[evtObject._datasetIndex] && evtObject._chart.config.data.datasets[evtObject._datasetIndex].measure);
                    filter.measure = evtObject._chart.config.data.datasets[evtObject._datasetIndex].measure;
                    return filter;
                } catch (e) {
                    return null;
                }
            };

            _chart.getDataByGroup = function () {
                var groups = _chart._group;
                var datasets = [];
                var multipleMeasure=[];
                var measureLength=Object.keys(groups).length;
                var i=0;
                var dualXLabel={};
                $.each(groups, function (key, grpObj) {
                    var finalData=grpObj;
                    if (eChart.isDataFormatApplied(_chart) && !_chart._sort && !_chart.topN) {
                        finalData = eChart.getDataByDateFormat(grpObj, key, _chart);
                    }
                    if(i==0)
                        grpObj.forEach(function (d) {
                            dualXLabel[d.key]=d.dualXKey;
                        });
                    datasets = _chart.getDatasetByGroupColor(finalData, key,i,measureLength);
                    datasets.forEach(function (d) {
                        multipleMeasure.push(d);
                    });
                    i++;
                });
                var dataInFormat = {
                    labels: _chart._labels,
                    datasets: multipleMeasure,
                    dualXLabel:dualXLabel
                };
                return dataInFormat;
            };

            _chart.getNormalData = function () {
                var colorsLength = eChart.colors.length;
                var groups = _chart._group;
                var labels = [];
                var datasets = [];
                var i = 0;
                var j = 0;
                var index = 0;
                // End topN and sorting
                _chart._dataSetLabels=[];
                var dualXLabel={};
                $.each(groups, function (key, grpObj) {
                    var data = [];
                    var backgroundColor = [];
                    var borderColor = [];
                    var pointRadius = [];
                    var pointBorderColor = [];
                    var finalData=grpObj;
                    if (eChart.isDataFormatApplied(_chart) && !_chart._sort && !_chart.topN) {
                        finalData = eChart.getDataByDateFormat(grpObj, key, _chart);
                    }
                    if(_chart._runningTotal && _chart._runningTotal[key]){
                        finalData = eChart.getRunTotal(finalData,key,_chart);
                    }
                    _chart._dataSetLabels.push(key);
                    finalData.forEach(function (d) {
                        if(j==0){
                            labels.push(d.key);
                            dualXLabel[d.key]=d.dualXKey;
                        }
                        if (_chart._operator && d.value!=null && d.value[_chart._operator[key].key]) {
                            data.push(d.value[_chart._operator[key].key]);
                        } else {
                            data.push(d.value);
                        }
                        var pointRadiusNumber=2;
                        if(/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
                            pointRadiusNumber=10;
                        }
                        backgroundColor.push(_chart._colorSelection[j]);
                        pointRadius.push(pointRadiusNumber);
                        pointBorderColor.push(_chart._colorSelection[j]);
                        borderColor.push(_chart._colorSelection[j]);
                        //backgroundColor.push(eChart.colors[j]);
                        i++;
                    });
                    var dt = {
                        label: key,
                        data: data,
                        backgroundColor: backgroundColor[0], // COlor not coming so change backgroundColor[j] to backgroundColor
                        borderColor: backgroundColor[0],
                        /*hoverBorderColor: borderColor[j],*/
                        pointBorderColor: pointBorderColor,
                        pointRadius: pointRadius,
                        fill: true,
                        borderWidth: 2
                    };
                    datasets.push(dt);
                    j++;
                    index++;
                });
                //  _chart._allLabels=labels;
                var dataInFormat = {
                    labels: labels,
                    datasets: datasets,
                    dualXLabel:dualXLabel
                };
                return dataInFormat;
            };

            _chart.getDataInFormat = function (order) {
                var dataInFormat = {};
                eChart.updateAttributes(_chart);
                if (eChart.isGroupColorApplied(_chart)) {
                    dataInFormat = _chart.getDataByGroup();
                } else {
                    dataInFormat = _chart.getNormalData();
                }
                if (_chart._dataFormateVal && _chart._dataFormateVal.tooltipSelector) {
                    eChart.processTooltipData(_chart);
                }
                return dataInFormat;
            };

            return _chart;
        };

        // ------------------------------------area  END--------------------------------------------------------------
        // ---------------------------------------Box plot chart-------------------------------------------------------
        eChart.boxPlotChart = function (id) {
            var _chart = {};
            _chart.id = id;
            _chart.type = 'boxPlotChart';
            _chart._sort = null;
            _chart._colorSelection = $.extend([], eChart.colors);

            _chart.tKey = function (totalKeys) {
                _chart.totalKey = totalKeys;
                return _chart;
            };

            _chart.group = function (serverData) {
                _chart._group = serverData;
                _chart.data();
                return _chart;
            };

            _chart.setExcludeKey = function (excludeKey) {
                _chart._excludeKey = excludeKey;
                return _chart;
            }

            _chart.measures = function(measures){
                var measure = {};
                $.each(measures,function(key,val){
                    measure[key] = val;
                });
                _chart._measures = measure;
                return _chart;
            };

            _chart.dimension = function (dimension) {
                _chart._dimension = dimension;
                return _chart;
            };

            _chart.dataFormat = function (dataFormat) {
                _chart._dataFormateVal = dataFormat;
                _chart.setDataFormat();
                return _chart;
            };

            _chart.dataSetLabels = function (labels) {
                _chart._dataSetLabels = labels;
                return _chart;
            };

            _chart.sort = function (order) {
                _chart._sort = order;
                eChart.registerDataOrder(_chart);
            };

            _chart.setDataFormat = function () {
                /* y axis font size
                */
                if (_chart._dataFormateVal.yaxis && _chart._dataFormateVal.yaxis.fontSize) {
                    _chart.yfontSize(_chart._dataFormateVal.yaxis.fontSize);
                } else {
                    _chart.yfontSize(12);
                }
                /*
                 * x axis font size
                 */
                if (_chart._dataFormateVal.xaxis && _chart._dataFormateVal.xaxis.fontSize) {
                    _chart.xfontSize(_chart._dataFormateVal.xaxis.fontSize);
                } else {
                    _chart.xfontSize(12);
                }
                if(_chart._dataFormateVal.sort){
                    _chart._sort=_chart._dataFormateVal.sort;
                    _chart._sortMeasure=_chart._dataFormateVal.sortMeasure;
                }
                if(_chart._dataFormateVal.topN){
                    _chart.topN = _chart._dataFormateVal.topN;
                    _chart.topnMeasure = _chart._dataFormateVal.topnMeasure;
                    _chart.topNOther = _chart._dataFormateVal.topNOther;
                }
                if(_chart._dataFormateVal.runningTotal!=undefined){
                    _chart._runningTotal=_chart._dataFormateVal.runningTotal;
                }
                _chart.groupColor = _chart._dataFormateVal.groupColor;
                _chart._dataType = _chart._dataFormateVal.dataType;
            }

            _chart.xAxes = function () {
                /*
                 * scale labels
                 */
                var scaleLabels=false;
                if(_chart._dataFormateVal && _chart._dataFormateVal.xaxis && _chart._dataFormateVal.xaxis.axisLabel){
                    scaleLabels=_chart._dataFormateVal.xaxis.axisLabel;
                }
                var dualXaxisLabel=false;
                var dualXaxisLabelVal="";
                var axisLabelHeader=true;
                var allAxisHeader=false;
                var axisHeaderRotate=90;
                var autoSkip=false;
                if(_chart._dataFormateVal && _chart._dataFormateVal.xaxis){
                    /*
                     * X axis name
                     */
                    if(_chart._dataFormateVal.xaxis.dualXaxisLabel){
                        dualXaxisLabel=_chart._dataFormateVal.xaxis.dualXaxisLabel;
                        dualXaxisLabelVal=JSON.parse(_chart._dataFormateVal.xaxis.dualXDimension).reName;
                    }
                    /*
                     * Axis label option
                     */
                    if(_chart._dataFormateVal.xaxis.axisHeader!=undefined){
                        axisLabelHeader=_chart._dataFormateVal.xaxis.axisHeader;
                    }
                    /*
                     * All axis label
                     */
                    if(_chart._dataFormateVal.xaxis.allAxisHeader!=undefined){
                        allAxisHeader=_chart._dataFormateVal.xaxis.allAxisHeader;
                        if(allAxisHeader==true){
                            allAxisHeader=false;
                            autoSkip=false;
                        }else{
                            allAxisHeader=10;
                            autoSkip=true;
                        }
                    }
                    /*
                     * Rotate label
                     */
                    if(_chart._dataFormateVal.xaxis.axisHeaderRotate!=undefined){
                        axisHeaderRotate=_chart._dataFormateVal.xaxis.axisHeaderRotate;
                    }
                }
                /*
                 * Dual x axis flag
                 */
                var dualXAxisFlag=false;
                if(_chart._dataFormateVal && _chart._dataFormateVal.xaxis && _chart._dataFormateVal.xaxis.dualX && _chart._dataFormateVal.xaxis.dualX=='yes'){
                    dualXAxisFlag=true;
                }
                /*
                 *  dimension value
                 */
                var dimensionValue="";
                if(_chart._dimension){
                    dimensionValue=Object.keys(_chart._dimension)[0];
                }

                if(/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
                    _chart._fontSize=30;
                }
                var dualXSkipLabel={};
                if ((_chart._dataType == "date" || _chart._fontSize == "datetime") && _chart._dataFormateVal.xAxis == undefined) {
                    return [
                        {
                            id:'xAxis1',
                            type:"category",
                            stacked: true,
                            scaleLabel: {
                                display: scaleLabels,
                                labelString: dimensionValue
                            },
                            ticks: {
                                display:axisLabelHeader,
                                fontSize: _chart._xfontSize,
                                autoSkip: autoSkip,
                                maxTicksLimit: allAxisHeader,
                                maxRotation: axisHeaderRotate,
                                minRotation: axisHeaderRotate,
                                //maxTicksLimit: 10,
                                fontFamily: "sans-serif",
                                callback:function(label){
                                    label=label.toString();
                                    //return label;
                                    return label.slice(0, 15) +(label.length > 15 ? "..." : "");
                                }
                            },
                            type: 'time',
                            unit: 'day',
                            unitStepSize: 1,
                            time: {
                                displayFormats: {
                                    'day': 'MMM DD'
                                }
                            }
                        },
                        {
                            id:'xAxis2',
                            type:"category",
                            stacked: true,
                            position: 'top',
                            display:dualXAxisFlag,
                            scaleLabel: {
                                display: dualXaxisLabel,
                                labelString: dualXaxisLabelVal
                            },
                            gridLines: {
                                color: axisLineColor,
                                drawOnChartArea: true, // only want the grid lines for one axis to show up
                                //tickMarkLength: 10,
                                offsetGridLines: true
                            },
                            ticks:{
                                display:axisLabelHeader,
                                fontSize: _chart._xfontSize,
                                maxRotation: 0,
                                minRotation: 0,
                                fontFamily: "sans-serif",
                                callback:function(label,index,labels){
                                    label=label.toString();
                                    if(dualXSkipLabel[_chart._data.dualXLabel[label]]==undefined){
                                        dualXSkipLabel[_chart._data.dualXLabel[label]]=index;
                                    }
                                    if(dualXSkipLabel[_chart._data.dualXLabel[label]]==index){
                                        return _chart._data.dualXLabel[label];
                                    }
                                    return "";
                                }
                            },
                            type: 'time',
                            unit: 'day',
                            unitStepSize: 1,
                            time: {
                                displayFormats: {
                                    'day': 'MMM DD'
                                }
                            }
                        }
                    ];
                } else {
                    return [{
                        id:'xAxis1',
                        type:"category",
                        stacked: true,
                        scaleLabel: {
                            display: scaleLabels,
                            labelString: dimensionValue
                        },
                        gridLines: {
                            color: axisLineColor,
                            display: axisLabelHeader,
                        },
                        ticks:{
                            // display: false,
                            display:axisLabelHeader,
                            fontSize: _chart._xfontSize,
                            autoSkip: autoSkip,
                            maxRotation: axisHeaderRotate,
                            maxTicksLimit: allAxisHeader,
                            minRotation: axisHeaderRotate,
                            fontFamily: "sans-serif",
                            callback:function(label){
                                label=label.toString();
                                //return label;
                                return label.slice(0, 15) +(label.length > 15 ? "..." : "");
                            }
                        }
                    },
                    {
                        id:'xAxis2',
                        type:"category",
                        display:dualXAxisFlag,
                        stacked: true,
                        position: 'top',
                        scaleLabel: {
                            display: dualXaxisLabel,
                            labelString: dualXaxisLabelVal
                        },
                        gridLines: {
                            color: axisLineColor,
                            display:axisLabelHeader,
                            drawOnChartArea: true, // only want the grid lines for one axis to show up
                            //tickMarkLength: 10,
                            offsetGridLines: true
                        },
                        ticks:{
                            display:axisLabelHeader,
                            fontSize: _chart._xfontSize,
                            maxRotation: 0,
                            minRotation: 0,
                            fontFamily: "sans-serif",
                            callback:function(label,index,labels){
                                label=label.toString();
                                if(dualXSkipLabel[_chart._data.dualXLabel[label]]==undefined){
                                    dualXSkipLabel[_chart._data.dualXLabel[label]]=index;
                                }
                                if(dualXSkipLabel[_chart._data.dualXLabel[label]]==index){
                                    return _chart._data.dualXLabel[label];
                                }
                                return "";
                            }
                        }
                    }];
                }
            };

            _chart.yAxes = function(chart){
                return eChart.renderyAxes(_chart._dataFormateVal, chart);
            }

            _chart.dataType = function (type) {
                _chart._dataType = type;
            };

            _chart.fontFamily = function (fontFamily) {
                _chart._fontFamily = fontFamily;
            }

            _chart.yAxisPrefix = function (yAxis) {
                if (yAxis) {
                    _chart._yAxisPrefix = yAxis;
                } else {
                    _chart._yAxisPrefix = "";
                }
            }

            _chart.xfontSize = function (fontSize) {
                _chart._xfontSize = fontSize;
            };

            _chart.yfontSize = function (fontSize) {
                _chart._yfontSize = fontSize;
            };

            //---------------------------Group Color Start--------------------
            _chart.getDatasetByGroupColor = function (finalData, groupKey,grpIndex,measureLength) {
                var groupMappings = _chart.createGroupMappings(finalData, _chart);
                var initialData = [];
                _chart._labels.forEach(function (d, i) {
                    initialData[i] = null;
                });
                var p = 0;
                var datasets = [];
                _chart.groupColorDataOrder = [];
                var testArr=[];
                $.each(groupMappings, function (key, value) {
                    var data = Object.assign([], initialData);
                    $.each(value, function (k, v) {
                        var labelIndexMatched = _chart._labels.indexOf(k);

                        var grpColorDataOrder = {};
                        var pushFlag = true;
                        grpColorDataOrder['order'] = labelIndexMatched;
                        grpColorDataOrder['key'] = key;
                        grpColorDataOrder['label'] = k;
                        grpColorDataOrder['val'] = v;
                        _chart.groupColorDataOrder.forEach(function(d,i){
                            if(labelIndexMatched == d.order){
                                pushFlag = false;
                            }
                        });
                        if(pushFlag){
                            _chart.groupColorDataOrder.push(grpColorDataOrder);
                        }
                        if (labelIndexMatched != -1) {
                            data[labelIndexMatched] = v;
                        }
                    });
                    /*
                      If single measure
                     */
                    /* if(measureLength==1){*/
                    data.forEach(function (d,i) {
                        if(testArr[i]==undefined){
                            testArr[i]=[];
                        }
                        testArr[i].push(d);
                    });

                    p++;
                });
                var dt = {
                    measure:groupKey,
                    outlierColor: '#000000',
                    borderColor: Array(testArr.length).fill(_chart._colorSelection[p % (_chart._colorSelection.length)]),
                    itemRadius: 0,
                    outlierColor: '#000000',
                    label: "",
                    data: testArr,
                    backgroundColor: Array(testArr.length).fill(_chart._colorSelection[p % (_chart._colorSelection.length)]),
                    borderWidth: 1
                };
                datasets.push(dt);
                return datasets;
            }

            _chart.updateDatasetByGroupColor = function (finalData, groupKey) {
                var groupMappings = _chart.createGroupMappings(finalData, _chart);
                var initialData = [];
                _chart._data.labels.forEach(function (d, i) {
                    initialData[i] = null;
                });
                var labelsList = {};
                $.each(groupMappings, function (key, value) {
                    $.each(value, function (k, v) {
                        if (!(v == 0 || v < 0 || isNaN(v))) {
                            labelsList[k] = 1;
                        }
                    });
                });
                var labelsListOrdered = {};
                Object.keys(labelsList).forEach(function(key) {
                    labelsListOrdered[key] = labelsList[key];
                });
                var indexData = 0;
                var labelsListArray = [];
                $.each(labelsListOrdered, function (key, value) {
                    initialData[indexData] = null;
                    labelsListArray.push(key);
                    indexData++;
                });
                var p = 0;
                var datasets = [];
                $.each(groupMappings, function (key, value) {
                    var data = Object.assign([], initialData);
                    $.each(value, function (k, v) {
                        var labelIndexMatched = _chart._data.labels.indexOf(k);
                        if (labelIndexMatched != -1) {
                            if (!(v <= 0))
                                data[labelIndexMatched] = v;
                        }
                    });
                    var dt = {
                        measure:groupKey,
                        label: key,
                        data: data,
                        backgroundColor: Array(data.length).fill(_chart._colorSelection[p % (_chart._colorSelection.length)]),
                        borderWidth: 1
                    };
                    // _chart._labels = labels
                    datasets.push(dt);
                    p++;
                });
                return datasets;
            }
            //---------------------------Group Color End-----------------------


            _chart.createGroupMappings = function (finalData, _chart) {
                _chart._groupMappings = eChart.createGroupMappings(finalData, _chart);
                return _chart._groupMappings;
            }

            _chart.getDataByGroup = function () {
                var groups = _chart._group;
                var tempArray=[];
                var datasets = [];
                var measureLength=Object.keys(groups).length;
                var i=0;
                var j=0;
                var dualXLabel={};
                $.each(groups, function (key, grpObj) {
                    var finalData=grpObj;
                    if (eChart.isDataFormatApplied(_chart) && !_chart._sort && !_chart.topN) {
                        finalData = eChart.getDataByDateFormat(grpObj, key, _chart);
                    }
                    if(j==0)
                        grpObj.forEach(function (d) {
                            dualXLabel[d.key]=d.dualXKey;
                        });
                    datasets = _chart.getDatasetByGroupColor(finalData, key,i,measureLength);
                    datasets.forEach(function (d) {
                        tempArray.push(d);
                    });
                    j++;
                    i++;
                });
                var dataInFormat = {
                    labels: _chart._labels,
                    dualXLabel:dualXLabel,
                    datasets: tempArray
                };
                return dataInFormat;
            }

            _chart.getNormalData = function () {
                var colorsLength = eChart.colors.length;
                var groups = Object.assign({},_chart._group);


                var labels = [];
                var dualXLabel={};
                var datasets = [];
                var i = 0;
                var j = 0;
                var index = 0;
                /*
                 * Top n and reorder reinitialize
                 */
                _chart._dataSetLabels=[];
                $.each(groups, function (key, grpObj) {
                    var data = [];
                    var backgroundColor = [];
                    var borderColor = [];
                    var hoverBorderColor = [];
                    var finalData;
                    finalData = grpObj;
                    if (eChart.isDataFormatApplied(_chart) && !_chart._sort && !_chart.topN) {
                        finalData = eChart.getDataByDateFormat(grpObj, key, _chart);
                    }
                    if(_chart._runningTotal && _chart._runningTotal[key]){
                        finalData = eChart.getRunTotal(finalData,key,_chart);
                    }
                    _chart._dataSetLabels.push(key);
                    var dualXKeyCheck={};
                    finalData.forEach(function (d) {
                        if (j === 0){
                            labels.push(d.key);
                            /*if(dualXKeyCheck[d.dualXKey]==undefined){
                                dualXKeyCheck[d.dualXKey]=true;*/
                            dualXLabel[d.key]=d.dualXKey;
                            /* }else{
                                 dualXLabel[d.key]="";
                             }*/
                        }
                        if (_chart._operator && d.value!=null && d.value[_chart._operator[key].key]) {
                            data.push([d.value[_chart._operator[key].key]]);
                        } else {
                            data.push([d.value]);
                        }
                        backgroundColor.push(_chart._colorSelection[j]);
                        hoverBorderColor.push("#000");
                        i++;
                    });
                    var dt = {
                        label: key,
                        data: data,
                        backgroundColor: 'rgba(255,0,0,0.5)',
                        borderColor: 'rgba(255,0,0,0.5)',
                        borderWidth: 1
                    };
                    datasets.push(dt);
                    j++;
                });
                var dataInFormat = {
                    labels: labels,
                    datasets: datasets,
                    dualXLabel:dualXLabel
                };
                return dataInFormat;
            }

            _chart.getDataInFormat = function (order) {
                eChart.updateAttributes(_chart);
                var dataInFormat = {};
                if (eChart.isGroupColorApplied(_chart)) {
                    dataInFormat = _chart.getDataByGroup();
                } else {
                    dataInFormat = _chart.getNormalData();
                }
                try {
                    if (_chart._dataFormateVal && _chart._dataFormateVal.tooltipSelector) {
                        eChart.processTooltipData(_chart);
                    }
                } catch (e) {

                }
                return dataInFormat;
            }

            _chart.data = function () {
                _chart._data = _chart.getDataInFormat();
                return _chart;
            };

            _chart.render = function () {
                eChart.clearContainer(id);
                var bar = eChart.getCanvasContext(_chart, function (context) {
                    _chart.draw(context);
                    _chart.bindEvents();
                });
                sketchServer.reportLoadedCountCheck(_chart);
            };

            _chart.draw = function (context) {
                var instance = new Chart(context, {
                    type: 'boxplot',
                    data: _chart._data,
                    options: eChart.createOption(_chart)
                });
                var res = id.split("-");
                _chart.chartInstance = instance;
                eChart.generateLegend("left" + res[1], _chart);
                _chart['tempGroupKey'] = _chart._group;
                eChart.registerChart(_chart);
                sketchServer.globalRegistry.register(_chart);
                sketchServer.globalRegistry.syncLocalRegistries(_chart.id,"eChart");
            };

            _chart.operator = function (operator) {
                _chart._operator = operator;
                return _chart;
            };

            _chart.bindEvents = function () {
                eChart.bindEvents(this);
            };

            _chart.highlightSelectedFilter = function () {
                var labels = this.getLabels();
                var labelsLength = labels.length;
                var colorsLength = eChart.colors.length;
                var chartFiltersLength = 0;
                if (!_chart.reset) {
                    chartFiltersLength = this.filters.length;
                }
                if ((labelsLength == chartFiltersLength  && !eChart.isGroupColorApplied(_chart)) || chartFiltersLength === 0) {
                    _chart._data.datasets.forEach(function (dataset, dsIndex) {
                        labels.forEach(function (filter, i) {
                            try{
                                _chart._data.datasets[dsIndex].backgroundColor[i] = _chart._colorSelection[dsIndex % colorsLength];
                            }catch (e){

                            }
                        });
                    });
                    _chart.filters = [];
                } else {
                    var datasetsArray = [];
                    var datasetWiseFilters = {};
                    _chart.filters.forEach(function (filter) {
                        if (!datasetWiseFilters[filter.datasetIndex])
                            datasetWiseFilters[filter.datasetIndex] = [];
                        datasetWiseFilters[filter.datasetIndex].push(filter.label);
                    });
                    _chart._data.datasets.forEach(function (dataset, dsIndex) {
                        var filterLabelsArray = datasetWiseFilters[dsIndex];
                        if (filterLabelsArray) {
                            labels.forEach(function (label, index) {
                                if (filterLabelsArray.indexOf(label) == -1) {
                                    dataset.backgroundColor[index] = "#ddd";
                                } else {
                                    dataset.backgroundColor[index] = _chart._colorSelection[dsIndex % colorsLength];
                                }
                            });
                        } else {
                            dataset.backgroundColor = Array(labels.length).fill("#ddd");
                        }
                    });
                }
            };

            _chart.colorChange = function (selectedDataSet, color) {
                var labels = this.getLabels();
                _chart._data.datasets.forEach(function (dataset, dsIndex) {
                    if (dataset.label == selectedDataSet.datasetLabel) {
                        _chart._colorSelection[dsIndex] = color;
                        var tempData = [];
                        labels.forEach(function (filter, i) {
                            tempData.push(color);
                        });
                        _chart._data.datasets[dsIndex].backgroundColor = tempData;
                        _chart._data.datasets[dsIndex].borderColor = tempData;
                    }
                });
                setTimeout(function () {
                    _chart.chartInstance.update();
                    eChart.registerColor(_chart);
                    var res = _chart.id.split("-");
                    eChart.generateLegend("left" + res[1], _chart);
                }, 1);
            };

            _chart.getClickedElementLabel = function (evt) {
                var filter = {};
                try {
                    var eveObj=this.chartInstance.getElementAtEvent(evt)[0];
                    filter.label = eveObj._model.label;
                    filter.datasetLabel = eveObj._model.datasetLabel;
                    filter.datasetIndex = eveObj._datasetIndex;
                    if(eveObj._chart.config.data.datasets[eveObj._datasetIndex] && eveObj._chart.config.data.datasets[eveObj._datasetIndex].measure);
                    filter.measure = eveObj._chart.config.data.datasets[eveObj._datasetIndex].measure;
                    return filter;
                } catch (e) {
                    return null;
                }
            };

            _chart.getLabels = function () {
                return this.chartInstance.config.data.labels;
            };
            return _chart;
        }
        // ---------------------------------------End box plot chart-----------------------------------------------------
        // -------------------------------------Line CHART DRAW CODE--------------------------------------------------------

        eChart.lineChart = function (id) {
            var _chart = {};

            _chart.id = id;
            _chart.type = 'lineChart';
            _chart.dataBackup = [];

            //Color section changes
            _chart._colorSelection = $.extend([], eChart.colors);
            //End

            _chart.tKey = function (totalKeys) {
                _chart.totalKey = totalKeys;
                return _chart;
            };

            _chart.group = function (group) {
                _chart._group = group;
                _chart.data();
                return _chart;
            };

            _chart.dimension = function (dimension) {
                _chart._dimension = dimension;
                return _chart;
            };

            _chart.dataFormat = function (dataFormat) {
                _chart._dataFormateVal = dataFormat;
                _chart.setDataFormat();
                return _chart;
            };

            _chart.measures = function(measures){
                var measure={};
                $.each(measures,function(key,val){
                    measure[key]=val;
                });
                _chart._measures = measure;
                return _chart;
            };

            _chart.setDataFormat = function () {
                /* y axis font size
                */
                if (_chart._dataFormateVal.yaxis && _chart._dataFormateVal.yaxis.fontSize) {
                    _chart.yfontSize(_chart._dataFormateVal.yaxis.fontSize);
                } else {
                    _chart.yfontSize(12);
                }
                /*
                 * x axis font size
                 */
                if (_chart._dataFormateVal.xaxis && _chart._dataFormateVal.xaxis.fontSize) {
                    _chart.xfontSize(_chart._dataFormateVal.xaxis.fontSize);
                } else {
                    _chart.xfontSize(12);
                }
                if(_chart._dataFormateVal.topN){
                    _chart.topN = _chart._dataFormateVal.topN;
                    _chart.topnMeasure = _chart._dataFormateVal.topnMeasure;
                    _chart.topNOther = _chart._dataFormateVal.topNOther;
                }
                if(_chart._dataFormateVal.sort){
                    _chart._sort=_chart._dataFormateVal.sort;
                    _chart._sortMeasure=_chart._dataFormateVal.sortMeasure;
                }
                if(_chart._dataFormateVal.runningTotal!=undefined){
                    _chart._runningTotal=_chart._dataFormateVal.runningTotal;
                }
                _chart.groupColor = _chart._dataFormateVal.groupColor;
                _chart._dataType = _chart._dataFormateVal.dataType;
            }

            _chart.xfontSize = function (fontSize) {
                _chart._xfontSize = fontSize
            };

            _chart.yfontSize = function (fontSize) {
                _chart._yfontSize = fontSize
            };

            _chart.operator = function (operator) {
                _chart._operator = operator;
                return _chart;
            };

            _chart.data = function () {
                _chart._data = _chart.getDataInFormat();
                return _chart;
            };

            _chart.render = function () {
                eChart.clearContainer(id);
                var bar = eChart.getCanvasContext(_chart, function (context) {
                    _chart.draw(context);
                    _chart.bindEvents();
                });
                sketchServer.reportLoadedCountCheck(_chart);
                return true;
            };
            _chart.sort = function (order) {
                _chart._sort = order;
                eChart.registerDataOrder(_chart);
                _chart._metadataId=eChart._metadataId;
                sketchServer._lineChartJs(_chart);
            };


            //Style Setting
            _chart.styleSetting = function () {
                var ticks = {};
                ticks['fontSize'] = 20;
            }

            //Group Mapping...
            _chart.createGroupMappings = function (finalData, _chart) {
                _chart._groupMappings = eChart.createGroupMappings(finalData, _chart);
                return _chart._groupMappings;
            }

            //M -Start

            //---------------------------Group Color Start--------------------
            _chart.getDatasetByGroupColor = function (finalData, groupKey,grpIndex,measureLength) {
                var groupMappings = _chart.createGroupMappings(finalData, _chart);
                var initialData = [];
                _chart._labels.forEach(function (d, i) {
                    initialData[i] = null;
                });
                var p = 0;
                var datasets = [];
                _chart.groupColorDataOrder = [];
                $.each(groupMappings, function (key, value) {
                    var data = Object.assign([], initialData);
                    var pointRadius = Array(data.length).fill(2);
                    var pointBorderColor = Array(data.length).fill(_chart._colorSelection[p % (_chart._colorSelection.length)]);
                    var sumAdder=0;
                    $.each(value, function (k, v) {
                        var labelIndexMatched = _chart._labels.indexOf(k);

                        var grpColorDataOrder = {};
                        var pushFlag = true;
                        grpColorDataOrder['order'] = labelIndexMatched;
                        grpColorDataOrder['key'] = key;
                        grpColorDataOrder['label'] = k;
                        grpColorDataOrder['val'] = v;
                        _chart.groupColorDataOrder.forEach(function(d,i){
                            if(labelIndexMatched == d.order){
                                pushFlag = false;
                            }
                        });
                        if(pushFlag){
                            _chart.groupColorDataOrder.push(grpColorDataOrder);
                        }
                        if (labelIndexMatched != -1) {
                            if(_chart._operator[Object.keys(_chart._operator)[0]].key=="runTotal"){
                                sumAdder+=v;
                                if (!(v <= 0))
                                    data[labelIndexMatched] = sumAdder;
                            }else{
                                if (!(v <= 0))
                                    data[labelIndexMatched] += v;
                            }
                        }
                    });
                    var dt = {
                        measure:groupKey,
                        label: key,
                        data: data,
                        backgroundColor: _chart._colorSelection[p % (_chart._colorSelection.length)],
                        borderColor: _chart._colorSelection[p % (_chart._colorSelection.length)],
                        showLine: true,
                        fill: false,
                        borderWidth: 2,
                        lineTension: 0,
                        pointBorderColor: pointBorderColor,
                        pointRadius: 2
                    };
                    //     _chart._labels=labels
                    datasets.push(dt);
                    p++;
                });
                return datasets;
            }

            _chart.updateDatasetByGroupColor = function (finalData, groupKey) {
                var groupMappings = _chart.createGroupMappings(finalData, _chart);
                var labelsList = {};
                $.each(groupMappings, function (key, value) {
                    $.each(value, function (k, v) {
                        if (!(v == 0 || v < 0 || isNaN(v))) {
                            labelsList[k] = 1;
                        }
                    });
                });
                var labelsListOrdered = {};
                Object.keys(labelsList).forEach(function(key) {
                    labelsListOrdered[key] = labelsList[key];
                });
                var initialData = [];
                _chart._data.labels.forEach(function (d, i) {
                    initialData[i] = null;
                });
                var initialData = [];
                var indexData = 0;
                var labelsListArray = [];
                $.each(labelsListOrdered, function (key, value) {
                    initialData[indexData] = null;
                    labelsListArray.push(key);
                    indexData++;
                });
                var p = 0;
                var datasets = [];
                var i=1;
                $.each(groupMappings, function (key, value) {
                    var data = Object.assign([], initialData);
                    var sumAdder=0;
                    $.each(value, function (k, v) {
                        var labelIndexMatched = _chart._data.labels.indexOf(k);
                        if (labelIndexMatched != -1) {
                            if(_chart._operator[Object.keys(_chart._operator)[0]].key=="runTotal"){
                                sumAdder+=v;
                                if (!(v <= 0))
                                    data[labelIndexMatched] = sumAdder;
                            }else{
                                if (!(v <= 0))
                                    data[labelIndexMatched] += v;
                            }
                        }
                    });
                    var pointBorder = "#ccc";
                    var dt = {
                        measure:groupKey,
                        label: key,
                        data: data,
                        backgroundColor: _chart._colorSelection[p % (_chart._colorSelection.length)],
                        borderColor: _chart._colorSelection[p % (_chart._colorSelection.length)],
                        showLine: true,
                        fill: false,
                        borderWidth: 2,
                        lineTension: 0,
                        pointRadius: 2
                        /*
                          Change line chart for higlight filter not coming
                         */
                        //pointBorderColor: _chart._data.datasets[p].pointBorderColor,
                        //    pointRadius: _chart._data.datasets[p].pointRadius
                    };
                    //     _chart._labels=labels
                    datasets.push(dt);
                    p++;
                    i++;
                });
                return datasets;
            }
            //---------------------------Group Color End-----------------------
            //M-End
            _chart.colorChange = function (selectedDataSet, color) {
                var labels = this.getLabels();
                var labelsLength = labels.length;
                var colorsLength = eChart.colors.length;
                var chartFiltersLength = 0;
                _chart._data.datasets.forEach(function (dataset, dsIndex) {
                    if (dataset.label == selectedDataSet.datasetLabel) {
                        _chart._colorSelection[dsIndex] = color;
                        _chart._data.datasets[dsIndex].backgroundColor = color;
                        _chart._data.datasets[dsIndex].borderColor = color;
                        _chart._data.datasets[dsIndex].pointBorderColor = Array(_chart._data.datasets[dsIndex].data.length).fill(color);
                    }
                });
                setTimeout(function () {
                    eChart.registerColor(_chart);
                    _chart.chartInstance.update();
                    var res = _chart.id.split("-");
                    eChart.generateLegend("left" + res[1], _chart);
                }, 1);

            };

            _chart.xAxes = function () {
                return eChart.renderxAxes(_chart._dataType, _chart._xfontSize, _chart._dataFormateVal,_chart);
            };

            _chart.yAxes=function(chart){
                return eChart.renderyAxes(_chart._dataFormateVal, chart);
            }

            _chart.dataType = function (type) {
                _chart._dataType = type;
            };

            _chart.fontFamily = function (fontFamily) {
                _chart._fontFamily = fontFamily;
            }

            _chart.yAxisPrefix = function (yAxis) {
                if (yAxis) {
                    _chart._yAxisPrefix = yAxis;
                } else {
                    _chart._yAxisPrefix = "";
                }
            }

            _chart.getDatasetByMeasure = function (finalData) {
                var datasets = [];
                var backgroundColorIndex = 0;
                var GroupColor = {};
                var backgroundColor = [];
                finalData.forEach(function (d) {
                    // if (j === 0)
                    // labels.push(d.key);
                    if (_chart._operator) {
                        data.push(d.value[_chart._operator[key].key]);
                    } else {
                        data.push(d.value);
                    }
                    if (d.value.groupColor != undefined) {
                        if (GroupColor[d.value.groupColor]) {
                            backgroundColor.push(GroupColor[d.value.groupColor]);
                        } else {
                            backgroundColor.push(_chart._colorSelection[backgroundColorIndex % (_chart._colorSelection.length)]);
                            GroupColor[d.value.groupColor] = _chart._colorSelection[backgroundColorIndex % (_chart._colorSelection.length)];
                            backgroundColorIndex++;
                        }
                    } else {
                        backgroundColor.push(_chart._colorSelection[j % (_chart._colorSelection.length)]);
                    }
                    //backgroundColor.push(eChart.colors[j]);
                    i++;
                });

                var dt = {
                    label: _chart._dataSetLabels[j],
                    data: data,
                    backgroundColor: backgroundColor,
                    borderColor: backgroundColor,
                    borderWidth: 1,
                    pointBorderColor: backgroundColor,
                    pointRadius: 2,
                };
                datasets.push(dt);
                return datasets;
            }

            _chart.draw = function (context){
                var referenceLineData = eChart.getReferenceLineData(_chart);
                var annotationData=[];
                referenceLineData.forEach(function (d,index) {
                    var obj={
                        drawTime: "afterDatasetsDraw",
                        id: "hline"+index,
                        type: "line",
                        mode: "horizontal",
                        scaleID: "y-axis-0",
                        value: d.point,
                        borderColor: "black",
                        borderWidth: 2,
                        borderDash: [10, 10],
                        onMouseover:function(e){
                            /*var element = this;
                            element.options.borderWidth = 2;
                            element.options.label.enabled = true;
                            element.options.label.content = d.tooltip;
                            element.chartInstance.chart.canvas.style.cursor = 'pointer';
                            element.chartInstance.update();*/
                            $("#refrenceLineTooltip").html(d.tooltip);
                            $("#refrenceLineTooltip").show();
                            $('#refrenceLineTooltip').css({'top' : e.clientY,'opacity':1,'left':e.clientX});
                        },
                        onMouseleave: function(e) {
                            /*var element = this;
                            element.options.borderWidth = 2;
                            element.options.label.enabled = false;
                            element.chartInstance.chart.canvas.style.cursor = 'pointer';
                            element.chartInstance.update();*/
                            $("#refrenceLineTooltip").hide();
                            $('#refrenceLineTooltip').css({'opacity':0});
                        },
                    }
                    annotationData.push(obj);
                });

                var responsiveData = true;
                if(_chart._dataFormateVal && _chart._dataFormateVal.canvas && _chart._dataFormateVal.canvas.width && _chart._dataFormateVal.canvas.height){
                    responsiveData = false;
                }

                var instance = new Chart(context, {
                    type: 'line',
                    data: _chart._data,
                    options: {
                        responsive: responsiveData,
                        maintainAspectRatio: false,
                        animation:{
                            duration: 0
                        },
                        hover:{
                            animationDuration: 0
                        },
                        responsiveAnimationDuration: 0,
                        legend:{
                            display: false
                        },
                        elements: {
                            line: {
                                tension: 0, // disables bezier curves
                            },
                            point: {
                                backgroundColor: "#999",
                            }
                        },
                        layout: {
                            padding: {
                                right: 10
                            }
                        },
                        bezierCurve: false,
                        legend: {
                            display: false,
                            /*labels: {
                                filter: function(item, chart) {
                                    // Logic to remove a particular legend item goes here
                                    return !item.text.includes('Legend 1');
                                }
                            }*/
                        },
                        legendCallback: function (chartInstance) {
                            if(eChart.legendChartObj[_chart.id]!=undefined){
                                chartInstance.legend.legendItems = eChart.legendChartObj[_chart.id];
                            }
                            var html='';
                            if(_chart.groupColor)
                                html +="<li title="+JSON.parse(_chart.groupColor).reName+"><p>&nbsp;&nbsp;"+JSON.parse(_chart.groupColor).reName.slice(0, 16) + (JSON.parse(_chart.groupColor).reName.length > 16 ? "..." : "") +"</p></li>";
                            html += "<ul>";

                            var chart=_chart;
                            if (chart._groupColor) {
                                $.each(chart._groupColor, function (key, value) {
                                    html += '<li class=""><span style="background-color:' + value + '"></span>' + key + '</li>';
                                });
                            } else {
                                var legends=[];
                                var legendsObj={};
                                /*
                                   Duplicate legend remove
                                 */
                                chartInstance.legend.legendItems.forEach(function (d) {
                                    if(legendsObj[d.text]==undefined){
                                        legendsObj[d.text]=true;
                                        legends.push(d);
                                    }
                                });
                                legends.forEach(function (d) {
                                    if(d.hidden==false){
                                        var title=d.text.replace(/ /g,"&nbsp;");
                                        html += '<li class="" title='+title+'><span style="background-color:' + d.fillStyle + '"></span>' + d.text.slice(0, 12) + (d.text.length > 12 ? "..." : "") + '</li>';
                                    }
                                    //strokeStyle
                                });
                            }
                            html += "</ul>"
                            return html;
                        },
                        tooltips: {
                            enabled: true,
                            callbacks: {
                                title: function (tooltipItem, data) {
                                    return "";
                                },
                                label: function(tooltipItem, data) {
                                    return eChart.customTooltipTitle(tooltipItem, data, _chart);
                                },footer: function(t, d){
                                    return eChart.customTooltipFooter(t,d,_chart);
                                }
                            }
                        },
                        scales: {
                            yAxes: _chart.yAxes(_chart),
                            xAxes: _chart.xAxes()
                            /*xAxes: xAxesArray*/
                        },
                        annotation: {
                            events: ['click', 'mouseover', 'mouseout'],
                            annotations:annotationData
                        },
                        pan: {
                            // Boolean to enable panning
                            enabled: false,
                            // Panning directions. Remove the appropriate direction to disable
                            // Eg. 'y' would only allow panning in the y direction
                            mode: 'xy'
                        },
                        // Container for zoom options
                        zoom: {
                            // Boolean to enable zooming
                            enabled: false,
                            sensitivity: 0,
                            // Zooming directions. Remove the appropriate direction to disable
                            // Eg. 'y' would only allow zooming in the y direction
                            mode: 'x'
                        },
                        stack:true,
                        animation: eChart.markLabels(_chart)
                  }
                });
                var res = id.split("-");
                _chart.chartInstance = instance;
                _chart['tempGroupKey'] = _chart._group;
                eChart.generateLegend("left" + res[1], _chart);
                eChart.registerChart(_chart);
            };

            _chart.bindEvents = function () {
                eChart.bindEvents(this);
            };

            _chart.highlightSelectedFilter = function () {
                var labels = this.getLabels();
                var labelsLength = labels.length;
                var chartFiltersLength = 0;
                if (!_chart.reset) {
                    chartFiltersLength = this.filters.length;
                }
                var pointRadiusNumber=2;
                if(/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
                    pointRadiusNumber=20;
                }
                if (labelsLength == chartFiltersLength || chartFiltersLength === 0) {
                    _chart._data.datasets.forEach(function (dataset, dsIndex) {
                        _chart._data.datasets[dsIndex].backgroundColor = _chart._colorSelection[dsIndex];
                        labels.forEach(function (filter, i) {
                            try {
                                _chart._data.datasets[dsIndex].pointRadius = pointRadiusNumber;
                                _chart._data.datasets[dsIndex].pointBorderColor[i] = _chart._colorSelection[dsIndex];
                            } catch (e) {
                                //_chart._data.datasets[dsIndex].pointBorderColor = _chart._colorSelection[dsIndex];
                            }

                        });
                    });
                    _chart.filters = [];
                } else {
                    var filterLookup = {};
                    $.map(_chart.filters, function (value, index) {
                        if (!filterLookup[value['datasetIndex']]) {
                            filterLookup[value['datasetIndex']] = [];
                        }
                        filterLookup[value['datasetIndex']].push(value['label']);
                    });
                    labels.forEach(function (label, i) {
                        $.each(filterLookup, function (dsIndex, val) {
                            if (val.indexOf(label) != -1) {
                                try {
                                    _chart._data.datasets[dsIndex].pointRadius = pointRadiusNumber;
                                    _chart._data.datasets[dsIndex].pointBorderColor[i] = 'rgb(0, 0, 0)';
                                } catch (e) {
                                    //_chart._data.datasets[dsIndex].pointBorderColor = 'rgb(0, 0, 0)';
                                }
                            }
                        });
                    });
                }
            };

            _chart.getLabels = function () {
                return this.chartInstance.config.data.labels;
            };
            _chart.dataSetLabels = function (labels) {
                _chart._dataSetLabels = labels;
                return _chart;
            };

            // _chart.highlightSelectedFilter = function () {
            //
            // };
            _chart.getClickedElementLabel = function (evt) {
                var filter = {};
                try {
                    var evtObject=this.chartInstance.getElementAtEvent(evt)[0];
                    filter.label = this.getLabels()[evtObject._index];
                    filter.datasetLabel = evtObject._chart.config.data.datasets[evtObject._datasetIndex].label;
                    filter.datasetIndex = evtObject._datasetIndex;
                    if(evtObject._chart.config.data.datasets[evtObject._datasetIndex] && evtObject._chart.config.data.datasets[evtObject._datasetIndex].measure);
                    filter.measure = evtObject._chart.config.data.datasets[evtObject._datasetIndex].measure;
                    return filter;
                } catch (e) {
                    return null;
                }
            };
            _chart.getDataByGroup = function () {
                var groups = _chart._group;
                var datasets = [];
                var multipleMeasure=[];
                var measureLength=Object.keys(groups).length;
                var i=0;
                var dualXLabel={};
                $.each(groups, function (key, grpObj) {
                    var finalData=grpObj;
                    if (eChart.isDataFormatApplied(_chart) && !_chart._sort && !_chart.topN) {
                        finalData = eChart.getDataByDateFormat(grpObj, key, _chart);
                    }
                    if(i==0)
                        grpObj.forEach(function (d) {
                            dualXLabel[d.key]=d.dualXKey;
                        });
                    datasets = _chart.getDatasetByGroupColor(finalData, key,i,measureLength);
                    datasets.forEach(function (d) {
                        multipleMeasure.push(d);
                    });
                    i++;
                });
                var dataInFormat = {
                    labels: _chart._labels,
                    datasets: multipleMeasure,
                    dualXLabel:dualXLabel
                };
                return dataInFormat;
            };

            _chart.getNormalData = function () {
                var colorsLength = eChart.colors.length;
                var groups = _chart._group;
                // End topN and sorting
                var labels = [];
                var datasets = [];
                var i = 0;
                var j = 0;
                var index = 0;
                _chart._dataSetLabels=[];
                var dualXLabel={};
                $.each(groups, function (key, grpObj) {
                    var data = [];
                    var backgroundColor = [];
                    var borderColor = [];
                    var pointRadius = [];
                    var pointBorderColor = [];
                    var finalData=grpObj;
                    if (eChart.isDataFormatApplied(_chart) && !_chart._sort && !_chart.topN) {
                        finalData = eChart.getDataByDateFormat(grpObj, key, _chart);
                    }
                    if(_chart._runningTotal && _chart._runningTotal[key]){
                        finalData = eChart.getRunTotal(finalData,key,_chart);
                    }
                    _chart._dataSetLabels.push(key);
                    finalData.forEach(function (d) {
                        if(j==0){
                            labels.push(d.key);
                            dualXLabel[d.key]=d.dualXKey;
                        }
                        if (_chart._operator && d.value!=null && d.value[_chart._operator[key].key]) {
                            data.push(d.value[_chart._operator[key].key]);
                        } else {
                            data.push(d.value);
                        }
                        var pointRadiusNumber=2;
                        if(/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
                            pointRadiusNumber=10;
                        }
                        backgroundColor.push(_chart._colorSelection[j]);
                        pointRadius.push(pointRadiusNumber);
                        pointBorderColor.push(_chart._colorSelection[j]);
                        borderColor.push(_chart._colorSelection[j]);
                        //backgroundColor.push(eChart.colors[j]);
                        i++;
                    });
                    var dt = {
                        label: key,
                        data: data,
                        backgroundColor: backgroundColor[0], // COlor not coming so change backgroundColor[j] to backgroundColor
                        borderColor: backgroundColor[0],
                        /*hoverBorderColor: borderColor[j],*/
                        pointBorderColor: pointBorderColor,
                        pointRadius: pointRadius,
                        fill: false,
                        borderWidth: 2
                    };

                    datasets.push(dt);
                    j++;
                    index++;
                });
                //  _chart._allLabels=labels;
                var dataInFormat = {
                    labels: labels,
                    datasets: datasets,
                    dualXLabel:dualXLabel
                };
                return dataInFormat;
            };

            _chart.getDataInFormat = function (order) {
                var dataInFormat = {};
                eChart.updateAttributes(_chart);
                if (eChart.isGroupColorApplied(_chart)) {
                    dataInFormat = _chart.getDataByGroup();
                } else {
                    dataInFormat = _chart.getNormalData();
                }
                if (_chart._dataFormateVal && _chart._dataFormateVal.tooltipSelector) {
                    eChart.processTooltipData(_chart);
                }
                return dataInFormat;
            };
            return _chart;
        };
        // ------------------------------------line  END--------------------------------------------------------------



        // -------------------------------------PIE CHART DRAW CODE--------------------------------------------------
        eChart.pieChart = function (id) {
            var _chart = {};
            _chart.id = id;
            _chart.type = "pieChart";
            _chart.dataBackup = [];
            _chart._colorSelection = $.extend([], eChart.colors);

            _chart.tKey = function (totalKeys) {
                _chart.totalKey = totalKeys;
                return _chart;
            };

            _chart.group = function (serverData) {
                _chart._group = serverData;
                _chart.data();
                return _chart;
            };

            _chart.measures = function(measures){
                var measure={};
                $.each(measures,function(key,val){
                    measure[key]=val;
                });
                _chart._measures = measure;
                return _chart;
            };

            _chart.dataFormat = function (dataFormate) {
                _chart._dataFormateVal = dataFormate;
                return _chart;
            };

            _chart.dimension = function (dimension) {
                _chart._dimension = dimension;
                return _chart;
            };

            _chart.data = function (data) {
                _chart._data = _chart.getDataInFormat();
                return _chart;
            };

            _chart.operator = function (operator) {
                _chart._operator = operator;
                return _chart;
            };

            _chart.render = function () {
                eChart.clearContainer(id);
                var bar = eChart.getCanvasContext(_chart, function (context) {
                    _chart.draw(context);
                    _chart.bindEvents();
                });
                sketchServer.reportLoadedCountCheck(_chart);
                return true;
            };

            _chart.draw = function (context) {
                var instance = new Chart(context, {
                    type: 'doughnut',
                    data: _chart._data,
                    options: {
                        responsive: true,
                        maintainAspectRatio: false,
                        legend: {
                            display: false,
                        },
                        title: {
                            display: false,
                            text: ''
                        },
                        animation: {},
                        pan: {
                            // Boolean to enable panning
                            enabled: false,

                            // Panning directions. Remove the appropriate direction to disable
                            // Eg. 'y' would only allow panning in the y direction
                            mode: 'xy'
                        },
                        // Container for zoom options
                        zoom: {
                            // Boolean to enable zooming
                            enabled: false,

                            // Zooming directions. Remove the appropriate direction to disable
                            // Eg. 'y' would only allow zooming in the y direction
                            mode: 'x',
                        },
                        tooltips: {
                            enabled: true,
                            callbacks: {
                                title: function (tooltipItem, data) {
                                    return "";
                                },
                                label: function(tooltipItem, data) {
                                    return eChart.customTooltipTitle(tooltipItem, data,_chart);
                                },footer: function(t, d){
                                    return eChart.customTooltipFooter(t,d,_chart);
                                }
                            }
                        },
                        legendCallback: function (chartInstance) {
                            if(eChart.legendChartObj[_chart.id]!=undefined){
                                chartInstance.legend.legendItems=eChart.legendChartObj[_chart.id];
                            }
                            var html='';
                            if(Object.keys(_chart._dimension).length){
                                var dimension=Object.keys(_chart._dimension)[0];
                                html +="<li title="+dimension+"><p>&nbsp;&nbsp;"+dimension.slice(0, 16) + (dimension.length > 16 ? "..." : "") +"</p></li>";
                            }
                            html += "<ul>";
                            chartInstance.legend.legendItems.forEach(function (d) {
                                if(d.hidden==false) {
                                    html += '<li class=""><span style="background-color:' + d.fillStyle + '"></span>' + d.text + '</li>';
                                    //strokeStyle
                                }
                            });
                            html += "</ul>"
                            return html;
                        },
                        animation: eChart.markLabels(_chart),
                        hover: {
                            animationDuration:0
                        }
                    }
                });
                var res = id.split("-");
                _chart.chartInstance = instance;
                eChart.generateLegend("left" + res[1], _chart);
                _chart['tempGroupKey'] = _chart._group;
                eChart.registerChart(_chart);
                sketchServer.globalRegistry.register(_chart);
                sketchServer.globalRegistry.syncLocalRegistries(_chart.id,"eChart");
            };

            _chart.parseTooltipPieChart = function (format, xLabel, yLabel) {
                var formatText = format.replace("{xLabel}", xLabel);
                formatText = formatText.replace("{yLabel}", yLabel);
                return formatText;
            }

            _chart.colorChange = function (selectedDataSet, color) {
                //var labels = this.getLabels();
                var colorsLength = eChart.colors.length;
                var colorIndex;
                _chart._data.labels.forEach(function (d,index) {
                    if(d==selectedDataSet.label){
                        colorIndex=index;
                    }
                });
                _chart._colorSelection[colorIndex % colorsLength]=color;
                _chart._data.datasets[0].backgroundColor[colorIndex] = color;
                setTimeout(function () {
                    _chart.chartInstance.update();
                    eChart.registerColor(_chart);
                    var res = _chart.id.split("-");
                    eChart.generateLegend("left" + res[1], _chart);
                }, 1);
            };

            _chart.getDataInFormat = function () {
                if(_chart._dataFormateVal.topN){
                    _chart.topN = _chart._dataFormateVal.topN;
                    _chart.topNOther = _chart._dataFormateVal.topNOther;
                }
                eChart.updateAttributes(_chart);
                var colorsLength = eChart.colors.length;
                var groups = _chart._group;
                var labels = [];
                var datasets = [];
                var i = 0, j = 0, index = 0;
                $.each(groups, function (key, grpObj) {
                    var finalData;
                    var data = [];
                    var backgroundColor = [];
                    if (!_chart._sort)
                        finalData = grpObj;

                    finalData.forEach(function (d) {
                        if (index === 0)
                            labels.push(d.key);
                        if (_chart._operator) {
                            if(_chart._dataFormateVal && _chart._dataFormateVal.tooltipSelector){
                                if (d.value!=null && d.value[_chart._operator[key].key]) {
                                    data.push(d.value[_chart._operator[key].key]);
                                }else{
                                    data.push(d.value);
                                }
                            }else{
                                if (d.value!=null && d.value[_chart._operator[key].key]) {
                                    data.push(d.value[_chart._operator[key].key]);
                                }else{
                                    data.push(d.value);
                                }
                            }
                        } else {
                            if (d.value!=null && d.value[_chart._operator[key].key]) {
                                data.push(d.value[_chart._operator[key].key]);
                            }else{
                                data.push(d.value);
                            }
                        }
                        backgroundColor.push(_chart._colorSelection[i % colorsLength]);
                        i++;
                    });

                    var dt = {
                        data: data,
                        backgroundColor: backgroundColor
                    };
                    datasets.push(dt);
                    j++;
                    index++;
                });
                if (_chart._dataFormateVal && _chart._dataFormateVal.tooltipSelector) {
                    eChart.processTooltipData(_chart);
                }
                var dataInFormat = {
                    labels: labels,
                    datasets: datasets
                };
                return dataInFormat;
            };

            _chart.highlightSelectedFilter = function () {
                var labels = this.getLabels();
                var labelsLength = labels.length;
                var colorsLength = eChart.colors.length;
                var chartFiltersLength = 0;
                if (!_chart.reset) {
                    chartFiltersLength = this.filters.length;
                }
                if (labelsLength == chartFiltersLength || chartFiltersLength === 0) {
                    labels.forEach(function (filter, i) {
                        _chart._data.datasets[0].backgroundColor[i] = _chart._colorSelection[i % colorsLength];
                    });
                    _chart.filters = [];
                } else {
                    labels.forEach(function (filter, i) {
                        var index = eChart.getIndexOfObj(_chart.filters, eChart.constants.LABEL, filter);
                        if (index != -1) {
                            _chart._data.datasets[0].backgroundColor[i] = _chart._colorSelection[i % colorsLength];
                        } else {
                            _chart._data.datasets[0].backgroundColor[i] = "#ddd";
                        }
                    });
                }
            };

            _chart.getClickedElementLabel = function (evt) {
                var filter = {};
                try {
                    filter.label = this.chartInstance.getElementAtEvent(evt)[0]._model.label;
                    filter.datasetLabel = null;
                    return filter;
                } catch (e) {
                    return null;
                }
            };

            _chart.bindEvents = function () {
                eChart.bindEvents(this);
            };

            _chart.getLabels = function () {
                return this.chartInstance.config.data.labels;
            };
            return _chart;
        };
        // ------------------------------------PIE END------------------------------------------------------------




        

        // ------------------------------------BUBBLE CHART---------------------------------------------------------
        eChart.bubbleChart = function (id){
            var _chart = {};
            _chart.id = id;
            _chart.type = "bubbleChart";
            _chart.dataBackup = [];

            _chart._colorSelection = $.extend([], eChart.colors);

            _chart.tKey = function (totalKeys) {
                _chart.totalKey = totalKeys;
                return _chart;
            };

            _chart.group = function (group) {
                _chart._group = group;
                _chart.data();
                return _chart;
            };

            _chart.dimension = function (dimension) {
                _chart._dimension = dimension;
                return _chart;
            };

            _chart.dataFormat = function (dataFormat) {
                _chart._dataFormateVal = dataFormat;
                _chart.setDataFormat();
                return _chart;
            };

            _chart.measures = function(measures){
                var measure = {};
                $.each(measures,function(key,val){
                    measure[key] = val;
                });
                _chart._measures = measure;
                return _chart;
            };

            _chart.setDataFormat = function () {
                /*
                 *   y axis font size
                 */
                if (_chart._dataFormateVal.yaxis && _chart._dataFormateVal.yaxis.fontSize) {
                    _chart.yfontSize(_chart._dataFormateVal.yaxis.fontSize);
                } else {
                    _chart.yfontSize(12);
                }
                /*
                 * x axis font size
                 */
                if (_chart._dataFormateVal.xaxis && _chart._dataFormateVal.xaxis.fontSize) {
                    _chart.xfontSize(_chart._dataFormateVal.xaxis.fontSize);
                } else {
                    _chart.xfontSize(12);
                }

                if(_chart._dataFormateVal.topN){
                    _chart.topN = _chart._dataFormateVal.topN;
                    _chart.topnMeasure = _chart._dataFormateVal.topnMeasure;
                    _chart.topNOther = _chart._dataFormateVal.topNOther;
                }
                if(_chart._dataFormateVal.runningTotal!=undefined){
                    _chart._runningTotal=_chart._dataFormateVal.runningTotal;
                }
                _chart.groupColor = _chart._dataFormateVal.groupColor;
                _chart._dataType = _chart._dataFormateVal.dataType;
            }

            _chart.xfontSize = function (fontSize) {
                _chart._xfontSize = fontSize;
            };

            _chart.yfontSize = function (fontSize) {
                _chart._yfontSize = fontSize;
            };

            _chart.operator = function (operator) {
                _chart._operator = operator;
                return _chart;
            };

            _chart.data = function () {
                _chart._data = _chart.getDataInFormat();
                return _chart;
            };

            _chart.dataSetLabels = function (labels) {
                _chart._dataSetLabels = labels;
                return _chart;
            };

            _chart.dataType = function (type) {
                _chart._dataType = type;
            };

            _chart.fontSize = function (fontSize) {
                _chart._fontSize = fontSize;
            };

            _chart.fontFamily = function (fontFamily) {
                _chart._fontFamily = fontFamily;
            }

            _chart.yAxisPrefix = function (yAxis) {
                if (yAxis) {
                    _chart._yAxisPrefix = yAxis;
                } else {
                    _chart._yAxisPrefix = "";
                }
            }

            _chart.colorChange = function (selectedDataSet, color) {
                var labels = this.getLabels();
                _chart._data.datasets.forEach(function (dataset, dsIndex) {
                    if (dataset.label == selectedDataSet.datasetLabel) {
                        _chart._colorSelection[dsIndex] = color;
                        var tempData = [];
                        labels.forEach(function (filter, i) {
                            tempData.push(color);
                        });
                        _chart._data.datasets[dsIndex].backgroundColor = color;
                        _chart._data.datasets[dsIndex].borderColor = color;
                    }
                });
                setTimeout(function (){
                    _chart.chartInstance.update();
                    eChart.registerColor(_chart);
                    var res = _chart.id.split("-");
                    eChart.generateLegend("left" + res[1], _chart);
                }, 1);
            };

            _chart.render = function () {
                eChart.clearContainer(id);
                var bubble = eChart.getCanvasContext(_chart, function (context) {
                    _chart.draw(context);
                    _chart.bindEvents();
                });
                sketchServer.reportLoadedCountCheck(_chart);
            };

            _chart.draw = function (context) {
                var labelColor, axisLineColor;
                if(_chart._dataFormateVal && _chart._dataFormateVal.axisLabelColor){
                    labelColor = _chart._dataFormateVal.axisLabelColor;
                }else{
                    labelColor = '#666666';
                }
                if(_chart._dataFormateVal && _chart._dataFormateVal.axisLabelColor){
                    axisLineColor = _chart._dataFormateVal.axisColor;
                }else{
                    axisLineColor = '#e6e6e6';
                }

                var xScaleLabels = false, xScaleLabelsString = '';
                if(_chart._dataFormateVal && _chart._dataFormateVal.xaxis && _chart._dataFormateVal.xaxis.axisLabel){
                    xScaleLabels = _chart._dataFormateVal.xaxis.axisLabel;
                    xScaleLabelsString = JSON.parse(bubbleObject.xAxis).reName;
                }
                var yScaleLabels = false, yScaleLabelsString = '';
                if(_chart._dataFormateVal && _chart._dataFormateVal.yaxis && _chart._dataFormateVal.yaxis.axisLabel){
                    yScaleLabels = _chart._dataFormateVal.yaxis.axisLabel;
                    yScaleLabelsString = JSON.parse(bubbleObject.yAxis).reName;
                }
                var responsiveData = true;
                if(_chart._dataFormateVal && _chart._dataFormateVal.canvas && _chart._dataFormateVal.canvas.width && _chart._dataFormateVal.canvas.height){
                    responsiveData = false;
                }

                var instance = new Chart(context, {
                    type: 'bubble',
                    data: _chart._data,
                    options: {
                        // hover: {mode: null},
                        responsive: responsiveData,
                        maintainAspectRatio: false,
                        legend: {
                            display: false
                        },
                        // padding: 3,
                        title: {
                            display: false,
                            text: ''
                        },
                        tooltips: {
                            callbacks: {
                                label: function(t, d) {
                                    var tooltipHeader = '';
                                    if(d.grpColor){
                                        tooltipHeader = d.datasets[t.datasetIndex].grpColor + " : ";
                                    }else{
                                        tooltipHeader = d.datasets[t.datasetIndex].dim + " : ";
                                    }
                                    return tooltipHeader + d.datasets[t.datasetIndex].label;
                                },

                                footer: function(tooltipItems, data){
                                    var finalToolTip = [];
                                    var toolTipObj = data.datasets[tooltipItems[0].datasetIndex].data[0];

                                    _chart.groupLabels.forEach(function(d,index){
                                        var str = "";
                                        if(index==0){
                                            str += d + " : ";
                                            if (_chart._dataFormateVal && _chart._dataFormateVal.xaxis) {
                                                var xAxisObj = _chart._dataFormateVal.xaxis;
                                                var temp = toolTipObj.x;

                                                if(eChart.checkFloat(temp)){
                                                    temp = temp.toFixed(2);
                                                }
                                                if(xAxisObj.decimal){
                                                    temp = Number(temp);
                                                    temp = temp.toFixed(_chart._dataFormateVal.xaxis.decimal);
                                                }
                                                if(xAxisObj.units=="yes"){
                                                    temp = eChart.formatNumber(temp);
                                                }
                                                if(xAxisObj.separator){
                                                    temp = temp.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
                                                }
                                                if(xAxisObj.prefix){
                                                    temp = " " + xAxisObj.prefix + " " + temp;
                                                }
                                                if(xAxisObj.suffix){
                                                    temp = temp + " " + xAxisObj.suffix;
                                                }
                                            } else {
                                                temp = eChart.formatNumber(temp);
                                            }
                                            str += temp;
                                        }
                                        if(index==1){
                                            str += d + " : ";
                                            if (_chart._dataFormateVal && _chart._dataFormateVal.yaxis) {
                                                var yAxisObj = _chart._dataFormateVal.yaxis;
                                                var temp = toolTipObj.y;
                                                if(eChart.checkFloat(temp)){
                                                    temp = temp.toFixed(2);
                                                }
                                                if(yAxisObj.decimal){
                                                    temp = Number(temp);
                                                    temp = temp.toFixed(_chart._dataFormateVal.yaxis.decimal);
                                                }
                                                if(yAxisObj.units=="yes"){
                                                    temp = eChart.formatNumber(temp);
                                                }
                                                if(yAxisObj.separator){
                                                    temp = temp.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
                                                }
                                                if(yAxisObj.prefix){
                                                    temp = " " + yAxisObj.prefix + " " + temp;
                                                }
                                                if(yAxisObj.suffix){
                                                    temp = temp + " " + yAxisObj.suffix;
                                                }
                                            } else {
                                                temp = eChart.formatNumber(temp);
                                            }
                                            str += temp;
                                        }
                                        if(index==2){
                                            if(toolTipObj.v){
                                                var temp = toolTipObj.v;
                                                if(eChart.checkFloat(temp)){
                                                    temp = temp.toFixed(2);
                                                }
                                                str += d + " : ";
                                                str += temp;
                                            }
                                        }
                                        finalToolTip.push(str);
                                    });

                                    //custom tooltip
                                    if(data.datasets[tooltipItems[0].datasetIndex] && data.datasets[tooltipItems[0].datasetIndex].tooltip[tooltipItems[0].datasetIndex]){
                                        var customStr = data.datasets[tooltipItems[0].datasetIndex].tooltip[tooltipItems[0].datasetIndex].tooltip;
                                        var customStrArr = customStr.split('<br>');
                                        finalToolTip = finalToolTip.concat(customStrArr);
                                    }
                                    return finalToolTip;
                                },
                            },
                            backgroundColor: '#000'
                        },
                        scales :{
                            // xAxes: _chart.xAxes(),
                            xAxes: [{
                                scaleLabel: {
                                    display: xScaleLabels,
                                    labelString: xScaleLabelsString,
                                },
                                gridLines: {
                                    color: axisLineColor,
                                    display: true,
                                },
                                ticks: {
                                    fontColor: labelColor,
                                    beginAtZero: true,
                                    userCallback: function (value, index, values) {
                                        var maxVal = Math.max.apply(null, values);
                                        if(maxVal<=10){
                                            value = value.toFixed(2);
                                        }
                                        if (_chart._dataFormateVal && _chart._dataFormateVal.xaxis) {
                                            var xAxisObj = _chart._dataFormateVal.xaxis;
                                            if(xAxisObj.decimal){
                                                value = value.toFixed(_chart._dataFormateVal.xaxis.decimal);
                                            }
                                            if(xAxisObj.units=="yes"){
                                                value = eChart.formatNumber(value);
                                            }
                                            if(xAxisObj.separator){
                                                value = value.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
                                            }
                                            if(xAxisObj.prefix){
                                                value = xAxisObj.prefix+" "+value;
                                            }
                                            if(xAxisObj.suffix){
                                                value = value+" "+xAxisObj.suffix;
                                            }
                                            return value;
                                        } else {
                                            return eChart.formatNumber(value);
                                        }
                                    },
                                    fontSize: _chart._xfontSize,
                                    fontFamily: 'sans-serif'
                                }
                            }],
                            yAxes: [{
                                scaleLabel: {
                                    display: yScaleLabels,
                                    labelString: yScaleLabelsString,
                                },
                                stacked: false,
                                gridLines: {
                                    color: axisLineColor,
                                    display: true,
                                },
                                ticks: {
                                    fontColor: labelColor,
                                    beginAtZero: true,
                                    userCallback: function (value, index, values) {
                                        var maxVal = Math.max.apply(null, values);
                                        if(maxVal<=10){
                                            value = value.toFixed(2);
                                        }
                                        if (_chart._dataFormateVal && _chart._dataFormateVal.yaxis) {
                                            var yAxisObj = _chart._dataFormateVal.yaxis;
                                            if(yAxisObj.decimal){
                                                value = value.toFixed(_chart._dataFormateVal.yaxis.decimal);
                                            }
                                            if(yAxisObj.units=="yes"){
                                                value = eChart.formatNumber(value);
                                            }
                                            if(yAxisObj.separator){
                                                value = value.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
                                            }
                                            if(yAxisObj.prefix){
                                                value = yAxisObj.prefix+" "+value;
                                            }
                                            if(yAxisObj.suffix){
                                                value = value+" "+yAxisObj.suffix;
                                            }
                                            return value;
                                        } else {
                                            return eChart.formatNumber(value);
                                        }
                                    },
                                    fontSize: _chart._yfontSize,
                                    fontFamily: 'sans-serif'
                                }
                            }]
                        },
                        legend: {
                            display: false
                        },
                        legendCallback: function (chartInstance) {
                            if(eChart.legendChartObj[_chart.id]!=undefined){
                                chartInstance.legend.legendItems = eChart.legendChartObj[_chart.id];
                            }
                            var html='';
                            if(_chart.groupColor)
                                html +="<li title="+JSON.parse(_chart.groupColor).reName+"><p>&nbsp;&nbsp;"+JSON.parse(_chart.groupColor).reName.slice(0, 16) + (JSON.parse(_chart.groupColor).reName.length > 16 ? "..." : "") +"</p></li>";
                            html += "<ul>";
                            var chart=_chart;
                            if (chart._groupColor) {
                                $.each(chart._groupColor, function (key, value) {
                                    html += '<li class=""><span style="background-color:' + value + '"></span>' + key + '</li>';
                                });
                            } else {
                                var legends=[];
                                var legendsObj={};
                                /*
                                   Duplicate legend remove
                                 */
                                chartInstance.legend.legendItems.forEach(function (d) {
                                    if(legendsObj[d.text]==undefined){
                                        legendsObj[d.text]=true;
                                        legends.push(d);
                                    }
                                });
                                legends.forEach(function (d) {
                                    if(d.hidden==false) {
                                        var title = d.text[0].replace(/ /g, "&nbsp;");
                                        html += '<li class="" title=' + title + '><span style="background-color:' + d.fillStyle + '"></span>' + d.text[0].slice(0, 12) + (d.text[0].length > 12 ? "..." : "") + '</li>';
                                        //strokeStyle
                                    }
                                });
                            }
                            html += "</ul>"
                            return html;
                        },
                        animation:{
                            duration: 0
                        },
                        hover:{
                            animationDuration: 0
                        },
                        animation: eChart.markLabels(_chart)
                    },
                    animation: {},
                    pan: {
                        // Boolean to enable panning
                        enabled: false,
                        // Panning directions. Remove the appropriate direction to disable
                        // Eg. 'y' would only allow panning in the y direction
                        mode: 'xy'
                    },
                    // Container for zoom options
                    zoom: {
                        // Boolean to enable zooming
                        enabled: false,
                        // Zooming directions. Remove the appropriate direction to disable
                        // Eg. 'y' would only allow zooming in the y direction
                        mode: 'x',
                    }
                });
                var res = id.split("-");
                _chart.chartInstance = instance;
                eChart.generateLegend("left" + res[1], _chart);
                _chart['tempGroupKey'] = _chart._group;
                eChart.registerChart(_chart);
                sketchServer.globalRegistry.register(_chart);
                sketchServer.globalRegistry.syncLocalRegistries(_chart.id, "eChart");
            };

            _chart.bindEvents = function () {
                eChart.bindEvents(this);
            };

            _chart.highlightSelectedFilter = function () {
                try {
                    var labels = this.getLabels();
                    var labelsLength = labels.length;
                    var chartFiltersLength = 0;
                    if (!_chart.reset) {
                        chartFiltersLength = this.filters.length;
                    }
                    if(labelsLength == chartFiltersLength || chartFiltersLength === 0){
                        _chart._data.datasets.forEach(function (dataset, dsIndex) {
                            labels.forEach(function (filter, i) {
                                _chart._data.datasets[dsIndex].backgroundColor = eChart.colors[dsIndex];
                                // _chart._data.datasets[dsIndex].backgroundColor[i] = eChart.colors[dsIndex];
                            });
                        });
                        _chart.filters = [];
                    } else {
                        _chart._data.datasets.forEach(function (dataset, dsIndex) {
                            var index = eChart.getIndexOfObj(_chart.filters, eChart.constants.LABEL, _chart._data.datasets[dsIndex].label[0]);
                            if(index!=-1){
                                _chart._data.datasets[dsIndex].backgroundColor = eChart.colors[dsIndex];
                            }else{
                                _chart._data.datasets[dsIndex].backgroundColor = "#ddd";
                            }
                        });
                    }
                } catch (e) {

                }
            };

            _chart.getClickedElementLabel = function (evt) {
                var filter = {};
                if(this.chartInstance.getElementAtEvent(evt)[0]){
                    filter.label = this.chartInstance.getElementAtEvent(evt)[0]._chart.config.data.labels[this.chartInstance.getElementAtEvent(evt)[0]._datasetIndex];
                    filter.datasetLabel = this.chartInstance.getElementAtEvent(evt)[0]._chart.config.data.labels[this.chartInstance.getElementAtEvent(evt)[0]._datasetIndex];
                }
                return filter;
            };

            _chart.getLabels = function () {
                return this.chartInstance.config.data.labels;
            };

            _chart.getDataInFormat = function () {
                eChart.updateAttributes(_chart);
                var groups = _chart._group;
                var labels = [];
                var datasets = [];
                var j = 0;
                _chart.groupLabels = [];
                var groupObjkeys = Object.keys(groups);
                var radiusArr = [];
                var bubbleTooltipArr = [];


                var dataMap = {};
                var oldMin = 0;
                var oldMax = 1;
                var newMin = 5;
                var newMax = 30;
                var bubbleObject = _chart._dataFormateVal.bubbleObject;
                _chart.groupLabels.push(JSON.parse(bubbleObject.xAxis).reName);
                _chart.groupLabels.push(JSON.parse(bubbleObject.yAxis).reName);

                var axisObj = {};
                axisObj['x'] = JSON.parse(bubbleObject.xAxis).columnName;
                axisObj['y'] = JSON.parse(bubbleObject.yAxis).columnName;
                var radiusFlag = true;
                if(bubbleObject.radius){
                    _chart.groupLabels.push(JSON.parse(bubbleObject.radius).reName);
                    axisObj['r'] = JSON.parse(bubbleObject.radius).columnName;
                    groups[axisObj['r']].forEach(function(d){
                        if($.isEmptyObject(d.value)){
                            radiusArr.push(d.value);
                        }else{
                            radiusArr.push(d.value[_chart._operator[axisObj['r']].key]);
                        }
                    });
                    radiusArr.sort(function(a, b){return a - b});
                    oldMin = radiusArr[0];
                    oldMax = radiusArr[radiusArr.length-1];
                    radiusFlag = false;
                }
                $.each(axisObj, function(k,v){
                    var tempStr;
                    groups[v].forEach(function(d,i){
                        if(dataMap[d.key] == undefined){
                            dataMap[d.key] = {};
                        }
                        if(k == 'x'){
                            labels.push(d.key);
                        }
                        // Axis & Radius
                        if($.isEmptyObject(d.value)){
                            if(k != 'r'){
                                dataMap[d.key][k] = d.value;
                                if(radiusFlag){
                                    dataMap[d.key]['r'] = 10;
                                }
                            }else{
                                dataMap[d.key]['v'] = d.value;
                                if((oldMax - oldMin) == 0){
                                    dataMap[d.key][k] = parseInt((newMin + (d.value - oldMin) * (newMax - newMin) / 1));
                                }else{
                                    dataMap[d.key][k] = parseInt((newMin + (d.value - oldMin) * (newMax - newMin) / (oldMax - oldMin)));
                                }
                            }
                        }else{
                            if(k != 'r'){
                                dataMap[d.key][k] = d.value[_chart._operator[v].key];
                                if(radiusFlag){
                                    dataMap[d.key]['r'] = 10;
                                }
                            }else{
                                dataMap[d.key]['v'] = d.value[_chart._operator[v].key];
                                if((oldMax - oldMin) == 0){
                                    dataMap[d.key][k] = parseInt((newMin + (parseInt(d.value[_chart._operator[v].key]) - oldMin) * (newMax - newMin) / 1));
                                }else{
                                    dataMap[d.key][k] = parseInt((newMin + (parseInt(d.value[_chart._operator[v].key] - oldMin)) * (newMax - newMin) / (oldMax - oldMin)));
                                }
                            }
                        }
                        // Tooltip
                        if(bubbleObject && bubbleObject.tooltip && bubbleObject.tooltip != '' && d.value.sumIndex && !d.value.calculated && k == 'x'){
                            var tempObj = {};
                            tempStr = bubbleObject.tooltip;
                            $.each(d.value,function(key,val){
                                tempStr = tempStr.replace(key, val);
                            });
                            if(tempStr){
                                tempStr = tempStr.replace(/\n/g, "<br>");
                            }
                            tempObj['key'] = d.key;
                            tempObj['tooltip'] = tempStr;
                            bubbleTooltipArr.push(tempObj);
                        }
                    });
                });
                var newLabels = [];
                labels.forEach(function (label, i){
                    j=0;
                    $.each(dataMap[label],function(key,val){
                        if(val==0){
                            j++;
                        }
                    });

                    var grpColor_BubbleTooltip;
                    var dimension_BubbleTooltip = JSON.parse(bubbleObject.dimension).reName;
                    if(bubbleObject.groupColor != undefined && bubbleObject.groupColor != ''){
                        grpColor_BubbleTooltip = JSON.parse(bubbleObject.groupColor).reName;
                    }

                    if(bubbleObject != undefined && bubbleObject.groupColor != undefined && bubbleObject.groupColor != '{}' && !$.isEmptyObject(bubbleObject.groupColor)){
                        if(j==0){
                            var dt = {
                                label: [label],
                                data: [dataMap[label]],
                                backgroundColor: eChart.colors[i % (eChart.colors.length)],
                                // borderColor: _chart._colorSelection[i % (_chart._colorSelection.length)],
                                dim:  dimension_BubbleTooltip,
                                grpColor:  grpColor_BubbleTooltip,
                                tooltip:  bubbleTooltipArr,
                            };
                            newLabels.push(label);
                            datasets.push(dt);
                        }
                    }else{
                        if(j==0){
                            var dt = {
                                label: [label],
                                data: [dataMap[label]],
                                backgroundColor: eChart.colors[0],
                                // borderColor: _chart._colorSelection[0],
                                dim:  dimension_BubbleTooltip,
                                grpColor:  grpColor_BubbleTooltip,
                                tooltip:  bubbleTooltipArr,
                            };
                            newLabels.push(label);
                            datasets.push(dt);
                        }
                    }
                });

                var grpColor_BubbleTooltip;
                var dimension_BubbleTooltip = JSON.parse(bubbleObject.dimension).reName;
                if(bubbleObject.groupColor != undefined && bubbleObject.groupColor != ''){
                    grpColor_BubbleTooltip = JSON.parse(bubbleObject.groupColor).reName;
                }
                var dataInFormat = {
                    // dim:  dimension_BubbleTooltip,
                    // grpColor:  grpColor_BubbleTooltip,
                    // tooltip:  bubbleTooltipArr,
                    labels:  newLabels,
                    datasets: datasets
                };
                return dataInFormat;
            };

            _chart.updateDataset = function(){
                eChart.updateAttributes(_chart);
                var groups = _chart._group;
                var labels = [];
                var datasets = [];
                var j = 0;
                _chart.groupLabels = [];
                var groupObjkeys = [];
                if(groups != undefined){
                    groupObjkeys = Object.keys(groups);
                }
                var bubbleObject = _chart._dataFormateVal.bubbleObject;
                var dataMap = {};
                var oldMin = 0;
                var oldMax = 1;
                var newMin = 5;
                var newMax = 30;
                var radiusArr = [];
                var bubbleTooltipArr = [];
                _chart.groupLabels.push(JSON.parse(bubbleObject.xAxis).reName);
                _chart.groupLabels.push(JSON.parse(bubbleObject.yAxis).reName);
                var axisObj = {};
                axisObj['x'] = JSON.parse(bubbleObject.xAxis).columnName;
                axisObj['y'] = JSON.parse(bubbleObject.yAxis).columnName;
                var radiusFlag = true;
                if(bubbleObject.radius){
                    _chart.groupLabels.push(JSON.parse(bubbleObject.radius).reName);
                    axisObj['r'] = JSON.parse(bubbleObject.radius).columnName;
                    if(groups && axisObj && axisObj['r'] && groups[axisObj['r']])
                    groups[axisObj['r']].forEach(function(d){
                        if($.isEmptyObject(d.value)){
                            radiusArr.push(d.value);
                        }else{
                            radiusArr.push(d.value[_chart._operator[axisObj['r']].key]);
                        }
                    });
                    radiusArr.sort(function(a, b){return a - b});
                    oldMin = radiusArr[0];
                    oldMax = radiusArr[radiusArr.length-1];
                    radiusFlag = false;
                }
                $.each(axisObj, function(k,v){
                    var tempStr;
                    groups[v].forEach(function(d){
                        if(dataMap[d.key] == undefined){
                            dataMap[d.key] = {};
                        }
                        if(k == 'x'){
                            labels.push(d.key);
                        }
                        // Axis & Radius
                        if($.isEmptyObject(d.value)){
                            if(k != 'r'){
                                dataMap[d.key][k] = d.value;
                                if(radiusFlag){
                                    dataMap[d.key]['r'] = 10;
                                }
                            }else{
                                dataMap[d.key]['v'] = d.value;
                                if((oldMax - oldMin) == 0){
                                    dataMap[d.key][k] = parseInt((newMin + (d.value - oldMin) * (newMax - newMin) / 1));
                                }else{
                                    dataMap[d.key][k] = parseInt((newMin + (d.value - oldMin) * (newMax - newMin) / (oldMax - oldMin)));
                                }
                            }
                        }else{
                            if(k != 'r'){
                                dataMap[d.key][k] = d.value[_chart._operator[v].key];
                                if(radiusFlag){
                                    dataMap[d.key]['r'] = 10;
                                }
                            }else{
                                dataMap[d.key]['v'] = d.value[_chart._operator[v].key];
                                if((oldMax - oldMin) == 0){
                                    dataMap[d.key][k] = parseInt((newMin + (parseInt(d.value[_chart._operator[v].key]) - oldMin) * (newMax - newMin) / 1));
                                }else{
                                    dataMap[d.key][k] = parseInt((newMin + (parseInt(d.value[_chart._operator[v].key] - oldMin)) * (newMax - newMin) / (oldMax - oldMin)));
                                }
                            }
                        }
                        // Tooltip
                        if(bubbleObject && bubbleObject.tooltip && bubbleObject.tooltip != '' && d.value.sumIndex && !d.value.calculated && k == 'x'){
                            var tempObj = {};
                            tempStr = bubbleObject.tooltip;
                            $.each(d.value,function(key,val){
                                tempStr = tempStr.replace(key, val);
                            });
                            tempStr = tempStr.replace(/\n/g, "<br>");
                            tempObj['key'] = d.key;
                            tempObj['tooltip'] = tempStr;
                            bubbleTooltipArr.push(tempObj);
                        }
                    });
                });

                labels.forEach(function (label, i) {
                    j=0;
                    $.each(dataMap[label],function(key,val){
                        if(val==0){
                            j++;
                        }
                    });

                    var grpColor_BubbleTooltip;
                    var dimension_BubbleTooltip = JSON.parse(bubbleObject.dimension).reName;
                    if(bubbleObject.groupColor != undefined && bubbleObject.groupColor != ''){
                        grpColor_BubbleTooltip = JSON.parse(bubbleObject.groupColor).reName;
                    }

                    if(bubbleObject != undefined && bubbleObject.groupColor != undefined && bubbleObject.groupColor != '{}' && !$.isEmptyObject(bubbleObject.groupColor)){
                        if(j==0){
                            var dt = {
                                label: [label],
                                data: [dataMap[label]],
                                // backgroundColor: eChart.colors[i % (eChart.colors.length)]
                                backgroundColor: _chart._colorSelection[i % (_chart._colorSelection.length)],
                                dim:  dimension_BubbleTooltip,
                                grpColor:  grpColor_BubbleTooltip,
                                tooltip:  bubbleTooltipArr,
                            };
                            datasets.push(dt);
                        }
                    }else{
                        if(j==0){
                            var dt = {
                                label: [label],
                                data: [dataMap[label]],
                                // backgroundColor: eChart.colors[0]
                                backgroundColor: _chart._colorSelection[0],
                                dim:  dimension_BubbleTooltip,
                                grpColor:  grpColor_BubbleTooltip,
                                tooltip:  bubbleTooltipArr,
                            };
                            datasets.push(dt);
                        }
                    }
                });
                _chart.chartInstance.config.data.datasets = datasets;
                _chart.chartInstance.config.data.labels = labels;
            }
            return _chart;
        };
        // --------------------------------------BUBBLE END---------------------------------------------------------------






        // -----------------------------------Composite Chart START----------------------------------------------------------
        eChart.compositeChart = function (id) {
            var _chart = {};
            _chart.type = 'composite';
            _chart.id = id;
            _chart._sort = null;

            _chart._colorSelection = $.extend([], eChart.colors);

            _chart.measures=function(measures){
                var measure={};
                $.each(measures,function(key,val){
                    measure[key]=val;
                });
                _chart._measures = measure;
                return _chart;
            };

            _chart.tKey = function (totalKeys) {
                _chart.totalKey = totalKeys;
                return _chart;
            };

            _chart.group = function (group) {
                _chart._group = group;
                _chart.data();
                return _chart;
            };

            _chart.dimension = function (dimension) {
                _chart._dimension = dimension;
                return _chart;
            };

            _chart.dataFormat = function (dataFormat) {
                _chart._dataFormateVal = dataFormat;

                _chart.setDataFormat();
                return _chart;
            };

            _chart.dataSetLabels = function (labels) {
                _chart._dataSetLabels = labels;
                return _chart;
            };

            _chart.sort = function (order) {
                _chart._sort = order;

                eChart.registerDataOrder(_chart);
                _chart._metadataId=eChart._metadataId;
                sketchServer._compositeChartJs(_chart);
            };

            _chart.xAxes = function () {
                var labelColor, axisLineColor;
                if(_chart._dataFormateVal && _chart._dataFormateVal.axisLabelColor){
                    labelColor = _chart._dataFormateVal.axisLabelColor;
                }else{
                    labelColor = '#666666';
                }
                if(_chart._dataFormateVal && _chart._dataFormateVal.axisLabelColor){
                    axisLineColor = _chart._dataFormateVal.axisColor;
                }else{
                    axisLineColor = '#e6e6e6';
                }

                /*
                 * scale labels
                 */
                var scaleLabels=false;
                var axisLabelHeader=true;
                var allAxisHeader=false;
                var axisHeaderRotate=90;
                var autoSkip=false;
                if(_chart._dataFormateVal && _chart._dataFormateVal.xaxis){
                    if(_chart._dataFormateVal.xaxis.axisLabel){
                        scaleLabels=_chart._dataFormateVal.xaxis.axisLabel;
                    }
                    /*
                     * Axis label option
                     */
                    if(_chart._dataFormateVal.yaxis.axisHeader!=undefined){
                        axisLabelHeader=_chart._dataFormateVal.yaxis.axisHeader;
                    }
                    /*
                     * All axis label
                     */
                    if(_chart._dataFormateVal.xaxis.allAxisHeader!=undefined){
                        allAxisHeader=_chart._dataFormateVal.xaxis.allAxisHeader;
                        if(allAxisHeader==true){
                            autoSkip=false;
                            allAxisHeader=false;
                        }else{
                            autoSkip=true;
                            allAxisHeader=10;
                        }
                    }
                    /*
                     * Rotate label
                     */
                    if(_chart._dataFormateVal.xaxis.axisHeaderRotate!=undefined){
                        axisHeaderRotate=_chart._dataFormateVal.xaxis.axisHeaderRotate;
                    }
                }
                /*
                 * Dual x axis flag
                 */
                var dualXAxisFlag=false;
                if(_chart._dataFormateVal && _chart._dataFormateVal.xaxis && _chart._dataFormateVal.xaxis.dualX && _chart._dataFormateVal.xaxis.dualX=='yes'){
                    dualXAxisFlag=true;
                }
                /*
                 *  dimension value
                 */
                var dimensionValue="";
                if(_chart._dimension){
                    dimensionValue=Object.keys(_chart._dimension)[0];
                }
                var dualXaxisLabel=false;
                var dualXaxisLabelVal="";
                if(_chart._dataFormateVal && _chart._dataFormateVal.xaxis && _chart._dataFormateVal.xaxis.dualXaxisLabel){
                    dualXaxisLabel=_chart._dataFormateVal.xaxis.dualXaxisLabel;
                    dualXaxisLabelVal=JSON.parse(_chart._dataFormateVal.xaxis.dualXDimension).reName;
                }

                if(/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
                    _chart._xfontSize=30;
                }
                var dualXSkipLabel={};
                if ((_chart._dataType == "date" || _chart._fontSize == "datetime") && _chart._dataFormateVal.xAxis == undefined) {
                    return [{
                        id:'xAxis1',
                        type:"category",
                        scales: {
                            xAxes: [{
                                stacked: true,
                            }],
                            yAxes: [{
                                stacked: true
                            }]
                        },
                        stacked: true,
                        scaleLabel: {
                            display: scaleLabels,
                            labelString: dimensionValue
                        },
                        gridLines: {
                            color: axisLineColor,
                            display: axisLabelHeader,
                        },
                        ticks: {
                            fontColor: labelColor,
                            display:axisLabelHeader,
                            fontSize: _chart._xfontSize,
                            autoSkip: autoSkip,
                            maxRotation: axisHeaderRotate,
                            minRotation: axisHeaderRotate,
                            maxTicksLimit: allAxisHeader,
                            fontFamily: "sans-serif",
                            callback:function(label){
                                label=label.toString();
                                //return label;
                                return label.slice(0, 15) +(label.length > 15 ? "..." : "");
                            }
                        },
                        type: 'time',
                        unit: 'day',
                        unitStepSize: 1,
                        time: {
                            displayFormats: {
                                'day': 'MMM DD'
                            }
                        }
                    },
                    {
                        id:'xAxis2',
                        type:"category",
                        display:dualXAxisFlag,
                        scales: {
                            xAxes: [{
                                stacked: true,
                            }],
                            yAxes: [{
                                stacked: true
                            }]
                        },
                        stacked: true,
                        position: 'top',
                        scaleLabel: {
                            display: dualXaxisLabel,
                            labelString: dualXaxisLabelVal
                        },
                        gridLines: {
                            color: axisLineColor,
                            display:axisLabelHeader,
                            drawOnChartArea: true, // only want the grid lines for one axis to show up
                            //tickMarkLength: 10,
                            offsetGridLines: true
                        },
                        ticks:{
                            fontColor: labelColor,
                            display:axisLabelHeader,
                            fontSize: _chart._xfontSize,
                            maxRotation: 0,
                            minRotation: 0,
                            fontFamily: "sans-serif",
                            callback:function(label,index,labels){
                                label=label.toString();
                                if(dualXSkipLabel[_chart._data.dualXLabel[label]]==undefined){
                                    dualXSkipLabel[_chart._data.dualXLabel[label]]=index;
                                }
                                if(dualXSkipLabel[_chart._data.dualXLabel[label]]==index){
                                    return _chart._data.dualXLabel[label];
                                }
                                return "";
                            }
                        },
                        type: 'time',
                        unit: 'day',
                        unitStepSize: 1,
                        time: {
                            displayFormats: {
                                'day': 'MMM DD'
                            }
                        }}];
                } else {
                    if(eChart.isGroupColorApplied(_chart)){
                        return [{
                            id:'xAxis1',
                            type:"category",
                            scales: {
                                xAxes: [{
                                    stacked: true,
                                }],
                                yAxes: [{
                                    stacked: true
                                }]
                            },
                            stacked: true,
                            scaleLabel: {
                                display: scaleLabels,
                                labelString: dimensionValue
                            },
                            gridLines: {
                                color: axisLineColor,
                                display: axisLabelHeader,
                            },
                            ticks:{
                                fontColor: labelColor,
                                display:axisLabelHeader,
                                fontSize: _chart._xfontSize,
                                autoSkip: autoSkip,
                                maxRotation: axisHeaderRotate,
                                minRotation: axisHeaderRotate,
                                maxTicksLimit: allAxisHeader,
                                fontFamily: "sans-serif",
                                callback:function(label){
                                    label=label.toString();
                                    //return label;
                                    return label.slice(0, 15) +(label.length > 15 ? "..." : "");
                                }
                            }
                        },
                        {
                            id:'xAxis2',
                            type:"category",
                            display:dualXAxisFlag,
                            scales: {
                                xAxes: [{
                                    stacked: true,
                                }],
                                yAxes: [{
                                    stacked: true
                                }]
                            },
                            stacked: true,
                            position: 'top',
                            scaleLabel: {
                                display: dualXaxisLabel,
                                labelString: dualXaxisLabelVal
                            },
                            gridLines: {
                                color: axisLineColor,
                                display:axisLabelHeader,
                                drawOnChartArea: true, // only want the grid lines for one axis to show up
                                //tickMarkLength: 10,
                                offsetGridLines: true
                            },
                            ticks:{
                                fontColor: labelColor,
                                display:axisLabelHeader,
                                fontSize: _chart._xfontSize,
                                maxRotation: 0,
                                minRotation: 0,
                                fontFamily: "sans-serif",
                                callback:function(label,index,labels){
                                    label=label.toString();
                                    if(dualXSkipLabel[_chart._data.dualXLabel[label]]==undefined){
                                        dualXSkipLabel[_chart._data.dualXLabel[label]]=index;
                                    }
                                    if(dualXSkipLabel[_chart._data.dualXLabel[label]]==index){
                                        return _chart._data.dualXLabel[label];
                                    }
                                    return "";
                                }
                            }}];
                    }else{
                        return [{
                            id:'xAxis1',
                            type:"category",
                            scales: {
                                xAxes: [{
                                    stacked: false,
                                }],
                                yAxes: [{
                                    stacked: false
                                }]
                            },
                            stacked: false,
                            scaleLabel: {
                                display: scaleLabels,
                                labelString: dimensionValue
                            },
                            gridLines: {
                                color: axisLineColor,
                                display: axisLabelHeader,
                            },
                            ticks:{
                                fontColor: labelColor,
                                display:axisLabelHeader,
                                fontSize: _chart._xfontSize,
                                autoSkip: autoSkip,
                                maxRotation: axisHeaderRotate,
                                minRotation: axisHeaderRotate,
                                maxTicksLimit: allAxisHeader,
                                fontFamily: "sans-serif",
                                callback:function(label){
                                    label=label.toString();
                                    //return label;
                                    return label.slice(0, 15) +(label.length > 15 ? "..." : "");
                                }
                            }
                        },
                        {
                            id:'xAxis2',
                            type:"category",
                            display:dualXAxisFlag,
                            scales: {
                                xAxes: [{
                                    stacked: false,
                                }],
                                yAxes: [{
                                    stacked: false
                                }]
                            },
                            stacked: false,
                            position: 'top',
                            scaleLabel: {
                                display: dualXaxisLabel,
                                labelString: dualXaxisLabelVal
                            },
                            gridLines: {
                                color: axisLineColor,
                                display:axisLabelHeader,
                                drawOnChartArea: true, // only want the grid lines for one axis to show up
                                //tickMarkLength: 10,
                                offsetGridLines: true
                            },
                            ticks:{
                                fontColor: labelColor,
                                display:axisLabelHeader,
                                fontSize: _chart._xfontSize,
                                maxRotation: 0,
                                minRotation: 0,
                                fontFamily: "sans-serif",
                                callback:function(label,index,labels){
                                    label=label.toString();
                                    if(dualXSkipLabel[_chart._data.dualXLabel[label]]==undefined){
                                        dualXSkipLabel[_chart._data.dualXLabel[label]]=index;
                                    }
                                    if(dualXSkipLabel[_chart._data.dualXLabel[label]]==index){
                                        return _chart._data.dualXLabel[label];
                                    }
                                    return "";
                                }
                            }}];
                    }

                }
            };

            _chart.dataType = function (type) {
                _chart._dataType = type;
            };

            _chart.fontSize = function (fontSize) {
                _chart._fontSize = fontSize
            };

            _chart.fontFamily = function (fontFamily) {
                _chart._fontFamily = fontFamily;
            }

            _chart.yAxisPrefix = function (yAxis) {
                if (yAxis) {
                    _chart._yAxisPrefix = yAxis;
                } else {
                    _chart._yAxisPrefix = "";
                }

            }
            _chart.setDataFormat = function () {
                /* y axis font size
                */
                if (_chart._dataFormateVal.yaxis && _chart._dataFormateVal.yaxis.fontSize) {
                    _chart.yfontSize(_chart._dataFormateVal.yaxis.fontSize);
                } else {
                    _chart.yfontSize(12);
                }
                /*
                 * x axis font size
                 */
                if (_chart._dataFormateVal.xaxis && _chart._dataFormateVal.xaxis.fontSize) {
                    _chart.xfontSize(_chart._dataFormateVal.xaxis.fontSize);
                } else {
                    _chart.xfontSize(12);
                }

                if(_chart._dataFormateVal.topN){
                    _chart.topN = _chart._dataFormateVal.topN;
                    _chart.topnMeasure = _chart._dataFormateVal.topnMeasure;
                    _chart.topNOther = _chart._dataFormateVal.topNOther;
                }
                if(_chart._dataFormateVal.sort){
                    _chart._sort = _chart._dataFormateVal.sort;
                    _chart._sortMeasure= _chart._dataFormateVal.sortMeasure;
                }
                if(_chart._dataFormateVal.runningTotal!=undefined){
                    _chart._runningTotal=_chart._dataFormateVal.runningTotal;
                }
                _chart.groupColor = _chart._dataFormateVal.groupColor;
                _chart._dataType = _chart._dataFormateVal.dataType;
            }

            _chart.xfontSize = function (fontSize) {
                _chart._xfontSize = fontSize
            };

            _chart.yfontSize = function (fontSize) {
                _chart._yfontSize = fontSize
            };

            //---------------------------Group Color Start--------------------
            _chart.getDatasetByGroupColor = function (finalData, groupKey,stackIndex) {
                var groupMappings = _chart.createGroupMappings(finalData, _chart);
                var initialData = [];
                _chart._labels.forEach(function (d, i) {
                    initialData[i] = null;
                });
                var p = 0;
                var datasets = [];
                _chart.groupColorDataOrder = [];
                $.each(groupMappings, function (key, value) {
                    var data = Object.assign([], initialData);
                    $.each(value, function (k, v) {
                        var labelIndexMatched = _chart._labels.indexOf(k);
                        var grpColorDataOrder = {};
                        var pushFlag = true;
                        grpColorDataOrder['order'] = labelIndexMatched;
                        grpColorDataOrder['key'] = key;
                        grpColorDataOrder['label'] = k;
                        grpColorDataOrder['val'] = v;
                        _chart.groupColorDataOrder.forEach(function(d,i){
                            if(labelIndexMatched == d.order){
                                pushFlag = false;
                            }
                        });
                        if(pushFlag){
                            _chart.groupColorDataOrder.push(grpColorDataOrder);
                        }
                        if (labelIndexMatched != -1) {
                            data[labelIndexMatched] = v;
                        }
                    });
                    var dt = {
                        measure:groupKey,
                        label: key,
                        data: data,
                        stack: 'Stack '+stackIndex,
                        backgroundColor: Array(data.length).fill(_chart._colorSelection[p % (_chart._colorSelection.length)]),
                        borderWidth: 1
                    };
                    // _chart._labels=labels
                    datasets.push(dt);
                    p++;
                });
                return datasets;
            }

            _chart.updateDatasetByGroupColor = function (finalData, groupKey,stackIndex) {
                var groupMappings = _chart.createGroupMappings(finalData, _chart);
                var initialData = [];
                _chart._data.labels.forEach(function (d, i) {
                    initialData[i] = null;
                });
                var labelsList = {};
                $.each(groupMappings, function (key, value) {
                    $.each(value, function (k, v) {
                        if (!(v == 0 || v < 0 || isNaN(v))) {
                            labelsList[k] = 1;
                        }
                    });
                });
                var labelsListOrdered = {};
                Object.keys(labelsList).forEach(function(key) {
                    labelsListOrdered[key] = labelsList[key];
                });
                var initialData = [];
                var indexData = 0;
                var labelsListArray = [];
                $.each(labelsListOrdered, function (key, value) {
                    initialData[indexData] = null;
                    labelsListArray.push(key);
                    indexData++;
                });

                var p = 0;
                var datasets = [];
                $.each(groupMappings, function (key, value) {
                    var data = Object.assign([], initialData);
                    $.each(value, function (k, v) {
                        var labelIndexMatched = _chart._data.labels.indexOf(k);
                        if (labelIndexMatched != -1) {
                            if (!(v <= 0))
                                data[labelIndexMatched] = v;
                        }
                    });
                    var dt = {
                        measure:groupKey,
                        label: key,
                        data: data,
                        stack: 'Stack '+stackIndex,
                        backgroundColor: Array(data.length).fill(_chart._colorSelection[p % (_chart._colorSelection.length)]),
                        borderWidth: 1
                    };
                    //     _chart._labels=labels
                    datasets.push(dt);
                    p++;
                });

                return datasets;
            }

            //---------------------------Group Color End-----------------------


            _chart.createGroupMappings = function (finalData, _chart) {
                _chart._groupMappings = eChart.createGroupMappings(finalData, _chart);
                return _chart._groupMappings;
            }
            _chart.getDataByGroup = function () {
                var groups = _chart._group;
                var datasets = [];
                var multipleMeasure=[];
                var stackIndex=0;
                var j=0;
                var dualXLabel={};
                $.each(groups, function (key, grpObj) {
                    var finalData=grpObj;
                    if (eChart.isDataFormatApplied(_chart) && !_chart._sort && !_chart.topN) {
                        finalData = eChart.getDataByDateFormat(grpObj, key, _chart);
                    }
                    if(j==0)
                        grpObj.forEach(function (d) {
                            dualXLabel[d.key]=d.dualXKey;
                        });
                    datasets = _chart.getDatasetByGroupColor(finalData, key,stackIndex);
                    stackIndex++;
                    datasets.forEach(function (d) {
                        multipleMeasure.push(d);
                    });
                    j++;
                });
                var dataInFormat = {
                    labels: _chart._labels,
                    dualXLabel:dualXLabel,
                    datasets: multipleMeasure
                };
                return dataInFormat;
            }
            _chart.getNormalData = function () {
                var colorsLength = eChart.colors.length;
                var groups = _chart._group;
                var labels = [];
                var datasets = [];
                var i = 0;
                var j = 0;
                var index = 0;
                var dualXLabel={};
                _chart._dataSetLabels=[];
                $.each(groups, function (key, grpObj) {
                    var data = [];
                    var backgroundColor = [];
                    var borderColor = [];
                    var hoverBorderColor = [];
                    var finalData=grpObj;
                    if (eChart.isDataFormatApplied(_chart) && !_chart._sort && !_chart.topN) {
                        finalData = eChart.getDataByDateFormat(grpObj, key, _chart);
                    }
                    if(_chart._runningTotal && _chart._runningTotal[key]){
                        finalData=eChart.getRunTotal(finalData,key,_chart);
                    }
                    _chart._dataSetLabels.push(key);
                    finalData.forEach(function (d) {
                        if (j === 0) {
                            dualXLabel[d.key] = d.dualXKey;
                            labels.push(d.key);
                        }
                        if (_chart._operator && d.value!=null && d.value[_chart._operator[key].key]) {

                            data.push(d.value[_chart._operator[key].key]);
                        } else {
                            data.push(d.value);
                        }
                        backgroundColor.push(_chart._colorSelection[j]);
                        hoverBorderColor.push("#000");
                        i++;
                    });
                    var dt = {
                        label: key,
                        data: data,
                        backgroundColor: backgroundColor,
                        borderColor: backgroundColor,
                        /*hoverBorderColor: hoverBorderColor,*/
                        borderWidth: 1
                    };
                    datasets.push(dt);
                    j++;
                });
                var dataInFormat = {
                    labels: labels,
                    datasets: datasets,
                    dualXLabel:dualXLabel
                };
                return dataInFormat;
            }
            _chart.getDataInFormat = function (order) {
                eChart.updateAttributes(_chart);
                var dataInFormat = {};
                if (eChart.isGroupColorApplied(_chart)) {
                    dataInFormat = _chart.getDataByGroup();
                } else {
                    dataInFormat = _chart.getNormalData();
                }
                try {
                    if (_chart._dataFormateVal && _chart._dataFormateVal.tooltipSelector) {
                        eChart.processTooltipData(_chart);
                    }
                } catch (e) {

                }
                return dataInFormat;
            }

            _chart.data = function (data) {
                _chart._data = _chart.getDataInFormat();
                return _chart;
            };

            _chart.render = function () {
                eChart.clearContainer(id);
                var bar = eChart.getCanvasContext(_chart, function (context) {
                    _chart.draw(context);
                    _chart.bindEvents();
                });
                sketchServer.reportLoadedCountCheck(_chart);
            };


            _chart.draw = function (context) {
                var instance = new Chart(context, {
                    type: 'bar',
                    data: _chart._data,
                    options: eChart.createOption(_chart)
                });
                var res = id.split("-");
                _chart.chartInstance = instance;
                eChart.generateLegend("left" + res[1], _chart);
                _chart['tempGroupKey'] = _chart._group;
                eChart.registerChart(_chart);
                sketchServer.globalRegistry.register(_chart);
                sketchServer.globalRegistry.syncLocalRegistries(_chart.id,"eChart");
            };

            _chart.operator = function (operator) {
                _chart._operator = operator;
                return _chart;
            };

            _chart.bindEvents = function () {
                eChart.bindEvents(this);
            };

            _chart.highlightSelectedFilter = function () {
                var labels = this.getLabels();
                var labelsLength = labels.length;
                var colorsLength = eChart.colors.length;
                var chartFiltersLength = 0;
                if (!_chart.reset) {
                    chartFiltersLength = this.filters.length;
                }

                if (labelsLength == chartFiltersLength || chartFiltersLength === 0) {
                    _chart._data.datasets.forEach(function (dataset, dsIndex) {
                        labels.forEach(function (filter, i) {
                            _chart._data.datasets[dsIndex].backgroundColor[i] = _chart._colorSelection[dsIndex % colorsLength];
                        });
                    });
                    _chart.filters = [];
                } else {
                    var datasetsArray = [];
                    var datasetWiseFilters = {};

                    _chart.filters.forEach(function (filter) {
                        if (!datasetWiseFilters[filter.datasetIndex])
                            datasetWiseFilters[filter.datasetIndex] = [];
                        datasetWiseFilters[filter.datasetIndex].push(filter.label);
                    });

                    _chart._data.datasets.forEach(function (dataset, dsIndex) {
                        var filterLabelsArray = datasetWiseFilters[dsIndex];
                        if (filterLabelsArray) {
                            labels.forEach(function (label, index) {
                                if (filterLabelsArray.indexOf(label) == -1) {
                                    dataset.backgroundColor[index] = "#ddd";
                                } else {
                                    dataset.backgroundColor[index] = _chart._colorSelection[dsIndex % colorsLength];
                                }
                            });
                        } else {
                            dataset.backgroundColor = Array(labels.length).fill("#ddd");
                        }
                    });
                }
            };

            _chart.colorChange = function (selectedDataSet, color) {
                var labels = this.getLabels();
                _chart._data.datasets.forEach(function (dataset, dsIndex) {
                    if (dataset.label == selectedDataSet.datasetLabel) {
                        _chart._colorSelection[dsIndex] = color;
                        var tempData = [];
                        labels.forEach(function (filter, i) {
                            tempData.push(color);
                        });
                        _chart._data.datasets[dsIndex].backgroundColor = tempData;
                        _chart._data.datasets[dsIndex].borderColor = tempData;
                    }
                });
                setTimeout(function () {
                    _chart.chartInstance.update();
                    eChart.registerColor(_chart);
                    var res = _chart.id.split("-");
                    eChart.generateLegend("left" + res[1], _chart);
                }, 1);
            };

            _chart.getClickedElementLabel = function (evt) {
                var filter = {};
                try {
                    var eveObj=this.chartInstance.getElementAtEvent(evt)[0];
                    filter.measure = eveObj._chart.config.data.datasets[eveObj._datasetIndex].measure;
                    filter.label = this.chartInstance.getElementAtEvent(evt)[0]._model.label;
                    filter.datasetLabel = this.chartInstance.getElementAtEvent(evt)[0]._model.datasetLabel;
                    filter.datasetIndex = this.chartInstance.getElementAtEvent(evt)[0]._datasetIndex;
                    return filter;
                } catch (e) {
                    return null;
                }
            };

            _chart.getLabels = function () {
                return this.chartInstance.config.data.labels;
            };

            return _chart;
        };
        // ------------------------------------Composite Chart END------------------------------------------------------------





        // -----------------------------------Stacked START-------------------------------------------------------------
        eChart.stackedChart = function (id) {
            var _chart = {};
            _chart.id = id;
            _chart.dataBackup = [];

            _chart.group = function (group) {
                _chart._group = group;
                _chart.data();
                return _chart;
            };

            _chart.measures=function(measures){
                var measure={};
                $.each(measures,function(key,val){
                    measure[key]=val;
                });
                _chart._measures = measure;
                return _chart;
            };
            _chart.dimension = function (dimension) {
                _chart._dimension = dimension;
                return _chart;
            }

            _chart.data = function (data) {
                _chart._data = _chart.getDataInFormat();
                return _chart;
            };

            _chart.operator = function (operator) {
                _chart._operator = operator;
                return _chart;
            }

            _chart.render = function () {
                eChart.clearContainer(id);
                var bar = eChart.getCanvasContext(_chart, function (context) {
                    _chart.draw(context);
                    _chart.bindEvents();
                });
                sketchServer.reportLoadedCountCheck(_chart);
                return true;
            };

            _chart.draw = function (context) {
                var instance = new Chart(context, {
                    type: 'bar',
                    data: _chart._data,
                    options: {
                        scales: {
                            xAxes: [{
                                stacked: true
                            }],
                            yAxes: [{
                                stacked: true,
                                ticks: {
                                    beginAtZero: true
                                }
                            }],
                        },
                        animation: {},
                        pan: {
                            // Boolean to enable panning
                            enabled: false,

                            // Panning directions. Remove the appropriate direction to disable
                            // Eg. 'y' would only allow panning in the y direction
                            mode: 'xy'
                        },
                        // Container for zoom options
                        zoom: {
                            // Boolean to enable zooming
                            enabled: false,

                            // Zooming directions. Remove the appropriate direction to disable
                            // Eg. 'y' would only allow zooming in the y direction
                            mode: 'x',
                        }
                    }
                });
                _chart.chartInstance = instance;
                _chart['tempGroupKey'] = _chart._group;
                eChart.registerChart(_chart);
                sketchServer.globalRegistry.register(_chart);
                sketchServer.globalRegistry.syncLocalRegistries(_chart.id,"eChart");
            };

            _chart.bindEvents = function () {
                eChart.bindEvents(this);
            };

            _chart.getClickedElementLabel = function (evt) {
                return this.chartInstance.getElementAtEvent(evt)[0]._model.label;
            };

            _chart.getLabels = function () {
                return this.chartInstance.config.data.labels;
            };

            _chart.dataSetLabels = function (labels) {
                _chart._dataSetLabels = labels;
                return _chart;
            };

            _chart.getDataInFormat = function () {
                var colorsLength = eChart.colors.length;
                var groups = _chart._group;
                var labels = [];
                var datasets = [];
                var i = 0;
                var j = 0;
                eChart.updateAttributes(_chart);
                groups.forEach(function (grpObj, index) {
                    var data = [];
                    var backgroundColor = [];
                    grpObj.forEach(function (d) {
                        if (index == 0)
                            labels.push(d.key);
                        if (_chart._operator) {
                            data.push(d.value);
                        } else {
                            data.push(d.value);
                        }
                        backgroundColor.push(eChart.colors[j]);
                        i++;
                    });
                    /* label : _chart._dataSetLabels[j].value, */
                    var dt = {
                        label: _chart._dataSetLabels[j].value,
                        data: data,
                        backgroundColor: backgroundColor
                    };
                    datasets.push(dt);
                    j++;
                });
                var dataInFormat = {
                    labels: labels,
                    datasets: datasets
                };

                return dataInFormat;
            }
            return _chart;
        };
        // ------------------------------------Stacked END------------------------------------------------------------
        // ------------------------------------Heat map-------------------------------------------------------------
        eChart.heatChart = function (id) {

            var data = {
                labels: ['0h', '1h', '2h', '3h', '4h', '5h', '6h', '7h', '8h', '9h', '10h', '11h'],
                datasets: [
                    {
                        label: 'Monday',
                        data: [8, 6, 5, 7, 9, 8, 1, 6, 3, 3, 8, 7]
                    },
                    {
                        label: 'Tuesday',
                        data: [6, 8, 5, 6, 5, 5, 7, 0, 0, 3, 0, 7]
                    },
                    {
                        label: 'Wednesday',
                        data: [8, 5, 6, 4, 2, 2, 3, 0, 2, 0, 10, 8]
                    },
                    {
                        label: 'Thursday',
                        data: [4, 0, 7, 4, 6, 3, 2, 4, 2, 10, 8, 2]
                    },
                    {
                        label: 'Friday',
                        data: [1, 0, 0, 7, 0, 4, 1, 3, 4, 5, 1, 10]
                    }
                ]
            };

            $("<canvas id='canvas-1' ></canvas>").appendTo($("#chart-" + id));
            var ctx = document.getElementById("canvas-" + id).getContext('2d');
            var newChart = new Chart(ctx).HeatMap(data, "");
        };

        // ------------------------------------Heat map end-----------------------------------------------------------




        // ------------------------------------Line Bar Chart Start---------------------------------------------------

        eChart.lineBarChart = function (id) {
            var _chart = {};
            _chart.id = id;
            _chart.type = 'lineBarChart';
            _chart._sort = null;

            _chart._colorSelection = $.extend([], eChart.colors);

            _chart.tKey = function (totalKeys) {
                _chart.totalKey = totalKeys;
                return _chart;
            };

            _chart.group = function (group) {
                _chart._group = group;
                _chart.data();
                return _chart;
            };

            _chart.dimension = function (dimension) {
                _chart._dimension = dimension;
                return _chart;
            };

            _chart.measures = function(measures){
                var measure={};
                $.each(measures,function(key,val){
                    measure[key]=val;
                });
                _chart._measures = measure;
                return _chart;
            };

            _chart.dataFormat = function (dataFormat) {
                _chart._dataFormateVal = dataFormat;
                _chart.setDataFormat();
                return _chart;
            };

            _chart.dataSetLabels = function (labels) {
                _chart._dataSetLabels = labels;
                return _chart;
            };

            _chart.sort = function (order) {
                _chart._sort = order;
                eChart.registerDataOrder(_chart);
                _chart._metadataId=eChart._metadataId;
                sketchServer._lineBarChartJs(_chart);
            };

            _chart.xAxes = function () {
                var labelColor, axisLineColor;
                if(_chart._dataFormateVal && _chart._dataFormateVal.axisLabelColor){
                    labelColor = _chart._dataFormateVal.axisLabelColor;
                }else{
                    labelColor = '#666666';
                }
                if(_chart._dataFormateVal && _chart._dataFormateVal.axisLabelColor){
                    axisLineColor = _chart._dataFormateVal.axisColor;
                }else{
                    axisLineColor = '#e6e6e6';
                }

                /*
                 * scale labels
                 */
                var scaleLabels=false;
                var axisLabelHeader=true;
                var allAxisHeader=false;
                var axisHeaderRotate=90;
                var autoSkip=false;
                if(_chart._dataFormateVal && _chart._dataFormateVal.xaxis){
                    if(_chart._dataFormateVal.xaxis.axisLabel){
                        scaleLabels=_chart._dataFormateVal.xaxis.axisLabel;
                    }
                    /*
                     * Axis label option
                     */
                    if(_chart._dataFormateVal.yaxis.axisHeader!=undefined){
                        axisLabelHeader=_chart._dataFormateVal.yaxis.axisHeader;
                    }
                    /*
                     * All axis label
                     */
                    if(_chart._dataFormateVal.xaxis.allAxisHeader!=undefined){
                        allAxisHeader=_chart._dataFormateVal.xaxis.allAxisHeader;
                        if(allAxisHeader==true){
                            allAxisHeader=false;
                            autoSkip=false;
                        }else{
                            allAxisHeader=10;
                            autoSkip=true;
                        }
                    }
                    /*
                     * Rotate label
                     */
                    if(_chart._dataFormateVal.xaxis.axisHeaderRotate!=undefined){
                        axisHeaderRotate=_chart._dataFormateVal.xaxis.axisHeaderRotate;
                    }
                }
                /*
                 *  dimension value
                 */
                var dimensionValue="";
                if(_chart._dimension){
                    dimensionValue=Object.keys(_chart._dimension)[0];
                }
                if(/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
                    _chart._xfontSize=30;
                }
                var dualXaxisLabel=false;
                var dualXaxisLabelVal="";
                if(_chart._dataFormateVal && _chart._dataFormateVal.xaxis && _chart._dataFormateVal.xaxis.dualXaxisLabel){
                    dualXaxisLabel=_chart._dataFormateVal.xaxis.dualXaxisLabel;
                    dualXaxisLabelVal=JSON.parse(_chart._dataFormateVal.xaxis.dualXDimension).reName;

                }
                /*
                 * Dual x axis flag
                 */
                var dualXAxisFlag=false;
                if(_chart._dataFormateVal && _chart._dataFormateVal.xaxis && _chart._dataFormateVal.xaxis.dualX && _chart._dataFormateVal.xaxis.dualX=='yes'){
                    dualXAxisFlag=true;
                }
                var dualXSkipLabel={};
                if ((_chart._dataType == "date" || _chart._fontSize == "datetime") && _chart._dataFormateVal.xAxis == undefined) {
                    return [{
                        id:'xAxis1',
                        type:"category",
                        stacked: true,
                        scaleLabel: {
                            display: scaleLabels,
                            labelString: dimensionValue
                        },
                        gridLines: {
                            color: axisLineColor,
                            display: axisLabelHeader
                        },
                        ticks: {
                            fontColor: labelColor,
                            display:axisLabelHeader,
                            fontSize: _chart._xfontSize,
                            autoSkip: autoSkip,
                            maxRotation: axisHeaderRotate,
                            minRotation: axisHeaderRotate,
                            maxTicksLimit: allAxisHeader,
                            fontFamily: "sans-serif",
                            callback:function(label){
                                label=label.toString();
                                //return label;
                                return label.slice(0, 15) +(label.length > 15 ? "..." : "");
                            }
                        },
                        type: 'time',
                        unit: 'day',
                        unitStepSize: 1,
                        time: {
                            displayFormats: {
                                'day': 'MMM DD'
                            }
                        }
                    },
                    {
                        id:'xAxis2',
                        type:"category",
                        stacked: true,
                        display:dualXAxisFlag,
                        position: 'top',
                        scaleLabel: {
                            display: dualXaxisLabel,
                            labelString: dualXaxisLabelVal
                        },
                        gridLines: {
                            color: axisLineColor,
                            display:axisLabelHeader,
                            drawOnChartArea: true, // only want the grid lines for one axis to show up
                            //tickMarkLength: 10,
                            offsetGridLines: true
                        },
                        ticks:{
                            fontColor: labelColor,
                            display:axisLabelHeader,
                            fontSize: _chart._xfontSize,
                            maxRotation: 0,
                            minRotation: 0,
                            fontFamily: "sans-serif",
                            callback:function(label,index,labels){
                                label=label.toString();
                                if(dualXSkipLabel[_chart._data.dualXLabel[label]]==undefined){
                                    dualXSkipLabel[_chart._data.dualXLabel[label]]=index;
                                }
                                if(dualXSkipLabel[_chart._data.dualXLabel[label]]==index){
                                    return _chart._data.dualXLabel[label];
                                }
                                return "";
                            }
                        },
                        type: 'time',
                        unit: 'day',
                        unitStepSize: 1,
                        time: {
                            displayFormats: {
                                'day': 'MMM DD'
                            }
                        }}];
                } else {
                    return [{
                        id:'xAxis1',
                        type:"category",
                        stacked: true,
                        scaleLabel: {
                            display: scaleLabels,
                            labelString: dimensionValue
                        },
                        gridLines: {
                            color: axisLineColor,
                            display: axisLabelHeader
                        },
                        ticks:{
                            fontColor: labelColor,
                            display:axisLabelHeader,
                            fontSize: _chart._xfontSize,
                            autoSkip: autoSkip,
                            maxTicksLimit: allAxisHeader,
                            maxRotation: axisHeaderRotate,
                            minRotation: axisHeaderRotate,
                            fontFamily: "sans-serif",
                            callback:function(label){
                                label=label.toString();
                                //return label;
                                return label.slice(0, 15) +(label.length > 15 ? "..." : "");
                            }
                        }
                    },
                    {
                        id:'xAxis2',
                        type:"category",
                        stacked: true,
                        position: 'top',
                        display:dualXAxisFlag,
                        scaleLabel: {
                            display: dualXaxisLabel,
                            labelString: dualXaxisLabelVal
                        },
                        gridLines: {
                            color: axisLineColor,
                            display:axisLabelHeader,
                            drawOnChartArea: true, // only want the grid lines for one axis to show up
                            //tickMarkLength: 10,
                            offsetGridLines: true
                        },
                        ticks:{
                            fontColor: labelColor,
                            display:axisLabelHeader,
                            fontSize: _chart._xfontSize,
                            maxRotation: 0,
                            minRotation: 0,
                            fontFamily: "sans-serif",
                            callback:function(label,index,labels){
                                label=label.toString();
                                if(dualXSkipLabel[_chart._data.dualXLabel[label]]==undefined){
                                    dualXSkipLabel[_chart._data.dualXLabel[label]]=index;
                                }
                                if(dualXSkipLabel[_chart._data.dualXLabel[label]]==index){
                                    return _chart._data.dualXLabel[label];
                                }
                                return "";
                            }
                        }}];
                }
                //return eChart.renderxAxes(_chart._dataType,_chart._fontSize);
            };

            _chart.yAxes=function(chart){
                return eChart.renderyAxes(_chart._dataFormateVal, chart);
            }

            _chart.dataType = function (type) {
                _chart._dataType = type;
            };

            _chart.fontSize = function (fontSize) {
                _chart._fontSize = fontSize
            };

            _chart.fontFamily = function (fontFamily) {
                _chart._fontFamily = fontFamily;
            }

            _chart.yAxisPrefix = function (yAxis) {
                if (yAxis) {
                    _chart._yAxisPrefix = yAxis;
                } else {
                    _chart._yAxisPrefix = "";
                }
            }

            _chart.setDataFormat = function () {
                /* y axis font size
                */
                if (_chart._dataFormateVal.yaxis && _chart._dataFormateVal.yaxis.fontSize) {
                    _chart.yfontSize(_chart._dataFormateVal.yaxis.fontSize);
                } else {
                    _chart.yfontSize(12);
                }
                /*
                 * x axis font size
                 */
                if (_chart._dataFormateVal.xaxis && _chart._dataFormateVal.xaxis.fontSize) {
                    _chart.xfontSize(_chart._dataFormateVal.xaxis.fontSize);
                } else {
                    _chart.xfontSize(12);
                }

                if(_chart._dataFormateVal.topN){
                    _chart.topN = _chart._dataFormateVal.topN;
                    _chart.topnMeasure = _chart._dataFormateVal.topnMeasure;
                    _chart.topNOther = _chart._dataFormateVal.topNOther;
                }
                if(_chart._dataFormateVal.sort){
                    _chart._sort = _chart._dataFormateVal.sort;
                    _chart._sortMeasure = _chart._dataFormateVal.sortMeasure;
                }
                if(_chart._dataFormateVal.runningTotal!=undefined){
                    _chart._runningTotal=_chart._dataFormateVal.runningTotal;
                }
                _chart.groupColor = _chart._dataFormateVal.groupColor;
                _chart._dataType = _chart._dataFormateVal.dataType;
                _chart._chartType = _chart._dataFormateVal.chartType;
            }
            _chart.xfontSize = function (fontSize) {
                _chart._xfontSize = fontSize
            };
            _chart.yfontSize = function (fontSize) {
                _chart._yfontSize = fontSize
            };

            //---------------------------Group Color Start--------------------
            _chart.getDatasetByGroupColor = function (finalData, groupKey, type) {
                var groupMappings = _chart.createGroupMappings(finalData, _chart);
                var initialData = [];
                _chart._labels.forEach(function (d, i) {
                    initialData[i] = null;
                });
                var p = 0;
                var datasets = [];
                $.each(groupMappings, function (key, value) {
                    var data = Object.assign([], initialData);
                    $.each(value, function (k, v) {
                        var labelIndexMatched = _chart._labels.indexOf(k);
                        if (labelIndexMatched != -1) {
                            data[labelIndexMatched] = v;
                        }
                    });
                    var backgroundColor;
                    if (type.type == "line") {
                        backgroundColor = _chart._colorSelection[p % (_chart._colorSelection.length)];
                    }else{
                        backgroundColor = Array(data.length).fill(_chart._colorSelection[p % (_chart._colorSelection.length)]);
                    }
                    var dt = {
                        measure:groupKey,
                        type:type.type,
                        fill : false,
                        label: key,
                        data: data,
                        backgroundColor: backgroundColor,
                        borderWidth: 1
                    };
                    // _chart._labels=labels
                    datasets.push(dt);
                    p++;
                });
                return datasets;
            }

            _chart.updateDatasetByGroupColor = function (finalData, groupKey,type) {
                var groupMappings = _chart.createGroupMappings(finalData, _chart);
                var labelsList = {};
                $.each(groupMappings, function (key, value) {
                    $.each(value, function (k, v) {
                        if (!(v == 0 || v < 0 || isNaN(v))) {
                            labelsList[k] = 1;
                        }
                    });
                });
                var labelsListOrdered = {};
                Object.keys(labelsList).forEach(function(key) {
                    labelsListOrdered[key] = labelsList[key];
                });
                var initialData = [];
                var indexData = 0;
                var labelsListArray = [];
                $.each(labelsListOrdered, function (key, value) {
                    initialData[indexData] = null;
                    labelsListArray.push(key);
                    indexData++;
                });
                var p = 0;
                var datasets = [];
                $.each(groupMappings, function (key, value) {
                    var data = Object.assign([], initialData);
                    $.each(value, function (k, v) {
                        var labelIndexMatched = _chart._data.labels.indexOf(k);
                        if (labelIndexMatched != -1) {
                            if (!(v <= 0))
                                data[labelIndexMatched] = v;
                        }
                    });
                    var backgroundColor;
                    if (type.type == "line") {
                        backgroundColor = _chart._colorSelection[p % (_chart._colorSelection.length)];
                    }else{
                        backgroundColor=Array(data.length).fill(_chart._colorSelection[p % (_chart._colorSelection.length)]);
                    }
                    var dt = {
                        measure:groupKey,
                        type:type.type,
                        fill : false,
                        label: key,
                        data: data,
                        backgroundColor: backgroundColor,
                        borderWidth: 1
                    };
                    // _chart._labels=labels
                    datasets.push(dt);
                    p++;
                });
                return datasets;
            }

            //---------------------------Group Color End-----------------------



            _chart.createGroupMappings = function (finalData, _chart) {
                _chart._groupMappings = eChart.createGroupMappings(finalData, _chart);
                return _chart._groupMappings;
            }

            _chart.getDataByGroup = function () {
                var groups = _chart._group;
                var datasets = [];
                var mulitpleMeasure=[];
                var j=0;
                var dualXLabel={};
                $.each(groups, function (key, grpObj) {
                    if(_chart._dataFormateVal && _chart._dataFormateVal.chartType && _chart._dataFormateVal.chartType[key] && _chart._dataFormateVal.chartType[key].sort!=undefined){
                        _chart._sort=_chart._dataFormateVal.chartType[key].sort;
                    }
                    var finalData=grpObj;
                    if (eChart.isDataFormatApplied(_chart) && !_chart._sort && !_chart.topN) {
                        finalData = eChart.getDataByDateFormat(grpObj, key, _chart);
                    }

                    if(j==0)
                        grpObj.forEach(function (d) {
                            dualXLabel[d.key]=d.dualXKey;
                        });
                    var type;
                    if (_chart._chartType) {
                        if (_chart._chartType[key]) {
                            type = _chart._chartType[key];
                        } else {
                            var types = ['line', 'bar']
                            type = types[(Math.random() * types.length) | 0];
                        }
                    } else {
                        var types = ['line', 'bar']
                        type = types[(Math.random() * types.length) | 0];
                    }

                    datasets = _chart.getDatasetByGroupColor(finalData, key,type);
                    datasets.forEach(function (d) {
                        mulitpleMeasure.push(d);
                    });
                    j++;
                });
                var dataInFormat = {
                    labels: _chart._labels,
                    dualXLabel:dualXLabel,
                    datasets: mulitpleMeasure
                };
                return dataInFormat;
            }

            _chart.getNormalData = function () {
                //  var colorsLength = eChart.colors.length;
                var groups = _chart._group;
                var labels = [];
                var datasets = [];
                var i = 0;
                var j = 0;
                var dualXLabel={};
                _chart._dataSetLabels=[];
                $.each(groups, function (key, grpObj) {
                    if(_chart._dataFormateVal && _chart._dataFormateVal.chartType && _chart._dataFormateVal.chartType[key] && _chart._dataFormateVal.chartType[key].sort){
                        _chart._sort=_chart._dataFormateVal.chartType[key].sort;
                    }
                    var data = [];
                    var backgroundColor = [];
                    //  var borderColor = [];
                    var hoverBorderColor = [];
                    var finalData=grpObj;

                    if (eChart.isDataFormatApplied(_chart) && !_chart._sort && !_chart.topN) {
                        finalData = eChart.getDataByDateFormat(grpObj, key, _chart);
                    }
                    if(_chart._runningTotal && _chart._runningTotal[key]){
                        finalData=eChart.getRunTotal(finalData,key,_chart);
                    }
                    _chart._dataSetLabels.push(key);
                    finalData.forEach(function (d) {
                        if (j === 0){
                            dualXLabel[d.key]=d.dualXKey;
                            labels.push(d.key);
                        }
                        if (_chart._operator && d.value!=null && d.value[_chart._operator[key].key]) {
                            data.push(d.value[_chart._operator[key].key]);
                        } else {
                            data.push(d.value);
                        }
                        backgroundColor.push(_chart._colorSelection[j]);
                        hoverBorderColor.push("#000");
                        i++;
                    });
                    var type;
                    if (_chart._chartType) {
                        if (_chart._chartType[key]) {
                            type = _chart._chartType[key];
                        } else {
                            var types = ['line', 'bar']
                            type = types[(Math.random() * types.length) | 0];
                        }
                    } else {
                        var types = ['line', 'bar']
                        type = types[(Math.random() * types.length) | 0];
                    }

                    if (type.type == "line") {
                        backgroundColor = backgroundColor[j];
                    }
                    // if (type == "line") {
                    //     backgroundColor = backgroundColor[j];
                    // }

                    var dt = {
                        // type: type,
                        type: type.type,
                        fill: false,
                        label: _chart._dataSetLabels[j],
                        data: data,
                        backgroundColor: backgroundColor,
                        borderColor: backgroundColor,
                        hoverBorderColor: hoverBorderColor,
                        borderWidth: 1,
                    };
                    datasets.push(dt);
                    j++;
                });
                var dataInFormat = {
                    labels: labels,
                    datasets: datasets,
                    dualXLabel:dualXLabel
                };
                return dataInFormat;
            }

            _chart.getDataInFormat = function (order) {
                eChart.updateAttributes(_chart);
                var groups = _chart._group;
                var labels = [];
                var datasets = [];
                var i = 0;
                var j = 0;
                var dataInFormat = {};
                if (eChart.isGroupColorApplied(_chart)) {
                    dataInFormat = _chart.getDataByGroup();
                } else {
                    dataInFormat = _chart.getNormalData();
                }
                if (_chart._dataFormateVal && _chart._dataFormateVal.tooltipSelector) {
                    eChart.processTooltipData(_chart);
                }
                return dataInFormat;
                //}
            }

            _chart.data = function (data) {
                _chart._data = _chart.getDataInFormat();
                return _chart;
            };

            _chart.render = function () {
                eChart.clearContainer(id);
                var bar = eChart.getCanvasContext(_chart, function (context) {
                    _chart.draw(context);
                    _chart.bindEvents();
                });
                sketchServer.reportLoadedCountCheck(_chart);
            };

            _chart.draw = function (context) {
                var instance = new Chart(context, {
                    type: 'bar',
                    data: _chart._data,
                    options: eChart.createOption(_chart)
                });
                var res = id.split("-");
                _chart.chartInstance = instance;
                eChart.generateLegend("left" + res[1], _chart);
                _chart['tempGroupKey'] = _chart._group;
                eChart.registerChart(_chart);
                sketchServer.globalRegistry.register(_chart);
                sketchServer.globalRegistry.syncLocalRegistries(_chart.id,"eChart");
            };

            _chart.operator = function (operator) {
                _chart._operator = operator;
                return _chart;
            };

            _chart.bindEvents = function () {
                eChart.bindEvents(this);
            };

            /*_chart.highlightSelectedFilter = function () {
                var labels = this.getLabels();
                var labelsLength = labels.length;
                // var colorsLength = eChart.colors.length;
                var chartFiltersLength = 0;
                if (!_chart.reset) {
                    chartFiltersLength = this.filters.length;
                }
                if (labelsLength == chartFiltersLength || chartFiltersLength === 0) {
                    try {
                        _chart._data.datasets.forEach(function (dataset, dsIndex) {
                            labels.forEach(function (filter, i) {
                                if (_chart._data.datasets[dsIndex].type == 'bar') {
                                    _chart._data.datasets[dsIndex].backgroundColor[i] = _chart._colorSelection[dsIndex];
                                }
                            });
                        });
                    } catch (e) {

                    }
                    _chart.filters = [];
                } else {
                    labels.forEach(function (filter, i) {
                        var index = eChart.getIndexOfObj(_chart.filters, eChart.constants.LABEL, filter);
                        if (index != -1) {
                            var dsIndex = eChart.getIndexOfObj(_chart._data.datasets, eChart.constants.LABEL, _chart.filters[index].datasetLabel);
                            //  _chart._data.datasets[dsIndex].backgroundColor[i] = _chart._colorSelection[dsIndex];
                            _chart._data.datasets.forEach(function (dataset, dsIndex) {
                                if (dataset.type == 'bar') {
                                    _chart._data.datasets[dsIndex].backgroundColor[i] = _chart._colorSelection[dsIndex];
                                }
                            });
                        } else {
                            _chart._data.datasets.forEach(function (dataset, dsIndex) {
                                if (dataset.type == 'bar') {
                                    _chart._data.datasets[dsIndex].backgroundColor[i] = "#ddd";
                                }
                            });
                        }
                    });
                }
            };*/

            _chart.highlightSelectedFilter = function () {
                var labels = this.getLabels();
                var labelsLength = labels.length;
                // var colorsLength = eChart.colors.length;

                var chartFiltersLength = 0;
                if (!_chart.reset) {
                    chartFiltersLength = this.filters.length;
                }

                var pointRadiusNumber=2;
                if(/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
                    pointRadiusNumber=10;
                }

                if ((labelsLength == chartFiltersLength && !eChart.isGroupColorApplied(_chart)) || chartFiltersLength === 0) {
                    try {
                        _chart._data.datasets.forEach(function (dataset, dsIndex) {
                            labels.forEach(function (filter, i) {
                                if (_chart._data.datasets[dsIndex].type == 'bar') {
                                    _chart._data.datasets[dsIndex].backgroundColor[i] = _chart._colorSelection[dsIndex];
                                }

                            });
                        });
                    } catch (e) {

                    }
                    _chart.filters = [];
                } else {
                    if(eChart.isGroupColorApplied(_chart)){
                        var filterLabelsArray=[];
                        var datasetWiseFilters = {};
                        var grpArray=[];
                        Object.keys(_chart._groupMappings).forEach(function (ke) {
                            grpArray.push(ke);
                        })
                        _chart.filters.forEach(function (filter) {
                            if (!datasetWiseFilters[filter.datasetIndex])
                                datasetWiseFilters[filter.datasetIndex] = [];
                            datasetWiseFilters[filter.datasetIndex].push(filter.label);
                        });
                        labels.forEach(function (filter, i) {
                            var index = eChart.getIndexOfObj(_chart.filters, eChart.constants.LABEL, filter);
                            if (index != -1) {
                                //var dsIndex = eChart.getIndexOfObj(_chart._data.datasets, eChart.constants.LABEL, _chart.filters[index].datasetLabel);
                                _chart._data.datasets.forEach(function (dataset, chartDsIndex) {
                                    var filterLabelsArray = datasetWiseFilters[chartDsIndex];
                                    if (filterLabelsArray) {
                                        if (filterLabelsArray.indexOf(filter) == -1) {
                                            _chart._data.datasets[chartDsIndex].backgroundColor[i] = "#ddd";
                                        } else if (dataset.type == 'bar') {
                                            _chart._data.datasets[chartDsIndex].backgroundColor[i] = _chart._colorSelection[grpArray.indexOf(_chart._data.datasets[chartDsIndex].label)];
                                        }
                                    } else {
                                        if (dataset.type == 'bar') {
                                            _chart._data.datasets[chartDsIndex].backgroundColor[i] = "#ddd";
                                        }
                                    }
                                });
                            } else {
                                _chart._data.datasets.forEach(function (dataset, dsIndex) {
                                    if (dataset.type == 'bar') {
                                        _chart._data.datasets[dsIndex].backgroundColor[i] = "#ddd";
                                    }
                                });
                            }
                        });
                    }else{
                        labels.forEach(function (filter, i) {
                            var index = eChart.getIndexOfObj(_chart.filters, eChart.constants.LABEL, filter);
                            if (index != -1) {
                                var dsIndex = eChart.getIndexOfObj(_chart._data.datasets, eChart.constants.LABEL, _chart.filters[index].datasetLabel);
                                //  _chart._data.datasets[dsIndex].backgroundColor[i] = _chart._colorSelection[dsIndex];
                                _chart._data.datasets.forEach(function (dataset, dsIndex) {
                                    if (dataset.type == 'bar') {
                                        _chart._data.datasets[dsIndex].backgroundColor[i] = _chart._colorSelection[dsIndex];
                                    }
                                });
                            } else {
                                _chart._data.datasets.forEach(function (dataset, dsIndex) {
                                    if (dataset.type == 'bar') {
                                        _chart._data.datasets[dsIndex].backgroundColor[i] = "#ddd";
                                    }
                                });
                            }
                        });
                    }
                }
            };

            _chart.colorChange = function (selectedDataSet, color) {
                var labels = this.getLabels();
                _chart._data.datasets.forEach(function (dataset, dsIndex) {
                    if (dataset.label == selectedDataSet.datasetLabel) {
                        _chart._colorSelection[dsIndex] = color;
                        /*var tempData = [];
                        labels.forEach(function (filter, i) {
                            tempData.push(color);
                        });*/
                        _chart._data.datasets[dsIndex].backgroundColor = color;
                        _chart._data.datasets[dsIndex].borderColor = color;
                    }
                });
                setTimeout(function () {
                    _chart.chartInstance.update();
                    eChart.registerColor(_chart);
                    var res = _chart.id.split("-");
                    eChart.generateLegend("left" + res[1], _chart);
                }, 1);
            };

            _chart.getClickedElementLabel = function (evt) {
                var filter = {};
                try {
                    var eveObj=this.chartInstance.getElementAtEvent(evt)[0];
                    if(this.chartInstance.config.data.datasets[eveObj._datasetIndex].type=="line"){
                        filter.label = this.getLabels()[eveObj._index];
                        filter.datasetLabel = eveObj._chart.config.data.datasets[eveObj._datasetIndex].label;
                        filter.datasetIndex = eveObj._datasetIndex;
                        if(eveObj._chart.config.data.datasets[eveObj._datasetIndex] && eveObj._chart.config.data.datasets[eveObj._datasetIndex].measure);
                        filter.measure = eveObj._chart.config.data.datasets[eveObj._datasetIndex].measure;
                    }else{
                        filter.label = eveObj._model.label;
                        filter.datasetLabel = eveObj._model.datasetLabel;
                        filter.datasetIndex = eveObj._datasetIndex;
                        if(eveObj._chart.config.data.datasets[eveObj._datasetIndex] && eveObj._chart.config.data.datasets[eveObj._datasetIndex].measure);
                        filter.measure = eveObj._chart.config.data.datasets[eveObj._datasetIndex].measure;
                    }
                    return filter;
                } catch (e) {
                    return null;
                }
            };

            _chart.getLabels = function () {
                return this.chartInstance.config.data.labels;
            };
            return _chart;
        };
// ------------------------------------Line Bar Chart End---------------------------------------------------

// ------------------------------------Dual Axis Chart Start---------------------------------------------------

        eChart.dualAxisChart = function (id) {
            var _chart = {};

            _chart.id = id;
            _chart.type = 'dualAxisChart';
            _chart._sort = null;
            _chart._colorSelection = $.extend([], eChart.colors);

            _chart.tKey = function (totalKeys) {
                _chart.totalKey = totalKeys;
                return _chart;
            };

            _chart.group = function (group) {
                _chart._group = group;
                _chart.data();
                return _chart;
            };

            _chart.dimension = function (dimension) {
                _chart._dimension = dimension;
                return _chart;
            };

            _chart.measures = function(measures){
                var measure={};
                $.each(measures,function(key,val){
                    measure[key]=val;
                });
                _chart._measures = measure;
                return _chart;
            };

            _chart.dataFormat = function (dataFormat) {
                _chart._dataFormateVal = dataFormat;
                _chart.setDataFormat();
                return _chart;
            };

            _chart.dataSetLabels = function (labels) {
                _chart._dataSetLabels = labels;
                return _chart;
            };

            _chart.sort = function (order) {
                _chart._sort = order;
                eChart.registerDataOrder(_chart);
                _chart._metadataId=eChart._metadataId;
                sketchServer._dualAxisChartJs(_chart);
            };

            _chart.xAxes = function () {
                var labelColor, axisLineColor;
                if(_chart._dataFormateVal && _chart._dataFormateVal.axisLabelColor){
                    labelColor = _chart._dataFormateVal.axisLabelColor;
                }else{
                    labelColor = '#666666';
                }
                if(_chart._dataFormateVal && _chart._dataFormateVal.axisLabelColor){
                    axisLineColor = _chart._dataFormateVal.axisColor;
                }else{
                    axisLineColor = '#e6e6e6';
                }

                /*
                 * scale labels
                 */
                var scaleLabels=false;
                var axisLabelHeader=true;
                var allAxisHeader=false;
                var axisHeaderRotate=90;
                var autoSkip=false;
                if(_chart._dataFormateVal && _chart._dataFormateVal.xaxis){
                    if(_chart._dataFormateVal.xaxis.axisLabel){
                        scaleLabels=_chart._dataFormateVal.xaxis.axisLabel;
                    }
                    /*
                     * Axis label option
                     */
                    if(_chart._dataFormateVal.yaxis.axisHeader!=undefined){
                        axisLabelHeader=_chart._dataFormateVal.yaxis.axisHeader;
                    }
                    /*
                     * All axis label
                     */
                    if(_chart._dataFormateVal.xaxis.allAxisHeader!=undefined){
                        allAxisHeader=_chart._dataFormateVal.xaxis.allAxisHeader;
                        if(allAxisHeader==true){
                            allAxisHeader=false;
                            autoSkip=false;
                        }else{
                            allAxisHeader=10;
                            autoSkip=true;
                        }
                    }
                    /*
                     * Rotate label
                     */
                    if(_chart._dataFormateVal.xaxis.axisHeaderRotate!=undefined){
                        axisHeaderRotate=_chart._dataFormateVal.xaxis.axisHeaderRotate;
                    }
                }
                /*
                 * Dual x axis flag
                 */
                var dualXAxisFlag=false;
                if(_chart._dataFormateVal && _chart._dataFormateVal.xaxis && _chart._dataFormateVal.xaxis.dualX && _chart._dataFormateVal.xaxis.dualX=='yes'){
                    dualXAxisFlag=true;
                }
                /*
                 *  dimension value
                 */
                var dimensionValue="";
                if(_chart._dimension){
                    dimensionValue=Object.keys(_chart._dimension)[0];
                }
                if(/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
                    _chart._xfontSize=30;
                }
                var dualXaxisLabel=false;
                var dualXaxisLabelVal="";
                if(_chart._dataFormateVal && _chart._dataFormateVal.xaxis && _chart._dataFormateVal.xaxis.dualXaxisLabel){
                    dualXaxisLabel=_chart._dataFormateVal.xaxis.dualXaxisLabel;
                    dualXaxisLabelVal=JSON.parse(_chart._dataFormateVal.xaxis.dualXDimension).reName;
                }

                var dualXSkipLabel={};
                if ((_chart._dataType == "date" || _chart._fontSize == "datetime") && _chart._dataFormateVal.xAxis == undefined) {
                    return [{
                        id:'xAxis1',
                        type:"category",
                        stacked: true,
                        scaleLabel: {
                            display: scaleLabels,
                            labelString: dimensionValue
                        },
                        gridLines: {
                            color: axisLineColor,
                            display: axisLabelHeader
                        },
                        ticks: {
                            fontColor: labelColor,
                            display:axisLabelHeader,
                            fontSize: _chart._xfontSize,
                            autoSkip: autoSkip,
                            maxRotation: axisHeaderRotate,
                            minRotation: axisHeaderRotate,
                            maxTicksLimit: allAxisHeader,
                            fontFamily: "sans-serif",
                            callback:function(label){
                                label=label.toString();
                                //return label;
                                return label.slice(0, 15) +(label.length > 15 ? "..." : "");
                            }
                        },
                        type: 'time',
                        unit: 'day',
                        unitStepSize: 1,
                        time: {
                            displayFormats: {
                                'day': 'MMM DD'
                            }
                        }
                    },
                    {
                        id:'xAxis2',
                        type:"category",
                        stacked: true,
                        display:dualXAxisFlag,
                        position: 'top',
                        scaleLabel: {
                            display: dualXaxisLabel,
                            labelString: dualXaxisLabelVal
                        },
                        gridLines: {
                            color: axisLineColor,
                            display:axisLabelHeader,
                            drawOnChartArea: true, // only want the grid lines for one axis to show up
                            //tickMarkLength: 10,
                            offsetGridLines: true
                        },
                        ticks:{
                            fontColor: labelColor,
                            display:axisLabelHeader,
                            fontSize: _chart._xfontSize,
                            maxRotation: 0,
                            minRotation: 0,
                            fontFamily: "sans-serif",
                            callback:function(label,index,labels){
                                label=label.toString();
                                if(dualXSkipLabel[_chart._data.dualXLabel[label]]==undefined){
                                    dualXSkipLabel[_chart._data.dualXLabel[label]]=index;
                                }
                                if(dualXSkipLabel[_chart._data.dualXLabel[label]]==index){
                                    return _chart._data.dualXLabel[label];
                                }
                                return "";
                            }
                        },
                        type: 'time',
                        unit: 'day',
                        unitStepSize: 1,
                        time: {
                            displayFormats: {
                                'day': 'MMM DD'
                            }
                        }}];
                } else {
                    return [{
                        id:'xAxis1',
                        type:"category",
                        stacked: true,
                        scaleLabel: {
                            display: scaleLabels,
                            labelString: dimensionValue
                        },
                        gridLines: {
                            color: axisLineColor,
                            display: axisLabelHeader,
                        },
                        ticks:{
                            fontColor: labelColor,
                            display:axisLabelHeader,
                            fontSize: _chart._xfontSize,
                            autoSkip: autoSkip,
                            maxTicksLimit: allAxisHeader,
                            maxRotation: axisHeaderRotate,
                            minRotation: axisHeaderRotate,
                            fontFamily: "sans-serif",
                            callback:function(label){
                                label=label.toString();
                                //return label;
                                return label.slice(0, 15) +(label.length > 15 ? "..." : "");
                            }
                        }
                    },
                    {
                        id:'xAxis2',
                        type:"category",
                        stacked: true,
                        display:dualXAxisFlag,
                        position: 'top',
                        scaleLabel: {
                            display: dualXaxisLabel,
                            labelString: dualXaxisLabelVal
                        },
                        gridLines: {
                            color: axisLineColor,
                            display:axisLabelHeader,
                            drawOnChartArea: true, // only want the grid lines for one axis to show up
                            //tickMarkLength: 10,
                            offsetGridLines: true
                        },
                        ticks:{
                            fontColor: labelColor,
                            display:axisLabelHeader,
                            fontSize: _chart._xfontSize,
                            maxRotation: 0,
                            minRotation: 0,
                            fontFamily: "sans-serif",
                            callback:function(label,index,labels){
                                label=label.toString();
                                if(dualXSkipLabel[_chart._data.dualXLabel[label]]==undefined){
                                    dualXSkipLabel[_chart._data.dualXLabel[label]]=index;
                                }
                                if(dualXSkipLabel[_chart._data.dualXLabel[label]]==index){
                                    return _chart._data.dualXLabel[label];
                                }
                                return "";
                            }
                        }}];
                }
                //return eChart.renderxAxes(_chart._dataType,_chart._fontSize);
            };

            _chart.yAxes=function(chart){
                return eChart.renderyAxes(_chart._dataFormateVal, chart);
            }

            _chart.dataType = function (type) {
                _chart._dataType = type;
            };

            _chart.fontSize = function (fontSize) {
                _chart._fontSize = fontSize
            };

            _chart.fontFamily = function (fontFamily) {
                _chart._fontFamily = fontFamily;
            }

            _chart.yAxisPrefix = function (yAxis) {
                if (yAxis) {
                    _chart._yAxisPrefix = yAxis;
                } else {
                    _chart._yAxisPrefix = "";
                }

            }

            _chart.setDataFormat = function () {
                /* y axis font size
                */
                if (_chart._dataFormateVal.yaxis && _chart._dataFormateVal.yaxis.fontSize) {
                    _chart.yfontSize(_chart._dataFormateVal.yaxis.fontSize);
                } else {
                    _chart.yfontSize(12);
                }
                /*
                 * x axis font size
                 */
                if (_chart._dataFormateVal.xaxis && _chart._dataFormateVal.xaxis.fontSize) {
                    _chart.xfontSize(_chart._dataFormateVal.xaxis.fontSize);
                } else {
                    _chart.xfontSize(12);
                }

                if(_chart._dataFormateVal.topN){
                    _chart.topN = _chart._dataFormateVal.topN;
                    _chart.topnMeasure = _chart._dataFormateVal.topnMeasure;
                    _chart.topNOther = _chart._dataFormateVal.topNOther;
                }
                if(_chart._dataFormateVal.sort){
                    _chart._sort = _chart._dataFormateVal.sort;
                    _chart._sortMeasure = _chart._dataFormateVal.sortMeasure;
                }
                if(_chart._dataFormateVal.runningTotal!=undefined){
                    _chart._runningTotal=_chart._dataFormateVal.runningTotal;
                }
                _chart.groupColor = _chart._dataFormateVal.groupColor;
                _chart._dataType = _chart._dataFormateVal.dataType;
                _chart._chartType = _chart._dataFormateVal.chartType;
            }

            _chart.xfontSize = function (fontSize) {
                _chart._xfontSize = fontSize
            };

            _chart.yfontSize = function (fontSize) {
                _chart._yfontSize = fontSize
            };

            //---------------------------Group Color Start--------------------
            _chart.getDatasetByGroupColor = function (finalData, groupKey,type,indexYaxis) {
                var groupMappings = _chart.createGroupMappings(finalData, _chart);
                var initialData = [];
                _chart._labels.forEach(function (d, i) {
                    initialData[i] = null;
                });
                var p = 0;
                var datasets = [];
                $.each(groupMappings, function (key, value) {
                    var data = Object.assign([], initialData);
                    $.each(value, function (k, v) {
                        var labelIndexMatched = _chart._labels.indexOf(k);
                        if (labelIndexMatched != -1) {
                            data[labelIndexMatched] = v;
                        }
                    });
                    var backgroundColor;
                    if (type == "line") {
                        backgroundColor = _chart._colorSelection[p % (_chart._colorSelection.length)];
                    }else{
                        backgroundColor = Array(data.length).fill(_chart._colorSelection[p % (_chart._colorSelection.length)])
                    }
                    var dt = {
                        measure:groupKey,
                        type: type,
                        fill: false,
                        label: key,
                        data: data,
                        backgroundColor: backgroundColor,
                        borderColor: backgroundColor,
                        // hoverBorderColor: hoverBorderColor,
                        borderWidth: 1,
                        yAxisID:"y-axis-"+indexYaxis
                    };
                    // _chart._labels=labels
                    datasets.push(dt);
                    p++;
                });
                return datasets;
            }

            _chart.updateDatasetByGroupColor = function (finalData, groupKey,type,indexYaxis) {
                var groupMappings = _chart.createGroupMappings(finalData, _chart);
                var labelsList = {};
                $.each(groupMappings, function (key, value) {
                    $.each(value, function (k, v) {
                        if (!(v == 0 || v < 0 || isNaN(v))) {
                            labelsList[k] = 1;
                        }
                    });
                });
                var labelsListOrdered = {};
                Object.keys(labelsList).forEach(function(key) {
                    labelsListOrdered[key] = labelsList[key];
                });
                var initialData = [];
                var indexData = 0;
                var labelsListArray = [];
                $.each(labelsListOrdered, function (key, value) {
                    initialData[indexData] = null;
                    labelsListArray.push(key);
                    indexData++;
                });
                var p = 0;
                var datasets = [];
                $.each(groupMappings, function (key, value) {
                    var data = Object.assign([], initialData);
                    $.each(value, function (k, v) {
                        var labelIndexMatched = _chart._data.labels.indexOf(k);
                        if (labelIndexMatched != -1) {
                            if (!(v <= 0))
                                data[labelIndexMatched] = v;
                        }
                    });
                    var backgroundColor;
                    if (type == "line") {
                        backgroundColor = _chart._colorSelection[p % (_chart._colorSelection.length)];
                    }else{
                        backgroundColor = Array(data.length).fill(_chart._colorSelection[p % (_chart._colorSelection.length)])
                    }
                    var dt = {
                        measure:groupKey,
                        type: type,
                        fill: false,
                        label: key,
                        data: data,
                        backgroundColor: backgroundColor,
                        borderColor: backgroundColor,
                        // hoverBorderColor: hoverBorderColor,
                        borderWidth: 1,
                        yAxisID:"y-axis-"+indexYaxis
                    };
                    datasets.push(dt);
                    p++;
                });

                return datasets;
            }
            //---------------------------Group Color End-----------------------
            _chart.createGroupMappings = function (finalData, _chart) {
                _chart._groupMappings = eChart.createGroupMappings(finalData, _chart);
                return _chart._groupMappings;
            }

            _chart.getDataByGroup = function () {
                var groups = _chart._group;
                var datasets = [];
                var indexYaxis=1;
                var multipleMeasure=[];
                var j=0;
                var dualXLabel={};
                $.each(groups, function (key, grpObj) {
                    var finalData=grpObj;
                    if (eChart.isDataFormatApplied(_chart) && !_chart._sort && !_chart.topN) {
                        finalData = eChart.getDataByDateFormat(grpObj, key, _chart);
                    }
                    if(j==0)
                        grpObj.forEach(function (d) {
                            dualXLabel[d.key]=d.dualXKey;
                        });
                    var type;
                    if (_chart._chartType) {
                        if (_chart._chartType[key]) {
                            type = _chart._chartType[key]['type'];
                        } else {
                            if(indexYaxis==1){
                                type="line";
                            }else{
                                type="bar";
                            }
                        }
                    } else {
                        if(indexYaxis==1){
                            type="line";
                        }else{
                            type="bar";
                        }
                    }
                    datasets = _chart.getDatasetByGroupColor(finalData, key,type,indexYaxis);
                    indexYaxis++;
                    datasets.forEach(function (d) {
                        multipleMeasure.push(d);
                    });
                    j++;
                });

                var dataInFormat = {
                    labels: _chart._labels,
                    datasets: multipleMeasure,
                    dualXLabel:dualXLabel
                };
                return dataInFormat;
            }

            _chart.getNormalData = function () {
                //  var colorsLength = eChart.colors.length;
                var groups = _chart._group;
                var labels = [];
                var datasets = [];
                var i = 0;
                var j = 0;
                var indexYaxis=1;
                _chart._dataSetLabels=[];
                var dualXLabel={};
                $.each(groups, function (key, grpObj) {
                    var data = [];
                    var backgroundColor = [];
                    //  var borderColor = [];
                    var hoverBorderColor = [];
                    var finalData;
                    /*
                      * Line or bar type customiztion function
                     */
                    var type;
                    if (_chart._chartType) {
                        if (_chart._chartType[key]) {
                            type = _chart._chartType[key]['type'];
                        } else {
                            if(indexYaxis==1){
                                type="line";
                            }else{
                                type="bar";
                            }
                        }
                    } else {
                        if(indexYaxis==1){
                            type="line";
                        }else{
                            type="bar";
                        }
                    }
                    /*
                     *    End line bar
                     */
                    finalData = grpObj;
                    if (eChart.isDataFormatApplied(_chart) && !_chart._sort && !_chart.topN) {
                        finalData = eChart.getDataByDateFormat(grpObj, key, _chart);
                    }
                    if(_chart._runningTotal && _chart._runningTotal[key]){
                        finalData = eChart.getRunTotal(finalData,key,_chart);
                    }
                    _chart._dataSetLabels.push(key);
                    finalData.forEach(function (d) {
                        if(j === 0){
                            dualXLabel[d.key]=d.dualXKey;
                            labels.push(d.key);
                        }
                        if(_chart._operator && d.value!=null && d.value[_chart._operator[key].key]){
                            data.push(d.value[_chart._operator[key].key]);
                        }else{
                            data.push(d.value);
                        }
                        backgroundColor.push(_chart._colorSelection[j]);
                        hoverBorderColor.push("#000");
                        i++;
                    });
                    if (type == "line") {
                        backgroundColor = backgroundColor[j];
                    }
                    var dt = {
                        type: type,
                        fill: false,
                        label: key,
                        data: data,
                        backgroundColor: backgroundColor,
                        borderColor: backgroundColor,
                        hoverBorderColor: hoverBorderColor,
                        borderWidth: 1,
                        yAxisID:"y-axis-"+indexYaxis
                    };
                    datasets.push(dt);
                    j++;
                    indexYaxis++
                });
                var dataInFormat = {
                    labels: labels,
                    datasets: datasets,
                    dualXLabel:dualXLabel
                };
                return dataInFormat;
            }

            _chart.getDataInFormat = function (order) {
                eChart.updateAttributes(_chart);
                var groups = _chart._group;
                var labels = [];
                var datasets = [];
                var i = 0;
                var j = 0;
                var dataInFormat = {};
                if (eChart.isGroupColorApplied(_chart)) {
                    dataInFormat = _chart.getDataByGroup();
                } else {
                    dataInFormat = _chart.getNormalData();
                }
                if (_chart._dataFormateVal && _chart._dataFormateVal.tooltipSelector) {
                    eChart.processTooltipData(_chart);
                }
                return dataInFormat;
            }

            _chart.data = function (data) {
                _chart._data = _chart.getDataInFormat();
                return _chart;
            };

            _chart.render = function () {
                eChart.clearContainer(id);
                var bar = eChart.getCanvasContext(_chart, function (context) {
                    _chart.draw(context);
                    _chart.bindEvents();
                });
                sketchServer.reportLoadedCountCheck(_chart);
            };

            _chart.draw = function (context) {
                var instance = new Chart(context, {
                    type: 'bar',
                    data: _chart._data,
                    options: eChart.createOption(_chart)
                });
                var res = id.split("-");
                _chart.chartInstance = instance;
                eChart.generateLegend("left" + res[1], _chart);
                _chart['tempGroupKey'] = _chart._group;
                eChart.registerChart(_chart);
                sketchServer.globalRegistry.register(_chart);
                sketchServer.globalRegistry.syncLocalRegistries(_chart.id,"eChart");
            };

            _chart.operator = function (operator) {
                _chart._operator = operator;
                return _chart;
            };

            _chart.bindEvents = function () {
                eChart.bindEvents(this);
            };

            _chart.highlightSelectedFilter = function () {
                var labels = this.getLabels();
                var labelsLength = labels.length;
                // var colorsLength = eChart.colors.length;

                var chartFiltersLength = 0;
                if (!_chart.reset) {
                    chartFiltersLength = this.filters.length;
                }

                var pointRadiusNumber=2;
                if(/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
                    pointRadiusNumber=10;
                }

                if ((labelsLength == chartFiltersLength && !eChart.isGroupColorApplied(_chart)) || chartFiltersLength === 0) {
                    try {
                        _chart._data.datasets.forEach(function (dataset, dsIndex) {
                            labels.forEach(function (filter, i) {
                                if (_chart._data.datasets[dsIndex].type == 'bar') {
                                    _chart._data.datasets[dsIndex].backgroundColor[i] = _chart._colorSelection[dsIndex];
                                }
                            });
                        });
                    } catch (e) {

                    }
                    _chart.filters = [];
                } else {
                    if(eChart.isGroupColorApplied(_chart)){
                        var filterLabelsArray=[];
                        var datasetWiseFilters = {};
                        var grpArray=[];
                        Object.keys(_chart._groupMappings).forEach(function (ke) {
                            grpArray.push(ke);
                        })
                        _chart.filters.forEach(function (filter) {
                            if (!datasetWiseFilters[filter.datasetIndex])
                                datasetWiseFilters[filter.datasetIndex] = [];
                            datasetWiseFilters[filter.datasetIndex].push(filter.label);
                        });
                        labels.forEach(function (filter, i) {
                            var index = eChart.getIndexOfObj(_chart.filters, eChart.constants.LABEL, filter);
                            if (index != -1) {
                                //var dsIndex = eChart.getIndexOfObj(_chart._data.datasets, eChart.constants.LABEL, _chart.filters[index].datasetLabel);
                                _chart._data.datasets.forEach(function (dataset, chartDsIndex) {
                                    var filterLabelsArray = datasetWiseFilters[chartDsIndex];
                                    if (filterLabelsArray) {
                                        if (filterLabelsArray.indexOf(filter) == -1) {
                                            _chart._data.datasets[chartDsIndex].backgroundColor[i] = "#ddd";
                                        } else if (dataset.type == 'bar') {
                                            _chart._data.datasets[chartDsIndex].backgroundColor[i] = _chart._colorSelection[grpArray.indexOf(_chart._data.datasets[chartDsIndex].label)];
                                        }
                                    } else {
                                        if (dataset.type == 'bar') {
                                            _chart._data.datasets[chartDsIndex].backgroundColor[i] = "#ddd";
                                        }
                                    }
                                });
                            } else {
                                _chart._data.datasets.forEach(function (dataset, dsIndex) {
                                    if (dataset.type == 'bar') {
                                        _chart._data.datasets[dsIndex].backgroundColor[i] = "#ddd";
                                    }
                                });
                            }
                        });
                    }else{
                        labels.forEach(function (filter, i) {
                            var index = eChart.getIndexOfObj(_chart.filters, eChart.constants.LABEL, filter);
                            if (index != -1) {
                                var dsIndex = eChart.getIndexOfObj(_chart._data.datasets, eChart.constants.LABEL, _chart.filters[index].datasetLabel);
                                //  _chart._data.datasets[dsIndex].backgroundColor[i] = _chart._colorSelection[dsIndex];
                                _chart._data.datasets.forEach(function (dataset, dsIndex) {
                                    if (dataset.type == 'bar') {
                                        _chart._data.datasets[dsIndex].backgroundColor[i] = _chart._colorSelection[dsIndex];
                                    }
                                });
                            } else {
                                _chart._data.datasets.forEach(function (dataset, dsIndex) {
                                    if (dataset.type == 'bar') {
                                        _chart._data.datasets[dsIndex].backgroundColor[i] = "#ddd";
                                    }
                                });
                            }
                        });
                    }
                }
            };

            _chart.colorChange = function (selectedDataSet, color) {
                var labels = this.getLabels();
                _chart._data.datasets.forEach(function (dataset, dsIndex) {
                    if (dataset.label == selectedDataSet.datasetLabel) {
                        _chart._colorSelection[dsIndex] = color;
                        /*var tempData = [];
                        labels.forEach(function (filter, i) {
                            tempData.push(color);
                        });*/
                        _chart._data.datasets[dsIndex].backgroundColor = color;
                        _chart._data.datasets[dsIndex].borderColor = color;
                    }
                });
                setTimeout(function () {
                    _chart.chartInstance.update();
                    eChart.registerColor(_chart);
                    var res = _chart.id.split("-");
                    eChart.generateLegend("left" + res[1], _chart);
                }, 1);
            };

            _chart.getClickedElementLabel = function (evt) {
                var filter = {};
                try {
                    var eveObj=this.chartInstance.getElementAtEvent(evt)[0];
                    if(this.chartInstance.config.data.datasets[eveObj._datasetIndex].type=="line"){
                        filter.label = this.getLabels()[eveObj._index];
                        filter.datasetLabel = eveObj._chart.config.data.datasets[eveObj._datasetIndex].label;
                        filter.datasetIndex = eveObj._datasetIndex;
                        if(eveObj._chart.config.data.datasets[eveObj._datasetIndex] && eveObj._chart.config.data.datasets[eveObj._datasetIndex].measure);
                        filter.measure = eveObj._chart.config.data.datasets[eveObj._datasetIndex].measure;
                    }else{
                        filter.label = eveObj._model.label;
                        filter.datasetLabel = eveObj._model.datasetLabel;
                        filter.datasetIndex = eveObj._datasetIndex;
                        if(eveObj._chart.config.data.datasets[eveObj._datasetIndex] && eveObj._chart.config.data.datasets[eveObj._datasetIndex].measure);
                        filter.measure = eveObj._chart.config.data.datasets[eveObj._datasetIndex].measure;
                    }
                    return filter;
                } catch (e) {
                    return null;
                }
            };

            _chart.getLabels = function () {
                return this.chartInstance.config.data.labels;
            };

            return _chart;
        };
        // -------------------------------------Dual Axis CHART END-------------------------------------------------------------
        // -------------------------------------Funnel CHART DRAW CODE--------------------------------------------------------

        eChart.funnelChart = function (id) {
            var _chart = {};
            _chart.id = id;
            _chart.type = "funnelChart";
            _chart.dataBackup = [];
            _chart._colorSelection = $.extend([], eChart.colors);

            _chart.tKey = function (totalKeys) {
                _chart.totalKey = totalKeys;
                return _chart;
            };

            _chart.group = function (serverData) {
                _chart._group = serverData;
                _chart.data();
                return _chart;
            };

            _chart.measures = function(measures){
                var measure={};
                $.each(measures,function(key,val){
                    measure[key]=val;
                });
                _chart._measures = measure;
                return _chart;
            };

            _chart.sort = function (order) {
                _chart._sort = order;
                eChart.registerDataOrder(_chart);
                _chart._metadataId=eChart._metadataId;
                sketchServer._funnelChart(_chart);
            };

            _chart.dataFormat = function (dataFormate) {
                _chart._dataFormateVal = dataFormate;
                return _chart;
            };

            _chart.dimension = function (dimension) {
                _chart._dimension = dimension;
                return _chart;
            };

            _chart.data = function (data) {
                _chart._data = _chart.getDataInFormat();
                return _chart;
            };

            _chart.operator = function (operator) {
                _chart._operator = operator;
                return _chart;
            };

            _chart.render = function () {
                eChart.clearContainer(id);
                var bar = eChart.getCanvasContext(_chart, function (context) {
                    _chart.draw(context);
                    _chart.bindEvents();
                });
                sketchServer.reportLoadedCountCheck(_chart);
                return true;
            };

            _chart.draw = function (context) {
                var sortType = 'desc';
                if(!$.isEmptyObject(_chart._dataFormateVal) && _chart._dataFormateVal.sort){
                    sortType = _chart._dataFormateVal.sort;
                }
                var responsiveData = true;
                if(_chart._dataFormateVal && _chart._dataFormateVal.canvas && _chart._dataFormateVal.canvas.width && _chart._dataFormateVal.canvas.height){
                    responsiveData = false;
                }

                var instance = new Chart(context, {
                    type: 'funnel',
                    data: _chart._data,
                    options: {
                        sort: sortType,
                        responsive: responsiveData,
                        maintainAspectRatio: false,
                        legend: {
                            display: false,
                        },
                        title: {
                            display: false,
                            text: ''
                        },
                        animation: {},
                        pan: {
                            enabled: false,
                            mode: 'xy'
                        },
                        zoom: {
                            enabled: false,
                            mode: 'x',
                        },
                        tooltips: {
                            enabled: true,
                            callbacks: {
                                title: function (tooltipItem, data) {
                                    return "";
                                },
                                label: function(tooltipItem, data) {
                                    return eChart.customTooltipTitle(tooltipItem, data,_chart);
                                },footer: function(t, d){
                                    return eChart.customTooltipFooter(t,d,_chart);
                                }
                            }
                        },
                        legendCallback: function (chartInstance) {
                            if(eChart.legendChartObj[_chart.id]!=undefined){
                                chartInstance.legend.legendItems=eChart.legendChartObj[_chart.id];
                            }
                            var html='';
                            if(Object.keys(_chart._dimension).length){
                                var dimension=Object.keys(_chart._dimension)[0];
                                html +="<li title="+dimension+"><p>&nbsp;&nbsp;"+dimension.slice(0, 16) + (dimension.length > 16 ? "..." : "") +"</p></li>";
                            }
                            html += "<ul>";
                            chartInstance.legend.legendItems.forEach(function (d) {
                                if(d.hidden==false) {
                                    html += '<li class=""><span style="background-color:' + d.fillStyle + '"></span>' + d.text + '</li>';
                                }
                            });
                            html += "</ul>"
                            return html;
                        },
                        animation: eChart.markLabels(_chart),
                        hover: {
                            animationDuration:0
                        }
                    }
                });
                var res = id.split("-");
                _chart.chartInstance = instance;
                eChart.generateLegend("left" + res[1], _chart);
                _chart['tempGroupKey'] = _chart._group;
                eChart.registerChart(_chart);
                sketchServer.globalRegistry.register(_chart);
                sketchServer.globalRegistry.syncLocalRegistries(_chart.id,"eChart");
            };

            _chart.colorChange = function (selectedDataSet, color) {
                //var labels = this.getLabels();
                var colorsLength = eChart.colors.length;
                var colorIndex;
                _chart._data.labels.forEach(function (d,index) {
                    if(d==selectedDataSet.label){
                        colorIndex=index;
                    }
                });
                _chart._colorSelection[colorIndex % colorsLength]=color;
                _chart._data.datasets[0].backgroundColor[colorIndex] = color;
                setTimeout(function () {
                    _chart.chartInstance.update();
                    eChart.registerColor(_chart);
                    var res = _chart.id.split("-");
                    eChart.generateLegend("left" + res[1], _chart);
                }, 1);
            };

            _chart.getDataInFormat = function () {
                if(_chart._dataFormateVal.topN){
                    _chart.topN = _chart._dataFormateVal.topN;
                    _chart.topNOther = _chart._dataFormateVal.topNOther;
                }
                eChart.updateAttributes(_chart);
                var colorsLength = eChart.colors.length;
                var groups = _chart._group;
                var labels = [];
                var datasets = [];
                var i = 0, j = 0, index = 0;
                $.each(groups, function (key, grpObj) {
                    var finalData;
                    var data = [];
                    var backgroundColor = [];
                    finalData = grpObj;
                    finalData.forEach(function (d) {
                        if (_chart._operator) {
                            if(_chart._dataFormateVal && _chart._dataFormateVal.tooltipSelector){
                                if (d.value!=null && d.value[_chart._operator[key].key] != undefined) {
                                    labels.push(d.key);
                                    data.push(d.value[_chart._operator[key].key]);
                                }else{
                                    labels.push(d.key);
                                    data.push(d.value);
                                }
                            }else{
                                if (d.value!=null && d.value[_chart._operator[key].key] != undefined) {
                                    labels.push(d.key);
                                    data.push(d.value[_chart._operator[key].key]);
                                }else{
                                    labels.push(d.key);
                                    data.push(d.value);
                                }
                            }
                        } else {
                            if (d.value!=null && d.value[_chart._operator[key].key] != undefined) {
                                labels.push(d.key);
                                data.push(d.value[_chart._operator[key].key]);
                            }else{
                                labels.push(d.key);
                                data.push(d.value);
                            }
                        }
                        backgroundColor.push(_chart._colorSelection[i % colorsLength]);
                        i++;
                    });

                    var dt = {
                        data: data,
                        backgroundColor: backgroundColor
                    };
                    datasets.push(dt);
                    j++;
                    index++;
                });
                if (_chart._dataFormateVal && _chart._dataFormateVal.tooltipSelector) {
                    eChart.processTooltipData(_chart);
                }
                var dataInFormat = {
                    labels: labels,
                    datasets: datasets
                };
                return dataInFormat;
            };

            _chart.highlightSelectedFilter = function () {
                var labels = this.getLabels();
                var labelsLength = labels.length;
                var colorsLength = eChart.colors.length;
                var chartFiltersLength = 0;
                if (!_chart.reset) {
                    chartFiltersLength = this.filters.length;
                }
                if (labelsLength == chartFiltersLength || chartFiltersLength === 0) {
                    labels.forEach(function (filter, i) {
                        _chart._data.datasets[0].backgroundColor[i] = _chart._colorSelection[i % colorsLength];
                    });
                    _chart.filters = [];
                } else {
                    labels.forEach(function (filter, i) {
                        var index = eChart.getIndexOfObj(_chart.filters, eChart.constants.LABEL, filter);
                        if (index != -1) {
                            _chart._data.datasets[0].backgroundColor[i] = _chart._colorSelection[i % colorsLength];
                        } else {
                            _chart._data.datasets[0].backgroundColor[i] = "#ddd";
                        }
                    });
                }
            };

            _chart.getClickedElementLabel = function (evt) {
                var filter = {};
                try {
                    filter.label = this.chartInstance.getElementAtEvent(evt)[0]._model.label;
                    filter.datasetLabel = null;
                    return filter;
                } catch (e) {
                    return null;
                }
            };

            _chart.bindEvents = function () {
                eChart.bindEvents(this);
            };

            _chart.getLabels = function () {
                return this.chartInstance.config.data.labels;
            };

            return _chart;
        };
        // ------------------------------------Funnel  END--------------------------------------------------------------
        return eChart;
    }
    this.eChartServer = _eChart(Chart);
})();
