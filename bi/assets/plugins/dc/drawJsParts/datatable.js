/**
 * Created by mitesh.panchal on 9/15/2017.
 */
sketch._dataTable = function () {
    var keyOfObj = "";
    var data = [];
    if (!$.isEmptyObject(draw._dimension)) {

        $.each(draw._dimension, function (key, val) {
            keyOfObj = key;
        });

        var dim = draw._crossfilter
            .dimension(function (d) {
                return d[draw._dimension[keyOfObj].value];
            });

        data = jQuery.extend(true, [], dim.top(Infinity));
    }
    else {
        data = jQuery.extend(true, [], draw._data);
    }
    var tableHeaders = "";
    var k = 0;
    var ColumnArrayForDataTable = [];
    var keys = [];


    if(draw._tableSetting && draw._tableSetting.tableColumnOrder){

        keys=[];
        var simpleKey=[];

        $.each(draw._tableSetting.tableColumnOrder,function(key,value){
            simpleKey.push(JSON.parse(value).columnName);
            keys.push(JSON.parse(value));
        });
        var commonArray=[];
        var commonArrayNames=[];
        $.each(draw._measure,function(key,value){
            commonArrayNames.push(key);
            commonArray.push(value);

        });
        $.each(draw._dimension,function(key,value){
            commonArrayNames.push(key);
            commonArray.push(value);
        });




        if(simpleKey.length<commonArrayNames.length){
            commonArrayNames.forEach(function(d,i){
                if(simpleKey.indexOf(d)==-1){
                    keys.push(commonArray[i]);
                }
            });
        }else if(simpleKey.length==commonArrayNames.length){


            for(var j=commonArrayNames.length-1;j>=0;j--){
                if(simpleKey.indexOf(commonArrayNames[j])==-1){

                    keys.push(commonArray[j]);
                    simpleKey.push(commonArray[j].columnName);

                }
            }

            for(var j=simpleKey.length-1;j>=0;j--){
                if(commonArrayNames.indexOf(simpleKey[j])==-1){

                    keys.splice(j,1);
                    //simpleKey.splice(j,1);

                }
            }


        }else {
            var temp=simpleKey;

            for(var j=simpleKey.length-1;j>=0;j--){
                if(commonArrayNames.indexOf(simpleKey[j])==-1){

                    keys.splice(j,1);
                    //simpleKey.splice(j,1);

                }
            }
        }

    }else{
        try {
            $.each(draw._dimension, function (key, value) {
                keys.push(value);
            });
        }
        catch (e) {

        }

        try {
            $.each(draw._measure, function (key, value) {
                keys.push(value);
            });
        }
        catch (e) {

        }
    }






    var columnNames=[];
    keys.forEach(function (d) {
        columnNames.push(d['columnName']);
        if(d.dataKey=="Dimension")
            draw._activeKey=d['columnName'];
        // var obj = new Object({
        //     "mData": d,
        //     "sDefaultContent": ""
        // });

        //  ColumnArrayForDataTable.push(obj);
        tableHeaders += "<th>"
            + '<a href="javascript:void(0)"  style="float:right" class="toggle-vis" data-column="'
            + k + '"></a>' + d['columnName'] + "</th>";
        k++;
    });
    if(draw._activeKey)
        keys.forEach(function (d) {
            if (d.form)
                if (d.type == "custom" && d.form == "group") {
                    var columnName = d.columnName;
                    var formula = d.formula;

                    calculate.processExpression(formula, data, columnName);
                }
        });

    // if(calculation._newkeysArray && !$.isEmptyObject(calculation._newkeysArray)){
    //     $.each(calculation._newkeysArray,function(key,value){
    //         if(value.form)
    //             if(value.type=="custom" && value.form=="group" ){
    //                 var columnName=value.columnName;
    //                 var formula=value.formula;
    //                 calculate.processExpression(formula,data, columnName);
    //             }
    //     });
    //
    // }

    var initialKey=columnNames[0];
    $("#chart-" + draw._container).html("");


    $("#chart-" + draw._container)
        .append('<table id="table-'
            + draw._container

            + '" class="table table-bordered bordered table-striped table-condensed tablesorter"  style="width: 100%;"><thead><tr>'
            + tableHeaders + '</tr></thead></table>');
    var p="<tr>";

    var  fieldsArray=keys;
    var keyIndex=0;
    var TotalObject={};
    function sum(key,processedObj,d){

        if(processedObj["total_"+key] == undefined){
            processedObj["total_"+key]=0;
        }
        processedObj["total"] ={123:{}};
    }
    aggergateKeyIndexes=[];

    if(draw._tableSetting){
        $.each(draw._tableSetting.footer,function(k,v){
            if(columnNames.indexOf(k)!=-1){
                aggergateKeyIndexes.push(columnNames.indexOf(k));
            }
        });
    }
    var aggregates={};
    var subAggregates={};

    function doAggregate(operation,key,d){
        if(operation=="sum"){
            if(!aggregates[key]){
                aggregates[key]=0;
                if(parseFloat(d[key]))
                    aggregates[key]+= parseFloat(d[key]);
            }else{
                if(parseFloat(d[key]))
                    aggregates[key]+= parseFloat(d[key]);
            }
        }else if(operation=="count"){
            if(!aggregates[key]){
                aggregates[key]=0;
                aggregates[key]+= 1;
            }else{
                aggregates[key]+= 1;
            }
        }else if(operation=="avg"){
            if(!aggregates[key]){
                aggregates[key]=0;
                if(parseFloat(d[key])){
                    aggregates[key]+= parseFloat(d[key]);
                }
            }else{
                if(parseFloat(d[key]))
                    aggregates[key]+= parseFloat(d[key]);
            }
        }

    }
    //Sub string Calculate
    function doSubAggregate(operation,d,initialKey,columnNames,index){
        if (operation == "sum") {

            if (!subAggregates[d[initialKey]]) {
                subAggregates[d[initialKey]] = {};
                if (parseFloat(d[columnNames[index]]))
                    subAggregates[d[initialKey]][columnNames[index]] = parseFloat(d[columnNames[index]]);
                else
                    subAggregates[d[initialKey]][columnNames[index]] = 0;
            }
            else {
                if (parseFloat(d[columnNames[index]])) {

                    if (!subAggregates[d[initialKey]][columnNames[index]]) {
                        subAggregates[d[initialKey]][columnNames[index]] = 0;
                    }
                    subAggregates[d[initialKey]][columnNames[index]] += parseFloat(d[columnNames[index]]);
                }
            }
        }else if(operation == "count"){
            if (!subAggregates[d[initialKey]]) {
                subAggregates[d[initialKey]] = {};

                if (parseFloat(d[columnNames[index]]))
                    subAggregates[d[initialKey]][columnNames[index]] = 1;
                else
                    subAggregates[d[initialKey]][columnNames[index]] = 0;
            }
            else {
                if (parseFloat(d[columnNames[index]])) {

                    if (!subAggregates[d[initialKey]][columnNames[index]]) {
                        subAggregates[d[initialKey]][columnNames[index]] = 0;
                    }
                    subAggregates[d[initialKey]][columnNames[index]] += 1;
                }
            }
        }else if(operation == "avg"){
            if (!subAggregates[d[initialKey]]) {
                subAggregates[d[initialKey]] = {};

                if (parseFloat(d[columnNames[index]]))
                    subAggregates[d[initialKey]][columnNames[index]] = parseFloat(d[columnNames[index]]);
                else
                    subAggregates[d[initialKey]][columnNames[index]] = 0;
            }
            else {
                if (parseFloat(d[columnNames[index]])) {

                    if (!subAggregates[d[initialKey]][columnNames[index]]) {
                        subAggregates[d[initialKey]][columnNames[index]] = 0;
                    }
                    subAggregates[d[initialKey]][columnNames[index]] += parseFloat(d[columnNames[index]]);
                }
            }
        }
    }

    //     var data=[{"id":7,"name":"er","sales":3000},{"id":6,"name":"er","sales":3000},{"id":5,"name":"er","sales":3000},{"id":4,"name":"cef","sales":2000},{"id":3,"name":"cef","sales":2000},{"id":2,"name":"abc","sales":1000},{"id":1,"name":"abc","sales":1000}];
    //        fieldsArray=[{"columType":"varchar","tableName":"testPeriod","columnName":"name","reName":"name","dataKey":"Dimension","type":"defined","key":"varchar","value":"name"},{"columType":"double","tableName":"testPeriod","columnName":"sales","reName":"sales","dataKey":"Measure","type":"defined","key":"double","value":"sales"}];
    function aggregateObject(processedObj,currentKey,d,key,lastKeyType,type){
        try{
            if(draw._aggregate[key].key=="sumIndex"){

                var lastKey = Object.keys(processedObj)[0];
                var newKey = parseFloat(lastKey) + parseFloat(d[key]);
                currentKey = newKey;
                if(newKey!=lastKey)
                {
                    processedObj[newKey] = Object.assign({}, processedObj[lastKey]);
                    delete processedObj[lastKey];
                }
            }else if(draw._aggregate[key].key=="count"){
                if(type=="repeated")
                {
                    var lastKey = Object.keys(processedObj)[0];
                    var newKey = parseFloat(lastKey)+parseFloat(1);
                    currentKey = newKey;
                    if(newKey!=lastKey)
                    {
                        processedObj[newKey] = Object.assign({}, processedObj[lastKey]);
                        delete processedObj[lastKey];
                    }
                }
                else{
                    var lastKey = Object.keys(processedObj)[0];
                    processedObj[1]=Object.assign({}, processedObj[lastKey]);
                    currentKey=1;
                    newKey=1;
                    if(newKey!=lastKey)
                    {
                        processedObj[newKey] = Object.assign({}, processedObj[lastKey]);
                        delete processedObj[lastKey];
                    }
                }
            }
        }catch(e){

        }
        return currentKey;
    }
    function group(d, index, processedObj,lastKeyType,type) {
        var newIndex = index + 1;
        var key = fieldsArray[index]['columnName'];
        var keyType = fieldsArray[index]['dataKey'].trim();
        if (keyType == 'Measure') {
            if(d[key]==null || isNaN(d[key])){
                d[key]=0;
            }
        }
        if(!lastKeyType)
            lastKeyType=keyType;
        if(!type)
            type="notrepeated";
        if (!processedObj[d[key]]) {
            var currentKey=d[key];
            if (keyType == 'Measure') {
                currentKey=parseFloat(currentKey);
                if ($.isEmptyObject(processedObj)) {
                    if(draw._aggregate[key].key!="count"){
                        processedObj[currentKey] = {};
                    }
                    else{
                        currentKey=aggregateObject(processedObj,currentKey,d,key,lastKeyType,type);
                    }
                } else {
                    currentKey=aggregateObject(processedObj,currentKey,d,key,lastKeyType,type);
                }
            } else {

                processedObj[currentKey] = {};
            }
            if (index < fieldsArray.length - 1) {
                processedObj[currentKey] = group(d, newIndex, processedObj[currentKey],lastKeyType,"notrepeated");
                lastKeyType=keyType;
                return processedObj;
            } else {
                lastKeyType=keyType;
                return processedObj;
            }
        } else {
            var currentKey = d[key];
            if (index < fieldsArray.length - 1) {
                if (keyType == 'Measure') {
                    currentKey=parseFloat(currentKey);
                    currentKey=aggregateObject(processedObj,currentKey,d,key,lastKeyType,type);
                }

                processedObj[currentKey] = group(d, newIndex, processedObj[currentKey],lastKeyType,"repeated");
                lastKeyType=keyType;
                return processedObj;
            } else {
                if (keyType == 'Measure') {
                    currentKey=parseFloat(currentKey);
                    currentKey=aggregateObject(processedObj,currentKey,d,key,lastKeyType,type);
                }
            }
            lastKeyType=keyType;
            return processedObj;
        }

    }

    var finalObj = {};
    data.forEach(function(d, i) {
        group(d, 0, finalObj);
    });

    var isObject = function(a) {
        if($.isEmptyObject(a))
            return false
        else
            return (!!a) && (a.constructor === Object);

    };

    function getAllChildren(group, children) {
        children = children || [];
        if(group && isObject(group)) {
            $.each(group,function(key,child){
                getAllChildren(child, children)
            })
        }
        else {
            children.push(group);
        }
        return children;
    }

    var p="";
    var r="";
    var classText="";
    $.each(finalObj,function(key,value){
        r="";

        p +="<tr>";
        var rowLength=getAllChildren(value).length;
        var tableData=key.split('#');
        if(rowLength>1){ classText="tablesorter-childRow";}
        p +="<td rowspan='"+rowLength+"'>"+tableData[0]+"</td>";
        p +=rowObj(value);
        if((!$.isEmptyObject(subAggregates[key])) && draw._tableSetting.footer.subtotal==1){
            p +="<tr class='subtotal'>";
            columnNames.forEach(function(d){
                if(subAggregates[key][d])
                    p +="<th> Sub"+draw._tableSetting.footer[d]['operation']+" : "+subAggregates[key][d]+"</th>";
                else
                    p +="<th>&nbsp;</th>";
            });
            p +="</tr>";
        }
    });
    //aggregates
    if(!$.isEmptyObject(aggregates) ){
        p +="<tr>";
        columnNames.forEach(function(d){
            if(aggregates[d]){
                p +="<th> Total "+draw._tableSetting.footer[d]['operation']+" : "+aggregates[d]+"</th>";
            }
            else
                p +="<th>&nbsp;</th>";
        });
        p +="</tr>";
    }
    function rowObj(value){
        var i=0;
        $.each(value,function(k,v){
            i++;
            var rLength=1;
            if(isObject(v)){
                rLength=getAllChildren(v).length;
            }
            if(i===1){
                var tableData=k.split('#');
                if(rLength>1){ classText="tablesorter-childRow";}
                var str=tableData[0];
                if(!isNaN(str))
                {
                    if(str.includes("."))
                        str=parseFloat(str).toFixed(2);
                    else
                        str=parseInt(str);
                }
                r +="<td rowspan='"+rLength+"'>"+str+"</td>";
                if(isObject(v)){
                    rowObj(v);
                }else{
                    r +="</tr>";
                }
            }
            else{
                var tableData=k.split('#');
                if(rLength>1){ classText="tablesorter-childRow";}
                var str=tableData[0];
                if(!isNaN(str))
                {
                    if(str.includes("."))
                        str=parseFloat(str).toFixed(2);
                    else
                        str=parseInt(str);

                }
                r +="<tr class='"+classText+"'><td rowspan='"+rLength+"' >"+str+"</td>";
                if(isObject(v)){
                    rowObj(v);
                }else{
                    r +="</tr>";
                }
            }
        });
        return r;
    }
    $("#table-" + draw._container).append("<tbody>"+p+"</tbody>");
    setTimeout(function(){
        try{
            $("#table-"+draw._container).tablesorter({
                widgets: [ 'zebra', 'resizable', 'stickyHeaders' ],
                widgetOptions: {
                    resizable: true,
                    // These are the default column widths which are used when the table is
                    // initialized or resizing is reset; note that the "Age" column is not
                    // resizable, but the width can still be set to 40px here

                }});
        }catch(e){
            
        }

        /*$().dataTable({

         });*/

    },1000);

    return true;
}