(function () {
    function _draw(dc, crossfilter) {
        var draw = {
            version: '1.0.0-dev',
            nodeApiUrl: configData.nodeUrl,
            constants: {
                LINE_CHART: "_lineChart",
                PIE_CHART: "_pieChart",
                BAR_CHART: "_barChart",
                NUMBER_WIDGET: "_numberWidget",
                BUBBLE_CHART: "_bubbleChart",
                BUBBLE_CHART: "_bubbleChart",
                COMPOSITE_CHART: "_compositeChart",
                STACK_CHART: "_stackChart",
                BAR_LINE_CHART: "_compositeLineBarChart",
                HEAT_CHART: "_heatChart"
            },
            calculateFunctionList: [
                {
                    "key": "if",
                    "operator": "if([condition]){return [value];}",
                    "description": "Syntax: if(condition){ return [value] }else{return [value]}"
                },
                {
                    "key": "ceil",
                    "operator": "Math.ceil()",
                    "description": "Ceil rounds a decimal value up to the next highest integer.Syntax: Math.ceil(decimal)"
                },
                {
                    "key": "concat",
                    "operator": "concat()",
                    "description": "concat concatenates multiple strings.Syntax: expression1.concat(expression2, expression3, expression4... )"
                },
                {
                    "key": "dateDiff",
                    "operator": "dateDiff('','','MM-DD-YYYY')",
                    "description": "dateDiff evaluates the difference between two date strings in days.Syntax: dateDiff(date, date)"
                },
                {
                    "key": "decimalToInt",
                    "operator": "decimalToInt()",
                    "description": "decimalToInt converts a decimal value to the integer data type by stripping off the decimal point and any numbers after it.                      Syntax: decimalToInt(decimal)"
                },
                {
                    "key": "floor",
                    "operator": "Math.floor()",
                    "description": "floor rounds a decimal value down to the closest, lowest integer. Syntax: Math.floor(decimal)"
                },
                {
                    "key": "formatDate",
                    "operator": "formatDate()",
                    "description": "formatDate formats a date using a specified pattern.Syntax: formatDate(date, ['format'], ['time_zone'])"
                },
                {
                    "key": "intToDecimal",
                    "operator": "parseFloat().toFixed(2)",
                    "description": "intToDecimal converts an integer value to the decimal data type.Syntax: parseFloat(expression).toFixed(number)"
                },
                {
                    "key": "isNotNull",
                    "operator": "isNotNull()",
                    "description": "isNotNull evaluates an expression and returns true if the expression is not null.Syntax: isNotNull(expression)"
                },
                {
                    "key": "isNull",
                    "operator": "isNull()",
                    "description": "isNull evaluates an expression and returns true if the expression is null.Syntax: isNull(expression)"
                },
                /*
                 * { "key" : "left", "operator" :
                 * "left(,)", "description" :
                 * "left returns a specific
                 * number of characters starting
                 * with the leftmost
                 * character.Syntax:
                 * left(expression, limit)" },
                 */
                /*{
                 "key": "ltrim",
                 "operator": "ltrim()",

                 "description": "ltrim removes preceding whitespace from a string.Syntax: ltrim(expression)"
                 },*/ /*
                 * { "key" : "now", "operator" :
                 * "now()", "description" : "now
                 * returns the UTC date and
                 * time, in the format
                 * yyyy-MM-ddTkk:mm:ss:SSSZ.Syntax:
                 * now()" },
                 */
                /*{
                 "key": "nullIf",
                 "operator": "nullIf()",
                 "description": "nullIf compares expression 1 and expression 2. If they are equal, then returns null, else returns expression 1.Syntax: nullIf                     (expression1, expression2)"
                 },*/
                {
                    "key": "parseDate",
                    "operator": "parseDate()",
                    "description": "parseDate parseDate parses a string to determine if it contains a date value. This function returns all rows that contain a                       date in a valid format and skips any rows that do not, including rows that contain null values.Syntax: parseDate(date, ['format'], [                             'time_zone'])"
                },
                /*
                 * { "key" : "parseDecimal",
                 * "operator" :
                 * "parseDecimal()",
                 * "description" : "parseDecimal
                 * parses a string to determine
                 * if it contains a decimal
                 * value.Syntax:
                 * parseDecimal(expression)" },
                 */
                {
                    "key": "replace",
                    "operator": "replace()",
                    "description": "replace replaces part of a string with another specified string.Syntax: replace(expression, substring, replacement)"
                },
                {
                    "key": "round",
                    "operator": "round()",
                    "description": "round rounds a decimal value to the closest integer, with decimal values of .5 or larger being rounded up. Syntax: round(decimal)"
                },
                {
                    "key": "min",
                    "operator": "min()",
                    "description": "Minimum of data: min(expression)"
                },
                {
                    "key": "max",
                    "operator": "max()",
                    "description": "Maximum of data: max(expression)"
                },
                {
                    "key": "strlen",
                    "operator": "strlen()",
                    "description": "strlen returns the number of characters in a string.Syntax: strlen(expression)"
                },
                {
                    "key": "toLower",
                    "operator": "toLower()",
                    "description": "toLower formats string in all lowercase; skips rows containing null values.Syntax: toLower(expression)"
                },
                {
                    "key": "toUpper",
                    "operator": "toUpper()",
                    "description": "toUpper formats string in all uppercase; skips rows containing null values.Syntax: toLower(expression)"
                },
                {
                    "key": "toString",
                    "operator": "toString()",
                    "description": "toString formats the input expression as a string; skips rows containing null values.Syntax: toString(expression)"
                },
                {
                    "key": "trim",
                    "operator": "trim()",
                    "description": "trim removes both preceding and following whitespace from a string.Syntax: trim(expression)"
                },
                {
                    "key": "count",
                    "operator": "count()",
                    "description": "it counts the no. of fields.  Syntax: count(expression)"
                },
                {
                    "key": "sum",
                    "operator": "sum()",
                    "description": "it sum the  field.  Syntax: sum(expression)"
                },
                {
                    "key": "avg",
                    "operator": "avg()",
                    "description": "it Average the  fields.  Syntax: avg(expression)"
                },
                {
                    "key": "DateMonthAdd",
                    "operator": "DateAdd('months',i,d,'DD-MM-YYYY')",
                    "description": "Add months in date Syntax: DateAdd('date_part','interval',date,format)"
                },
                {
                    "key": "DateYearAdd",
                    "operator": "DateAdd('year',i,d,'DD-MM-YYYY')",
                    "description": "Add years in date Syntax: DateAdd('date_part','interval',date,format)"
                },
                {
                    "key": "DateDayAdd",
                    "operator": "DateAdd('days',i,d,'DD-MM-YYYY')",
                    "description": "Add days in date Syntax: DateAdd('date_part','interval',date,format)"
                },
                {
                    "key": "Current Date",
                    "operator": "currDate('DD-MM-YYYY')",
                    "description": "Current date Syntax: currDate('DD-MM-YYYY hh:mm:ss')"
                },
                {
                    "key": "dateTimeDiff",
                    "operator": "dateTimeDiff('hh:mm:ss', date1,date2)",
                    "description": "Date time difference dateTimeDiff('hh:mm:ss', date1,date2)"
                },
                {
                    "key": "dcount",
                    "operator": "dcount()",
                    "description": "Distinct Counts the no. of fields.  Syntax: dcount(Column Name)"
                },
                {
                    "key": "AGGR",
                    "operator": "AGGR()",
                    "description": "Distinct Aggregate by primary key.  Syntax: AGGR(dimension, count(Column Name), Primary Key)"
                },
            ],
            operatorsList: [
                {
                    "key": "Addition",
                    "value": "+",
                    "iconValue": "+",
                    "icon": "fa fa-plus"
                }, {
                    "key": "Subtraction",
                    "value": "-",
                    "iconValue": "-",
                    "icon": "fa fa-minus"
                }, {
                    "key": "Multiplication",
                    "value": "*",
                    "iconValue": "*",
                    "icon": "fa fa-asterisk"
                }, {
                    "key": "Division",
                    "value": "/",
                    "iconValue": "/",
                    "icon": "fa fa-divide"
                }
            ],
            chartData: [
                {
                    "name": "Line Chart",
                    "id": 0,
                    "key": "_lineChartJs",
                    "value": "theme/assets/chart_svg/graph-presentation-chart-analytics-39da9f76fa0ec03b.svg",
                    "required": {
                        "Dimension": 1,
                        "Measure": [1, 11],
                        "Aggregate": [1, 11],
                        "modelRequired":false
                    },
                    "containerSize": {
                        "x": 12,
                        "y": 12
                    },
                    "image": "theme/assets/chart_svg/graph-presentation-chart-analytics-39da9f76fa0ec03b.svg",

                },
                {
                    "name": "Area Chart",
                    "id": 0,
                    "key": "_areaChartJs",
                    "value": "theme/assets/chart_svg/graph-presentation-chart-analytics-3ff245ddd0c3ed1c.svg",
                    "required": {
                        "Dimension": 1,
                        "Measure": [1, 11],
                        "Aggregate": [1, 11],
                        "modelRequired":false
                    },
                    "containerSize": {
                        "x": 18,
                        "y": 15
                    },
                    "image": "theme/assets/chart_svg/graph-presentation-chart-analytics-3ff245ddd0c3ed1c.svg",

                },
                {
                    "id": 1,
                    "name": "Bar Chart",
                    "key": "_barChartJs",
                    "value": "theme/assets/chart_svg/graph-presentation-chart-analytics-34db6bd89f97ecfd.svg",
                    "required": {
                        "Dimension": 1,
                        "Measure": [1, 11],
                        "Aggregate": [1, 11],
                        "modelRequired":false
                    },
                    "containerSize": {
                        "x": 18,
                        "y": 15
                    },
                    "image": "theme/assets/chart_svg/graph-presentation-chart-analytics-34db6bd89f97ecfd.svg",

                },
                {
                    "id": 111,
                    "name": "Row Chart",
                    "key": "_rowChartJs",
                    "value": "theme/assets/chart_svg/graph-presentation-chart-analytics-344ac7ff4ac09c1c.svg",
                    "required": {
                        "Dimension": 1,
                        "Measure": [1, 11],
                        "Aggregate": [1, 11],
                        "modelRequired":false
                    },
                    "containerSize": {
                        "x": 18,
                        "y": 15
                    },
                    "image": "theme/assets/chart_svg/graph-presentation-chart-analytics-344ac7ff4ac09c1c.svg",

                },
                {
                    "name": "Pie Chart",
                    "id": 2,
                    "key": "_pieChartJs",
                    "value": "theme/assets/chart_svg/graph-pie-chart-business-presentation-statistic-315e35f158e6a32f.svg",
                    "required": {
                        "Dimension": 1,
                        "Measure": [1, 1],
                        "Aggregate": [1, 2],
                        "modelRequired":false
                    },
                    "containerSize": {
                        "x": 18,
                        "y": 15
                    },
                    "image": "theme/assets/chart_svg/graph-pie-chart-business-presentation-statistic-315e35f158e6a32f.svg",
                },
                {
                    "id": 10,
                    "name": "Number Widget",
                    "key": "_numberWidget",
                    "value": "theme/assets/chart_svg/number-sort-numerically-38bc62731879e3c0.svg",
                    "required": {
                        "Dimension": 1,
                        "Measure": [1, 1],
                        "Aggregate": [1, 1],
                        "modelRequired":false
                    },
                    "image": "theme/assets/chart_svg/number-sort-numerically-38bc62731879e3c0.svg",
                    "attributes": ["labelColor", "NumberFormate"]
                },
                {
                    "id": 3,
                    "name": "Bubble Chart",
                    "key": "_bubbleChartJs",
                    "value": "theme/assets/chart_svg/graph-presentation-chart-analytics-3a378f15ed4111e5.svg",
                    "required": {
                        "Dimension": 0,
                        "Measure": [0, 0],
                        "Aggregate": [1, 11],
                        "modelRequired":true
                    },
                    "containerSize": {
                        "x": 18,
                        "y": 15
                    },
                    "image": "theme/assets/chart_svg/graph-presentation-chart-analytics-3a378f15ed4111e5.svg",
                },
                {
                    "id": 5,
                    "name": "Composite Chart",
                    "key": "_compositeChartJs",
                    "value": "theme/assets/chart_svg/graph-presentation-chart-analytics-31172df6d09476fb.svg",
                    "required": {
                        "Dimension": 1,
                        "Measure": [1, 11],
                        "Aggregate": [1, 11],
                        "modelRequired":false
                    },
                    "containerSize": {
                        "x": 18,
                        "y": 15
                    },
                    "image": "theme/assets/chart_svg/graph-presentation-chart-analytics-31172df6d09476fb.svg",
                    "attributes": ["chartWidth", "chartHeight",
                        "labelColor", "chartColor", "xAxisLabelName",
                        "yAxisLabelName", "xAxisMargin", "yAxisMargin",
                        "rotateChart", "rotateLabel", "TranslateXaxis",
                        "TranslateYaxis"]
                },
                {
                    "name": "DataTable",
                    "key": "_dataTable",
                    "value": "theme/assets/chart_svg/spreadsheet-cell-row.svg",
                    "required": {
                        "Dimension": 1,
                        "Measure": [1, 50],
                        "Aggregate": [1, 1],
                        "modelRequired":false
                    },
                    "containerSize": {
                        "x": 18,
                        "y": 15
                    },
                    "image": "theme/assets/chart_svg/spreadsheet-cell-row.svg",
                },

                /*{
                 "name": "X Table",
                 "key": "_xTable",
                 "value": "assets/img/chart-images/table.png",
                 "required": {
                 "Dimension": 0,
                 "Measure": [0, 0],
                 "Aggregate": [0, 0]
                 },
                 "image": "assets/img/chart-images/table.png",
                 "attributes": [
                 "chartWidth", "chartHeight", "labelColor", "chartColor",
                 "xAxisLabelName", "yAxisLabelName", "xAxisMargin",
                 "yAxisMargin", "rotateChart", "rotateLabel"]
                 },*/

                // {
                //     "name": "Pivot Table",
                //     "key": "_pivotTable",
                //     "value": "theme/assets/chart_svg/table-for-data.svg",
                //     "required": {
                //         "Dimension": 0,
                //         "Measure": [0, 0],
                //         "Aggregate": [0, 0],
                //         "modelRequired":true
                //     },
                //     "containerSize": {
                //         "x": 18,
                //         "y": 15
                //     },
                //     "image": "theme/assets/chart_svg/table-for-data.svg",
                // },
                {
                    "name": "Pivot Table Customized",
                    "key": "_PivotCustomized",
                    "value": "theme/assets/chart_svg/table-for-data.svg",
                    "required": {
                        "Dimension": 1,
                        "Measure": [1, 50],
                        "Aggregate": [1, 50],
                        "modelRequired":false
                    },
                    "containerSize": {
                        "x": 18,
                        "y": 15
                    },
                    "image": "theme/assets/chart_svg/table-for-data.svg",
                },
                {
                    "name": "Line + Bar Chart",
                    "id": 0,
                    "key": "_lineBarChartJs",
                    "value": "theme/assets/chart_svg/bar-chart.svg",
                    //                    "value": "assets/img/chart-images/line.png",
                    "required": {
                        "Dimension": 1,
                        "Measure": [1, 11],
                        "Aggregate": [1, 11],
                        "modelRequired":false
                    },
                    "containerSize": {
                        "x": 18,
                        "y": 15
                    },
                    "image": "theme/assets/chart_svg/bar-chart.svg",
                },
                {
                    "name": "Dual Axis Chart",
                    "id": 13,
                    "key": "_dualAxisChartJs",
                    "value": "theme/assets/chart_svg/bar-chart.svg",
                    // "value": "assets/img/chart-images/line.png",
                    "required": {
                        "Dimension": 1,
                        "Measure": [2, 2],
                        "Aggregate": [1, 11],
                        "modelRequired":false
                    },
                    "containerSize": {
                        "x": 18,
                        "y": 15
                    },
                    "image": "theme/assets/chart_svg/bar-chart.svg",
                },
                {
                    "name": "Male & Female Comparison",
                    "id": 11,
                    "key": "_maleFemaleChartJs",
                    "value": "theme/assets/chart_svg/female-and-male-shapes-silhouettes-outlines.svg",
                    "required": {
                        "Dimension": 0,
                        "Measure": [0, 0],
                        "Aggregate": [1, 1],
                        "modelRequired":true
                    },
                    "containerSize": {
                        "x": 18,
                        "y": 15
                    },
                    "image": "theme/assets/chart_svg/female-and-male-shapes-silhouettes-outlines.svg",
                },
                // {
                //     "name": "Building Comparison",
                //     "id": 12,
                //     "key": "_buildingChartJs",
                //     "value": "theme/assets/chart_svg/skyline.svg",
                //     "required": {
                //         "Dimension": 0,
                //         "Measure": [0, 0],
                //         "Aggregate": [1, 1]
                //     },
                //     "containerSize": {
                //         "x": 18,
                //         "y": 15
                //     },
                //     "image": "theme/assets/chart_svg/skyline.svg",
                // },
                {
                    "name": "Map",
                    "id": 13,
                    "key": "_mapChartJs",
                    "value": "theme/assets/chart_svg/continents.svg",
                    "required": {
                        "Dimension": 0,
                        "Measure": [0, 0],
                        "Aggregate": [1, 1],
                        "modelRequired":true
                    },
                    "containerSize": {
                        "x": 18,
                        "y": 15
                    },
                    "image": "theme/assets/chart_svg/continents.svg",
                },
                // {
                //     "name": "Floor Plan",
                //     "id": 14,
                //     "key": "_floorPlanJs",
                //     "value": "theme/assets/chart_svg/rectangle.svg",
                //     "required": {
                //         "Dimension": 1,
                //         "Measure": [1, 1],
                //         "Aggregate": [1, 1],
                //         "modelRequired":true
                //     },
                //     "image": "theme/assets/chart_svg/rectangle.svg",
                //     "containerSize": {
                //         "x": 18,
                //         "y": 15
                //     }
                // },
                {
                    "name": "Word Cloud",
                    "id": 15,
                    "key": "_wordCloud",
                    "value": "theme/assets/chart_svg/sort-alphabetically-3c87e21ad588398c.svg",
                    "required": {
                        "Dimension": 1,
                        "Measure": [1, 1],
                        "Aggregate": [1, 1],
                        "modelRequired":false
                    },
                    "image": "theme/assets/chart_svg/sort-alphabetically-3c87e21ad588398c.svg",
                    "containerSize": {
                        "x": 18,
                        "y": 15
                    }
                },
                {
                    "name": "SpeedoMeter",
                    "id": 15,
                    "key": "_speedoMeterChart",
                    "value": "theme/assets/chart_svg/speedometer-3cefa1e910acab93.svg",
                    "required": {
                        "Dimension": 1,
                        "Measure": [1, 1],
                        "Aggregate": [1, 1],
                        "modelRequired":false
                    },
                    "image": "theme/assets/chart_svg/speedometer-3cefa1e910acab93.svg",
                    "containerSize": {
                        "x": 4,
                        "y": 8
                    },
                },
                {
                    "name": "Text/Image",
                    "id": 16,
                    "key": "_TextImageChart",
                    "value": "theme/assets/chart_svg/text.svg",
                    "required": {
                        "Dimension": 0,
                        "Measure": [0, 0],
                        "Aggregate": [1, 1],
                        "modelRequired":true
                    },
                    "image": "theme/assets/chart_svg/text.svg",
                    "containerSize": {
                        "x": 18,
                        "y": 15
                    },
                },
                {
                    "name": "BoxPlot",
                    "id": 17,
                    "key": "_BoxPlotChart",
                    "value": "theme/assets/chart_svg/box-plot-chart-interface-symbol.svg",
                    "required": {
                        "Dimension": 1,
                        "Measure": [1, 1],
                        "Aggregate": [1, 1],
                        "modelRequired":false
                    },
                    "image": "theme/assets/chart_svg/box-plot-chart-interface-symbol.svg",
                    "containerSize": {
                        "x": 18,
                        "y": 15
                    },
                },
                {
                    "name": "Funnel Chart",
                    "id": 18,
                    "key": "_funnelChart",
                    "value": "theme/assets/chart_svg/funnel-chart.svg",
                    "required": {
                        "Dimension": 1,
                        "Measure": [1, 1],
                        "Aggregate": [1, 1],
                        "modelRequired":false
                    },
                    "image": "theme/assets/chart_svg/funnel-chart.svg",
                    "containerSize": {
                        "x": 18,
                        "y": 15
                    },
                },
            ],
            dataTypeMeasure: [
                {
                    "name": "Integer",
                    "type": "int"
                }, {
                    "name": "Double",
                    "type": "double"
                }, {
                    "name": "Decimal",
                    "type": "decimal"
                }, {
                    "name": "Float",
                    "type": "float"
                }],
            dataTypeDimension: [
                {
                    "name": "Integer",
                    "type": "int"
                }, {
                    "name": "Double",
                    "type": "double"
                }, {
                    "name": "Decimal",
                    "type": "decimal"
                }, {
                    "name": "Float",
                    "type": "float"
                }, {
                    "name": "Date",
                    "type": "date"
                }, {
                    "name": "Date Time",
                    "type": "datetime"
                }, {
                    "name": "String",
                    "type": "text"
                }, {
                    "name": "Varchar",
                    "type": "varchar"
                }, {
                    "name": "Char",
                    "type": "char"
                }],
            axisData: [{
                "Name": "Xaxis",
                "label": "X Axis",
                "modelName": "Xaxis",
                "modelValue": "Xaxis",
                "columnType": ['Dimension', 'DimensionCustom'],
                "visible": false
            }, {
                "Name": "Yaxis",
                "label": "Y Axis",
                "modelName": "Yaxis",
                "modelValue": "Yaxis",
                "columnType": ['Dimension', 'DimensionCustom'],
                "visible": false
            }, {
                "Name": "Maxis",
                "label": "Measure",
                "modelName": "Maxis",
                "modelValue": "Maxis",
                "columnType": ['Measure', 'MeasureCustom'],
                "visible": false
            }, {
                "Name": "Aggregate",
                "label": "Aggregate",
                "modelName": "Aggregate",
                "modelValue": "Aggregate",
                "columnType": ['Aggregate'],
                "visible": false
            }, {
                "Name": "M2axis",
                "label": "Measure(Chart 2)",
                "modelName": "M2axis",
                "modelValue": "M2axis",
                "columnType": ['Measure', 'MeasureCustom'],
                "visible": false
            }, {
                "Name": "Aggregate2",
                "label": "Aggregate(Chart 2)",
                "modelName": "Aggregate2",
                "modelValue": "Aggregate2",
                "columnType": ['Aggregate'],
                "visible": false
            }],

            AggregatesObject: [
                {
                    "key": "count",
                    "value": "Count",
                    "type": "Aggregate"
                },
                {
                    "key": "sumIndex",
                    "value": "Sum",
                    "type": "Aggregate"
                },
                {
                    "key": "avgIndex",
                    "value": "Average",
                    "type": "Aggregate"
                },

                /*{
                    "key": "median",
                    "value": "Median",
                    "type": "Aggregate"
                },
                {
                    "key": "stdDev",
                    "value": "Variance",
                    "type": "Aggregate"
                },*/
                {
                    "key": "countDistinct",
                    "value": "Count Distinct",
                    "type": "Aggregate"
                },
                // {
                //     "key": "totalDistinct",
                //     "value": "Sum Distinct",
                //     "type": "Aggregate"
                // },
            ],

            aggrMeasureObject: [
                {
                    "key": "avg",
                    "value": "Average",
                    "type": "Aggregate"
                },
                {
                    "key": "count",
                    "value": "Count",
                    "type": "Aggregate"
                },
                {
                    "key": "sum",
                    "value": "Sum",
                    "type": "Aggregate"
                },
                {
                    "key": "min",
                    "value": "Minimum",
                    "type": "Aggregate"
                },
                {
                    "key": "max",
                    "value": "Maximum",
                    "type": "Aggregate"
                },
            ],
            aggrMeasureObjectTooltip: [
                {
                    "key": "avg",
                    "value": "Average",
                    "type": "Aggregate"
                },
                {
                    "key": "count",
                    "value": "Count",
                    "type": "Aggregate"
                },
                {
                    "key": "sum",
                    "value": "Sum",
                    "type": "Aggregate"
                },
                {
                    "key": "min",
                    "value": "Minimum",
                    "type": "Aggregate"
                },
                {
                    "key": "max",
                    "value": "Maximum",
                    "type": "Aggregate"
                },
            ],
            aggrDimensionObject: [
                {
                    "key": "all",
                    "value": "All",
                    "type": "Aggregate"
                },
                {
                    "key": "count",
                    "value": "Count",
                    "type": "Aggregate"
                },
                {
                    "key": "countDistinct",
                    "value": "Count Distinct",
                    "type": "Aggregate"
                },
                {
                    "key": "min",
                    "value": "Minimum",
                    "type": "Aggregate"
                },
                {
                    "key": "max",
                    "value": "Maximum",
                    "type": "Aggregate"
                },
            ],

            attributesData: [
                {
                    "Name": "labelColor",
                    "label": "Label Color",
                    "modelName": "labelColor",
                    "modelValue": "labelColor",
                    "type": "color",
                    "mode": function () {
                        return this.modelValue + "show";
                    },
                    "visible": false
                },
                {
                    "Name": "chartColor",
                    "label": "Chart Color",
                    "modelName": "chartColor",
                    "modelValue": "chartColor",
                    "type": "color",
                    "default": null,
                    "mode": function () {
                        return this.modelValue + "show";
                    },
                    "visible": false
                },
                {
                    "Name": "xAxisLabelName",
                    "label": "X Axis Label Name",
                    "modelName": "xAxisLabelName",
                    "modelValue": "xAxisLabelName",
                    "type": "text",
                    "default": null,
                    "mode": function () {
                        return this.modelValue + "show";
                    },
                    "visible": false
                },
                {
                    "Name": "yAxisLabelName",
                    "label": "Y Axis Label Name",
                    "modelName": "yAxisLabelName",
                    "modelValue": "yAxisLabelName",
                    "type": "text",
                    "mode": function () {
                        return this.modelValue + "show";
                    },
                    "visible": false
                },
                {
                    "Name": "xAxisMargin",
                    "label": "X Axis Margin",
                    "modelName": "xAxisMargin",
                    "modelValue": "xAxisMargin",
                    "type": "text",
                    "mode": function () {
                        return this.modelValue + "show";
                    },
                    "visible": false
                },
                {
                    "Name": "yAxisMargin",
                    "label": "y Axis Margin",
                    "modelName": "yAxisMargin",
                    "modelValue": "yAxisMargin",
                    "type": "text",
                    "mode": function () {
                        return this.modelValue + "show";
                    },
                    "visible": false
                },
                {
                    "Name": "rotateChart",
                    "label": "Rotate Chart",
                    "modelName": "rotateChart",
                    "modelValue": "rotateChart",
                    "type": "select",
                    "options": function () {
                        if (!this.tempArray) {
                            this.tempArray = [];
                            for (var i = 0; i < 9; i++) {
                                var val = i * 45;
                                this.tempArray.push({
                                    "text": val,
                                    "value": val
                                });
                            }
                        }
                        return this.tempArray;
                    },
                    "mode": function () {
                        return this.modelValue + "show";
                    },
                    "visible": false
                },
                {
                    "Name": "rotateLabel",
                    "label": "Rotate Label",
                    "modelName": "rotateLabel",
                    "modelValue": "rotateLabel",
                    "type": "select",
                    "options": function () {
                        if (!this.tempArray) {
                            this.tempArray = [];
                            for (var i = 0; i < 9; i++) {
                                var val = i * 45;
                                this.tempArray.push({
                                    "text": val,
                                    "value": val
                                });
                            }
                        }

                        return this.tempArray;
                    },
                    "mode": function () {
                        return this.modelValue + "show";
                    },
                    "visible": false
                },
                {
                    "Name": "TranslateXaxis",
                    "label": "Translate X Axis",
                    "modelName": "translateX",
                    "modelValue": "translateX",
                    "type": "number",
                    "mode": function () {
                        return this.modelValue + "show";
                    },
                    "visible": false
                },
                {
                    "Name": "TranslateYaxis",
                    "label": "Translate Y Axis",
                    "modelName": "translateY",
                    "modelValue": "translateY",
                    "type": "number",
                    "mode": function () {
                        return this.modelValue + "show";
                    },
                    "visible": false
                },
                {
                    "Name": "chartWidth",
                    "label": "Chart Width",
                    "modelName": "cWidth",
                    "modelValue": "cWidth",
                    "type": "text",
                    "default": "",
                    "mode": "widthShow",
                    "visible": false
                },
                {
                    "Name": "chartHeight",
                    "label": "Chart Height",
                    "modelName": "cHeight",
                    "modelValue": "cHeight",
                    "type": "text",
                    "default": "",
                    "mode": "heightShow",
                    "visible": false,
                },
                {
                    "Name": "NumberFormate",
                    "label": "Number Formate",
                    "modelName": "NumberFormate",
                    "modelValue": "NumberFormate",
                    "type": "select",
                    "options": function () {
                        if (!this.arrayOption) {
                            this.arrayOption = [
                                {
                                    text: "Normal",
                                    value: "Normal"
                                },
                                {
                                    text: "Formate",
                                    value: "Formate"
                                },
                                {
                                    text: "Percentage",
                                    value: "%"
                                },
                                {
                                    text: "Doller",
                                    value: "$"
                                },
                                {
                                    text: "Rupees",
                                    value: "INR"
                                }
                            ];
                        }
                        return this.arrayOption;
                    },
                    "mode": function () {
                        return this.modelValue + "show";
                    },
                    "visible": false
                }
            ],

            _dateFormats: {
                currentFormat: "dd-mm-yyyy",
                formats: [
                    {
                        value: "dd-mm-yyyy",
                        specifier: "%d-%M-%y",
                    },
                    {
                        value: "dd-mm-yyyy",
                        specifier: "%d/%M/%y",
                    }
                ],
                groups: [
                    {
                        name: "Day",
                        specifier: "%d",
                    },
                    {
                        name: "Month",
                        specifier: "%M",
                    },
                    {
                        name: "Year",
                        specifier: "%y",
                    }
                ]
            },

            _MapGroupColor: [],
            _MapTooltip: [],
            TooltipStr: [],
            FloorMeasure: [],
            xCoordVal: [],
            yCoordVal: [],
            _MapColors: [
                '#FF0000', '#0000FF', '#008000', '#202020', '#A2745A', '#7E909A', '#1C4E80', '#A5D8DD', '#EA6A47', '#0091D5',
                '#FE8B00', '#F1464D', '#63BAB4', '#37A846', '#F1CD14', '#B972A3', '#FF95A3', '#FF7800', '#00A815', '#4676AC',
                '#E70000', '#9E59C2', '#935346', '#b03719', '#120081', '#aa67f5', '#b00019', '#53c487', '#0cdef7', '#0073BA',
                '#156242', '#00ffbf', '#1796a1', '#ffc400', '#17fffc', '#ff00ff', '#535e87', '#53c487', '#4979a7', '#C44429',
                '#59a14f', '#9c755f', '#f28e2b', '#edc948', '#bab0ac', '#e15759', '#b07aa1', '#76b7b2', '#ff9da7', '#574EE1'
            ],
            _renderlet: null,
            _chartConfig: null,
            _data: null,
            _container: null,
            _dimension: {},
            _measure: {},
            _crossfilter: null,
            _categoryData: null,
            _categoryObj: null,
            _groupExpression: null,
            _measureCustom: null,
            _dimensionCustom: null,
            _datatable: [],
            _datatableArray: [],
            _noOfLines: 0,
            _noOfBars: 0,
            _lineMeasure: [],
            _barMeasure: [],
            _colorPalette: ["#2196F3", "#3F51B5", "#8BC34A", "#FFC107", "#FFEB3B", "#FF9800", "#FF5722", "#607D8B", "#F44336", "#E3F2FD", "#BBDEFB", "#90CAF9",
                "#64B5F6", "#42A5F5", "#2196F3", "#1E88E5", "#1976D2", "#1565C0", "#0D47A1", "#FFFDE7", "#FFF9C4", "#FFF59D", "#FFF176", "#FFEE58", "#FFEB3B",
                "#FDD835", "#FBC02D", "#F9A825"],
            _timeline: null,
            _option: null,
            _pivotCust_Arr: [],
            _totalReportCount:0,
            _loadedReportCount:0,
            _MapImgObject: {},
            /*
             * "rgb(0,206,209)", "rgb(0,139,139)", "rgb(100,149,237)",
             * "rgb(238,130,238)", "rgb(112,128,144)", "rgba(248, 208, 83, 1)",
             * "rgba(245, 87, 83, 1)", "rgba(230, 230, 230, 1)", "rgba(98, 98,
             * 98, 1)"
             */
        };






        draw.getBaseUrl=function(){
            var pathArray = window.location.pathname.split('/');
            return pathArray;
        }

        draw.reportLoadedCountCheck = function(chart){
            draw._loadedReportCount++;
            if(draw._totalReportCount == draw._loadedReportCount){
                setTimeout(function(){
                    //$(".loadingBar").hide();
                },1000);
            }
        }

        draw.globalRegistry=(function(){
            var _chartMap = {};
            var _chartAttributes = {};
            return {
                /**
                 * Determine if a given chart instance resides in the registry.
                 *
                 * @method has
                 *
                 * @returns {Boolean}
                 */
                has: function (chart) {
                    if (_chartMap[chart.id]) {
                        return true;
                    }
                    return false;
                },
                syncLocalRegistries:function(chartId,type){
                    if(type=="sketch"){
                        if(eChart.chartRegistry.has(chartId)){
                            eChartServer.chartRegistry.deregister(chartId);
                        }
                    }if(type=="eChart"){
                        if(sketch.chartRegistry.has(chartId)){
                            draw.chartRegistry.deregister(chartId);
                        }
                    }
                },
                /**
                 * Add given chart instance .
                 *
                 */
                register: function (chart) {
                    _chartMap[chart.id] = chart;
                },
                /**
                 *
                 * @method registerAttributes
                 * @memberof eCharts.chartRegistry
                 *
                 */
                registerColor: function (chart) {
                    if (_chartAttributes[chart.id]) {
                        _chartAttributes[chart.id]['color'] = chart._colorSelection;
                    } else {
                        _chartAttributes[chart.id] = {colors: chart._colorSelection};
                    }
                },
                /**
                 *
                 * @method registerAttributes
                 * @memberof eCharts.chartRegistry
                 *
                 */
                registerAllAttributes: function (chartAttributes) {
                    _chartAttributes = chartAttributes;
                },
                /**
                 *
                 * @method registerAttributes
                 * @memberof eCharts.chartRegistry
                 *
                 */
                listAttributes: function () {
                    return _chartAttributes;
                },
                /**
                 * Remove given chart instance
                 */
                deregister: function (chart) {
                    delete _chartMap[chart._container];
                },
                /**
                 *
                 * @method clear
                 * @memberof eCharts.chartRegistry
                 *
                 */
                clear: function () {
                    // delete _chartMap;
                },
                /**
                 *
                 * @method list
                 * @memberof eCharts.chartRegistry
                 *
                 */
                list: function () {
                    var list = [];
                    $.each(_chartMap, function (key, value) {
                        list.push(value);
                    });
                    return list;
                },
            };
        })();

        // -----------------------------Chartjs functions------------------------------Start
        draw.rgba = function (dataLength) {
            var rgb = [];
            for (var i = 0; i < dataLength; i++)
                rgb.push('rgba(' + Math.floor(Math.random() * 255) + ',' + Math.floor(Math.random() * 255) + ',' + Math.floor(Math.random() * 255) + ',' + 1 + ')');
            return rgb;
        };

        draw._rotateLabels = function (divID) {
            // rotate bar chart labels so they don't overlap
            d3.selectAll(divID + ' > svg > g > .axis.x > .tick > text').style(
                "font-size", "10px").style("text-anchor", "end").attr(
                "transform", function (d) {
                    return "rotate(-45) translate(-10,-12)";
                });
        };

        /*
         * draw.timeLine=function(timeline){
         *
         * draw._timeline=timeline; return draw; }
         */

        // -----------------------------Chartjs functions------------------------------End
        draw.setGroupExpression = function (exp) {
            draw._groupExpression = exp;
        };

        draw.getDateFormats = function () {
            return draw._dateFormats.formats;
        };

        draw.chartJsArray = [];
        draw.categoryData = function (categoryData) {
            draw._categoryData = categoryData;
            var obj = {};
            var keys = Object.keys(categoryData.selectedItems);
            keys.forEach(function (index) {
                categoryData.selectedItems[index].forEach(function (d) {
                        obj[d] = categoryData.categoryName[index];
                    }
                )
            });
            draw._categoryObj = obj;
            return draw;
        };

        draw.getCategoryData = function () {
            return draw._categoryData;
        };

        draw.isGroupColorApplied = function () {
            return draw._dataFormat && draw._dataFormat['groupColor'];
        }

        draw.accessToken=function(accessToken){
            draw._accessToken=accessToken;
            return draw;
        }

        draw.getAccessToken=function(){
            return draw._accessToken;
        }

        draw.options = function (option) {
            draw._option = option;
            return draw;
        }

        draw.data = function (data) {
            // if(draw._data==null)
            // {
            draw._data = data;
            if (draw._crossfilter != null) {
                if (!draw._axisConfig.timeline) {
                    try {
                    } catch (e) {
                    }
                } else {
                    if (draw._crossfilter.size() > 0) {
                        draw._crossfilter.remove();
                    }
                    draw._crossfilter.add(draw._data);
                }
            } else {
                draw._crossfilter = crossfilter(draw._data);
            }
            // }
            return draw;
        };

        draw.getData = function () {
            return draw._data;
        };

        draw.resetData = function () {
            draw._data = null;
            draw._crossfilter = null;
        };

        draw.container = function (container) {
            draw._container = container;
            return draw;
        };

        draw.chartConfig = function (config) {
            draw._chartConfig = config;
            return draw;
        };

        draw._registerdDimensionList = {};
        draw._createDataTableDimension = function (container) {
            if (!draw._registerdDimensionList[container]) {
                if (!$.isEmptyObject(draw._dimension)) {
                    keyOfObj = Object.keys(draw._dimension)[0];
                    var dim = draw._crossfilter.dimension(function (d) {
                        return d[keyOfObj];
                    });
                    draw._registerdDimensionList[container] = dim;
                    return dim;
                } else {
                    return false;
                }
            } else {
                return  draw._registerdDimensionList[container];
            }
        }

        draw.axisConfig = function (config) {
            draw._axisConfig = config;
            draw._expandAxisConfig();
            return draw;
        };

        draw.selfAdjustChart = function (activeReport) {
            if (activeReport.chart.key != "_numberWidget") {
                var id = "chart-" + activeReport.reportContainer.id;
                draw.adjustXaxisLabels(id, activeReport);
                draw.applyTooltip(activeReport.chartObject);
                draw.adjustYaxisLabels(id);
            }
            // dc.renderAll();
        };

        draw.applyTooltip = function (chart) {
            try {
                dc.tooltipMixin(chart);
            } catch (e) {

            }
        }

        draw.adjustYaxisLabels = function (id) {
            try {
                var yAxisWidth = $("#" + id).find('svg').find('g').find('.y')[0].getBBox().width;
                var leftPadding = 0.00;
                if (yAxisWidth > 35) {
                    leftPadding = yAxisWidth - 36;
                    $("#" + id).find('svg').attr('style','padding-left:' + leftPadding);
                }
            } catch (e) {

            }
        };

        draw.adjustXaxisLabels = function (id, activeReport) {
            try {
                var containerWidth = activeReport.chartObject.root().node().parentNode.getBoundingClientRect().width;
                var xAxisWidth = $("#" + id).find('svg').find('g').find('.x')[0].getBBox().width;
                var xAxisChildren = $("#" + id).find('svg').find('g').find('.x').children();
                var lengthOfLabels = xAxisChildren.length;
                var totalWidthLabels = 0.0;
                var maxLabelWidth = 0;
                for (var i = 0; i < xAxisChildren.length; i++) {
                    var currentWidth = xAxisChildren[i].getBBox().width;
                    if (maxLabelWidth < currentWidth) {
                        maxLabelWidth = currentWidth;
                    }
                    // totalWidthLabels+=xAxisChildren[i].getBBox().width+30;
                }
                var absoluteWidthRequired = (maxLabelWidth / 3) * xAxisChildren.length;
                if (absoluteWidthRequired > xAxisWidth && absoluteWidthRequired > containerWidth) {
                    activeReport.chartObject.width(absoluteWidthRequired).render();
                    draw._rotateLabels("#" + id);
                }
            } catch (e) {

            }
        }

        draw.getTimeLineFilteredData = function (metadataId,accessToken,timelineObj,baseUrl) {
            var respones;
            var data={
                metadataId:metadataId,
                sessionId:accessToken,
                timelineObj:timelineObj
            };
            respones=$.post(baseUrl+"dataRangeFilter",data);
            return respones;
        }

        draw._createTimeLineGroup = function (object, dimension) {
            var dataType;
            var columName;
            var expr = null;
            var flag;
            var validationExpression = null;
            var timelineObject = draw._timeline;
            var timelineObj;
            var objectKey;
            $.each(timelineObject, function (key, value) {
                objectKey = key;
                timelineObj = value;
            });
            dataType = object.key;
            columName = object.value;
            var obj = {};
            groups = {};
            timelineObj.period.forEach(
                function (d, i) {
                    var group = dimension.group().reduce(
                        /* callback for when data is added to the current filter results */
                        function (g, v) {
                            var value = moment(v[objectKey]);
                            if (v['periodType'])
                                if (v['periodType'].includes('period' + i)) {
                                    ++g.count;
                                    var c = g.count;
                                    // if (c == 1) {
                                    //     g.arr = [];
                                    // }
                                    // var val;
                                    val = v[columName];
                                    // if (!obj[val]) {
                                    //     obj[val] = true;
                                    //     if (!g.countDistinct) {
                                    //         g.countDistinct = 1;
                                    //     } else {
                                    //         g.countDistinct++;
                                    //     }
                                    //     if (!g.totalDistinct) {
                                    //         g.totalDistinct = parseFloat(val);
                                    //     } else {
                                    //         g.totalDistinct += parseFloat(val);
                                    //     }
                                    // }
                                    var currentVal = parseFloat(val);
                                    if (!isNaN(currentVal))
                                        g.sumIndex += parseFloat(val);
                                    else
                                        g.sumIndex += 0;

                                    g.avgIndex = g.sumIndex / g.count;
                                    // g.arr[c] = parseInt(v[columName]);
                                    // var sortedArr = g.arr.sort(function (a, b) {
                                    //     return a - b
                                    // });
                                    // if (g.count % 2 == 0) {
                                    //     var n1 = g.count / 2;
                                    //     var n2 = (g.count / 2) + 1;
                                    //     g.median = (sortedArr[n1] + sortedArr[n2]) / 2;
                                    // } else {
                                    //     var n1 = parseInt(g.count / 2);
                                    //     g.median = sortedArr[n1];
                                    // }
                                    // if (g.count == 1) {
                                    //     g.n = 0;
                                    //     g.mean = 0.0;
                                    //     g.M2 = 0.0;
                                    // }
                                    //   g.n += 1;

                                    // g.delta = v[columName] - g.mean;
                                    // g.mean += g.delta / g.n;
                                    // g.M2 += g.delta * (v[columName] - g.mean);

                                    // if (g.n < 2)
                                    //     g.stdDev = parseFloat('nan');
                                    // else
                                    //     g.stdDev = g.M2 / (g.n - 1);
                                }
                            return g;
                        },
                        /*
                         * callback for when data is removed from the current filter
                         * results
                         */
                        function (g, v) {
                            var value = moment(v[objectKey]);
                            if (v['periodType'] == 'period' + i) {
                                --g.count;
                                var val;
                                if (expr != null) {
                                    val = eval(expr);
                                } else {
                                    val = v[columName];
                                }
                                g.sumIndex -= parseFloat(val);
                                g.avgIndex = g.count ? g.sumIndex / g.count : 0;
                            }
                            return g;
                        },
                        /* initialize p */
                        function () {
                            return {
                                count: 0,
                                sumIndex: 0,
                                avgIndex: 0
                            };
                        });
                    groups["period" + i] = group;
                });
            return groups;
        }


        draw._adjustRowChart = function (id, activeReport) {
            try {
                // var elem=$("#chart-"+id).find('svg').find('g');

                // var
                // xAxisChildren=$("#"+id).find('svg').find('g').find('.x').children();
                // var lengthOfLabels=xAxisChildren.length;
                // var totalWidthLabels=0.0;
                // var maxLabelWidth=0;
                // for(var i=0;i<xAxisChildren.length;i++)
                // {
                // var currentWidth=xAxisChildren[i].getBBox().width;
                // if(maxLabelWidth<currentWidth)
                // {
                // maxLabelWidth=currentWidth;
                // }
                // //totalWidthLabels+=xAxisChildren[i].getBBox().width+30;
                // }
                // var
                // absoluteWidthRequired=(maxLabelWidth+5)*xAxisChildren.length;
                //
                // if(absoluteWidthRequired>xAxisWidth &&
                // absoluteWidthRequired>containerWidth)
                // {
                // activeReport.chartObject.width(absoluteWidthRequired).render();
                // }
            } catch (e) {

            }
        }


        draw.chartRegistry = (function () {
            var _chartMap = {};
            var _chartAttributes = {};
            var _colorObject = {};
            return {
                /**
                 * Determine if a given chart instance resides in the registry.
                 *
                 * @method has
                 *
                 * @returns {Boolean}
                 */
                has: function (chart) {
                    if (_chartMap[chart.id]) {
                        return true;
                    }
                    return false;
                },

                get: function (chartId) {
                    if (_chartMap[chartId]) {
                        return _chartMap[chartId];
                    }
                    return false;
                },

                /**
                 * Add given chart instance .
                 *
                 */
                register: function (chart) {

                    _chartMap[chart.id] = chart;
                },

                /**
                 *
                 * @method registerAttributes
                 * @memberof eCharts.chartRegistry
                 *
                 */

                registerColor: function (chart) {
                    if (_chartAttributes[chart.id]) {
                        _chartAttributes[chart.id]['color'] = chart._colorSelection;
                    } else {
                        _chartAttributes[chart.id] = {colors: chart._colorSelection};
                    }
                },

                /**
                 *
                 * @method registerAttributes
                 * @memberof eCharts.chartRegistry
                 *
                 */
                registerAllAttributes: function (chartAttributes) {
                    _chartAttributes = chartAttributes;

                },
                /**
                 *
                 * @method registerAttributes
                 * @memberof eCharts.chartRegistry
                 *
                 */
                listAttributes: function () {
                    return _chartAttributes;
                },
                /**
                 * Remove given chart instance
                 */
                deregister: function (chart) {
                    delete _chartMap[chart._container];
                },
                /**
                 *
                 * @method clear
                 * @memberof eCharts.chartRegistry
                 *
                 */
                clear: function () {
                    // delete _chartMap;
                },
                /**
                 *
                 * @method list
                 * @memberof eCharts.chartRegistry
                 *
                 */
                list: function () {
                    var list = [];
                    $.each(_chartMap, function (key, value) {
                        list.push(value);
                    });
                    return list;
                },
                get: function (chartId) {
                    if (_chartMap[chartId]) {
                        return _chartMap[chartId];
                    }
                    return false;
                },
                otherRegistryCheck:function (id) {
                    eChartServer.chartRegistry.ifExist(id);
                },
                ifExist:function (id) {
                    if(_chartMap[id])
                        delete _chartMap[id];
                }
            };
        })();

        draw._expandAxisConfig = function (){
            draw._dimension = {};
            draw._measure = {};
            // draw._measureCustom=[];
            // draw._dimensionCustom=[];
            draw._aggregate = {};
            draw._dataFormat = {};
            // draw._timeline = {};
            var axis = draw._axisConfig;
            var firstSelectedMeasureKey = "";
            if(axis.checkboxModelDimension && axis.checkboxModelMeasure){
                Object.keys(axis.checkboxModelDimension).forEach(function (d,i){
                    var key = axis.checkboxModelDimension[d].columType;
                    var value = axis.checkboxModelDimension[d].reName;
                    axis.checkboxModelDimension[d]['key'] = key;
                    axis.checkboxModelDimension[d]['value'] = value;
                    draw._dimension[value] = axis.checkboxModelDimension[d];
                });
                Object.keys(axis.checkboxModelMeasure).forEach(function (d){
                    var key = axis.checkboxModelMeasure[d].columType;
                    var value = axis.checkboxModelMeasure[d].reName;
                    axis.checkboxModelMeasure[d]['key'] = key;
                    axis.checkboxModelMeasure[d]['value'] = value;
                    firstSelectedMeasureKey = value;
                    draw._measure[value] = axis.checkboxModelMeasure[d];
                });
            }
            if(!$.isEmptyObject(axis.aggregateModel)) {
                Object.keys(draw._measure).forEach(function (d) {
                    if (axis.aggregateModel[d]){
                        draw._aggregate[d] = axis.aggregateModel[d];
                    }else if(axis.aggregateModel[draw._measure[d].columnName]){
                        draw._aggregate[d] = axis.aggregateModel[draw._measure[d].columnName];
                    }else{
                        draw._aggregate[d] = {
                            "key" : "sumIndex",
                            "value" : "Sum",
                            "type" : "Aggregate"
                        };
                    }
                });
            }else{
                draw._aggregate[firstSelectedMeasureKey] = {
                    "key" : "sumIndex",
                    "value" : "Sum",
                    "type" : "Aggregate"
                };
            }
            if(axis.pivotTable){
                draw.pivotTableConfig = axis.pivotTable;
            }
            if(axis.dataFormat != undefined){
                draw._dataFormat = axis.dataFormat;
                if (axis.dataFormat.groupColor != undefined && axis.dataFormat.groupColor!="" ) {
                    try{
                        draw._groupColorObject = JSON.parse(axis.dataFormat.groupColor);
                        draw._groupColor = draw._groupColorObject.reName;
                    }catch(e){
                        draw._groupColor = axis.dataFormat.groupColor;
                    }
                }
            }
            if(axis.tableSettting != undefined){
                draw._tableSetting = axis.tableSettting;
            }
            if(axis.timeline != undefined){
                draw._timeline = axis.timeline;
            }
        };


        draw._filterDataToTimeLine = function () {
            var timelineObj = draw._timeline;
            var objectKey = {};
            if (!$.isEmptyObject(timelineObj)) {
                $.each(timelineObj, function (key, value) {
                    objectKey = key;
                    timelineObj = value;
                });
            }
            var rawData = draw._data;
            var filterdData = rawData.filter(function (row) {
                value = moment(row[objectKey], "YYYY-MM-DD");
                var flag = false;
                timelineObj.period.forEach(function (d, i) {
                    var periodStart = moment(d.start, "YYYY-MM-DD");
                    var periodEnd = moment(d.end, "YYYY-MM-DD");
                    if (periodStart < value && periodEnd > value) {
                        flag = true;
                    }
                });
                return flag;
            });
            rawData = filterdData;
            draw._crossfilter.remove();
            draw._crossfilter.add(rawData);
            eChartServer.redrawAllP();
        };

        draw.realTime=function (metadataId,id, time, sessionId,sharedviewId,companyId) {
            return $.post(draw.nodeApiUrl + "/realtimeDraw", {
                id:id,
                sessionId: sessionId,
                metadataId: metadataId,
                companyId:companyId,
                time: time,
                sharedviewId:sharedviewId
            })
        }

        draw.render = function (callback) {
            var errorFlag = false;
            if (!draw.chartConfig) {
                errorFlag = true;
            }
            if (!draw.container) {
                errorFlag = true;
            }
            if (!draw.axisConfig) {
                errorFlag = true;
            }
            if (!draw.data) {
                errorFlag = true;
            }

            if (!errorFlag) {
                // dc.renderAll();

                $("#chart-" + draw._container).html("");
                var chart = draw[draw._chartConfig.key]();
                // Changed
                /* setTimeout(function() { */
                draw._containerResized[draw._container] = false;
                if (chart != null)
                    draw._resize(chart, draw._container);
                if (typeof callback == 'function')
                    callback(chart);
                /* }, 100); */
                // return true;
            } else {
                if (typeof callback === 'function')
                    callback(false);
            }
        };

        // ...................................................................................

        // Dimension And Group Creation

        draw._createDimension = function (object) {
            //var dataType;
            var columName;
            //  var categoryObj;
            //dataType = object.key;
            columName = object.columnName;
            /*var timelineObject = draw._timeline;
             var timelineObj;
             var objectKey;
             var flag = false;*/
            if (draw._dataFormat && (object.key == 'date' || object.key == 'datetime')) {
                return draw._crossfilter.dimension(function (d) {
                    var format = draw._dataFormat.xAxis;
                    if (!format) {
                        return d[columName];
                    } else {
                        //, "YYYY-MM-DD"
                        return moment(d[columName]).format(format);
                    }
                });
            } else {
                return draw._crossfilter.dimension(function (d) {
                    return  d[columName] || "";
                });
            }
            // }
        };

        draw.updateFilters=function(filterList){
            draw._filterList = filterList;
        };

        draw.getFilterList=function(){
            if(!draw._filterList){
                draw._filterList=[];
            }
            return draw._filterList;
        }

        draw._createFilterDimension = function (filter) {
            try {
                return draw._crossfilter.dimension(function (d) {
                    if (d[filter] === null)
                        return 0;
                    if (d[filter] === 'null')
                        return 0;
                    return d[filter];
                });
            } catch (e) {
                return null;
            }
        }





        draw.proccessTooltipText = function (d, g, v) {
            var val = v[d.reName];
            if (val == null || isNaN(val) || val == 'NaN' || val == undefined) {
                val = 1;
            }
            // if(!(val!=null && !isNaN(val))){
            //    val=0;
            // }
            else {
                val = Math.round(val);
            }
            if (!g["<sum(" + d.reName + ")>"])
                g["<sum(" + d.reName + ")>"] = 0;
            if (!g["<count(" + d.reName + ")>"])
                g["<count(" + d.reName + ")>"] = 0;
            switch (d.aggregate) {
                case "sum":
                    g["<sum(" + d.reName + ")>"] += val;
                    break;
                case "count":
                    g["<count(" + d.reName + ")>"]++;
                    break;
                default :
                    g["<sum(" + d.reName + ")>"] += val;
            }
        }


        draw.proccessTooltipTextDecrement = function (d, g, v) {
            var val = v[d.reName];
            if (!(val != null && !isNaN(val))) {
                val = 0;
            } else {
                val = Math.round(val);
            }
            if (!g["<sum(" + d.reName + ")>"])
                g["<sum(" + d.reName + ")>"] = 0;
            if (!g["<count(" + d.reName + ")>"])
                g["<count(" + d.reName + ")>"] = 0;
            switch (d.aggregate) {
                case "sum":
                    g["<sum(" + d.reName + ")>"] -= val;
                    break;
                case "count":
                    g["<count(" + d.reName + ")>"]--;
                    break;
                default :
                    g["<sum(" + d.reName + ")>"] -= val;
            }
        }


        // draw.parseFormula = function (expression) {
        //     const regex = /group\(((sum|SUM|count|COUNT|avg|AVG)\(\[([a-z|A-Z|0-9|_|' ']*)\]\))\)/g;
        //     const str = expression;
        //     var params = {};
        //     var alteredFormula = expression;
        //     while ((m = regex.exec(str)) !== null) {
        //
        //         if (m.index === regex.lastIndex) {
        //             regex.lastIndex++;
        //         }
        //
        //         params[m[3]] = m[2];
        //         alteredFormula = alteredFormula.replace(m[0], 'g[\'' + m[2] + '\'][\'' + m[3] + '\']');
        //     }
        //     return {formula: alteredFormula, params: params};
        //
        // }

        draw.parseFormula = function (expression) {
            const regex = /(sum|SUM|count|COUNT|avg|AVG)\(\[([a-z|A-Z|0-9|_|' '|'('|')']*)\]\)/g;
            const str = expression;
            var params = {};
            var alteredFormula = expression;
            while ((m = regex.exec(str)) !== null) {
                if (m.index === regex.lastIndex) {
                    regex.lastIndex++;
                }
                params[m[2]] = m[1];
                alteredFormula = alteredFormula.replace(m[0], 'parseFloat(g[\'' + m[1] + '\'][\'' + m[2] + '\'])');
            }
            return {formula: alteredFormula, params: params};
        }
        // function EVAL(exp) {

        //     return result;
        // }

        draw.calculatedGroups = function (selectedKey, dimension) {
            var columName = selectedKey.value;
            var formula = draw.reduceMultilevelFormula(selectedKey.formula);
            var parsedFormulaObj = draw.parseFormula(formula);
            formula = parsedFormulaObj['formula'];
            var params = parsedFormulaObj['params'];
            var dimensionFormula=undefined;
            if(draw._groupColorObject)
                dimensionFormula  =draw.reduceMultilevelFormula(draw._groupColorObject.formula);
            if(dimensionFormula){
                var parsedDimensionFormulaObj=draw.parseFormula(dimensionFormula);
                $.each(parsedDimensionFormulaObj['params'],function(key,val){
                    params[key]=val;
                });
                dimensionFormula=parsedDimensionFormulaObj['formula'];
            }
            var groupColor=draw._groupColor;
            return dimension.group().reduce(function (g, v) {
                try{
                    if (!groupColor) {
                        $.each(params, function (key, val) {
                            var value = v[key];
                            if (!value || isNaN(value)) {
                                value = 0;
                            } else {
                                value = Math.round(value);
                            }
                            if (!g['sum'][key])
                                g['sum'][key] = 0;
                            g['sum'][key] += value;
                            if (!g['count'][key])
                                g['count'][key] = 0;
                            g['count'][key] += 1;
                            if (!g['avg'][key]) {
                                g['avg'][key] = 0;
                            }
                            g['avg'][key] = g['sum'][key] / g['count'][key];
                            if (isNaN(g['avg'][key])) {
                                g['avg'][key] = 0;
                            }
                        });
                        g.sumIndex = eval(formula);
                    }
                    if (groupColor) {
                        if (!g.done) {
                            $.each(params, function (key, val) {
                                g.groupRepeatCheck['count'][key] = {};
                                g.groupRepeatCheck['sum'][key] = {};
                            });
                            g.done = true;
                        }
                        var valTemp = (v[draw._groupColor]);
                        $.each(params, function (key, val) {
                            var value = v[key];
                            if (!value || isNaN(value)) {
                                value = 0;
                            } else {
                                value =Math.round(value);
                            }
                            if (!g.groupRepeatCheck['count'][key][valTemp]){
                                g.groupRepeatCheck['count'][key][valTemp] = 1;
                                g.groupRepeatCheck['sum'][key][valTemp] = value;
                            } else {
                                g.groupRepeatCheck['count'][key][valTemp]++;
                                g.groupRepeatCheck['sum'][key][valTemp] += value;
                            }
                            //   g.groupRepeatCheck['avg'][key][valTemp]=g.groupRepeatCheck['sumIndex'][valTemp]/g.groupRepeatCheck['count'][valTemp];

                            value = g.groupRepeatCheck['sum'][key][valTemp];
                            if (!g['sum'][key])
                                g['sum'][key] = 0;
                            g['sum'][key] = value;
                            if (!g['count'][key])
                                g['count'][key] = 0;
                            g['count'][key] = g.groupRepeatCheck['count'][key][valTemp];
                            if (!g['avg'][key]) {
                                g['avg'][key] = 0;
                            }
                            g['avg'][key] = g['sum'][key] / g['count'][key];
                            if (isNaN(g['avg'][key])) {
                                g['avg'][key] = 0;
                            }
                        });
                        g.sumIndex = Math.round(eval(formula));
                        if(dimensionFormula){
                            valTemp=eval(dimensionFormula);
                            g.groupRepeatCheck['val']={};
                        }
                        g.groupRepeatCheck['val'][valTemp] = g.sumIndex;
                        g.groupColor = g.groupRepeatCheck['val'];
                    }
                }catch(e){
                    return g;
                }
                return g;
            }, function (g, v) {
                if (!groupColor) {
                    if (!g.done) {
                        $.each(params, function (key, val) {
                            g.groupRepeatCheck['count'][key] = {};
                            g.groupRepeatCheck['sum'][key] = {};
                        });
                        g.done = true;
                    }
                    $.each(params, function (key, val) {
                        var value = v[key];
                        if (!value || isNaN(value)) {
                            value = 0;
                        } else {
                            value = Math.round(value);
                        }
                        if (!g['sum'][key])
                            g['sum'][key] = 0;
                        if (v[key] != null)
                            g['sum'][key] -= value;
                        if (!g['count'][key])
                            g['count'][key] = 0;
                        g['count'][key] -= 1;
                        if (!g['avg'][key]) {
                            g['avg'][key] = 0;
                        }
                        g['avg'][key] = g['sum'][key] / g['count'][key];
                        if (isNaN(g['avg'][key])) {
                            g['avg'][key] = 0;
                        }
                    });
                    g.sumIndex = eval(formula);
                }
                if (groupColor) {
                    var valTemp = (v[draw._groupColor]);
                    $.each(params, function (key, val) {
                        var value = v[key];
                        if (!value || isNaN(value)) {
                            value = 0;
                        } else {
                            value = Math.round(value);
                        }
                        g.groupRepeatCheck['count'][key][valTemp]--;
                        g.groupRepeatCheck['sum'][key][valTemp] -= value;
                        value = g.groupRepeatCheck['sum'][key][valTemp];
                        g['sum'][key] = value;
                        g['count'][key] = g.groupRepeatCheck['count'][key][valTemp];
                        g['avg'][key] = g['sum'][key] / g['count'][key];
                        if (isNaN(g['avg'][key])) {
                            g['avg'][key] = 0;
                        }
                    });
                    g.sumIndex = Math.round(eval(formula));
                    if(dimensionFormula){
                        valTemp=eval(dimensionFormula);
                        g.groupRepeatCheck['val']={};
                    }
                    g.groupRepeatCheck['val'][valTemp] = g.sumIndex;
                    g.groupColor = g.groupRepeatCheck['val'];
                }
                return g;
            }, function (g, v) {
                // if(!g['groupRepeatCheck']){
                //    g['groupRepeatCheck']={};
                // }
                return {
                    sumIndex: 0,
                    tempSum:0,
                    sum: {},
                    count: {},
                    avg: {},
                    groupColor: {},
                    groupRepeatCheck: {count: {}, sum: {}, avg: {}, val: {}}
                };
            });
        }






        draw.normalGroups = function (selectedKey, dimension) {
            var columName = selectedKey.columnName;
            var columType=selectedKey.columType;
            return dimension.group().reduce(
                /* callback for when data is added to the current filter results */
                function (g, v) {
                    ++g.count;
                    var val = v[columName];
                    if (!val || isNaN(val)) {
                        val = 0;
                    } else {
                        if(columType=="int"){
                            val=parseInt(val);
                        }else{
                            val=Math.round(val);
                        }
                    }

                    // if (!obj[val]) {
                    //     obj[val] = true;
                    //     if (!g.countDistinct) {
                    //         g.countDistinct = 1;
                    //     } else {
                    //         g.countDistinct++;
                    //     }
                    //     if (!g.totalDistinct) {
                    //         g.totalDistinct = parseFloat(val);
                    //     } else {
                    //         g.totalDistinct += parseFloat(val);
                    //     }
                    // }
                    //
                    g.sumIndex += val;
                    g.avgIndex = g.sumIndex / g.count;
                    g.toolTipObj = {};
                    if (draw._axisConfig.tooltipSelector) {
                        draw._axisConfig.tooltipSelector.forEach(function (d) {
                            if (d.dataKey == "Measure") {
                                draw.proccessTooltipText(d, g, v);
                            } else {
                                g["<" + d.reName + ">"] = v[d.reName];
                            }
                        });
                    }
                    if (draw._groupColor) {
                        var valTemp = (v[draw._groupColor]);
                        if (!g.groupRepeatCheck[valTemp])
                            g.groupRepeatCheck[valTemp] = val;
                        else
                            g.groupRepeatCheck[valTemp] += val;
                        g.groupColor = g.groupRepeatCheck;
                    }
                    // g.arr[c] = parseInt(v[columName]);
                    // var sortedArr = g.arr.sort(function(a, b) {
                    //     return a - b;
                    // });
                    // if (g.count % 2 == 0) {
                    //     var n1 = g.count / 2;
                    //     var n2 = (g.count / 2) + 1;
                    //     g.median = (sortedArr[n1] + sortedArr[n2]) / 2;
                    // } else {
                    //     var n1 = parseInt(g.count / 2);
                    //     g.median = sortedArr[n1];
                    // }
                    // if (g.count == 1) {
                    //     g.n = 0;
                    //     g.mean = 0.0;
                    //     g.M2 = 0.0;
                    // }
                    // g.n += 1;
                    // g.delta = v[columName] - g.mean;
                    // g.mean += g.delta / g.n;
                    // g.M2 += g.delta * (v[columName] - g.mean);
                    // if (g.n < 2)
                    //     g.stdDev = parseFloat('nan');
                    // else
                    //     g.stdDev = g.M2 / (g.n - 1);
                    return g;
                }
                ,
                /*
                 * callback for when data is removed from the current filter
                 * results
                 */
                function (g, v) {
                    --g.count;
                    var val;
                    val = v[columName];
                    if(columType=="int"){
                        val=parseInt(val);
                    }else{
                        val=Math.round(val);
                    }
                    if (draw._axisConfig.tooltipSelector) {
                        draw._axisConfig.tooltipSelector.forEach(function (d) {
                            if (d.dataKey == "Measure") {
                                draw.proccessTooltipTextDecrement(d, g, v);
                            } else {
                                // g["<"+d.reName+">"]=v[d.reName];
                            }
                        });
                    }
                    if (draw._groupColor) {
                        var valTemp = (v[draw._groupColor]);
                        if (!g.groupRepeatCheck[valTemp]){
                            g.groupRepeatCheck[valTemp] = val;
                        } else {
                            g.groupRepeatCheck[valTemp] = g.groupRepeatCheck[valTemp] - val;
                        }
                        g.groupColor = g.groupRepeatCheck;
                    }
                    g.sumIndex -= val;
                    g.avgIndex = g.count ? g.sumIndex / g.count : 0;
                    return g;
                },
                /* initialize p */
                function () {
                    return {
                        count: 0,
                        sumIndex: 0,
                        avgIndex: 0,
                        groupColor: {},
                        groupRepeatCheck: {}
                    };
                });
        }

        draw._isAggregateField = function (formula) {
            var flag = !$.isEmptyObject((draw.parseFormula(formula)).params);
            return flag;
        }

        draw.columnsByColumnName=function(){

        }

        draw.reduceMultilevelFormula=function(formula){
            const regex = /(?:sum|SUM|count|COUNT|avg|AVG)\(\[([\w|' ']*)\]\)/g;
            var reducedFormula=formula;
            var runFormula=formula;
            var flag=false;
            var  m;
            while((m = regex.exec(formula)) !== null) {
                flag=true;
                try{
                    var fieldObject=draw.columnsByColumnName[m[1]];
                    if(fieldObject && fieldObject.type=="custom" && draw._isAggregateField(fieldObject.formula)){
                        reducedFormula=reducedFormula.replace(m[0],"("+fieldObject.formula+")");
                    }
                }catch(e){
                    return reducedFormula;
                }
            }
            if(flag)
                if(draw.hasMultiLevelCalculation(reducedFormula)){
                    reducedFormula=draw.reduceMultilevelFormula(reducedFormula);
                    return reducedFormula;
                }else{
                    const checkDivide =/\(?(?:sum|SUM|count|COUNT|avg|AVG)?\(?\[[\w|' ']*\]\)?\)\/\(?(?:sum|SUM|count|COUNT|avg|AVG)?\(?\[[\w|' ']*\]\)?\)/g;
                    var o;
                    var i=1;
                    var replacements={};
                    while((o = checkDivide.exec(reducedFormula)) !== null) {
                        replacements[o[0]]="("+o[0]+")?0:"+o[0]+")";
                        i++;
                    }
                    $.each(replacements,function(key,val){
                        reducedFormula=reducedFormula.replace(key,val);
                    });
                    return reducedFormula;
                }
            return reducedFormula;
        }

        draw.hasMultiLevelCalculation = function (formula) {
            var flag = false;
            const regex = /\[([\w|' ']*)\]/g;
            var m;
            while ((m = regex.exec(formula)) !== null) {
                try {
                    var fieldObject = draw.columnsByColumnName[m[1]];
                    if (fieldObject.type == "custom" && draw._isAggregateField(fieldObject.formula)) {
                        flag = true;
                    }
                } catch (e) {
                    return false;
                }
            }
            return flag;
        }

        draw._isCalculated = function (formula) {
            var flag = draw.hasMultiLevelCalculation(formula) || draw._isAggregateField(formula);
            return flag;
        }

        draw._createGroup = function (d, dimension) {
            if (d.type == "custom" && draw._isCalculated(d.formula)) {
                return  draw.calculatedGroups(d, dimension);
            } else {
                return  draw.normalGroups(d, dimension);
            }
        };


        // ...............................................................................
        // create Composite Measure
        draw._createCompositeMeasure = function (measure, dimension) {
            var yParameter = draw._dimension[1].value;
            var operator = draw._aggregate[0].key;
            if (measure == null && draw._measureCustom != null) {
                var tempObject = draw._measureCustom[1];
                dataType = tempObject.key;
                columName = tempObject.value;
                expr = tempObject.formula;
            } else {
                measure = measure.value;
            }
            return dimension
                .group()
                .reduce(
                    /*
                     * callback for when data is added to the current filter
                     * results
                     */
                    function (g, v) {
                        ++g.count;
                        var c = g.count;
                        if (c == 1) {
                            g.arr = [];
                        }
                        g.sumIndex += parseFloat(v[measure]);
                        g.avgIndex = g.sumIndex / g.count;
                        g.arr[c] = parseInt(v[measure]);
                        var sortedArr = g.arr.sort(function (a, b) {
                            return a - b;
                        });
                        if (g.count % 2 == 0) {
                            var n1 = g.count / 2;
                            var n2 = (g.count / 2) + 1;
                            g.median = (sortedArr[n1] + sortedArr[n2]) / 2;
                        } else {
                            var n1 = parseInt(g.count / 2);
                            g.median = sortedArr[n1];
                        }
                        if (g.count == 1) {
                            g.n = 0;
                            g.mean = 0.0;
                            g.M2 = 0.0;
                        }
                        g.n += 1;
                        g.delta = v[measure] - g.mean;
                        g.mean += g.delta / g.n;
                        g.M2 += g.delta * (v[measure] - g.mean);
                        if (g.n < 2)
                            g.stdDev = parseFloat('nan');
                        else
                            g.stdDev = g.M2 / (g.n - 1);
                        if (g[v[yParameter] + "count"] != undefined) {
                            g[v[yParameter] + "count"] = g[v[yParameter]
                            + "count"] + 1;
                        } else {
                            g[v[yParameter] + "count"] = 1;
                        }
                        if (g[v[yParameter] + "sum"] != undefined) {
                            g[v[yParameter] + "sum"] = parseFloat(g[v[yParameter] + "sum"]) + parseFloat(v[measure]);
                        } else {
                            g[v[yParameter] + "sum"] = parseFloat(v[measure]);
                        }
                        // g[v[yParameter]+"sum"]=+v[measure];

                        switch (operator) {
                            case "count": g[v[yParameter]] = g[v[yParameter] + "count"];
                                break;
                            case "sumIndex": g[v[yParameter]] = g[v[yParameter] + "sum"];
                                break;
                            case "avgIndex" : g[v[yParameter]] = g[v[yParameter] + "sum"] / g[v[yParameter] + "count"];
                                break;
                        }
                        return g;
                    },
                    /*
                     * callback for when data is removed from the current filter
                     * results
                     */
                    function (g, v) {
                        g[v[yParameter] + "count"] -= 1;
                        g[v[yParameter] + "sum"] -= parseFloat(v[measure]);
                        g[v[yParameter]] = g.count ? g[v[yParameter] + "sum"] / g[v[yParameter] + "count"] : 0;
                        switch (operator) {
                            case "count": g[v[yParameter]] = g[v[yParameter] + "count"];
                                break;
                            case "sumIndex": g[v[yParameter]] = g[v[yParameter] + "sum"];
                                break;
                            case "avgIndex": g[v[yParameter]] = g[v[yParameter] + "sum"] / g[v[yParameter] + "count"];
                                break;
                        }
                        /*--g.count;
                         g.sumIndex -= parseFloat(v[measure]);
                         g.avgIndex = g.count ? g.sumIndex / g.count : 0;*/
                        return g;
                    },
                    /* initialize p */
                    function () {
                        return {
                            count: 0,
                            sumIndex: 0,
                            avgIndex: 0,
                            stdDev: 0
                        };
                    });
        };


        // end composite Group

        // numberDisplay Group
        draw.normalNumberGroups = function (d) {
            var columName = d.columnName;
            return draw._crossfilter.groupAll().reduce(function (g, v) {
                    ++g.count;
                    var val = v[columName];
                    if (!(val != null && !isNaN(val))) {
                        val = 0;
                    } else {
                        val = Math.round(val);
                    }
                    g.sumIndex += val;
                    g.avgIndex = g.sumIndex / g.count;
                    // g.arr[c] = parseInt(v[columName]);
                    // var sortedArr = g.arr.sort(function (a, b) {
                    //     return a - b
                    // });
                    // if (g.count % 2 == 0) {
                    //     var n1 = g.count / 2;
                    //     var n2 = (g.count / 2) + 1;
                    //     g.median = (sortedArr[n1] + sortedArr[n2]) / 2;
                    // } else {
                    //     var n1 = parseInt(g.count / 2);
                    //     g.median = sortedArr[n1];
                    // }
                    //var val;
                    // if (!obj[val]) {
                    //     obj[val] = true;
                    //     if (!g.countDistinct) {
                    //         g.countDistinct = 1;
                    //     } else {
                    //         g.countDistinct++;
                    //     }
                    //     if (!g.totalDistinct) {
                    //         g.totalDistinct = parseFloat(val);
                    //     } else {
                    //         g.totalDistinct += parseFloat(val);
                    //     }
                    // }
                    // if (g.count == 1) {
                    //     g.n = 0;
                    //     g.mean = 0.0;
                    //     g.M2 = 0.0;
                    // }
                    // g.delta = v[columName] - g.mean;
                    // g.mean += g.delta / g.n;
                    // g.M2 += g.delta * (v[columName] - g.mean);
                    // if (g.n < 2)
                    //     g.stdDev = parseFloat('nan');
                    // else
                    //     g.stdDev = g.M2 / (g.n - 1);
                    // g[v[yParameter]+"sum"]=+v[measure];
                    return g;
                },
                /*
                 * callback for when data is removed from the current filter
                 * results
                 */
                function (g, v) {
                    var val = v[columName];
                    if (!(val != null && !isNaN(val))) {
                        val = 0;
                    } else {
                        val = Math.round(val);
                    }
                    --g.count;
                    g.sumIndex -= val;
                    g.avgIndex = g.count ? g.sumIndex / g.count : 0;
                    return g;
                },
                /* initialize p */
                function () {
                    return {
                        count: 0,
                        sumIndex: 0,
                        avgIndex: 0,
                    };
                });
        }

        draw.calculatedNumberGroups = function (d) {
            var columName = d.value;
            var formula = d.formula;
            var parsedFormulaObj = draw.parseFormula(formula);
            formula = parsedFormulaObj['formula'];
            var params = parsedFormulaObj['params'];
            return draw._crossfilter.groupAll().reduce(function (g, v) {
                $.each(params, function (key, val) {
                    var value = v[key];
                    if (!(value != null && !isNaN(value))) {
                        value = 0;
                    } else {
                        value = Math.round(value);
                    }
                    if (!g['sum'][key])
                        g['sum'][key] = 0;
                    g['sum'][key] += value;
                    if (!g['count'][key])
                        g['count'][key] = 0;
                    g['count'][key] += 1;

                    if (!g['avg'][key]) {
                        g['avg'][key] = 0;
                    }
                    g['avg'][key] = g['sum'][key] / g['count'][key];
                });
                g.sumIndex = eval(formula);
                return g;
            }, function (g, v) {
                $.each(params, function (key, val) {
                    var value = v[key];
                    if (!(value != null && !isNaN(value))) {
                        value = 0;
                    } else {
                        value = Math.round(value);
                    }
                    if (!g['sum'][key])
                        g['sum'][key] = 0;
                    g['sum'][key] -= value;
                    if (!g['count'][key])
                        g['count'][key] = 0;
                    g['count'][key] -= 1;
                    if (!g['avg'][key]) {
                        g['avg'][key] = 0;
                    }
                    g['avg'][key] = g['sum'][key] / g['count'][key];
                });
                g.sumIndex = eval(formula);
                return g;
            }, function (g, v) {
                return {
                    count: {},
                    sum: {},
                    avg: {}
                };
            });
        }
        // numberDisplay Group
        draw.createNumberDisplayGroup = function (d) {
            if (d.type == "custom" && !$.isEmptyObject((draw.parseFormula(d.formula)).params)) {
                return  draw.calculatedNumberGroups(d);
            } else {
                return  draw.normalNumberGroups(d);
            }
        };
        // Group and Dimension End..............................................

        var newDim = [];
        draw.applyAllFilter = function (columnObjFilter,metadataId) {
            $.post(draw.nodeApiUrl + "/applyExternalAllFilters", {
                columnObjFilter: columnObjFilter,
                sessionId: draw.getAccessToken(),
                metadataId: metadataId
            }).done(function(data){
                eChartServer.server.externalFilterDataUpdate(data, function () {
                    eChartServer.redrawAll();
                });
                $(".loadingBar").hide();
            });
        }

        function cascade(index,filterList,cascadeFlag,metadataId,filter,totalLength,filterIndex,callback){
            if(cascadeFlag){
                // if(filter.cascade){
                var data = {columnObject:filterList[index],metadataId:metadataId,totalLength:totalLength,sessionId:draw.getAccessToken(),filterIndex:filterIndex};
                $.post(draw.nodeApiUrl+"/getUniqueDataFieldsOfColumnAfterCascade",data).done(function(data){
                    if(filterList[index] && filterList[index].key==filter.key){
                        cascadeFlag=true;
                    }
                    if(filterList.length>index){
                        callback(data,filterList[index],true);
                        cascade(++index,filterList,cascadeFlag,metadataId,filter,totalLength,filterIndex,callback);
                    }else{
                        if(!cascadeFlag){
                            callback(data,filterList[index],false);
                        }
                    }
                });
                //  }
            }else{
                if(filterList[index].key==filter.key){
                    cascadeFlag=true;
                }
                callback("","",false);
                cascade(++index,filterList,cascadeFlag,metadataId,filter,totalLength,filterIndex,callback);
            }
        }

        draw.applyFilter = function (item, filter, metadataId, totalLength, filterIndex, callback) {
            var f = item;
            $.post(draw.nodeApiUrl+"/applyCascadeFilters", {item:item,sessionId:draw.getAccessToken(),dimensionObject:filter,metadataId:metadataId,totalLength:totalLength,filterIndex:filterIndex}).done(function(data) {
                var cascadeFlag = false;
                var data = null;
                var filterObj = null;
                cascade(0,draw.getFilterList(),cascadeFlag,metadataId,filter,totalLength,filterIndex,callback);
            });

            // if (newDim[filter.key] == undefined)
            //     newDim[filter.key] = draw._createFilterDimension(filter.key);

            // if (item == '') {
            //     newDim[filter.key].filterAll();
            // } else {
            //     if (filter.value == "int" || filter.value == "float" || filter.value == "decimal" || filter.value == "double") {
            //         var f = item;
            //         newDim[filter.key].filter(function (d) {
            //             if (!isNaN(d)) {
            //             } else {
            //                 return false;
            //             }
            //             if ((parseFloat(f[0]) <= parseFloat(d)) && (parseFloat(f[1]) >= parseFloat(d))) {
            //                 return true;
            //             } else {
            //                 return false;
            //             }
            //         });
            //     } else if (filter.value == "DATETIME" || filter.value == "date") {
            //         var startDate = moment(item['start'], "YYYY-MM-DD");
            //         var endDate = moment(item['end'], "YYYY-MM-DD");
            //         newDim[filter.key].filter(function (d) {
            //             var dateInData = moment(d);
            //             if (startDate < dateInData && endDate > dateInData) {
            //                 return true;
            //             } else {
            //                 return false;
            //             }
            //         });
            //     } else {
            //         newDim[filter.key].filter(function (d) {
            //             return f.indexOf(d) > -1;
            //         });
            //     }
            // }



            //  eChart.redrawAll();
        }

        draw._containerResized = {};

        draw.resizeContainer = function (id, width, chart) {
            try {
                if (draw._containerResized[id] === undefined)
                    draw._containerResized[id] = false;
                if (!draw._containerResized[id]) {
                    chart.width(width);
                    draw._containerResized[id] = true;
                }
            } catch (e) {

            }
        };

        // Attribute Change
        // Functions...........................................................

        // Graph
        draw._resize = function (chart, id) {
            // chart.key=="_numberWidget" ||

            /*
             * if (!(chart == true || !chart)) { var _bbox =
             * chart.root().node().parentNode .getBoundingClientRect(); var
             * newWidth = (parseFloat(_bbox.width) * parseFloat(100.0)) / 100.0;
             * var newHeight = (parseFloat(_bbox.height) * parseFloat(95.0)) /
             * 100.0; if (chart.chartType != undefined) { if (chart.chartType() ==
             * "rowChart") { draw.resizeContainer(id, newWidth, chart);
             *
             * var elem = $("#chart-" + id).find('svg').find('g')
             * .find('.y').find('g').children().length;
             *
             * chart.height(40 * elem); chart.render();
             * draw.applyTooltip(chart); } else if (chart.chartType() ==
             * "pieChart") { var newWidth = (parseFloat(_bbox.width) *
             * parseFloat(85)) / 100.0; if (newWidth < newHeight)
             * chart.innerRadius(newWidth * (20.0 / 100.0)); else
             * chart.innerRadius(newHeight * (20.0 / 100.0));
             *
             * chart.legend(dc.legend().x(10).y(2).itemHeight(10).gap(
             * 5).legendWidth(20));
             * chart.width(newWidth).height(newHeight).render();
             * draw.applyTooltip(chart); } else { draw.resizeContainer(id,
             * newWidth, chart); chart.height(newHeight);
             * draw._setOption(chart); chart.render(); draw.applyTooltip(chart);
             * draw._rotateLabels("#chart-" + id); } } else {
             * draw.resizeContainer(id, newWidth, chart); chart.height(newHeight -
             * 50); draw._setOption(chart); chart.render();
             * draw.applyTooltip(chart); draw.adjustYaxisLabels(id);
             * draw._rotateLabels("#chart-" + id); } }
             */
        }
        draw._setOption = function (chart) {
            var xAxisLabel = draw._dimension[0].value;
            var measureLabel = draw._measure[0].value;
            chart.options({
                renderTitleLabel: false,
                title: function (d) {
                    return xAxisLabel.capitalizeFirstLetter() + " is <font color='blue'>" + d.key + "</font> </br> " + draw._aggregate[0].value + " of " + measureLabel.capitalizeFirstLetter() + " is  <font color='blue'>" + d.value[draw._aggregate[0].key] + "</font>";
                },
                label: function (d) {
                    return d.key;
                },
                cap: 15,
                // if elastic is set than the sub charts will have different
                // extent ranges, which could mean the data is interpreted
                // incorrectly
                elasticX: false
            });
        }

        function setOption(chart, xD, yD, aggr) {
            chart.options({
                renderTitleLabel: false,
                title: function (d) {
                    if (aggr == "count") {
                        return xD + ':' + d.key + "</br>" + "Count(" + yD + ")" + ':' + d.value[aggr];
                    } else {
                        return xD + ':' + d.key + "</br>" + yD + ':' + d.value[aggr];
                    }
                },
                label: function (d) {
                    return d.key;
                },
                cap: 15,
                elasticX: false
            });
        }

        // Change Width

        draw.setWidth = function (chart, width) {
            chart.width(parseFloat(width));
            chart["resize"] = false;
        };

        // Change Height
        draw.setHeight = function (chart, height) {
            chart.height(height);
            chart["resize"] = false;
        };

        // Render Attributes
        draw.renderAttributes = function (chart, styleSettings) {
            if (styleSettings.resize)
                chart["resize"] = styleSettings.resize;
            else
                chart["resize"] = "ON";
            if (styleSettings.type == "Number Widget") {
                // Number Formate
                // $(".number-display").css("color",
                // styleSettings['labelColor']);
                if (styleSettings['NumberFormate'] == "Formate") {
                    chart.formatNumber(d3.format(".3s"));
                } else if (styleSettings['NumberFormate'] == "Normal") {
                    chart.formatNumber(function(d) {
                        return d;
                    });
                } else if (styleSettings['NumberFormate'] == "$") {
                    chart.formatNumber(function (d) {
                        return styleSettings['NumberFormate'] + " " + d;
                    });
                } else {
                    chart.formatNumber(function (d) {
                        return d + " " + styleSettings['NumberFormate']
                    });
                }
                // percentage
                // chart.formatNumber(d3.format(",%"));
            } else {
                if (styleSettings.resize == "OFF") {
                    chart.width(styleSettings['cWidth']);
                    chart.height(styleSettings['cHeight']);
                }
                /*
                 * chart.xAxisLabel(styleSettings['xAxisLabelName']);
                 * chart.yAxisLabel(styleSettings['yAxisLabelName']);
                 * chart.ordinalColors([ styleSettings['chartColor'] ]);
                 */
                // CHart Margin from the container
                /*
                 * margins({top: styleSettings['xAxisMargin'], right: 0, bottom:
                 * 30, left: styleSettings['yAxisMargin']}
                 */
                // Rotate x axis Label
                chart.renderlet(function (chart) {
                    chart.selectAll('g.x text').attr(
                        'transform',
                        'translate(' + styleSettings['translateX'] + ','
                        + styleSettings['translateY'] + ') rotate('
                        + styleSettings['rotateLabel'] + ')');
                    /*
                     * chart.selectAll('.x-axis-label')
                     * .attr('transform','translate('+styleSettings['xAxisMargin']+','+styleSettings['yAxisMargin']+')');
                     */
                });
            }
            // chart.formatNumber(d3.format(".3s"))
            chart.render();

        }

        draw.checkFloat = function(n){
            return Number(n) === n && n % 1 !== 0;
        }

        draw.checkInt = function(n){
            return Number(n) === n && n % 1 === 0;
        }



        draw.numberWidgetRecordGroup=(function(){
            var _numberWidgetGroupRecords={};
            return {
                value:function(containerId){
                    return _numberWidgetGroupRecords[containerId];
                },
                set:function (containerId,data) {
                    _numberWidgetGroupRecords[containerId]=data;
                },
                list:function () {
                    return Object.keys(_numberWidgetGroupRecords);
                }
            }
        })();




        // Show Number Widget
        draw._numberWidget = function () {
            var _chart = {};
            _chart._axisConfig = draw._axisConfig;
            _chart._container = draw._container;
            _chart.id = "chart-" + draw._container;
            _chart.metadataId = draw._axisConfig['queryObj'];
            _chart.type = '_numberWidget';

            var object;
            $.each(draw._measure, function (key, value) {
                object = value;
            });
            var operator = draw._aggregate;
            var measures=draw._measure;
            var dimension=draw._dimension;
            $("#chart-" + draw._container).html('');
            $.each(measures, function (key, value) {
                operator = draw._aggregate[value.reName];
            });
            var metadataId=draw._axisConfig['queryObj'];
            var containerId="chart-" + draw._container;
            var config={_measure:measures}
            var numberStyle="";
            if(draw._axisConfig.numberStyle){
                var numberStyle=draw._axisConfig.numberStyle;
            }
            /*
             * For dimension
             */
            measures['dimension']=dimension;
            $.post(draw.nodeApiUrl+"/drawNumberWidget", {"axisConfig":measures,"sessionId":draw.getAccessToken(),"reportId":containerId,metadataId:metadataId}).done(function(data) {
                draw.numberWidgetRecordGroup.set(containerId,data);
                eChartServer.chartRegistry.deregister({id:containerId});
                draw.chartRegistry.deregister({id:containerId});
                var yGrp={
                    value:function(){
                        return draw.numberWidgetRecordGroup.value(containerId);
                    }
                }
                dc.numberDisplay("#"+containerId).group(yGrp).valueAccessor(function (d) {
                    return (d[operator.key]);
                }).formatNumber(function (d) {
                    var value=d;
                    if(numberStyle){
                        if(numberStyle.decimal!=undefined){
                            value = value.toFixed(numberStyle.decimal);
                        }else if(draw.checkFloat(value)){
                            value = value.toFixed(2);

                        }
                        if(numberStyle.units=="yes"){
                            value = eChartServer.formatNumber(value);
                        }
                        if(numberStyle.separator){
                            value = value.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
                        }
                        if(numberStyle.prefix){
                            value = numberStyle.prefix+" "+value;
                        }
                        if(numberStyle.suffix){
                            value = value+" "+numberStyle.suffix;
                        }
                    }else{
                        if(draw.checkFloat(value)){
                            value = value.toFixed(2);
                        }
                    }
                    return value;
                }).render();
                if (numberStyle) {
                    //font-size:' + numberStyle.fontSize +
                    $("#chart-" + draw._container + " > .number-display").attr('style', 'color:' + numberStyle.fontColor);
                }
                draw.reportLoadedCountCheck(containerId);
            });

            draw.chartRegistry.otherRegistryCheck(_chart.id);
            draw.chartRegistry.register(_chart);
            draw.globalRegistry.register(_chart);
            draw.globalRegistry.syncLocalRegistries(_chart.id,"sketch");
            return _chart;
        };




        // DataTable Show
        var dimension = [];
        draw._dataTable = function () {
            var _chart = {};
            _chart._container = draw._container;
            _chart.id = "chart-"+draw._container;
            _chart._dimension = draw._dimension;
            _chart._measure = draw._measure;
            _chart._aggregate = draw._aggregate;
            _chart._tableSetting = draw._tableSetting;
            _chart._axisConfig = draw._axisConfig;
            _chart.grandTotal = _chart._axisConfig.grandTotal;
            _chart.decimalObj = _chart._axisConfig.decimal;
            _chart.decimalPlaces = {};

            _chart.dtExcludeKey = [];
            if(_chart._axisConfig.excludeKey != undefined){
                _chart.dtExcludeKey = _chart._axisConfig.excludeKey;
            }

            _chart.topN = draw._axisConfig.topN;
            _chart.topnMeasure = draw._axisConfig.topnMeasure;

            if(draw._dataFormat && draw._dataFormat.runningTotal){
                _chart._runningTotal=draw._dataFormat.runningTotal;
            }
            _chart.metadataId=draw._axisConfig['queryObj'];
            if (draw._tableSetting && draw._tableSetting.tableColumnOrder){
                _chart._tableSetting.tableColumnOrder = draw._tableSetting.tableColumnOrder;
            }

            if (draw._tableSetting && draw._tableSetting.dataTypeOrder){
                _chart._tableSetting.dataTypeOrder = draw._tableSetting.dataTypeOrder;
            }

            var chartObject = Object.assign({},_chart);
            _chart.render = function () {
                try {
                    var keyOfObj = "";
                    var data = [];
                    var tableHeaders = "";
                    var k = 0;
                    var ColumnArrayForDataTable = [];
                    var keys = [];
                    var keysOrder=[];

                    if (_chart._tableSetting && _chart._tableSetting.tableColumnOrder) {
                        keys = [];
                        var simpleKey = [];
                        $.each(_chart._tableSetting.tableColumnOrder, function (key, value) {
                            simpleKey.push(JSON.parse(value).reName);
                            keys.push(JSON.parse(value));
                            keysOrder.push(JSON.parse(value).columnName);
                        });

                        var commonArray = [];
                        var commonArrayNames = [];

                        $.each(_chart._dimension, function (key, value) {
                            commonArrayNames.push(key);
                            commonArray.push(value);
                        });
                        $.each(_chart._measure, function (key, value) {
                            commonArrayNames.push(key);
                            commonArray.push(value);
                        });

                        if (simpleKey.length < commonArrayNames.length) {
                            commonArrayNames.forEach(function (d, i) {
                                if (simpleKey.indexOf(d) === -1) {
                                    keys.push(commonArray[i]);
                                    keysOrder.push(commonArrayNames[i]);
                                }
                            });
                        } else if (simpleKey.length === commonArrayNames.length) {
                            for (var j = commonArrayNames.length - 1; j >= 0; j--) {
                                if (simpleKey.indexOf(commonArrayNames[j]) === -1) {
                                    keys.push(commonArray[j]);
                                    simpleKey.push(commonArray[j].reName);
                                    keysOrder.push(commonArrayNames[j]);
                                }
                            }
                            for (var j = simpleKey.length - 1; j >= 0; j--) {
                                if (commonArrayNames.indexOf(simpleKey[j]) === -1) {
                                    keys.splice(j, 1);
                                    keysOrder.splice(j, 1);
                                }
                            }
                        } else {
                            var temp = simpleKey;
                            for (var j = simpleKey.length - 1; j >= 0; j--) {
                                if (commonArrayNames.indexOf(simpleKey[j]) === -1) {
                                    keys.splice(j, 1);
                                    keysOrder.splice(j, 1);
                                    //simpleKey.splice(j,1);
                                }
                            }
                        }
                    } else {
                        try {
                            $.each(_chart._dimension, function (key, value) {
                                keys.push(value);
                                keysOrder.push(key);
                            });
                        } catch (e) {

                        }
                        try {
                            $.each(_chart._measure, function (key, value) {
                                keys.push(value);
                                keysOrder.push(key);
                            });
                        } catch (e) {

                        }
                    }
                    chartObject.keysOrder = keysOrder;

                    if(_chart.decimalObj){
                        $.each(_chart._axisConfig.checkboxModelMeasure, function (k,v) {
                            var index = keysOrder.indexOf(v.reName);
                            _chart.decimalPlaces[index] = _chart.decimalObj[v.columnName];
                        });
                    }

                    draw._tableSetting = null;
                    var columnNames = [];
                    var _activeKey = [];

                    _chart.keys = keys;
                    keys.forEach(function(d) {
                        columnNames.push(d['columnName']);
                        if (d.dataKey === "Dimension") {
                            _activeKey.push(d['columnName']);
                        }
                        tableHeaders += "<th>" + '<a href="javascript:void(0)" style="float:right" class="toggle-vis" data-column="' + k + '"></a>' + d['reName'] + "</th>";
                        k++;
                    });

                    var groupParams=[];
                    var allCalcutionFinished=0;

                    if (_activeKey.length > 0){
                        draw.chartLoading_Hide();
                        keys.forEach(function (d,index) {
                            if (d.type == "custom" && !$.isEmptyObject(draw.parseFormula(d.formula)) && !d.createdby) {
                                var columnName = d.columnName;
                                var formula;
                                if(!d.formula.includes("DimensionSum")){
                                    formula = draw.reduceMultilevelFormula(d.formula);
                                }else{
                                    formula = d.formula;
                                }
                                groupParams.push({formula:formula,columnName:columnName,columnObject:d});
                                calculateServer.processExpression(formula, "", columnName,d,draw.getAccessToken(),_chart.metadataId,"",_activeKey).done(function(){
                                    allCalcutionFinished++;
                                    if(keys.length==allCalcutionFinished){
                                        draw_DataTable();
                                    }
                                });
                            }else{
                                allCalcutionFinished++;
                                if(keys.length==allCalcutionFinished){
                                    draw_DataTable();
                                }
                            }
                        });
                    }else{
                        draw_DataTable();
                    }
                    var initialKey = columnNames[0];
                    function objIterate(obj,index,newObj,rs){
                        $.each(obj,function(e,v){
                            if(!$.isEmptyObject(v)){
                                if(_chart._runningTotal && _chart._runningTotal[keys[index].reName]){
                                    rs[index]+=parseFloat(e);
                                    e=rs[index];
                                    newObj[e]={};
                                }else{
                                    newObj[e]={};
                                }
                                objIterate(v,++index,newObj[e],rs);
                                index--;
                            }else{
                                if(_chart._runningTotal && _chart._runningTotal[keys[index].reName]){
                                    rs[index]+=parseFloat(e);
                                    e=rs[index];
                                    newObj[e]={};
                                }else{
                                    newObj[e]={};
                                }
                                index=0;
                                return 0;
                            }
                        });
                        index=0;
                    }
                    function paginationTable(info,page){
                        var start=parseInt(100*page)-parseInt(99);
                        var end=100*page;
                        if(end>info.totalData){
                            end=info.totalData;
                        }
                        $("#paginationEntry_"+_chart._container).html("Showing "+start+" to "+ end +" of "+info.totalData+" entries");
                        $("#pagination_"+_chart._container).twbsPagination({
                            totalPages: Math.ceil(info.totalData/100),
                            startPage: 1,
                            visiblePages: 5,
                            initiateStartPageClick: true,
                            href: false,
                            hrefVariable: '{{number}}',
                            first: 'First',
                            prev: 'Previous',
                            next: 'Next',
                            last: 'Last',
                            loop: false,
                            onPageClick: function (event, page) {
                                var start=parseInt(100*page)-parseInt(99);
                                var end=100*page;
                                if(end>info.totalData){
                                    end=info.totalData;
                                }
                                $("#paginationEntry_"+_chart._container).html("Showing "+start+" to "+ end +" of "+info.totalData+" entries");
                                $('.page-active').removeClass('page-active');
                                $('#page'+page).addClass('page-active');
                                draw_DataTable(page);
                            },
                            paginationClass: 'pagination',
                            nextClass: 'next',
                            prevClass: 'prev',
                            lastClass: 'last',
                            firstClass: 'first',
                            pageClass: 'page',
                            activeClass: 'active',
                            disabledClass: 'disabled'
                        });
                    }
                    function draw_DataTable(pageNo){
                        var pageNumber=1;
                        if(pageNo){
                            pageNumber=pageNo;
                        }
                        $.post(draw.nodeApiUrl + "/processDataForDatatable", {"config":chartObject,"sessionId":draw.getAccessToken(),groupParams:groupParams,groupKeys:_activeKey,pageNo:pageNumber}).done(function(objj){
                            var obj = objj.data;
                            _chart.dtExcludeKey.forEach(function (d) {
                                delete obj[d];
                            });
                            _chart._data = {};
                            _chart._data.labels = Object.keys(obj);

                            if(pageNo==undefined){
                                paginationTable(objj.info,pageNumber);
                            }

                            draw.chartLoading_Hide();
                            var finalObj = {};
                            var rs = [];
                            keys.forEach(function(d){
                                rs.push(0);
                            });
                            var newObj = {};
                            objIterate(obj,0,newObj,rs);
                            obj = newObj;
                            Object.keys(obj).forEach(function(key) {
                                finalObj[key] = obj[key];
                            });

                            $("#chart-" + _chart._container).html("");
                            $("#chart-" + _chart._container).append('<table id="table-' + _chart.id + '" class="table table-bordered bordered table-striped table-condensed tablesorter" style="width: 100%;"><thead><tr>' + tableHeaders + '</tr></thead></table>');

                            var isObject = function (a) {
                                if ($.isEmptyObject(a))
                                    return false;
                                else
                                    return (!!a) && (a.constructor === Object);
                            };
                            function getAllChildren(group, children) {
                                children = children || [];
                                if (group && isObject(group)) {
                                    $.each(group, function (key, child) {
                                        getAllChildren(child, children)
                                    });
                                } else {
                                    children.push(group);
                                }
                                return children;
                            }
                            var p = "";
                            var r = "";
                            var classText = "";
                            $.each(finalObj, function (key, value) {
                                r = "";
                                p += "<tr>";
                                var rowLength = getAllChildren(value).length;
                                var tableData = key.split('#');
                                if (rowLength > 1) {
                                    classText = "tablesorter-childRow";
                                }
                                p += "<td rowspan='" + rowLength + "' pos='0'>" + tableData[0] + "</td>";
                                p += rowObj(value,1);
                            });

                            function rowObj(value,index) {
                                var i = 0;
                                $.each(value, function (k, v) {
                                    i++;
                                    var rLength = 1;
                                    if (isObject(v)) {
                                        rLength = getAllChildren(v).length;
                                    }
                                    if (i === 1) {
                                        var tableData = k.split('#');
                                        if (rLength > 1) {
                                            classText = "tablesorter-childRow";
                                        }
                                        var str =k;
                                        if (!isNaN(str)){
                                            if (str.includes(".")){
                                                str = parseFloat(str).toFixed(2);
                                            }else{
                                                str = parseInt(str);
                                            }
                                            if(_chart.decimalPlaces[index]){
                                                str = parseFloat(str).toFixed(_chart.decimalPlaces[index]);
                                            }
                                        }
                                        r += "<td rowspan='" + rLength + "' pos='"+index+"'>" + str + "</td>";
                                        if (isObject(v)) {
                                            rowObj(v,++index);
                                        } else {
                                            r += "</tr>";
                                        }
                                    } else {
                                        var tableData = k.split('#');
                                        if (rLength > 1) {
                                            classText = "nr tablesorter-childRow";
                                        }
                                        var str = tableData[0];
                                        if (!isNaN(str)){
                                            if (str.includes(".")){
                                                str = parseFloat(str).toFixed(2);
                                            }else{
                                                str = parseInt(str);
                                            }
                                        }
                                        r += "<tr class='" + classText + "'><td rowspan='" + rLength + "' pos='"+(index-1)+"' >" + str + "</td>";
                                        if (isObject(v)) {
                                            rowObj(v,index);
                                        } else {
                                            r += "</tr>";
                                        }
                                    }
                                });
                                return r;
                            }

                            var GrandRow_DT = '';
                            keys.forEach(function (d,i){
                                if(!i){
                                    GrandRow_DT += '<tfoot><tr>';
                                }
                                var temp = _chart._container + '_Grand_' + d.columnName;
                                var obj = objj.info.GrandTotal[d.columnName];
                                var val;
                                if(d.dataKey == 'Measure'){
                                    if(_chart._tableSetting && _chart._tableSetting.dataTypeOrder && _chart._tableSetting.dataTypeOrder[d.columnName]){
                                        val = obj[_chart._tableSetting.dataTypeOrder[d.columnName]];
                                    }else{
                                        val = obj['sumIndex'];
                                    }
                                    val = Number(val);
                                    if(_chart.decimalObj && _chart.decimalObj[d.columnName]){
                                        val = val.toFixed(_chart.decimalObj[d.columnName]);
                                    }else if(draw.checkFloat(val)){
                                        val = val.toFixed(2);
                                    }
                                }
                                if(!i){
                                    GrandRow_DT += '<td id="' + temp + '" pos="' + i + '" style="font-weight: bold;">Grand Total</td>';
                                }else{
                                    if(val != undefined){
                                        GrandRow_DT += '<td id="' + temp + '" pos="' + i + '" style="font-weight: bold;">' + val + '</td>';
                                    }else{
                                        GrandRow_DT += '<td id="' + temp + '" pos="' + i + '"></td>';
                                    }
                                }
                                if(i == keys.length-1){
                                    GrandRow_DT += '</tr></tfoot>';
                                }
                            });

                            if(_chart.grandTotal == 'yes'){
                                $("#table-" + _chart.id).append("<tbody>" + p + GrandRow_DT + "</tbody>");
                            }else{
                                $("#table-" + _chart.id).append("<tbody>" + p + "</tbody>");
                            }

                        }).then(function(){
                            $("#table-" + _chart.id).on('click', 'td', function(e){
                                var selectedPos=$(this).attr('pos');
                                if(_chart.keys[selectedPos] && _chart.keys[selectedPos].dataKey=="Measure"){
                                    return;
                                }
                                if(_chart.keys[selectedPos] && _chart.keys[selectedPos].dataKey=="Dimension" && $(this).text() == 'Grand Total'){
                                    return;
                                }

                                eChartServer._metadataId = _chart.metadataId;
                                var row = $(this).text();
                                if (row !== "" && row !==null){
                                    if (!_chart.filter) {
                                        _chart.filter = {};
                                    }
                                    if(!_chart.filter["|"+selectedPos]){
                                        _chart.filter["|"+selectedPos]=[];
                                    }
                                    var index = _chart.filter["|"+selectedPos].indexOf(row);
                                    if (index === -1) {
                                        $(this).attr('style', "background-color:blue;color:white");
                                        _chart.filter["|"+selectedPos].push(row);
                                    } else {
                                        $(this).attr('style', "");
                                        _chart.filter["|"+selectedPos].splice(index, 1);
                                        var filterObj={
                                            filters:_chart.filter,
                                            reportId:_chart.id,
                                            reset:false,
                                            chartKeyOrder:_chart.keys
                                        }
                                        eChartServer.server.filter({id:_chart.id},filterObj,function(d) {
                                            eChartServer.redrawEchartChartsOnly(_chart.id);
                                        });
                                    }
                                    if (_chart.filter["|"+selectedPos].length > 0) {
                                        var filterObj = {
                                            filters: _chart.filter,
                                            reportId: _chart.id,
                                            reset: false,
                                            chartKeyOrder: _chart.keys
                                        }
                                        eChartServer.server.filter({id:_chart.id},filterObj,function(d) {
                                            eChartServer.redrawEchartChartsOnly(_chart.id);
                                        });
                                    }
                                }
                            });
                            try {
                                $("#table-" + _chart.id).tablesorter({
                                    widgets: ['zebra', 'resizable', 'stickyHeaders'],
                                    widgetOptions: {
                                        resizable: true,
                                        // These are the default column widths which are used when the table is
                                        // initialized or resizing is reset; note that the "Age" column is not
                                        // resizable, but the width can still be set to 40px here
                                    }
                                });

                            } catch (e) {

                            }
                            draw.reportLoadedCountCheck(_chart);
                        });
                    }
                } catch (e) {

                }
            };
            draw.chartRegistry.otherRegistryCheck(_chart.id);
            draw.chartRegistry.register(_chart);
            draw.globalRegistry.register(_chart);
            draw.globalRegistry.syncLocalRegistries(_chart.id,"sketch");

            _chart.render();
        }

        draw._expandLineBarAxisConfig = function (config) {
            draw._barMeasure = [];
            draw._lineMeasure = [];
            draw._noOfBars = config["barMeasure"].length;
            draw._noOfLines = config["lineMeasure"].length;
            config["barMeasure"].forEach(function (d) {
                draw._barMeasure.push(d);
            });
            config["lineMeasure"].forEach(function (d) {
                draw._lineMeasure.push(d);
            });
        }

        draw._isAllFieldsPassed = function () {
            var chartConfig = draw._chartConfig;
            var axisConfig = draw._axisConfig;
            if (!(draw._chartConfig.key == "_compositeLineBarChart" || draw._chartConfig.key == "_dataTable" ||  draw._chartConfig.key == "_PivotCustomized" || draw._chartConfig.key == "_pivotTable" || draw._chartConfig.key == "_xTable" || draw._chartConfig.key == "_maleFemaleChartJs" || draw._chartConfig.key == "_buildingChartJs" || draw._chartConfig.key == "_mapChartJs" || draw._chartConfig.key == "_TextImageChart")) {
                if (axisConfig == undefined)
                    return false;
                if (chartConfig.required.Dimension != Object.keys(draw._dimension).length)
                    return false;
                if (!((chartConfig.required.Measure[0] <= Object.keys(draw._measure).length) && (chartConfig.required.Measure[1] >= Object.keys(draw._measure).length)))
                    return false;
                if (!((chartConfig.required.Aggregate[0] <= Object.keys(draw._aggregate).length) && (chartConfig.required.Aggregate[1] >= Object.keys(draw._aggregate).length)))
                    return false;
            } else if (draw._chartConfig.key == "_dataTable") {
                if (axisConfig == undefined)
                    return false;
                if (!((1 <= Object.keys(draw._measure).length) || (1 <= Object.keys(draw._dimension).length)))
                    return false;
            }
            return true;
        }


        // <---------------------------Chart Js Start------------------------------>

        // <------------ Line Chart Js------------------------->
        draw._lineChartJs = function (chart) {
            var containerId="chart-" + draw._container;
            var chartConfigKey=draw._chartConfig.key;
            var measures=draw._measure;
            var dimension=draw._dimension;
            var dataFormat=draw._dataFormat;
            var operator = draw._aggregate;
            if(chart!=undefined){
                var axisConfig={};
                axisConfig['aggregateModel']=chart._operator;
                axisConfig['checkboxModelMeasure']=chart._measures;
                axisConfig['checkboxModelDimension']=chart._dimension;
                if(Object.keys(chart._dataFormateVal).length==0 && chart._sort!=undefined){
                    axisConfig['dataFormat']={};
                    axisConfig['dataFormat']['sort']=chart._sort;
                    axisConfig['dataFormat']['sortMeasure']=chart._sortMeasure;
                }else{
                    axisConfig['dataFormat']=chart._dataFormateVal;
                }
                axisConfig['queryObj']=chart._metadataId;
                axisConfig['startPagination']=0;
                axisConfig['endPagination']=50;
                containerId = chart.id;
                chartConfigKey= "_lineChartJs";
            }else{
                var axisConfig=draw._axisConfig;
            }
            function lineChartDraw(start,end,type) {
                start = end - 50;
                if(start < 0){
                    start = 0;
                }
                axisConfig['startPagination']=start;
                axisConfig['endPagination']=end;
                var url="";
                if(type==0){
                    url="/draw";
                }else{
                    url="/redraw";
                }
                $.post(draw.nodeApiUrl+url, {"axisConfig":axisConfig,"chartType":chartConfigKey,"sessionId":draw.getAccessToken(),"reportId":containerId}).done(function(data){
                    var totalKeys=data['totalKey'];
                    var getReportId=data['reportId'].split("-")[1];
                    delete data['totalKey'];
                    delete data['reportId'];
                    draw.ExcludeKeyData = data;
                    draw.chartLoading_Hide();
                    var labels = [];
                    $.each(measures, function (key, value){
                        labels.push(key);
                    });
                    if(totalKeys>50 && type==0){
                        $("#rangeScroll_"+getReportId).rangeSlider({
                            bounds: {min: 0, max: totalKeys},
                            defaultValues:{min: 0, max: 50},
                            //range: {min: 0, max: 0},
                            //step: 3,
                            valueLabels:'hide',
                            symmetricPositionning:true
                        }).bind("userValuesChanged", function(e, data){
                            lineChartDraw(parseInt(data.values.min),parseInt(data.values.max),1);
                        });
                    }
                    var metadataId=axisConfig['queryObj'];
                    if(type==1){
                        var chart;
                        eChartServer.chartRegistry.list().forEach(function(d){
                            if(d.id=="chart-"+getReportId){
                                chart=d;
                            }
                        });
                        chart._group = data;
                        eChartServer.updateChart(chart,true);
                        eChartServer.highlightSelectedFilter(chart);
                        chart.chartInstance.update();
                    }else{
                        return eChartServer.setSessionId(draw.getAccessToken()).setMetadataId(metadataId).lineChart(containerId).operator(operator).dataFormat(dataFormat).dataSetLabels(labels).measures(measures).dimension(dimension).group(data).tKey(totalKeys).render();
                    }
                }).fail(function(response) {
                    draw.errorNotification("line");
                });
            }
            if(chart!=undefined){
                lineChartDraw(0,50,1);
            }else{
                lineChartDraw(0,50,0);
            }
        };

        draw._areaChartJs = function (chart) {
            var containerId="chart-" + draw._container;
            var chartConfigKey=draw._chartConfig.key;
            var measures=draw._measure;
            var dataFormat=draw._dataFormat;
            var dimension=draw._dimension;
            var operator = draw._aggregate;
            if(chart!=undefined){
                var axisConfig={};
                axisConfig['aggregateModel']=chart._operator;
                axisConfig['checkboxModelMeasure']=chart._measures;
                axisConfig['checkboxModelDimension']=chart._dimension;
                if(Object.keys(chart._dataFormateVal).length==0 && chart._sort!=undefined){
                    axisConfig['dataFormat']={};
                    axisConfig['dataFormat']['sort']=chart._sort;
                    axisConfig['dataFormat']['sortMeasure']=chart._sortMeasure;
                }else{
                    axisConfig['dataFormat']=chart._dataFormateVal;
                }
                axisConfig['queryObj']=chart._metadataId;
                axisConfig['startPagination']=0;
                axisConfig['endPagination']=50;
                containerId = chart.id;
                chartConfigKey= "_areaChartJs";
            }else{
                var axisConfig=draw._axisConfig;
            }
            function areaChartDraw(start,end,type) {
                start = end - 50;
                if(start < 0){
                    start = 0;
                }
                axisConfig['startPagination']=start;
                axisConfig['endPagination']=end;
                var url="/draw";
                if(type==1){
                    url="/redraw";
                }
                $.post(draw.nodeApiUrl+url, {"axisConfig":axisConfig,"chartType":chartConfigKey,"sessionId":draw.getAccessToken(),"reportId":containerId}).done(function(data){
                    var totalKeys=data['totalKey'];
                    var getReportId=data['reportId'].split("-")[1];
                    delete data['totalKey'];
                    delete data['reportId'];
                    draw.ExcludeKeyData = data;
                    draw.chartLoading_Hide();
                    var labels = [];
                    $.each(measures, function (key, value) {
                        labels.push(key);
                        // yGrp[key] = draw._createGroup(value, xDim, yGrp, labels);
                    });
                    var metadataId = axisConfig['queryObj'];
                    if(totalKeys>50 && type==0){
                        $("#rangeScroll_"+getReportId).rangeSlider({
                            bounds: {min: 0, max: totalKeys},
                            defaultValues:{min: 0, max: 50},
                            //range: {min: 0, max: 0},
                            //step: 3,
                            valueLabels:'hide',
                            symmetricPositionning:true
                        }).bind("userValuesChanged", function(e, data){
                            areaChartDraw(parseInt(data.values.min),parseInt(data.values.max),1);
                        });
                    }
                    if(type==1){
                        var chart;
                        eChartServer.chartRegistry.list().forEach(function(d){
                            if(d.id=="chart-"+getReportId){
                                chart=d;
                            }
                        });
                        chart._group = data;
                        eChartServer.updateChart(chart,true);
                        eChartServer.highlightSelectedFilter(chart);
                        chart.chartInstance.update();
                    }else{
                        return eChartServer.setSessionId(draw.getAccessToken()).setMetadataId(metadataId).areaChart(containerId).operator(operator).dataFormat(dataFormat).dataSetLabels(labels).measures(measures).dimension(dimension).group(data).tKey(totalKeys).render();
                    }
                    //return eChartServer.areaChart("chart-" + draw._container).dataFormate(draw._dataFormate).setDataFromServer(data).data().render();
                }).fail(function(response) {
                    draw.errorNotification("Area");
                });
            }
            if(chart!=undefined){
                areaChartDraw(0,50,1);
            }else{
                areaChartDraw(0,50,0);
            }
        };

        draw._barChartJs = function (chart) {
            var containerId="chart-" + draw._container;
            var chartConfigKey=draw._chartConfig.key;
            var measures=draw._measure;
            var dataFormat=draw._dataFormat;
            var dimension=draw._dimension;
            var operator = draw._aggregate;
            if(chart!=undefined){
                var axisConfig={};
                axisConfig['aggregateModel']=chart._operator;
                axisConfig['checkboxModelMeasure']=chart._measures;
                axisConfig['checkboxModelDimension']=chart._dimension;
                if(Object.keys(chart._dataFormateVal).length==0 && chart._sort!=undefined){
                    axisConfig['dataFormat']={};
                    axisConfig['dataFormat']['sort']=chart._sort;
                    axisConfig['dataFormat']['sortMeasure']=chart._sortMeasure;
                }else{
                    axisConfig['dataFormat']=chart._dataFormateVal;
                }
                axisConfig['queryObj']=chart._metadataId;
                axisConfig['startPagination']=0;
                axisConfig['endPagination']=50;
                containerId = chart.id;
                chartConfigKey= "_barChartJs";
            }else{
                var axisConfig=draw._axisConfig;
            }
            function barChartDraw(start,end,type) {
                start = end - 50;
                if(start < 0){
                    start = 0;
                }
                axisConfig['startPagination']=start;
                axisConfig['endPagination']=end;
                var url="/draw";
                if(type==1){
                    url="/redraw";
                }
                $.post(draw.nodeApiUrl+url, {"axisConfig":draw._axisConfig,"chartType":draw._chartConfig.key,"sessionId":draw.getAccessToken(),"reportId":containerId}).done(function(data){
                    var totalKeys=data['totalKey'];
                    var getReportId=data['reportId'].split("-")[1];
                    delete data['totalKey'];
                    delete data['reportId'];
                    draw.ExcludeKeyData = data;
                    draw.chartLoading_Hide();
                    var labels = [];
                    $.each(measures, function (key, value) {
                        labels.push(key);
                    });
                    var metadataId=axisConfig['queryObj'];
                    var excludeKey={};
                    if(axisConfig['excludeKey']){
                        excludeKey=axisConfig['excludeKey'];
                    }
                    if(totalKeys>50 && type==0){
                        $("#rangeScroll_"+getReportId).rangeSlider({
                            bounds: {min: 0, max: totalKeys},
                            defaultValues:{min: 0, max: 50},
                            //range: {min: 0, max: 0},
                            //step: 3,
                            valueLabels:'hide',
                            symmetricPositionning:true
                        }).bind("userValuesChanged", function(e, data){
                            barChartDraw(parseInt(data.values.min),parseInt(data.values.max),1);
                        });
                    }
                    if(type==1){
                        var chart;
                        eChartServer.chartRegistry.list().forEach(function(d){
                            if(d.id=="chart-"+getReportId){
                                chart=d;
                            }
                        });
                        chart._group = data;
                        eChartServer.updateChart(chart,true);
                        eChartServer.highlightSelectedFilter(chart);
                        chart.chartInstance.update();
                    }else {
                        return eChartServer.setSessionId(draw.getAccessToken()).setMetadataId(metadataId).barChart(containerId).operator(operator).dataFormat(dataFormat).dataSetLabels(labels).measures(measures).dimension(dimension).group(data).tKey(totalKeys).render();
                    }
                    //return eChartServer.lineChart("chart-" + draw._container).dataFormate(draw._dataFormate).setDataFromServer(data).data().render();
                }).fail(function(response) {
                    draw.errorNotification("Bar");
                });
            }
            if(chart!=undefined){
                barChartDraw(0,50,1);
            }else{
                barChartDraw(0,50,0);
            }

        };

        //****************************** CHartjs Row Chart*****************************************
        draw._rowChartJs = function (chart) {
            var containerId="chart-" + draw._container;
            var chartConfigKey=draw._chartConfig.key;
            var measures=draw._measure;
            var dataFormat=draw._dataFormat;
            var dimension=draw._dimension;
            var operator = draw._aggregate;
            if(chart!=undefined){
                var axisConfig={};
                axisConfig['aggregateModel']=chart._operator;
                axisConfig['checkboxModelMeasure']=chart._measures;
                axisConfig['checkboxModelDimension']=chart._dimension;
                if(Object.keys(chart._dataFormateVal).length==0 && chart._sort!=undefined){
                    axisConfig['dataFormat']={};
                    axisConfig['dataFormat']['sort']=chart._sort;
                    axisConfig['dataFormat']['sortMeasure']=chart._sortMeasure;
                }else{
                    axisConfig['dataFormat']=chart._dataFormateVal;
                }
                axisConfig['queryObj']=chart._metadataId;
                axisConfig['startPagination']=0;
                axisConfig['endPagination']=50;
                containerId = chart.id;
                chartConfigKey= "_rowChartJs";
            }else{
                var axisConfig=draw._axisConfig;
            }
            function rowChartDraw(start,end,type){
                start = end - 50;
                if(start < 0){
                    start = 0;
                }
                axisConfig['startPagination']=start;
                axisConfig['endPagination']=end;
                var url="/draw";
                if(type==1){
                    url="/redraw";
                }
                $.post(draw.nodeApiUrl+url, {"axisConfig":axisConfig,"chartType":draw._chartConfig.key,"sessionId":draw.getAccessToken(),"reportId":containerId}).done(function(data){
                    var totalKeys=data['totalKey'];
                    var getReportId=data['reportId'].split("-")[1];
                    delete data['totalKey'];
                    delete data['reportId'];
                    draw.ExcludeKeyData = data;
                    draw.chartLoading_Hide();
                    var labels = [];
                    $.each(measures, function (key, value) {
                        labels.push(key);
                        // yGrp[key] = draw._createGroup(value, xDim, yGrp, labels);
                    });
                    var metadataId=axisConfig['queryObj'];
                    if(totalKeys>50 && type==0){
                        $("#rangeScroll_"+getReportId).rangeSlider({
                            bounds: {min: 0, max: totalKeys},
                            defaultValues:{min: 0, max: 50},
                            //range: {min: 0, max: 0},
                            //step: 3,
                            valueLabels:'hide',
                            symmetricPositionning:true
                        }).bind("userValuesChanged", function(e, data){
                            rowChartDraw(parseInt(data.values.min),parseInt(data.values.max),1);
                        });
                    }
                    if(type==1){
                        var chart;
                        eChartServer.chartRegistry.list().forEach(function(d){
                            if(d.id=="chart-"+getReportId){
                                chart=d;
                            }
                        });
                        chart._group = data;
                        eChartServer.updateChart(chart,true);
                        eChartServer.highlightSelectedFilter(chart);
                        chart.chartInstance.update();
                    }else {
                        return eChartServer.setSessionId(draw.getAccessToken()).setMetadataId(metadataId).rowChart(containerId).operator(operator).dataFormat(dataFormat).dataSetLabels(labels).measures(measures).dimension(dimension).group(data).tKey(totalKeys).render();
                    }
                    //return eChartServer.lineChart("chart-" + draw._container).dataFormate(draw._dataFormate).setDataFromServer(data).data().render();
                }).fail(function(response) {
                    draw.errorNotification("Row");
                });
            }
            if(chart!=undefined){
                rowChartDraw(0,50,1);
            }else{
                rowChartDraw(0,50,0);
            }

        };

        // <--------------------Pie chart Js code---------------------------------->
        draw._pieChartJs = function () {
            var containerId="chart-" + draw._container;
            var axisConfig=draw._axisConfig;
            var chartConfigKey=draw._chartConfig.key;
            var measures=draw._measure;
            var dataFormat=draw._dataFormat;
            var operator = draw._aggregate;
            var dimension=draw._dimension;
            $.post(draw.nodeApiUrl+"/draw", {"axisConfig":axisConfig,"chartType":chartConfigKey,"sessionId":draw.getAccessToken(),"reportId":containerId}).done(function(data){
                var totalKeys=data['totalKey'];
                var getReportId=data['reportId'].split("-")[1];
                delete data['totalKey'];
                delete data['reportId'];
                draw.ExcludeKeyData = data;
                draw.chartLoading_Hide();
                var labels = [];
                $.each(draw._measure, function (key, value) {
                    labels.push(key);
                    // yGrp[key] = draw._createGroup(value, xDim, yGrp, labels);
                });
                var metadataId=axisConfig['queryObj'];
                return eChartServer.setSessionId(draw.getAccessToken()).setMetadataId(metadataId).pieChart(containerId).operator(operator).dataFormat(dataFormat).measures(measures).dimension(dimension).group(data).tKey(totalKeys).render();
                //return eChartServer.lineChart("chart-" + draw._container).dataFormate(draw._dataFormate).setDataFromServer(data).data().render();
            }).fail(function(response) {
                draw.errorNotification("Pie");
            });

        };
        // <----------------------End Pie Chart------------------------------------------------------------------->


        // <--------------------bubble chart Js code---------------------------------->
        draw._bubbleChartJs = function () {
            var containerId = "chart-" + draw._container;
            var axisConfig = draw._axisConfig;
            // var chartConfigKey = "_bubbleChartJs";
            var chartConfigKey = draw._chartConfig.key;
            var measures = {};
            measures[JSON.parse(draw._axisConfig.bubbleObject.xAxis).columnName] = JSON.parse(draw._axisConfig.bubbleObject.xAxis);
            measures[JSON.parse(draw._axisConfig.bubbleObject.yAxis).columnName] = JSON.parse(draw._axisConfig.bubbleObject.yAxis);
            if(draw._axisConfig.bubbleObject.radius != undefined){
                measures[JSON.parse(draw._axisConfig.bubbleObject.radius).columnName] = JSON.parse(draw._axisConfig.bubbleObject.radius);
            }
            var dimension = {};
            dimension[JSON.parse(draw._axisConfig.bubbleObject.groupColor).columnName] = JSON.parse(draw._axisConfig.bubbleObject.groupColor);
            var operator = {};
            operator[JSON.parse(draw._axisConfig.bubbleObject.xAxis).columnName] = {"key": "sumIndex", "value": "Sum", "type": "Aggregate"};
            operator[JSON.parse(draw._axisConfig.bubbleObject.yAxis).columnName] = {"key": "sumIndex", "value": "Sum", "type": "Aggregate"};
            if(draw._axisConfig.bubbleObject.radius != undefined && draw._axisConfig.bubbleObject.radius != ""){
                operator[JSON.parse(draw._axisConfig.bubbleObject.radius).columnName] = {"key": "sumIndex", "value": "Sum", "type": "Aggregate"};
            }

            var dataFormat = {};
            dataFormat.bubbleObject = axisConfig.bubbleObject;
            if(draw._axisConfig.bubbleObject.groupColor != undefined && draw._axisConfig.bubbleObject.groupColor != ""){
                dataFormat['groupColor'] = draw._axisConfig.bubbleObject.groupColor;
            }

            dataFormat['xaxis'] = {};
            if(draw._axisConfig.bubbleObject.dataFormat.xaxis){
                dataFormat['xaxis'] = draw._axisConfig.bubbleObject.dataFormat.xaxis;
            }
            dataFormat['yaxis'] = {};
            if(draw._axisConfig.bubbleObject.dataFormat.yaxis){
                dataFormat['yaxis'] = draw._axisConfig.bubbleObject.dataFormat.yaxis;
            }
            if(draw._axisConfig.bubbleObject.dataFormat.canvas){
                dataFormat['canvas'] = draw._axisConfig.bubbleObject.dataFormat.canvas;
            }

            if(draw._dataFormat && draw._dataFormat.axisColor){
                dataFormat['axisColor'] = draw._dataFormat.axisColor;
            }
            if(draw._dataFormat && draw._dataFormat.axisLabelColor){
                dataFormat['axisLabelColor'] = draw._dataFormat.axisLabelColor;
            }
            if(draw._dataFormat && draw._dataFormat.tooltip){
                dataFormat['tooltip'] = draw._dataFormat.tooltip;
            }
            if(draw._dataFormat && draw._dataFormat.tooltipSelector){
                dataFormat['tooltipSelector'] = draw._dataFormat.tooltipSelector;
            }

            draw._axisConfig['aggregateModel'] = {};
            draw._axisConfig['aggregateModel'][JSON.parse(draw._axisConfig.bubbleObject.xAxis).columnName] = {"key": "sumIndex", "value": "Sum", "type": "Aggregate"};
            draw._axisConfig['aggregateModel'][JSON.parse(draw._axisConfig.bubbleObject.yAxis).columnName] = {"key": "sumIndex", "value": "Sum", "type": "Aggregate"};
            if(draw._axisConfig.bubbleObject.radius != undefined){
                draw._axisConfig['aggregateModel'][JSON.parse(draw._axisConfig.bubbleObject.radius).columnName] = {"key": "sumIndex", "value": "Sum", "type": "Aggregate"};
            }

            draw._axisConfig['checkboxModelMeasure'] = {};
            draw._axisConfig['checkboxModelMeasure'][JSON.parse(draw._axisConfig.bubbleObject.xAxis).columnName] = JSON.parse(draw._axisConfig.bubbleObject.xAxis);
            draw._axisConfig['checkboxModelMeasure'][JSON.parse(draw._axisConfig.bubbleObject.yAxis).columnName] = JSON.parse(draw._axisConfig.bubbleObject.yAxis);
            if(draw._axisConfig.bubbleObject.radius != undefined){
                draw._axisConfig['checkboxModelMeasure'][JSON.parse(draw._axisConfig.bubbleObject.radius).columnName] = JSON.parse(draw._axisConfig.bubbleObject.radius);
            }

            draw._axisConfig['checkboxModelDimension'] = {};
            draw._axisConfig['checkboxModelDimension'][JSON.parse(draw._axisConfig.bubbleObject.dimension).columnName] = JSON.parse(draw._axisConfig.bubbleObject.dimension);
            if(draw._axisConfig.bubbleObject.groupColor != undefined && draw._axisConfig.bubbleObject.groupColor != ""){
                draw._axisConfig['checkboxModelDimension'][JSON.parse(draw._axisConfig.bubbleObject.groupColor).columnName] = JSON.parse(draw._axisConfig.bubbleObject.groupColor);
            }

            draw._axisConfig['dataFormat'] = {};
            if(draw._axisConfig.bubbleObject.tooltip != undefined && draw._axisConfig.bubbleObject.tooltip != ""){
                draw._axisConfig['dataFormat']['tooltip'] = draw._axisConfig.bubbleObject.tooltip;
            }
            if(draw._axisConfig.bubbleObject.tooltipObj != undefined && draw._axisConfig.bubbleObject.tooltipObj != ""){
                draw._axisConfig['dataFormat']['tooltipSelector'] = draw._axisConfig.bubbleObject.tooltipObj;
            }

            if(draw._axisConfig.bubbleObject.topN != undefined && draw._axisConfig.bubbleObject.topN != ""){
                dataFormat['topN'] = draw._axisConfig.bubbleObject.topN;
                draw._axisConfig['dataFormat']['topN'] = draw._axisConfig.bubbleObject.topN;
            }
            if(draw._axisConfig.bubbleObject.topnMeasure != undefined && draw._axisConfig.bubbleObject.topnMeasure != ""){
                dataFormat['topnMeasure'] = draw._axisConfig.bubbleObject.topnMeasure;
                draw._axisConfig['dataFormat']['topnMeasure'] = draw._axisConfig.bubbleObject.topnMeasure;
            }
            if(draw._axisConfig.bubbleObject.topNOther != undefined && draw._axisConfig.bubbleObject.topNOther != ""){
                dataFormat['topNOther'] = draw._axisConfig.bubbleObject.topNOther;
                draw._axisConfig['dataFormat']['topNOther'] = draw._axisConfig.bubbleObject.topNOther;
            }

            $.post(draw.nodeApiUrl + "/draw", {"axisConfig":draw._axisConfig, "chartType":chartConfigKey, "sessionId":draw.getAccessToken(), "reportId":containerId}).done(function(data){
                var totalKeys=data['totalKey'];
                var getReportId=data['reportId'].split("-")[1];
                delete data['totalKey'];
                delete data['reportId'];
                draw.ExcludeKeyData = data;
                draw.chartLoading_Hide();
                var labels = [];
                $.each(measures, function (key, value) {
                    labels.push(key);
                });
                var metadataId = draw._axisConfig['queryObj'];
                return eChartServer.setSessionId(draw.getAccessToken()).setMetadataId(metadataId).bubbleChart(containerId).operator(operator).dataFormat(dataFormat).measures(measures).dimension(dimension).group(data).tKey(totalKeys).render();
            }).fail(function(response) {
                draw.errorNotification("Bubble");
            });
        };
        // <----------------------End bubble Chart------------------------------------------------------------------->
        // <--------------------Heat Map chart Js code---------------------------------->
        draw._heatChartJs = function () {
            return eChart.heatChart(draw._container);
        };

        // <--------------------Composite chart Js code---------------------------------->
        draw._compositeChartJs = function (chart) {
            var containerId="chart-" + draw._container;
            var chartConfigKey=draw._chartConfig.key;
            var measures=draw._measure;
            var dataFormat=draw._dataFormat;
            var operator = draw._aggregate;
            var dimension=draw._dimension;
            if(chart!=undefined){
                var axisConfig={};
                axisConfig['aggregateModel']=chart._operator;
                axisConfig['checkboxModelMeasure']=chart._measures;
                axisConfig['checkboxModelDimension']=chart._dimension;
                if(Object.keys(chart._dataFormateVal).length==0 && chart._sort!=undefined){
                    axisConfig['dataFormat']={};
                    axisConfig['dataFormat']['sort']=chart._sort;
                    axisConfig['dataFormat']['sortMeasure']=chart._sortMeasure;
                }else{
                    axisConfig['dataFormat']=chart._dataFormateVal;
                }
                axisConfig['queryObj']=chart._metadataId;
                axisConfig['startPagination']=0;
                axisConfig['endPagination']=50;
                containerId = chart.id;
                chartConfigKey= "_compositeChartJs";
            }else{
                var axisConfig=draw._axisConfig;
            }
            function compositeChartDraw(start,end,type){
                start = end - 50;
                if(start < 0){
                    start = 0;
                }
                axisConfig['startPagination']=start;
                axisConfig['endPagination']=end;
                var url="/draw";
                if(type==1){
                    url="/redraw";
                }
                $.post(draw.nodeApiUrl+url, {"axisConfig":draw._axisConfig,"chartType":draw._chartConfig.key,"sessionId":draw.getAccessToken(),"reportId":containerId}).done(function(data){
                    var totalKeys=data['totalKey'];
                    var getReportId=data['reportId'].split("-")[1];
                    delete data['totalKey'];
                    delete data['reportId'];
                    draw.ExcludeKeyData = data;
                    draw.chartLoading_Hide();
                    var labels = [];
                    $.each(measures, function (key, value) {
                        labels.push(key);
                        // yGrp[key] = draw._createGroup(value, xDim, yGrp, labels);
                    });
                    var metadataId=axisConfig['queryObj'];
                    if(totalKeys>50 && type==0){
                        $("#rangeScroll_"+getReportId).rangeSlider({
                            bounds: {min: 0, max: totalKeys},
                            defaultValues:{min: 0, max: 50},
                            //range: {min: 0, max: 0},
                            //step: 3,
                            valueLabels:'hide',
                            symmetricPositionning:true
                        }).bind("userValuesChanged", function(e, data){
                            compositeChartDraw(parseInt(data.values.min),parseInt(data.values.max),1);
                        });
                    }
                    if(type==1){
                        var chart;
                        eChartServer.chartRegistry.list().forEach(function(d){
                            if(d.id=="chart-"+getReportId){
                                chart=d;
                            }
                        });
                        chart._group = data;
                        eChartServer.updateChart(chart,true);
                        eChartServer.highlightSelectedFilter(chart);
                        chart.chartInstance.update();
                    }else {
                        return eChartServer.setSessionId(draw.getAccessToken()).setMetadataId(metadataId).compositeChart(containerId).operator(operator).dataFormat(dataFormat).dataSetLabels(labels).measures(measures).dimension(dimension).group(data).tKey(totalKeys).render();
                    }
                    //return eChartServer.lineChart("chart-" + draw._container).dataFormate(draw._dataFormate).setDataFromServer(data).data().render();
                }).fail(function(response) {
                    draw.errorNotification("Composite");
                });
            }
            if(chart!=undefined){
                compositeChartDraw(0,50,1);
            }else{
                compositeChartDraw(0,50,0);
            }

        };
        // <----------------------End Composite  Chart------------------------------------------------------------------->



        // <--------------------stacked chart Js code---------------------------------->
        draw._stackedChartJs = function () {
            var containerId="chart-" + draw._container;
            var axisConfig=draw._axisConfig;
            var chartConfigKey=draw._chartConfig.key;
            var measures=draw._measure;
            var dataFormat=draw._dataFormat;
            var operator = draw._aggregate;
            var dimension=draw._dimension;
            $.post(draw.nodeApiUrl+"/draw", {"axisConfig":draw._axisConfig,"chartType":draw._chartConfig.key,"sessionId":draw.getAccessToken(),"reportId":containerId}).done(function(data){
                draw.ExcludeKeyData = data;

                draw.chartLoading_Hide();
                var labels = [];
                $.each(measures, function (key, value) {
                    labels.push(key);
                    // yGrp[key] = draw._createGroup(value, xDim, yGrp, labels);
                });
                var metadataId=axisConfig['queryObj'];
                return eChartServer.setSessionId(draw.getAccessToken()).setMetadataId(metadataId).compositeChart(containerId).operator(operator).dataFormate(dataFormat).dataSetLabels(labels).measures(measures).dimension(dimension).group(data).tKey(totalKeys).render();
                //return eChartServer.lineChart("chart-" + draw._container).dataFormate(draw._dataFormate).setDataFromServer(data).data().render();
            }).fail(function(response) {
                draw.errorNotification("Stacked");
            });

        };
        // <----------------------End stacked Chart------------------------------------------------------------------
        draw._xTable = function () {
            var keyOfObj = "";
            var data = [];
            if (!$.isEmptyObject(draw._dimension)) {
                $.each(draw._dimension, function (key, val) {
                    keyOfObj = key;
                });
                var dim = draw._crossfilter
                    .dimension(function (d) {
                        return d[draw._dimension[keyOfObj].value];
                    });
                data = jQuery.extend(true, [], dim.top(Infinity));
            } else {
                data = jQuery.extend(true, [], draw._data);
            }
            var tableHeaders = "";
            var k = 0;
            var ColumnArrayForDataTable = [];
            var keys = [];
            try {
                $.each(draw._dimension, function (key, value) {
                    keys.push(key);
                });
            } catch (e) {

            }
            try {
                $.each(draw._measure, function (key, value) {
                    keys.push(key);
                });
            } catch (e) {

            }

            data.forEach(function (d) {
                $.each(d, function (key, value) {
                    if (keys.indexOf(key) === -1)
                        delete d[key];
                });
            });

            // Check Obj
            //Define Group By
            var groupBy = "";
            var obj;
            if (draw._tableSetting) {
                groupBy = draw._tableSetting.group;
            }
            keys.forEach(function (d) {
                var operator = "";
                if (draw._tableSetting) {
                    $.each(draw._tableSetting.footer, function (key, value) {
                        if (key === d) {
                            if (value) {
                                operator = value.operation;
                            }
                        }
                    });
                }
                if (groupBy != undefined) {
                    obj = new Object({
                        "title": d,
                        "field": d,
                        "topCalc": operator
                    });
                } else {
                    obj = new Object({
                        "title": d,
                        "field": d,
                        "bottomCalc": operator
                    });
                }
                ColumnArrayForDataTable.push(obj);
            });
            if (!$("#chart-" + draw._container).hasClass('tabulator')) {
                $("#chart-" + draw._container).tabulator({
                    height: 320,
                    variableHeight: true,
                    fitColumns: true,
                    // pagination: "local",
                    //paginationSize: 7,
                    movableColumns: true,
                    groupBy: groupBy,
                    columns: ColumnArrayForDataTable,
                });
                var tabledata = data;
                return $("#chart-" + draw._container).tabulator("setData", tabledata);
            } else {
                $("#chart-" + draw._container).tabulator('destroy');
                $("#chart-" + draw._container).tabulator({
                    height: 320,
                    variableHeight: true,
                    fitColumns: true,
                    // pagination: "local",
                    //paginationSize: 7,
                    movableColumns: true,
                    groupBy: groupBy,
                    columns: ColumnArrayForDataTable,
                });
                //define some sample data
                var tabledata = data;
                //load sample data into the table
                return  $("#chart-" + draw._container).tabulator("setData", tabledata);
            }
        }

        draw._lineBarChartJs = function (chart) {
            var containerId="chart-" + draw._container;
            var chartConfigKey=draw._chartConfig.key;
            var measures=draw._measure;
            var dataFormat=draw._dataFormat;
            var operator = draw._aggregate;
            var dimension=draw._dimension;
            if(chart!=undefined){
                var axisConfig={};
                axisConfig['aggregateModel']=chart._operator;
                axisConfig['checkboxModelMeasure']=chart._measures;
                axisConfig['checkboxModelDimension']=chart._dimension;
                if(Object.keys(chart._dataFormateVal).length==0 && chart._sort!=undefined){
                    axisConfig['dataFormat']={};
                    axisConfig['dataFormat']['sort']=chart._sort;
                    axisConfig['dataFormat']['sortMeasure']=chart._sortMeasure;
                }else{
                    axisConfig['dataFormat']=chart._dataFormateVal;
                }
                axisConfig['queryObj']=chart._metadataId;
                axisConfig['startPagination']=0;
                axisConfig['endPagination']=50;
                containerId = chart.id;
                chartConfigKey= "_lineBarChartJs";
            }else{
                var axisConfig=draw._axisConfig;
            }
            function lineBarChartDraw(start,end,type) {
                start = end - 50;
                if(start < 0){
                    start = 0;
                }
                axisConfig['startPagination']=start;
                axisConfig['endPagination']=end;
                var url="/draw";
                if(type==1){
                    url="/redraw";
                }
                $.post(draw.nodeApiUrl+url, {"axisConfig":draw._axisConfig,"chartType":draw._chartConfig.key,"sessionId":draw.getAccessToken(),"reportId":containerId})
                    .done(function(data){
                        var totalKeys=data['totalKey'];
                        var getReportId=data['reportId'].split("-")[1];
                        delete data['totalKey'];
                        delete data['reportId'];
                        draw.ExcludeKeyData = data;

                        draw.chartLoading_Hide();
                        var labels = [];
                        $.each(measures, function (key, value) {
                            labels.push(key);
                            // yGrp[key] = draw._createGroup(value, xDim, yGrp, labels);
                        });
                        var metadataId=axisConfig['queryObj'];
                        if(totalKeys>50 && type==0){
                            $("#rangeScroll_"+getReportId).rangeSlider({
                                bounds: {min: 0, max: totalKeys},
                                defaultValues:{min: 0, max: 50},
                                //range: {min: 0, max: 0},
                                //step: 3,
                                valueLabels:'hide',
                                symmetricPositionning:true
                            }).bind("userValuesChanged", function(e, data){
                                lineBarChartDraw(parseInt(data.values.min),parseInt(data.values.max),1);
                            });
                        }
                        if(type==1){
                            var chart;
                            eChartServer.chartRegistry.list().forEach(function(d){
                                if(d.id=="chart-"+getReportId){
                                    chart=d;
                                }
                            });
                            chart._group = data;
                            eChartServer.updateChart(chart,true);
                            eChartServer.highlightSelectedFilter(chart);
                            chart.chartInstance.update();
                        }else {
                            return eChartServer.setSessionId(draw.getAccessToken()).setMetadataId(metadataId).lineBarChart(containerId).operator(operator).dataFormat(dataFormat).dataSetLabels(labels).measures(measures).dimension(dimension).group(data).tKey(totalKeys).render();
                        }
                        //return eChartServer.lineChart("chart-" + draw._container).dataFormate(draw._dataFormate).setDataFromServer(data).data().render();
                    }).fail(function(response) {
                    draw.errorNotification("Line bar");
                });
            }
            if(chart!=undefined){
                lineBarChartDraw(0,50,1);
            }else{
                lineBarChartDraw(0,50,0);
            }
        }

        draw._dualAxisChartJs = function (chart) {
            var containerId="chart-" + draw._container;
            var chartConfigKey=draw._chartConfig.key;
            var measures=draw._measure;
            var dataFormat=draw._dataFormat;
            var operator = draw._aggregate;
            var dimension=draw._dimension;
            if(chart!=undefined){
                var axisConfig={};
                axisConfig['aggregateModel']=chart._operator;
                axisConfig['checkboxModelMeasure']=chart._measures;
                axisConfig['checkboxModelDimension']=chart._dimension;
                if(Object.keys(chart._dataFormateVal).length==0 && chart._sort!=undefined){
                    axisConfig['dataFormat']={};
                    axisConfig['dataFormat']['sort']=chart._sort;
                    axisConfig['dataFormat']['sortMeasure']=chart._sortMeasure;
                }else{
                    axisConfig['dataFormat']=chart._dataFormateVal;
                }
                axisConfig['queryObj']=chart._metadataId;
                axisConfig['startPagination']=0;
                axisConfig['endPagination']=50;
                containerId = chart.id;
                chartConfigKey= "_dualAxisChartJs";
            }else{
                var axisConfig=draw._axisConfig;
            }
            function dualAxisChartDraw(start,end,type) {
                start = end - 50;
                if(start < 0){
                    start = 0;
                }
                axisConfig['startPagination']=start;
                axisConfig['endPagination']=end;
                var url="/draw";
                if(type==1){
                    url="/redraw";
                }
                $.post(draw.nodeApiUrl+url, {"axisConfig":draw._axisConfig,"chartType":draw._chartConfig.key,"sessionId":draw.getAccessToken(),"reportId":containerId}).done(function(data){
                    var totalKeys=data['totalKey'];
                    var getReportId=data['reportId'].split("-")[1];
                    delete data['totalKey'];
                    delete data['reportId'];
                    draw.ExcludeKeyData = data;
                    draw.chartLoading_Hide();
                    var labels = [];
                    $.each(measures, function (key, value) {
                        labels.push(key);
                        // yGrp[key] = draw._createGroup(value, xDim, yGrp, labels);
                    });
                    var metadataId=axisConfig['queryObj'];
                    if(totalKeys>50 && type==0){
                        $("#rangeScroll_"+getReportId).rangeSlider({
                            bounds: {min: 0, max: totalKeys},
                            defaultValues:{min: 0, max: 50},
                            //range: {min: 0, max: 0},
                            //step: 3,
                            valueLabels:'hide',
                            symmetricPositionning:true
                        }).bind("userValuesChanged", function(e, data){
                            dualAxisChartDraw(parseInt(data.values.min),parseInt(data.values.max),1);
                        });
                    }
                    if(type==1){
                        var chart;
                        eChartServer.chartRegistry.list().forEach(function(d){
                            if(d.id=="chart-"+getReportId){
                                chart=d;
                            }
                        });
                        chart._group = data;
                        eChartServer.updateChart(chart,true);
                        eChartServer.highlightSelectedFilter(chart);
                        chart.chartInstance.update();
                    }else {
                        return eChartServer.setSessionId(draw.getAccessToken()).setMetadataId(metadataId).dualAxisChart(containerId).operator(operator).dataFormat(dataFormat).dataSetLabels(labels).measures(measures).dimension(dimension).group(data).tKey(totalKeys).render();
                    }
                    //return eChartServer.lineChart("chart-" + draw._container).dataFormate(draw._dataFormate).setDataFromServer(data).data().render();
                }).fail(function(response) {
                    draw.errorNotification("Dual axis");
                });
            }
            if(chart!=undefined){
                dualAxisChartDraw(0,50,1);
            }else{
                dualAxisChartDraw(0,50,0);
            }

        }




























        // Floor Plan Comparison

        draw._floorPlanJs = function () {
            // $('#FloorPlan').modal('show');
        }

        draw._floorPlanDraw = function () {
            var FloorPlanObj = {};
            FloorPlanObj['height'] = draw._axisConfig.FloorPlanObject.height;
            FloorPlanObj['width'] = draw._axisConfig.FloorPlanObject.width;
            FloorPlanObj['image'] = draw._axisConfig.FloorPlanObject.image;
            FloorPlanObj['tooltip'] = draw._axisConfig.FloorPlanObject.tooltip;
            FloorPlanObj['tooltipData'] = draw._axisConfig.FloorPlanObject.tooltipData;
            var multipleTooltip = FloorPlanObj['tooltipData'];
            // Custom Tooltip
            var floorTooltip = FloorPlanObj['tooltip'][0];
            draw._axisConfig.tooltipSelector = floorTooltip;

            var measure = FloorPlanObj['tooltip'][0];
            var dimension = FloorPlanObj['tooltip'][0];

            var yGrp = {}, colName, tooltipData, tooltip_key = [];
            var xDim = draw._createDimension(dimension);

            yGrp[measure.columnName] = draw._createGroup(measure, xDim);
            yGrp[measure.columnName].all().forEach(function (d) {
                var tempStr = multipleTooltip;
                $.each(floorTooltip, function (key, val) {
                    if (val.dataKey == 'Measure') {
                        tooltipData = '<sum(' + val.columnName + ')>';
                        tooltip_key.push(tooltipData);
                    } else {
                        tooltipData = '<' + val.columnName + '>';
                        tooltip_key.push(tooltipData);
                    }
                    $.each(tooltip_key, function (toolKey, toolVal) {
                        tempStr = tempStr.replace(toolVal, d.value[tooltipData]);
                    });
                    draw._FloorMapTooltip = tempStr;
                });
            });

            // Seat Mapping

            var measure = draw._axisConfig.FloorPlanObject.measure;
            var grpData = draw._axisConfig.FloorPlanObject.grpData;
            var grpColorArrLength = grpData.length;
            var floorGrpMapping = {}, floorGrpMappingArr = [];

            grpData.forEach(function (d) {
                floorGrpMapping[d.key] = (d.value.sumIndex) / grpColorArrLength;
                floorGrpMappingArr.push(Object.assign({}, floorGrpMapping));
            });




            // Set SVG Image

            var generated_Div = $("#chart-" + draw._container).append("<div id='svgContainer'></div>");
            var width = 820, height = 500;
            var text = new Konva.Text({});

            var stage = new Konva.Stage({
                container: 'svgContainer',
                width: width,
                height: height
            });

            var layer = new Konva.Layer();

            if (FloorPlanObj.height && FloorPlanObj.width) {
                var h = (FloorPlanObj.height * 35) / 100;
                var w = (FloorPlanObj.width * 90) / 100;
                if (h > 200) {
                    h = 200;
                }
                if (w > 700) {
                    w = 700;
                }
                var imageObj = new Image();
                imageObj.onload = function () {
                    var setSVGImg = new Konva.Image({
                        x: 50,
                        y: 50,
                        image: imageObj,
                        width: w,
                        height: h,
                    });
                    layer.add(setSVGImg);
                    setSVGImg.moveToBottom();
                    stage.add(layer);
                };
                imageObj.src = FloorPlanObj.image;
            } else if (FloorPlanObj.height) {
                var h = (FloorPlanObj.height * 35) / 100;
                if (h > 200) {
                    h = 200;
                }
                var imageObj = new Image();
                imageObj.onload = function () {
                    var setSVGImg = new Konva.Image({
                        x: 50,
                        y: 50,
                        image: imageObj,
                        width: 700,
                        height: h,
                    });
                    layer.add(setSVGImg);
                    setSVGImg.moveToBottom();
                    stage.add(layer);
                };
                imageObj.src = FloorPlanObj.image;
            } else if (FloorPlanObj.width) {
                var w = (FloorPlanObj.width * 90) / 100;
                if (w > 700) {
                    w = 700;
                }
                var imageObj = new Image();
                imageObj.onload = function () {
                    var setSVGImg = new Konva.Image({
                        x: 50,
                        y: 50,
                        image: imageObj,
                        width: w,
                        height: 200,
                    });
                    layer.add(setSVGImg);
                    setSVGImg.moveToBottom();
                    stage.add(layer);
                };
                imageObj.src = FloorPlanObj.image;
            } else {
                var imageObj = new Image();
                imageObj.onload = function () {
                    var setSVGImg = new Konva.Image({
                        x: 50,
                        y: 50,
                        image: imageObj,
                        width: 700,
                        height: 200,
                    });
                    layer.add(setSVGImg);
                    setSVGImg.moveToBottom();
                    stage.add(layer);
                };
                imageObj.src = FloorPlanObj.image;
            }

            // Draw Scales around SVG Image

            var y_Line = new Konva.Line({
                points: [35, 0, 35, 271],
                stroke: 'black',
                strokeWidth: 1,
            });
            layer.add(y_Line);

            var x_Line = new Konva.Line({
                points: [35, 271, 810, 271],
                stroke: 'black',
                strokeWidth: 1,
            });
            layer.add(x_Line);

            // Draw Occupancy Scale

            var occupiedText = new Konva.Text({
                x: 33,
                y: 302,
                text: '% Occupied',
                fontSize: 15,
                fontFamily: 'Calibri',
                fill: 'black'
            });
            layer.add(occupiedText);

            var firstRect = new Konva.Rect({
                x: 33,
                y: 318,
                width: 50,
                height: 10,
                fill: 'red',
                stroke: 'black',
                strokeWidth: 1
            });

            layer.add(firstRect);

            var secondRect = new Konva.Rect({
                x: 83,
                y: 318,
                width: 50,
                height: 10,
                fill: 'blue',
                stroke: 'black',
                strokeWidth: 1
            });

            layer.add(secondRect);

            var rectFirstText = new Konva.Text({
                x: 30,
                y: 330,
                text: '0',
                fontSize: 13,
                fontFamily: 'Calibri',
                fill: 'black'
            });
            layer.add(rectFirstText);

            var rectMidText = new Konva.Text({
                x: 75,
                y: 330,
                text: '0.5',
                fontSize: 13,
                fontFamily: 'Calibri',
                fill: 'black'
            });
            layer.add(rectMidText);
            var rectLastText = new Konva.Text({
                x: 127,
                y: 330,
                text: '1',
                fontSize: 13,
                fontFamily: 'Calibri',
                fill: 'black'
            });
            layer.add(rectLastText);
            // Set Origin Text
            var origin_Text = new Konva.Text({
                x: 25,
                y: 270,
                text: '0',
                fontSize: 20,
                fontFamily: 'Calibri',
                fill: 'black'
            });
            layer.add(origin_Text);
            // Set Y-Line Text
            if (FloorPlanObj.height) {
                var xCoord = -2, yCoord = 5, yCoordLine = 15, text_display = FloorPlanObj.height, temp = FloorPlanObj.height;
                for (var i = 0; i < 4; i++) {
                    if (i == 0) {
                        text_display = text_display;
                        text_display = temp;
                        yCoord = yCoord;
                        yCoordLine = yCoordLine;
                    } else if (i == 1) {
                        text_display = (text_display + (text_display / 2)) / 2;
                        yCoord += 70;
                        yCoordLine += 70;
                    } else if (i == 2) {
                        text_display = temp;
                        text_display /= 2;
                        yCoord += 70;
                        yCoordLine += 70;
                    } else {
                        text_display = temp;
                        text_display /= 4;
                        yCoord += 70;
                        yCoordLine += 70;
                    }
                    var y_LineText = new Konva.Text({
                        x: xCoord,
                        y: yCoord,
                        text: text_display,
                        fontSize: 20,
                        fontFamily: 'Calibri',
                        fill: 'black'
                    });
                    layer.add(y_LineText);

                    var y_Line = new Konva.Line({
                        points: [30, yCoordLine, 35, yCoordLine],
                        stroke: 'black',
                        strokeWidth: 1,
                    });
                    layer.add(y_Line);
                }
            } else {
                var xCoord = -2, yCoord = 5, yCoordLine = 15, text_display = 400, temp = text_display;
                for (var i = 0; i < 4; i++) {
                    if (i == 0) {
                        text_display = text_display;
                        text_display = temp;
                        yCoord = yCoord;
                        yCoordLine = yCoordLine;
                    } else if (i == 1) {
                        text_display = (text_display + (text_display / 2)) / 2;
                        yCoord += 70;
                        yCoordLine += 70;
                    } else if (i == 2) {
                        text_display = temp;
                        text_display /= 2;
                        yCoord += 70;
                        yCoordLine += 70;
                    } else {
                        text_display = temp;
                        text_display /= 4;
                        yCoord += 70;
                        yCoordLine += 70;
                    }
                    var y_LineText = new Konva.Text({
                        x: xCoord,
                        y: yCoord,
                        text: text_display,
                        fontSize: 20,
                        fontFamily: 'Calibri',
                        fill: 'black'
                    });
                    layer.add(y_LineText);

                    var y_Line = new Konva.Line({
                        points: [30, yCoordLine, 35, yCoordLine],
                        stroke: 'black',
                        strokeWidth: 1,
                    });
                    layer.add(y_Line);
                }
            }
            // Set X-Line Text
            if (FloorPlanObj.width) {
                var xCoord = 780, xCoordLine = 193, yCoord = 275, text_display = FloorPlanObj.width, temp = FloorPlanObj.width;
                for (var i = 0; i < 4; i++) {
                    if (i == 0) {
                        text_display = text_display;
                        text_display = temp;
                        xCoord = xCoord;
                        xCoordLine = xCoordLine;
                    } else if (i == 1) {
                        text_display = (text_display + (text_display / 2)) / 2;
                        xCoord -= 200;
                        xCoordLine += 200
                    } else if (i == 2) {
                        text_display = temp;
                        text_display /= 2;
                        xCoord -= 200;
                        xCoordLine += 200
                    } else {
                        text_display = temp;
                        text_display /= 4;
                        xCoord -= 200;
                        xCoordLine += 200;
                    }
                    var x_LineText = new Konva.Text({
                        x: xCoord,
                        y: yCoord,
                        text: text_display,
                        fontSize: 20,
                        fontFamily: 'Calibri',
                        fill: 'black'
                    });
                    layer.add(x_LineText);

                    var x_Line = new Konva.Line({
                        points: [xCoordLine, 270, xCoordLine, 277],
                        stroke: 'black',
                        strokeWidth: 1,
                    });
                    layer.add(x_Line);
                }
            } else {
                var xCoord = 780, xCoordLine = 193, yCoord = 275, text_display = 800, temp = text_display;
                for (var i = 0; i < 4; i++) {
                    if (i == 0) {
                        text_display = text_display;
                        text_display = temp;
                        xCoord = xCoord;
                        xCoordLine = xCoordLine;
                    } else if (i == 1) {
                        text_display = (text_display + (text_display / 2)) / 2;
                        xCoord -= 200;
                        xCoordLine += 200
                    } else if (i == 2) {
                        text_display = temp;
                        text_display /= 2;
                        xCoord -= 200;
                        xCoordLine += 200
                    } else {
                        text_display = temp;
                        text_display /= 4;
                        xCoord -= 200;
                        xCoordLine += 200;
                    }
                    var x_LineText = new Konva.Text({
                        x: xCoord,
                        y: yCoord,
                        text: text_display,
                        fontSize: 20,
                        fontFamily: 'Calibri',
                        fill: 'black'
                    });
                    layer.add(x_LineText);
                    var x_Line = new Konva.Line({
                        points: [xCoordLine, 270, xCoordLine, 277],
                        stroke: 'black',
                        strokeWidth: 1,
                    });
                    layer.add(x_Line);
                }
            }
            // ToolTip on image
            var tooltip = new Konva.Label({
                x: 0,
                y: 0,
                opacity: 0.75,
            });
            tooltip.add(new Konva.Tag({
                fill: 'black',
                pointerDirection: 'down',
                pointerWidth: 10,
                pointerHeight: 10,
                lineJoin: 'round',
                shadowColor: 'black',
                shadowBlur: 10,
                shadowOffset: 10,
                shadowOpacity: 0.5
            }));
            var textElem = new Konva.Text({
                text: 'X : ' + 0 + '\n' + ' Y : ' + 0 + '\n' + draw._FloorMapTooltip,
                fontFamily: 'Calibri',
                width: 160,
                height: 130,
                // width: 150,
                // height: 100,
                fontSize: 18,
                padding: 5,
                fill: 'white'
            });

            tooltip.add(textElem);
            layer.add(tooltip);
            // On Click Floor Mapping
            $('#svgContainer').click(function (e) {
                var offset = $(this).offset();
                draw._xFloorCoord = (e.pageX - offset.left).toFixed(2);
                draw._yFloorCoord = (e.pageY - offset.top).toFixed(2);

                draw.xCoordVal.push(draw._xFloorCoord);
                draw.yCoordVal.push(draw._yFloorCoord);

                if ((draw._xFloorCoord >= 35 && draw._xFloorCoord <= 810) && (draw._yFloorCoord >= 0 && draw._yFloorCoord <= 271)) {
                    $('#FloorPlanSetting').modal('show');
                    var xCoordLast = draw.xCoordVal[draw.xCoordVal.length - 1];
                    var yCoordLast = draw.yCoordVal[draw.yCoordVal.length - 1];
                    var floorFlag = 0, strFloorTooltip, seatCheckFlag = 1, floorGrp = [], occupancyRate = 0;

                    // var strTooltip = 'X: ' + draw._xFloorCoord + '\n' + 'Y: ' + draw._yFloorCoord ;

                    var strTooltip = 'X: ' + draw._xFloorCoord + '\n' + 'Y: ' + draw._yFloorCoord + '\n' + draw._FloorMapTooltip;
                    if (draw.FloorMeasure.length != 0) {
                        $.each(draw.FloorMeasure, function (key, value) {
                            if (seatCheckFlag) {
                                draw._SeatName = value.dimension;
                                var xIncVal = value.xCoord - 13.5;
                                var xDecVal = parseFloat(value.xCoord) + parseFloat(13.5);
                                var yIncVal = value.yCoord - 10;
                                var yDecVal = parseFloat(value.yCoord) + parseFloat(10);
                                if ((xIncVal <= xCoordLast && xDecVal >= xCoordLast) && (yIncVal <= yCoordLast && yDecVal >= yCoordLast)) {
                                    floorGrp = floorGrpMappingArr[floorGrpMappingArr.length - 1];
                                    occupancyRate = floorGrp[value.dimension];
                                    floorFlag = 1;
                                    seatCheckFlag = 0;
                                    setTimeout(function () {
                                        $('#FloorPlanSetting').modal('hide');
                                    }, 500);
                                    $('#FloorPlanSetting').modal('hide');
                                    // strFloorTooltip = 'X: ' + draw._xFloorCoord + '\n' + 'Y: ' + draw._yFloorCoord + '\n' + 'Seat: ' + draw._SeatName + '\n' + 'Occupancy: ' + occupancyRate ;
                                    strFloorTooltip = 'X: ' + draw._xFloorCoord + '\n' + 'Y: ' + draw._yFloorCoord + '\n' + draw._FloorMapTooltip + 'Seat: ' + draw._SeatName + '\n' + 'Occupancy: ' + occupancyRate;
                                    textElem.setText(strFloorTooltip);
                                    return true;
                                }
                                // strFloorTooltip = 'X: ' + draw._xFloorCoord + '\n' + 'Y: ' + draw._yFloorCoord ;
                                strFloorTooltip = 'X: ' + draw._xFloorCoord + '\n' + 'Y: ' + draw._yFloorCoord + '\n' + draw._FloorMapTooltip;
                                textElem.setText(strFloorTooltip);
                            }
                        });
                    } else {
                        // strFloorTooltip = 'X: ' + draw._xFloorCoord + '\n' + 'Y: ' + draw._yFloorCoord;
                        strFloorTooltip = 'X: ' + draw._xFloorCoord + '\n' + 'Y: ' + draw._yFloorCoord + '\n' + draw._FloorMapTooltip;
                        textElem.setText(strFloorTooltip);
                        floorFlag = 0;
                    }
                    if (!seatCheckFlag) {
                        var fillColor = 'black';
                        if (occupancyRate == 0.5) {
                            fillColor = 'red';
                        } else if (occupancyRate == 1) {
                            fillColor = 'blue';
                        } else {
                            fillColor = 'black';
                        }
                        //                      var rect = new Konva.Rect({
                        //                          x: draw._xFloorCoord,
                        //                          y: draw._yFloorCoord,
                        //                          width: 25,
                        //                          height: 20,
                        //                          fill: fillColor,
                        //                          stroke: 'black',
                        //                          strokeWidth: 2,
                        //                      });
                        //                      layer.add(rect);
                        //                      stage.add(layer);
                        // Drag Rectangle
                        var rect = new Konva.Rect({
                            x: draw._xFloorCoord,
                            y: draw._yFloorCoord,
                            width: 25,
                            height: 20,
                            fill: fillColor,
                            stroke: 'black',
                            strokeWidth: 2,
                            draggable: true,
                        });
                        rect.on('mouseover', function () {
                            document.body.style.cursor = 'pointer';
                        });
                        rect.on('mouseout', function () {
                            document.body.style.cursor = 'default';
                        });
                        layer.add(rect);
                        stage.add(layer);
                    } else {
                        var rect = new Konva.Rect({
                            x: draw._xFloorCoord,
                            y: draw._yFloorCoord,
                            width: 25,
                            height: 20,
                            fill: '',
                            stroke: 'black',
                            strokeWidth: 2,
                        });
                        layer.add(rect);
                        stage.add(layer);
                    }

                    if (floorFlag) {
                        textElem.setText(strFloorTooltip);
                    } else {
                        textElem.setText(strTooltip);
                    }
                    tooltip.setX(draw._xFloorCoord);
                    tooltip.setY(draw._yFloorCoord);
                    stage.add(layer);
                } else {

                }
            });
            return true;
        }


        // Floor Plan Setting

        draw._floorPlanSettingDraw = function (floorPlanSettingObj) {
            var floorPlanObjMap = {};
            floorPlanObjMap['xCoord'] = draw._xFloorCoord;
            floorPlanObjMap['yCoord'] = draw._yFloorCoord;
            floorPlanObjMap['dimension'] = floorPlanSettingObj.dimension;

            draw.FloorMeasure.push(floorPlanObjMap);
            draw._floorPlanMapping = {};
            draw._floorPlanMapping = floorPlanObjMap;
        }






        // Building Comparison

        draw._buildingChartJs = function () {
            //              $('#Building-Comparison').modal({
            //                  backdrop: 'static',
            //                  keyboard: false
            //                 });
            $('#Building-Comparison').modal('show');
        }

        draw._buildingChartDraw = function (row, col) {
            $('#Building-Comparison').modal('hide');
            var generated_SVG = $("#chart-" + draw._container).append("<svg ></svg>");
            var x = 0, y = 30, room_data = {}, dataset = [];

            for (var i = 0; i < row; i++) {
                for (var j = 0; j < col; j++) {
                    room_data ["X"] = x;
                    room_data ["Y"] = y;
                    room_data ["width"] = 30;
                    room_data ["height"] = 30;
                    room_data ["color"] = "red";
                    dataset.push(Object.assign({}, room_data));
                    x = x + 30;
                }
                x = 0;
                y = y + 30;
            }

            var svgContainer = d3.select("#chart-" + draw._container + " svg")
                .attr("width", 800)
                .attr("height", 400);

            var rectangle = svgContainer.selectAll("rect")
                .data(dataset)
                .enter()
                .append("rect");

            var rectangleAttributes = rectangle.attr("x", function (d) {
                return d.X;
            })
                .attr("y", function (d) {
                    return d.Y;
                })
                .attr("width", function (d) {
                    return d.width;
                })
                .attr("height", function (d) {
                    return d.height;
                })
                .attr("stroke-width", 2)
                .attr("stroke", "black")
                .style("fill", function (d) {
                    if (d.X <= 90) {
                        return "red"
                    } else if (d.X >= 91 && d.X <= 180) {
                        return "orange"
                    } else if (d.X >= 181 && d.X <= 270) {
                        return "blue"
                    } else if (d.X >= 271 && d.X <= 330) {
                        return "yellow"
                    } else {
                        return "green"
                    }
                    ;
                })
                .append("svg:title").text(function (d) {
                    return "X : " + d.X + "\n" + "Y : " + d.Y
                });
            return true;

        };















// word cloud start
        draw._wordCloud = function () {
            var _chart = {};
            _chart._axisConfig = draw._axisConfig;
            _chart._container = draw._container;
            _chart.id = "chart-" + draw._container;
            _chart.metadataId = draw._axisConfig['queryObj'];

            _chart.render = function(){
                var rangeArr = [];
                var textArr = [];
                var metadataId = draw._axisConfig['queryObj'];
                $.post(draw.nodeApiUrl+"/drawWordCloud", {"axisConfig":_chart._axisConfig,"reportId":_chart.id,sessionId:draw.getAccessToken(),metadataId:metadataId}).done(function(data){
                    _chart._data={};
                    _chart._data.labels=[];
                    data.dummyMeasureForWordCloud.forEach(function(d){
                        _chart._data.labels.push(d.key);
                        rangeArr.push(d.value);
                    });
                    rangeArr.sort(function(a, b){return a - b});
                    var newMin = 21;
                    var newMax = 30;
                    var oldMin = rangeArr[0];
                    var oldMax = rangeArr[rangeArr.length-1];
                    data.dummyMeasureForWordCloud.forEach(function(d, index){
                        if(d.value != 0){
                            if(oldMin == 1 && oldMax == 1){
                                if(d.value == 1){
                                    var size = 20;
                                }else{
                                    var size = d.value;
                                }
                            }else{
                                if((oldMax - oldMin) != 0){
                                    var size = (newMin + (d.value - oldMin) * (newMax - newMin) / (oldMax - oldMin)).toFixed(0);
                                }else{
                                    var size = 20;
                                }
                            }

                            var tempObj = {};
                            tempObj['text'] = d.key;
                            tempObj['textIndex'] = index;
                            tempObj['totalOccurance'] = d.value;
                            tempObj['size'] = size;
                            textArr.push(tempObj);
                        }
                    });

                    var wcDivContainer = 'cloud-' + _chart._container;
                    var wcDivHtml = '<div id="' + wcDivContainer + '"></div>';

                    $("#chart-" + _chart._container).html("");
                    $("#chart-" + _chart._container).append(wcDivHtml);
                    setTimeout(function () {
                        var height = $('#chart-' + _chart._container).height();
                        var width = $('#chart-' + _chart._container).width();

                        if(height){
                            height = height.toFixed(0);
                        }else{
                            height =  300;
                        }
                        if(width){
                            width = width.toFixed(0);
                        }else{
                            width =  500;
                        }

                        var fill = d3.scale.category20();
                        d3.layout.cloud()
                            .size([width, height])
                            .words(textArr)
                            .rotate(function() {
                                return (~~(Math.random() * 6) - 3) * 30;
                            })
                            .font("Impact")
                            .fontSize(function(d) {
                                return d.size;
                            })
                            .on("end", drawSkillCloud)
                            .start();

                        function drawSkillCloud(words) {
                            var div = d3.select("body").append("div")
                                .attr("class", "tooltip")
                                .style("opacity", 0);

                            var wordSVG = d3.select("#" + wcDivContainer)
                                .append("svg")
                                .attr('id', 'wcSVG')
                                .attr("width", width)
                                .attr("height", height)
                                .append("g")
                                .attr("transform", "translate(" + ~~(width / 2) + "," + ~~(height / 2) + ")")
                                .selectAll("text")
                                .data(words)
                                .enter()
                                .append("text")
                                .style("font-size", function(d) {
                                    return d.size + "px";
                                })
                                .style("-webkit-touch-callout", "none")
                                .style("-webkit-user-select", "none")
                                .style("-khtml-user-select", "none")
                                .style("-moz-user-select", "none")
                                .style("-ms-user-select", "none")
                                .style("user-select", "none")
                                .style("cursor", "pointer")
                                .style("font-family", "Impact")
                                .style("fill", function(d, i) {
                                    return fill(i);
                                })
                                .attr('id', function(d){
                                    var textId = 'wcText-' + d.textIndex;
                                    return textId;
                                })
                                .attr('alt', function(d){
                                    return  d.text;
                                })
                                .attr("text-anchor", "middle")
                                .attr("transform", function(d) {
                                    return "translate(" + [d.x, d.y] + ")rotate(" + d.rotate + ")";
                                })
                                .on("mouseover", function(d) {
                                    var wcToolTipHtml = 'Count : ' + d.totalOccurance;
                                    div.transition()
                                        .duration(200)
                                        .style("opacity", .9);
                                    div .html(wcToolTipHtml)
                                        .style("font-size", "15px")
                                        .style("color", "white")
                                        .style("background-color", "black")
                                        .attr('height', 500)
                                        .attr('width', 500)
                                        .style("left", (d3.event.pageX) + "px")
                                        .style("top", (d3.event.pageY - 28) + "px");
                                })
                                .on('mouseout', function(d){
                                    div.transition()
                                        .duration(200)
                                        .style("opacity", 0);
                                })
                                .text(function(d) {
                                    return d.text;
                                });
                            // word Cloud cross filter start
                            wordSVG.on("click", function(d, index) {
                                eChartServer._metadataId = _chart.metadataId;
                                if (d !== "" && d !== null) {
                                    if (!_chart.filter) {
                                        _chart.filter = [];
                                    }
                                    var index = _chart.filter.indexOf(d.text);
                                    if (index === -1) {
                                        _chart.filter.push(d.text);
                                        var element = d3.select(this);
                                        element.style("fill","none");
                                        element.style("stroke","#000");
                                        element.style("stroke-linejoin","round");
                                    } else {
                                        _chart.filter.splice(index, 1);
                                        var element = d3.select(this);
                                        element.style("fill", function(d, i) {
                                            return fill(i);
                                        });
                                        element.style("stroke","none");
                                        element.style("stroke-linejoin","unset");
                                    }

                                    if (_chart.filter.length > 0) {
                                        var filterObj={
                                            filters:_chart.filter,
                                            reportId:_chart.id,
                                            reset:false
                                        }
                                        eChartServer.server.filter({id:_chart.id},filterObj,function(d) {
                                            eChartServer.redrawEchartChartsOnly(_chart.id);
                                        });
                                    } else {
                                        var filterObj={
                                            filters:_chart.filter,
                                            reset:true
                                        }
                                        _chart.filter=[];
                                        eChartServer.server.filterAll(function(d) {
                                            eChartServer.redrawEchartChartsOnly();
                                        },_chart.metadataId);
                                    }
                                }
                            });
// word Cloud cross filter end
                            draw.reportLoadedCountCheck(_chart);
                        }
                    },500);
                }).fail(function(response) {
                    draw.errorNotification("Wordcloud");
                });
                // var svg = document.getElementsByTagName("svg")[0];
                // var bbox = svg.getBBox();
                // var viewBox = [bbox.x, bbox.y, bbox.width, bbox.height].join(" ");
                // svg.setAttribute("viewBox", viewBox);

            }
            /*
              Chart registry exist with extended server then delete
             */
            draw.chartRegistry.otherRegistryCheck(_chart.id);
            draw.chartRegistry.register(_chart);
            draw.globalRegistry.register(_chart);
            draw.globalRegistry.syncLocalRegistries(_chart.id,"sketch");
            _chart.render();

        }
// word cloud end












// speedoMeter Start
        draw._speedoMeterChart = function(){
            var _chart = {};
            _chart._axisConfig = draw._axisConfig;
            _chart._container = draw._container;
            _chart.id = "chart-" + draw._container;
            eChartServer._metadataId = _chart.metadataId;

            var object;
            $.each(draw._measure, function (key, value) {
                object = value;
            });

            _chart.operator = draw._aggregate;
            _chart.yGrp = draw.createNumberDisplayGroup(object);
            $.each(draw._measure, function (key, value) {
                _chart.operator = draw._aggregate[value.reName];
            });
            _chart.opKey = _chart.operator.key;
            _chart.speedValue = 0;

            var measures = draw._measure;
            var containerId = "chart-" + draw._container;
            var metadataId = draw._axisConfig['queryObj'];
            var dimension=draw._dimension;
            measures['dimension']=dimension;
            _chart.render = function(){
                var speed_Promise = $.post(draw.nodeApiUrl+"/drawSpeedoMeter", {"axisConfig":measures,"sessionId":draw.getAccessToken(),"reportId":containerId,metadataId:metadataId})
                    .done(function(data){
                        _chart.data = data;
                        // draw.chartLoading_Hide();
                        if(_chart.opKey == 'runTotal'){
                            _chart.opKey = 'sumIndex';
                        }
                        if(_chart.opKey == 'countDistinct'){
                            _chart.opKey = 'count';
                        }
                        _chart.speedValue = data[_chart.opKey];

                        if(_chart.speedValue == undefined || _chart.speedValue == null || _chart.speedValue == 'null'){
                            _chart.speedValue = 0;
                        }
                    }).fail(function(response){
                        draw.errorNotification("Speedometer");
                    }).then(function(){
                        _chart.modVal = _chart.speedValue % 50;
                        _chart.tempVal = _chart.speedValue + 100 - _chart.modVal;
                        _chart.speedoMeterObject = _chart._axisConfig.speedoMeterSettingObject;

                        if(_chart.speedoMeterObject){
                            _chart.lowRange = parseFloat((_chart.speedoMeterObject.lowRange).toFixed(2));
                            _chart.midRange = parseFloat((_chart.speedoMeterObject.midRange).toFixed(2));
                            _chart.highRange = parseFloat((_chart.speedoMeterObject.highRange).toFixed(2));
                        }else {
                            _chart.lowRange = parseFloat((_chart.speedValue/3).toFixed(2));
                            _chart.midRange =  parseFloat(((_chart.speedValue/3)*2).toFixed(2));
                            _chart.highRange = parseFloat(_chart.tempVal);
                        }

                        if(_chart.opKey == 'count'){
                            _chart.label_1 = 0;
                        }else{
                            _chart.label_1 = parseInt(_chart.data.min);
                        }

                        // _chart.label_1 = 0;
                        _chart.label_3 = parseFloat(_chart.highRange/2);
                        _chart.label_2 = parseFloat(_chart.label_3/2);
                        _chart.label_4 = parseFloat(_chart.label_2 + _chart.label_3);
                        _chart.label_5 = parseFloat(_chart.highRange);

                        var gaugeCanvasContainer = 'gaugeCanvas-' + _chart._container;
                        var gaugeTextContainer = 'gaugeText-' + _chart._container;

                        var height = $('#chart-' + _chart._container).height();
                        var width = $('#chart-' + _chart._container).width();

                        var gaugeHeight = (height - 20).toFixed(0) + 'px !important;';
                        var gaugeWidth = (width - 0).toFixed(0) + 'px !important;';

                        var gaugeStyle = 'height: ' + gaugeHeight + 'width: ' + gaugeWidth + 'text-align: center;';
                        var gaugeHtml = '<div><span id="' + gaugeTextContainer + '" style="color:red; font-weight: bold; font-size:80%; display: block; text-align: center;"></span><canvas id="' + gaugeCanvasContainer + '" style="' + gaugeStyle + '"></canvas></div>';

                        $('#chart-' + _chart._container).html("");
                        $('#chart-' + _chart._container).append(gaugeHtml);

                        var opts = {
                            angle: -0.4,
                            lineWidth: 0.1,
                            limitMax: 'true',
                            strokeColor: 'red',
                            radiusScale: 1,
                            generateGradient: true,
                            pointer: {
                                length: 0.5,
                                strokeWidth: 0.03,
                                color: 'black'
                            },
                            staticLabels: {
                                // font: "80% sans-serif",
                                font: "60% sans-serif",
                                labels: [_chart.label_1, _chart.label_2, _chart.label_3, _chart.label_4, _chart.label_5],
                                color: "black",
                                fractionDigits: 2
                            },
                            strokeColor: 'red',
                            staticZones: [
                                {strokeStyle: "#e92213", min: _chart.label_1, max: _chart.lowRange},
                                {strokeStyle: "#FFFE35", min: _chart.lowRange, max: _chart.midRange},
                                {strokeStyle: "#3EE91A", min: _chart.midRange, max: _chart.highRange}
                            ],
                            generateGradient: true
                        };

                        var target = document.getElementById(gaugeCanvasContainer);
                        var gauge = new Gauge(target).setOptions(opts);
                        gauge.minValue = _chart.label_1;
                        gauge.maxValue = _chart.highRange;
                        gauge.animationSpeed = 1;
                        // gauge.setTextField(document.getElementById(gaugeTextContainer));
                        $('#' + gaugeTextContainer).append(_chart.speedValue.toFixed(2));
                        gauge.set(_chart.speedValue);

                        // setTimeout(function(){
                        //     // gauge.textField.el.innerHTML = _chart.speedValue.toFixed(2);
                        //     gauge.textField.el.innerText = _chart.speedValue.toFixed(2);
                        // }, 2000);

                        draw.reportLoadedCountCheck(_chart);
                    });
            }
            _chart.render();
            draw.chartRegistry.otherRegistryCheck(_chart.id);
            draw.chartRegistry.register(_chart);
            draw.globalRegistry.register(_chart);
            draw.globalRegistry.syncLocalRegistries(_chart.id,"sketch");
        }
// speedoMeter End
















// Gender Comparison Start
        draw._populateMaleFemaleChart = function (genderData, metadataId) {
            var dim_gender = JSON.parse(genderData.dim_gender);
            var mea_gender = JSON.parse(genderData.mea_gender);
            var mea_arr;
            var containerId = "chart-" + draw._container;

            if(draw._axisConfig == undefined){
                var metadataId = metadataId;
            }else{
                var metadataId = draw._axisConfig['queryObj'];
            }

            var maleFeamlePopulatePromise = $.post(draw.nodeApiUrl + "/populateMaleFemaleOptions", {"dimensionObject":dim_gender, "sessionId":draw.getAccessToken(), metadataId:metadataId});
            return maleFeamlePopulatePromise;
        }

        draw._maleFemaleChartJs = function () {

        };

        draw._maleFemaleChartDraw = function () {
            var _chart = {};
            _chart._axisConfig = draw._axisConfig;
            _chart._container = draw._container;
            _chart.id = "chart-" + draw._container;
            eChartServer._metadataId = _chart.metadataId;

            _chart.render = function () {
                var dim_gender = _chart._axisConfig.genderObject.dim_gender;
                var mea_gender = _chart._axisConfig.genderObject.mea_gender;

                dim_gender = JSON.parse(dim_gender);
                mea_gender = JSON.parse(mea_gender);

                var axisConfig = draw._axisConfig;
                var containerId = "chart-" +  _chart._container;
                var metadataId = _chart._axisConfig['queryObj'];

                $.post(draw.nodeApiUrl + "/maleFemaleChartDraw", {"measureObject":mea_gender,"dimensionObject":dim_gender,"sessionId":draw.getAccessToken(),"reportId":containerId,metadataId:metadataId})
                    .done(function(data){
                        try {
                            var male = _chart._axisConfig.genderObject.male;
                            var female = _chart._axisConfig.genderObject.female;
                            var totalForMale = 0, totalForFemale = 0;
                            Object.values(data)[0].forEach(function(d){
                                if (male == d.key) {
                                    // totalForMale = d.value;
                                    if(d.value.sum){
                                        totalForMale = Object.values(d.value.sum)[0];
                                    }else{
                                        totalForMale = d.value;
                                    }
                                }
                                if (female == d.key) {
                                    // totalForFemale = d.value;
                                    if(d.value.sum){
                                        totalForFemale = Object.values(d.value.sum)[0];
                                    }else{
                                        totalForFemale = d.value;
                                    }
                                }
                            });

                            var totalMaleFemale = totalForFemale + totalForMale;
                            if(isNaN(totalMaleFemale) || totalMaleFemale === 0) {
                                totalMaleFemale = 1;
                            }
                            var maleTotalPercent = (totalForMale / (totalMaleFemale)) * 100;
                            var femaleTotalPercent = (totalForFemale / (totalMaleFemale)) * 100;
                            var height = ($('#'+containerId).height()*70)/100;
                            var width = ($('#'+containerId).width()*40)/100;
                            // var fill_male_value = parseInt(maleTotalPercent);
                            // var fill_female_value = parseInt(femaleTotalPercent);
                            var fill_male_value = maleTotalPercent.toFixed(2);
                            var fill_female_value = femaleTotalPercent.toFixed(2);
                            var fill_type = "fill";
                            var fill_color = "blue";
                            var fill_direction = "btt";
                            var male_img_path = "assets/img/demo/male.svg";
                            var female_img_path = "assets/img/demo/female.svg";
                            var fill_Class_Male = "ldBar";
                            var fill_Class_Female = "ldBar";
                            var left_class = 'float:right;height:'+height+'px;width:'+width+'px;';
                            var right_class = 'height:'+height+'px;width:'+width+'px;';
                            var maleDiv_Id = 'Id_male-' + _chart._container;
                            var femaleDiv_Id = 'Id_female-' + _chart._container;
                            var genderDiv_Id = 'Id_gender-' + _chart._container;
                            if(/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
                                var left_class = 'left : 25% !important; height:300px; width: 300px; font-size: 2.5em; ';
                                var right_class = 'right : -15% !important; height:300px; width:300px; font-size: 2.5em; ';
                            }

                            var male_Div = "<div class='col-sm-6'><div style='" + left_class + "' data-preset='energy' id='" + maleDiv_Id + "' data-type='" + fill_type + "' data-value='" + fill_male_value + "' data-img='" + male_img_path + "' class='" + fill_Class_Male + "  male-female' data-fill='" + fill_color + "' data-fill-dir='" + fill_direction + "'></div></div>";
                            var female_Div = "<div class='col-sm-6'><div style='" + right_class + "' data-preset='energy' id='" + femaleDiv_Id + "' data-type='" + fill_type + "' data-value='" + fill_female_value + "' data-img='" + female_img_path + "' class='" + fill_Class_Female + " col-sm-6 male-female' data-fill='" + fill_color + "' data-fill-dir='" + fill_direction + "'></div></div>";
                            var gender_Div = "<div id='" + genderDiv_Id + "' style='width:100%;height: 100%;'>" + male_Div + female_Div + "</div>";
                            $("#chart-" + _chart._container).html("");
                            $('#chart-' + _chart._container).append(gender_Div);
                            var male_new = new ldBar("#Id_male-" + _chart._container, {

                            });
                            var female_new = new ldBar("#Id_female-" + _chart._container, {

                            });

// male cross filter start
                            $('div#' + maleDiv_Id).click(function(d){
                                if (d !== "" && d !== null) {
                                    if (!_chart.filter) {
                                        _chart.filter = [];
                                    }
                                    var index = _chart.filter.indexOf(male);
                                    if (index === -1) {
                                        _chart.filter.push(male);
                                    } else {
                                        _chart.filter.splice(index, 1);
                                    }
                                    if (_chart.filter.length > 0) {
                                        var filterObj={
                                            filters:_chart.filter,
                                            reportId:_chart.id,
                                            reset:false
                                        }
                                        eChartServer.server.filter({id:_chart.id},filterObj,function(d) {
                                            eChartServer.redrawEchartChartsOnly(_chart.id);
                                        });
                                    } else {
                                        var filterObj={
                                            filters:_chart.filter,
                                            reset:true
                                        }
                                        _chart.filter=[];
                                        eChartServer.server.filterAll(function(d) {
                                            eChartServer.redrawEchartChartsOnly();
                                        },_chart.metadataId);
                                    }
                                }
                            });
// male cross filter end

// female cross filter start
                            $('div#' + femaleDiv_Id).click(function(d){
                                if (d !== "" && d !== null) {
                                    if (!_chart.filter) {
                                        _chart.filter = [];
                                    }
                                    var index = _chart.filter.indexOf(female);
                                    if (index === -1) {
                                        _chart.filter.push(female);
                                    } else {
                                        _chart.filter.splice(index, 1);
                                    }
                                    if (_chart.filter.length > 0) {
                                        var filterObj={
                                            filters:_chart.filter,
                                            reportId:_chart.id,
                                            reset:false
                                        }
                                        eChartServer.server.filter({id:_chart.id},filterObj,function(d) {
                                            eChartServer.redrawEchartChartsOnly(_chart.id);
                                        });
                                    } else {
                                        var filterObj={
                                            filters:_chart.filter,
                                            reset:true
                                        }
                                        _chart.filter=[];
                                        eChartServer.server.filterAll(function(d) {
                                            eChartServer.redrawEchartChartsOnly();
                                        },_chart.metadataId);
                                    }
                                }
                            });
// female cross filter end
                            return true;
                        } catch (e) {

                        }
                        draw.reportLoadedCountCheck(_chart);
                    }).fail(function(response) {
                    draw.errorNotification("Male Female");
                });
            };
            draw.chartRegistry.otherRegistryCheck(_chart.id);
            draw.chartRegistry.register(_chart);
            draw.globalRegistry.register(_chart);
            draw.globalRegistry.syncLocalRegistries(_chart.id,"sketch");
            _chart.render();

        };
// Gender Comparison End















// Map Draw Start
        draw._mapChartJs = function () {

        };

        draw._createCoordinate = function(coordData){
            var selectedId = coordData.split('_-_')[0];
            var chartId = coordData.split('_-_')[1];
            var _chart = {};
            _chart = draw.chartRegistry.get(chartId);

            var mapObj = _chart._axisConfig.mapObject;

            var derivedRadius = 1, multiplicationFactor = 10, baseRadius = null;
            _chart.radius = {};
            var setMeasureForRadius = JSON.parse(mapObj.measure_map[selectedId]);
// set min-max range for radius start
            var radiusArr = [], newRadius;

            _chart.Map_LatLng_Arr.forEach(function(d,i){
                radiusArr.push(d.value);
            });
            radiusArr.sort(function(a, b){return a - b});
            var newMin = 11;
            var newMax = 20;
            var oldMin = radiusArr[0];
            var oldMax = radiusArr[radiusArr.length-1];
// set min-max range for radius end
            _chart.Map_LatLng_Arr.forEach(function(d,i){
                if (!baseRadius && d.value !== 0) {
                    baseRadius = d.value;
                }
                derivedRadius = (d.value / baseRadius).toFixed(2);
                if (isNaN(derivedRadius)) {
                    derivedRadius = 0;
                }
                if(derivedRadius!=0){
                    newRadius = (newMin + (d.value - oldMin) * (newMax - newMin) / (oldMax - oldMin)).toFixed(2);
                    _chart.radius[d.key] = newRadius;
                }
            });

            var set_Measure_ForMap = JSON.parse(mapObj.measure_map[selectedId]);
            if(mapObj.multiGroupColor_map != undefined && mapObj.multiGroupColor_map[selectedId] != undefined && !$.isEmptyObject(mapObj.multiGroupColor_map)){
                var set_Dimension_ForMap = JSON.parse(mapObj.multiGroupColor_map[selectedId]);
            }else{
                var set_Dimension_ForMap = '';
            }

            var set_Tooltip = mapObj.multipleTooltip[selectedId];
            var set_Tooltip_Arr = mapObj.multipleTooltipObj[selectedId];

            _chart.radius_Data = [];
            _chart.groupMappings = {};
            _chart._toolTipStr = [];

            $.post(draw.nodeApiUrl + "/drawMap_Radius", {"measureObj":set_Measure_ForMap,"dimensionObj":set_Dimension_ForMap,"tooltip":set_Tooltip, "tooltipArr":set_Tooltip_Arr, "sessionId":draw.getAccessToken(), "reportId":_chart.id, metadataId:_chart.metadataId})
                .done(function(data){
                    if(_chart.mapExcludeKey.length){
                        _chart.mapExcludeKey.forEach(function (dd) {
                            data.forEach(function (d,i) {
                                if(d.key == dd){
                                    data.splice(i,1);
                                }
                            });
                        });
                    }
                    _chart.radius_Data = data;
                    _chart._data={};
                    _chart._data.labels=[];
                    _chart.radius_Data.forEach(function (d) {
                        var tempTex="";
                        if(Object.keys(d.value.groupColor).length){
                            Object.keys(d.value.groupColor).forEach(function (p) {
                                _chart._data.labels.push(p+"{{"+d.key+"}}");
                            });
                        }else{
                            _chart._data.labels.push(d.key);
                        }
                    })
                }).fail(function(response) {
                draw.errorNotification("Map");
            }).then(function(){
// map groupColor start
                _chart.radius_Data.forEach(function(d,i){
                    if(d.value.groupColor !== undefined && d.value.groupColor !=={}){
                        $.each(d.value, function(k, v){
                            if (_chart.groupMappings[k]) {
                                _chart.groupMappings[k][d.key] = v;
                            } else {
                                _chart.groupMappings[k] = {};
                                _chart.groupMappings[k][d.key] = v;
                                // _chart.groupMappings[k][d.key] = {radius: v};
                            }
                        });
                    }
                });
// map groupColor end

// map tooltip start
                if(mapObj.multipleTooltipObj[selectedId]){
                    var latLng_tooltipArr = [];
                    var multiple_Tooltip_Obj = mapObj.multipleTooltipObj[selectedId];
                    var tempTooltipArr = [], newTooltipArr = [];
                    var map_tooltip_Arr = [];
                    map_tooltip_Arr = set_Tooltip_Arr;
                    var temp_Tooltip_Text = mapObj.multipleTooltip[0];
                    var tooltip_key = [], tempStr;

                    _chart.radius_Data.forEach(function (d) {
                        if(d.value.sumIndex != 0 && d.value.sumIndex != undefined && d.value.sumIndex != ''){
                            mapObj.multipleTooltip.forEach(function (a, index) {
                                if(selectedId == index){
                                    temp_Tooltip_Text = map_tooltip_Arr;
                                    var tooltip_key = [];
                                    tempStr = a;
                                    $.each(d.value,function(key,val){
                                        if(draw.checkFloat(val)){
                                            val = val.toFixed(2);
                                        }
                                        if(val != null){
                                            tempStr = tempStr.replace(key,val);
                                        }
                                    });
                                }
                            });
                            tempStr = tempStr.replace(/\n/g, "<br>");
                            _chart._toolTipStr.push(tempStr);
                            var tempObj = {};
                            tempObj['LatLng'] = d.key;
                            tempObj['tooltip'] = tempStr;
                            latLng_tooltipArr.push(tempObj);
                        }
                    });
                }
                _chart._createMarkerAndTooltip(_chart.radius, _chart._toolTipStr, latLng_tooltipArr);
                L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
                    maxZoom: 18,
                }).addTo(_chart.map);
// map tooltip end
            });
        }

        draw._mapChartDraw = function () {
            var _chart = {};
            _chart._axisConfig = draw._axisConfig;

            _chart._container = draw._container;
            _chart.id = "chart-" + draw._container;
            _chart.metadataId = draw._axisConfig['queryObj'];
            eChartServer._metadataId = _chart.metadataId;
            _chart.mapObj = _chart._axisConfig.mapObject;
            _chart.mapExcludeKey = [];

            if(_chart._axisConfig.excludeKey != undefined){
                _chart._axisConfig.excludeKey.forEach(function (d) {
                    var str;
                    if(d.includes('{{') &&  d.includes('}}')){
                        str = d.split('{{')[1].split('}}')[0];
                    }else{
                        str = d;
                    }
                    _chart.mapExcludeKey.push(str);
                });
            }

            _chart.AddMap = function(){
                var set_map_Id = "mapbox-" + _chart._container;
                $("#chart-" + _chart._container).html("");
                var calculatedHeight = $("#chart-" + _chart._container).height() - 15;
                var height = calculatedHeight + 'px;';
                var calculatedWidth = $("#chart-" + _chart._container).width();
                var width = calculatedWidth + 'px;';
                var mapHtml = "<div style='width:" + width + " height: " + height + "' id='mapbox-" + _chart._container + "'> </div> ";

                _chart.generated_Div_Map = $("#chart-" + _chart._container).append(mapHtml);
                L.mapbox.accessToken = 'pk.eyJ1IjoiZG9zcyIsImEiOiI1NFItUWs4In0.-9qpbOfE3jC68WDUQA1Akg';
                _chart.map = L.mapbox.map(set_map_Id, 'mapbox.streets').setView(_chart.Map_Center_Arr, 5);
                _chart.myLayer = L.mapbox.featureLayer().addTo(_chart.map);

                $("#measure_" + _chart._container).html(_chart._setRadioButtonList());

                var coordData = 0 + '_-_' + _chart.id;
                draw._createCoordinate(coordData);

                _chart.map._initPathRoot();
                $('.leaflet-control-attribution').hide();
                $('.mapbox-logo').hide();    // mapbox-logo leaflet-control mapbox-logo-true

                var tiles = L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
                    maxZoom: 18,
                }).addTo(_chart.map);
            }
// set Coordinates


// Setting Circles
            _chart._createMarkerAndTooltip = function(radius, toolTip, latLng_tooltipArr){
                var setCircleLayer = [], setCircleMarkerLayer = [];
                _chart.map.eachLayer(function (setCircleLayer) {
                    _chart.map.removeLayer(setCircleLayer);
                });

                var radius_Group_LatLng_Arr = [];
                var Group_Color_Arr = [];
                var temp_Color_Arr = [];
                var mapTooltipCounter = 0;

                $.each(_chart.groupMappings.groupColor, function(k, v){
                    if(!temp_Color_Arr.includes(Object.keys(v)[0])){
                        var tempObj = {};
                        tempObj['groupColor'] = Object.keys(v)[0];
                        tempObj['color'] = draw._MapColors[mapTooltipCounter % (draw._MapColors.length - 1)];
                        temp_Color_Arr.push(Object.keys(v)[0]);
                        Group_Color_Arr.push(tempObj);
                        mapTooltipCounter++;
                    }
                    var temp_Obj = {};
                    temp_Obj['latlng'] = k;
                    temp_Obj['groupColor'] = Object.keys(v)[0];
                    Group_Color_Arr.forEach(function(d, i){
                        if(d.groupColor == Object.keys(v)[0]){
                            temp_Obj['color'] = d.color;
                        }
                    });

                    $.each(radius, function(key, val){
                        if(key == k){
                            temp_Obj['value'] = val;
                        }
                    });
                    if(latLng_tooltipArr){
                        latLng_tooltipArr.forEach(function(d, i){
                            if(k == d.LatLng){
                                temp_Obj['tooltip'] = d.tooltip;
                            }
                        });
                    }
                    radius_Group_LatLng_Arr.push(temp_Obj);
                });

                radius_Group_LatLng_Arr.forEach(function (d, index) {
                    if(d.value != undefined){
                        var lat = d.latlng.split(",")[0];
                        var lng = d.latlng.split(",")[1];
                        var newLatLng = [];
                        newLatLng.push(lat,lng);
                        L.Circle.prototype._checkIfEmpty = function () { return false; }; // circle visible on zooming
                        setCircleLayer.push(L.circleMarker(newLatLng, { color: d.color, radius: d.value }).addTo(_chart.map));
                        var circleMarker = L.marker(newLatLng).addTo(_chart.map);
                        L.Icon.Default.imagePath = "assets/img";
                        // circleMarker = L.marker(d).bindPopup(toolTip[index]).addTo(_chart.map);
                        setCircleMarkerLayer.push(circleMarker);
                        if(toolTip.length){
                            var popup;
                            circleMarker.on('mouseover', function(e) {
                                popup = L.popup().setLatLng(newLatLng).setContent(d.tooltip).openOn(_chart.map);
                                $('.leaflet-popup').css('opacity', '1');
                            });
                            circleMarker.on('mouseout', function(e) {
                                $('.leaflet-popup').css('opacity', '0');
                            });
                        }else{
                            circleMarker.on('mouseover', function(e) {
                                var tooltipData = 'Latitude : ' + e.latlng.lat.toFixed(2) + '<br>' + 'Longitude : ' + e.latlng.lng.toFixed(2);
                                var popup = L.popup().setLatLng(newLatLng).setContent(tooltipData).openOn(_chart.map);
                                $('.leaflet-popup').css('opacity', '1');
                            });
                            circleMarker.on('mouseout', function(e) {
                                $('.leaflet-popup').css('opacity', '0');
                            });
                        }
// Map CrossFilters Start
                        //_chart.generated_Div_Map.on('click', function(d){
                        //eChartServer._metadataId = _chart.metadataId;
                        circleMarker.on('click', function (e) {
                            var latitude = e.latlng.lat;
                            var longitude = e.latlng.lng;
                            if (!_chart.filter) {
                                _chart.filter = [];
                            }
                            var index = _chart.filter.indexOf(latitude.toFixed(6) + "," + longitude.toFixed(6));
                            if (index === -1) {
                                _chart.filter.push(latitude.toFixed(6) + "," + longitude.toFixed(6));
                            } else {
                                _chart.filter.splice(index, 1);
                            }
                            if (_chart.filter.length > 0) {
                                var filterObj={
                                    filters:_chart.filter,
                                    reportId:_chart.id,
                                    reset:false
                                }
                                eChartServer.server.filter({id:_chart.id},filterObj,function(d) {
                                    eChartServer.redrawEchartChartsOnly(_chart.id);
                                });
                            } else {
                                var filterObj={
                                    filters:_chart.filter,
                                    reset:true
                                }
                                _chart.filter=[];
                                eChartServer.server.filterAll(function(d) {
                                    eChartServer.redrawEchartChartsOnly();
                                },_chart.metadataId);
                            }
                        });
                        // });
// Map CrossFilters End
                    }
                });
            }

//  Radio Button
            _chart._setRadioButtonList = function(){
                var mapObj = _chart._axisConfig.mapObject;
                var radius_name = [], html = [], j = 1, setRadio = '';
                for (var i = 0; i < Object.keys(mapObj.measure_map).length; i++){
                    var temp = JSON.parse(mapObj.measure_map[i]);
                    radius_name.push(temp.reName);
                }
                for (var i = 0; i < radius_name.length; i++) {
                    if(i == 0) {
                        setRadio = 'checked ';
                    } else {
                        setRadio = '';
                    }
                    var radName = radius_name[i].slice(0, 25) + (radius_name[i].length > 25 ? "..." : "");
                    var coordData = i + '_-_' + _chart.id;
                    var radio_Html = '<li><input style="cursor: pointer;" type="radio" ' + setRadio + 'name="measure_radio" title="' + radius_name[i] + '" id="map_measure_radio_' + i + '" onclick="sketchServer._createCoordinate(\'' + coordData + '\')" > ' + radName + ' </li>';
                    html.push(radio_Html);
                    j++;
                }
                return html;
            }





// map Function calls start
            _chart.render = function(){
                var Center_Response = $.post(draw.nodeApiUrl + "/drawMap_Center", { "mapConfig":_chart._axisConfig.mapObject, "sessionId":draw.getAccessToken(), "reportId":_chart.id, metadataId:_chart.metadataId}).done(function(data){
                    _chart.Map_Center_Arr = data.center;
                });

                var LatLng_Response = $.post(draw.nodeApiUrl + "/drawMap_LatLng", {"measureObject":_chart.mapObj.measure_map[0], "mapConfig":_chart._axisConfig.mapObject, "sessionId":draw.getAccessToken(), "reportId":_chart.id, metadataId:_chart.metadataId}).done(function(data){
                    if(_chart.mapExcludeKey.length){
                        _chart.mapExcludeKey.forEach(function (dd) {
                            data.location.forEach(function (d,i) {
                                if(d.key == dd){
                                    data.location.splice(i,1);
                                }
                            });
                        });
                    }
                    _chart.Map_LatLng_Arr = data.location;
                });

                var center_LatLngCreation = new Promise(function(resolve,reject){
                    var flag=0;
                    Center_Response.then(function(){
                        flag++;
                        if(flag==2){
                            resolve();
                        }
                    });
                    LatLng_Response.then(function(){
                        flag++;
                        if(flag==2){
                            resolve();
                        }
                    });
                });
                center_LatLngCreation.then(function(){
                    _chart.AddMap();
                    draw.reportLoadedCountCheck(_chart);
                });
            }
            draw.chartRegistry.otherRegistryCheck(_chart.id);
            draw.chartRegistry.register(_chart);
            draw.globalRegistry.register(_chart);
            draw.globalRegistry.syncLocalRegistries(_chart.id,"sketch");

            _chart.render();
            return _chart;

// map Function calls end
        }
// Map Draw End



















// Text/Image Chart Start
        draw._TextImageChart = function () {
            var _chart = {};
            _chart._axisConfig = draw._axisConfig;
            _chart._container = draw._container;
            _chart.id = "chart-" + draw._container;
            _chart.metadataId = draw._axisConfig['queryObj'];
            eChartServer._metadataId = _chart.metadataId;

            _chart.textObj = _chart._axisConfig.TextImageObject;
            _chart.textMeasure = [];
            _chart.textDimension = [];

            _chart.textConfig = {};
            _chart.textConfig.String = _chart._axisConfig.TextImageObject.textString;
            _chart.textConfig._measure = {};
            _chart.textConfig._dimension = {};
            _chart.textObj.measure.forEach(function (d) {
                _chart.textConfig._measure[d.columnName] = d;
            });

            _chart.textObj.dimension.forEach(function (d) {
                _chart.textConfig._dimension[d.columnName] = d;
            });

            String.prototype.replaceAll = function(a, b) {
                return this.replace(new RegExp(a.replace(/([.?*+^$[\]\\(){}|-])/ig, "\\$1"), 'ig'), b)
            }

            var tempArr = _chart.textObj.measure.concat(_chart.textObj.dimension);
            tempArr.forEach(function(d){
                var findStr = '{{' + d.reName + ',';
                var replaceStr = '{{' + d.columnName + ',';
                if(_chart.textConfig.String.includes(findStr)){
                    _chart.textConfig.String = _chart.textConfig.String.replaceAll(findStr, replaceStr);
                }
            });

            _chart.drawText = function(){
                $("#chart-" + _chart._container).html("");
                $("#chart-" + _chart._container).html(_chart.TextData);
            }

            _chart.render = function(){
                var Text_Response = $.post(draw.nodeApiUrl + "/drawTextImage", {"sessionId":draw.getAccessToken(), "reportId":_chart.id, "metadataId":_chart.metadataId, "config":_chart.textConfig}).done(function(data){
                    _chart.TextData = data.data;
                });
                var TextCreation = new Promise(function(resolve,reject){
                    var flag=0;
                    Text_Response.then(function(){
                        flag++;
                        if(flag==1){
                            resolve();
                        }
                    });
                });
                TextCreation.then(function(){
                    _chart.drawText();
                    draw.reportLoadedCountCheck(_chart);
                });
            }
            draw.chartRegistry.otherRegistryCheck(_chart.id);
            draw.chartRegistry.register(_chart);
            draw.globalRegistry.register(_chart);
            draw.globalRegistry.syncLocalRegistries(_chart.id,"sketch");

            _chart.render();
            return _chart;
        }
// Text/Image Chart End























// customized PivotTable Start

        draw._PivotCustomized = function(){

        }

        draw.pivotCust_Draw = function(){
            var _chart = {};
            _chart._axisConfig = draw._axisConfig;

            _chart.ptCustMeasure = Object.values(_chart._axisConfig.checkboxModelMeasure);
            _chart.ptCustDimension = Object.values(_chart._axisConfig.checkboxModelDimension);
            _chart.ptCust_MeasureDimension_Arr = _chart.ptCustMeasure.concat(_chart.ptCustDimension);

            var measureObj = Object.assign({}, _chart._axisConfig.checkboxModelMeasure);
            var dimensionObj = Object.assign({}, _chart._axisConfig.checkboxModelDimension);
            _chart.ptCust_meaDimObj = Object.assign(measureObj, dimensionObj);

            _chart._container = draw._container;
            _chart.id = "chart-" + draw._container;
            _chart.ptCust_Dimension_Cross_Filter = {};
            _chart._dimension = draw._dimension;

            _chart.dummyDimObj = draw._dimension[Object.keys(draw._dimension)[0]];
            _chart.metadataId = draw._axisConfig['queryObj'];


            var ptCust_Object = {};
            ptCust_Object['container_ID'] = _chart._container;
            ptCust_Object['config'] = _chart._axisConfig;
            draw._pivotCust_Arr.push(ptCust_Object);

// Pivot Object start
            _chart.pivotCust_Obj = _chart._axisConfig.pivotCust_Object;
            _chart.pivotCust_Selected_Column = [];
            _chart.pivotCust_Selected_Row = [];
            _chart.pivotCust_Selected_Data = [];

            _chart.pivotCust_Obj.Column.forEach(function(d){
                _chart.ptCust_MeasureDimension_Arr.forEach(function(data){
                    if(d == data.reName){
                        _chart.pivotCust_Selected_Column.push(data.columnName);
                    }
                });
            });
            _chart.pivotCust_Obj.Row.forEach(function(d){
                _chart.ptCust_MeasureDimension_Arr.forEach(function(data){
                    if(d == data.reName){
                        _chart.pivotCust_Selected_Row.push(data.columnName);
                    }
                });
            });
            _chart.pivotCust_Obj.Data.forEach(function(d){
                _chart.ptCust_MeasureDimension_Arr.forEach(function(data){
                    if(d == data.reName){
                        _chart.pivotCust_Selected_Data.push(data.columnName);
                    }
                });
            });

            if(_chart._axisConfig.pivotCust_DecimalObject && _chart._axisConfig.pivotCust_DecimalObject.data){
                _chart.pt_DecimalObj = {};
                $.each(_chart._axisConfig.checkboxModelMeasure, function(k,v){
                    if(_chart._axisConfig.pivotCust_DecimalObject.data[k] != undefined){
                        _chart.pt_DecimalObj[v.columnName] = _chart._axisConfig.pivotCust_DecimalObject.data[k];
                    }
                });
            }
// Pivot Object end


// Pivot Sorting Object start
            var row_Sort_Obj = {};
            _chart.pivotCust_Selected_Row.forEach(function(d){
                if(row_Sort_Obj[d] == undefined){
                    row_Sort_Obj[d] = 'asc';
                }
            });
            var col_Sort_Obj = {};
            _chart.pivotCust_Selected_Column.forEach(function(d){
                if(col_Sort_Obj[d] == undefined){
                    col_Sort_Obj[d] = 'asc';
                }
            });
            _chart.pivotCust_Sort_Object = {};
            _chart.pivotCust_Sort_Object['Column'] = col_Sort_Obj;
            _chart.pivotCust_Sort_Object['Row'] = row_Sort_Obj;
// Pivot Sorting Object end


// Data_Type Object start
            _chart.pivotCust_Data_Object = _chart._axisConfig.pivotCust_DataObject;

            if(_chart.pivotCust_Data_Object != undefined){
                var tempRenameArr = [];
                var ptDataTypeObj = {};
                _chart.pivotCust_Selected_Data.forEach(function(d){
                    _chart.ptCust_MeasureDimension_Arr.forEach(function(data){
                        if(d == data.columnName){
                            var tempObj = {};
                            tempObj[data.reName] = data.columnName;
                            tempRenameArr.push(tempObj);
                        }
                    });
                });

                tempRenameArr.forEach(function(data){
                    $.each(data, function(k, v){
                        $.each(_chart.pivotCust_Data_Object['data'], function(kk, vv){
                            if(v == kk){
                                ptDataTypeObj[v] = vv;
                            }
                            // if(k == kk){
                            //     ptDataTypeObj[v] = vv;
                            // }
                            if(_chart.pivotCust_Data_Object['data'][v] == undefined){
                                ptDataTypeObj[v] = 'sum';
                            }
                        });
                    });
                });
                _chart.pivotCust_Selected_Data_Type = ptDataTypeObj;
            }else{
                var temp = {};
                _chart.pivotCust_Selected_Data.forEach(function(d){
                    temp[d] = 'sum';
                });
                _chart.pivotCust_Selected_Data_Type = temp;
            }


// Data_Type Object end


// Grand Total Object start
            _chart.pivotCust_GrandTotal_Object = _chart._axisConfig.pivotCust_GrandTotalObject;
            if(_chart.pivotCust_GrandTotal_Object != undefined){
                _chart.grandTotal_Row = _chart.pivotCust_GrandTotal_Object.grandTotal.row;
                _chart.grandTotal_Column = _chart.pivotCust_GrandTotal_Object.grandTotal.column;
            }else{
                _chart.grandTotal_Row = 'no';
                _chart.grandTotal_Column = 'no';
            }
// Grand Total Object end


// Sub Total Object start
            _chart.pivotCust_SubTotal_Object = _chart._axisConfig.pivotCust_SubTotalObject;
            if(_chart.pivotCust_SubTotal_Object != undefined){
                _chart.subTotal_Row = _chart.pivotCust_SubTotal_Object.subTotal.row;
                _chart.subTotal_Column = _chart.pivotCust_SubTotal_Object.subTotal.column;
            }else{
                _chart.subTotal_Row = 'no';
                _chart.subTotal_Column = 'no';
            }
// Sub Total Object end

// Running Total Object start
            _chart.pivotCust_RunningTotal_Object = _chart._axisConfig.pivotCust_RunningTotalObject;
            if(!$.isEmptyObject(_chart.pivotCust_RunningTotal_Object) && _chart.pivotCust_RunningTotal_Object != undefined){
                _chart.runningTotal_Row = _chart.pivotCust_RunningTotal_Object.runningTotal.row;
                _chart.runningTotal_Column = _chart.pivotCust_RunningTotal_Object.runningTotal.column;
            }else{
                _chart.runningTotal_Row = 'no';
                _chart.runningTotal_Column = 'no';
            }
// Running Total Object end


// Exclude Key Start
            _chart.exclude_Row = {};
            _chart.exclude_Col = {};
            if(_chart._axisConfig.pivotCust_Exclude){
                _chart.exclude_Row = _chart._axisConfig.pivotCust_Exclude.row;
                _chart.exclude_Col = _chart._axisConfig.pivotCust_Exclude.col;
            }
// Exclude Key End

// ReOrder Key Start
            _chart.reOrder_Row = [];
            _chart.reOrder_Col = [];
            if(_chart._axisConfig.pivotCust_ReOrder){
                _chart.reOrder_Row = _chart._axisConfig.pivotCust_ReOrder.row;
                _chart.reOrder_Col = _chart._axisConfig.pivotCust_ReOrder.col;
            }
// ReOrder Key End





// Custom Pivot Function Start
            _chart.Pivot_Create_JsonData = function(){
                _chart.pivotCust_Selected_Column_Data = [];
                if(_chart.pivotCust_Selected_Data.length > 1){
                    _chart.pivotCust_Selected_Column_Data = _chart.pivotCust_Selected_Column.concat(_chart.pivotCust_Selected_Data);
                }else{
                    _chart.pivotCust_Selected_Column_Data = _chart.pivotCust_Selected_Column;
                }
            }
            _chart.Pivot_Create_RowColHeader = function(){
                _chart.rowHeadAttr = [];
                _chart.colHeadAttr = [];
                _chart.pivotCust_Selected_Row.forEach(function(d,i){
                    _chart.ptCust_MeasureDimension_Arr.forEach(function(data, index){
                        if(d == data.columnName){
                            var titleRowData = data.reName.slice(0, 15) + (data.reName.length > 15 ? "..." : "");
                            var temp = i + '_-_' + d + '_-_Row';
                            if(_chart.reOrder_Row.length){
                                var tempRowData = '<td><span title="' + data.reName + '" class="ptCust_Attr_Table" contId="' + _chart.id + '" id="' + temp + '" >' + titleRowData +'</span></td>';
                            }else{
                                if(_chart.pivotCust_Sort_Object.Row[d] == 'asc'){
                                    var tempRowData = '<td><span title="' + data.reName + '" class="ptCust_Attr_Table" contId="' + _chart.id + '" id="' + temp + '" onclick="sketchServer.Pivot_Sort_Row_Col(this)">' + titleRowData +'&nbsp;&nbsp;<i class="fa fa-sort-asc ptCust_SortIcon"></i></span></td>';
                                }else{
                                    var tempRowData = '<td><span title="' + data.reName + '" class="ptCust_Attr_Table" contId="' + _chart.id + '" id="' + temp + '" onclick="sketchServer.Pivot_Sort_Row_Col(this)">' + titleRowData +'&nbsp;&nbsp;<i class="fa fa-sort-desc ptCust_SortIcon"></i></span></td>';
                                }
                            }
                            _chart.rowHeadAttr.push(tempRowData);
                        }
                    });
                });

                _chart.pivotCust_Selected_Column.forEach(function(d,i){
                    _chart.ptCust_MeasureDimension_Arr.forEach(function(data, index){
                        if(d == data.columnName){
                            var titleColData = data.reName.slice(0, 15) + (data.reName.length > 15 ? "..." : "");
                            var temp = i + '_-_' + d + '_-_Column';
                            if(_chart.reOrder_Row.length){
                                var tempColData = '<td><span title="' + data.reName + '" class="ptCust_Attr_Table" contId="' + _chart.id + '" id="' + temp + '" >' + titleColData +'</span></td>';
                            }else{
                                if(_chart.pivotCust_Sort_Object.Column[d] == 'asc'){
                                    var tempColData = '<td><span title="' + data.reName + '" class="ptCust_Attr_Table" contId="' + _chart.id + '" id="' + temp + '" onclick="sketchServer.Pivot_Sort_Row_Col(this)">' + titleColData +'&nbsp;&nbsp;<i class="fa fa-sort-asc ptCust_SortIcon"></i></span></td>';
                                }else{
                                    var tempColData = '<td><span title="' + data.reName + '" class="ptCust_Attr_Table" contId="' + _chart.id + '" id="' + temp + '" onclick="sketchServer.Pivot_Sort_Row_Col(this)">' + titleColData +'&nbsp;&nbsp;<i class="fa fa-sort-desc ptCust_SortIcon"></i></span></td>';
                                }
                            }
                            _chart.colHeadAttr.push(tempColData);
                        }
                    });
                });
            }





            draw.Pivot_Sort_Row_Col = function(spanHtml){
                var chart_ID = $(spanHtml).attr('contid');
                var _chart = draw.chartRegistry.get(chart_ID);

                var span_Html_ID = $(spanHtml).attr('id');
                var span_ID_Arr = span_Html_ID.split('_-_');
                var span_Id_Val = span_ID_Arr[0];
                var span_Name_Val = span_ID_Arr[1];
                var span_Attr_Type = span_ID_Arr[2];

                if(span_Attr_Type == 'Row'){
                    $.each(_chart.pivotCust_Sort_Object.Row, function(k, v){
                        if(span_Name_Val == k){
                            if(v == 'asc'){
                                _chart.pivotCust_Sort_Object.Row[k] = 'desc';
                            }else{
                                _chart.pivotCust_Sort_Object.Row[k] = 'asc';
                            }
                        }
                    });
                }else if(span_Attr_Type == 'Column'){
                    $.each(_chart.pivotCust_Sort_Object.Column, function(k, v){
                        if(span_Name_Val == k){
                            if(v == 'asc'){
                                _chart.pivotCust_Sort_Object.Column[k] = 'desc';
                            }else{
                                _chart.pivotCust_Sort_Object.Column[k] = 'asc';
                            }
                        }
                    });
                }
                _chart.render();
            }





            _chart.Pivot_Create_RowData = function(){
                _chart.dataGroupRow.forEach(function(d){
                    var i = 0;
                    createRowLevel(d,i);
                });

                function createRowLevel(theObject,pk) {
                    var result = null;
                    if(theObject.key!=undefined){
                        theObject['rowLevel'] = pk;
                    }
                    if(theObject instanceof Array) {
                        for(var i = 0; i < theObject.length; i++) {
                            result = createRowLevel(theObject[i],pk);
                        }
                    }else{
                        pk++;
                        for(var prop in theObject) {
                            if(theObject[prop] instanceof Object || theObject[prop] instanceof Array){
                                result = createRowLevel(theObject[prop],pk);
                            }
                        }
                    }
                }

                $.each(_chart.pivotCust_Sort_Object.Row, function(k,v){
                    if(_chart.pivotCust_Selected_Row[0] == k){
                        if(!_chart.reOrder_Row.length){
                            if(v == 'desc'){
                                _chart.dataGroupRow.sort(function(a, b){
                                    return d3.descending(a.key, b.key);
                                });
                            }else{
                                _chart.dataGroupRow.sort(function(a, b){
                                    return d3.ascending(a.key, b.key);
                                });
                            }
                        }
                    }
                });
                $.each(_chart.pivotCust_Sort_Object.Row, function(k, v){
                    if(_chart.pivotCust_Selected_Row.indexOf(k) != -1){
                        var R_Id = _chart.pivotCust_Selected_Row.indexOf(k);
                        if(R_Id != 0){
                            data_Sort_Row(_chart.dataGroupRow);
                        }
                        function data_Sort_Row(rowData){
                            if(rowData != undefined){
                                rowData.forEach(function(d){
                                    if(d.rowLevel == R_Id-1){
                                        d.values.sort(function(a, b) {
                                            if(v == 'desc'){
                                                return d3.descending(a.key, b.key);
                                            }else{
                                                return d3.ascending(a.key, b.key);
                                            }
                                        });
                                    }
                                    data_Sort_Row(d.values);
                                });
                            }
                        }
                    }
                });

                processRowData(_chart.dataGroupRow);
                function processRowData(process_Row_Data){
                    process_Row_Data.forEach(function(d){
                        if(d.key){
                            if(!process_Row_Data.count)
                                process_Row_Data.count = 0;
                            process_Row_Data.count += processRowData(d.values);
                        }else{
                            if(!process_Row_Data.count)
                                process_Row_Data.count = 0;
                            process_Row_Data.count = 1;
                            return process_Row_Data.count;
                        }
                    });
                    return process_Row_Data.count;
                }

                _chart.rowTableData = [];
                createRowTableID(_chart.dataGroupRow,0);

                function createRowTableID(nodes, level){
                    _chart.rowTableData[level] = _chart.rowTableData[level] || [];
                    nodes.forEach(function(d){
                        if(d.key){
                            if ('values' in d) {
                                createRowTableID(d.values, level + 1);
                            }
                            _chart.rowTableData[level].push({
                                key: d.key,
                                count: d.values.count
                            });
                        }
                    });
                }

                _chart.rowTableDataKey = _chart.rowTableData;
                _chart.rowTableDataKey.splice((_chart.rowTableDataKey.length-1),1);

                if(_chart.subTotal_Row == 'yes' && _chart.pivotCust_Selected_Row.length>1){
                    var temp_Arr = [];
                    if(_chart.rowTableDataKey[0] != undefined){
                        _chart.rowTableDataKey[0].forEach(function(d){
                            var temp = d.count;
                            temp_Arr.push(temp);
                        });
                    }
                    _chart.Sub_Row_Pos_Arr = [];
                    var lastArrVal=0;
                    for(var i=0; i<temp_Arr.length; i++){
                        _chart.Sub_Row_Pos_Arr.push(temp_Arr[i]+lastArrVal);
                        lastArrVal = temp_Arr[i] + lastArrVal;
                    }
                }

                _chart.rowTableDataKey.forEach(function(d,i){
                    draw_Td_Row_ID(i,d);
                });
                function draw_Td_Row_ID(index,data){
                    window['Row_' + index]=[];
                    data.forEach(function(d){
                        for(var a=0; a<d.count; a++){
                            window['Row_' + index].push(d.key);
                        }
                    });
                }

                _chart.row_Table_Id_Arr = [];
                for(var i=0; i<_chart.rowTableDataKey.length; i++){
                    _chart.row_Table_Id_Arr.push(window['Row_' + i]);
                }

                if(_chart.subTotal_Row == 'yes' && _chart.pivotCust_Selected_Row.length>1){
                    var tempArr = _chart.row_Table_Id_Arr;
                    _chart.row_Table_Id_Arr = [];
                    tempArr.forEach(function(d, index){
                        var lastRowIndex = 0, arr = [];
                        if(index == 0){
                            _chart.Sub_Row_Pos_Arr.forEach(function(e,i){
                                for(var i=lastRowIndex; i<e; i++){
                                    arr.push(d[i]);
                                    if(i == e-1 ){
                                        lastRowIndex = e;
                                        arr.push(d[i]);
                                    }
                                }
                            });
                            _chart.row_Table_Id_Arr.push(arr);
                        }else{
                            _chart.Sub_Row_Pos_Arr.forEach(function(e,i){
                                for(var i=lastRowIndex; i<e; i++){
                                    arr.push(d[i]);
                                    if(i == e-1 ){
                                        lastRowIndex = e;
                                        arr.push("SubRow");
                                    }
                                }
                            });
                            _chart.row_Table_Id_Arr.push(arr);
                        }
                    });
                }

                if(_chart.grandTotal_Row == 'yes'){
                    if(_chart.row_Table_Id_Arr.length){
                        var grand_Total_Row_Key_Length = _chart.row_Table_Id_Arr[0].length;
                        _chart.row_Table_Id_Arr.forEach(function(d,index){
                            d.forEach(function(e,i){
                                if(i==0){
                                    _chart.row_Table_Id_Arr[index][grand_Total_Row_Key_Length] = 'GrandRow';
                                }
                            });
                        });
                    }
                }

                _chart.row_Data_Id_Arr = [];
                if(_chart.row_Table_Id_Arr != undefined && _chart.row_Table_Id_Arr[0] != undefined){
                    for(var i=0; i<_chart.row_Table_Id_Arr[0].length; i++){
                        var tempArr=[];
                        for(var j=0; j<_chart.row_Table_Id_Arr.length; j++){
                            var temp = _chart._container + '_' + _chart.row_Table_Id_Arr[j][i] + '_-_';
                            tempArr.push(temp);
                        };
                        _chart.row_Data_Id_Arr.push(tempArr);
                    }
                }

                _chart.data_RowID = [];
                _chart.row_Data_Id_Arr.forEach(function(d){
                    var temp = '';
                    d.forEach(function(e){
                        temp+=e;
                    });
                    _chart.data_RowID.push(temp);
                });

                if(_chart.subTotal_Row == 'yes' && _chart.pivotCust_Selected_Row.length>1){
                    var sub_Row_Key_Length = _chart.pivotCust_Selected_Row.length - 2;
                    _chart.dataGroupRow.forEach(function(d){
                        if(d.values[d.values.length-1].key != 'SubTotal'){
                            d.values[d.values.length] = {};
                            d.values[d.values.length-1]['key'] = 'SubTotal';
                            d.values[d.values.length-1]['values'] = 'Sub_Row_Val';
                            d.values.count += 1;
                        }
                    });

                    var curr_Obj = [{"key":"", values:'end'}];
                    var createRow_SubKeys = function(curr_Obj, n){
                        if(n-2<1){
                            curr_Obj[0]['values']=[{"key":"", values:[{"Temp_ID": 11}] }];
                        }else{
                            curr_Obj[0]['values']=[{"key":"", values:[{"Temp_ID": 11}] }];
                            createRow_SubKeys(curr_Obj[0]['values'],n-1);
                        }
                    }
                    createRow_SubKeys(curr_Obj, sub_Row_Key_Length);
                    _chart.dataGroupRow.forEach(function(d){
                        d.values[d.values.length-1].values = curr_Obj;
                    });
                }

                _chart.pivotCust_RowData_Arr = [];
                createRowTable('main', _chart.dataGroupRow);

                function createRowTable(type, data){
                    data.forEach(function(d,i){
                        if(d.key){
                            var pt_Row_Id = _chart._container + '_' + d.key;
                            if(i==0){
                                if(type == 'main'){
                                    var temp = '<tr class="rowTableData_Tr"><td type="Row" cellId="'+d.rowLevel+'" onclick="sketchServer.Pivot_Cross_Filter(this)" value="'+_chart._container+'" class="rowTableData_Td" title="' + d.key + '" id="' + pt_Row_Id + '" colspan="1" rowspan="' + d.values.count + '" >' + d.key + '</td>';
                                    _chart.pivotCust_RowData_Arr.push(temp);
                                }else if(type == 'if'){
                                    var temp = '<td type="Row" cellId="'+d.rowLevel+'" onclick="sketchServer.Pivot_Cross_Filter(this)" value="'+_chart._container+'" class="rowTableData_Td" title="' + d.key + '" id="' + pt_Row_Id + '" colspan="1" rowspan="' + d.values.count + '" >' + d.key + '</td>';
                                    _chart.pivotCust_RowData_Arr.push(temp);
                                }else{
                                    var temp = '<td type="Row" cellId="'+d.rowLevel+'" onclick="sketchServer.Pivot_Cross_Filter(this)" value="'+_chart._container+'" class="rowTableData_Td" title="' + d.key + '" id="' + pt_Row_Id + '" colspan="1" rowspan="' + d.values.count + '" >' + d.key + '</td>';
                                    _chart.pivotCust_RowData_Arr.push(temp);
                                }
                                createRowTable('if', d.values);
                            }else{
                                if(type == 'main'){
                                    var temp = '<tr class="rowTableData_Tr"><td type="Row" cellId="'+d.rowLevel+'" onclick="sketchServer.Pivot_Cross_Filter(this)" value="'+_chart._container+'" class="rowTableData_Td" title="' + d.key + '" id="' + pt_Row_Id + '" colspan="1" rowspan="' + d.values.count + '" >' + d.key + '</td>';
                                    _chart.pivotCust_RowData_Arr.push(temp);
                                }else if(type == 'else'){
                                    if(d.values.count == undefined){
                                        var cSpan_Val = _chart.pivotCust_Selected_Row.length - 1;
                                        var temp = '<tr class="rowTableData_Tr"><td type="Row" cellId="'+d.rowLevel+'" onclick="sketchServer.Pivot_Cross_Filter(this)" value="'+_chart._container+'" class="rowTableData_Td row_Col_SubTotal_Header" title="' + d.key + '" id="' + pt_Row_Id + '" colspan="' + cSpan_Val + '" rowspan="1" >' + d.key + '</td>';
                                        _chart.pivotCust_RowData_Arr.push(temp);
                                    }else{
                                        var temp = '<tr class="rowTableData_Tr"><td type="Row" cellId="'+d.rowLevel+'" onclick="sketchServer.Pivot_Cross_Filter(this)" value="'+_chart._container+'" class="rowTableData_Td" title="' + d.key + '" id="' + pt_Row_Id + '" colspan="1" rowspan="' + d.values.count + '" >' + d.key + '</td>';
                                        _chart.pivotCust_RowData_Arr.push(temp);
                                    }
                                }else{
                                    if(d.values.count == undefined){
                                        var cSpan_Val = _chart.pivotCust_Selected_Row.length - 1;
                                        var temp = '<tr class="rowTableData_Tr"><td type="Row" cellId="'+d.rowLevel+'" onclick="sketchServer.Pivot_Cross_Filter(this)" value="'+_chart._container+'" class="rowTableData_Td row_Col_SubTotal_Header" title="' + d.key + '" id="' + pt_Row_Id + '" colspan="' + cSpan_Val + '" rowspan="1" >' + d.key + '</td>';
                                        _chart.pivotCust_RowData_Arr.push(temp);
                                    }else{
                                        var temp = '<tr class="rowTableData_Tr"><td type="Row" cellId="'+d.rowLevel+'" onclick="sketchServer.Pivot_Cross_Filter(this)" value="'+_chart._container+'" class="rowTableData_Td" title="' + d.key + '" id="' + pt_Row_Id + '" colspan="1" rowspan="' + d.values.count + '" >' + d.key + '</td>';
                                        _chart.pivotCust_RowData_Arr.push(temp);
                                    }
                                }
                                createRowTable('else', d.values);
                            }
                        }
                    });
                }

                if(_chart.grandTotal_Row == 'yes'){
                    var temp = '<tr class="rowTableData_Tr"><td title="GrandTotal" type="row" cellId="'+i+'" onclick="sketchServer.Pivot_Cross_Filter(this)" value="'+_chart._container+'" class="rowTableData_Td row_Col_GrandTotal_Header" id="GrandTotal" rowspan="1" colspan="' + _chart.pivotCust_Selected_Row.length + '" >GrandTotal</td></tr>';
                    _chart.pivotCust_RowData_Arr.push(temp);
                }
            }





            _chart.Pivot_Create_ColData = function(){
                _chart.dataGroupCol.forEach(function(d){
                    var i = 0;
                    createColLevel(d,i);
                });

                function createColLevel(theObject,pk) {
                    var result = null;
                    if(theObject.key!=undefined){
                        theObject['colLevel'] = pk;
                    }
                    if(theObject instanceof Array) {
                        for(var i = 0; i < theObject.length; i++) {
                            result = createColLevel(theObject[i],pk);
                        }
                    }else{
                        pk++;
                        for(var prop in theObject) {
                            if(theObject[prop] instanceof Object || theObject[prop] instanceof Array){
                                result = createColLevel(theObject[prop],pk);
                            }
                        }
                    }
                }

                $.each(_chart.pivotCust_Sort_Object.Column, function(k,v){
                    if(_chart.pivotCust_Selected_Column[0] == k){
                        if(!_chart.reOrder_Col.length){
                            if(v == 'desc'){
                                _chart.dataGroupCol.sort(function(a, b){
                                    return d3.descending(a.key, b.key);
                                });
                            }else{
                                _chart.dataGroupCol.sort(function(a, b){
                                    return d3.ascending(a.key, b.key);
                                });
                            }
                        }
                    }
                });

                $.each(_chart.pivotCust_Sort_Object.Column, function(k, v){
                    if(_chart.pivotCust_Selected_Column.indexOf(k) != -1){
                        var C_Id = _chart.pivotCust_Selected_Column.indexOf(k);
                        if(C_Id != 0){
                            data_Sort_Col(_chart.dataGroupCol);
                        }
                        function data_Sort_Col(colData){
                            if(colData != undefined){
                                colData.forEach(function(d){
                                    if(d.colLevel == C_Id-1){
                                        d.values.sort(function(a, b) {
                                            if(v == 'desc'){
                                                return d3.descending(a.key, b.key);
                                            }else{
                                                return d3.ascending(a.key, b.key);
                                            }
                                        });
                                    }
                                    data_Sort_Col(d.values);
                                });
                            }
                        }
                    }
                });

                processColData(_chart.dataGroupCol);
                function processColData(data){
                    data.forEach(function(d){
                        if(d.key){
                            if(!data.count)
                                data.count=0;
                            data.count+=processColData(d.values);
                        }else{
                            if(!data.count)
                                data.count=0;
                            data.count=1;
                            return data.count;
                        }
                    });
                    return data.count;
                }

                if(_chart.subTotal_Column == 'yes' && _chart.pivotCust_Selected_Column.length>1){
                    var sub_Col_Key_Length = _chart.pivotCust_Selected_Column.length - 2;
                    _chart.dataGroupCol.forEach(function(d){
                        if(d.values[d.values.length-1].key != 'SubTotal'){
                            d.values[d.values.length] = {};
                            d.values[d.values.length-1]['key'] = 'SubTotal';
                            d.values[d.values.length-1]['values'] = 'Sub_Col_Val';
                            d.values.count += 1;
                        }
                    });
                    var curr_Obj = [{"key":" ", values:'end'}];
                    var createCol_SubKeys = function(curr_Obj_1, n){
                        if(n<=0){
                            curr_Obj = [{"Temp_Key_1":"key_1", "Temp_Key_2":"key_2"}];
                            return curr_Obj;
                        }else if(n==1){
                            curr_Obj = [{"key":" ", values: [{"Temp_Key_1":"key_1", "Temp_Key_2":"key_2"}] }];
                            return curr_Obj;
                        }
                        else{
                            if(n-2<1){
                                curr_Obj_1[0]['values'] = [{"key":" ", values:[{"Temp_ID": 11}] }];
                            }else{
                                curr_Obj_1[0]['values'] = [{"key":" ", values:[{"Temp_ID": 11}] }];
                                createCol_SubKeys(curr_Obj_1[0]['values'], n-1);
                            }
                        }
                    }
                    createCol_SubKeys(curr_Obj, sub_Col_Key_Length);
                    _chart.dataGroupCol.forEach(function(d){
                        d.values[d.values.length-1].values = curr_Obj;
                    });
                }

                _chart.columnTableData = [];
                processColObject(_chart.dataGroupCol,0);
                function processColObject(nodes, level) {
                    _chart.columnTableData[level] = _chart.columnTableData[level] || [];
                    nodes.forEach(function(d){
                        if(d.key){
                            if ('values' in d) {
                                processColObject(d.values, level + 1);
                            }
                            _chart.columnTableData[level].push({
                                key: d.key,
                                count: d.values.count
                            });
                        }
                    });
                }

                _chart.colTableDataKey = _chart.columnTableData;
                _chart.colTableDataKey.splice((_chart.colTableDataKey.length-1),1);

                _chart.ptCust_ColData_Arr = [];
                if(_chart.pivotCust_Selected_Data.length > 1){
                    _chart.columnTableData.forEach(function(d,i){
                        if(i == _chart.columnTableData.length-1){
                            for(var i=0; i<d.length; i++){
                                for(var j=0; j<_chart.pivotCust_Selected_Data.length; j++){
                                    var temp = {};
                                    temp['key'] = _chart.pivotCust_Selected_Data[j];
                                    temp['count'] = 1;
                                    _chart.ptCust_ColData_Arr.push(temp);
                                }
                            }
                        }
                    });
                }

                if(_chart.pivotCust_Selected_Data.length > 1){
                    _chart.columnTableData[_chart.columnTableData.length] = _chart.ptCust_ColData_Arr;
                }

                _chart.columnTableData.forEach(function(d,i){
                    if(_chart.pivotCust_Selected_Data.length > 1){
                        if(i != _chart.columnTableData.length-1){
                            d.forEach(function(e){
                                if(e.key == 'SubTotal' || e.key == ' ' ){
                                    e.count = _chart.pivotCust_Selected_Data.length;
                                }else{
                                    e.count = e.count * _chart.pivotCust_Selected_Data.length;
                                }
                            });
                        }
                    }else{
                        if(i != _chart.columnTableData.length-1){
                            d.forEach(function(e){
                                e.count = e.count * _chart.pivotCust_Selected_Data.length;
                            });
                        }
                    }
                });

                _chart.colTableDataKey.forEach(function(d,i){
                    draw_Td_Col_ID(i, d);
                });
                function draw_Td_Col_ID(index,data){
                    window['Col_' + index] = [];
                    data.forEach(function(d){
                        for(var a=0; a<d.count; a++){
                            window['Col_' + index].push(d.key);
                        }
                    });
                }

                _chart.col_Table_Id_Arr = [];
                for(var i=0; i < _chart.colTableDataKey.length; i++){
                    _chart.col_Table_Id_Arr.push(window['Col_' + i]);
                }

                if(_chart.subTotal_Column == 'yes' && _chart.pivotCust_Selected_Column.length>1){
                    if(_chart.pivotCust_Selected_Data.length>1){
                        _chart.col_Table_Id_Arr.forEach(function(d, index){
                            if(index!=0 && index!=(_chart.col_Table_Id_Arr.length-1)){
                                d.forEach(function(e,i){
                                    if(e == 'SubTotal' || e == ' ' ){
                                        d[i] = 'SubColumn';
                                    }
                                });
                            }
                        });
                    }else{
                        var temp_Col_Multi_ID_Arr = _chart.col_Table_Id_Arr;
                        _chart.col_Table_Id_Arr = [];
                        temp_Col_Multi_ID_Arr.forEach(function(d,index){
                            if(index == 0){
                                _chart.Sub_Single_Col_Pos_Obj = {};
                                _chart.Sub_Single_Col_Pos_Arr = [];
                                d.forEach(function(e, i){
                                    if(_chart.Sub_Single_Col_Pos_Obj[e] == undefined){
                                        _chart.Sub_Single_Col_Pos_Obj[e] = i;
                                    }else{
                                        _chart.Sub_Single_Col_Pos_Obj[e] = i;
                                    }
                                });
                                $.each(_chart.Sub_Single_Col_Pos_Obj, function(e,i){
                                    _chart.Sub_Single_Col_Pos_Arr.push(i);
                                });
                                _chart.col_Table_Id_Arr.push(d);
                            }else{
                                var tempArr=[];
                                d.forEach(function(f){
                                    if(_chart.Sub_Single_Col_Pos_Arr.indexOf(tempArr.length)!=-1){
                                        tempArr.push("SubColumn");
                                    }
                                    tempArr.push(f);
                                });
                                tempArr.push("SubColumn");
                                _chart.col_Table_Id_Arr.push(tempArr);
                            }
                        });
                    }
                }

                if(_chart.grandTotal_Column == 'yes'){
                    if(_chart.pivotCust_Selected_Data.length > 1){
                        _chart.col_Table_Id_Arr.forEach(function(d,index){
                            if(index == (_chart.col_Table_Id_Arr.length-1)){
                                _chart.pivotCust_Selected_Data.forEach(function(e){
                                    _chart.col_Table_Id_Arr[_chart.col_Table_Id_Arr.length-1].push(e);
                                });
                            }else{
                                for(var i=0; i<_chart.pivotCust_Selected_Data.length; i++){
                                    _chart.col_Table_Id_Arr[index].push('GrandColumn');
                                }
                            }
                        });
                    }else{
                        if(_chart.col_Table_Id_Arr && _chart.col_Table_Id_Arr.length){
                            var grand_Total_Column_Key_Length = _chart.col_Table_Id_Arr[0].length;
                            _chart.col_Table_Id_Arr.forEach(function(d,index){
                                d.forEach(function(e,i){
                                    if(i==0){
                                        _chart.col_Table_Id_Arr[index][grand_Total_Column_Key_Length] = 'GrandColumn';
                                    }
                                });
                            });
                        }

                    }
                }

                _chart.col_Data_Id_Arr = [];
                if(_chart.col_Table_Id_Arr.length){
                    for(var i=0; i<_chart.col_Table_Id_Arr[0].length; i++){
                        var tempArr=[];
                        for(var j=0; j < _chart.col_Table_Id_Arr.length ;j++){
                            if(_chart.col_Table_Id_Arr[j][i] != undefined){
                                var temp = _chart._container + '_' + _chart.col_Table_Id_Arr[j][i] + '_-_';
                                tempArr.push(temp);
                            }
                        };
                        _chart.col_Data_Id_Arr.push(tempArr);
                    }
                }

                _chart.data_ColID = [];
                _chart.col_Data_Id_Arr.forEach(function(d){
                    var temp = '';
                    d.forEach(function(e){
                        temp+=e;
                    });
                    _chart.data_ColID.push(temp);
                });

                if(_chart.pivotCust_Selected_Data.length>1){
                    var temp_Rename_Arr = [];
                    _chart.pivotCust_Selected_Data.forEach(function(d){
                        _chart.ptCust_MeasureDimension_Arr.forEach(function(data){
                            if(d == data.columnName){
                                var temp_Rename_Obj = {};
                                temp_Rename_Obj[data.columnName] = data.reName;
                                temp_Rename_Arr.push(temp_Rename_Obj);
                            }
                        });
                    });
                    temp_Rename_Arr.forEach(function(d,i){
                        $.each(d, function(k,v){
                            _chart.columnTableData[_chart.columnTableData.length-1].forEach(function(d,i){
                                if(k == d.key){
                                    d.key = v;
                                }
                            });
                        });
                    });
                }

                _chart.pivotCust_ColData = '';
                createColTable(_chart.columnTableData);

                function createColTable(colData){
                    colData.forEach(function(d,index){
                        _chart.pivotCust_ColData += "<tr class='colTableData_Tr'>";
                        d.forEach(function(e){
                            var pt_Col_Id = _chart._container + '_' + e.key;
                            if(e.key == "SubTotal"){
                                var temp = '<td type="Col" cellId="'+index+'" onclick="sketch.Pivot_Cross_Filter(this)" value="'+_chart._container+'" class="colTableData_Td row_Col_SubTotal_Header" title="' + e.key + '" id="' + pt_Col_Id + '" colspan="' + _chart.pivotCust_Selected_Data.length + '" >' + e.key + '</td>';
                                _chart.pivotCust_ColData += temp;
                            }else{
                                var temp = '<td type="Col" cellId="'+index+'" onclick="sketchServer.Pivot_Cross_Filter(this)" value="'+_chart._container+'" class="colTableData_Td" title="' + e.key + '"  id="' + pt_Col_Id + '" colspan=' + e.count + '>' + e.key + '</td>';
                                _chart.pivotCust_ColData += temp;
                            }
                        });

                        if(_chart.grandTotal_Column == 'yes'){
                            if(_chart.pivotCust_Selected_Data.length>1){
                                if(index == 0){
                                    var temp = '<td type="Col" cellId="'+index+'" onclick="sketchServer.Pivot_Cross_Filter(this)" value="'+_chart._container+'" class="colTableData_Td row_Col_GrandTotal_Header" title="Grand Total" id="Grand Total" colspan="' + _chart.pivotCust_Selected_Data.length + '" >Grand Total</td>';
                                    _chart.pivotCust_ColData += temp;
                                }else if(index == (_chart.columnTableData.length-1)){
                                    _chart.pivotCust_Selected_Data.forEach(function(d){
                                        _chart.ptCust_MeasureDimension_Arr.forEach(function(a){
                                            if(d == a.columnName){
                                                var pt_Col_Id = _chart._container + '_' + d;
                                                var temp = '<td type="Col" cellId="'+index+'" onclick="sketchServer.Pivot_Cross_Filter(this)" value="'+_chart._container+'" class="colTableData_Td row_Col_GrandTotal_Data" title="' + a.reName + '" id="' + d + '" >' + a.reName + '</td>';
                                                _chart.pivotCust_ColData += temp;
                                            }
                                        });
                                    });
                                }else{
                                    var temp = '<td type="Col" cellId="'+index+'" onclick="sketchServer.Pivot_Cross_Filter(this)" value="'+_chart._container+'" class="colTableData_Td" colspan="' + _chart.pivotCust_Selected_Data.length + '"></td>';
                                    _chart.pivotCust_ColData += temp;
                                }
                            }else{
                                if(index == 0){
                                    var temp = '<td type="Col" cellId="'+index+'" onclick="sketchServer.Pivot_Cross_Filter(this)" value="'+_chart._container+'" class="colTableData_Td row_Col_GrandTotal_Header" title="Grand Total" id="Grand Total" colspan="' + _chart.pivotCust_Selected_Data.length + '">Grand Total</td>';
                                    _chart.pivotCust_ColData += temp;
                                }else{
                                    var temp = '<td type="Col" cellId="'+index+'" onclick="sketchServer.Pivot_Cross_Filter(this)" value="'+_chart._container+'" class="colTableData_Td" ></td>';
                                    _chart.pivotCust_ColData += temp;
                                }
                            }
                        }
                        _chart.pivotCust_ColData += "</tr>";
                    });
                    return _chart.pivotCust_ColData;
                }
            }





            draw._pivotCust_Filter_Arr = [];
            draw._pivotCust_Filter_Obj = {};
            draw.Pivot_Cross_Filter = function(Table_Cell_Html){
                if($( ".cust-tab-pane" ).length){
                    $(".cust-tab-pane").addClass("active");
                    $(".loadingBar").show();
                }
                var cont_ID = $(Table_Cell_Html).attr('value');
                var table_Type = $(Table_Cell_Html).attr('type');
                var table_TD_ID = $(Table_Cell_Html).attr('id');

                var table_Data_Val = table_TD_ID.split('_')[1];
                var table_Cell_Id = $(Table_Cell_Html).attr('cellId');
                var _pivotCust_Config, _pivotCust_Column = [], _pivotCust_Row = [], _pivotCust_Data = [];

                var chart_ID = 'chart-' + cont_ID;
                var _chart = draw.chartRegistry.get(chart_ID);

                eChartServer._metadataId = _chart.metadataId;
                draw._pivotCust_Arr.forEach(function(d){
                    if(cont_ID == d.container_ID){
                        _pivotCust_Column = _chart.pivotCust_Selected_Column;
                        _pivotCust_Row = _chart.pivotCust_Selected_Row;
                        _pivotCust_Data = _chart.pivotCust_Selected_Data;
                        _pivotCust_Config = d.config;
                    }
                });

                if(table_Type == 'Col' || table_Type == 'Row'){
                    if(table_Type == 'Col'){
                        var _pivotCust_Attribute = _pivotCust_Column[table_Cell_Id];
                    }else if(table_Type == 'Row'){
                        var _pivotCust_Attribute = _pivotCust_Row[table_Cell_Id];
                    }

                    var tempPivotMea = Object.values(_pivotCust_Config.checkboxModelMeasure);
                    var tempPivotDim = Object.values(_pivotCust_Config.checkboxModelDimension);

                    var _pivotCust_Measure = [];
                    var _pivotCust_Dimension = [];
                    var _pivotCust_Attr_Type;

                    tempPivotMea.forEach(function(d,i){
                        _pivotCust_Measure.push(d.columnName);
                    });
                    tempPivotDim.forEach(function(d,i){
                        _pivotCust_Dimension.push(d.columnName);
                    });

                    if($.isEmptyObject(draw._pivotCust_Filter_Obj)){
                        _pivotCust_Dimension.forEach(function(d){
                            draw._pivotCust_Filter_Obj[d] = [];
                        });
                    }

                    draw._pivotCust_Filter_Arr.push(draw._pivotCust_Filter_Obj);

                    _pivotCust_Measure.forEach(function(d){
                        if(d.includes(_pivotCust_Attribute)){
                            _pivotCust_Attr_Type = 'measure';
                        }
                    });
                    _pivotCust_Dimension.forEach(function(d){
                        if(d.includes(_pivotCust_Attribute)){
                            _pivotCust_Attr_Type = 'dimension';
                        }
                    });

                    if(_pivotCust_Attr_Type == 'dimension'){
                        var _pivotCust_Attr_Obj = {};
                        _pivotCust_Attr_Obj['columnName'] = _pivotCust_Attribute;
                        if(_chart.ptCust_Dimension_Cross_Filter[_pivotCust_Attribute] == undefined){
                            _chart.ptCust_Dimension_Cross_Filter[_pivotCust_Attribute] = draw._createDimension(_pivotCust_Attr_Obj);
                        }
                        if (!_chart.filter) {
                            _chart.filter = [];
                        }

                        var index = _chart.filter.indexOf(table_Data_Val);
                        if (index === -1) {
                            $("td[id='" + table_TD_ID + "']").addClass('row_Col_Filter');
                            _chart.filter.push(table_Data_Val);

                            $.each(draw._pivotCust_Filter_Arr[0], function(k,v){
                                if(_pivotCust_Attribute == k){
                                    v.push(table_Data_Val);
                                }
                            });
                        } else {
                            $("td[id='" + table_TD_ID + "']").removeClass('row_Col_Filter');
                            _chart.filter.splice(index, 1);

                            $.each(draw._pivotCust_Filter_Arr[0], function(k,v){
                                if(_pivotCust_Attribute == k){
                                    var V_Index = v.indexOf(table_Data_Val);
                                    v.splice(V_Index, 1);
                                }
                            });
                        }

                        $.post(draw.nodeApiUrl + "/filterPivotRowColumn", {"dimensionAttribute":_pivotCust_Attr_Obj, "filterItem":JSON.stringify(draw._pivotCust_Filter_Arr[0]), "sessionId":draw.getAccessToken(), metadataId:_chart.metadataId})
                            .done(function(data){
                                if(data.status){
                                    demo.showNotification('danger',"No Relevant Data Found");
                                }
                                eChartServer.chartRegistry
                                    .list()
                                    .forEach(function(chart){
                                        if(_chart.id !== chart.id){
                                            delete data[chart.id]['totalKey'];
                                            delete data[chart.id]['reportId'];
                                            chart._group = data[chart.id];
                                        }
                                    });
                                sketchServer.numberWidgetRecordGroup.list().forEach(function(id){
                                    sketchServer.numberWidgetRecordGroup.set(id,data[id]['numberWidget']);
                                });
                                //eChartServer.redrawChartsOnly();
                                eChartServer.redrawEchartChartsOnly(chart_ID);
                            }).fail(function(response) {
                            draw.errorNotification("Pivot");
                        });
                    }
                }

                // if(table_Type == 'Data'){
                //     var Table_Cell_ID = $(Table_Cell_Html).attr('id');
                //     var td_id = Table_Cell_ID.substring(0, Table_Cell_ID.lastIndexOf('_-_'));
                //     var ID_Arr = td_id.split('_-_');
                //     var regex = /([^a-zA-Z0-9])/g;
                //     var sub_String = '\\$1';
                //     Table_Cell_ID = Table_Cell_ID.replace(regex, sub_String);
                //
                //     ID_Arr.forEach(function(d, i){
                //         var filterVal = d.split('_')[1];
                //         if(i == 0 || i == _pivotCust_Row.length ){
                //             if(!_chart.filter){
                //                 _chart.filter = [];
                //             }
                //             var index = _chart.filter.indexOf(filterVal);
                //             if(index === -1){
                //                 $('#' + Table_Cell_ID).addClass('row_Col_Filter');
                //                 _chart.filter.push(filterVal);
                //             }else{
                //                 $('#' + Table_Cell_ID).removeClass('row_Col_Filter');
                //                 _chart.filter.splice(index, 1);
                //             }
                //
                //             var dimObj = {};
                //             $.each(_chart._dimension, function(k,v){
                //                 if(k === _chart.pivotCust_Selected_Row[0]){
                //                     dimObj[k] = v;
                //                 }
                //                 if(k === _chart.pivotCust_Selected_Column[0]){
                //                     dimObj[k] = v;
                //                 }
                //             });
                //
                //             if(i == 1){
                //                 $.post(draw.nodeApiUrl + "/filterPivotData", {"dimensionAttribute":dimObj, "filterItem":_chart.filter, "sessionId":draw.getAccessToken(), metadataId:_chart.metadataId})
                //                     .done(function(data) {
                //                         eChartServer.chartRegistry
                //                             .list()
                //                             .forEach(function (chart) {
                //                                 if(_chart.id !== chart.id){
                //                                     chart._group = data[chart.id];
                //                                 }
                //                             });
                //                         sketchServer.numberWidgetRecordGroup.list().forEach(function(id){
                //                             sketchServer.numberWidgetRecordGroup.set(id,data[id]['numberWidget']);
                //                         });
                //                         eChartServer.redrawChartsOnly();
                //                         eChartServer.redrawEchartChartsOnly(chart_ID);
                //                     });
                //             }
                //         }
                //     });
                // }
            }






            _chart.Pivot_Create_TableDataWithID = function(){
                _chart.row_Col_ID_Arr = [];
                for(var r = 0; r<_chart.data_RowID.length; r++){
                    for(var c = 0; c<_chart.data_ColID.length; c++){
                        var temp = _chart.data_RowID[r] + _chart.data_ColID[c];
                        _chart.row_Col_ID_Arr.push(temp);
                    }
                }

                _chart.pivotCust_Data = [];

                for(var i=0; i<_chart.data_RowID.length; i++){
                    var tdArr = [];
                    for(var j=i*_chart.data_ColID.length, k=0; j<_chart.data_ColID.length*(i+1), k<_chart.data_ColID.length; j++, k++){
                        var className = _chart._container + 'PT_' + i + '_' + k;
                        var temp = _chart.row_Col_ID_Arr[j];
                        if(temp.includes('_SubRow_-_') || temp.includes('_SubColumn_-_')){
                            var temp_1 = '<td type="Data" id="' + temp + '" onclick="sketchServer.Pivot_Cross_Filter(this)" value="'+_chart._container+'" class="dataTableData_Td row_Col_SubTotal_Data defaultcontainercls_'+_chart._container+'" ></td>';
                        }else if(temp.includes('_GrandRow_-_') || temp.includes('_GrandColumn_-_')){
                            var temp_1 = '<td type="Data" id="' + temp + '" onclick="sketchServer.Pivot_Cross_Filter(this)" value="'+_chart._container+'" class="dataTableData_Td row_Col_GrandTotal_Data defaultcontainercls_'+_chart._container+'"></td>';
                        }else{
                            var temp_1 = '<td type="Data" id="' + temp + '" onclick="sketchServer.Pivot_Cross_Filter(this)" value="'+_chart._container+'" class="dataTableData_Td ' + className + ' defaultcontainercls_'+_chart._container+'"></td>';
                        }
                        tdArr.push(temp_1);
                    }
                    var temp_2 = '<tr class="dataTableData_Tr">' + tdArr + '</tr>';
                    _chart.pivotCust_Data.push(temp_2);
                }
            }







            _chart.Pivot_Create_Data = function(){

                _chart.pivotDataTable_Object;
                _chart.pivotDataTable_Obj_Arr = [];

                _chart.ptCustTableData_Arr.forEach(function(d){
                    _chart.pivotDataTable_Object = convert_ArrayToObject(d);
                    _chart.pivotDataTable_Obj_Arr.push(_chart.pivotDataTable_Object);
                });

                function convert_ArrayToObject(dataGroup_Data){
                    var output = {};
                    dataGroup_Data.forEach(function(item) {
                        var temp = _chart._container + '_' + item.key;
                        if (!item) return;
                        if (Array.isArray(item.values)) {
                            output[temp] = convert_ArrayToObject(item.values);
                        } else {
                            output[temp] = item;
                        }
                    });
                    return output;
                }

                _chart.Row_Col_Key_Arr = [];

                _chart.pivotCust_Selected_Data.forEach(function(d,i){
                    _chart.pivotDataTable_Obj_Arr.forEach(function(c, index){
                        if(i == index){
                            var temp = _chart._container + '_' + d;
                            create_Row_Column_Key_Data(temp, c, '');
                        }
                    });
                });

                function create_Row_Column_Key_Data(dataFieldKeyName, obj, stack){
                    for(var property in obj){
                        if(obj.hasOwnProperty(property)){
                            if(typeof obj[property] == "object"){
                                create_Row_Column_Key_Data(dataFieldKeyName, obj[property], stack + '_-_' + property);
                            }else{
                                if(property == 'values'){
                                    if(_chart.pivotCust_Selected_Data.length > 1){
                                        var temp = stack + '_-_' + dataFieldKeyName + '_-_' + obj[property];
                                        _chart.Row_Col_Key_Arr.push(temp);
                                    }else{
                                        var temp = stack + '_-_' + obj[property];
                                        _chart.Row_Col_Key_Arr.push(temp);
                                    }
                                }
                            }
                        }
                    }
                }
            }






            _chart.Pivot_Create_TableForPivotData = function(){
                var pivot_Col_HeaderTableID = 'pt_Col_Header_Table_' + _chart._container;
                var main_ColBlock = '<tr style="height: 45px;"><td style="padding: 10px;" colspan="' + _chart.col_Data_Id_Arr.length + '" class="ptCust_Header_Bg_Color ptCust_Main_ColBlockHeader_Table ptCust_Main_RowColBlockHeader_Table"><table id="' + pivot_Col_HeaderTableID + '" ><tbody><tr>' + _chart.colHeadAttr + '</tr></tbody></table></td></tr>';
                main_ColBlock = main_ColBlock.replace(/,/g, '');

                var pivot_ColumnTableID = 'pt_Col_Table_' + _chart._container;
                var data_ColBlock_Table = '<table id="' + pivot_ColumnTableID + '" class="ptCust_Data_RowColData_Table ptCust_Col_DataBlockHeader_Table"><tbody>' + main_ColBlock + _chart.pivotCust_ColData + '</tbody></table>';
                data_ColBlock_Table = data_ColBlock_Table.replace(/,/g, '');

                var pivot_RowTableID = 'pt_Row_Table_' + _chart._container;
                var data_RowBlock_Table = '<table id="' + pivot_RowTableID + '" class="ptCust_Data_RowColData_Table ptCust_Col_DataBlockHeader_Table"><tbody>' + _chart.pivotCust_RowData_Arr + '</tbody></table>';
                data_RowBlock_Table = data_RowBlock_Table.replace(/,/g, '');

                var pivot_DataTableID = 'pt_Data_Table_' + _chart._container;
                var data_DataBlock_Table = '<table id="' + pivot_DataTableID + '" class="ptCust_Data_RowColData_Table ptCust_Col_DataBlockHeader_Table" style="width: -webkit-fill-available;"><tbody>' + _chart.pivotCust_Data + '</tbody></table>';
                data_DataBlock_Table = data_DataBlock_Table.replace(/,/g, '');

                var pivot_Row_HeaderTableID = 'pt_Row_Header_Table_' + _chart._container;
                var main_RowBlock = '<table id="' + pivot_Row_HeaderTableID + '"><tbody><tr>' + _chart.rowHeadAttr + '</tr></tbody></table>';
                main_RowBlock = main_RowBlock.replace(/,/g, '');

                var data_EmptyBlock = '<td style="background-color: white; border: 1px solid transparent;">' + main_RowBlock + '</td>';

                var data_ColBlock = '<td class="ptCust_Header_Bg_Color ptCust_Data_RowColBlockHeader_Table">' + data_ColBlock_Table + '</td>';
                var data_RowBlock = '<td class="ptCust_Header_Bg_Color ptCust_Data_RowColBlockHeader_Table">' + data_RowBlock_Table + '</td>';

                var data_DataBlock = '<td class="ptCust_EmptyBlockHeader_Table">' + data_DataBlock_Table + '</td>';

                var data_FirstRow = '<tr>' + data_EmptyBlock + data_ColBlock + '</tr>';
                var data_SecondRow = '<tr class="ptCust_Data_Table_SecondRow">' + data_RowBlock + data_DataBlock + '</tr>';

                var data_TableBody = '<tbody>' + data_FirstRow + data_SecondRow + '</tbody>';
                var pivotDataTableID = 'pivot_Data_Table_' + _chart._container;
                _chart.data_TableHTML = '<table id="' + pivotDataTableID + '" style="margin-bottom: 0px; border: none !important; " class="table table-bordered bordered table-striped ">' + data_TableBody + '</table>';
            }






            _chart.Pivot_Create_MainTableForPivot = function(){
                var ptCust_Height = $('#chart-' + _chart._container).height();
                var ptCust_Width = $('#chart-' + _chart._container).width();

                var main_EmptyBlock = '<td class="ptCust_EmptyBlockHeader_Table" style="width: 15%;"></td>';
                var main_DataBlock = '<td class="ptCust_EmptyBlockHeader_Table" style="padding: 0px; border-bottom: none !important;">' + _chart.data_TableHTML + '</td>';

                var main_SecondRow = '<tr>' + main_DataBlock + '</tr>';
                var main_TableBody = '<tbody>' + main_SecondRow + '</tbody>';

                var pivotMainTableID = 'pivot_MainTable_' + _chart._container;
                var pivotTableCust_HTML = '<table style="border-bottom: none;" id="' + pivotMainTableID + '" class="table-bordered bordered table-striped">' + main_TableBody + '</table>';

                var ptCust_DivStyle_Attr = 'width: 100%;';
                _chart.pivotContainer_ID = 'pivotCustomized_TableDiv_' + _chart._container;
                var pivotCustomizeHtml = '<div id="' + _chart.pivotContainer_ID + '" style="' + ptCust_DivStyle_Attr + '" >' + pivotTableCust_HTML + '</div>';

                $('#chart-' + _chart._container).html('');
                $('#chart-' + _chart._container).append(pivotCustomizeHtml);
            }






            _chart.Pivot_AssignValue_MainTable = function(){
                $('.defaultcontainercls_'+_chart._container).html(0);

                if(_chart.grandTotal_Column == 'yes' && _chart.pivotCust_Selected_Data.length >1 ){
                    _chart.grand_Row_Col_Key = '';
                    for(var i=0; i<_chart.pivotCust_Selected_Column.length; i++){
                        _chart.grand_Row_Col_Key += _chart._container + '_' + 'GrandColumn_-_';
                    }
                }
                _chart.gTotal_RowObj = {};
                _chart.gTotal_ColObj = {};
                _chart.sTotal_RowObj = {};
                _chart.sTotal_SingleColObj = {};
                _chart.sTotal_MultiColObj = {};
                _chart.sTotal_Row_Col_Single_Obj = {};
                _chart.sTotal_Row_Col_Multi_Obj = {};

                var R_Sub_Row_Arr = [], R_Sub_Col_Arr = [], R_Sub_Val_Arr = [];
                var C_Sub_Row_Col_Arr = [], C_Sub_Val_Arr = [];



                _chart.Row_Col_Key_Arr.forEach(function(d, index){
                    d = d.replace('_-_', '');
                    var td_String = d;
                    var td_ID = td_String.substring(0, td_String.lastIndexOf("_-_") + 1);
                    td_ID = td_ID + '-_';
                    var temp_TD_ID = td_ID;
                    var decimalCol = td_ID.split("_-_")[td_ID.split("_-_").length-2].substring(2);

                    var td_Value = td_String.substring(td_String.lastIndexOf("_-_") + 1, td_String.length);
                    td_Value = td_Value.replace('-_', '');
                    td_Value = Number(td_Value);

                    if(_chart.pt_DecimalObj && !$.isEmptyObject(_chart.pt_DecimalObj)){
                        if(_chart.pivotCust_Selected_Data.length > 1){
                            td_Value = td_Value.toFixed(_chart.pt_DecimalObj[decimalCol]);
                        }else{
                            td_Value = td_Value.toFixed(Object.values(_chart.pt_DecimalObj)[0]);
                        }
                    }else if(draw.checkFloat(td_Value)){
                        td_Value = td_Value.toFixed(2);
                    }

                    if(_chart.subTotal_Row == 'yes' && _chart.pivotCust_Selected_Row.length>1){
                        var temp_td_Id = td_ID.substring(0, td_ID.lastIndexOf('-'));
                        temp_td_Id = temp_td_Id.substring(0, temp_td_Id.lastIndexOf('_'));

                        var temp_row_Id = temp_td_Id.substring(0, temp_td_Id.indexOf('_-_'));
                        var row_Id = temp_row_Id + '_-_';
                        var temp_Col_Arr = temp_td_Id.split("_-_");
                        var col_Id = '';
                        if(_chart.pivotCust_Selected_Data.length>1){
                            var Col_length = temp_Col_Arr.length - _chart.pivotCust_Selected_Column.length - 1;
                        }else{
                            var Col_length = temp_Col_Arr.length - _chart.pivotCust_Selected_Column.length;
                        }
                        for(var i = Col_length; i<temp_Col_Arr.length; i++){
                            col_Id += temp_Col_Arr[i] + '_-_';
                        }
                        col_Id = col_Id.substring(0, col_Id.lastIndexOf('_-_'));
                        var R_SubTotal_Key = '';
                        for(var i=0; i<_chart.pivotCust_Selected_Row.length-1; i++){
                            R_SubTotal_Key += _chart._container + '_' + 'SubRow_-_';
                        }

                        R_Sub_Row_Arr.push(temp_row_Id);
                        R_Sub_Col_Arr.push(col_Id);
                        R_Sub_Val_Arr.push(td_Value);

                        if(index == _chart.Row_Col_Key_Arr.length-1 ){
                            for(var i=0; i<R_Sub_Row_Arr.length; i++){
                                var temp_Sub_Key = R_Sub_Row_Arr[i] + "_-_" + R_SubTotal_Key + R_Sub_Col_Arr[i] + '_-_' ;
                                if(_chart.sTotal_RowObj[temp_Sub_Key] == undefined){
                                    _chart.sTotal_RowObj[temp_Sub_Key] = parseFloat(R_Sub_Val_Arr[i]);
                                }else{
                                    _chart.sTotal_RowObj[temp_Sub_Key] += parseFloat(R_Sub_Val_Arr[i]);
                                }
                            }
                        }
                    }

                    if(_chart.subTotal_Column == 'yes' && _chart.pivotCust_Selected_Column.length>1){
                        if(_chart.pivotCust_Selected_Data.length>1){
                            var temp_td_Id = td_ID.substring(0, td_ID.lastIndexOf('-'));
                            temp_td_Id = temp_td_Id.substring(0, temp_td_Id.lastIndexOf('_'));

                            var temp_Row_Arr = temp_td_Id.split('_-_');
                            var row_Arr = [], temp_Row = '', temp_Col_Arr = [], col_Arr = [], temp_Col = '';
                            temp_Row_Arr.forEach(function(d,i){
                                if(i < _chart.pivotCust_Selected_Row.length){
                                    temp_Row += d + '_-_';
                                    if(i == (_chart.pivotCust_Selected_Row.length-1)){
                                        row_Arr.push(temp_Row);
                                    }
                                }else{
                                    temp_Col += d + '_-_';
                                    if(i == (temp_Row_Arr.length-1)){
                                        temp_Col_Arr.push(temp_Col);
                                    }
                                }
                            });
                            temp_Col_Arr.forEach(function(d){
                                var temp = d.substring(0, d.lastIndexOf('-'));
                                temp = temp.substring(0, temp.lastIndexOf('_'));
                                col_Arr.push(temp);
                            });
                            var col_First_ID = col_Arr[0].substring(0, col_Arr[0].indexOf('_-_'));
                            var col_Last_ID = col_Arr[0].substring(col_Arr[0].lastIndexOf('_-_'));
                            col_Last_ID = col_Last_ID.substring(3);
                            var col_Mid_ID = '';
                            for(var i=0; i < _chart.pivotCust_Selected_Column.length-1; i++){
                                col_Mid_ID += _chart._container + '_' + 'SubColumn_-_';
                            }
                            var temp_Obj_Key = row_Arr + col_First_ID + '_-_' + col_Mid_ID + col_Last_ID + '_-_';
                            if(_chart.sTotal_MultiColObj[temp_Obj_Key] == undefined){
                                _chart.sTotal_MultiColObj[temp_Obj_Key] = parseFloat(td_Value);
                            }else{
                                _chart.sTotal_MultiColObj[temp_Obj_Key] += parseFloat(td_Value);
                            }
                        }else{
                            var temp_td_Id = td_ID.substring(0, td_ID.lastIndexOf('-'));
                            temp_td_Id = temp_td_Id.substring(0, temp_td_Id.lastIndexOf('_'));
                            var temp_Arr = temp_td_Id.split("_-_");
                            var temp_R_C_ID = '';
                            temp_Arr.forEach(function(d,i){
                                if(i <= _chart.pivotCust_Selected_Row.length){
                                    temp_R_C_ID += d + '_-_';
                                }
                            });
                            var temp_Sub_Col_Key = '';
                            for(var i=0; i<_chart.pivotCust_Selected_Column.length-1; i++){
                                temp_Sub_Col_Key += _chart._container + '_' + 'SubColumn_-_';
                            }
                            C_Sub_Row_Col_Arr.push(temp_R_C_ID);
                            C_Sub_Val_Arr.push(td_Value);
                            if(index == _chart.Row_Col_Key_Arr.length-1 ){
                                for(var i=0; i<C_Sub_Row_Col_Arr.length ;i++){
                                    var tempKey = C_Sub_Row_Col_Arr[i] + temp_Sub_Col_Key;
                                    if(_chart.sTotal_SingleColObj[tempKey] == undefined){
                                        _chart.sTotal_SingleColObj[tempKey] = parseFloat(C_Sub_Val_Arr[i]);
                                    }else{
                                        _chart.sTotal_SingleColObj[tempKey] += parseFloat(C_Sub_Val_Arr[i]);
                                    }
                                }
                            }
                        }
                    }

                    if((_chart.subTotal_Row == 'yes'&&_chart.pivotCust_Selected_Row.length>1) && (_chart.subTotal_Column == 'yes'&&_chart.pivotCust_Selected_Column.length>1)){
                        if(_chart.pivotCust_Selected_Data.length>1){
                            if(index == _chart.Row_Col_Key_Arr.length-1 ){
                                $.each(_chart.sTotal_RowObj, function(k, v){
                                    var tempSub_Single_Id = k.substring(0, k.lastIndexOf('_-_'));
                                    var subID_Single_Arr = tempSub_Single_Id.split('_-_');
                                    var row_Length = _chart.pivotCust_Selected_Row.length;
                                    var col_Length = _chart.pivotCust_Selected_Column.length;
                                    var sub_Row_Id = '';
                                    subID_Single_Arr.forEach(function(d,i){
                                        if(i<row_Length+1){
                                            sub_Row_Id += d + '_-_';
                                        }
                                    });
                                    var sub_Col_Id = '';
                                    for(var i=0; i<col_Length-1; i++){
                                        sub_Col_Id += _chart._container + '_' + 'SubColumn_-_';
                                    }
                                    var sub_Row_Col_Key = sub_Row_Id + sub_Col_Id;
                                    _chart.pivotCust_Selected_Data.forEach(function(d){
                                        sub_Row_Col_Key = sub_Row_Id + sub_Col_Id + d + '_-_';
                                        if(k.includes(sub_Row_Id) &&  k.includes(d) ){
                                            if(_chart.sTotal_Row_Col_Multi_Obj[sub_Row_Col_Key] == undefined){
                                                _chart.sTotal_Row_Col_Multi_Obj[sub_Row_Col_Key] = parseFloat(v);
                                            }else{
                                                _chart.sTotal_Row_Col_Multi_Obj[sub_Row_Col_Key] += parseFloat(v);
                                            }
                                        }
                                    });
                                });
                            }
                        }else{
                            if(index == _chart.Row_Col_Key_Arr.length-1 ){
                                $.each(_chart.sTotal_RowObj, function(k, v){
                                    var tempSub_Single_Id = k.substring(0, k.lastIndexOf('_-_'));
                                    var subID_Single_Arr = tempSub_Single_Id.split('_-_');
                                    var row_Length = _chart.pivotCust_Selected_Row.length;
                                    var col_Length = _chart.pivotCust_Selected_Column.length;
                                    var sub_Row_Id = '';
                                    subID_Single_Arr.forEach(function(d,i){
                                        if(i<row_Length+1){
                                            sub_Row_Id += d + '_-_';
                                        }
                                    });
                                    var sub_Col_Id = '';
                                    for(var i=0; i<col_Length-1; i++){
                                        sub_Col_Id += _chart._container + '_' + 'SubColumn_-_';
                                    }
                                    var sub_Row_Col_Key = sub_Row_Id + sub_Col_Id;
                                    if(k.includes(sub_Row_Id)){
                                        if(_chart.sTotal_Row_Col_Single_Obj[sub_Row_Col_Key] == undefined){
                                            _chart.sTotal_Row_Col_Single_Obj[sub_Row_Col_Key] = parseFloat(v);
                                        }else{
                                            _chart.sTotal_Row_Col_Single_Obj[sub_Row_Col_Key] += parseFloat(v);
                                        }
                                    }
                                });
                            }
                        }
                    }

                    if(_chart.grandTotal_Row == 'yes'){
                        var grd_Row_Key = '';
                        for(var i=0; i<_chart.pivotCust_Selected_Row.length; i++){
                            grd_Row_Key += _chart._container + '_' + 'GrandRow_-_';
                        }
                        _chart.data_ColID.forEach(function(e){
                            if(d.includes(e)){
                                var tempKey = grd_Row_Key + e;
                                if(_chart.gTotal_RowObj[tempKey] == undefined){
                                    _chart.gTotal_RowObj[tempKey] = parseFloat(td_Value);
                                }else{
                                    _chart.gTotal_RowObj[tempKey] += parseFloat(td_Value);
                                }
                            }
                        });
                    }

                    if(_chart.grandTotal_Column == 'yes'){
                        if(_chart.pivotCust_Selected_Data.length > 1){
                            _chart.data_RowID.forEach(function(e){
                                if(temp_TD_ID.includes(e)){
                                    _chart.pivotCust_Selected_Data.forEach(function(a){
                                        var tempKey = e + _chart.grand_Row_Col_Key + _chart._container + '_' + a + '_-_';
                                        var tempStr = temp_TD_ID;
                                        tempStr = tempStr.substr(0, tempStr.lastIndexOf('_-_'));
                                        tempStr = tempStr.substr(tempStr.lastIndexOf('_-_'));
                                        tempStr = tempStr.substr(3);
                                        if(_chart.gTotal_ColObj[tempKey] == undefined){
                                            var tempp_key = _chart._container + '_' + a;
                                            if(!tempStr.localeCompare(tempp_key)){
                                                _chart.gTotal_ColObj[tempKey] = parseFloat(td_Value);
                                            }
                                        }else{
                                            if(!tempStr.localeCompare(tempp_key)){
                                                _chart.gTotal_ColObj[tempKey] += parseFloat(td_Value);
                                            }
                                        }
                                    });
                                }
                            });
                        }else{
                            _chart.data_RowID.forEach(function(e){
                                if(d.includes(e)){
                                    if(_chart.gTotal_ColObj[e] == undefined){
                                        _chart.gTotal_ColObj[e] = parseFloat(td_Value);
                                    }else{
                                        _chart.gTotal_ColObj[e] += parseFloat(td_Value);
                                    }
                                }
                            });
                        }
                    }

                    var regex = /([^a-zA-Z0-9])/g;
                    var sub_String = '\\$1';
                    td_ID = td_ID.replace(regex, sub_String);
                    $("#" + td_ID).html(td_Value);
                    $("#" + td_ID).attr('title', td_Value);
                });
            }






            _chart.Pivot_AssignValue_SubTotal_Row = function(){
                if(_chart.subTotal_Row == 'yes' && _chart.pivotCust_Selected_Row.length>1){
                    $.each(_chart.sTotal_RowObj, function(k, v){
                        v = Number(v);
                        if(draw.checkFloat(v)){
                            v = parseFloat(v).toFixed(2);
                        }
                        var td_ID = k;
                        var regex = /([^a-zA-Z0-9])/g;
                        var sub_String = '\\$1';
                        td_ID = td_ID.replace(regex, sub_String);
                        $("#" + td_ID).html(v);
                        $("#" + td_ID).attr('title', v);
                        $('#' + td_ID).addClass('row_Col_SubTotal_Data');
                    });
                }
            }






            _chart.Pivot_AssignValue_SubTotal_Col = function(){
                if(_chart.subTotal_Column == 'yes' && _chart.pivotCust_Selected_Column.length>1){
                    if(_chart.pivotCust_Selected_Data.length>1){
                        $.each(_chart.sTotal_MultiColObj, function(k, v){
                            v = Number(v);
                            if(draw.checkFloat(v)){
                                v = parseFloat(v).toFixed(2);
                            }
                            var td_ID = k;
                            var regex = /([^a-zA-Z0-9])/g;
                            var sub_String = '\\$1';
                            td_ID = td_ID.replace(regex, sub_String);
                            $("#" + td_ID).html(v);
                            $("#" + td_ID).attr('title', v);
                            $('#' + td_ID).addClass('row_Col_SubTotal_Data');
                        });
                    }else{
                        $.each(_chart.sTotal_SingleColObj, function(k, v){
                            v = Number(v);
                            if(draw.checkFloat(v)){
                                v = parseFloat(v).toFixed(2);
                            }
                            var td_ID = k;
                            var regex = /([^a-zA-Z0-9])/g;
                            var sub_String = '\\$1';
                            td_ID = td_ID.replace(regex, sub_String);
                            $("#" + td_ID).html(v);
                            $("#" + td_ID).attr('title', v);
                            $('#' + td_ID).addClass('row_Col_SubTotal_Data');
                        });
                    }
                }
            }






            _chart.Pivot_AssignValue_SubTotal_Row_Col = function(){
                if(_chart.pivotCust_Selected_Data.length>1){
                    $.each(_chart.sTotal_Row_Col_Multi_Obj, function(k, v){
                        v = Number(v);
                        if(draw.checkFloat(v)){
                            v = parseFloat(v).toFixed(2);
                        }
                        var td_ID = k;
                        var regex = /([^a-zA-Z0-9])/g;
                        var sub_String = '\\$1';
                        td_ID = td_ID.replace(regex, sub_String);
                        $("#" + td_ID).html(v);
                        $("#" + td_ID).attr('title', v);
                        $('#' + td_ID).addClass('row_Col_Total_SubTotal_Data');
                        $('#' + td_ID).addClass('row_Col_SubTotal_Data');
                    });
                }else{
                    $.each(_chart.sTotal_Row_Col_Single_Obj, function(k, v){
                        v = Number(v);
                        if(draw.checkFloat(v)){
                            v = parseFloat(v).toFixed(2);
                        }
                        var td_ID = k;
                        var regex = /([^a-zA-Z0-9])/g;
                        var sub_String = '\\$1';
                        td_ID = td_ID.replace(regex, sub_String);
                        $("#" + td_ID).html(v);
                        $("#" + td_ID).attr('title', v);
                        $('#' + td_ID).addClass('row_Col_Total_SubTotal_Data');
                        $('#' + td_ID).addClass('row_Col_SubTotal_Data');
                    });
                }
            }






            _chart.Pivot_AssignValue_GrandTotal_Row = function(){
                if(_chart.grandTotal_Row == 'yes'){
                    var grandTotal_Row_ID = '';
                    for(var i=0; i<_chart.pivotCust_Selected_Row.length; i++){
                        grandTotal_Row_ID += _chart._container + '_' + 'GrandRow_-_';
                    }
                    $.each(_chart.gTotal_RowObj, function(k, v){
                        v = Number(v);
                        if(draw.checkFloat(v)){
                            v = parseFloat(v).toFixed(2);
                        }
                        var td_ID = k;
                        var regex = /([^a-zA-Z0-9])/g;
                        var sub_String = '\\$1';
                        td_ID = td_ID.replace(regex, sub_String);
                        $("#" + td_ID).html(v);
                        $("#" + td_ID).attr('title', v);
                        $('#' + td_ID).addClass('row_Col_GrandTotal_Data');
                    });
                }
            }





            _chart.Pivot_AssignValue_GrandTotal_Col = function(){
                if(_chart.grandTotal_Column == 'yes'){
                    if(_chart.pivotCust_Selected_Data.length > 1){
                        $.each(_chart.gTotal_ColObj, function(k, v){
                            v = Number(v);
                            if(draw.checkFloat(v)){
                                v = parseFloat(v).toFixed(2);
                            }
                            var td_ID = k;
                            var regex = /([^a-zA-Z0-9])/g;
                            var sub_String = '\\$1';
                            td_ID = td_ID.replace(regex, sub_String);
                            $("#" + td_ID).html(v);
                            $("#" + td_ID).attr('title', v);
                            $('#' + td_ID).addClass('row_Col_GrandTotal_Data');
                        });
                    }else{
                        var grandTotal_Col_ID = '';
                        for(var i=0; i<_chart.pivotCust_Selected_Column.length; i++){
                            grandTotal_Col_ID += _chart._container + '_' + 'GrandColumn_-_';
                        }
                        $.each(_chart.gTotal_ColObj, function(k, v){
                            v = Number(v);
                            if(draw.checkFloat(v)){
                                v = parseFloat(v).toFixed(2);
                            }
                            var td_ID = k + grandTotal_Col_ID;
                            var regex = /([^a-zA-Z0-9])/g;
                            var sub_String = '\\$1';
                            td_ID = td_ID.replace(regex, sub_String);
                            $("#" + td_ID).html(v);
                            $("#" + td_ID).attr('title', v);
                            $('#' + td_ID).addClass('row_Col_GrandTotal_Data');
                        });
                    }
                }
            }






            _chart.Pivot_AssignValue_GrandTotal_Row_Col = function(){
                if(_chart.grandTotal_Row == 'yes' && _chart.grandTotal_Column == 'yes'){
                    if(_chart.pivotCust_Selected_Data.length > 1){
                        var grand_Row_Key_Multi = '', grdTotal_Row_Col_Obj_Multi = {}, grand_RC_Key_Arr_Multi = [];
                        for(var i=0; i<_chart.pivotCust_Selected_Row.length; i++){
                            grand_Row_Key_Multi += _chart._container + '_' + 'GrandRow_-_';
                        }
                        _chart.pivotCust_Selected_Data.forEach(function(d){
                            var temp = _chart.grand_Row_Col_Key + _chart._container + '_' + d + '_-_';
                            grand_RC_Key_Arr_Multi.push(temp);
                        });
                        $.each(_chart.gTotal_ColObj, function(k,v){
                            grand_RC_Key_Arr_Multi.forEach(function(d){
                                if(k.includes(d)){
                                    _chart.pivotCust_Selected_Data.forEach(function(e){
                                        if(k.includes(e)){
                                            var temp_Grd_Key_Multi = grand_Row_Key_Multi + _chart.grand_Row_Col_Key + _chart._container + '_' + e + '_-_';
                                            if(grdTotal_Row_Col_Obj_Multi[temp_Grd_Key_Multi] == undefined){
                                                grdTotal_Row_Col_Obj_Multi[temp_Grd_Key_Multi] = v;
                                            }else{
                                                grdTotal_Row_Col_Obj_Multi[temp_Grd_Key_Multi] += v;
                                            }
                                        }
                                    });
                                }
                            });
                        });
                        $.each(grdTotal_Row_Col_Obj_Multi, function(k, v){
                            v = Number(v);
                            if(draw.checkFloat(v)){
                                v = parseFloat(v).toFixed(2);
                            }
                            var td_ID = k;
                            var regex = /([^a-zA-Z0-9])/g;
                            var sub_String = '\\$1';
                            td_ID = td_ID.replace(regex, sub_String);
                            $("#" + td_ID).html(v);
                            $("#" + td_ID).attr('title', v);
                            $('#' + td_ID).addClass('row_Col_GrandTotal_Data');
                            $('#' + td_ID).addClass('rowCol_GrandTotal_Data');
                        });
                    }else{
                        var grand_Row_Col_Key_Single = '', grdTotal_Row_Col_Obj_Single = {};
                        for(var i=0; i<_chart.pivotCust_Selected_Row.length; i++){
                            grand_Row_Col_Key_Single += _chart._container + '_' + 'GrandRow_-_';
                        }
                        for(var i=0; i<_chart.pivotCust_Selected_Column.length; i++){
                            grand_Row_Col_Key_Single += _chart._container + '_' + 'GrandColumn_-_';
                        }
                        $.each(_chart.gTotal_RowObj, function(k, v){
                            if(grdTotal_Row_Col_Obj_Single[grand_Row_Col_Key_Single] == undefined){
                                grdTotal_Row_Col_Obj_Single[grand_Row_Col_Key_Single] = v;
                            }else{
                                grdTotal_Row_Col_Obj_Single[grand_Row_Col_Key_Single] += v;
                            }
                        });
                        $.each(grdTotal_Row_Col_Obj_Single, function(k, v){
                            v = Number(v);
                            if(draw.checkFloat(v)){
                                v = parseFloat(v).toFixed(2);
                            }
                            var td_ID = k;
                            var regex = /([^a-zA-Z0-9])/g;
                            var sub_String = '\\$1';
                            td_ID = td_ID.replace(regex, sub_String);
                            $("#" + td_ID).html(v);
                            $("#" + td_ID).attr('title', v);
                            $('#' + td_ID).addClass('row_Col_GrandTotal_Data');
                            $('#' + td_ID).addClass('rowCol_GrandTotal_Data');
                        });
                    }
                }
            }






            _chart.Pivot_CalculateValue_Sub_Grand_Total_Row_Col = function(){
                _chart.sub_Grd_Single_Row_Obj = {};
                _chart.sub_Grd_Single_Col_Obj = {};
                _chart.sub_Grd_Multi_Row_Obj = {};
                _chart.sub_Grd_Multi_Col_Obj = {};

                if(_chart.pivotCust_Selected_Data.length>1){
                    if(_chart.grandTotal_Row == 'yes' && _chart.subTotal_Row == 'yes' && _chart.subTotal_Column == 'yes'){
                        $.each(_chart.sTotal_Row_Col_Multi_Obj, function(k,v){
                            var Row_Multi_Sub_Grd_Td_Id = k;
                            Row_Multi_Sub_Grd_Td_Id = Row_Multi_Sub_Grd_Td_Id.substring(0, Row_Multi_Sub_Grd_Td_Id.lastIndexOf('_-_'));
                            var Row_Multi_Sub_Grd_Td_Id_Arr = Row_Multi_Sub_Grd_Td_Id.split('_-_');
                            var Row_Multi_Row = '';
                            for(var i=0; i<_chart.pivotCust_Selected_Row.length; i++){
                                Row_Multi_Row += _chart._container + '_' + 'GrandRow_-_';
                            }
                            var Row_Multi_Col_Header = '';
                            Row_Multi_Sub_Grd_Td_Id_Arr.forEach(function(d,i){
                                if(i == _chart.pivotCust_Selected_Row.length){
                                    Row_Multi_Col_Header = d;
                                }
                            });
                            var Row_Multi_Col = '';
                            for(var i=0; i<_chart.pivotCust_Selected_Column.length-1; i++){
                                Row_Multi_Col += _chart._container + '_' + 'SubColumn_-_';
                            }
                            var Row_Multi_Col_Data = '';
                            _chart.pivotCust_Selected_Data.forEach(function(d,i){
                                Row_Multi_Col_Data = _chart._container + '_' + d;
                                var Row_Multi_Obj_Key = Row_Multi_Row + Row_Multi_Col_Header + '_-_' + Row_Multi_Col + Row_Multi_Col_Data + '_-_';
                                if(k.includes(d)){
                                    if(_chart.sub_Grd_Multi_Row_Obj[Row_Multi_Obj_Key] == undefined){
                                        _chart.sub_Grd_Multi_Row_Obj[Row_Multi_Obj_Key] = parseFloat(v);
                                    }else{
                                        _chart.sub_Grd_Multi_Row_Obj[Row_Multi_Obj_Key] += parseFloat(v);
                                    }
                                }
                            });
                        });
                    }

                    if(_chart.grandTotal_Column == 'yes' && _chart.subTotal_Row == 'yes' && _chart.subTotal_Column == 'yes'){
                        $.each(_chart.sTotal_Row_Col_Multi_Obj, function(k,v){
                            var Col_Multi_Sub_Grd_Td_Id = k;
                            Col_Multi_Sub_Grd_Td_Id = Col_Multi_Sub_Grd_Td_Id.substring(0, Col_Multi_Sub_Grd_Td_Id.lastIndexOf('_-_'));
                            var Col_Multi_Sub_Grd_Td_Id_Arr =  Col_Multi_Sub_Grd_Td_Id.split('_-_');
                            var Col_S_Grd_Multi_Row_Header = '';
                            Col_Multi_Sub_Grd_Td_Id_Arr.forEach(function(d,i){
                                if(i==0){
                                    Col_S_Grd_Multi_Row_Header = d;
                                }
                            });
                            var Col_S_Grd_Multi_Row = '';
                            for(var i=0; i<_chart.pivotCust_Selected_Row.length-1; i++){
                                Col_S_Grd_Multi_Row += _chart._container + '_' + 'SubRow_-_';
                            }
                            var Col_S_Grd_Multi_Col = '';
                            for(var i=0; i<_chart.pivotCust_Selected_Column.length; i++){
                                Col_S_Grd_Multi_Col += _chart._container + '_' + 'GrandColumn_-_';
                            }
                            var Col_S_Grd_Multi_Data = '';
                            _chart.pivotCust_Selected_Data.forEach(function(d){
                                Col_S_Grd_Multi_Data = _chart._container + '_' + d + '_-_';
                                var Col_S_Grd_Multi_Key = Col_S_Grd_Multi_Row_Header + '_-_' + Col_S_Grd_Multi_Row + Col_S_Grd_Multi_Col + Col_S_Grd_Multi_Data;
                                if(k.includes(d)){
                                    if(_chart.sub_Grd_Multi_Col_Obj[Col_S_Grd_Multi_Key] == undefined){
                                        _chart.sub_Grd_Multi_Col_Obj[Col_S_Grd_Multi_Key] = parseFloat(v);
                                    }else{
                                        _chart.sub_Grd_Multi_Col_Obj[Col_S_Grd_Multi_Key] += parseFloat(v);
                                    }
                                }
                            });
                        });
                    }
                }else{
                    if(_chart.grandTotal_Row == 'yes' && _chart.subTotal_Row == 'yes' && _chart.subTotal_Column == 'yes'){
                        $.each(_chart.sTotal_Row_Col_Single_Obj, function(k,v){
                            var Row_S_Grd_Td_Id = k;
                            Row_S_Grd_Td_Id = Row_S_Grd_Td_Id.substring(0, Row_S_Grd_Td_Id.lastIndexOf('_-_'));
                            var Row_S_Grd_Td_Id_Arr =  Row_S_Grd_Td_Id.split('_-_');
                            var Row_S_Grd_Single_Row = '';
                            for(var i=0; i<_chart.pivotCust_Selected_Row.length; i++){
                                Row_S_Grd_Single_Row += _chart._container + '_' + 'GrandRow_-_';
                            }
                            var Row_S_Grd_Single_Col = '';
                            for(var i=0; i<_chart.pivotCust_Selected_Column.length-1; i++){
                                Row_S_Grd_Single_Col += _chart._container + '_' + 'SubColumn_-_';
                            }
                            var Row_S_Grd_Single_Col_Header = '';
                            Row_S_Grd_Td_Id_Arr.forEach(function(d,i){
                                if(i == _chart.pivotCust_Selected_Row.length){
                                    Row_S_Grd_Single_Col_Header = d;
                                }
                            });
                            var Row_S_Grd_Single_Key = Row_S_Grd_Single_Row + Row_S_Grd_Single_Col_Header + '_-_' + Row_S_Grd_Single_Col;
                            if(_chart.sub_Grd_Single_Row_Obj[Row_S_Grd_Single_Key] == undefined){
                                _chart.sub_Grd_Single_Row_Obj[Row_S_Grd_Single_Key] = parseFloat(v);
                            }else{
                                _chart.sub_Grd_Single_Row_Obj[Row_S_Grd_Single_Key] += parseFloat(v);
                            }
                        });
                    }

                    if(_chart.grandTotal_Column == 'yes' && _chart.subTotal_Row == 'yes' && _chart.subTotal_Column == 'yes'){
                        $.each(_chart.sTotal_Row_Col_Single_Obj, function(k,v){
                            var Col_S_Grd_Td_Id = k;
                            Col_S_Grd_Td_Id = Col_S_Grd_Td_Id.substring(0, Col_S_Grd_Td_Id.lastIndexOf('_-_'));
                            var Col_S_Grd_Td_Id_Arr =  Col_S_Grd_Td_Id.split('_-_');
                            var Col_S_Grd_Single_Col_Header = '';
                            Col_S_Grd_Td_Id_Arr.forEach(function(d,i){
                                if(i == 0){
                                    Col_S_Grd_Single_Col_Header = d;
                                }
                            });
                            var Col_S_Grd_Single_Row = '';
                            for(var i=0; i<_chart.pivotCust_Selected_Row.length-1; i++){
                                Col_S_Grd_Single_Row += _chart._container + '_' + 'SubRow_-_';
                            }
                            var Col_S_Grd_Single_Col = '';
                            for(var i=0; i<_chart.pivotCust_Selected_Column.length; i++){
                                Col_S_Grd_Single_Col += _chart._container + '_' + 'GrandColumn_-_';
                            }
                            var Col_S_Grd_Single_Key = Col_S_Grd_Single_Col_Header + '_-_' + Col_S_Grd_Single_Row + Col_S_Grd_Single_Col;
                            if(_chart.sub_Grd_Single_Col_Obj[Col_S_Grd_Single_Key] == undefined){
                                _chart.sub_Grd_Single_Col_Obj[Col_S_Grd_Single_Key] = parseFloat(v);
                            }else{
                                _chart.sub_Grd_Single_Col_Obj[Col_S_Grd_Single_Key] += parseFloat(v);
                            }
                        });
                    }
                }
            }






            _chart.Pivot_AssignValue_Sub_Grand_Total_Row_Col = function(){
                if(_chart.pivotCust_Selected_Data.length>1){
                    if(_chart.grandTotal_Row == 'yes' && _chart.subTotal_Row == 'yes' && _chart.subTotal_Column == 'yes'){
                        $.each(_chart.sub_Grd_Multi_Row_Obj, function(k, v){
                            v = Number(v);
                            if(draw.checkFloat(v)){
                                v = parseFloat(v).toFixed(2);
                            }
                            var td_ID = k;
                            var regex = /([^a-zA-Z0-9])/g;
                            var sub_String = '\\$1';
                            td_ID = td_ID.replace(regex, sub_String);
                            $("#" + td_ID).html(v);
                            $("#" + td_ID).attr('title', v);
                            $('#' + td_ID).addClass('row_Col_SubTotal_Data');
                            $('#' + td_ID).addClass('row_Col_Total_SubTotal_Data');
                        });
                    }
                    if(_chart.grandTotal_Column == 'yes' && _chart.subTotal_Row == 'yes' && _chart.subTotal_Column == 'yes'){
                        $.each(_chart.sub_Grd_Multi_Col_Obj, function(k, v){
                            v = Number(v);
                            if(draw.checkFloat(v)){
                                v = parseFloat(v).toFixed(2);
                            }
                            var td_ID = k;
                            var regex = /([^a-zA-Z0-9])/g;
                            var sub_String = '\\$1';
                            td_ID = td_ID.replace(regex, sub_String);
                            $("#" + td_ID).html(v);
                            $("#" + td_ID).attr('title', v);
                            $('#' + td_ID).addClass('row_Col_SubTotal_Data');
                            $('#' + td_ID).addClass('row_Col_Total_SubTotal_Data');
                        });
                    }
                }else{
                    if(_chart.grandTotal_Row == 'yes' && _chart.subTotal_Row == 'yes' && _chart.subTotal_Column == 'yes'){
                        $.each(_chart.sub_Grd_Single_Row_Obj, function(k, v){
                            v = Number(v);
                            if(draw.checkFloat(v)){
                                v = parseFloat(v).toFixed(2);
                            }
                            var td_ID = k;
                            var regex = /([^a-zA-Z0-9])/g;
                            var sub_String = '\\$1';
                            td_ID = td_ID.replace(regex, sub_String);
                            $("#" + td_ID).html(v);
                            $("#" + td_ID).attr('title', v);
                            $('#' + td_ID).addClass('row_Col_SubTotal_Data');
                            $('#' + td_ID).addClass('row_Col_Total_SubTotal_Data');
                        });
                    }
                    if(_chart.grandTotal_Column == 'yes' && _chart.subTotal_Row == 'yes' && _chart.subTotal_Column == 'yes'){
                        $.each(_chart.sub_Grd_Single_Col_Obj, function(k, v){
                            v = Number(v);
                            if(draw.checkFloat(v)){
                                v = parseFloat(v).toFixed(2);
                            }
                            var td_ID = k;
                            var regex = /([^a-zA-Z0-9])/g;
                            var sub_String = '\\$1';
                            td_ID = td_ID.replace(regex, sub_String);
                            $("#" + td_ID).html(v);
                            $("#" + td_ID).attr('title', v);
                            $('#' + td_ID).addClass('row_Col_SubTotal_Data');
                            $('#' + td_ID).addClass('row_Col_Total_SubTotal_Data');
                        });
                    }
                }
            }






            _chart.Pivot_Create_RunningTotal = function(){
                if(_chart.runningTotal_Column == 'yes'){
                    var tempObj = {};
                    for(var i=0; i<_chart.data_RowID.length; i++){
                        var tempKey = 0;
                        for(var k=0; k<_chart.data_ColID.length; k++){
                            var className = _chart._container + 'PT_' + i + '_' + k;
                            var runVal = $("." + className).text();
                            if(runVal.length == 0){
                                runVal = 0;
                            }
                            tempObj[className] = parseInt(runVal);
                        }
                    }
                    for(var i=0; i<_chart.data_ColID.length; i++){
                        var tempKey = 0;
                        $.each(tempObj, function(k,v){
                            var key = k.split('_')[2];
                            if(parseInt(key) == i){
                                var tempVal = parseInt(v) + parseInt(tempKey);
                                tempKey = tempVal;
                                if(v != 0){
                                    $('.' + k).html(tempVal);
                                    $("." + k).attr('title', tempVal);
                                }
                            }
                        });
                    }
                }else if(_chart.runningTotal_Row == 'yes'){
                    for(var i=0; i<_chart.data_RowID.length; i++){
                        var tempKey = 0;
                        for(var k=0; k<_chart.data_ColID.length; k++){
                            var className = _chart._container + 'PT_' + i + '_' + k;
                            var runVal = $("." + className).text();
                            if(runVal.length == 0){
                                runVal = 0;
                            }
                            var tempVal = parseInt(runVal) + parseInt(tempKey);
                            tempKey = tempVal;
                            if(runVal != 0){
                                $('.' + className).html(tempVal);
                                $("." + className).attr('title', tempVal);
                            }
                        }
                    }
                }
            }

// Custom Pivot Function End




// Custom Pivot Render_Function Start
            _chart.render = function () {
                _chart.Pivot_Create_JsonData();
                _chart.Pivot_Create_RowColHeader();

                var pt_Cust_Row_Col_Arr = _chart.pivotCust_Selected_Row.concat(_chart.pivotCust_Selected_Column);
                var pt_Cust_Dimension = [];
                $.each(_chart._axisConfig.checkboxModelDimension, function(k, v){
                    pt_Cust_Dimension.push(v.columnName);
                });
                var pt_Cust_Dim_Arr =  [];
                pt_Cust_Row_Col_Arr.forEach(function(d){
                    if(pt_Cust_Dimension.includes(d)){
                        pt_Cust_Dim_Arr.push(d);
                    }
                });
                var pt_Cust_Dim_Mea_Arr = Object.values(_chart._axisConfig.checkboxModelMeasure).concat(Object.values(_chart._axisConfig.checkboxModelDimension));
                var groupParams=[];

                if (pt_Cust_Dim_Arr.length > 0){
                    pt_Cust_Dim_Mea_Arr.forEach(function (d) {
                        if (d.type === "custom" && !$.isEmptyObject(draw.parseFormula(d.formula))) {
                            var columnName = d.columnName;
                            if(d.formula){
                                var formula = draw.reduceMultilevelFormula(d.formula);
                                if(formula){
                                    groupParams.push({formula:formula,columnName:columnName,columnObject:d});
                                    calculateServer.processExpression(formula, "", columnName,d,draw.getAccessToken(),_chart.metadataId,"",pt_Cust_Dim_Arr);
                                }
                            }

                        }
                    });
                }

                var rowPivotObj = {
                    "dummyDimensionObj" : _chart.dummyDimObj,
                    "rowAttribute" : _chart.pivotCust_Selected_Row,
                    "sessionId" : draw.getAccessToken(),
                    "reportId" : _chart.id,
                    "metadataId" : _chart.metadataId,
                    "exclude_Row" : _chart.exclude_Row,
                    "reOrder_Row" : _chart.reOrder_Row,
                };

                var colPivotObj = {
                    "dummyDimensionObj" : _chart.dummyDimObj,
                    "columnAttribute" : _chart.pivotCust_Selected_Column,
                    "sessionId" : draw.getAccessToken(),
                    "reportId" : _chart.id,
                    "metadataId" : _chart.metadataId,
                    "exclude_Col" : _chart.exclude_Col,
                    "reOrder_Col" : _chart.reOrder_Col,
                };

                var dataPivotObj = {
                    "dummyDimensionObj" : _chart.dummyDimObj,
                    "rowAttribute" : _chart.pivotCust_Selected_Row,
                    "columnAttribute" : _chart.pivotCust_Selected_Column,
                    "dataAttribute" : _chart.pivotCust_Selected_Data_Type,
                    "sessionId" : draw.getAccessToken(),
                    "reportId" : _chart.id,
                    "metadataId" : _chart.metadataId,
                    "exclude_Row" : _chart.exclude_Row,
                    "exclude_Col" : _chart.exclude_Col,
                };

                var Row_Response = $.post(draw.nodeApiUrl + "/drawPivot_Row", rowPivotObj).done(function(data){
                    _chart.dataGroupRow = data;
                }).fail(function(response){
                    $("#error_"+draw._container+" h5").html("Save the dashboard & restart server");
                    $("#error_"+draw._container).show();
                });

                var Column_Response = $.post(draw.nodeApiUrl + "/drawPivot_Column", colPivotObj).done(function(data){
                    _chart.dataGroupCol = data;
                }).fail(function(response){
                    $("#error_"+draw._container+" h5").html("Save the dashboard & restart server");
                    $("#error_"+draw._container).show();
                });

                var Data_Response = $.post(draw.nodeApiUrl + "/drawPivot_Data", dataPivotObj).done(function(data){
                    _chart.ptCustTableData_Arr = data;
                }).fail(function(response){
                    $("#error_"+draw._container+" h5").html("Save the dashboard & restart server");
                    $("#error_"+draw._container).show();
                });

                var rowColumnDataCreation = new Promise(function(resolve,reject){
                    var flag=0;
                    Row_Response.then(function(){
                        flag++;
                        if(flag==3){
                            resolve();
                        }
                    });
                    Column_Response.then(function(){
                        flag++;
                        if(flag==3){
                            resolve();
                        }
                    });
                    Data_Response.then(function(){
                        flag++;
                        if(flag==3){
                            resolve();
                        }
                    });
                });

                rowColumnDataCreation.then(function(){
                    _chart.Pivot_Create_RowData();
                    _chart.Pivot_Create_ColData();
                    _chart.Pivot_Create_TableDataWithID();

                    _chart.Pivot_Create_Data();
                    _chart.Pivot_Create_TableForPivotData();
                    _chart.Pivot_Create_MainTableForPivot();

                    _chart.Pivot_AssignValue_MainTable();

                    _chart.Pivot_AssignValue_SubTotal_Row();
                    _chart.Pivot_AssignValue_SubTotal_Col();
                    _chart.Pivot_AssignValue_SubTotal_Row_Col();

                    _chart.Pivot_AssignValue_GrandTotal_Row();
                    _chart.Pivot_AssignValue_GrandTotal_Col();
                    _chart.Pivot_AssignValue_GrandTotal_Row_Col();

                    _chart.Pivot_CalculateValue_Sub_Grand_Total_Row_Col();
                    _chart.Pivot_AssignValue_Sub_Grand_Total_Row_Col();

                    if(_chart.runningTotal_Row == 'yes' || _chart.runningTotal_Column == 'yes'){
                        _chart.Pivot_Create_RunningTotal();
                    }
                    draw.reportLoadedCountCheck(_chart);
                });
            }
// Custom Pivot Render_Function End
            draw.chartRegistry.register(_chart);
            draw.globalRegistry.register(_chart);
            draw.globalRegistry.syncLocalRegistries(_chart.id,"sketch");

            _chart.render();
            return _chart;
        }
// customized PivotTable End













// Box plote Start
        draw._BoxPlotChart = function () {
            var containerId = "chart-" + draw._container;
            var axisConfig = draw._axisConfig;
            var chartConfigKey = draw._chartConfig.key;
            var measures = draw._measure;
            var dimension = draw._dimension;
            var dataFormat = draw._dataFormat;
            var operator = draw._aggregate;
            // return eChartServer.boxPlotChart(containerId);

            $.post(draw.nodeApiUrl+"/draw", {"axisConfig":axisConfig,"chartType":chartConfigKey,"sessionId":draw.getAccessToken(),"reportId":containerId}).done(function(data){
                draw.ExcludeKeyData = data;
                draw.chartLoading_Hide();
                var labels = [];
                $.each(measures, function (key, value) {
                    labels.push(key);
                });
                var metadataId=axisConfig['queryObj'];
                return eChartServer.setSessionId(draw.getAccessToken()).setMetadataId(metadataId).boxPlotChart(containerId).operator(operator).dataFormat(dataFormat).dataSetLabels(labels).measures(measures).dimension(dimension).group(data).tKey(totalKeys).render();
            }).fail(function(response) {
                draw.errorNotification("BoxPlot");
            });
        }
// Box plote End





//Funnel Start
        draw._funnelChart = function (chart) {
            var containerId="chart-" + draw._container;
            if(chart!=undefined){
                var axisConfig={};
                axisConfig['aggregateModel']=chart._operator;
                axisConfig['checkboxModelMeasure']=chart._measures;
                axisConfig['checkboxModelDimension']=chart._dimension;
                if(Object.keys(chart._dataFormateVal).length==0 && chart._sort!=undefined){
                    axisConfig['dataFormat']={};
                    axisConfig['dataFormat']['sort']=chart._sort;
                    axisConfig['dataFormat']['sortMeasure']=chart._sortMeasure;
                }else{
                    axisConfig['dataFormat']=chart._dataFormateVal;
                }
                axisConfig['queryObj']=chart._metadataId;
                axisConfig['startPagination']=0;
                axisConfig['endPagination']=50;
                containerId = chart.id;
                chartConfigKey= "_funnelChart";
            }else{
                var axisConfig=draw._axisConfig;
            }
            var chartConfigKey=draw._chartConfig.key;
            var measures=draw._measure;
            var dimension=draw._dimension;
            var dataFormat=draw._dataFormat;
            var operator = draw._aggregate;
            var url="/draw";
            if(chart!=undefined){
                url="/reDraw";
            }
            $.post(draw.nodeApiUrl+url, {"axisConfig":axisConfig,"chartType":chartConfigKey,"sessionId":draw.getAccessToken(),"reportId":containerId}).done(function(data){
                var totalKeys=data['totalKey'];
                var getReportId=data['reportId'].split("-")[1];
                delete data['totalKey'];
                delete data['reportId'];
                draw.ExcludeKeyData = data;
                draw.chartLoading_Hide();
                var labels = [];
                $.each(measures, function (key, value) {
                    labels.push(key);
                });
                var metadataId=axisConfig['queryObj'];
                if(chart!=undefined){
                    var chartNew;
                    eChartServer.chartRegistry.list().forEach(function(d){
                        if(d.id=="chart-"+getReportId){
                            chartNew=d;
                        }
                    });
                    chart._group = data;
                    eChartServer.updateChart(chartNew,true);
                    eChartServer.highlightSelectedFilter(chart);
                    chartNew.chartInstance.options.sort=chart._sort;
                    chartNew.chartInstance.update();
                }else{
                    return eChartServer.setSessionId(draw.getAccessToken()).setMetadataId(metadataId).funnelChart(containerId).operator(operator).dataFormat(dataFormat).measures(measures).dimension(dimension).group(data).tKey(totalKeys).render();
                }
            }).fail(function(response) {
                draw.errorNotification("funnel");
            });
        };
//Funnel End











        draw.chartLoading_Show = function(){
            $(".loadingBar").show();
        }

        draw.chartLoading_Hide = function(){
            //$(".loadingBar").hide();
        }


        draw._text = function (text) {
            $("#chart-" + draw._container).append(text);
        };

        draw.reset = function () {
            return new _draw(dc, crossfilter);
        };

        draw.errorNotification=function (type) {
            demo.showNotification('danger',"Something went wrong on "+type+" chart restart server");
            $("#error_"+draw._container+" h5").html("Save the dashboard & restart server");
            $("#error_"+draw._container).show();
        }





        return draw;
    }
    this.sketchServer = _draw(dc, crossfilter);
})();