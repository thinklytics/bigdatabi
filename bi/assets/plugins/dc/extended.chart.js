/* global Chart */

// Chart Js With Crossfilter .....................//
// .........By Mitesh Panchal.....................//
(function () {

    function _eChart(Chart) {
        'use strict';

        var eChart = {
            VERSION: "1.0.0",
            constants: {
                DELAY: 4,
                NEGLIGIBLE_NUMBER: 1e-10,
                DEFAULT_COLOR: "rgba(255, 99, 132, 0)",
                INACTIVE_COLOR: "rgba(255, 99, 132, 0)",
                LABEL: "label"
            }
        };
         Array.prototype.isNull = function (){
             return this.join().replace(/,/g,'').length === 0;
         };
        eChart.checkNaN=function(arr){
            var flag=true;
            arr.forEach(function(v){
                if(v && !isNaN(v) && v!=0 ){
                   flag=false;
                }
            });
            return flag;
        }
        //Utility Functions ...
        eChart.getIndexOfObj = function (array, attr, value) {
            for (var i = 0; i < array.length; i++) {
                if (array[i].hasOwnProperty(attr) && array[i][attr] === value) {
                    return i;
                }
            }
            return -1;
        };

        eChart.monthWiseSortObject = {
            January: {},
            February: {},
            March: {},
            April: {},
            May: {},
            June: {},
            July: {},
            August: {},
            September: {},
            October: {},
            November: {},
            December: {}
        };

        eChart.chartRegistry = (function () {

            var _chartMap = {};
            var _chartAttributes = {};
            var _colorObject = {};
            return {
                /**
                 * Determine if a given chart instance resides in the registry.
                 *
                 * @method has
                 *
                 * @returns {Boolean}
                 */
                has: function (chart) {

                    if (_chartMap[chart.id]) {
                        return true;
                    }

                    return false;
                },

                /**
                 * Add given chart instance .
                 *
                 */
                register: function (chart) {


                    if(sketch.chartRegistry.has(chart)){
                        sketch.chartRegistry.deregister(chart);
                    }
                    _chartMap[chart.id] = chart;
                    // if(sketch.chartRegistry.getRegisterdIds().indexOf(chart.id)!=-1){
                    //    sketch.chartRegistry.deregister(chart);
                    //  }
                },

                /**
                 *
                 * @method registerAttributes
                 * @memberof eCharts.chartRegistry
                 *
                 */

                registerColor: function (chart) {
                    if (_chartAttributes[chart.id]) {
                        _chartAttributes[chart.id]['colors'] = chart._colorSelection;
                    } else {
                        _chartAttributes[chart.id] = {colors: chart._colorSelection};
                    }
                },
                /**
                 *
                 * @method registerPublicViewColor
                 * @memberof eCharts.registerPublicViewColor
                 *
                 */
                registerPublicViewColor: function (id, color) {
                    _colorObject[id] = color;
                    _chartAttributes = _colorObject;
                },
                registerDataOrder: function (chart) {
                    var sort=chart._sort;
                    if (_chartAttributes[chart.id]) {
                        _chartAttributes[chart.id] = {order: sort};
                    } else {
                        _chartAttributes[chart.id] = {order: sort};
                    }

                },

                /**
                 *
                 * @method registerAttributes
                 * @memberof eCharts.chartRegistry
                 *
                 */
                registerAllAttributes: function (chartAttributes) {
                    _chartAttributes = chartAttributes;

                },
                /**
                 *
                 * @method registerAttributes
                 * @memberof eCharts.chartRegistry
                 *
                 */

                listAttributes: function () {
                    return _chartAttributes;
                },

                /**
                 * Remove given chart instance
                 */
                deregister: function (chart) {

                    delete _chartMap[chart.id];

                },

                /**
                 *
                 * @method clear
                 * @memberof eCharts.chartRegistry
                 *
                 */
                clear: function () {

                    // delete _chartMap;

                },
                /**
                 *
                 * @method list
                 * @memberof eCharts.chartRegistry
                 *
                 */
                list: function () {
                    var list = [];
                    $.each(_chartMap, function (key, value) {
                        list.push(value);
                    });
                    return list;
                }

            };
        })();
        //Xaxes Define
        eChart.renderxAxes = function (type, fontSize, dataFormateVal) {
            if(/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
                fontSize=36;
            }
            if ((type === "date" || type === "datetime") && dataFormateVal.xAxis === undefined) {
                return [{
                    stacked: true,
                    ticks: {
                        fontSize: fontSize,
                        autoSkip: true,
                        maxTicksLimit: 10
                    },
                    type: 'time',
                    unit: 'day',
                    unitStepSize: 1,
                    time: {
                        displayFormats: {
                            'day': 'MMM DD'
                        }
                    }
                }];
            } else {
                return [{
                    stacked: true,
                    ticks: {
                        fontSize: fontSize,
                        autoSkip: true,
                        maxTicksLimit: 10
                    }
                }];
            }
        }
        eChart.renderyAxes = function (chart) {
            var fontSize=13;
            if(/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
                fontSize=36;
            }
            return [{
                //Label formate
                stacked: false,
                ticks: {
                    beginAtZero: true,
                    // Return an empty string to draw the tick line but hide the tick label
                    // Return `null` or `undefined` to hide the tick line entirely
                    userCallback: function (value, index, values) {
                        // Convert the number to a string and splite the string every 3 charaters from the end

                        // Convert the array to a string and format the output
                        //value = value.join('.');
                        if (chart._yAxisPrefix) {
                            value = value.toString();
                            value = value.split(/(?=(?:...)*$)/);
                            return _chart._yAxisPrefix + ' ' + value;
                        } else {
                            return value;
                        }
                    },
                    fontSize:fontSize,
                    fontFamily: 'sans-serif'
                }
            }];
        }
        eChart.registerChart = function (chart) {
            eChart.chartRegistry.register(chart);
        };

        eChart.registerColor = function (chart) {
            eChart.chartRegistry.registerColor(chart);
        }
        eChart.registerDataOrder = function (chart) {
            eChart.chartRegistry.registerDataOrder(chart);
        }

        eChart.getRunTotal=function(finalData,key){

            var sumKeeper=0;

            finalData.forEach(function(val){
                sumKeeper+=val.value['sumIndex'];
                val.value['runTotal']=sumKeeper;

            });

            return finalData;
        }
        //eChart.colors = ["#0251D3", "#DE3B00", "#FF8700", "#FFB500", "#B9E52F", "#2CAD00", "#30CB71", "#33AEB1", "#A8577B", "#EE94C9", "#F6AA54", "#F7E65A", "#B6F5AC", "#25F1AA", "#C6F0D7", "#A7DBE2", "#219FD7", "#0251D3", "#863CFF", "#E436BB", "#EA5081", "#8B5011", "#000000", "#AAAAAA", "#9FDEF1", "#ED90C7", "#6197E2", "#C2C4E3", "#DFC0ED", "#DDCFA4", "#2A5D78", "#EEEEEE"];
        //  eChart.colors =["rgba(0,206,209,.5)", "rgba(0,139,139,.5)", "rgba(100,149,237,.5)","rgba(238,130,238,.5)", "rgba(112,128,144,.5)", "rgba(248, 208, 83,.5)","rgba(245, 87, 83,.5)", "rgba(230, 230, 230,.5)", "rgba(98, 98,98,.5)"]
        //  eChart.colors = ['rgba(54, 162, 235, 0.5)',
        //       'rgba(255, 99, 132, 0.5)',
        //       'rgba(255, 206, 86, 0.5)',
        //       'rgba(75, 192, 192, 0.5)',
        //       'rgba(153, 102, 255, 0.5)',
        //       'rgba(255, 159, 64, 0.5)'];

        eChart.colors = [
            'rgb(255, 99, 132)',
            'rgb(54, 162, 235)',
            'rgb(75, 192, 192)',
            'rgb(255, 159, 64)',
            'rgb(255, 205, 86)',
            'rgb(153, 102, 255)',
            'rgb(201, 203, 207)',
            '#4dc9f6',
            '#f67019',
            '#f53794',
            '#537bc4',
            '#acc236',
            '#166a8f',
            '#00a950',
            '#58595b',
            '#8549ba',
            /*'#4676AC',
             '#FE8B00',
             '#F1464D',
             '#63BAB4',
             '#37A846',
             '#F1CD14',
             '#B972A3',
             '#FF95A3',
             '#A2745A',
             '#BCB0AB',*/
            /*'#0091D5','#EA6A47','#A5D8DD','#1C4E80','#7E909A','#202020','#F1F1F1',*/
            '#0073BA', '#FF7800', '#00A815', '#E70000', '#9E59C2', '#935346'
        ];
        var highlights = ["#1f88c5", "#aed8e9", "#ff8f3f", "#ffbb78",
            "#2ca02c", "#98df8a", "#d62728", "#ff9896", "#9467bd",
            "#c5b0d5", "#8c564b", "#c49c94"];

        // Used to remove already drawn canvas.........................
        eChart.clearContainer = function (id) {
            $(id).html("");
        };
        //Generate legend
        eChart.generateLegend = function (id, chart) {
            try {
                var instance = chart.chartInstance;
                //$("#"+id+"> ul").append(instance.generateLegend());
                document.getElementById(id + "select").innerHTML = instance.generateLegend();
                chart._datasetsLength = instance.config.data.datasets.length;
                chart._hiddenDatasets = 0;
                //Select
                $("#" + id + "select > ul > li").on("click", function (e) {
                    var index = $(this).index();
                    if (chart.type == "pieChart") {
                        $(this).toggleClass("strike");
                        var curr = instance.config.data.datasets[0]._meta[instance.id].data[index];
                        curr.hidden = !curr.hidden;
                    } else {
                        var curr = instance.config.data.datasets[index]._meta[instance.id];

                        if (curr.hidden == null) {

                            curr.hidden = false;
                        }
                        if (!curr.hidden && chart._datasetsLength - 1 === chart._hiddenDatasets) {

                        } else {

                            curr.hidden = !curr.hidden;
                            $(this).toggleClass("strike");
                            if (curr.hidden) {
                                chart._hiddenDatasets++;
                            } else {

                                chart._hiddenDatasets--;
                            }
                        }
                    }
                    instance.update();
                });
                $("#" + id + "> ul > li").on("click", function (e) {
                    var index = $(this).index();
                    if (chart.type == "pieChart") {
                        $(this).toggleClass("strike");
                        var curr = instance.config.data.datasets[0]._meta[instance.id].data[index];
                        curr.hidden = !curr.hidden;
                    } else {
                        var curr = instance.config.data.datasets[index]._meta[instance.id];

                        if (curr.hidden == null) {

                            curr.hidden = false;
                        }
                        if (!curr.hidden && chart._datasetsLength - 1 === chart._hiddenDatasets) {

                        } else {

                            curr.hidden = !curr.hidden;
                            $(this).toggleClass("strike");
                            if (curr.hidden) {
                                chart._hiddenDatasets++;
                            } else {

                                chart._hiddenDatasets--;
                            }
                        }
                    }
                    instance.update();
                });
            } catch (e) {

            }
        }
        // used to find add the canvas
        function addCanvas(_chart) {




//          var width = "651px";
            var height = "329px";


            if (_chart.type == 'barChart' || _chart.type == 'composite') {

                var width = "875px";
            } else if (_chart.type == 'horizontalBar') {

                var width = "850px";
            } else {

                var width = "651px";
            }



            if (_chart._dataFormateVal && _chart._dataFormateVal.canvas) {
                if(_chart._dataFormateVal.canvas.width){
                    width=_chart._dataFormateVal.canvas.width;
                }
                if(_chart._dataFormateVal.canvas.height){
                    height=_chart._dataFormateVal.canvas.height;
                }
                var element = $("<canvas id='canvas" + _chart.id + "' width='" + width + "' height='" + height + "'></canvas>")
                    .appendTo($("#" + _chart.id));
            } else {

                var element = $("<canvas id='canvas" + _chart.id + "' width='" + width + "' height='" + height + "'></canvas>")
                    .appendTo($("#" + _chart.id));
            }



            _chart.element = element;
            return element;

        }

        eChart.getCanvasContext = function (chart, callback) {

            var element = addCanvas(chart);

            setTimeout(function () {
                callback(element[0].getContext("2d"));
            }, eChart.constants.DELAY);
        };

        eChart.sortData = function (order) {

            eChart._activeChart.sort(order);
        }

        eChart.updateAttributes = function (_chart) {
            var attributes = eChart.chartRegistry.listAttributes();
            if (!$.isEmptyObject(attributes)) {
                if (attributes[_chart.id]) {
                    if (attributes[_chart.id].colors)
                        _chart._colorSelection = attributes[_chart.id].colors;
                    if (attributes[_chart.id].order)
                        _chart._sort = attributes[_chart.id].order;
                }
            }
        }



        eChart.bindEvents = function (chart) {
            chart.element.on('click', callback);
            /*document.addEventListener('touchstart',function(e){
             alert("hello");
             });*/

            function callback(evt) {

                var filter = chart.getClickedElementLabel(evt);

                //chart.colorChange(filter);

                if (filter != null) {
                    try {
                        var pathname = window.location.href;
                        if (pathname[pathname.length - 1] != 1) // View page
                        {
                            if (chart.type == "barChart" || chart.type == "lineChart" || chart.type == "composite" || chart.type == "areaChart" || chart.type == "horizontalBar") {
                                //    $('.loadingBar').show();
                                var bodyOffsets = document.body.getBoundingClientRect();
                                var scroll = $(".sideSubBarWidth2").scrollTop();
                                var leftSideWidth = $(".sideSubBarWidth1").width() + $(".sidebar").width() + 20;
                                var tempX = evt.pageX - bodyOffsets.left - leftSideWidth + $(".sideSubBarWidth2").scrollLeft();
                                var tempY = evt.pageY + scroll - 55;
                                $('.colorPalette').css('left', tempX);
                                $('.colorPalette').css('top', tempY);
                                $(".colorPalette").show();
                            } else {

                                eChart.addFilter(chart, filter);
                                eChart.applyFiltersOnData(chart, filter, function () {
                                    eChart.updateAll(chart);
                                });
                            }
                        } else {
                            eChart.addFilter(chart, filter);
                            eChart.applyFiltersOnData(chart, filter, function () {
                                eChart.updateAll(chart);
                            });
                        }
                    } catch ($e) {

                    }
                    eChart.activeChart(chart, filter);
                }
            }
        };
        eChart.activeChart = function (chart, filter) {
            eChart._activeChart = chart;
            eChart._activeDataset = filter;
        }
        eChart.addColor = function (color) {
            eChart._activeChart.colorChange(eChart._activeDataset, color);
            $(".colorPalette").hide();
        };
        eChart.applyFilter = function () {
            eChart.addFilter(eChart._activeChart, eChart._activeDataset);
            eChart.applyFiltersOnData(eChart._activeChart, eChart._activeDataset, function () {
                eChart.updateAll(eChart._activeChart);
            });
            $(".colorPalette").hide();
        };
        //Add Filters To Filter List
        eChart.addFilter = function (chart, filter) {
            chart.reset = false;
            if (!chart.filters) {
                chart.filters = [];

                chart.filters.push(filter);

            } else {

                var flag = false;
                var foundIndex = -1;
                chart.filters.forEach(function (prevFilter, index) {
                    if (prevFilter.label == filter.label && prevFilter.datasetLabel == filter.datasetLabel) {
                        flag = true;
                        foundIndex = index;
                    }
                });
                if (flag) {
                    chart.filters.splice(foundIndex, 1);
                } else {
                    chart.filters.push(filter);
                }



                if (chart.filters.length == 0) {

                    //  if (eChart.getIndexOfObj(chart.filters, eChart.constants.LABEL, filter.label) != -1 && eChart.getIndexOfObj(chart.filters, "datasetIndex", filter.datasetIndex) != -1) {
                    chart.reset = true;
                    chart.filters = false;
                    if (eChart.isGroupColorApplied(chart)) {
                        try{
                            chart._dataFormateVal['groupColorDimension'].filterAll();
                        }catch(e){

                        }

                    }

                    chart._dimension.filterAll();
                    //}
                }
                // if (eChart.getIndexOfObj(chart.filters, eChart.constants.LABEL, filter.label) == -1 || eChart.getIndexOfObj(chart.filters, "datasetIndex", filter.datasetIndex) == -1 ) {
                //     if (chart.filters)
                //         chart.filters.push(filter);

                // } else {

                //     if(chart.filters){
                //         var deleteIndex=0;
                //         for(var j=0;j<chart.filters.length;j++){
                //             if(filter.label==chart.filters[j].label && filter.datasetIndex==chart.filters[j].datasetIndex)
                //                 deleteIndex=j;

                //         }
                //         chart.filters.splice(deleteIndex, 1);
                //     }
                // }
            }

        };

        eChart.isFiltersApplied = function (_chart, flag) {
            if (!flag) {
                if (eChart._activeChart && eChart._activeChart.filters && eChart._activeChart.filters.length > 0)
                {
                    return true;
                } else {
                    return false;
                }
            }

            return true;
        }

        eChart.resetDataToInitialState = function (data, chart, key) {
            var newData = [];
            var labels = [];
            data.forEach(function (dataObj, i) {
                labels.push(dataObj.key);
                newData.push(dataObj.value[chart._operator[key].key]);


            });
            return {data: newData, labels: labels};
        }

        eChart.reConstructData = function (data, chart, key) {
            var newData = [];
            var labels = [];

            data.forEach(function (dataObj, i) {
                if (dataObj.value[chart._operator[key].key] != 0 && !isNaN(dataObj.value[chart._operator[key].key])) {
                    labels.push(dataObj.key);
                    newData.push(dataObj.value[chart._operator[key].key]);
                }

            });
            return {data: newData, labels: labels};
        }

        eChart.updateDataset = function (chart, data, key, index, flag) {

            var finalData = {};

            if (eChart.isFiltersApplied(chart, flag))
            {
                finalData = eChart.reConstructData(data, chart, key);

            } else {
                finalData = eChart.resetDataToInitialState(data, chart, key);

            }
            chart.chartInstance.config.data.labels = finalData["labels"];
            chart.chartInstance.config.data.datasets[index].data = finalData['data'];

            setTimeout(function () {
                var res = chart.id.split("-");
                eChart.generateLegend("left" + res[1], chart);
            }, 1);


        };
        eChart.getTopN=function(_chart,grpObj,key){

            if (!eChart.isGroupColorApplied(_chart)) {

                _chart._sortedGroup = grpObj.order(function (v) {

                    return (v[_chart._operator[key].key]);

                });
            } else {
                _chart._sortedGroup = grpObj.order(function (v) {
                    var totalSum = 0;
                    $.each(v.groupColor, function (key, val) {
                        totalSum += val;
                    });
                    return totalSum;
                });
            }


            return _chart._sortedGroup.top(_chart.topN);
        }
        eChart.updateChart = function (chart, flag) {
            if(chart.type!="bubbleChart"){
                var grp = chart._group;
                var index = 0;
                if (chart.type == 'maleFemaleChart') {
                    chart.updateDataset();
                    return;
                }
                if (!eChart.isGroupColorApplied(chart)) {
                    $.each(grp, function (key, group) {
                        var newData = [];
                        var data = [];
                        if (chart._sort) {
                            data = chart.getSortedData(group, key, chart._sort);

                         //   eChart.updateDataset(chart, data, key, index, flag);

                        }
                        else if(chart.topN){

                            data= eChart.getTopN(chart,group,key);

                    //        eChart.updateDataset(chart, data, key, index, flag);
                        }else {
                            data = group.all();
                            if (eChart.isDataFormatApplied(chart)) {
                                data = eChart.getDataByDateFormat(group, key, chart);
                            }

                        }
                        if(chart._operator[key].key=="runTotal"){
                            data=eChart.getRunTotal(data);
                        }
                        eChart.updateDataset(chart, data, key, index, flag);
                        index++;
                    });
                } else {

                    var tempDataList = {};
                    // if(chart.type=="lineChart")
                    // {
                    //     chart.dataFormate.groupColorDimension.filter(chart.dataFormate.groupColor);
                    // }

                    var dataAfterSort=[];
                    $.each(grp, function (key, group) {
                        var data = group.all();

                        if(chart._sort){
                            data = chart.getSortedData(group, key, chart._sort);
                        }else if(chart.topN){

                            data= eChart.getTopN(chart,group,key);



                        }
                        if (eChart.isDataFormatApplied(chart)) {
                            data = eChart.getDataByDateFormat(group, key, chart);
                        }

                        tempDataList = chart.updateDatasetByGroupColor(data, key);

                    });



                    chart._data.labels=chart._labels;
                    chart._data.datasets = tempDataList;
                    setTimeout(function () {
                        var res = chart.id.split("-");
                        eChart.generateLegend("left" + res[1], chart);
                    }, 1);
                }
            }else{
                chart.updateDataset();
            }

            chart.chartInstance.update();

        };

        eChart.updateAll = function (currentChart) {
            var idList = {};
            eChart.chartRegistry
                .list()
                .forEach(
                    function (chart) {
                        idList[chart.id] = true;
                        if (chart.id != currentChart.id) {
                            eChart.updateChart(chart, true);
                        } else {
                            try {
                                chart.highlightSelectedFilter();
                                chart.chartInstance.update();
                            } catch (e) {

                            }
                        }
                    });
            sketch.chartRegistry
                .list()
                .forEach(
                    function (_chart) {
                        if (!idList[_chart.id])
                            _chart.render();
                    });
            setTimeout(function(){
                $(".loadingBar").hide();
            },2000);

        };


        eChart.redrawEchartChartsOnly = function (id) {
            sketch.chartRegistry
                .list()
                .forEach(
                    function (_chart) {
                        if (_chart._container != id) {
                            _chart.render();
                        }
                    });
        }


        eChart.updateAllChartsColor = function () {
            var colorSelection = eChart.chartRegistry.listAttributes();

            eChart.chartRegistry.list().forEach(function (chart) {

                var labels = chart.getLabels();

                var colors = null;

                if (colorSelection[chart.id])
                    colors = colorSelection[chart.id].colors;

                if (chart.type != "lineChart" && chart.type != "areaChart" && colors != null) {
                    chart._data.datasets.forEach(function (dataset, dsIndex) {

                        var color = colors[dsIndex];

                        var tempData = [];
                        labels.forEach(function (filter, i) {
                            tempData.push(color);
                        });

                        chart._data.datasets[dsIndex].backgroundColor = tempData;
                        chart._data.datasets[dsIndex].borderColor = tempData;

                    });
                } else {
                    chart._data.datasets.forEach(function (dataset, dsIndex) {


                        var color = colors[dsIndex];
                        chart._data.datasets[dsIndex].backgroundColor = color;
                        chart._data.datasets[dsIndex].borderColor = color;

                    });


                }

                chart._colorSelection = colors;
                chart.chartInstance.update();

            });


        }

        //Function used to redraw chart on applying custom filters
        eChart.redrawAll = function () {

            var idList = {};
            eChart.chartRegistry.list().forEach(function (chart) {
                idList[chart.id] = true;
                eChart.updateChart(chart, true);
            });
            sketch.chartRegistry
                .list()
                .forEach(
                    function (_chart) {

                        _chart.render();
                        //  _chart.resetZoom();
                    });
            setTimeout(function () {
                dc.redrawAll();

                //  callback();
            }, 1000);


        };

        eChart.redrawChartsOnly = function () {
            eChart.chartRegistry.list().forEach(function (chart) {
                eChart.updateChart(chart, true);
            });
            setTimeout(function () {
                dc.redrawAll();
            }, 1000);
        };

        //chart data reset
        eChart.resetAll = function () {
            try {
                eChart.chartRegistry
                    .list()
                    .forEach(
                        function (chart) {
                            chart._dimension.filterAll();
                            if (eChart.isGroupColorApplied(chart)) {
                                try {
                                    chart._dataFormateVal['groupColorDimension'].filterAll();
                                } catch (e) {

                                }
                            }
                            chart.reset = true;
                            chart.highlightSelectedFilter();
                        });
                sketch.chartRegistry
                    .list()
                    .forEach(
                        function (_chart) {
                            _chart.filter = null;
                            try{
                                if(!_chart.type && _chart.type != 'Pivot'){
                                    _chart.createdDimension.filterAll();
                                }else{
                                    _chart.createdDimension.filterAll();
                                    $.each(_chart.ptCust_Dimension_Cross_Filter, function(k, v){
                                        v.filterAll();
                                    })
                                }
                            }catch(e){

                            }
                        });
                eChart.redrawAll();

            }catch(e){

            }
        }


        eChart.resetChart=function(id){
            try{
                sketch.chartRegistry
                    .list()
                    .forEach(function (_chart) {
                        if(_chart.id == id){
                            (function(){
                                var chart = _chart;
                                chart.render();
                            })();
                        }
                    });
            }catch(e){

            }
        }


        //function to redraw all periods
        eChart.redrawAllP = function () {
            eChart.chartRegistry.list().forEach(function (chart) {
                var grp = chart._group;
                var index = 0;
                $.each(grp, function (key, group) {
                    var newData = [];
                    var labels = [];

                    group.all().forEach(function (dataObj) {
                        labels.push(dataObj.key);
                        newData.push(dataObj.value[chart._operator[key].key]);
                    });

                    chart.chartInstance.config.data.labels = labels;

                    chart.chartInstance.config.data.datasets[index].data = newData;
                    index++;
                });
                chart.chartInstance.update();
            });
        };


        //Create the group mappings
        eChart.createGroupMappings = function (finalData, _chart) {
            var groupMapppings = {};
            var data = Object.assign([], finalData);
            var labels = [];
             data.forEach(function (d) {
                    labels.push(d.key);
                    if (d.value.groupColor != undefined) {
                        $.each(d.value.groupColor, function (k, val) {
                            if (groupMapppings[k]) {
                                groupMapppings[k][d.key] = val;
                            } else {
                                groupMapppings[k] = {};
                                groupMapppings[k][d.key] = val;
                            }
                        });
                    }
                });


            _chart._labels = labels;


            return groupMapppings;
        }


        // ----------------------Apply Filters On Data----------------------------------------------------------//

        eChart.applyFiltersOnData = function (chart, currentFilter, callback) {
            try {
                var filters = [];
                chart.filters.forEach(
                    function (filter) {
                        filters.push(filter.label);
                    }
                );

                if (eChart.isGroupColorApplied(chart)) {
                    var datasetFilters = [];
                    chart.filters.forEach(function (filter) {
                        datasetFilters.push(filter['datasetLabel']);
                    });
                    try{
                        chart._dataFormateVal['groupColorDimension'].filter(function (d) {
                            if (datasetFilters.indexOf(d) != -1 ) {
                                return true;

                            } else {
                                return false;
                            }
                        });
                    }catch(e){}

                }

                if (!chart.reset) {

                    chart._dimension.filter(function (d) {

                        if (filters.indexOf(d) != -1) {

                            return true;
                        } else {

                            return false;
                        }
                    });
                } else {
                    if (eChart.isGroupColorApplied(chart)) {
                        chart._dataFormateVal['groupColorDimension'].filterAll();
                    }
                    chart._dimension.filterAll();

                }

            } catch (e) {

            }
            setTimeout(function () {
                dc.redrawAll();
                callback();
            }, 4);

        };

        // ----------------------CHART DATA MANUPLATION FROM DIMENSION------------------------------------------//

        eChart.getDataInFormat = function (chart) {

            var colorsLength = colors.length;
            var grpObj = chart._group.all();
            var labels = [];
            var datasets = [];
            var data = [];
            var backgroundColor = [];
            var i = 0;

            grpObj.forEach(function (d) {
                labels.push(d.key);
                data.push(d.value[chart._operator]);
                backgroundColor.push(chart._colorSelection[i % colorsLength]);
                i++;
            });

            var dt = {
                data: data,
                backgroundColor: backgroundColor
            };

            datasets.push(dt);

            var dataInFormat = {
                labels: labels,
                datasets: datasets
            };

            return dataInFormat;
        };

        //Check if Group Color Applied
        eChart.isGroupColorApplied = function (_chart) {
            return _chart._dataFormateVal && _chart._dataFormateVal.groupColor;
        }


        //eChart create options for chart...................................................

        eChart.createOption = function (chart) {
            var fontSize = 13;
            if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
                fontSize = 36;
            }
            var option = {};
            option.responsive = true;
            option.maintainAspectRatio = false;
            option.legend = {
                display: false
            };
            option.legendCallback = function (chartInstance) {
                var html = "<ul>";
                if (chart._groupColor) {
                    $.each(chart._groupColor, function (key, value) {
                        if(chartInstance.config.data.labels.indexOf(key)!=-1)
                        html += '<li class=""><span style="background-color:' + value + '"></span>' + key + '</li>';
                    });
                } else {
                    var legend=[];
                    chartInstance.config.data.datasets.forEach(function(d){
                        if(!d.data.isNull() && !eChart.checkNaN(d.data)){
                            legend.push(d.label);
                        }
                    });

                    chartInstance.legend.legendItems.forEach(function (d) {
                        if(legend.indexOf(d.text)!=-1)
                        html += '<li class=""><span style="background-color:' + d.fillStyle + '"></span>' + d.text + '</li>';
                        //strokeStyle
                    });
                }
                html += "</ul>"
                return html;
            }
            option.title = {
                display: true,
                text: ''
            };
            // if(eChart.isGroupColorApplied(chart)){
            //      option.scales = {
            //              xAxes: chart.xAxes(),
            //              yAxes: [{
            //                  stacked: true
            //              }]
            //             };
            //  }



            if (chart.type == 'composite') {
                option.scales = {
                    xAxes: chart.xAxes(),
                    yAxes: [{
                        stacked: false,
                        ticks: {
                            beginAtZero: true,
                            userCallback: function (value, index, values) {
                                if (chart._yAxisPrefix) {
                                    value = value.toString();
                                    value = value.split(/(?=(?:...)*$)/);
                                    return chart._yAxisPrefix + ' ' + value;
                                } else {
                                    return value;
                                }
                            },
                            fontSize: fontSize,
                            fontFamily: 'sans-serif'
                        }
                    }]
                };

            } else if (chart.type == "horizontalBar") {
                if(chart._dataFormateVal && chart._dataFormateVal.canvas && Object.keys(chart._dataFormateVal.canvas).length){
                    option.responsive = false;
                    option.maintainAspectRatio = false;
                }
                option.scales = {
                    xAxes: chart.xAxes(),
                    yAxes: [{
                        stacked: true,
                        ticks: {
                            beginAtZero: true,
                            userCallback: function (value, index, values) {
                                if (chart._yAxisPrefix) {
                                    value = value.toString();
                                    value = value.split(/(?=(?:...)*$)/);
                                    return chart._yAxisPrefix + ' ' + value;
                                } else {
                                    return value;
                                }
                            },
                            fontSize: fontSize,
                            fontFamily: 'sans-serif'
                        }
                    }]
                };

            } else {
                option.scales = {
                    xAxes: chart.xAxes(),
                    yAxes: chart.yAxes(chart)
                };

            }

            if(eChart.isGroupColorApplied(chart)){
                option.scales = {
                    xAxes: chart.xAxes(),
                    yAxes: [{
                        stacked: true,
                        ticks: {
                            beginAtZero: true,
                            userCallback: function (value, index, values) {
                                if (chart._yAxisPrefix) {
                                    value = value.toString();
                                    value = value.split(/(?=(?:...)*$)/);
                                    return chart._yAxisPrefix + ' ' + value;
                                } else {
                                    return value;
                                }
                            },
                            fontFamily: 'sans-serif'
                        }
                    }]
                };
            }




            if (chart.type != "horizontalBar") {
                option.tooltips = {
                    position: 'nearest',
                    intersect: false,
                    enabled: false,
                    custom: function (tooltip) {
                        eChart.customTooltips(tooltip, chart, this)
                    }
                };
                option.pan = {
                    // Boolean to enable panning
                    enabled: false,
                    // Panning directions. Remove the appropriate direction to disable
                    // Eg. 'y' would only allow panning in the y direction
                    mode: 'x'
                };
                option.zoom = {
                    // Boolean to enable zooming
                    enabled: false,
                    /*drag: true,*/
                    sensitivity: 0,
                    // Zooming directions. Remove the appropriate direction to disable
                    // Eg. 'y' would only allow zooming in the y direction
                    mode: 'x',
                };
            } else {
                option.pan = {
                    // Boolean to enable panning
                    enabled: false,
                    // Panning directions. Remove the appropriate direction to disable
                    // Eg. 'y' would only allow panning in the y direction
                    mode: 'y'
                };
                option.zoom = {
                    // Boolean to enable zooming
                    enabled: false,
                    /*drag: true,*/
                    sensitivity: 0,
                    // Zooming directions. Remove the appropriate direction to disable
                    // Eg. 'y' would only allow zooming in the y direction
                    mode: 'y',
                };
                option.tooltips = {
                    mode: 'index',
                    intersect: true,
                    enabled: false,
                    custom: function (tooltip) {
                        eChart.customTooltips(tooltip, chart, this)
                    }
                };

            }
            return option;

        }


        eChart.decodeTimeLineObject = function () {
            return {
                time1: {
                    start: '2015-07-01',
                    end: '2016-07-01'
                },
                time2: {
                    start: '2016-07-01',
                    end: '2017-07-01'
                },
                time3: {
                    start: '2018-07-01',
                    end: '2019-07-01'
                }
            };
        }


        //Echart Parsing tooltips
        eChart.parseTooltip = function (format, data, tooltipItems) {
            try {
                var formatText = format.replace("{xLabel}", data.datasets[tooltipItems.datasetIndex].label);
                formatText = formatText.replace("{yLabel}", tooltipItems.yLabel);
                return formatText;
            } catch (e) {
                return "";
            }
        }


        //Function to sort data
        eChart.getSortedData = function (_chart, grpObj, key, order) {
            $(".colorPalette").hide();

            if(!eChart.isGroupColorApplied(_chart)){

                _chart._sortedGroup = grpObj.order(function (v) {

                    return (v[_chart._operator[key].key]);

                });
            }else{
                _chart._sortedGroup = grpObj.order(function (v) {
                    var  totalSum=0;
                    $.each(v.groupColor,function(key,val){
                        if(!isNaN(val))
                        totalSum+=val;
                    });

                    return totalSum;
                });
            }
            if (order == 'desc') {
                if(!_chart.topN){
                    return _chart._sortedGroup.top(Infinity);
                }else{
                    return   _chart._sortedGroup.top(_chart.topN);
                }

            } else {
                if(!_chart.topN){
                    return _chart._sortedGroup.top(Infinity).reverse();
                }else{
                    return   _chart._sortedGroup.top(_chart.topN).reverse();
                }

            }

        }

        eChart.updateSortData = function (_chart, order) {
            var grp = _chart._group;
            var index = 0;
            $.each(grp, function (key, group) {
                var newData = [];
                var labels = [];

                var data=eChart.getSortedData(_chart, group, key, order);

                if(!eChart.isGroupColorApplied(_chart)){
                    data.forEach(function (dataObj) {
                        labels.push(dataObj.key);
                        newData.push(dataObj.value[_chart._operator[key].key]);
                    });

                    _chart.chartInstance.config.data.labels = labels;

                    _chart.chartInstance.config.data.datasets[index].data = newData;
                    index++;
                }else{


                    var initialData = [];
                    _chart._labels=[];
                    data.forEach(function (dataObj,i) {
                        _chart._labels.push(dataObj.key);
                        initialData[i] = null;

                    });



                    var groupMappings = _chart.createGroupMappings(data, _chart);


                    var p = 0;
                    var datasets = [];


                    $.each(groupMappings, function (key, value) {
                        var data = Object.assign([], initialData);

                        $.each(value, function (k, v) {
                            var labelIndexMatched = _chart._labels.indexOf(k);
                            if (labelIndexMatched != -1) {
                                data[labelIndexMatched] = v;

                            }


                        });
                        var dt = {
                            label: key,
                            data: data,
                            backgroundColor: Array(data.length).fill(_chart._colorSelection[p % (_chart._colorSelection.length)]),
                            borderWidth: 1
                        };

                        //     _chart._labels=labels
                        datasets.push(dt);
                        p++;
                    });
                    _chart.chartInstance.config.data.datasets=datasets;
                    _chart.chartInstance.config.data.labels = _chart._labels;
                }

            });
            _chart.chartInstance.update();
        }
        //------------
        eChart.isDataFormatApplied = function (_chart) {
            if (_chart._dataFormateVal && _chart._dataFormateVal.xAxis) {
                return true;
            }
            return false;
        }
        eChart.getDataByDateFormat = function (grpObj, key, _chart) {
            if (_chart._dataFormateVal.xAxis == "MMMM") {
                var obj = Object.assign([], grpObj.all());
                var months = ["January", "February", "March", "April", "May", "June",
                    "July", "August", "September", "October", "November", "December"];
                function sortObject(obj) {
                    return obj.sort(function (a, b) {
                        return months.indexOf(a.key) - months.indexOf(b.key);
                    });
                }
                return sortObject(obj);
            } else if (_chart._dataFormateVal.xAxis == "MMM") {
                var obj = grpObj.all();
                var months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
                    "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
                function sortObject(obj) {
                    return obj.sort(function (a, b) {
                        return months.indexOf(a.key) - months.indexOf(b.key);
                    });
                }
                return sortObject(obj);
            }
            return grpObj.all();
        }
        //-------------Common Tooltip--------------------------------------
        eChart.processTooltipData = function (_chart) {
            _chart._tooltipData = {};
            var i = 0;

            $.each(_chart._group, function (key, value) {
                if (i == 0) {
                    _chart._tooltipData = {};
                    value.all().forEach(function (d) {
                        _chart._tooltipData[d.key] = d.value;
                    });
                }
                i++;
            });
        }

        //-------------Common Tooltip--------------------------------------
        eChart.processGroupData = function (data) {

            var processedData = Object.create({});
            data.forEach(function (d) {
                processedData[d.key] = d.value;
            });
            return processedData;
        }
        eChart.customTooltips = function (tooltip, _chart, instance) {

            // Tooltip Element
            var tooltipEl = document.getElementById('chartjs-tooltip');
            if (!tooltipEl) {
                tooltipEl = document.createElement('div');
                tooltipEl.id = 'chartjs-tooltip';
                tooltipEl.innerHTML = "<table ></table>"
                document.body.appendChild(tooltipEl);
            }
            var tooltipObj=_chart._dataFormateVal.tooltip;
            
            if(tooltipObj==undefined || tooltipObj==""){
                tooltipObj="{title}<br>{xLabel}:{yLabel}";
            }
            // Hide if no tooltip
            if (tooltip.opacity === 0) {
                tooltipEl.style.opacity = 0;
                return;
            }
            // Set caret Position
            tooltipEl.classList.remove('above', 'below', 'no-transform');
            if (tooltip.yAlign) {
                tooltipEl.classList.add(tooltip.yAlign);
            } else {
                tooltipEl.classList.add('no-transform');
            }
            function getBody(bodyItem) {
                return bodyItem.lines;
            }
            // Set Text
            if (tooltip.body) {
                var titleLines = tooltip.title || [];
                var bodyLines = tooltip.body.map(getBody);
                var tooltipHtml = "";
                var innerHtml = "";
                var innerHtml = '<thead>';
                if(titleLines.length){
                    titleLines.forEach(function (title) {
                        //innerHtml += '<tr><th>' + title + '</th></tr>';+"<br>"
                        tooltipObj=tooltipObj.replace(/{title}/g,title);
                    });
                }else{
                    tooltipObj=tooltipObj.replace(/{title}/g,"");
                }
                var tooltipIndex=[];
                if(tooltip.body.length>1){
                    tooltip.body.forEach(function(d,index){
                        var tempTooltip=d.lines[0].split(":");
                        if(!isNaN(tempTooltip[1])){
                            tooltipIndex.push(index);
                        }
                    });
                    var tooltipArr=tooltip.body[tooltipIndex[0]].lines[0].split(":");
                    var groupTooltip="";
                    if(tooltipIndex.length>1){
                        //groupTooltip +="<br>";
                        tooltipIndex.forEach(function(d,index){
                            if(index!=0)
                            groupTooltip += tooltip.body[d].lines[0]+"<br>";
                        });
                    }
                }
                else
                    var tooltipArr=tooltip.body[0].lines[0].split(":");
                tooltipObj=tooltipObj.replace(/{xLabel}/g,tooltipArr[0]);
                tooltipObj=tooltipObj.replace(/{yLabel}/g,tooltipArr[1]);
                if (_chart._dataFormateVal && _chart._dataFormateVal.tooltipSelector && tooltipObj.trim() != "") {
                    titleLines.forEach(function (title) {
                        //if(_chart._dataFormateVal.tooltipSelector.length)
                        //innerHtml +='<tr><td>'+title+'</td></tr>';
                        var d = _chart._tooltipData[title];
                        _chart._dataFormateVal.tooltipSelector.forEach(function (p) {
                            if (tooltipHtml) {
                                if (p.dataKey == "Measure") {
                                    tooltipHtml = tooltipHtml.replace("<" + p.aggregate + "(" + p.columnName + ")" + ">", d["<" + p.aggregate + "(" + p.columnName + ")" + ">"]);
                                } else {
                                    tooltipHtml = tooltipHtml.replace("<" + p.columnName + ">", d["<" + p.columnName + ">"]);
                                }
                            } else {
                                if (p.dataKey == "Measure") {
                                    tooltipHtml = tooltipObj.replace("<" + p.aggregate + "(" + p.columnName + ")" + ">", d["<" + p.aggregate + "(" + p.columnName + ")" + ">"]);
                                } else {
                                    tooltipHtml = tooltipObj.replace("<" + p.columnName + ">", d["<" + p.columnName + ">"]);
                                }
                            }
                        });
                    });
                    if (tooltipHtml) {
                        tooltipHtml = tooltipHtml.replace(/\n/g, "<br>");
                    }
                    if(_chart._dataFormateVal.tooltipSelector==undefined || _chart._dataFormateVal.tooltipSelector.length==0){
                        tooltipHtml= tooltipObj;
                    }
                    if (tooltipObj) {
                        innerHtml += tooltipHtml;
                    }
                    if(groupTooltip){
                        innerHtml += groupTooltip;
                    }
                } else {
                    /*var innerHtml = '<thead>';
                     titleLines.forEach(function (title) {
                     innerHtml += '<tr><th>' + title + '</th></tr>';
                     });
                     innerHtml += '</thead><tbody>';
                     bodyLines.forEach(function (body, i) {
                     if (!body[0].includes("NaN")) {
                     var colors = tooltip.labelColors[i];
                     var style = 'background:' + colors.backgroundColor;
                     style += '; border-color:' + colors.borderColor;
                     style += '; border-width: 2px';
                     var span = '<span class="chartjs-tooltip-key" style="' + style + '"></span>';
                     innerHtml += '<tr><td>' + body + '</td></tr>';
                     }
                     });
                     innerHtml += '</tbody>';*/
                    innerHtml += tooltipObj;
                }
                var tableRoot = tooltipEl.querySelector('table');
                tableRoot.innerHTML = innerHtml;
            }

            var scroll_Top_Pos = $('#' + _chart.id).scrollTop();
            var scroll_Left_Pos = $('#' + _chart.id).scrollLeft();

            var offsets = $('#' + _chart.id).offset();
            var top = offsets.top;
            var left = offsets.left;

            if(_chart.type == 'horizontalBar' ){
                tooltipEl.style.top = parseFloat(top) + 20 - scroll_Top_Pos + parseFloat(tooltip.caretY) + 'px';
                tooltipEl.style.left = parseFloat(left) - scroll_Left_Pos + parseFloat(tooltip.caretX) + 'px';
            }else{
                tooltipEl.style.top = parseFloat(top) + 20 + parseFloat(tooltip.caretY) + 'px';
                tooltipEl.style.left = parseFloat(left) + parseFloat(tooltip.caretX) + 'px';
            }

                       
            // Display, position, and set styles for font
            tooltipEl.style.opacity = 1;
            //tooltipEl.style.left = position.left + tooltip.caretX + 'px';
            //tooltipEl.style.top = position.top + tooltip.caretY + 'px';
            tooltipEl.style.fontFamily = tooltip._fontFamily;
            tooltipEl.style.fontSize = tooltip.fontSize;
            tooltipEl.style.fontStyle = tooltip._fontStyle;
            tooltipEl.style.padding = tooltip.yPadding + 'px ' + tooltip.xPadding + 'px';
            if(/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
                tooltipEl.style.fontSize = 32;
                setTimeout(function(){
                    tooltipEl.style.opacity = 0;
                },2000);
            }

        };

        eChart.sortArray =function(obj) {
            var months = ["January", "February", "March", "April", "May", "June",
                "July", "August", "September", "October", "November", "December"];
            return obj.sort(function (a, b) {
                return months.indexOf(a) - months.indexOf(b);
            });
        }

        // -------------BAR CHART DRAW CODE-----------------------------------
        eChart.barChart = function (id) {
            var _chart = {};

            _chart.type = 'barChart';

            _chart.id = id;

            _chart._sort = null;

            _chart._colorSelection = $.extend([], eChart.colors);

            _chart.group = function (group) {
                _chart._group = group;
                _chart.data();
                return _chart;
            };


            _chart.dimension = function (dimension) {
                _chart._dimension = dimension;
                return _chart;
            };

            _chart.dataFormate = function (dataFormat) {
                _chart._dataFormateVal = dataFormat;
                _chart.setDataFormat();
                return _chart;
            };
            _chart.dataSetLabels = function (labels) {
                _chart._dataSetLabels = labels;
                return _chart;
            };


            _chart.sort = function (order) {
                _chart._sort = order;
                eChart.updateSortData(_chart, order);
                eChart.registerDataOrder(_chart);
            };
            _chart.xAxes = function () {
                if(/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
                    _chart._fontSize=36;
                }
                if ((_chart._dataType == "date" || _chart._fontSize == "datetime") && _chart._dataFormateVal.xAxis == undefined) {
                    return [{
                        stacked: true,
                        ticks: {
                            fontSize: _chart._fontSize,
                            autoSkip: true,
                            maxTicksLimit: 10,
                            fontFamily: "sans-serif"
                        },
                        type: 'time',
                        unit: 'day',
                        unitStepSize: 1,
                        time: {
                            displayFormats: {
                                'day': 'MMM DD'
                            }
                        }
                    }];
                } else {
                    return [{
                        stacked: true,
                        ticks: {
                            fontSize: _chart._fontSize,
                            autoSkip: true,
                            maxTicksLimit: 10,
                            fontFamily: "sans-serif"
                        }
                    }];
                }
            };
            _chart.yAxes=function(chart){
                return eChart.renderyAxes(chart);
            }
            _chart.dataType = function (type) {
                _chart._dataType = type;
            };
            _chart.fontSize = function (fontSize) {
                _chart._fontSize = fontSize
            };
            _chart.fontFamily = function (fontFamily) {
                _chart._fontFamily = fontFamily;
            }
            _chart.yAxisPrefix = function (yAxis) {

                if (yAxis) {
                    _chart._yAxisPrefix = yAxis;
                } else {
                    _chart._yAxisPrefix = "";
                }

            }
            _chart.setDataFormat = function () {

                if (_chart._dataFormateVal.yAxis) {
                    _chart.yAxisPrefix(_chart._dataFormateVal.yAxis);
                } else {
                    _chart.yAxisPrefix("");
                }
                if (_chart._dataFormateVal.fontSize) {
                    _chart.fontSize(_chart._dataFormateVal.fontSize);
                } else {
                    _chart.fontSize(12);
                }
                if(_chart._dataFormateVal.topN){
                    _chart.topN=_chart._dataFormateVal.topN;
                }
                _chart.groupColor = _chart._dataFormateVal.groupColor;

                _chart._dataType = _chart._dataFormateVal.dataType;

            }
            //---------------------------Group Color Start--------------------
            _chart.getDatasetByGroupColor = function (finalData, groupKey) {
                var groupMappings = _chart.createGroupMappings(finalData, _chart);

                var initialData = [];
                _chart._labels.forEach(function (d, i) {
                    initialData[i] = null;
                });


                var p = 0;
                var datasets = [];


                $.each(groupMappings, function (key, value) {
                    var data = Object.assign([], initialData);
                    var sumAdder=0;
                    $.each(value, function (k, v) {
                        var labelIndexMatched = _chart._labels.indexOf(k);

                        if (labelIndexMatched != -1) {
                            if(_chart._operator[Object.keys(_chart._operator)[0]].key=="runTotal"){

                                sumAdder+=v;
                                if (!(v <= 0))
                                    data[labelIndexMatched] = sumAdder;
                            }else{
                                if (!(v <= 0))
                                    data[labelIndexMatched] += v;

                            }
                        }


                    });
                    var dt = {
                        label: key,
                        data: data,
                        backgroundColor: Array(data.length).fill(_chart._colorSelection[p % (_chart._colorSelection.length)]),
                        borderWidth: 1
                    };
                    //     _chart._labels=labels
                    datasets.push(dt);
                    p++;
                });
                return datasets;
            }
            _chart.updateDatasetByGroupColor = function (finalData, groupKey) {

                var groupMappings = _chart.createGroupMappings(finalData, _chart);

                var labelsList = {};
                $.each(groupMappings, function (key, value) {
                    $.each(value, function (k, v) {



                        if (!(v == 0 || v < 0 || isNaN(v))) {

                            labelsList[k] = 1;


                        }


                    });
                });


                var initialData = [];
                var indexData = 0;
                var labelsListArray = [];

                $.each(labelsList, function (key, value) {
                    initialData[indexData] = null;
                    labelsListArray.push(key);
                    indexData++;
                });
                if(eChart.isDataFormatApplied(_chart) && (_chart._dataFormateVal.xAxis == "MMMM" || _chart._dataFormateVal.xAxis == "MMM")){
                    _chart._labels = eChart.sortArray(labelsListArray);
                }else{
                    _chart._labels = labelsListArray;
                }



                var p = 0;
                var datasets = [];


                $.each(groupMappings, function (key, value) {
                    var data = Object.assign([], initialData);
                    var sumAdder=0;
                    $.each(value, function (k, v) {
                        var labelIndexMatched = _chart._labels.indexOf(k);

                        if (labelIndexMatched != -1) {
                            if(_chart._operator[Object.keys(_chart._operator)[0]].key=="runTotal"){
                                sumAdder+=v;
                                if (!(v <= 0))
                                    data[labelIndexMatched] = sumAdder;
                            }else{
                                if (!(v <= 0))
                                    data[labelIndexMatched] += v;

                            }
                        }


                    });
                    var dt = {
                        label: key,
                        data: data,
                        backgroundColor: Array(data.length).fill(_chart._colorSelection[p % (_chart._colorSelection.length)]),
                        borderWidth: 1
                    };
                    //     _chart._labels=labels
                    datasets.push(dt);
                    p++;
                });

                return datasets;
            }
            //---------------------------Group Color End-----------------------

            _chart.getSortedData = function (grpObj, key, order) {
                return eChart.getSortedData(_chart, grpObj, key, order);
            }

            _chart.createGroupMappings = function (finalData, _chart) {
                _chart._groupMappings = eChart.createGroupMappings(finalData, _chart);

                return _chart._groupMappings;
            }
            _chart.getDataByGroup = function () {
                var groups = _chart._group;
                var datasets = [];
                $.each(groups, function (key, grpObj) {

                    var finalData;

                    if (!_chart._sort) {

                        finalData = grpObj.all();
                        if(_chart.topN){

                            finalData =  eChart.getTopN(_chart,grpObj,key);

                        }

                    } else {
                        finalData = _chart.getSortedData(grpObj, key,_chart.sort);
                    }
                    if (eChart.isDataFormatApplied(_chart)) {
                        finalData = eChart.getDataByDateFormat(grpObj, key, _chart);
                    }

                    datasets = _chart.getDatasetByGroupColor(finalData, key);
                });

                var dataInFormat = {
                    labels: _chart._labels,
                    datasets: datasets
                };
                return dataInFormat;
            }
            _chart.getNormalData = function () {

                var colorsLength = eChart.colors.length;
                var groups = _chart._group;

                var labels = [];
                var datasets = [];
                var i = 0;
                var j = 0;
                var index = 0;
                $.each(groups, function (key, grpObj) {
                    var data = [];
                    var backgroundColor = [];
                    var borderColor = [];
                    var hoverBorderColor = [];
                    var finalData;

//
                    if (!_chart._sort) {
                        finalData = grpObj.all();
                        if(_chart.topN){

                            finalData =  eChart.getTopN(_chart,grpObj,key);

                        }
                    } else {

                        finalData = _chart.getSortedData(grpObj, key, _chart._sort);
                    }

                    if (eChart.isDataFormatApplied(_chart)) {
                        finalData = eChart.getDataByDateFormat(grpObj, key, _chart);
                    }

                    if(_chart._operator[key].key=="runTotal"){
                             finalData=eChart.getRunTotal(finalData);
                    }

                    finalData.forEach(function (d) {
                        if (j === 0)
                            labels.push(d.key);
                        if (_chart._operator) {
                            data.push(d.value[_chart._operator[key].key]);
                        } else {
                            data.push(d.value);
                        }

                        backgroundColor.push(_chart._colorSelection[j]);
                        hoverBorderColor.push("#000");
                        i++;
                    });
                    var dt = {
                        label: _chart._dataSetLabels[j],
                        data: data,
                        backgroundColor: backgroundColor,
                        borderColor: backgroundColor,
                        /*hoverBorderColor: hoverBorderColor,*/
                        borderWidth: 1
                    };
                    datasets.push(dt);
                    j++;
                });
                var dataInFormat = {
                    labels: labels,
                    datasets: datasets
                };
                return dataInFormat;
            }
            _chart.getDataInFormat = function (order) {
                try {
                    if (_chart._dataFormateVal && _chart._dataFormateVal.tooltipSelector) {
                        eChart.processTooltipData(_chart);
                    }

                } catch (e) {

                }

                eChart.updateAttributes(_chart);
                var dataInFormat = {};
                if (eChart.isGroupColorApplied(_chart)) {
                    dataInFormat = _chart.getDataByGroup();
                } else {
                    dataInFormat = _chart.getNormalData();
                }


                return dataInFormat;


            }

            _chart.data = function (data) {

                _chart._data = _chart.getDataInFormat();
                return _chart;
            };

            _chart.render = function () {
                eChart.clearContainer(id);
                var bar = eChart.getCanvasContext(_chart, function (context) {
                    _chart.draw(context);
                    _chart.bindEvents();
                });
            };

            _chart.draw = function (context) {
                var instance = new Chart(context, {
                    type: 'bar',
                    data: _chart._data,
                    options: eChart.createOption(_chart)
                });
                var res = id.split("-");
                _chart.chartInstance = instance;
                eChart.generateLegend("left" + res[1], _chart);


                eChart.registerChart(_chart);
            };

            _chart.operator = function (operator) {
                _chart._operator = operator;
                return _chart;
            };

            _chart.bindEvents = function () {
                eChart.bindEvents(this);
            };

            _chart.highlightSelectedFilter = function () {

                var labels = this.getLabels();
                var labelsLength = labels.length;
                var colorsLength = eChart.colors.length;
                var chartFiltersLength = 0;
                if (!_chart.reset) {
                    chartFiltersLength = this.filters.length;
                }

                if (labelsLength == chartFiltersLength || chartFiltersLength === 0) {


                    _chart._data.datasets.forEach(function (dataset, dsIndex) {
                        labels.forEach(function (filter, i) {
                            _chart._data.datasets[dsIndex].backgroundColor[i] = _chart._colorSelection[dsIndex % colorsLength];
                        });
                    });

                    _chart.filters = [];
                } else {

                    var datasetsArray = [];
                    var datasetWiseFilters = {};


                    _chart.filters.forEach(function (filter) {
                        if (!datasetWiseFilters[filter.datasetIndex])
                            datasetWiseFilters[filter.datasetIndex] = [];
                        datasetWiseFilters[filter.datasetIndex].push(filter.label);
                    });


                    _chart._data.datasets.forEach(function (dataset, dsIndex) {
                        var filterLabelsArray = datasetWiseFilters[dsIndex];
                        if (filterLabelsArray) {
                            labels.forEach(function (label, index) {
                                if (filterLabelsArray.indexOf(label) == -1) {

                                    dataset.backgroundColor[index] = "#ddd";
                                } else {
                                    dataset.backgroundColor[index] = _chart._colorSelection[dsIndex % colorsLength];
                                }

                            });

                        } else {
                            dataset.backgroundColor = Array(labels.length).fill("#ddd");
                        }

                    });


                }
            };

            _chart.colorChange = function (selectedDataSet, color) {
                var labels = this.getLabels();
                _chart._data.datasets.forEach(function (dataset, dsIndex) {
                    if (dataset.label == selectedDataSet.datasetLabel) {
                        _chart._colorSelection[dsIndex] = color;
                        var tempData = [];
                        labels.forEach(function (filter, i) {

                            tempData.push(color);
                        });
                        _chart._data.datasets[dsIndex].backgroundColor = tempData;
                        _chart._data.datasets[dsIndex].borderColor = tempData;
                    }
                });
                setTimeout(function () {
                    _chart.chartInstance.update();
                    eChart.registerColor(_chart);
                    var res = _chart.id.split("-");
                    eChart.generateLegend("left" + res[1], _chart);
                }, 1);
            };
            _chart.getClickedElementLabel = function (evt) {
                var filter = {};
                try {
                    filter.label = this.chartInstance.getElementAtEvent(evt)[0]._model.label;
                    filter.datasetLabel = this.chartInstance.getElementAtEvent(evt)[0]._model.datasetLabel;
                    filter.datasetIndex = this.chartInstance.getElementAtEvent(evt)[0]._datasetIndex;
                    return filter;
                } catch (e) {

                    return null;
                }


            };

            _chart.getLabels = function () {
                return this.chartInstance.config.data.labels;
            };
            return _chart;

        };
        // -------------------------------------BAR CHART END---------------------------------------------------------------


        // ------------------------------------Row CHART DRAW CODE----------------------------------------------------------
        eChart.rowChart = function (id) {
            var _chart = {};
            _chart.type = 'horizontalBar';
            _chart.id = id;
            _chart._sort = null;
            _chart._colorSelection = $.extend([], eChart.colors);

            _chart.group = function (group) {
                _chart._group = group;
                _chart.data();
                return _chart;
            };

            _chart.dimension = function (dimension) {
                _chart._dimension = dimension;
                return _chart;
            };

            _chart.dataFormate = function (dataFormat) {
                _chart._dataFormateVal = dataFormat;
                _chart.setDataFormat();
                return _chart;
            };

            _chart.dataSetLabels = function (labels) {
                _chart._dataSetLabels = labels;
                return _chart;
            };

            _chart.sort = function (order) {
                _chart._sort = order;
                eChart.updateSortData(_chart, order);
                eChart.registerDataOrder(_chart);
            };

            _chart.xAxes = function () {
                if(/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
                    _chart._fontSize=36;
                }
                if ((_chart._dataType == "date" || _chart._fontSize == "datetime") && _chart._dataFormateVal.xAxis == undefined) {
                    return [{
                        stacked: true,
                        ticks: {
                            fontSize: _chart._fontSize,
                            autoSkip: true,
                            maxTicksLimit: 10,
                            fontFamily: "sans-serif"
                        },
                        type: 'time',
                        unit: 'day',
                        unitStepSize: 1,
                        time: {
                            displayFormats: {
                                'day': 'MMM DD'
                            }
                        }
                    }];
                } else {
                    return [{
                        stacked: true,
                        ticks: {
                            fontSize: _chart._fontSize,
                            autoSkip: true,
                            maxTicksLimit: 10,
                            fontFamily: "sans-serif"
                        }
                    }];
                }
            };
            _chart.yAxes=function(chart){
                return eChart.renderyAxes(chart);
            }
            _chart.dataType = function (type) {
                _chart._dataType = type;
            };
            _chart.fontSize = function (fontSize) {
                _chart._fontSize = fontSize
            };
            _chart.fontFamily = function (fontFamily) {
                _chart._fontFamily = fontFamily;
            }
            _chart.yAxisPrefix = function (yAxis) {
                if (yAxis) {
                    _chart._yAxisPrefix = yAxis;
                } else {
                    _chart._yAxisPrefix = "";
                }
            }
            _chart.setDataFormat = function () {
                if (_chart._dataFormateVal.yAxis) {
                    _chart.yAxisPrefix(_chart._dataFormateVal.yAxis);
                } else {
                    _chart.yAxisPrefix("");
                }
                if (_chart._dataFormateVal.fontSize) {
                    _chart.fontSize(_chart._dataFormateVal.fontSize);
                } else {
                    _chart.fontSize(12);
                }
                if(_chart._dataFormateVal.topN){
                    _chart.topN=_chart._dataFormateVal.topN;
                }
                _chart.groupColor = _chart._dataFormateVal.groupColor;

                _chart._dataType = _chart._dataFormateVal.dataType;

            }
            //---------------------------Group Color Start--------------------
            _chart.getDatasetByGroupColor = function (finalData, groupKey) {
                var groupMappings = _chart.createGroupMappings(finalData, _chart);
                var initialData = [];
                _chart._labels.forEach(function (d, i) {
                    initialData[i] = null;
                });
                var p = 0;
                var datasets = [];
                $.each(groupMappings, function (key, value) {
                    var data = Object.assign([], initialData);
                    var sumAdder=0;
                    $.each(value, function (k, v) {
                        var labelIndexMatched = _chart._labels.indexOf(k);

                        if (labelIndexMatched != -1) {
                            if(_chart._operator[Object.keys(_chart._operator)[0]].key=="runTotal"){
                                sumAdder+=v;
                                if (!(v <= 0))
                                    data[labelIndexMatched] = sumAdder;
                            }else{
                                if (!(v <= 0))
                                    data[labelIndexMatched] += v;

                            }
                        }


                    });
                    var dt = {
                        label: key,
                        data: data,
                        backgroundColor: Array(data.length).fill(_chart._colorSelection[p % (_chart._colorSelection.length)]),
                        borderWidth: 1
                    };

                    //     _chart._labels=labels
                    datasets.push(dt);
                    p++;
                });
                return datasets;
            }
            _chart.updateDatasetByGroupColor = function (finalData, groupKey) {

                var groupMappings = _chart.createGroupMappings(finalData, _chart);
                _chart.legendList=[];
                var labelsList = {};
                $.each(groupMappings, function (key, value) {
                    $.each(value, function (k, v) {



                        if (!(v == 0 || v < 0 || isNaN(v))) {

                            labelsList[k] = 1;


                        }


                    });
                });


                var initialData = [];
                var indexData = 0;
                var labelsListArray = [];
                $.each(labelsList, function (key, value) {
                    initialData[indexData] = null;
                    labelsListArray.push(key);
                    indexData++;
                });

                if(eChart.isDataFormatApplied(_chart) && (_chart._dataFormateVal.xAxis == "MMMM" || _chart._dataFormateVal.xAxis == "MMM")){
                    _chart._labels = eChart.sortArray(labelsListArray);
                }else{
                    _chart._labels = labelsListArray;
                }

                var p = 0;
                var datasets = [];


                $.each(groupMappings, function (key, value) {
                    var data = Object.assign([], initialData);

                    var sumAdder=0;
                    $.each(value, function (k, v) {
                        var labelIndexMatched = _chart._labels.indexOf(k);

                        if (labelIndexMatched != -1) {
                            if(_chart._operator[Object.keys(_chart._operator)[0]].key=="runTotal"){
                                sumAdder+=v;
                                if (!(v <= 0))
                                    data[labelIndexMatched] = sumAdder;
                            }else{
                                if (!(v <= 0))
                                    data[labelIndexMatched] += v;

                            }
                        }


                    });
                    _chart.legendList.push(key);
                    var dt = {
                        label: key,
                        data: data,
                        backgroundColor: Array(data.length).fill(_chart._colorSelection[p % (_chart._colorSelection.length)]),
                        borderWidth: 1
                    };
                    //     _chart._labels=labels
                    datasets.push(dt);
                    p++;
                });

                return datasets;
            }
            //---------------------------Group Color End-----------------------

            _chart.getSortedData = function (grpObj, key, order) {
                return eChart.getSortedData(_chart, grpObj, key, order);
            }

            _chart.createGroupMappings = function (finalData, _chart) {
                _chart._groupMappings = eChart.createGroupMappings(finalData, _chart);

                return _chart._groupMappings;
            }
            _chart.getDataByGroup = function () {
                var groups = _chart._group;
                var datasets = [];
                $.each(groups, function (key, grpObj) {

                    var finalData;

                    if (!_chart._sort) {
                        finalData = grpObj.all();
                        if(_chart.topN){

                            finalData =  eChart.getTopN(_chart,grpObj,key);

                        }
                    }
                    else {
                        finalData = _chart.getSortedData(grpObj, key, _chart._sort);
                    }

                    if (eChart.isDataFormatApplied(_chart)) {
                        finalData = eChart.getDataByDateFormat(grpObj, key, _chart);
                    }
                    if (eChart.isDataFormatApplied(_chart)) {
                        finalData = eChart.getDataByDateFormat(grpObj, key, _chart);
                    }
                    datasets = _chart.getDatasetByGroupColor(finalData, key);
                });

                var dataInFormat = {
                    labels: _chart._labels,
                    datasets: datasets
                };
                return dataInFormat;
            }
            _chart.getNormalData = function () {

                var colorsLength = eChart.colors.length;
                var groups = _chart._group;

                var labels = [];
                var datasets = [];
                var i = 0;
                var j = 0;
                var index = 0;
                $.each(groups, function (key, grpObj) {
                    var data = [];
                    var backgroundColor = [];
                    var borderColor = [];
                    var hoverBorderColor = [];
                    var finalData;


                    if (!_chart._sort) {



                        finalData = grpObj.all();
                        if(_chart.topN){

                            finalData =  eChart.getTopN(_chart,grpObj,key);

                        }
                    }
                    else {

                        finalData = _chart.getSortedData(grpObj, key, _chart._sort);
                    }

                    if (eChart.isDataFormatApplied(_chart)) {
                        finalData = eChart.getDataByDateFormat(grpObj, key, _chart);
                    }
                    if(_chart._operator[key].key=="runTotal"){
                        finalData=eChart.getRunTotal(finalData);
                    }

                    finalData.forEach(function (d) {
                        if (j === 0)
                            labels.push(d.key);
                        if (_chart._operator) {
                            data.push(d.value[_chart._operator[key].key]);
                        } else {
                            data.push(d.value);
                        }

                        backgroundColor.push(_chart._colorSelection[j]);
                        hoverBorderColor.push("#000");
                        i++;
                    });
                    var dt = {
                        label: _chart._dataSetLabels[j],
                        data: data,
                        backgroundColor: backgroundColor,
                        borderColor: backgroundColor,
                        /*hoverBorderColor: hoverBorderColor,*/
                        borderWidth: 1
                    };
                    datasets.push(dt);
                    j++;
                });
                var dataInFormat = {
                    labels: labels,
                    datasets: datasets
                };
                return dataInFormat;
            }
            _chart.getDataInFormat = function (order) {
                try {
                    if (_chart._dataFormateVal && _chart._dataFormateVal.tooltipSelector) {
                        eChart.processTooltipData(_chart);
                    }

                } catch (e) {

                }

                eChart.updateAttributes(_chart);
                var dataInFormat = {};
                if (eChart.isGroupColorApplied(_chart)) {
                    dataInFormat = _chart.getDataByGroup();
                } else {
                    dataInFormat = _chart.getNormalData();
                }


                return dataInFormat;


            }

            _chart.data = function (data) {

                _chart._data = _chart.getDataInFormat();
                return _chart;
            };

            _chart.render = function () {
                eChart.clearContainer(id);

                var bar = eChart.getCanvasContext(_chart, function (context) {
                    _chart.draw(context);
                    _chart.bindEvents();
                });
                setTimeout(function () {
                    var res = _chart.id.split("-");
                    eChart.generateLegend("left" + res[1], _chart);
                }, 1);
            };

            _chart.draw = function (context) {
                var instance = new Chart(context, {
                    type: 'horizontalBar',
                    data: _chart._data,
                    options: eChart.createOption(_chart)
                });
                var res = id.split("-");
                _chart.chartInstance = instance;
                eChart.generateLegend("left" + res[1], _chart);


                eChart.registerChart(_chart);
            };

            _chart.operator = function (operator) {
                _chart._operator = operator;
                return _chart;
            };

            _chart.bindEvents = function () {
                eChart.bindEvents(this);
            };

            _chart.highlightSelectedFilter = function () {

                var labels = this.getLabels();
                var labelsLength = labels.length;
                var colorsLength = eChart.colors.length;
                var chartFiltersLength = 0;
                if (!_chart.reset) {
                    chartFiltersLength = this.filters.length;
                }

                if (labelsLength == chartFiltersLength || chartFiltersLength === 0) {


                    _chart._data.datasets.forEach(function (dataset, dsIndex) {
                        labels.forEach(function (filter, i) {
                            _chart._data.datasets[dsIndex].backgroundColor[i] = _chart._colorSelection[dsIndex % colorsLength];
                        });
                    });

                    _chart.filters = [];
                } else {

                    var datasetsArray = [];
                    var datasetWiseFilters = {};


                    _chart.filters.forEach(function (filter) {
                        if (!datasetWiseFilters[filter.datasetIndex])
                            datasetWiseFilters[filter.datasetIndex] = [];
                        datasetWiseFilters[filter.datasetIndex].push(filter.label);
                    });


                    _chart._data.datasets.forEach(function (dataset, dsIndex) {
                        var filterLabelsArray = datasetWiseFilters[dsIndex];
                        if (filterLabelsArray) {
                            labels.forEach(function (label, index) {
                                if (filterLabelsArray.indexOf(label) == -1) {

                                    dataset.backgroundColor[index] = "#ddd";
                                } else {
                                    dataset.backgroundColor[index] = _chart._colorSelection[dsIndex % colorsLength];
                                }

                            });

                        } else {
                            dataset.backgroundColor = Array(labels.length).fill("#ddd");
                        }

                    });


                }
            };

            _chart.colorChange = function (selectedDataSet, color) {
                var labels = this.getLabels();
                _chart._data.datasets.forEach(function (dataset, dsIndex) {
                    if (dataset.label == selectedDataSet.datasetLabel) {
                        _chart._colorSelection[dsIndex] = color;
                        var tempData = [];
                        labels.forEach(function (filter, i) {

                            tempData.push(color);
                        });
                        _chart._data.datasets[dsIndex].backgroundColor = tempData;
                        _chart._data.datasets[dsIndex].borderColor = tempData;
                    }
                });
                setTimeout(function () {
                    _chart.chartInstance.update();
                    eChart.registerColor(_chart);
                    var res = _chart.id.split("-");
                    eChart.generateLegend("left" + res[1], _chart);
                }, 1);
            };
            _chart.getClickedElementLabel = function (evt) {
                var filter = {};
                try {
                    filter.label = this.chartInstance.getElementAtEvent(evt)[0]._model.label;
                    filter.datasetLabel = this.chartInstance.getElementAtEvent(evt)[0]._model.datasetLabel;
                    filter.datasetIndex = this.chartInstance.getElementAtEvent(evt)[0]._datasetIndex;
                    return filter;
                } catch (e) {

                    return null;
                }


            };

            _chart.getLabels = function () {
                return this.chartInstance.config.data.labels;
            };
            return _chart;

        };
        // -------------------------------------BAR CHART END---------------------------------------------------------------


        // -------------------------------------LINE CHART DRAW CODE--------------------------------------------------------

        eChart.lineChart = function (id) {

            var _chart = {};
            _chart.id = id;
            _chart.dataBackup = [];
            //Color section changes
            _chart.type = 'lineChart';
            _chart._colorSelection = $.extend([], eChart.colors);
            //End
            _chart.group = function (group) {
                _chart._group = group;
                _chart.data();
                return _chart;
            };
            _chart.dimension = function (dimension) {
                _chart._dimension = dimension;
                return _chart;
            };
            _chart.dataFormate = function (dataFormate) {
                _chart._dataFormateVal = dataFormate;
                _chart.setDataFormat();
                return _chart;
            };
            _chart.setDataFormat = function () {

                if (_chart._dataFormateVal.yAxis) {
                    _chart.yAxisPrefix(_chart._dataFormateVal.yAxis);
                } else {
                    _chart.yAxisPrefix("");
                }
                if (_chart._dataFormateVal.fontSize) {
                    _chart.fontSize(_chart._dataFormateVal.fontSize);
                } else {
                    _chart.fontSize(12);
                }
                if(_chart._dataFormateVal.topN){
                    _chart.topN=_chart._dataFormateVal.topN;
                }
                _chart.groupColor = _chart._dataFormateVal.groupColor;

                _chart._dataType = _chart._dataFormateVal.dataType;

            }
            _chart.operator = function (operator) {
                _chart._operator = operator;
                return _chart;
            };
            _chart.data = function () {
                _chart._data = _chart.getDataInFormat();
                return _chart;
            };

            _chart.render = function () {
                eChart.clearContainer(id);
                var bar = eChart.getCanvasContext(_chart, function (context) {
                    _chart.draw(context);
                    _chart.bindEvents();
                });
                return true;
            };

            _chart.sort = function (order) {
                _chart._sort = order;
                eChart.updateSortData(_chart, order);
                eChart.registerDataOrder(_chart);
            };

            _chart.getSortedData = function (grpObj, key, order) {
                return eChart.getSortedData(_chart, grpObj, key, order);

            }
            //Style Setting
            _chart.styleSetting = function () {
                var ticks = {};
                ticks['fontSize'] = 20;
            }

            //Group Mapping...
            _chart.createGroupMappings = function (finalData, _chart) {

                _chart._groupMappings = eChart.createGroupMappings(finalData, _chart);
                return _chart._groupMappings;
            }

            //M -Start

            _chart.getDatasetByGroupColor = function (finalData, groupKey) {
                var groupMappings = _chart.createGroupMappings(finalData, _chart);


                var initialData = [];
                _chart._labels.forEach(function (d, i) {
                    initialData[i] = null;
                });


                var p = 0;
                var datasets = [];


                $.each(groupMappings, function (key, value) {
                    var data = Object.assign([], initialData);
                    var pointRadius = Array(data.length).fill(2);
                    var pointBorderColor = Array(data.length).fill(_chart._colorSelection[p % (_chart._colorSelection.length)]);
                    var sumAdder=0;
                    $.each(value, function (k, v) {
                        var labelIndexMatched = _chart._labels.indexOf(k);

                        if (labelIndexMatched != -1) {
                            if(_chart._operator[Object.keys(_chart._operator)[0]].key=="runTotal"){
                                sumAdder+=v;
                                if (!(v <= 0))
                                    data[labelIndexMatched] = sumAdder;
                            }else{
                                if (!(v <= 0))
                                    data[labelIndexMatched] += v;

                            }
                        }


                    });
                    var dt = {
                        label: key,
                        data: data,
                        backgroundColor: _chart._colorSelection[p % (_chart._colorSelection.length)],
                        borderColor: _chart._colorSelection[p % (_chart._colorSelection.length)],
                        showLine: true,
                        fill: false,
                        borderWidth: 2,
                        lineTension: 0,
                        pointBorderColor: pointBorderColor,
                        pointRadius: 2
                    };
                    //     _chart._labels=labels
                    datasets.push(dt);
                    p++;
                });
                return datasets;
            }
            _chart.updateDatasetByGroupColor = function (finalData, groupKey) {

                var groupMappings = _chart.createGroupMappings(finalData, _chart);
                var labelsList = {};

                $.each(groupMappings, function (key, value) {
                    $.each(value, function (k, v) {



                        if (!(v == 0 || v < 0 || isNaN(v))) {

                            labelsList[k] = 1;


                        }


                    });
                });

                var initialData = [];
                var indexData = 0;
                var labelsListArray = [];
                $.each(labelsList, function (key, value) {
                    initialData[indexData] = null;
                    labelsListArray.push(key);
                    indexData++;
                });

                if(eChart.isDataFormatApplied(_chart) && (_chart._dataFormateVal.xAxis == "MMMM" || _chart._dataFormateVal.xAxis == "MMM")){
                    _chart._labels = eChart.sortArray(labelsListArray);
                }else{
                    _chart._labels = labelsListArray;
                }

                var p = 0;
                var datasets = [];


                $.each(groupMappings, function (key, value) {
                    var data = Object.assign([], initialData);

                    var sumAdder=0;
                    $.each(value, function (k, v) {
                        var labelIndexMatched = _chart._labels.indexOf(k);

                        if (labelIndexMatched != -1) {
                            if(_chart._operator[Object.keys(_chart._operator)[0]].key=="runTotal"){
                                sumAdder+=v;
                                if (!(v <= 0))
                                    data[labelIndexMatched] = sumAdder;
                            }else{
                                if (!(v <= 0))
                                    data[labelIndexMatched] += v;

                            }
                        }


                    });
                    var pointBorder = "#ccc";
                    var dt = {
                        label: key,
                        data: data,
                        backgroundColor: _chart._colorSelection[p % (_chart._colorSelection.length)],
                        borderColor: _chart._colorSelection[p % (_chart._colorSelection.length)],
                        showLine: true,
                        fill: false,
                        borderWidth: 2,
                        lineTension: 0,
                        //   pointBorderColor: _chart._data.datasets[p].pointBorderColor,
                        //    pointRadius: _chart._data.datasets[p].pointRadius
                    };

                    //     _chart._labels=labels
                    datasets.push(dt);

                    p++;

                });

                return datasets;
            }

            //M-End
            _chart.colorChange = function (selectedDataSet, color) {
                var labels = this.getLabels();
                var labelsLength = labels.length;
                var colorsLength = eChart.colors.length;
                var chartFiltersLength = 0;

                _chart._data.datasets.forEach(function (dataset, dsIndex) {

                    if (dataset.label == selectedDataSet.datasetLabel) {

                        _chart._colorSelection[dsIndex] = color;

                        _chart._data.datasets[dsIndex].backgroundColor = color;
                        _chart._data.datasets[dsIndex].borderColor = color;

                        _chart._data.datasets[dsIndex].pointBorderColor = Array(_chart._data.datasets[dsIndex].data.length).fill(color);

                    }
                });
                setTimeout(function () {
                    eChart.registerColor(_chart);
                    _chart.chartInstance.update();
                    var res = _chart.id.split("-");
                    eChart.generateLegend("left" + res[1], _chart);
                }, 1);

            };
            _chart.xAxes = function () {
                return eChart.renderxAxes(_chart._dataType, _chart._fontSize, _chart._dataFormateVal);
            };
            _chart.yAxes=function(chart){
                return eChart.renderyAxes(chart);
            }
            _chart.dataType = function (type) {
                _chart._dataType = type;
            };
            _chart.fontSize = function (fontSize) {
                _chart._fontSize = fontSize
            };
            _chart.fontFamily = function (fontFamily) {
                _chart._fontFamily = fontFamily;
            }
            _chart.yAxisPrefix = function (yAxis) {
                if (yAxis) {
                    _chart._yAxisPrefix = yAxis;
                } else {
                    _chart._yAxisPrefix = "";
                }
            }
            _chart.getDatasetByMeasure = function (finalData) {
                var datasets = [];
                var backgroundColorIndex = 0;
                var GroupColor = {};
                var backgroundColor = [];

                finalData.forEach(function (d) {
                    // if (j === 0)
                    // labels.push(d.key);
                    if (_chart._operator) {
                        data.push(d.value[_chart._operator[key].key]);
                    } else {
                        data.push(d.value);
                    }

                    if (d.value.groupColor != undefined) {

                        if (GroupColor[d.value.groupColor]) {
                            backgroundColor.push(GroupColor[d.value.groupColor]);
                        } else {
                            backgroundColor.push(_chart._colorSelection[backgroundColorIndex % (_chart._colorSelection.length)]);
                            GroupColor[d.value.groupColor] = _chart._colorSelection[backgroundColorIndex % (_chart._colorSelection.length)];
                            backgroundColorIndex++;
                        }
                    } else {
                        backgroundColor.push(_chart._colorSelection[j % (_chart._colorSelection.length)]);
                    }
                    //backgroundColor.push(eChart.colors[j]);
                    i++;
                });

                var dt = {
                    label: _chart._dataSetLabels[j],
                    data: data,
                    backgroundColor: backgroundColor,
                    borderColor: backgroundColor,
                    borderWidth: 1,
                    pointBorderColor: backgroundColor,
                    pointRadius: 2,
                };
                datasets.push(dt);
                return datasets;
            }

            _chart.draw = function (context) {
                var instance = new Chart(context, {
                    type: 'line',
                    data: _chart._data,
                    options: {
                        responsive: true,
                        maintainAspectRatio: false,
                        elements: {
                            line: {
                                tension: 0, // disables bezier curves
                            },
                            point: {
                                backgroundColor: "#999",
                            }
                        },
                        bezierCurve: false,
                        legend: {
                            display: false
                        },
                        tooltips: {
                            position: 'nearest',
                            /*mode: 'index',*/
                            intersect: false,
                            enabled: false,
                            custom: function (tooltip) {
                                eChart.customTooltips(tooltip, _chart, this)
                            }
                        },
                        scales: {
                            yAxes: _chart.yAxes(_chart),
                            xAxes: _chart.xAxes()
                            /*xAxes: xAxesArray*/
                        },
                        animation: {
                            // duration: 0 disable animation
                        },

                        pan: {
                            // Boolean to enable panning
                            enabled: false,

                            // Panning directions. Remove the appropriate direction to disable
                            // Eg. 'y' would only allow panning in the y direction
                            mode: 'xy'
                        },
                        // Container for zoom options
                        zoom: {
                            // Boolean to enable zooming
                            enabled: false,
                            sensitivity: 0,
                            // Zooming directions. Remove the appropriate direction to disable
                            // Eg. 'y' would only allow zooming in the y direction
                            mode: 'x'
                        },
                        stack:true
                    }
                });
                var res = id.split("-");
                _chart.chartInstance = instance;
                eChart.generateLegend("left" + res[1], _chart);
                eChart.registerChart(_chart);
            };

            _chart.bindEvents = function () {
                eChart.bindEvents(this);
            };

            _chart.highlightSelectedFilter = function () {
                var labels = this.getLabels();
                var labelsLength = labels.length;
                var chartFiltersLength = 0;
                if (!_chart.reset) {
                    chartFiltersLength = this.filters.length;
                }
                var pointRadiusNumber=2;
                if(/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
                    pointRadiusNumber=20;
                }
                if (labelsLength == chartFiltersLength || chartFiltersLength === 0) {
                    _chart._data.datasets.forEach(function (dataset, dsIndex) {
                        _chart._data.datasets[dsIndex].backgroundColor = _chart._colorSelection[dsIndex];
                        labels.forEach(function (filter, i) {
                            try {
                                _chart._data.datasets[dsIndex].pointRadius[i] = pointRadiusNumber;
                                _chart._data.datasets[dsIndex].pointBorderColor[i] = _chart._colorSelection[dsIndex];
                            } catch (e) {

                            }

                        });
                    });
                    _chart.filters = [];
                } else {
                    var filterLookup = {};
                    $.map(_chart.filters, function (value, index) {
                        if (!filterLookup[value['datasetIndex']]) {
                            filterLookup[value['datasetIndex']] = [];
                        }
                        filterLookup[value['datasetIndex']].push(value['label']);
                    });
                    labels.forEach(function (label, i) {
                        $.each(filterLookup, function (dsIndex, val) {
                            if (val.indexOf(label) != -1) {
                                try {
                                    _chart._data.datasets[dsIndex].pointRadius[i] = pointRadiusNumber;
                                    _chart._data.datasets[dsIndex].pointBorderColor[i] = 'rgb(0,0, 0)';
                                } catch (e) {

                                }
                            }
                        });


                    });
                }
                /*var labels = this.getLabels();
                 var labelsLength = labels.length;


                 var chartFiltersLength = 0;
                 if (!_chart.reset) {
                 chartFiltersLength = this.filters.length;
                 }

                 if (labelsLength == chartFiltersLength || chartFiltersLength === 0) {


                 _chart._data.datasets.forEach(function (dataset, dsIndex) {
                 _chart._data.datasets[dsIndex].backgroundColor = _chart._colorSelection[dsIndex];
                 _chart._data.datasets[dsIndex].borderColor=_chart._colorSelection[dsIndex];
                 labels.forEach(function (filter, i) {


                 _chart._data.datasets[dsIndex].pointRadius[i] = 2;
                 _chart._data.datasets[dsIndex].pointBorderColor[i] = _chart._colorSelection[dsIndex];


                 });
                 });


                 _chart.filters = [];
                 } else {
                 var filterLookup = {};
                 $.map(_chart.filters, function (value, index) {
                 if (!filterLookup[value['datasetIndex']]) {
                 filterLookup[value['datasetIndex']] = [];
                 }
                 filterLookup[value['datasetIndex']].push(value['label']);

                 // if (!filterLookup[value['label']]) {
                 //     filterLookup[value['label']]=true;
                 // }
                 });



                 labels.forEach(function (label, i) {


                 $.each(filterLookup, function (dsIndex, val) {
                 // if (filterLookup[label]) {
                 try{
                 _chart._data.datasets.forEach(function(d,index){
                 if(index==dsIndex && val.indexOf(label)!=-1){
                 d.pointRadius[i] = 5;
                 d.pointBorderColor[i] = 'rgb(0,0, 0)';
                 }
                 else if(index==dsIndex){
                 d.pointRadius[i] = 2;
                 d.pointBorderColor[i] = _chart._colorSelection[index];
                 }
                 if(index!=dsIndex){
                 d.backgroundColor="#ccc";
                 d.borderColor="#ccc";
                 d.pointBorderColor[i]="#ccc";
                 }

                 });
                 // _chart._data.datasets[dsIndex].pointRadius[i] = 5;
                 // _chart._data.datasets[dsIndex].pointBorderColor[i] = 'rgb(0,0, 0)';
                 }catch(e){

                 }
                 // }
                 // else{
                 //
                 //      _chart._data.datasets.forEach(function(d,index){
                 //          d.pointRadius[i] = 2;
                 //          d.pointBorderColor[i] = _chart._colorSelection[index];
                 //      });
                 //      // _chart._data.datasets[dsIndex].pointRadius[i] = 2;
                 //      // _chart._data.datasets[dsIndex].pointBorderColor[i] = _chart._colorSelection[dsIndex];
                 //  }
                 });


                 });
                 }*/


            };

            _chart.getLabels = function () {
                return this.chartInstance.config.data.labels;
            };
            _chart.dataSetLabels = function (labels) {
                _chart._dataSetLabels = labels;
                return _chart;
            };

//            _chart.highlightSelectedFilter = function () {
//
//
//            };
            _chart.getClickedElementLabel = function (evt) {
                var filter = {};
                try {
                    filter.label = this.getLabels()[this.chartInstance.getElementAtEvent(evt)[0]._index];
                    filter.datasetLabel = this.chartInstance.getElementAtEvent(evt)[0]._chart.config.data.datasets[this.chartInstance.getElementAtEvent(evt)[0]._datasetIndex].label;
                    filter.datasetIndex = this.chartInstance.getElementAtEvent(evt)[0]._datasetIndex;
                    return filter;
                } catch (e) {
                    return null;

                }
            };
            _chart.getDataByGroup = function () {

                var groups = _chart._group;


                var datasets = [];


                $.each(groups, function (key, grpObj) {
                    var finalData;

                    if (!_chart._sort) {
                        finalData = grpObj.all();
                        if(_chart.topN){

                            finalData =  eChart.getTopN(_chart,grpObj,key);

                        }
                    } else {
                        finalData = _chart.getSortedData(grpObj, key, _chart._sort);
                    }
                    if (eChart.isDataFormatApplied(_chart)) {
                        finalData = eChart.getDataByDateFormat(grpObj, key, _chart);
                    }
                    datasets = _chart.getDatasetByGroupColor(finalData, key);
                });

                var dataInFormat = {
                    labels: _chart._labels,
                    datasets: datasets
                };
                return dataInFormat;
            };
            _chart.getNormalData = function () {
                var colorsLength = eChart.colors.length;
                var groups = _chart._group;
                var labels = [];
                var datasets = [];
                var i = 0;
                var j = 0;
                var index = 0;
                $.each(groups, function (key, grpObj) {
                    var data = [];
                    var backgroundColor = [];
                    var borderColor = [];
                    var pointRadius = [];
                    var pointBorderColor = [];
                    var finalData;

                    if (_chart._sort == undefined) {
                        finalData = grpObj.all();
                        if(_chart.topN){

                            finalData =  eChart.getTopN(_chart,grpObj,key);

                        }
                    } else {
                        finalData = _chart.getSortedData(grpObj, key, _chart._sort);
                    }
                    if (eChart.isDataFormatApplied(_chart)) {
                        finalData = eChart.getDataByDateFormat(grpObj, key, _chart);
                    }
                    if(_chart._operator[key].key=="runTotal"){
                        finalData=eChart.getRunTotal(finalData);
                    }

                    finalData.forEach(function (d) {
                        if (j === 0)
                            labels.push(d.key);

                        if (_chart._operator) {

                            data.push(d.value[_chart._operator[key].key]);
                        } else {
                            data.push(d.value);
                        }
                        var pointRadiusNumber=2;
                        if(/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
                            pointRadiusNumber=20;
                        }

                        backgroundColor.push(_chart._colorSelection[j]);
                        pointRadius.push(pointRadiusNumber);
                        pointBorderColor.push(_chart._colorSelection[j]);
                        borderColor.push(_chart._colorSelection[j]);
                        //backgroundColor.push(eChart.colors[j]);
                        i++;
                    });
                    var dt = {
                        label: _chart._dataSetLabels[j],
                        data: data,
                        backgroundColor: backgroundColor[j],
                        borderColor: backgroundColor[j],
                        /*hoverBorderColor: borderColor[j],*/
                        pointBorderColor: pointBorderColor,
                        pointRadius: pointRadius,
                        fill: false,
                        borderWidth: 2
                    };

                    datasets.push(dt);
                    j++;
                    index++;
                });
                var dataInFormat = {
                    labels: labels,
                    datasets: datasets
                };
                return dataInFormat;
            };
            _chart.getDataInFormat = function (order) {
                if (_chart._dataFormateVal && _chart._dataFormateVal.tooltipSelector) {
                    eChart.processTooltipData(_chart);
                }
                var dataInFormat = {};
                eChart.updateAttributes(_chart);


                if (eChart.isGroupColorApplied(_chart)) {
                    dataInFormat = _chart.getDataByGroup();
                } else {
                    dataInFormat = _chart.getNormalData();
                }

                return dataInFormat;
            };

            return _chart;


        };

        // ------------------------------------LINE  END--------------------------------------------------------------


        // -------------------------------------Area CHART DRAW CODE--------------------------------------------------------
        eChart.areaChart = function (id) {

            var _chart = {};
            _chart.id = id;
            _chart.dataBackup = [];
            //Color section changes
            _chart.type = 'areaChart';
            _chart._colorSelection = $.extend([], eChart.colors);
            //End
            _chart.group = function (group) {
                _chart._group = group;
                _chart.data();
                return _chart;
            };
            _chart.dimension = function (dimension) {
                _chart._dimension = dimension;
                return _chart;
            };
            _chart.dataFormate = function (dataFormate) {
                _chart._dataFormateVal = dataFormate;
                _chart.setDataFormat();
                return _chart;
            };
            _chart.setDataFormat = function () {

                if (_chart._dataFormateVal.yAxis) {
                    _chart.yAxisPrefix(_chart._dataFormateVal.yAxis);
                } else {
                    _chart.yAxisPrefix("");
                }
                if (_chart._dataFormateVal.fontSize) {
                    _chart.fontSize(_chart._dataFormateVal.fontSize);
                } else {
                    _chart.fontSize(12);
                }
                if(_chart._dataFormateVal.topN){
                    _chart.topN=_chart._dataFormateVal.topN;
                }
                _chart.groupColor = _chart._dataFormateVal.groupColor;

                _chart._dataType = _chart._dataFormateVal.dataType;

            }
            _chart.operator = function (operator) {
                _chart._operator = operator;
                return _chart;
            };
            _chart.data = function () {
                _chart._data = _chart.getDataInFormat();
                return _chart;
            };

            _chart.render = function () {
                eChart.clearContainer(id);
                var bar = eChart.getCanvasContext(_chart, function (context) {
                    _chart.draw(context);
                    _chart.bindEvents();
                });
                return true;
            };

            _chart.sort = function (order) {
                _chart._sort = order;
                eChart.updateSortData(_chart, order);
                eChart.registerDataOrder(_chart);
            };

            _chart.getSortedData = function (grpObj, key, order) {
                return eChart.getSortedData(_chart, grpObj, key, order);

            }
            //Style Setting
            _chart.styleSetting = function () {
                var ticks = {};
                ticks['fontSize'] = 20;
            }

            //Group Mapping...
            _chart.createGroupMappings = function (finalData, _chart) {

                _chart._groupMappings = eChart.createGroupMappings(finalData, _chart);
                return _chart._groupMappings;
            }

            //M -Start

            _chart.getDatasetByGroupColor = function (finalData, groupKey) {
                var groupMappings = _chart.createGroupMappings(finalData, _chart);


                var initialData = [];
                _chart._labels.forEach(function (d, i) {
                    initialData[i] = null;
                });


                var p = 0;
                var datasets = [];


                $.each(groupMappings, function (key, value) {
                    var data = Object.assign([], initialData);
                    var pointRadius = Array(data.length).fill(2);
                    var pointBorderColor = Array(data.length).fill(_chart._colorSelection[p % (_chart._colorSelection.length)]);
                    var sumAdder=0;
                    $.each(value, function (k, v) {
                        var labelIndexMatched = _chart._labels.indexOf(k);

                        if (labelIndexMatched != -1) {
                            if(_chart._operator[Object.keys(_chart._operator)[0]].key=="runTotal"){
                                sumAdder+=v;
                                if (!(v <= 0))
                                    data[labelIndexMatched] = sumAdder;
                            }else{
                                if (!(v <= 0))
                                    data[labelIndexMatched] += v;

                            }
                        }


                    });
                    var dt = {
                        label: key,
                        data: data,
                        backgroundColor: _chart._colorSelection[p % (_chart._colorSelection.length)],
                        borderColor: _chart._colorSelection[p % (_chart._colorSelection.length)],
                        showLine: true,
                        fill: true,
                        borderWidth: 2,
                        lineTension: 0,
                        pointBorderColor: pointBorderColor,
                        pointRadius: pointRadius
                    };
                    //     _chart._labels=labels
                    datasets.push(dt);
                    p++;
                });
                return datasets;
            }
            _chart.updateDatasetByGroupColor = function (finalData, groupKey) {

                var groupMappings = _chart.createGroupMappings(finalData, _chart);

                var labelsList = {};

                $.each(groupMappings, function (key, value) {
                    $.each(value, function (k, v) {



                        if (!(v == 0 || v < 0 || isNaN(v))) {

                            labelsList[k] = 1;


                        }


                    });
                });

                var initialData = [];
                var indexData = 0;
                var labelsListArray = [];

                $.each(labelsList, function (key, value) {
                    initialData[indexData] = null;
                    labelsListArray.push(key);
                    indexData++;
                });

                _chart._labels = labelsListArray;

                var p = 0;
                var datasets = [];


                $.each(groupMappings, function (key, value) {
                    var data = Object.assign([], initialData);

                    var sumAdder=0;
                    $.each(value, function (k, v) {
                        var labelIndexMatched = _chart._labels.indexOf(k);

                        if (labelIndexMatched != -1) {
                            if(_chart._operator[Object.keys(_chart._operator)[0]].key=="runTotal"){
                                sumAdder+=v;
                                if (!(v <= 0))
                                    data[labelIndexMatched] = sumAdder;
                            }else{
                                if (!(v <= 0))
                                    data[labelIndexMatched] += v;

                            }
                        }


                    });
                    var pointBorder = "#ccc";
                    var dt = {
                        label: key,
                        data: data,
                        backgroundColor: _chart._colorSelection[p % (_chart._colorSelection.length)],
                        borderColor: _chart._colorSelection[p % (_chart._colorSelection.length)],
                        showLine: true,
                        fill: true,
                        borderWidth: 2,
                        lineTension: 0,
                        //   pointBorderColor: _chart._data.datasets[p].pointBorderColor,
                        //    pointRadius: _chart._data.datasets[p].pointRadius
                    };

                    //     _chart._labels=labels
                    datasets.push(dt);

                    p++;

                });

                return datasets;
            }

            //M-End
            _chart.colorChange = function (selectedDataSet, color) {

                var labels = this.getLabels();
                var labelsLength = labels.length;
                var colorsLength = eChart.colors.length;
                var chartFiltersLength = 0;

                _chart._data.datasets.forEach(function (dataset, dsIndex) {

                    if (dataset.label == selectedDataSet.datasetLabel) {

                        _chart._colorSelection[dsIndex] = color;

                        _chart._data.datasets[dsIndex].backgroundColor = color;
                        _chart._data.datasets[dsIndex].borderColor = color;

                        _chart._data.datasets[dsIndex].pointBorderColor = Array(_chart._data.datasets[dsIndex].data.length).fill(color);

                    }
                });
                setTimeout(function () {
                    eChart.registerColor(_chart);
                    _chart.chartInstance.update();
                    var res = _chart.id.split("-");
                    eChart.generateLegend("left" + res[1], _chart);
                }, 1);

            };
            _chart.xAxes = function () {
                return eChart.renderxAxes(_chart._dataType, _chart._fontSize, _chart._dataFormateVal);
            };
            _chart.dataType = function (type) {
                _chart._dataType = type;
            };
            _chart.fontSize = function (fontSize) {
                _chart._fontSize = fontSize
            };
            _chart.fontFamily = function (fontFamily) {
                _chart._fontFamily = fontFamily;
            }
            _chart.yAxisPrefix = function (yAxis) {
                if (yAxis) {
                    _chart._yAxisPrefix = yAxis;
                } else {
                    _chart._yAxisPrefix = "";
                }
            }
            _chart.getDatasetByMeasure = function (finalData) {
                var datasets = [];
                var backgroundColorIndex = 0;
                var GroupColor = {};
                var backgroundColor = [];

                finalData.forEach(function (d) {
                    // if (j === 0)
                    // labels.push(d.key);
                    if (_chart._operator) {
                        data.push(d.value[_chart._operator[key].key]);
                    } else {
                        data.push(d.value);
                    }

                    if (d.value.groupColor != undefined) {

                        if (GroupColor[d.value.groupColor]) {
                            backgroundColor.push(GroupColor[d.value.groupColor]);
                        } else {
                            backgroundColor.push(_chart._colorSelection[backgroundColorIndex % (_chart._colorSelection.length)]);
                            GroupColor[d.value.groupColor] = _chart._colorSelection[backgroundColorIndex % (_chart._colorSelection.length)];
                            backgroundColorIndex++;
                        }
                    } else {
                        backgroundColor.push(_chart._colorSelection[j % (_chart._colorSelection.length)]);
                    }
                    //backgroundColor.push(eChart.colors[j]);
                    i++;
                });

                var dt = {
                    label: _chart._dataSetLabels[j],
                    data: data,
                    fill:true,
                    backgroundColor: backgroundColor,
                    borderColor: backgroundColor,
                    borderWidth: 1,
                    pointBorderColor: backgroundColor,
                    pointRadius: 2
                };
                datasets.push(dt);
                return datasets;
            }

            _chart.draw = function (context) {
                var instance = new Chart(context, {
                    type: 'line',
                    data: _chart._data,
                    options: {
                        responsive: true,
                        maintainAspectRatio: false,
                        pointDotRadius: 1,
                        elements: {
                            line: {
                                tension: 0, // disables bezier curves
                            },
                            point: {
                                backgroundColor: "#999"
                            }
                        },
                        bezierCurve: false,
                        legend: {
                            display: false
                        },
                        tooltips: {
                            position: 'nearest',
                            /*mode: 'index',*/
                            intersect: false,
                            enabled: false,
                            custom: function (tooltip) {
                                eChart.customTooltips(tooltip, _chart, this)
                            }
                        },
                        scales: {
                            yAxes: [{
                                //Label formate
                                stacked: false,
                                ticks: {
                                    beginAtZero: true,
                                    // Return an empty string to draw the tick line but hide the tick label
                                    // Return `null` or `undefined` to hide the tick line entirely
                                    userCallback: function (value, index, values) {
                                        // Convert the number to a string and splite the string every 3 charaters from the end

                                        // Convert the array to a string and format the output
                                        //value = value.join('.');
                                        if (_chart._yAxisPrefix) {
                                            value = value.toString();
                                            value = value.split(/(?=(?:...)*$)/);
                                            return _chart._yAxisPrefix + ' ' + value;
                                        } else {
                                            return value;
                                        }
                                    },
                                    fontFamily: 'sans-serif'
                                }
                            }],
                            xAxes: _chart.xAxes()

                            /*xAxes: xAxesArray*/
                        },

                        animation: {
                            // duration: 0 disable animation
                        },

                        pan: {
                            // Boolean to enable panning
                            enabled: false,

                            // Panning directions. Remove the appropriate direction to disable
                            // Eg. 'y' would only allow panning in the y direction
                            mode: 'xy'
                        },
                        // Container for zoom options
                        zoom: {
                            // Boolean to enable zooming
                            enabled: false,
                            sensitivity: 0,
                            // Zooming directions. Remove the appropriate direction to disable
                            // Eg. 'y' would only allow zooming in the y direction
                            mode: 'x'
                        },
                        fill:true
                    }
                });
                var res = id.split("-");
                _chart.chartInstance = instance;
                eChart.generateLegend("left" + res[1], _chart);


                eChart.registerChart(_chart);
            };

            _chart.bindEvents = function () {
                eChart.bindEvents(this);
            };

            _chart.highlightSelectedFilter = function () {
                var labels = this.getLabels();
                var labelsLength = labels.length;


                var chartFiltersLength = 0;
                if (!_chart.reset) {
                    chartFiltersLength = this.filters.length;
                }

                if (labelsLength == chartFiltersLength || chartFiltersLength === 0) {


                    _chart._data.datasets.forEach(function (dataset, dsIndex) {
                        _chart._data.datasets[dsIndex].backgroundColor = _chart._colorSelection[dsIndex];
                        labels.forEach(function (filter, i) {
                            try {
                                _chart._data.datasets[dsIndex].pointRadius[i] = 2;
                                _chart._data.datasets[dsIndex].pointBorderColor[i] = _chart._colorSelection[dsIndex];
                            } catch (e) {

                            }

                        });
                    });


                    _chart.filters = [];
                } else {
                    var filterLookup = {};
                    $.map(_chart.filters, function (value, index) {
                        if (!filterLookup[value['datasetIndex']]) {
                            filterLookup[value['datasetIndex']] = [];
                        }
                        filterLookup[value['datasetIndex']].push(value['label']);


                    });

                    labels.forEach(function (label, i) {


                        $.each(filterLookup, function (dsIndex, val) {
                            if (val.indexOf(label) != -1) {
                                try {
                                    _chart._data.datasets[dsIndex].pointRadius[i] = 5;
                                    _chart._data.datasets[dsIndex].pointBorderColor[i] = 'rgb(0,0, 0)';
                                } catch (e) {

                                }
                            }
                        });


                    });
                }



            };

            _chart.getLabels = function () {
                return this.chartInstance.config.data.labels;
            };
            _chart.dataSetLabels = function (labels) {
                _chart._dataSetLabels = labels;
                return _chart;
            };

//            _chart.highlightSelectedFilter = function () {
//
//
//            };
            _chart.getClickedElementLabel = function (evt) {
                var filter = {};
                try {
                    filter.label = this.getLabels()[this.chartInstance.getElementAtEvent(evt)[0]._index];
                    filter.datasetLabel = this.chartInstance.getElementAtEvent(evt)[0]._chart.config.data.datasets[this.chartInstance.getElementAtEvent(evt)[0]._datasetIndex].label;
                    filter.datasetIndex = this.chartInstance.getElementAtEvent(evt)[0]._datasetIndex;
                    return filter;
                } catch (e) {
                    return null;

                }
            };
            _chart.getDataByGroup = function () {

                var groups = _chart._group;


                var datasets = [];


                $.each(groups, function (key, grpObj) {

                    var finalData;

                    if (!_chart._sort) {
                        finalData = grpObj.all();
                        if(_chart.topN){

                            finalData =  eChart.getTopN(_chart,grpObj,key);

                        }

                    } else {
                        finalData = _chart.getSortedData(grpObj, key, _chart._sort);
                    }
                    if (eChart.isDataFormatApplied(_chart)) {
                        finalData = eChart.getDataByDateFormat(grpObj, key, _chart);
                    }
                    datasets = _chart.getDatasetByGroupColor(finalData, key);
                });

                var dataInFormat = {
                    labels: _chart._labels,
                    datasets: datasets
                };
                return dataInFormat;
            };
            _chart.getNormalData = function () {
                var colorsLength = eChart.colors.length;
                var groups = _chart._group;
                var labels = [];
                var datasets = [];
                var i = 0;
                var j = 0;
                var index = 0;
                $.each(groups, function (key, grpObj) {
                    var data = [];
                    var backgroundColor = [];
                    var borderColor = [];
                    var pointRadius = [];
                    var pointBorderColor = [];
                    var finalData;
                    if (_chart._sort == undefined) {
                        finalData = grpObj.all();
                        if(_chart.topN){

                            finalData =  eChart.getTopN(_chart,grpObj,key);

                        }
                    } else {
                        finalData = _chart.getSortedData(grpObj, key, _chart._sort);
                    }
                    if (eChart.isDataFormatApplied(_chart)) {
                        finalData = eChart.getDataByDateFormat(grpObj, key, _chart);
                    }
                    if(_chart._operator[key].key=="runTotal"){
                        finalData=eChart.getRunTotal(finalData);
                    }
                    finalData.forEach(function (d) {

                        if (j === 0)
                            labels.push(d.key);

                        if (_chart._operator) {

                            data.push(d.value[_chart._operator[key].key]);
                        } else {
                            data.push(d.value);
                        }

                        backgroundColor.push(_chart._colorSelection[j]);
                        pointRadius.push(2);
                        pointBorderColor.push(_chart._colorSelection[j]);
                        borderColor.push(_chart._colorSelection[j]);
                        //backgroundColor.push(eChart.colors[j]);
                        i++;
                    });
                    var dt = {
                        label: _chart._dataSetLabels[j],
                        data: data,
                        fill:true,
                        backgroundColor: backgroundColor[j],
                        borderColor: backgroundColor[j],
                        /*hoverBorderColor: borderColor[j],*/
                        pointBorderColor: pointBorderColor,
                        pointRadius: pointRadius,

                        borderWidth: 2
                    };

                    datasets.push(dt);
                    j++;
                    index++;
                });
                var dataInFormat = {
                    labels: labels,
                    datasets: datasets
                };
                return dataInFormat;
            };
            _chart.getDataInFormat = function (order) {
                if (_chart._dataFormateVal && _chart._dataFormateVal.tooltipSelector) {
                    eChart.processTooltipData(_chart);
                }
                var dataInFormat = {};
                eChart.updateAttributes(_chart);


                if (eChart.isGroupColorApplied(_chart)) {
                    dataInFormat = _chart.getDataByGroup();
                } else {
                    dataInFormat = _chart.getNormalData();
                }

                return dataInFormat;
            };

            return _chart;


        };


        // ------------------------------------Area  END--------------------------------------------------------------


        // -------------------------------------PIE CHART DRAW CODE--------------------------------------------------

        eChart.pieChart = function (id) {

            var _chart = {};
            _chart.id = id;
            _chart.dataBackup = [];
            _chart._colorSelection = $.extend([], eChart.colors);
            _chart.group = function (group) {

                _chart._group = group;
                _chart.data();
                return _chart;
            };
            _chart.type = "pieChart";
            _chart.dataFormate = function (dataFormate) {

                _chart._dataFormateVal = dataFormate;
                return _chart;
            };
            _chart.dimension = function (dimension) {
                _chart._dimension = dimension;
                return _chart;
            };

            _chart.data = function (data) {
                _chart._data = _chart.getDataInFormat();
                return _chart;
            };
            _chart.operator = function (operator) {
                _chart._operator = operator;
                return _chart;
            };

            _chart.render = function () {
                eChart.clearContainer(id);
                var bar = eChart.getCanvasContext(_chart, function (context) {
                    _chart.draw(context);
                    _chart.bindEvents();
                });
                return true;

            };


            _chart.draw = function (context) {

                var instance = new Chart(context, {
                    type: 'doughnut',
                    data: _chart._data,
                    options: {
                        responsive: true,
                        maintainAspectRatio: false,
                        legend: {
                            display: false,
                        },
                        title: {
                            display: true,
                            text: ''
                        },
                        animation: {},
                        pan: {
                            // Boolean to enable panning
                            enabled: false,

                            // Panning directions. Remove the appropriate direction to disable
                            // Eg. 'y' would only allow panning in the y direction
                            mode: 'xy'
                        },
                        // Container for zoom options
                        zoom: {
                            // Boolean to enable zooming
                            enabled: false,

                            // Zooming directions. Remove the appropriate direction to disable
                            // Eg. 'y' would only allow zooming in the y direction
                            mode: 'x',
                        },
                        tooltips: {
                            position: 'nearest',
                            /*mode: 'index',*/
                            intersect: false,
                            enabled: false,
                            custom: function (tooltip) {
                                eChart.customTooltips(tooltip, _chart, this)
                            }
                        },
                        legendCallback: function (chartInstance) {
                            var html = "<ul>";
                            chartInstance.legend.legendItems.forEach(function (d) {
                                html += '<li class=""><span style="background-color:' + d.fillStyle + '"></span>' + d.text + '</li>';
                                //strokeStyle
                            });
                            html += "</ul>"
                            return html;
                        }
                    }
                });
                var res = id.split("-");
                _chart.chartInstance = instance;

                eChart.generateLegend("left" + res[1], _chart);
                eChart.registerChart(_chart);
            };

            _chart.parseTooltipPieChart = function (format, xLabel, yLabel) {
                var formatText = format.replace("{xLabel}", xLabel);
                formatText = formatText.replace("{yLabel}", yLabel);
                return formatText;
            }

            _chart.colorChange = function (selectedDataSet, color) {

                var labels = this.getLabels();


                _chart._data.datasets.forEach(function (dataset, dsIndex) {

                    if (dataset.label == selectedDataSet.datasetLabel) {

                        _chart._colorSelection[dsIndex] = color;

                        var tempData = [];
                        /*labels.forEach(function(filter, i) {
                         tempData.push(color);
                         });*/
                        _chart._data.datasets[dsIndex].backgroundColor = color;
                        _chart._data.datasets[dsIndex].borderColor = color;
                    }
                });
                setTimeout(function () {
                    eChart.registerColor(_chart);
                    _chart.chartInstance.update();
                    var res = _chart.id.split("-");
                    eChart.generateLegend("left" + res[1], _chart);
                }, 1);
            };

            _chart.getDataInFormat = function () {
                if (_chart._dataFormateVal && _chart._dataFormateVal.tooltipSelector) {
                    eChart.processTooltipData(_chart);
                }
                eChart.updateAttributes(_chart);
                var colorsLength = eChart.colors.length;
                var groups = _chart._group;
                var labels = [];
                var datasets = [];
                var i = 0;
                var j = 0;

                var index = 0;

                $.each(groups, function (key, grpObj) {

                    var data = [];

                    var backgroundColor = [];

                    grpObj.all().forEach(function (d) {

                        if (index === 0)
                            labels.push(d.key);


                        if (_chart._operator) {
                            data.push(d.value[_chart._operator[key].key]);
                        } else {
                            data.push(d.value);
                        }
                        backgroundColor.push(eChart.colors[i % colorsLength]);
                        i++;

                    });

                    var dt = {

                        data: data,
                        backgroundColor: backgroundColor

                    };

                    datasets.push(dt);
                    j++;
                    index++;

                });

                var dataInFormat = {
                    labels: labels,
                    datasets: datasets
                };


                return dataInFormat;
            };

            _chart.highlightSelectedFilter = function () {
                var labels = this.getLabels();
                var labelsLength = labels.length;
                var colorsLength = eChart.colors.length;


                var chartFiltersLength = 0;
                if (!_chart.reset) {
                    chartFiltersLength = this.filters.length;
                }

                if (labelsLength == chartFiltersLength || chartFiltersLength === 0) {

                    labels.forEach(function (filter, i) {
                        _chart._data.datasets[0].backgroundColor[i] = eChart.colors[i % colorsLength];
                    });

                    _chart.filters = [];
                } else {
                    labels.forEach(function (filter, i) {

                        var index = eChart.getIndexOfObj(_chart.filters, eChart.constants.LABEL, filter);


                        if (index != -1) {


                            _chart._data.datasets[0].backgroundColor[i] = eChart.colors[i % colorsLength];

                        } else {


                            _chart._data.datasets[0].backgroundColor[i] = "#ddd";

                        }

                    });
                }


            };

            _chart.getClickedElementLabel = function (evt) {
                var filter = {};
                try {
                    filter.label = this.chartInstance.getElementAtEvent(evt)[0]._model.label;
                    filter.datasetLabel = null;
                    return filter;
                } catch (e) {

                    return null;
                }


            };
            _chart.bindEvents = function () {
                eChart.bindEvents(this);
            };


            _chart.getLabels = function () {
                return this.chartInstance.config.data.labels;
            };

            return _chart;

        };

        // ------------------------------------PIE END------------------------------------------------------------


        // ------------------------------------BUBBLE CHART---------------------------------------------------------
        eChart.bubbleChart = function (id) {
            var _chart = {};
            _chart.id = id;
            _chart.type="bubbleChart";
            _chart.dataBackup = [];
            _chart.group = function (group) {
                _chart._group = group;
                _chart.data();
                return _chart;
            };
            _chart.dimension = function (dimension) {
                _chart._dimension = dimension;
                return _chart;
            };

            _chart.data = function (data) {
                _chart._data = _chart.getDataInFormat();
                return _chart;
            };

            _chart.operator = function (operator) {
                _chart._operator = operator;
                return _chart;
            };

            _chart.dataSetLabels = function (labels) {
                _chart._dataSetLabels = labels;
                return _chart;
            };

            _chart.render = function () {
                eChart.clearContainer(id);
                var bar = eChart.getCanvasContext(_chart, function (context) {
                    _chart.draw(context);
                    _chart.bindEvents();
                });
                return true;
            };

            _chart.draw = function (context) {
                var instance = new Chart(context, {
                    type: 'bubble',
                    data: _chart._data,
                    options: {
                        responsive: true,
                        maintainAspectRatio: false,
                        legend: {
                            display: false
                        },
                        title: {
                            display: true,
                            text: ''
                        },
                        tooltips: {
                            callbacks: {
                                label: function(t, d) {
                                    return d.datasets[t.datasetIndex].label;
                                },
                                footer: function(tooltipItems, data) {
                                    var finalTip=[];
                                    _chart.groupLabels.forEach(function(d,index){
                                        var str="";
                                        if(index==0){
                                            str+=d+": ";
                                            str+=data.datasets[tooltipItems[0].datasetIndex].data[0].x+" ";
                                        }
                                        if(index==1){
                                            str+=d+": ";
                                            str+=data.datasets[tooltipItems[0].datasetIndex].data[0].y+" ";
                                        }
                                        if(index==2){
                                            if(data.datasets[tooltipItems[0].datasetIndex].data[0].v)
                                                str+=d+": ";
                                            str+=data.datasets[tooltipItems[0].datasetIndex].data[0].v+" ";
                                        }
                                        finalTip.push(str);
                                    });
                                    return finalTip;
                                }
                            }
                        }
                    },
                    animation: {},
                    pan: {
                        // Boolean to enable panning
                        enabled: false,
                        // Panning directions. Remove the appropriate direction to disable
                        // Eg. 'y' would only allow panning in the y direction
                        mode: 'xy'
                    },
                    // Container for zoom options
                    zoom: {
                        // Boolean to enable zooming
                        enabled: false,
                        // Zooming directions. Remove the appropriate direction to disable
                        // Eg. 'y' would only allow zooming in the y direction
                        mode: 'x',
                    }
                });
                var res = id.split("-");
                _chart.chartInstance = instance;
                eChart.generateLegend("left" + res[1], _chart);
                eChart.registerChart(_chart);
            };

            _chart.bindEvents = function () {
                eChart.bindEvents(this);
            };

            _chart.highlightSelectedFilter = function () {
                try {
                    var labels = this.getLabels();
                    var labelsLength = labels.length;
                    var colorsLength = eChart.colors.length;

                    var chartFiltersLength = 0;
                    if (!_chart.reset) {
                        chartFiltersLength = this.filters.length;
                    }

                    if (labelsLength == chartFiltersLength || chartFiltersLength === 0) {
                        _chart._data.datasets.forEach(function (dataset, dsIndex) {
                            labels.forEach(function (filter, i) {
                                _chart._data.datasets[dsIndex].backgroundColor[i] = eChart.colors[dsIndex];
                            });
                        });
                        _chart.filters = [];
                    } else {
                        labels.forEach(function (filter, i) {
                            var index = eChart.getIndexOfObj(_chart.filters, eChart.constants.LABEL, filter);
                            if (index != -1) {
                                var dsIndex = eChart.getIndexOfObj(_chart._data.datasets, eChart.constants.LABEL, _chart.filters[index].datasetLabel);
                                _chart._data.datasets[dsIndex].backgroundColor[i] = eChart.colors[dsIndex];
                            } else {
                                _chart._data.datasets.forEach(function (dataset, dsIndex) {
                                    _chart._data.datasets[dsIndex].backgroundColor[i] = "#ddd";
                                });
                            }
                        });
                    }
                } catch (e) {

                }
            };
            _chart.getClickedElementLabel = function (evt) {
                var filter = {};
                filter.label = this.chartInstance.getElementAtEvent(evt)[0]._chart.config.data.labels[this.chartInstance.getElementAtEvent(evt)[0]._datasetIndex];
                filter.datasetLabel = this.chartInstance.getElementAtEvent(evt)[0]._chart.config.data.labels[this.chartInstance.getElementAtEvent(evt)[0]._datasetIndex];
                return filter;
            };

            _chart.getLabels = function () {
                return this.chartInstance.config.data.labels;
            };

            _chart.getDataInFormat = function () {
                eChart.updateAttributes(_chart);
                var colorsLength = eChart.colors.length;
                var groups = _chart._group;
                var labels = [];
                var datasets = [];
                var j = 0;
                var index = 0;
                var data = {
                    x: 0,
                    y: 0,
                    r: 0
                };

                var dataMap = {};
                var dataTest = [];
                var datasetIndex = 0;
                _chart.groupLabels=[];
                var oldMin=0;
                var oldMax=0;
                var newMin=11;
                var newMax=30;

                $.each(groups, function (key, grpObj) {
                    _chart.groupLabels.push(key);
                    var grpData = grpObj.all();
                    var pointSelectFlag = true;
                    var initValue = 1;

                    if(datasetIndex==2){
                        oldMin=grpData[0].value[_chart._operator[key].key];
                        oldMax=grpData[0].value[_chart._operator[key].key];
                        grpData.forEach(function (d, index) {
                            var val=d.value[_chart._operator[key].key];
                            if(oldMax<val){
                                oldMax=val;
                            }
                            if(oldMin>val){
                                oldMin=val;
                            }
                        });
                    }

                    grpData.forEach(function (d, index) {
                        if (datasetIndex === 0) {
                            labels.push(d.key);
                            var data = {
                                x: 0,
                                y: 0,
                                r: 0
                            };
                            if (_chart._operator) {
                                data["x"] = grpData[index].value[_chart._operator[key].key];
                                data["y"] = grpData[index].value[_chart._operator[key].key];
                                data["r"] = 10;
                            } else {
                                data["x"] = grpData[index].value;
                                data["y"] = grpData[index].value;
                                data["r"] = grpData[index].value;
                            }
                            dataTest[index] = data;
                        }

                        if (datasetIndex === 1) {
                            if (_chart._operator) {
                                //  data["x"] = d.value[_chart._operator[key].key];
                                dataTest[index]["y"] = grpData[index].value[_chart._operator[key].key];
                                // data["z"] = d.value[_chart._operator[key].key];
                            } else {
                                //  data["x"] = d.value;
                                dataTest[index]["y"] = grpData[index].value;
                                //    data["z"] = d.value;
                            }
                        }

                        if (datasetIndex === 2) {
                            if (_chart._operator) {
                                // if (pointSelectFlag && grpData[index].value[_chart._operator[key].key] != 0) {
                                //     initValue = grpData[index].value[_chart._operator[key].key];
                                //     pointSelectFlag = false;
                                // }
                                //  data["x"] = d.value[_chart._operator[key].key];

                                var newRadius = (newMin + (grpData[index].value[_chart._operator[key].key] - oldMin) * (newMax - newMin) / (oldMax - oldMin)).toFixed(2);
                                dataTest[index]["r"] =newRadius;
                                dataTest[index]["v"]=grpData[index].value[_chart._operator[key].key];
                                // data["z"] = d.value[_chart._operator[key].key];
                            } else {
                                //  data["x"] = d.value;
                                dataTest[index]["r"] = d.value;
                                //    data["z"] = d.value;
                            }
                        }
                        dataMap[d.key] = dataTest[index];
                    });
                    datasetIndex++;

                });
                var newLabels=[];
                labels.forEach(function (label, i) {
                    j=0;
                    $.each(dataMap[label],function(key,val){
                        if(val==0){
                            j++;
                        }
                    });
                    if(j==0){
                        var dt = {
                            label: [label],
                            data: [dataMap[label]],
                            backgroundColor: eChart.colors[i % (eChart.colors.length)]
                        };
                        newLabels.push(label);
                        datasets.push(dt);
                    }
                });
                var dataInFormat = {
                    labels:  newLabels,
                    datasets: datasets
                };
                return dataInFormat;
            };

            _chart.updateDataset=function(){
                eChart.updateAttributes(_chart);
                var colorsLength = eChart.colors.length;
                var groups = _chart._group;
                var labels = [];
                var datasets = [];
                var j = 0;
                var index = 0;
                var data = {
                    x: 0,
                    y: 0,
                    r: 0
                };
                var dataMap = {};
                var dataTest = [];
                var datasetIndex = 0;
                _chart.groupLabels=[];
                var oldMin=1;
                var oldMax=1;
                var newMin=11;
                var newMax=30;
                $.each(groups, function (key, grpObj) {
                    _chart.groupLabels.push(key);
                    var grpData = grpObj.all();
                    var pointSelectFlag = true;
                    var initValue = 1;
                    if(datasetIndex==2){
                        oldMin=grpData[0].value[_chart._operator[key].key];
                        oldMax=grpData[0].value[_chart._operator[key].key];
                        grpData.forEach(function (d, index) {
                            var val=d.value[_chart._operator[key].key];
                            if(oldMax<val){
                                oldMax=val;
                            }
                            if(oldMin>val){
                                oldMin=val;
                            }
                        });
                    }
                    grpData.forEach(function (d, index) {
                        if (datasetIndex === 0) {
                            labels.push(d.key);
                            var data = {
                                x: 0,
                                y: 0,
                                r: 0
                            };
                            if (_chart._operator) {
                                data["x"] = grpData[index].value[_chart._operator[key].key];
                                data["y"] = grpData[index].value[_chart._operator[key].key];
                                data["r"] = 10;
                                data["v"] = 10;
                            } else {
                                data["x"] = grpData[index].value;
                                data["y"] = grpData[index].value;
                                data["r"] = grpData[index].value;
                                data["v"] = 10;
                            }
                            dataTest[index] = data;
                        }

                        if (datasetIndex === 1) {
                            if (_chart._operator) {
                                //  data["x"] = d.value[_chart._operator[key].key];
                                dataTest[index]["y"] = grpData[index].value[_chart._operator[key].key];
                                // data["z"] = d.value[_chart._operator[key].key];
                            } else {
                                //  data["x"] = d.value;
                                dataTest[index]["y"] = grpData[index].value;
                                //    data["z"] = d.value;
                            }
                        }
                        if (datasetIndex === 2) {
                            if (_chart._operator) {
                                // if (pointSelectFlag && grpData[index].value[_chart._operator[key].key] != 0) {
                                //     initValue = grpData[index].value[_chart._operator[key].key];
                                //     pointSelectFlag = false;
                                // }
                                //  data["x"] = d.value[_chart._operator[key].key];

                                var newRadius = (newMin + (grpData[index].value[_chart._operator[key].key] - oldMin) * (newMax - newMin) / (oldMax - oldMin)).toFixed(2);

                                dataTest[index]["r"] = newRadius;
                                dataTest[index]["v"]=grpData[index].value[_chart._operator[key].key];
                                // data["z"] = d.value[_chart._operator[key].key];
                            } else {
                                //  data["x"] = d.value;
                                dataTest[index]["r"] = d.value;
                                //    data["z"] = d.value;
                                dataTest[index]["v"]=d.value;
                            }
                        }
                        dataMap[d.key] = dataTest[index];
                    });
                    datasetIndex++;
                });

                labels.forEach(function (label, i) {
                    j=0;
                    $.each(dataMap[label],function(key,val){
                        if(val==0){
                            j++;
                        }
                    });
                    if(j==0){
                        var dt = {
                            label: [label],
                            data: [dataMap[label]],
                            backgroundColor: eChart.colors[i % (eChart.colors.length)]
                        };
                        datasets.push(dt);
                    }
                });
                _chart.chartInstance.config.data.datasets=datasets;
                _chart.chartInstance.config.data.labels=labels;
            }
            return _chart;
        };
        // --------------------------------------BUBBLE END---------------------------------------------------------------



        // -----------------------------------Composite START----------------------------------------------------------
        eChart.compositeChart = function (id) {


            var _chart = {};

            _chart.type = 'composite';

            _chart.id = id;

            _chart._sort = null;

            _chart._colorSelection = $.extend([], eChart.colors);

            _chart.group = function (group) {
                _chart._group = group;
                _chart.data();
                return _chart;
            };

            _chart.dimension = function (dimension) {
                _chart._dimension = dimension;
                return _chart;
            };

            _chart.dataFormate = function (dataFormat) {
                _chart._dataFormateVal = dataFormat;
                _chart.setDataFormat();
                return _chart;
            };

            _chart.dataSetLabels = function (labels) {
                _chart._dataSetLabels = labels;
                return _chart;
            };


            _chart.sort = function (order) {
                _chart._sort = order;
                eChart.updateSortData(_chart, order);
                eChart.registerDataOrder(_chart);
            };

            _chart.xAxes = function () {
                if(/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
                    _chart._fontSize=36;
                }
                if ((_chart._dataType == "date" || _chart._fontSize == "datetime") && _chart._dataFormateVal.xAxis == undefined) {
                    return [{
                        stacked: false,
                        ticks: {
                            fontSize: _chart._fontSize,
                            autoSkip: true,
                            maxTicksLimit: 10,
                            fontFamily: "sans-serif"
                        },
                        type: 'time',
                        unit: 'day',
                        unitStepSize: 1,
                        time: {
                            displayFormats: {
                                'day': 'MMM DD'
                            }
                        }
                    }];
                } else {
                    return [{
                        stacked: false,
                        ticks: {
                            fontSize: _chart._fontSize,
                            autoSkip: true,
                            maxTicksLimit: 10,
                            fontFamily: "sans-serif"
                        }
                    }];
                }
            };
            _chart.dataType = function (type) {
                _chart._dataType = type;
            };
            _chart.fontSize = function (fontSize) {
                _chart._fontSize = fontSize
            };
            _chart.fontFamily = function (fontFamily) {
                _chart._fontFamily = fontFamily;
            }
            _chart.yAxisPrefix = function (yAxis) {

                if (yAxis) {
                    _chart._yAxisPrefix = yAxis;
                } else {
                    _chart._yAxisPrefix = "";
                }

            }
            _chart.setDataFormat = function () {
                if (_chart._dataFormateVal.yAxis) {
                    _chart.yAxisPrefix(_chart._dataFormateVal.yAxis);
                } else {
                    _chart.yAxisPrefix("");
                }
                if (_chart._dataFormateVal.fontSize) {
                    _chart.fontSize(_chart._dataFormateVal.fontSize);
                } else {
                    _chart.fontSize(12);
                }
                _chart.groupColor = _chart._dataFormateVal.groupColor;

                _chart._dataType = _chart._dataFormateVal.dataType;

            }
            //---------------------------Group Color Start--------------------
            _chart.getDatasetByGroupColor = function (finalData, groupKey) {
                var groupMappings = _chart.createGroupMappings(finalData, _chart);

                var initialData = [];
                _chart._labels.forEach(function (d, i) {
                    initialData[i] = null;
                });


                var p = 0;
                var datasets = [];


                $.each(groupMappings, function (key, value) {
                    var data = Object.assign([], initialData);

                    $.each(value, function (k, v) {
                        var labelIndexMatched = _chart._labels.indexOf(k);
                        if (labelIndexMatched != -1) {
                            data[labelIndexMatched] = v;

                        }


                    });
                    var dt = {
                        label: key,
                        data: data,
                        backgroundColor: Array(data.length).fill(_chart._colorSelection[p % (_chart._colorSelection.length)]),
                        borderWidth: 1
                    };

                    //     _chart._labels=labels
                    datasets.push(dt);
                    p++;
                });
                return datasets;
            }
            _chart.updateDatasetByGroupColor = function (finalData, groupKey) {

                var groupMappings = _chart.createGroupMappings(finalData, _chart);

                var labelsList = {};
                $.each(groupMappings, function (key, value) {
                    $.each(value, function (k, v) {

                        if (!(v == 0 || v < 0 || isNaN(v))) {

                            labelsList[k] = 1;


                        }


                    });
                });


                var initialData = [];
                var indexData = 0;
                var labelsListArray = [];
                $.each(labelsList, function (key, value) {
                    initialData[indexData] = null;
                    labelsListArray.push(key);
                    indexData++;
                });

                if(eChart.isDataFormatApplied(_chart) && (_chart._dataFormateVal.xAxis == "MMMM" || _chart._dataFormateVal.xAxis == "MMM")){
                    _chart._labels = eChart.sortArray(labelsListArray);
                }else{
                    _chart._labels = labelsListArray;
                }


                var p = 0;
                var datasets = [];


                $.each(groupMappings, function (key, value) {
                    var data = Object.assign([], initialData);

                    $.each(value, function (k, v) {
                        var labelIndexMatched = _chart._labels.indexOf(k);
                        if (labelIndexMatched != -1) {
                            if (!(v <= 0))
                                data[labelIndexMatched] = v;

                        }


                    });
                    var dt = {
                        label: key,
                        data: data,
                        backgroundColor: Array(data.length).fill(_chart._colorSelection[p % (_chart._colorSelection.length)]),
                        borderWidth: 1
                    };
                    //     _chart._labels=labels
                    datasets.push(dt);
                    p++;
                });

                return datasets;
            }

            //---------------------------Group Color End-----------------------

            _chart.getSortedData = function (grpObj, key, order) {
                return eChart.getSortedData(_chart, grpObj, key, order);
            }

            _chart.createGroupMappings = function (finalData, _chart) {
                _chart._groupMappings = eChart.createGroupMappings(finalData, _chart);

                return _chart._groupMappings;
            }
            _chart.getDataByGroup = function () {
                var groups = _chart._group;
                var datasets = [];
                $.each(groups, function (key, grpObj) {

                    var finalData;

                    if (!_chart._sort) {
                        finalData = grpObj.all();
                    } else {
                        finalData = _chart.getSortedData(grpObj, key, order);
                    }
                    if (eChart.isDataFormatApplied(_chart)) {
                        finalData = eChart.getDataByDateFormat(grpObj, key, _chart);
                    }
                    if (eChart.isDataFormatApplied(_chart)) {
                        finalData = eChart.getDataByDateFormat(grpObj, key, _chart);
                    }
                    datasets = _chart.getDatasetByGroupColor(finalData, key);
                });

                var dataInFormat = {
                    labels: _chart._labels,
                    datasets: datasets
                };
                return dataInFormat;
            }
            _chart.getNormalData = function () {

                var colorsLength = eChart.colors.length;
                var groups = _chart._group;

                var labels = [];
                var datasets = [];
                var i = 0;
                var j = 0;
                var index = 0;
                $.each(groups, function (key, grpObj) {
                    var data = [];
                    var backgroundColor = [];
                    var borderColor = [];
                    var hoverBorderColor = [];
                    var finalData;


                    if (!_chart._sort) {
                        finalData = grpObj.all();

                    } else {

                        finalData = _chart.getSortedData(grpObj, key, _chart._sort);
                    }

                    if (eChart.isDataFormatApplied(_chart)) {
                        finalData = eChart.getDataByDateFormat(grpObj, key, _chart);
                    }
                    if(_chart._operator[key].key=="runTotal"){
                        finalData=eChart.getRunTotal(finalData);
                    }
                    finalData.forEach(function (d) {
                        if (j === 0)
                            labels.push(d.key);
                        if (_chart._operator) {
                            data.push(d.value[_chart._operator[key].key]);
                        } else {
                            data.push(d.value);
                        }
                        backgroundColor.push(_chart._colorSelection[j]);
                        hoverBorderColor.push("#000");
                        i++;
                    });
                    var dt = {
                        label: _chart._dataSetLabels[j],
                        data: data,
                        backgroundColor: backgroundColor,
                        borderColor: backgroundColor,
                        /*hoverBorderColor: hoverBorderColor,*/
                        borderWidth: 1
                    };
                    datasets.push(dt);
                    j++;
                });
                var dataInFormat = {
                    labels: labels,
                    datasets: datasets
                };
                return dataInFormat;
            }
            _chart.getDataInFormat = function (order) {
                try {
                    if (_chart._dataFormateVal && _chart._dataFormateVal.tooltipSelector) {
                        eChart.processTooltipData(_chart);
                    }
                } catch (e) {

                }

                eChart.updateAttributes(_chart);
                var dataInFormat = {};
                if (eChart.isGroupColorApplied(_chart)) {
                    dataInFormat = _chart.getDataByGroup();
                } else {
                    dataInFormat = _chart.getNormalData();
                }

                return dataInFormat;


            }

            _chart.data = function (data) {
                _chart._data = _chart.getDataInFormat();
                return _chart;
            };

            _chart.render = function () {
                eChart.clearContainer(id);
                var bar = eChart.getCanvasContext(_chart, function (context) {
                    _chart.draw(context);
                    _chart.bindEvents();
                });
            };


            _chart.draw = function (context) {
                var instance = new Chart(context, {
                    type: 'bar',
                    data: _chart._data,
                    options: eChart.createOption(_chart)
                });
                var res = id.split("-");
                _chart.chartInstance = instance;
                eChart.generateLegend("left" + res[1], _chart);
                eChart.registerChart(_chart);
            };

            _chart.operator = function (operator) {
                _chart._operator = operator;
                return _chart;
            };

            _chart.bindEvents = function () {
                eChart.bindEvents(this);
            };

            _chart.highlightSelectedFilter = function () {
                var labels = this.getLabels();
                var labelsLength = labels.length;
                var colorsLength = eChart.colors.length;
                var chartFiltersLength = 0;
                if (!_chart.reset) {
                    chartFiltersLength = this.filters.length;
                }

                if (labelsLength == chartFiltersLength || chartFiltersLength === 0) {
                    _chart._data.datasets.forEach(function (dataset, dsIndex) {
                        labels.forEach(function (filter, i) {
                            _chart._data.datasets[dsIndex].backgroundColor[i] = _chart._colorSelection[dsIndex % colorsLength];
                        });
                    });
                    _chart.filters = [];
                } else {
                    var datasetsArray = [];
                    var datasetWiseFilters = {};

                    _chart.filters.forEach(function (filter) {
                        if (!datasetWiseFilters[filter.datasetIndex])
                            datasetWiseFilters[filter.datasetIndex] = [];
                        datasetWiseFilters[filter.datasetIndex].push(filter.label);
                    });

                    _chart._data.datasets.forEach(function (dataset, dsIndex) {
                        var filterLabelsArray = datasetWiseFilters[dsIndex];
                        if (filterLabelsArray) {
                            labels.forEach(function (label, index) {
                                if (filterLabelsArray.indexOf(label) == -1) {

                                    dataset.backgroundColor[index] = "#ddd";
                                } else {
                                    dataset.backgroundColor[index] = _chart._colorSelection[dsIndex % colorsLength];
                                }

                            });

                        } else {
                            dataset.backgroundColor = Array(labels.length).fill("#ddd");
                        }

                    });


                }
            };

            _chart.colorChange = function (selectedDataSet, color) {
                var labels = this.getLabels();
                _chart._data.datasets.forEach(function (dataset, dsIndex) {
                    if (dataset.label == selectedDataSet.datasetLabel) {
                        _chart._colorSelection[dsIndex] = color;
                        var tempData = [];
                        labels.forEach(function (filter, i) {

                            tempData.push(color);
                        });
                        _chart._data.datasets[dsIndex].backgroundColor = tempData;
                        _chart._data.datasets[dsIndex].borderColor = tempData;
                    }
                });
                setTimeout(function () {
                    _chart.chartInstance.update();
                    eChart.registerColor(_chart);
                    var res = _chart.id.split("-");
                    eChart.generateLegend("left" + res[1], _chart);
                }, 1);
            };
            _chart.getClickedElementLabel = function (evt) {
                var filter = {};
                try {
                    filter.label = this.chartInstance.getElementAtEvent(evt)[0]._model.label;
                    filter.datasetLabel = this.chartInstance.getElementAtEvent(evt)[0]._model.datasetLabel;
                    filter.datasetIndex = this.chartInstance.getElementAtEvent(evt)[0]._datasetIndex;
                    return filter;
                } catch (e) {

                    return null;
                }


            };

            _chart.getLabels = function () {
                return this.chartInstance.config.data.labels;
            };
            return _chart;

        }

        // ------------------------------------Composite END------------------------------------------------------------

        // -----------------------------------Stacked START-------------------------------------------------------------
        eChart.stackedChart = function (id) {
            var _chart = {};
            _chart.id = id;
            _chart.dataBackup = [];
            _chart.group = function (group) {
                _chart._group = group;
                _chart.data();
                return _chart;
            };

            _chart.dimension = function (dimension) {
                _chart._dimension = dimension;
                return _chart;
            }

            _chart.data = function (data) {
                _chart._data = _chart.getDataInFormat();
                return _chart;
            };

            _chart.operator = function (operator) {
                _chart._operator = operator;
                return _chart;
            }

            _chart.render = function () {
                eChart.clearContainer(id);
                var bar = eChart.getCanvasContext(_chart, function (context) {
                    _chart.draw(context);
                    _chart.bindEvents();
                });
                return true;
            };

            _chart.draw = function (context) {
                var instance = new Chart(context, {
                    type: 'bar',
                    data: _chart._data,
                    options: {
                        scales: {
                            xAxes: [{
                                stacked: true
                            }],
                            yAxes: [{
                                stacked: true,
                                ticks: {
                                    beginAtZero: true
                                }
                            }],
                        },
                        animation: {},
                        pan: {
                            // Boolean to enable panning
                            enabled: false,

                            // Panning directions. Remove the appropriate direction to disable
                            // Eg. 'y' would only allow panning in the y direction
                            mode: 'xy'
                        },
                        // Container for zoom options
                        zoom: {
                            // Boolean to enable zooming
                            enabled: false,

                            // Zooming directions. Remove the appropriate direction to disable
                            // Eg. 'y' would only allow zooming in the y direction
                            mode: 'x',
                        }
                    }
                });
                _chart.chartInstance = instance;
                eChart.registerChart(_chart);
            };

            _chart.bindEvents = function () {
                eChart.bindEvents(this);
            };

            _chart.getClickedElementLabel = function (evt) {
                return this.chartInstance.getElementAtEvent(evt)[0]._model.label;
            };

            _chart.getLabels = function () {
                return this.chartInstance.config.data.labels;
            };

            _chart.dataSetLabels = function (labels) {
                _chart._dataSetLabels = labels;
                return _chart;
            };

            _chart.getDataInFormat = function () {
                var colorsLength = eChart.colors.length;
                var groups = _chart._group;
                var labels = [];
                var datasets = [];
                var i = 0;
                var j = 0;
                eChart.updateAttributes(_chart);
                groups.forEach(function (grpObj, index) {
                    var data = [];
                    var backgroundColor = [];
                    grpObj.all().forEach(function (d) {
                        if (index == 0)
                            labels.push(d.key);
                        if (_chart._operator) {
                            data.push(d.value[_chart._operator[key].key]);
                        } else {
                            data.push(d.value);
                        }
                        backgroundColor.push(eChart.colors[j]);
                        i++;
                    });
                    /* label : _chart._dataSetLabels[j].value, */
                    var dt = {
                        label: _chart._dataSetLabels[j].value,
                        data: data,
                        backgroundColor: backgroundColor
                    };
                    datasets.push(dt);
                    j++;
                });
                var dataInFormat = {
                    labels: labels,
                    datasets: datasets
                };

                return dataInFormat;
            }
            return _chart;
        };
        // ------------------------------------Stacked END------------------------------------------------------------
        // ------------------------------------Heat map-------------------------------------------------------------
        eChart.heatChart = function (id) {

            var data = {
                labels: ['0h', '1h', '2h', '3h', '4h', '5h', '6h', '7h', '8h', '9h', '10h', '11h'],
                datasets: [
                    {
                        label: 'Monday',
                        data: [8, 6, 5, 7, 9, 8, 1, 6, 3, 3, 8, 7]
                    },
                    {
                        label: 'Tuesday',
                        data: [6, 8, 5, 6, 5, 5, 7, 0, 0, 3, 0, 7]
                    },
                    {
                        label: 'Wednesday',
                        data: [8, 5, 6, 4, 2, 2, 3, 0, 2, 0, 10, 8]
                    },
                    {
                        label: 'Thursday',
                        data: [4, 0, 7, 4, 6, 3, 2, 4, 2, 10, 8, 2]
                    },
                    {
                        label: 'Friday',
                        data: [1, 0, 0, 7, 0, 4, 1, 3, 4, 5, 1, 10]
                    }
                ]
            };
            $("<canvas id='canvas-1' ></canvas>").appendTo($("#chart-" + id));
            var ctx = document.getElementById("canvas-" + id).getContext('2d');
            var newChart = new Chart(ctx).HeatMap(data, "");
        };

        // ------------------------------------Heat map end-----------------------------------------------------------


        // ------------------------------------Line Bar Chart Start---------------------------------------------------

        eChart.lineBarChart = function (id) {

            var _chart = {};

            _chart.type = 'lineBarChart';

            _chart.id = id;

            _chart._sort = null;

            _chart._colorSelection = $.extend([], eChart.colors);

            _chart.group = function (group) {
                _chart._group = group;
                _chart.data();
                return _chart;
            };
            _chart.dimension = function (dimension) {

                _chart._dimension = dimension;
                return _chart;
            };

            _chart.dataFormate = function (dataFormat) {

                _chart._dataFormateVal = dataFormat;
                _chart.setDataFormat();
                return _chart;
            };
            _chart.dataSetLabels = function (labels) {
                _chart._dataSetLabels = labels;
                return _chart;
            };


            _chart.sort = function (order) {
                _chart._sort = order;
                eChart.updateSortData(_chart, order);
                eChart.registerDataOrder(_chart);
            };
            _chart.xAxes = function () {
                if(/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
                    _chart._fontSize=36;
                }
                if ((_chart._dataType == "date" || _chart._fontSize == "datetime") && _chart._dataFormateVal.xAxis == undefined) {
                    return [{
                        stacked: true,
                        ticks: {
                            fontSize: _chart._fontSize,
                            autoSkip: true,
                            maxTicksLimit: 10
                        },
                        type: 'time',
                        unit: 'day',
                        unitStepSize: 1,
                        time: {
                            displayFormats: {
                                'day': 'MMM DD'
                            }
                        }
                    }];
                } else {
                    return [{
                        stacked: true,
                        ticks: {
                            fontSize: _chart._fontSize,
                            autoSkip: true,
                            maxTicksLimit: 10
                        }
                    }];
                }
                //return eChart.renderxAxes(_chart._dataType,_chart._fontSize);
            };
            _chart.yAxes=function(chart){
                return eChart.renderyAxes(chart);
            }
            _chart.dataType = function (type) {
                _chart._dataType = type;
            };
            _chart.fontSize = function (fontSize) {
                _chart._fontSize = fontSize
            };
            _chart.fontFamily = function (fontFamily) {
                _chart._fontFamily = fontFamily;
            }
            _chart.yAxisPrefix = function (yAxis) {
                if (yAxis) {
                    _chart._yAxisPrefix = yAxis;
                } else {
                    _chart._yAxisPrefix = "";
                }

            }
            _chart.setDataFormat = function () {
                if (_chart._dataFormateVal.yAxis) {
                    _chart.yAxisPrefix(_chart._dataFormateVal.yAxis);
                } else {
                    _chart.yAxisPrefix("");
                }
                if (_chart._dataFormateVal.fontSize) {
                    _chart.fontSize(_chart._dataFormateVal.fontSize);
                } else {
                    _chart.fontSize(13);
                }
                _chart.groupColor = _chart._dataFormateVal.groupColor;

                _chart._dataType = _chart._dataFormateVal.dataType;
                _chart._chartType = _chart._dataFormateVal.chartType;

            }
            //---------------------------Group Color Start--------------------
            _chart.getDatasetByGroupColor = function (finalData, groupKey) {
                var groupMappings = _chart.createGroupMappings(finalData, _chart);

                var initialData = [];
                _chart._labels.forEach(function (d, i) {
                    initialData[i] = null;
                });


                var p = 0;
                var datasets = [];


                $.each(groupMappings, function (key, value) {
                    var data = Object.assign([], initialData);

                    $.each(value, function (k, v) {
                        var labelIndexMatched = _chart._labels.indexOf(k);
                        if (labelIndexMatched != -1) {
                            data[labelIndexMatched] = v;
                        }


                    });
                    var dt = {
                        label: key,
                        data: data,
                        backgroundColor: Array(data.length).fill(_chart._colorSelection[p % (_chart._colorSelection.length)]),
                        borderWidth: 1
                    };

                    //     _chart._labels=labels
                    datasets.push(dt);
                    p++;
                });
                return datasets;
            }
            _chart.updateDatasetByGroupColor = function (finalData, groupKey) {

                var groupMappings = _chart.createGroupMappings(finalData, _chart);
                var labelsList = {};

                $.each(groupMappings, function (key, value) {
                    $.each(value, function (k, v) {



                        if (!(v == 0 || v < 0 || isNaN(v))) {

                            labelsList[k] = 1;


                        }


                    });
                });


                var initialData = [];
                var indexData = 0;
                var labelsListArray = [];
                $.each(labelsList, function (key, value) {
                    initialData[indexData] = null;
                    labelsListArray.push(key);
                    indexData++;
                });

                if(eChart.isDataFormatApplied(_chart) && (_chart._dataFormateVal.xAxis == "MMMM" || _chart._dataFormateVal.xAxis == "MMM")){
                    _chart._labels = eChart.sortArray(labelsListArray);
                }else{
                    _chart._labels = labelsListArray;
                }


                var p = 0;
                var datasets = [];


                $.each(groupMappings, function (key, value) {
                    var data = Object.assign([], initialData);

                    $.each(value, function (k, v) {
                        var labelIndexMatched = _chart._labels.indexOf(k);
                        if (labelIndexMatched != -1) {
                            if (!(v <= 0))
                                data[labelIndexMatched] = v;

                        }


                    });
                    var dt = {
                        label: key,
                        data: data,
                        backgroundColor: Array(data.length).fill(_chart._colorSelection[p % (_chart._colorSelection.length)]),
                        borderWidth: 1
                    };
                    //     _chart._labels=labels
                    datasets.push(dt);
                    p++;
                });

                return datasets;
            }
            //---------------------------Group Color End-----------------------

            _chart.getSortedData = function (grpObj, key, order) {
                return eChart.getSortedData(_chart, grpObj, key, order);
            }

            _chart.createGroupMappings = function (finalData, _chart) {
                _chart._groupMappings = eChart.createGroupMappings(finalData, _chart);

                return _chart._groupMappings;
            }
            _chart.getDataByGroup = function () {
                var groups = _chart._group;
                var datasets = [];
                $.each(groups, function (key, grpObj) {

                    var finalData;

                    if (!_chart._sort) {
                        finalData = grpObj.all();
                    } else {
                        finalData = _chart.getSortedData(grpObj, key,_chart._sort);
                    }
                    if (eChart.isDataFormatApplied(_chart)) {
                        finalData = eChart.getDataByDateFormat(grpObj, key, _chart);
                    }
                    if (eChart.isDataFormatApplied(_chart)) {
                        finalData = eChart.getDataByDateFormat(grpObj, key, _chart);
                    }
                    datasets = _chart.getDatasetByGroupColor(finalData, key);
                });

                var dataInFormat = {
                    labels: _chart._labels,
                    datasets: datasets
                };
                return dataInFormat;
            }
            _chart.getNormalData = function () {

                //  var colorsLength = eChart.colors.length;
                var groups = _chart._group;

                var labels = [];
                var datasets = [];
                var i = 0;
                var j = 0;
                //   var index = 0;
                $.each(groups, function (key, grpObj) {
                    var data = [];
                    var backgroundColor = [];
                    //  var borderColor = [];
                    var hoverBorderColor = [];
                    var finalData;


                    if (!_chart._sort) {
                        finalData = grpObj.all();

                    } else {

                        finalData = _chart.getSortedData(grpObj, key, _chart._sort);
                    }

                    if (eChart.isDataFormatApplied(_chart)) {
                        finalData = eChart.getDataByDateFormat(grpObj, key, _chart);
                    }
                    if(_chart._operator[key].key=="runTotal"){
                        finalData=eChart.getRunTotal(finalData);
                    }
                    finalData.forEach(function (d) {
                        if (j === 0)
                            labels.push(d.key);
                        if (_chart._operator) {
                            data.push(d.value[_chart._operator[key].key]);
                        } else {
                            data.push(d.value);
                        }

                        backgroundColor.push(_chart._colorSelection[j]);
                        hoverBorderColor.push("#000");
                        i++;
                    });
                    var type;
                    if (_chart._chartType) {
                        if (_chart._chartType[key]) {
                            type = _chart._chartType[key];
                        } else {
                            var types = ['line', 'bar']
                            type = types[(Math.random() * types.length) | 0];
                        }
                    } else {
                        var types = ['line', 'bar']
                        type = types[(Math.random() * types.length) | 0];
                    }

                    if (type == "line") {
                        backgroundColor = backgroundColor[j];
                    }
                    var dt = {
                        type: type,
                        fill: false,
                        label: _chart._dataSetLabels[j],
                        data: data,
                        backgroundColor: backgroundColor,
                        borderColor: backgroundColor,
                        hoverBorderColor: hoverBorderColor,
                        borderWidth: 1,

                    };
                    datasets.push(dt);
                    j++;
                });
                var dataInFormat = {
                    labels: labels,
                    datasets: datasets
                };
                return dataInFormat;
            }
            _chart.getDataInFormat = function (order) {

                if (_chart._dataFormateVal && _chart._dataFormateVal.tooltipSelector) {
                    eChart.processTooltipData(_chart);
                }
                eChart.updateAttributes(_chart);
                var groups = _chart._group;

                var labels = [];
                var datasets = [];

                var i = 0;
                var j = 0;
                var dataInFormat = {};
                if (eChart.isGroupColorApplied(_chart)) {
                    dataInFormat = _chart.getDataByGroup();
                } else {
                    dataInFormat = _chart.getNormalData();
                }
                return dataInFormat;
                //}

            }

            _chart.data = function (data) {

                _chart._data = _chart.getDataInFormat();
                return _chart;
            };

            _chart.render = function () {
                eChart.clearContainer(id);
                var bar = eChart.getCanvasContext(_chart, function (context) {
                    _chart.draw(context);
                    _chart.bindEvents();
                });
            };

            _chart.draw = function (context) {
                var instance = new Chart(context, {
                    type: 'bar',
                    data: _chart._data,
                    options: eChart.createOption(_chart)
                });
                var res = id.split("-");
                _chart.chartInstance = instance;
                eChart.generateLegend("left" + res[1], _chart);


                eChart.registerChart(_chart);
            };

            _chart.operator = function (operator) {
                _chart._operator = operator;
                return _chart;
            };

            _chart.bindEvents = function () {
                eChart.bindEvents(this);
            };

            _chart.highlightSelectedFilter = function () {
                var labels = this.getLabels();
                var labelsLength = labels.length;
                // var colorsLength = eChart.colors.length;


                var chartFiltersLength = 0;
                if (!_chart.reset) {
                    chartFiltersLength = this.filters.length;
                }
                if (labelsLength == chartFiltersLength || chartFiltersLength === 0) {

                    try {
                        _chart._data.datasets.forEach(function (dataset, dsIndex) {
                            labels.forEach(function (filter, i) {
                                if (_chart._data.datasets[dsIndex].type == 'bar') {
                                    _chart._data.datasets[dsIndex].backgroundColor[i] = _chart._colorSelection[dsIndex];
                                }

                            });
                        });
                    } catch (e) {
                        
                    }
                    _chart.filters = [];
                } else {

                    labels.forEach(function (filter, i) {
                        var index = eChart.getIndexOfObj(_chart.filters, eChart.constants.LABEL, filter);
                        if (index != -1) {
                            var dsIndex = eChart.getIndexOfObj(_chart._data.datasets, eChart.constants.LABEL, _chart.filters[index].datasetLabel);
                            //  _chart._data.datasets[dsIndex].backgroundColor[i] = _chart._colorSelection[dsIndex];
                            _chart._data.datasets.forEach(function (dataset, dsIndex) {
                                if (dataset.type == 'bar') {
                                    _chart._data.datasets[dsIndex].backgroundColor[i] = _chart._colorSelection[dsIndex];
                                }

                            });

                        } else {

                            _chart._data.datasets.forEach(function (dataset, dsIndex) {
                                if (dataset.type == 'bar') {
                                    _chart._data.datasets[dsIndex].backgroundColor[i] = "#ddd";
                                }
                            });
                        }
                    });


                }
            };

            _chart.colorChange = function (selectedDataSet, color) {
                var labels = this.getLabels();
                _chart._data.datasets.forEach(function (dataset, dsIndex) {
                    if (dataset.label == selectedDataSet.datasetLabel) {
                        _chart._colorSelection[dsIndex] = color;
                        var tempData = [];
                        labels.forEach(function (filter, i) {

                            tempData.push(color);
                        });
                        _chart._data.datasets[dsIndex].backgroundColor = tempData;
                        _chart._data.datasets[dsIndex].borderColor = tempData;
                    }
                });
                setTimeout(function () {
                    _chart.chartInstance.update();
                    eChart.registerColor(_chart);
                    var res = _chart.id.split("-");
                    eChart.generateLegend("left" + res[1], _chart);
                }, 1);
            };
            _chart.getClickedElementLabel = function (evt) {
                var filter = {};
                try {
                    filter.label = this.getLabels()[this.chartInstance.getElementAtEvent(evt)[0]._index];
                    filter.datasetLabel = this.chartInstance.getElementAtEvent(evt)[0]._chart.config.data.datasets[this.chartInstance.getElementAtEvent(evt)[0]._datasetIndex].label;
                    filter.datasetIndex = this.chartInstance.getElementAtEvent(evt)[0]._datasetIndex;

                    return filter;
                } catch (e) {

                    return null;
                }


            };

            _chart.getLabels = function () {
                return this.chartInstance.config.data.labels;
            };
            return _chart;
        };
        // -------------------------------------LINE BAR CHART END---------------------------------------------------------------




        return eChart;
    }
    this.eChart = _eChart(Chart);
})();