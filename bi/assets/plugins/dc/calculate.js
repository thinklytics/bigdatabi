/**
 * Created by mitesh.panchal on 8/3/2017.
 */
// Chart Js With Crossfilter .....................//
// .........By Mitesh Panchal.....................//
(function () {

    function _calculate() {
        'use strict';

        var calculate = {
            VERSION: "1.0.0",
            constants: {
                DELAY: 4,
                NEGLIGIBLE_NUMBER: 1e-10,
            },
            _data: null,
            _dataGroupsParams: {},
            _specialFunctionKeywordList: {
                'if(': {
                    evaluate: function (expression, data, keyName) {
                        if((new RegExp(/group\(((sum|SUM|count|COUNT|avg|AVG)\(\[([a-z|A-Z|0-9|_|' '|'('|')']*)\]\))\)/g)).test(expression)){
                            calculate.applyGroupInIf(expression, data, keyName);
                        }else{
                            calculate.applyNormalIf(expression, data, keyName);
                        }
                    }
                }

            }
        };


        calculate.applyGroupInIf=function(expression, data, keyName){
            calculate.preProcessFunctions(expression);
            var groupedData = {};
            if (!$.isEmptyObject(calculate.getDataGroupParams())) {
                var dataGroupParams=calculate.getDataGroupParams();

                var dataLength= data.length;
                data.forEach(function (d,index) {

                    var groupKey=d[sketch._activeKey];
                    if(!groupedData[groupKey])
                    {
                        groupedData[groupKey]={};

                    }
                    $.each(dataGroupParams,function(key,value){

                        if(value=='sum'){
                            if(!groupedData[groupKey][key])
                                groupedData[groupKey][key]=0;

                            if(d[key]==null){
                                d[key]=0;
                            }
                            groupedData[groupKey][key]+=parseFloat(d[key]);
                        }
                        if(value=='count'){
                            if(!groupedData[groupKey][key])
                                groupedData[groupKey][key]=0;

                            // if(d[key]==null){
                            //     d[key]=0;
                            // }
                            groupedData[groupKey][key]+=1;
                        }

                        if(value=='avg'){
                            if(dataLength==(index+1))
                            { if(!calculate._tempAvgKey){
                                calculate._tempAvgKey=[];
                            }

                                calculate._tempAvgKey.push(key);
                            }

                            //Group Sum Part......

                            if(!groupedData[groupKey][key+"_sum"])
                                groupedData[groupKey][key+"_sum"]=0;

                            if(d[key]==null){
                                d[key]=0;
                            }
                            groupedData[groupKey][key+"_sum"]+=parseFloat(d[key]);

                            //Group Count Part.....



                            if(!groupedData[groupKey][key+"_count"])
                                groupedData[groupKey][key+"_count"]=0;
                            groupedData[groupKey][key+"_count"]+=1;

                        }

                    });
                });

                if(calculate._tempAvgKey && calculate._tempAvgKey.length!=0){
                    calculate._tempAvgKey.forEach(function(avgKey,i){

                        $.each(groupedData,function(key,d){
                            d[avgKey]=d[avgKey+"_sum"]/d[avgKey+"_count"];
                        });
                    });

                }

                calculate._tempAvgKey=null;



                $.each(groupedData,function(key,d){

                    var res=eval(calculate._abstractExpression);
                    if(isNaN(res))
                    {
                        d['newCalculation']=0;
                    }else{
                        d['newCalculation']=res;
                    }

                });
                var tempSearchObj={};

                data.forEach(function (d) {
                    var groupKey=d[sketch._activeKey];
                    if(!tempSearchObj[groupKey]){
                        tempSearchObj[groupKey]=true;
                        d[keyName]=groupedData[groupKey]['newCalculation'];
                    }else{
                        d[keyName]=0;
                    }


                });
                try{
                    self.postMessage(data);
                }catch(e){

                }


            }

        }

        calculate.applyNormalIf=function(expression, data, keyName){
            var indexStart = expression.indexOf("(");
            var indexEnd = expression.lastIndexOf("){");
            var conditionalExpression = calculate.preProcessToken(expression.substring(indexStart + 1, indexEnd));
            var indexStartIn = expression.indexOf("{");
            var indexEndInIf = expression.lastIndexOf("}");
            var elseExecutingExpression = null;

            if (expression.includes('else{')) {
                indexEndInIf = expression.lastIndexOf("}else");

                var indexStartInElse = expression.indexOf("else{");
                var indexEndInElse = expression.lastIndexOf("}");

                var result = expression.substring(indexStartInElse + 5, indexEndInElse);
                var result = result.trim();
                elseExecutingExpression = calculate.preProcessToken(result);

            }

            var executingExpression = calculate.preProcessToken(expression.substring(indexStartIn + 1, indexEndInIf));


            data.forEach(function (d) {

                if (eval(conditionalExpression)) {

                    var res = eval(executingExpression);

                }
                else {

                    if (elseExecutingExpression) {
                        res = eval(elseExecutingExpression);
                    }

                }
                d[keyName] = res;

            });


        }

        calculate.getGroupCalculations=function(data,flag){
            var groupedData = {};
            if (!$.isEmptyObject(calculate.getDataGroupParams())) {
                var dataGroupParams=calculate.getDataGroupParams();

                  
                data.forEach(function (d) {
                    var groupKey=d[sketch._activeKey];
                    if(!groupedData[groupKey])
                    {
                        groupedData[groupKey]={};

                    }
                    $.each(dataGroupParams,function(key,value){

                        if(value=='sum'){
                            if(!groupedData[groupKey][key])
                                groupedData[groupKey][key]=0;

                            if(d[key]==null){
                                d[key]=0;
                            }
                            if (eval(conditionalExpression)) {
                                groupedData[groupKey][key] += d[key];
                            }
                        }
                        if(value=='count'){
                            if(!groupedData[groupKey][key])
                                groupedData[groupKey][key]=0;


                            groupedData[groupKey][key] += 1;

                        }

                    });
                });
                var newFieldVal='newCalculation';
                if(flag){
                    newFieldVal='newElseCalculation';
                }

                $.each(groupedData,function(key,d){
                    var res=eval(calculate._abstractExpression);
                    d[newFieldVal]=res;
                });


                return Object.assign({},groupedData);


            }
        }

         calculate.processExpression = function (expression, data, keyName,column) {

            var concatVar = "";
            calculate._data = data;

            try {
                var flag = false;
                /*$.each(calculate._specialFunctionKeywordList, function (keyword, val) {
                 if (expression.includes(keyword)) {

                 val.evaluate(expression, data, keyName);
                 flag = true;

                 return false;
                 }
                 });*/

                
                if (!flag) {

                    var executingExpression = calculate.preProcessToken(expression);
                    var groupedData = {};
                    if (!$.isEmptyObject(calculate.getDataGroupParams())) {
                        var dataGroupParams=calculate.getDataGroupParams();
                        function getGroupedKey(d){
                            var tempString=undefined;
                            if(sketch._activeKey && sketch._activeKey.length>0){
                                tempString="";
                                sketch._activeKey.forEach(function(key,index){
                                    tempString+="||"+d[key];
                                });
                            }
                            return tempString;
                        }


                        //   if(index==0){
                        groupedData=d3.nest().key(function(d){return getGroupedKey(d);});
                        //    }else{
                        //     groupedData=groupedData.key(function(d){return d[key];});
                        //  }


                        function checkValIntegrity(val){
                            if(!val || isNaN(val)){
                                val=0;
                            }
                            else{
                                val=Math.round(val);
                            }
                            return val;
                        }


                        function processedReturnObj(v){
                            var tempObj={};
                            $.each(dataGroupParams,function(key,value){
                                if(value=="sum"){
                                    tempObj[key]=d3.sum(v,function(d){
                                        return checkValIntegrity(d[key]);
                                    });
                                }
                                if(value=="count"){


                                    tempObj[key]=d3.sum(v,function(d){
                                        if(d[key])
                                            return 1;
                                        else
                                            return 0;
                                    });

                                }
                                if(value=="avg"){
                                    tempObj[key]=d3.mean(v,function(d){
                                        return checkValIntegrity(d[key]);
                                    });
                                }


                            });
                            return tempObj;
                        }


                        groupedData=groupedData.rollup(function(v){
                            return processedReturnObj(v);
                        });

                        groupedData=groupedData.entries(data);
                        var testData={};
                        groupedData.forEach(function(d){
                            testData[d.key]=d.values;
                        });
                        groupedData=testData;
                        // data.forEach(function (d,index) {

                        //     if(!groupedData[groupKey])
                        //     {
                        //         groupedData[groupKey]={};

                        //     }
                        //     $.each(dataGroupParams,function(key,value){

                        //         if(value=='sum'){
                        //             if(!groupedData[groupKey][key])
                        //                 groupedData[groupKey][key]=0;

                        //             if(d[key]==null){
                        //                 d[key]=0;
                        //             }
                        //             groupedData[groupKey][key]+=parseFloat(d[key]);
                        //         }
                        //         if(value=='count'){
                        //             if(!groupedData[groupKey][key])
                        //                 groupedData[groupKey][key]=0;

                        //             // if(d[key]==null){
                        //             //     d[key]=0;
                        //             // }
                        //             groupedData[groupKey][key]+=1;
                        //         }

                        //         if(value=='avg'){
                        //             if(dataLength==(index+1))
                        //             { if(!calculate._tempAvgKey){
                        //                 calculate._tempAvgKey=[];
                        //             }

                        //                 calculate._tempAvgKey.push(key);
                        //             }

                        //             //Group Sum Part......
                        //             if(!groupedData[groupKey][key+"_sum"])
                        //                 groupedData[groupKey][key+"_sum"]=0;

                        //             if(d[key]==null){
                        //                 d[key]=0;
                        //             }
                        //             groupedData[groupKey][key+"_sum"]+=parseFloat(d[key]);

                        //             //Group Count Part.....



                        //             if(!groupedData[groupKey][key+"_count"])
                        //                 groupedData[groupKey][key+"_count"]=0;
                        //             groupedData[groupKey][key+"_count"]+=1;

                        //         }

                        //     });
                        // });

                        // if(calculate._tempAvgKey && calculate._tempAvgKey.length!=0){
                        //     calculate._tempAvgKey.forEach(function(avgKey,i){
                        //         $.each(groupedData,function(key,d){
                        //             d[avgKey]=d[avgKey+"_sum"]/d[avgKey+"_count"];
                        //         });
                        //     });

                        // }
                        calculate._tempAvgKey=null;

                        $.each(groupedData,function(key,d){

                            var res=eval(calculate._abstractExpression);
                            // if(isNaN(res))
                            // {
                            //     d['newCalculation']=0;
                            // }else{
                                d['newCalculation']=res;
                            // }

                        });
                        var tempSearchObj={};

                        if(column && column.dataKey=="Dimension"){
                             data.forEach(function (d) {
                            var groupKey=getGroupedKey(d);
                            //   if(groupKey){
                            if(!tempSearchObj[groupKey]){
                                tempSearchObj[groupKey]=true;
                                d[keyName]=groupedData[groupKey]['newCalculation'];
                            }else{
                                 d[keyName]=groupedData[groupKey]['newCalculation'];
                               
                            }
                            // }
                          });
                        }else{
                             data.forEach(function (d) {
                            var groupKey=getGroupedKey(d);
                            //   if(groupKey){
                            if(!tempSearchObj[groupKey]){
                                tempSearchObj[groupKey]=true;
                                d[keyName]=groupedData[groupKey]['newCalculation'];
                            }else{
                                 d[keyName]=0;
                            }
                            });
                            // }
                        }
                       
                        

                    }else {
                        //  setTimeout(function(d){
                        data.forEach(function (d) {
                            var res = eval(executingExpression);
                            d[keyName] = res;

                        });
                        //        },2000);

                    }

                }
                calculate._abstractExpression=null;
                calculate._dataGroupsParams={};
                calculate.cachedSum = null;
                calculate.cachedCount = null;
                calculate.cachedAvg = null;
                return false;
            } catch (e) {
                return e.message;
            }
        }

        calculate.getDataGroupParams = function () {
            return calculate._dataGroupsParams;
        }


        
        calculate.preProcessToken = function (expr) {
            calculate.preProcessFunctions(expr);
            var expression = expr
                .replace(/isNull[(]/g,
                    "calculate.isEmpty(")
                .replace(/isNotNull[(]/g,
                    "calculate.isEmpty(")
                .replace(/floor[(]/g,
                    "Math.floor(")
                .replace(/ceil[(]/g,
                    "ceil(")
                .replace(/decimalToInt[(]/g,
                    "parseInt(")
                .replace(/concat[(]/g,
                    "concatVar.concat(")
                .replace(/strlen[(]/g,
                    "calculate.strlen(")
                .replace(/dateDiff[(]/g,
                    "calculate.dateDiff(")
                .replace(/toLower[(]/g,
                    "calculate.toLower(")
                .replace(/toString[(]/g,
                    "calculate.toString(")
                .replace(/trim[(]/g,
                    "calculate.trim(")

                .replace(/replace[(]/g,
                    "calculate.replace(")
                .replace(/round[(]/g,
                    "calculate.round(")
                .replace(/parseDate[(]/g,
                    "calculate.parseDate(")
                .replace(/formatDate[(]/g,
                    "calculate.formatDate(")
                .replace(/toUpper[(]/g,
                    "calculate.toUpper(")
                .replace(/DateAdd[(]/g,
                    "calculate.DateAdd(")
                .replace(/return/g, "")
                .replace(/\[/g,"calculate.checkValue(d['")
                .replace(/dateTimeDiff[(]/g,"calculate.dateTimeDiff(") 
                .replace(/\]/g, "'])")
                .replace(/currDate[(]/g,"calculate.currentDate(");

            return expression.trim();
        };
        //Manual calculation function
        //Date time Diff 
        calculate.dateTimeDiff=function(format,date1,date2){
           var ms = moment(date2,"DD/MM/YYYY HH:mm:ss").diff(moment(date1,"DD/MM/YYYY HH:mm:ss"));
           var d = moment.duration(ms);
           var s = d.format(format);   
           return s;  
        }
        //Date time interval incress
        calculate.DateAdd=function(type,interval,date,dateFormat){
            var dateObj=new Date(date);
            if(!dateFormat){var dateFormat='DD-MM-YYYY';}
            var momentObj=moment(dateObj).add(interval,type);
            return momentObj.format(dateFormat);
        }
        calculate.round = function (value) {
            return Math.round(value);
        };
        calculate.isEmpty = function (val) {
            return (val === undefined|| val == null || val.length <= 0) ? true : false;
        };
        calculate.replace = function (expression, substring, replacement) {
            return expression.replace(
                substring,
                replacement);
        };
        calculate.parseDate = function (dateVal, format) {
            if (format == null) {
                return moment(dateVal).toDate();
            } else {
                return moment(dateVal, format).toDate();
            }
        };
        calculate.formatDate = function (date, format) {
            if (format == null) {
                return moment(date).format('YYYY-MM-DD');
            } else {
                return moment(date).format(format);
            }
        };
        calculate.currentDate=function(){
            return moment();
        }
        calculate.currentDate=function(){
            return new Date();
        }

        calculate.checkValue = function (value) {

            if (value == null) {
                return 0;
            }
            return value;
        }
        calculate.strlen = function (expression) {
            if (expression != 0) {
                return expression.length;
            } else {
                return 0;
            }
        };

        calculate.toLower = function (expression) {

            try{
                if (!Number(expression) && expression!=null && expression!="") {
                    return expression.toLowerCase();
                } else {
                    return "na";
                }
            }catch(e){

            }

        };
        calculate.toUpper = function (expression) {
            if (!Number(expression) && expression!=null && expression!="") {
                var newString = expression.toUpperCase();
                return newString;
            } else {
                return "NA";
            }

        };
        calculate.toString = function (expression) {
            return expression.toString();
        };
        calculate.trim = function (expression) {
            if (!Number(expression) && expression!=null && expression!="") {
                var newString = expression.trim();
                return newString;
            } else {
                return "NA";
            }
        };
        calculate.dateDiff = function (getDate1, getDate2,format) {
            if(format!=undefined){ 
                var startDate = moment(getDate1, format);
                var endDate = moment(getDate2, format);
           }else{
               var startDate = moment(getDate1);
                var endDate = moment(getDate2); 
           }  
           return endDate.diff(startDate, 'days');
        };
        /*calculate.ltrim=function(expression) {
         var trimmed = expression.replace(/^\s+/g,'');
         return trimmed;
         };
         calculate.rtrim=function(expression) {
         var trimmed = expression.replace(/\s+$/g,'');
         return trimmed;
         }; */
        //End manual calculation function

        calculate.sum = function (expression) {
            return calculate.cachedSum;
        };
        calculate.count = function (expression) {

            return calculate.cachedCount;
        };
        calculate.avg = function (expression) {
            return calculate.cachedAvg;
        };


        calculate.evaluateTokens = function (expr) {

            var processedExpression = calculate.preProcessToken(expr);
            return eval(processedExpression);
        }

        calculate.validate = function (expression) {
            s = expression;
            var matches = [];
            var pattern = /\[(.*?)\]/g;
            var match;
            while ((match = pattern.exec(s)) != null) {
                matches.push(match[1]);
            }

        }


        calculate.applyDataGroupOn = function (key, operator) {
            calculate._dataGroupsParams[key] = operator;
        }



        calculate.preProcessFunctions = function (expression) {

            //  if (expression.includes("group")) {

            const  regex = /(sum|SUM|count|COUNT|avg|AVG)\(\[([a-z|A-Z|0-9|_|' '|'('|')']*)\]\)?/g;
            const str = expression;
            var flagGroup=false;
            calculate._abstractExpression=expression;
            var m;

            while ((m = regex.exec(str)) !== null) {


                // This is necessary to avoid infinite loops with zero-width matches
                if (m.index === regex.lastIndex) {
                    regex.lastIndex++;
                }

                calculate.applyDataGroupOn(m[2], m[1]);
                calculate._abstractExpression=calculate._abstractExpression.replace(m[0],'d[\''+m[2]+'\']');
                flagGroup=true;
                // The result can be accessed through the `m`-variable.

            }

            if(!flagGroup)
            {

                calculate._dataGroupsParams={};
            }
            



            //    }
            //   else {

            //     if (!calculate.cachedSum) {
            //         var sum = 0;
            //
            //         const regex = /sum\(\[\w+\]\)/;
            //
            //         var m;
            //         var str = null;
            //         if ((m = regex.exec(expression)) !== null) {
            //             // The result can be accessed through the `m`-variable.
            //             m.forEach(function (match, groupIndex) {
            //
            //                 str = match.substring(match.indexOf('[') + 1, match.lastIndexOf(']'));
            //
            //             });
            //         }
            //         if (str) {
            //             calculate._data.forEach(function (d) {
            //
            //                 if (d[str]) {
            //                     sum += d[str];
            //                 }
            //
            //             });
            //         }
            //
            //         calculate.cachedSum = sum;
            //     }
            //     if (!calculate.cachedCount) {
            //         var count = 0;
            //
            //         const regex = /count\(\[\w+\]\)/;
            //
            //         var m;
            //         var str = null;
            //         if ((m = regex.exec(expression)) !== null) {
            //             // The result can be accessed through the `m`-variable.
            //             m.forEach(function (match, groupIndex) {
            //                 str = match.substring(match.indexOf('[') + 1, match.lastIndexOf(']'));
            //
            //             });
            //         }
            //         if (str) {
            //             calculate._data.forEach(function (d) {
            //
            //                 if (d[str]) {
            //                     count++;
            //                 }
            //
            //             });
            //         }
            //
            //         calculate.cachedCount = count;
            //     }
            //     if (!calculate.cachedAvg) {
            //         var count = 0;
            //         var sum = 0;
            //         const regex = /avg\(\[\w+\]\)/;
            //
            //         var m;
            //         var str = null;
            //         if ((m = regex.exec(expression)) !== null) {
            //             // The result can be accessed through the `m`-variable.
            //             m.forEach(function (match, groupIndex) {
            //                 str = match.substring(match.indexOf('[') + 1, match.lastIndexOf(']'));
            //
            //             });
            //         }
            //
            //         if (str) {
            //             calculate._data.forEach(function (d) {
            //
            //                 if (d[str]) {
            //                     count++;
            //                     sum += d[str];
            //                 }
            //
            //             });
            //         }
            //         calculate.cachedAvg = sum / count;
            //     }
            //
            // }




        }
        return calculate;

    }

    this.calculate = _calculate();
})();