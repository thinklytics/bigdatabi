// Canvas object Start
var canvas = {
    canvasWidth : 1100,
    canvasHeight : 1100,
    rowHeight : 60,
    colLength : 200,
    rectSetting : {
        x : 50,
        y : 50,
        width : 35,
        height : 40,
        fill : '#fff',
        stroke : 'black',
        strokeWidth : 1,
        col : 0,
        row : 0,
        lineOffset : 25,
    },
    rectArray:[],
};
// Canvas object End







//Draw Arrow Start
canvas.drawArrow = function(parent, child, titleAttr){
    // console.log(titleAttr)
    // var parentRow = parent.attrs.row;
    // var childRow = child.attrs.row;
    var parentX = parent.attrs.x;
    // var parentY = parent.attrs.y;
    var childX = child.attrs.x;
    var childY = child.attrs.y;
    var arrow;
    if(childX == 250){
        arrow = new Konva.Arrow({
            points : [parentX+canvas.rectSetting.width+10, childY+(canvas.rectSetting.height/2), childX, childY+(canvas.rectSetting.height/2)],
            stroke : 'black',
            strokeWidth : 1,
            fill : 'black',
            pointerLength : 7,
            pointerWidth : 7,
            id : titleAttr,
        });
    }else{
        arrow = new Konva.Arrow({
            points : [childX-160, childY+(canvas.rectSetting.height/2), childX, childY+(canvas.rectSetting.height/2)],
            stroke : 'black',
            strokeWidth : 1,
            fill : 'black',
            pointerLength : 7,
            pointerWidth : 7,
            id : titleAttr,
        });
    }
    return arrow;
}
//Draw Arrow End







//Draw Turn Line Bottom Start
canvas.drawTurnLine_Bottom = function(parent, child, titleAttr){
    // console.log(titleAttr);
    var childY = child.attrs.y;
    var BottomLine = new Konva.Line({
        points : [-110, childY-40, 1070, childY-40],
        stroke : 'black',
        strokeWidth : 1,
        id : titleAttr,
    });
    return BottomLine;
}
//Draw Turn Line Bottom End







//Draw Turn Line Left Start
canvas.drawTurnLine_Left = function(parent, child, titleAttr){
    // console.log(titleAttr);
    var childY = child.attrs.y;
    var LeftLine = new Konva.Line({
        points : [0, childY-40, 0, childY+20],
        stroke : 'black',
        strokeWidth : 3,
        id : titleAttr,
    });
    return LeftLine;
}
//Draw Turn Line Left End







//Draw Turn Line Right Start
canvas.drawTurnLine_Right = function(parent, child, titleAttr){
    // console.log(titleAttr);
    var childY = child.attrs.y;
    var Right_Line = new Konva.Line({
        points : [1070, childY-85, 1070, childY-40],
        stroke : 'black',
        strokeWidth : 2,
        id : titleAttr,
    });
    return Right_Line;
}
//Draw Turn Line Right End







//Draw Icon Start
canvas.iconDraw = function(rect, ImgObj, titleAttr, position, processCounter){
    // console.log(position, processCounter, operationCounter, titleAttr, ImgObj);
    return new Konva.Text({
        x : rect.attrs.x + 10,
        y : rect.attrs.y + 12,
        text : ImgObj.icon,
        fontSize : 20,
        fontFamily : 'FontAwesome',
        fill : ImgObj.color,
        id : titleAttr,
        positionIndex : position,
        processIndex : processCounter,
    });
}
//Draw Icon End







//Draw Root React Start
canvas.getRootRect = function(titleAttr, title, dropCounter){
    // console.log(dropCounter, title, titleAttr);
    return new Konva.Rect({
        x : canvas.rectSetting.x,
        y : canvas.rectSetting.y,
        width : canvas.rectSetting.width,
        height : canvas.rectSetting.height,
        fill : canvas.rectSetting.fill,
        stroke : 'white',
        opacity : 0,
        strokeWidth : canvas.rectSetting.strokeWidth,
        col : canvas.rectSetting.col,
        row : canvas.rectSetting.row,
        id : titleAttr,
        title : title,
        isRoot : true,
        childList : {},
        lastChildRow : null,
        addChild : function(titleAttr, title, dropCounter){
            // console.log(dropCounter, title, titleAttr);
            var rect;
            var newRow;
            var newCol;
            var x;
            var y;
            if(dropCounter%6 == 0){
                newRow = this.row + 2;
                newCol = 0;
            }else{
                newRow = this.row;
                newCol = this.col+1;
            }
            this.col = newCol;
            this.row = newRow;
            x = canvas.rectSetting.x+newCol*canvas.colLength;
            y = canvas.rectSetting.y+newRow*canvas.rowHeight;
            rect = canvas.getRectangle(x, y, newRow, newCol, titleAttr, title);
            this.childList[titleAttr] = rect;
            return rect;
        },
        removeObject : function(){
            if(this.lastChildRow>=-1){
                this.lastChildRow--;
                if(this.lastChildRow==-1){
                    this.lastChildRow=null;
                }
            }
        }
    });
}

canvas.getRectangle = function(x, y, row, col, id, title){
    // console.log(x, y, row, col, id, title);
    return new Konva.Rect({
        x : x,
        y : y,
        width : canvas.rectSetting.width,
        height : canvas.rectSetting.height,
        fill : canvas.rectSetting.fill,
        stroke : 'white',
        opacity : 0,
        strokeWidth : canvas.rectSetting.strokeWidth,
        col : col,
        row : row,
        id : id,
        title : title,
        isRoot : false,
        childList : {},
        lastChildRow : 0,
    });
}
//Draw Root React End





