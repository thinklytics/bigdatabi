//Canvas object
var canvas={
    canvasWidth:800,
    canvasHeight:2000,
    rowHeight:60,
    colLength:200,
    rectSetting:{
        x: 50,
        y: 50,
        width:100,
        height:40,
        fill:'#fff',
        stroke:'#ddd',
        strokeWidth:1,
        col: 0,
        row: 0,
        lineOffset:25
    },

    rectArray:[]

};

//Draw line function
canvas.drawLine=function(parent,child,id)
{
    var parentRow=parent.attrs.row;
    var childRow=child.attrs.row;
    var parentX= parent.attrs.x;
    var parentY= parent.attrs.y;
    var childX= child.attrs.x;
    var childY= child.attrs.y;
    var line;
    if(parentRow==childRow)
    {
        line = new Konva.Line({

            points: [parentX+canvas.rectSetting.width, parentY+(canvas.rectSetting.height/2),childX, childY+(canvas.rectSetting.height/2)],
            stroke: 'blue',
            strokeWidth: 1,
            id: id
        });
    }
    else {
        line = new Konva.Line({
            points: [parentX+canvas.rectSetting.width+canvas.rectSetting.lineOffset, parentY+(canvas.rectSetting.height/2),parentX+canvas.rectSetting.width+canvas.rectSetting.lineOffset,childY+(canvas.rectSetting.height/2),childX,childY+(canvas.rectSetting.height/2)],
            stroke: 'blue',
            strokeWidth: 1,
            id: id
        });
    }
    return line;
}
//Draw text function
function getWords(str) {
    return str.slice(0,10);
}
canvas.textDraw=function(rect,text){
    var textCount=text.length;
    var newText=text;
    if(textCount>10){
        newText=getWords(newText)+"...";
    }
    return new Konva.Text({
        x: rect.attrs.x+10,
        y: rect.attrs.y+12,
        text: newText,
        fontSize: 14,
        fontFamily: 'Roboto,sans-serif',
        fill: '#757575',
        id : text
    });
}
//Draw image
var imageObj = new Image();
canvas.drawImage=function(rect,id) {

    return  new Konva.Image({
        x: rect.attrs.x-canvas.colLength/3,
        y: rect.attrs.y+canvas.rectSetting.height/6-6,
        image: imageObj,
        width: 60,
        height: 40,
        id : id
    });

}
//End draw image
canvas.getRootRect= function(id){
    return new Konva.Rect({
        x: canvas.rectSetting.x,
        y: canvas.rectSetting.y,
        width: canvas.rectSetting.width,
        height: canvas.rectSetting.height,
        fill: canvas.rectSetting.fill,
        stroke: canvas.rectSetting.stroke,
        strokeWidth: canvas.rectSetting.strokeWidth,
        col : canvas.rectSetting.col,
        row : canvas.rectSetting.row,
        id : id,
        isRoot:true,
        childList:{},
        lastChildRow:null,
        getChildNextRow:function(){

            return this.lastChildRow++;
        },
        addChild:function(id){
            var rect;
            var newRow;
            var newCol;
            var x;
            var y;
            if(this.lastChildRow==null)
            {

                newRow=this.row;
                newCol=this.col+1;
                x=canvas.rectSetting.x+newCol*canvas.colLength;
                y=canvas.rectSetting.y+newRow*canvas.rowHeight;
                this.lastChildRow=0;
                rect = canvas.getRectangle(x,y,newRow,newCol,id);
                this.childList["test"+x]=rect;
            }
            else {

                this.lastChildRow++;
                newRow=this.lastChildRow;
                newCol=this.col+1;
                x=canvas.rectSetting.x+newCol*canvas.colLength;
                y=canvas.rectSetting.y+newRow*canvas.rowHeight;
                rect = canvas.getRectangle(x,y,newRow,newCol,id);
                this.childList[id]=rect;
            }


            return rect;
        },removeObject:function(){
            if(this.lastChildRow>=-1){
                //  console.log(this.lastChildRow);
                console.log(this.lastChildRow);
                this.lastChildRow--;
                if(this.lastChildRow==-1)
                    this.lastChildRow=null;
                // console.log(this.lastChildRow);
            }
        }
    });
}
canvas.getRectangle= function(x,y,row,col,id){

    return new Konva.Rect({
        x: x,
        y: y,
        width: canvas.rectSetting.width,
        height: canvas.rectSetting.height,
        fill: canvas.rectSetting.fill,
        stroke: canvas.rectSetting.stroke,
        strokeWidth: canvas.rectSetting.strokeWidth,
        col : col,
        row : row,
        id : id,
        isRoot:false,
        childList:{},
        lastChildRow:0,
        getChildNextRow:function(){
            return this.lastChildRow+1;
        },

        addChild:function(){
            var rect;
            if(!this.lastChildRow)
            {
                rect= canvas.getRectangle(this.x+this.width+canvas.colLength,this.y,this.row,this.col+1,id);
                this.childList["test"+this.x]=rect;

            }
            else {

                rect= canvas.getRectangle(this.x+this.width+canvas.colLength,this.y,this.getChildNextRow(),this.col+1,id);
                this.childList["test"+this.x]=rect;
            }

            return rect;
        }
    });


}
var updateElement=function(elem,group,root){
    newRow=elem.attrs.row;
    newCol=elem.attrs.col;
    var id=elem.attrs.id;
    var x=canvas.rectSetting.x+newCol*canvas.colLength;
    var y=canvas.rectSetting.y+newRow*canvas.rowHeight;

    var textElem=group.find('Text')[0];
    var lineElem=group.find('Line')[0];
    console.log(lineElem);

    textElem.setX(x+10);
    textElem.setY(y+15);
    elem.setX(x);
    elem.setY(y);

    lineElem.setPoints(canvas.drawLine(root,elem,id).attrs.points);

    imageElem=group.find('Image')[0];
    imageElem.setX(x-canvas.colLength/3);

    imageElem.setY(y+canvas.rectSetting.height/6-4);
    return {x:x,y:y};
}
