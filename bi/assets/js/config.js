/* ============================================================
 * File: config.js
 * Configure routing
 * ============================================================ */

angular.module('app')
    .config(['$stateProvider', '$urlRouterProvider', '$ocLazyLoadProvider',
        function($stateProvider, $urlRouterProvider, $ocLazyLoadProvider, $rootScope) {
            $urlRouterProvider.otherwise('/Login');
            $stateProvider
                .state('app', {
                    abstract: true,
                    url: "/app",
                    templateUrl: "tpl/app.html"
                })
                /*
                 *******************Login Routes to redirect to login page************************
                 */
                .state('thinklaytics', {
                    template: '<div class="full-height" ui-view></div>'
                })
                //-------------------------login state---------------------------------------------
                .state('thinklaytics.login', {
                    url: '/Login',
                    templateUrl: "html/login/login.html",
                    controller:"loginController",
                    permission:"login",
                    resolve: {
                        deps: ['$ocLazyLoad', function($ocLazyLoad) {
                            return $ocLazyLoad.load([
                                'js'
                            ], {
                                insertBefore: '#lazyload_placeholder'
                            }).then(function() {
                                return $ocLazyLoad.load([
                                    'theme/assets/css/demo.css',
                                    'theme/assets/css/turbo.css',
                                    'assets/js/controllers/login.js'
                                ]);
                            });
                        }]
                    }
                })

                //-------------------------------Signup-------------------------------
                .state('thinklaytics.signup', {
                    url: '/SignUp',
                    templateUrl: "html/signup/signup.html",
                    controller:"signupController",
                    permission:"signup",
                    resolve: {
                        deps: ['$ocLazyLoad', function($ocLazyLoad) {
                            return $ocLazyLoad.load([
                                'js'
                            ], {
                                insertBefore: '#lazyload_placeholder'
                            }).then(function() {
                                return $ocLazyLoad.load([
                                    'theme/assets/css/bootstrap.min.css',
                                    'theme/assets/css/demo.css',
                                    'theme/assets/css/turbo.css',
                                    'assets/js/controllers/signup.js'
                                ]);
                            });
                        }]
                    }
                })
                //*********************End Loign Signup**************************************


                /*
                 *********************Main route**************************
                 */
                .state('datasource', {
                    url: "/datasource",
                    template: '<div class="container-fluid" ui-view></div>'
                })
                //-------------------Datasource index route-----------------------------------
                .state('datasource.index', {
                    url: "/",
                    templateUrl: "html/datasource/index.html",
                    controller: 'datasourceCtrl',
                    permission:"datasource-list",
                    resolve: {
                        deps: ['$ocLazyLoad','$rootScope', function($ocLazyLoad,$rootScope) {
                            return $ocLazyLoad.load([
                                'js'
                            ], {
                                insertBefore: '#lazyload_placeholder'
                            }).then(function() {
                                return $ocLazyLoad.load([
//                                        'animation',
                                    'assets/js/controllers/datasource.js'
                                ]);
                            });
                        }]
                    }
                })
                .state('datasource.add', {
                    url: "/add",
                    templateUrl: "html/datasource/add.html",
                    controller: 'datasourceCtrl',
                    permission:"datasource-add",
                    resolve: {
                        deps: ['$ocLazyLoad', function($ocLazyLoad) {
                            return $ocLazyLoad.load([
                                'js'
                            ], {
                                insertBefore: '#lazyload_placeholder'
                            }).then(function() {
                                return $ocLazyLoad.load([
//                                        'animation',
                                    'assets/js/controllers/datasource.js'
                                ]);
                            });
                        }]
                    }
                })
                //----------------------------------------End datasource-------------------------


                /*
                 *********************************** Metadata Routes *********************************
                 */
                .state('metadata', {
                    url: "/metadata",
                    template: '<div class="full-height" ui-view></div>'
                })
                //-------------------Metadata index route-----------------------------------
                .state('metadata.index', {
                    url: "/",
                    templateUrl: "html/metadata/index.html",
                    controller: 'metadataViewController',
                    permission:"metadata-list",
                    resolve: {
                        deps: ['$ocLazyLoad', function($ocLazyLoad) {
                            return $ocLazyLoad.load([
                                'js'
                            ], {
                                insertBefore: '#lazyload_placeholder'
                            }).then(function() {
                                return $ocLazyLoad.load([
                                    'assets/js/controllers/metadata/metadataViewController.js'
                                ]);
                            });
                        }]
                    }
                })
                //-------------------Metadata Add route-----------------------------------
                .state('metadata.add', {
                    url: "/add",
                    templateUrl: "html/metadata/add.html",
                    controller: 'metadataController',
                    permission:"metadata-add",
                    resolve: {
                        deps: ['$ocLazyLoad', function($ocLazyLoad) {
                            return $ocLazyLoad.load([
                                'js'
                            ], {
                                insertBefore: '#lazyload_placeholder'
                            }).then(function() {
                                return $ocLazyLoad.load([
                                    'konvaJs',
                                    'assets/js/controllers/metadata/metadataController.js'
                                ]);
                            });
                        }]
                    }
                })
                //-------------------metadata edit-----------------------------------------------------
                .state('metadata.copy', {
                    url: '/copy/:data/:copy',
                    templateUrl: "html/metadata/copy.html",
                    controller: 'metadataEditController',
                    permission:"metadata-edit",
                    resolve: {
                        deps: ['$ocLazyLoad', function($ocLazyLoad) {
                            return $ocLazyLoad.load([
                                'js'
                            ], {
                                insertBefore: '#lazyload_placeholder'
                            }).then(function() {
                                return $ocLazyLoad.load([
                                    'konvaJs',
                                    'assets/js/controllers/metadata/metadataEditController.js'
                                ]);
                            });
                        }]
                    }

                })
                //-------------------Datasource to metadata Add route-----------------------------------
                .state('metadata.addDatasource', {
                    url: "/add/:id/:tableName",
                    templateUrl: "tpl/new/metadata-add.html",
                    controller: 'metadataController',
                    permission:"metadata-add",
                    resolve: {
                        deps: ['$ocLazyLoad', function($ocLazyLoad) {
                            return $ocLazyLoad.load([
                                'select',
                                'multiselect',
                                'dataTables',
                            ], {
                                insertBefore: '#lazyload_placeholder'
                            }).then(function() {
                                return $ocLazyLoad.load([
                                    'material-js',
                                    'material-css',
                                    'assets/plugins/belay/belay.js',
                                    'https://rawgit.com/icodeforlove/node-balanced/master/dist/balanced-min.js',
                                    'https://cdn.rawgit.com/konvajs/konva/1.6.1/konva.min.js',
                                    'assets/plugins/konvajs/konvaSetting.js',
                                    'assets/js/controllers/metadata/metadataController.js'
                                ]);
                            });
                        }]
                    }
                })

                //-------------------Metadata Edit route-----------------------------------
                .state('metadata.edit', {
                    url: '/edit/:data',
                    templateUrl: "html/metadata/edit.html",
                    controller: 'metadataEditController',
                    permission:"metadata-edit",
                    resolve: {
                        deps: ['$ocLazyLoad', function($ocLazyLoad) {
                            return $ocLazyLoad.load([
                                'js'
                            ], {
                                insertBefore: '#lazyload_placeholder'
                            }).then(function() {
                                return $ocLazyLoad.load([
                                    'konvaJs',
                                    'assets/js/controllers/metadata/metadataEditController.js'
                                ]);
                            });
                        }]
                    }

                })

                //-------------------Metadata Increment Data route-----------------------------------
                .state('metadata.incrementData', {
                    url: '/incrementData/:data',
                    templateUrl: "html/metadata/incrementData.html",
                    controller: 'metadataIncrementDataController',
                    permission:"metadata-edit",
                    resolve: {
                        deps: ['$ocLazyLoad', function($ocLazyLoad) {
                            return $ocLazyLoad.load([
                                'js'
                            ], {
                                insertBefore: '#lazyload_placeholder'
                            }).then(function() {
                                return $ocLazyLoad.load([
                                    'assets/js/controllers/metadata/metadataIncrementDataController.js'
                                ]);
                            });
                        }]
                    }

                })

                //***************************************Metadata blending*************************************
                .state('metadata.blendingAdd', {
                    url: '/blending/add',
                    templateUrl: "html/metadata/blending/add.html",
                    controller: 'metadataBlendingAddController',
                    permission:"metadata-add",
                    resolve: {
                        deps: ['$ocLazyLoad', function($ocLazyLoad) {
                            return $ocLazyLoad.load([
                                'js'
                            ], {
                                insertBefore: '#lazyload_placeholder'
                            }).then(function() {
                                return $ocLazyLoad.load([
                                    /*'custom-js/strelki.js',*/
                                    'custom-js/alasql.min.js',
                                    'konvaJs',
                                    'assets/js/controllers/metadata/metadataBlendingAddController.js'
                                ]);
                            });
                        }]
                    }
                })
                .state('metadata.blendingEdit', {
                    url: '/blending/edit/:id',
                    templateUrl: "html/metadata/blending/edit.html",
                    controller: 'metadataBlendingEditController',
                    permission:"metadata-edit",
                    resolve: {
                        deps: ['$ocLazyLoad', function($ocLazyLoad) {
                            return $ocLazyLoad.load([
                                'js'
                            ], {
                                insertBefore: '#lazyload_placeholder'
                            }).then(function() {
                                return $ocLazyLoad.load([
                                    /*'custom-js/strelki.js',*/
                                    'custom-js/alasql.min.js',
                                    'konvaJs',
                                    'assets/js/controllers/metadata/metadataBlendingEditController.js'
                                ]);
                            });
                        }]
                    }
                })
                .state('metadata.blendingList', {
                    url: '/blending/',
                    templateUrl: "html/metadata/blending/list.html",
                    controller: 'metadataBlendingListController',
                    permission:"metadata-edit",
                    resolve: {
                        deps: ['$ocLazyLoad', function($ocLazyLoad) {
                            return $ocLazyLoad.load([
                                'js'
                            ], {
                                insertBefore: '#lazyload_placeholder'
                            }).then(function() {
                                return $ocLazyLoad.load([
                                    /*'custom-js/strelki.js',*/
                                    'custom-js/alasql.min.js',
                                    'konvaJs',
                                    'assets/js/controllers/metadata/metadataBlendingListController.js'
                                ]);
                            });
                        }]
                    }
                })
                //***************************************End Metadata Blending*********************************




                //***************************************Machine Learning Start*********************************
                .state('machine', {
                    url: "/machine",
                    template: '<div class="full-height" ui-view></div>'
                })

                //------------------------Machine Add route-----------------------------------
                .state('machine.add', {
                    url: "/add",
                    templateUrl: "html/machine/add.html",
                    controller: 'machineAddController',
                    permission:"ml-add",
                    resolve: {
                        deps: ['$ocLazyLoad', function($ocLazyLoad) {
                            return $ocLazyLoad.load([
                                'js',
                                'dc',
                                'gridster',
                                'multiselect'
                            ], {
                                insertBefore: '#lazyload_placeholder'
                            }).then(function() {
                                return $ocLazyLoad.load([
                                    'konvaJsMachine',
                                    'assets/js/controllers/machine/machineAddController.js'
                                ]);
                            });
                        }]
                    }
                })
                //------------------------Machine Edit route-----------------------------------
                .state('machine.edit', {
                    url: '/edit/:data/:type',
                    templateUrl: "html/machine/edit.html",
                    controller:"machineEditController",
                    permission:"ml-edit",
                    resolve: {
                        deps: ['$ocLazyLoad', function($ocLazyLoad) {
                            return $ocLazyLoad.load([
                                'js',
                                'dc',
                                'gridster',
                                'multiselect'
                            ], {
                                insertBefore: '#lazyload_placeholder'
                            }).then(function() {
                                return $ocLazyLoad.load([
                                    'konvaJsMachine',
                                    'assets/js/controllers/machine/machineEditController.js'
                                ]);
                            });
                        }]
                    }
                })
                //------------------------Machine List View route-----------------------------------
                .state('machine.listView', {
                    url: '/',
                    templateUrl: "html/machine/list-view.html",
                    controller:"machineViewController",
                    permission:"ml-list",
                    resolve: {
                        deps: ['$ocLazyLoad', function($ocLazyLoad) {
                            return $ocLazyLoad.load([
                                'js',
                                'dc',
                                'gridster',
                                'multiselect'
                            ], {
                                insertBefore: '#lazyload_placeholder'
                            }).then(function() {
                                return $ocLazyLoad.load([
                                    'konvaJsMachine',
                                    'assets/js/controllers/machine/machineViewController.js'
                                ]);
                            });
                        }]
                    }
                })
                //------------------------Machine View route-----------------------------------
                .state('machine.view', {
                    url: '/view/:data/:type',
                    templateUrl: "html/machine/view.html",
                    controller:"machineEditController",
                    permission:"ml-view",
                    resolve: {
                        deps: ['$ocLazyLoad', function($ocLazyLoad) {
                            return $ocLazyLoad.load([
                                'js',
                                'dc',
                                'gridster',
                                'multiselect'
                            ], {
                                insertBefore: '#lazyload_placeholder'
                            }).then(function() {
                                return $ocLazyLoad.load([
                                    'konvaJsMachine',
                                    'assets/js/controllers/machine/machineEditController.js'
                                ]);
                            });
                        }]
                    }
                })
                    
                //***************************************Machine Learning End*********************************








                //***************************************Dashboard Route****************************************
                //------------------Dashboard--------------------------------------------
                .state('dashboard', {
                    url: "/dashboard",
                    template: '<div class="full-height" ui-view></div>'
                })

                //------------------------Dashboard Edit route-----------------------------------  :data
                .state('dashboard.edit', {
                    url: '/edit/:data/:type',
                    templateUrl: "html/dashboard/edit.html",
                    controller:"dashboardEditController",
                    permission:"dashboard-edit",
                    resolve: {
                        deps: ['$ocLazyLoad', function($ocLazyLoad) {
                            return $ocLazyLoad.load([
                                'js',
                                'dc',
                                'gridster',
                                'pivot-table',
                                'multiselect'
                            ], {
                                insertBefore: '#lazyload_placeholder'
                            }).then(function() {
                                return $ocLazyLoad.load([
                                    'assets/js/controllers/dashboard/dashboardEditController.js'
                                ]);
                            });
                        }]
                    }
                })
                //----------------------- dashboard Copy ---------------------------------------------
                .state('dashboard.copy', {
                    url: '/copy/:data/:copy/:type',
                    templateUrl: "html/dashboard/copy.html",
                    controller:"dashboardEditController",
                    permission:"dashboard-edit",
                    resolve: {
                        deps: ['$ocLazyLoad', function($ocLazyLoad) {
                            return $ocLazyLoad.load([
                                'js',
                                'dc',
                                'gridster',
                                'pivot-table',
                                'multiselect'
                            ], {
                                insertBefore: '#lazyload_placeholder'
                            }).then(function() {
                                return $ocLazyLoad.load([
                                    'assets/js/controllers/dashboard/dashboardEditController.js'
                                ]);
                            });
                        }]
                    }
                })
                //------------------------Dashboard List View route-----------------------------------
                .state('dashboard.listView', {
                    url: '/',
                    templateUrl: "html/dashboard/list-view.html",
                    controller:"dashboardViewController",
                    permission:"dashboard-list",
                    resolve: {
                        deps: ['$ocLazyLoad', function($ocLazyLoad) {
                            return $ocLazyLoad.load([
                                'js',
                                'dc',
                                'gridster',
                                'pivot-table',
                                'multiselect'
                            ], {
                                insertBefore: '#lazyload_placeholder'
                            }).then(function() {
                                return $ocLazyLoad.load([
                                    'assets/js/controllers/dashboard/dashboardViewController.js'
                                ]);
                            });
                        }]
                    }
                })
                //------------------------Dashboard View route-----------------------------------  :data
                .state('dashboard.view', {
                    url: '/view/:data/:type',
                    templateUrl: "html/dashboard/view.html",
                    controller:"dashboardEditController",
                    permission:"dashboard-view",
                    resolve: {
                        deps: ['$ocLazyLoad', function($ocLazyLoad) {
                            return $ocLazyLoad.load([
                                'js',
                                'dc',
                                'gridster',
                                'pivot-table',
                                'multiselect'
                            ], {
                                insertBefore: '#lazyload_placeholder'
                            }).then(function() {
                                return $ocLazyLoad.load([
                                    'assets/js/controllers/dashboard/dashboardEditController.js'
                                ]);
                            });
                        }]
                    }
                })
                //------------------------Dashboard Add route-----------------------------------  :data
                .state('dashboard.add', {
                    url: "/add",
                    templateUrl: "html/dashboard/add.html",
                    controller: 'dashboardController',
                    permission:"dashboard-add",
                    resolve: {
                        deps: ['$ocLazyLoad', function($ocLazyLoad) {
                            return $ocLazyLoad.load([
                                'js',
                                'dc',
                                'gridster',
                                'pivot-table',
                                'multiselect'
                            ], {
                                insertBefore: '#lazyload_placeholder'
                            }).then(function() {
                                return $ocLazyLoad.load([
                                    'assets/js/controllers/dashboard/dashboardController.js'
                                ]);
                            });
                        }]
                    }
                })
                .state('dashboard.addTODatasource', {
                    url: "/add/:id",
                    templateUrl: "tpl/new/new-dashboard-add.html",
                    controller: 'dashboardController',
                    resolve: {
                        deps: ['$ocLazyLoad', function($ocLazyLoad) {
                            return $ocLazyLoad.load([
                                'select',
                                'dc',
                                'gridster',
                                'multiselect'
                            ], {
                                insertBefore: '#lazyload_placeholder'
                            }).then(function() {
                                return $ocLazyLoad.load([
                                    'material-js',
                                    'material-css',
                                    'https://cdnjs.cloudflare.com/ajax/libs/linq.js/2.2.0.2/linq.js',
                                    'http://getbootstrapadmin.com/remark/material/global/vendor/bootstrap-datepicker/bootstrap-datepicker.min.css?v2.2.0',
                                    'assets/js/controllers/dashboardController.js',
                                    'https://rawgit.com/icodeforlove/node-balanced/master/dist/balanced-min.js'
                                ]);
                            });
                        }]
                    }
                })
                //------------------------Dashboard Metadata edit-----------------------------------  :data
                .state('dashboard.metadata', {
                    url: "/metadata/:id",
                    templateUrl: "html/dashboard/metadataDashboardEdit.html",
                    controller: 'dashboardMetadataController',
                    permission:"metadata-edit",
                    resolve: {
                        deps: ['$ocLazyLoad', function($ocLazyLoad) {
                            return $ocLazyLoad.load([
                                'js'
                            ], {
                                insertBefore: '#lazyload_placeholder'
                            }).then(function() {
                                return $ocLazyLoad.load([
                                    'konvaJs',
                                    'assets/js/controllers/dashboard/dashboardMetadataController.js'
                                ]);
                            });
                        }]
                    }
                })
                //------------------------Dashboard Metadata blending edit-----------------------------------  :data
                .state('dashboard.metadataBlending', {
                    url: "/metadata/blending/:id",
                    templateUrl: "html/dashboard/metadataDashboardBlendingEdit.html",
                    controller: 'dashboardMetadataBlendingController',
                    permission:"metadata-edit",
                    resolve: {
                        deps: ['$ocLazyLoad', function($ocLazyLoad) {
                            return $ocLazyLoad.load([
                                'js'
                            ], {
                                insertBefore: '#lazyload_placeholder'
                            }).then(function() {
                                return $ocLazyLoad.load([
                                    'konvaJs',
                                    'custom-js/alasql.min.js',
                                    'assets/js/controllers/dashboard/dashboardMetadataBlendingController.js'
                                ]);
                            });
                        }]
                    }
                })
                //------------------Dashboard End--------------------------------------------



                .state('sharedview', {
                    url: "/sharedView",
                    template: '<div class="full-height" ui-view></div>'
                })
                //---------------------------------------SharedVIew View Add Controller-------------------------------------------
                .state('sharedview.add', {
                    url: "/add/{id}",
                    templateUrl: "html/shared_view/add.html",
                    controller: 'publicViewAddController',
                    permission:"sharedview-add",
                    resolve: {
                        deps: ['$ocLazyLoad', function($ocLazyLoad) {
                            return $ocLazyLoad.load([
                                'js',
                                'dc',
                                'gridster',
                                'pivot-table',
                                'multiselect'
                            ], {
                                insertBefore: '#lazyload_placeholder'
                            }).then(function() {
                                return $ocLazyLoad.load([
                                    'assets/js/controllers/publicView/publicViewAddController.js'
                                ]);
                            });
                        }]
                    }
                })
                //---------------------------------------Shared view-------------------------------------------
                .state('sharedview.show', {
                    url: "/show/{auth_key}/{id}",
                    templateUrl: "html/shared_view/public-view.html",
                    controller:  'sharedviewController',
                    permission:"sharedview-view",
                    resolve: {
                        deps: ['$ocLazyLoad', function($ocLazyLoad) {
                            return $ocLazyLoad.load([
                                'js',
                                'dc',
                                'gridster',
                                'pivot-table',
                                'multiselect'
                            ], {
                                insertBefore: '#lazyload_placeholder'
                            }).then(function() {
                                return $ocLazyLoad.load([
                                    'assets/js/controllers/publicView/sharedViewController.js'
                                ]);
                            });
                        }]
                    }
                })
                .state('sharedview.show1', {
                    url: "/show1/{auth_key}/{id}",
                    templateUrl: "html/shared_view/public-view1.html",
                    controller:  'sharedviewController1',
                    permission:"sharedview-view",
                    resolve: {
                        deps: ['$ocLazyLoad', function($ocLazyLoad) {
                            return $ocLazyLoad.load([
                                'js',
                                'dc',
                                'gridster',
                                'pivot-table',
                                'multiselect'
                            ], {
                                insertBefore: '#lazyload_placeholder'
                            }).then(function() {
                                return $ocLazyLoad.load([
                                    'assets/js/controllers/publicView/sharedViewController1.js'
                                ]);
                            });
                        }]
                    }
                })
                //---------------------------------------Shared view mobile----------------------------------------
                .state('sharedview.mobile', {
                    url: "/mobileView/{auth_key}/{id}",
                    templateUrl: "html/shared_view/mobile-public-view.html",
                    controller:  'sharedviewController',
                    permission:"sharedview-view",
                    resolve: {
                        deps: ['$ocLazyLoad', function($ocLazyLoad) {
                            return $ocLazyLoad.load([
                                'js',
                                'dc',
                                'gridster',
                                'pivot-table',
                                'multiselect'
                            ], {
                                insertBefore: '#lazyload_placeholder'
                            }).then(function() {
                                return $ocLazyLoad.load([
                                    'assets/js/controllers/publicView/sharedViewController.js'
                                ]);
                            });
                        }]
                    }
                })
                //--------------------------------------
                //---------------------------------------SharedView List view-------------------------------------------
                .state('sharedview.list', {
                    url: "/",
                    templateUrl: "html/shared_view/list-view.html",
                    controller:  'publicViewListController',
                    permission:"sharedview-list",
                    resolve: {
                        deps: ['$ocLazyLoad', function($ocLazyLoad) {
                            return $ocLazyLoad.load([
                                'js',
                                'dc',
                                'gridster',
                                'pivot-table',
                                'multiselect'
                            ], {
                                insertBefore: '#lazyload_placeholder'
                            }).then(function() {
                                return $ocLazyLoad.load([
                                    'assets/js/controllers/publicView/publicViewListController.js'
                                ]);
                            });
                        }]
                    }
                })
                //-------------------------------- Public view edit---------------------------
                //---------------------------------------Public View Add Controller-------------------------------------------
                .state('sharedview.edit', {
                    url: "/edit/{auth_key}/{id}",
                    templateUrl: "html/shared_view/edit.html",
                    controller: 'publicViewEditController',
                    permission:"sharedview-edit",
                    resolve: {
                        deps: ['$ocLazyLoad', function($ocLazyLoad) {
                            return $ocLazyLoad.load([
                                'js',
                                'dc',
                                'gridster',
                                'pivot-table',
                                'multiselect'
                            ], {
                                insertBefore: '#lazyload_placeholder'
                            }).then(function() {
                                return $ocLazyLoad.load([
                                    'assets/js/controllers/publicView/publicViewEditController.js'
                                ]);
                            });
                        }]
                    }
                })
                //-----------------------------------public copy-----------------------------------------
                .state('sharedview.copy', {
                    url: "/copy/{auth_key}/{copy}/{id}",
                    templateUrl: "html/shared_view/copy.html",
                    controller: 'publicViewEditController',
                    permission:"sharedview-edit",
                    resolve: {
                        deps: ['$ocLazyLoad', function($ocLazyLoad) {
                            return $ocLazyLoad.load([
                                'js',
                                'dc',
                                'gridster',
                                'pivot-table',
                                'multiselect'
                            ], {
                                insertBefore: '#lazyload_placeholder'
                            }).then(function() {
                                return $ocLazyLoad.load([
                                    'assets/js/controllers/publicView/publicViewEditController.js'
                                ]);
                            });
                        }]
                    }
                })
                //-----------------------------------Public view------------------------------------------
                .state('publicView', {
                    url: "/publicView",
                    template: '<div class="full-height" ui-view></div>'
                })
                .state('publicView.show', {
                    url: "/show/{auth_key}/{comId}/{userPort}/{id}",
                    templateUrl: "html/shared_view/public-view.html",
                    controller: 'publicviewController',
                    permission:"publicView",
                    resolve: {
                        deps: ['$ocLazyLoad', function($ocLazyLoad) {
                            return $ocLazyLoad.load([
                                'js',
                                'dc',
                                'gridster',
                                'pivot-table',
                                'multiselect'
                            ], {
                                insertBefore: '#lazyload_placeholder'
                            }).then(function() {
                                return $ocLazyLoad.load([
                                    'https://cdnjs.cloudflare.com/ajax/libs/fingerprintjs2/1.8.1/fingerprint2.js',
                                    'assets/js/controllers/publicView/publicViewController.js'
                                ]);
                            });
                        }]
                    }
                })
                .state('publicView.snap', {
                    url: "/show/{auth_key}/{id}/{tab}/{comId}/{userPort}",
                    templateUrl: "html/shared_view/snap.html",
                    controller: 'publicViewSnap',
                    permission:"publicView",
                    resolve: {
                        deps: ['$ocLazyLoad', function($ocLazyLoad) {
                            return $ocLazyLoad.load([
                                'js',
                                'dc',
                                'gridster',
                                'pivot-table',
                                'multiselect'
                            ], {
                                insertBefore: '#lazyload_placeholder'
                            }).then(function() {
                                return $ocLazyLoad.load([
                                    'https://cdnjs.cloudflare.com/ajax/libs/fingerprintjs2/1.8.1/fingerprint2.js',
                                    'assets/js/controllers/publicView/publicViewSnapController.js'
                                ]);
                            });
                        }]
                    }
                }) 
                .state('publicView.mobile', {
                    url: "/mobileView/{auth_key}/{comId}/{userPort}/{id}",
                    templateUrl: "html/shared_view/mobile-public-view.html",
                    controller: 'publicviewController',
                    permission:"publicView",
                    resolve: {
                        deps: ['$ocLazyLoad', function($ocLazyLoad) {
                            return $ocLazyLoad.load([
                                'js',
                                'dc',
                                'gridster',
                                'pivot-table',
                                'multiselect'
                            ], {
                                insertBefore: '#lazyload_placeholder'
                            }).then(function() {
                                return $ocLazyLoad.load([
                                    'https://cdnjs.cloudflare.com/ajax/libs/fingerprintjs2/1.8.1/fingerprint2.js',
                                    'assets/js/controllers/publicView/publicViewController.js'
                                ]);
                            });
                        }]
                    }
                })
                //---------------------------------------Settings-------------------------------------------
                .state('company', {
                    url: "/company",
                    template: '<div class="full-height" ui-view></div>'
                })
                .state('company.details', {
                    url: "/details",
                    templateUrl: "html/company/index.html",
                    controller: 'companyController',
                    resolve: {
                        deps: ['$ocLazyLoad', function($ocLazyLoad) {
                            return $ocLazyLoad.load([
                                'js'
                            ], {
                                insertBefore: '#lazyload_placeholder'
                            }).then(function() {
                                return $ocLazyLoad.load([
                                    'assets/js/controllers/company/companyController.js'
                                ]);
                            });
                        }]
                    }
                })
                .state('company.add', {
                    url: "/add",
                    templateUrl: "html/company/add.html",
                    controller: 'companyAddController',
                    resolve: {
                        deps: ['$ocLazyLoad', function($ocLazyLoad) {
                            return $ocLazyLoad.load([
                                'js'
                            ], {
                                insertBefore: '#lazyload_placeholder'
                            }).then(function() {
                                return $ocLazyLoad.load([
                                    'assets/js/controllers/company/companyAddController.js'
                                ]);
                            });
                        }]
                    }
                })
                .state('company.edit', {
                    url: "/edit/{id}",
                    templateUrl: "html/company/edit.html",
                    controller: 'companyEditController',
                    resolve: {
                        deps: ['$ocLazyLoad', function($ocLazyLoad) {
                            return $ocLazyLoad.load([
                                'js'
                            ], {
                                insertBefore: '#lazyload_placeholder'
                            }).then(function() {
                                return $ocLazyLoad.load([
                                    'assets/js/controllers/company/companyEditController.js'
                                ]);
                            });
                        }]
                    }
                })
                .state('user', {
                    url: "/user",
                    template: '<div class="full-height" ui-view></div>'
                })
                .state('user.list', {
                    url: "/list",
                    templateUrl: "html/user/index.html",
                    controller: 'userController',
                    resolve: {
                        deps: ['$ocLazyLoad', function($ocLazyLoad) {
                            return $ocLazyLoad.load([
                                'js'
                            ], {
                                insertBefore: '#lazyload_placeholder'
                            }).then(function() {
                                return $ocLazyLoad.load([
                                    'assets/js/controllers/user/userController.js'
                                ]);
                            });
                        }]
                    }
                })
                .state('user.add', {
                    url: "/add",
                    templateUrl: "html/user/add.html",
                    controller: 'userAddController',
                    resolve: {
                        deps: ['$ocLazyLoad', function($ocLazyLoad) {
                            return $ocLazyLoad.load([
                                'js'
                            ], {
                                insertBefore: '#lazyload_placeholder'
                            }).then(function() {
                                return $ocLazyLoad.load([
                                    'assets/js/controllers/user/userAddController.js'
                                ]);
                            });
                        }]
                    }
                })
                .state('user.edit', {
                    url: "/edit/{id}",
                    templateUrl: "html/user/edit.html",
                    controller: 'userEditController',
                    resolve: {
                        deps: ['$ocLazyLoad', function($ocLazyLoad) {
                            return $ocLazyLoad.load([
                                'js'
                            ], {
                                insertBefore: '#lazyload_placeholder'
                            }).then(function() {
                                return $ocLazyLoad.load([
                                    'assets/js/controllers/user/userEditController.js'
                                ]);
                            });
                        }]
                    }
                })
                .state('enquiry', {
                    url: "/enquiry",
                    template: '<div class="full-height" ui-view></div>'
                })
                .state('enquiry.details', {
                    url: "/details",
                    templateUrl: "html/enquiry/index.html",
                    controller: 'enquiryController',
                    resolve: {
                        deps: ['$ocLazyLoad', function($ocLazyLoad) {
                            return $ocLazyLoad.load([
                                'js'
                            ], {
                                insertBefore: '#lazyload_placeholder'
                            }).then(function() {
                                return $ocLazyLoad.load([
                                    'assets/js/controllers/enquiry/enquiryController.js'
                                ]);
                            });
                        }]
                    }
                })
                .state('enquiry.add', {
                    url: "/add",
                    templateUrl: "html/enquiry/add.html",
                    controller: 'enquiryAddController',
                    resolve: {
                        deps: ['$ocLazyLoad', function($ocLazyLoad) {
                            return $ocLazyLoad.load([
                                'js'
                            ], {
                                insertBefore: '#lazyload_placeholder'
                            }).then(function() {
                                return $ocLazyLoad.load([
                                    'assets/js/controllers/enquiry/enquiryAddController.js'
                                ]);
                            });
                        }]
                    }
                })
                .state('enquiry.edit', {
                    url: "/edit/{id}",
                    templateUrl: "html/enquiry/edit.html",
                    controller: 'enquiryEditController',
                    resolve: {
                        deps: ['$ocLazyLoad', function($ocLazyLoad) {
                            return $ocLazyLoad.load([
                                'js'
                            ], {
                                insertBefore: '#lazyload_placeholder'
                            }).then(function() {
                                return $ocLazyLoad.load([
                                    'assets/js/controllers/enquiry/enquiryEditController.js'
                                ]);
                            });
                        }]
                    }
                })





                .state('setting', {
                    url: "/setting",
                    template: '<div class="full-height" ui-view></div>'
                })
                .state('setting.view', {
                    url: "/{tabId}",
                    templateUrl: "html/setting/setting.html",
                    controller: 'settingController',
                    permission:"setting",
                    resolve: {
                        deps: ['$ocLazyLoad', function($ocLazyLoad) {
                            return $ocLazyLoad.load([
                                'js',
                                'multiselect'
                            ], {
                                insertBefore: '#lazyload_placeholder'
                            }).then(function() {
                                return $ocLazyLoad.load([
                                    'assets/js/controllers/settingController.js'
                                ]);
                            });
                        }]
                    }
                })
                .state('setting.userroleAdd', {
                    url: "/userrole/add",
                    templateUrl: "html/roles/role-add.html",
                    controller: 'userRoleController',
                    permission:"setting",
                    resolve: {
                        deps: ['$ocLazyLoad', function($ocLazyLoad) {
                            return $ocLazyLoad.load([
                                'js',
                                'multiselect'
                            ], {
                                insertBefore: '#lazyload_placeholder'
                            }).then(function() {
                                return $ocLazyLoad.load([
                                    'assets/js/controllers/userrole/userRoleController.js'
                                ]);
                             });
                        }]
                    }
                })
                .state('setting.userroleEdit', {
                    url: "/userrole/edit/{id}",
                    templateUrl: "html/roles/role-edit.html",
                    controller: 'userRoleEditController',
                    permission:"setting",
                    resolve: {
                        deps: ['$ocLazyLoad', function($ocLazyLoad) {
                            return $ocLazyLoad.load([
                                'js',
                                'multiselect'
                            ], {
                                insertBefore: '#lazyload_placeholder'
                            }).then(function() {
                                return $ocLazyLoad.load([
                                    'assets/js/controllers/userrole/userRoleEditController.js'
                                ]);
                            });
                        }]
                    }
                })
                .state('setting.userroleView', {
                    url: "/userrole/view/{id}",
                    templateUrl: "html/roles/role-view.html",
                    controller: 'userRoleEditController',
                    permission:"setting",
                    resolve: {
                        deps: ['$ocLazyLoad', function($ocLazyLoad) {
                            return $ocLazyLoad.load([
                                'js',
                                'multiselect'
                            ], {
                                insertBefore: '#lazyload_placeholder'
                            }).then(function() {
                                return $ocLazyLoad.load([
                                    'assets/js/controllers/userrole/userRoleEditController.js'
                                ]);
                            });
                        }]
                    }
                })
                .state('setting.userroleAssign', {
                    url: "/userrole/assign",
                    templateUrl: "html/roles/role-assign.html",
                    controller: 'userRoleAssignController',
                    permission:"setting",
                    resolve: {
                        deps: ['$ocLazyLoad', function($ocLazyLoad) {
                            return $ocLazyLoad.load([
                                'js',
                                'multiselect'
                            ], {
                                insertBefore: '#lazyload_placeholder'
                            }).then(function() {
                                return $ocLazyLoad.load([
                                    'assets/js/controllers/userrole/userRoleAssignController.js'
                                ]);
                            });
                        }]
                    }
                })
                .state('setting.userroleAssignEdit', {
                    url: "/userrole/assign/edit/{id}",
                    templateUrl: "html/roles/role-assign-edit.html",
                    controller: 'userRoleAssignEditController',
                    permission:"setting",
                    resolve: {
                        deps: ['$ocLazyLoad', function($ocLazyLoad) {
                            return $ocLazyLoad.load([
                                'js',
                                'multiselect'
                            ], {
                                insertBefore: '#lazyload_placeholder'
                            }).then(function() {
                                return $ocLazyLoad.load([
                                    'assets/js/controllers/userrole/userRoleAssignEditController.js'
                                ]);
                            });
                        }]
                    }
                })
                .state('setting.roleassign', {
                    url: "/roleassign",
                    templateUrl: "tpl/new/role-assign.html",
                    controller: 'roleAssignController',
                    permission:"setting",
                    resolve: {
                        deps: ['$ocLazyLoad', function($ocLazyLoad) {
                            return $ocLazyLoad.load([
                                'select',
                                'dataTables'
                            ], {
                                insertBefore: '#lazyload_placeholder'
                            }).then(function() {
                                return $ocLazyLoad.load([
                                    'assets/js/controllers/roleAssignController.js'
                                ]);
                            });
                        }]
                    }
                })
                /*
                 * Report group
                 */
                .state('setting.reportGroupAdd', {
                    url: "/report-group/add/{type}",
                    templateUrl: "html/report_group/add.html",
                    controller: 'reportGroupController',
                    permission:"setting",
                    resolve: {
                        deps: ['$ocLazyLoad', function($ocLazyLoad) {
                            return $ocLazyLoad.load([
                                'js'
                            ], {
                                insertBefore: '#lazyload_placeholder'
                            }).then(function() {
                                return $ocLazyLoad.load([
                                    'assets/js/controllers/reportGroup/reportGroupController.js'
                                ]);
                            });
                        }]
                    }
                })
                .state('setting.reportGroupEdit', {
                    url: "/report-group/edit/{id}/{type}",
                    templateUrl: "html/report_group/edit.html",
                    controller: 'reportGroupEditController',
                    permission:"setting",
                    resolve: {
                        deps: ['$ocLazyLoad', function($ocLazyLoad) {
                            return $ocLazyLoad.load([
                                'js'
                            ], {
                                insertBefore: '#lazyload_placeholder'
                            }).then(function() {
                                return $ocLazyLoad.load([
                                    'assets/js/controllers/reportGroup/reportGroupEditController.js'
                                ]);
                            });
                        }]
                    }
                })
                //--------------------------Role level security----------------------------------------------
                .state('setting.rolelevelsecurity', {
                    url: "/rolelevel/security",
                    templateUrl: "html/role_security/rolelevelsecurity.html",
                    controller: 'roleLevelSecurityController',
                    permission:"setting",
                    resolve: {
                        deps: ['$ocLazyLoad', function($ocLazyLoad) {
                            return $ocLazyLoad.load([
                                'js'
                            ], {
                                insertBefore: '#lazyload_placeholder'
                            }).then(function() {
                                return $ocLazyLoad.load([
                                    'assets/js/controllers/roleLevelSecurity/roleLevelSecurityController.js'
                                ]);
                            });
                        }]
                    }
                })
                .state('setting.rolelevelsecurityEdit', {
                    url: "/rolelevel/security/edit/{id}",
                    templateUrl: "html/role_security/rolelevelsecurityEdit.html",
                    controller: 'roleLevelSecurityEditController',
                    permission:"setting",
                    resolve: {
                        deps: ['$ocLazyLoad', function($ocLazyLoad) {
                            return $ocLazyLoad.load([
                                'js'
                            ], {
                                insertBefore: '#lazyload_placeholder'
                            }).then(function() {
                                return $ocLazyLoad.load([
                                    'assets/js/controllers/roleLevelSecurity/roleLevelSecurityEditController.js'
                                ]);
                            });
                        }]
                    }
                })
                .state('setting.rlsUser', {
                    url: "/rolelevelsecurity/user",
                    templateUrl: "html/role_security/user.html",
                    controller: 'RlsUserController',
                    permission:"setting",
                    resolve: {
                        deps: ['$ocLazyLoad', function($ocLazyLoad) {
                            return $ocLazyLoad.load([
                                'js'
                            ], {
                                insertBefore: '#lazyload_placeholder'
                            }).then(function() {
                                return $ocLazyLoad.load([
                                    'assets/js/controllers/roleLevelSecurity/RlsUserController.js'
                                ]);
                            });
                        }]
                    }
                })
                .state('setting.rlsUserEdit', {
                    url: "/rolelevelsecurity/user/edit/{id}",
                    templateUrl: "html/role_security/userEdit.html",
                    controller: 'RlsUserEditController',
                    permission:"setting",
                    resolve: {
                        deps: ['$ocLazyLoad', function($ocLazyLoad) {
                            return $ocLazyLoad.load([
                                'js'
                            ], {
                                insertBefore: '#lazyload_placeholder'
                            }).then(function() {
                                return $ocLazyLoad.load([
                                    'assets/js/controllers/roleLevelSecurity/RlsUserEditController.js'
                                ]);
                            });
                        }]
                    }
                })
                .state('setting.rlsUserGroup', {
                    url: "/rolelevelsecurity/usergroup",
                    templateUrl: "html/role_security/userGroup.html",
                    controller: 'RlsUserGroupController',
                    permission:"setting",
                    resolve: {
                        deps: ['$ocLazyLoad', function($ocLazyLoad) {
                            return $ocLazyLoad.load([
                                'js'
                            ], {
                                insertBefore: '#lazyload_placeholder'
                            }).then(function() {
                                return $ocLazyLoad.load([
                                    'assets/js/controllers/roleLevelSecurity/rlsUserGroupController.js'
                                ]);
                            });
                        }]
                    }
                })
                .state('setting.rlsUserGroupEdit', {
                    url: "/rolelevelsecurity/usergroup/edit/{id}",
                    templateUrl: "html/role_security/userGroupEdit.html",
                    controller: 'RlsUserGroupEditController',
                    permission:"setting",
                    resolve: {
                        deps: ['$ocLazyLoad', function($ocLazyLoad) {
                            return $ocLazyLoad.load([
                                'js'
                            ], {
                                insertBefore: '#lazyload_placeholder'
                            }).then(function() {
                                return $ocLazyLoad.load([
                                    'assets/js/controllers/roleLevelSecurity/rlsUserGroupEditController.js'
                                ]);
                            });
                        }]
                    }
                })
                .state('setting.emailAdd', {
                    url: "/email/schedule",
                    templateUrl: "html/email/add.html",
                    controller: 'emailController',
                    permission:"setting",
                    resolve: {
                        deps: ['$ocLazyLoad', function($ocLazyLoad) {
                            return $ocLazyLoad.load([
                                'js',
                                'multiselect'
                            ], {
                                insertBefore: '#lazyload_placeholder'
                            }).then(function() {
                                return $ocLazyLoad.load([
                                    'assets/js/controllers/emailController.js'
                                ]);
                            });
                        }]
                    }
                })
                //-------------------------------------------group Email------------------------------------------
                .state('setting.emailGroupAdd', {
                    url: "/email/group/add",
                    templateUrl: "html/email_group/add.html",
                    controller: 'emailGroupAddController',
                    permission:"setting",
                    resolve: {
                        deps: ['$ocLazyLoad', function($ocLazyLoad) {
                            return $ocLazyLoad.load([
                                'js',
                                'multiselect'
                            ], {
                                insertBefore: '#lazyload_placeholder'
                            })
                            .then(function() {
                                return $ocLazyLoad.load([
                                    'assets/js/controllers/emailGroup/emailGroupAddController.js'
                                ]);
                            });
                        }]
                    }
                })
                .state('setting.emailGroupEdit', {
                    url: "/email/group/edit/{id}",
                    templateUrl: "html/email_group/edit.html",
                    controller: 'emailGroupEditController',
                    permission:"setting",
                    resolve: {
                        deps: ['$ocLazyLoad', function($ocLazyLoad) {
                            return $ocLazyLoad.load([
                                'js',
                                'multiselect'
                            ], {
                                insertBefore: '#lazyload_placeholder'
                            }).then(function() {
                                return $ocLazyLoad.load([
                                    'assets/js/controllers/emailGroup/emailGroupEditController.js'
                                ]);
                            });
                        }]
                    }
                })
                //--------------------------------------------User Register----------------------------------------------
                .state('setting.registeruserAdd', {
                    url: "/register_user/add",
                    templateUrl: "html/register_user/user_add.html",
                    controller: 'userRegisterController',
                    permission:"setting",
                    resolve: {
                        deps: ['$ocLazyLoad', function($ocLazyLoad) {
                            return $ocLazyLoad.load([
                                'js',
                                'multiselect'
                            ], {
                                insertBefore: '#lazyload_placeholder'
                            }).then(function() {
                                return $ocLazyLoad.load([
                                    'assets/js/controllers/registerUser/userRegisterController.js'
                                ]);
                            });
                        }]
                    }
                })
                .state('setting.registeruserEdit', {
                    url: "/register_user/edit/{id}",
                    templateUrl: "html/register_user/user_edit.html",
                    controller: 'userRegisterEditController',
                    permission:"setting",
                    resolve: {
                        deps: ['$ocLazyLoad', function($ocLazyLoad) {
                            return $ocLazyLoad.load([
                                'js',
                                'multiselect'
                            ], {
                                insertBefore: '#lazyload_placeholder'
                            }).then(function() {
                                return $ocLazyLoad.load([
                                    'assets/js/controllers/registerUser/userRegisterEditController.js'
                                ]);
                            });
                        }]
                    }
                })


                //--------------------------------------------Profile ----------------------------------------------
                .state('profile', {
                    url: "/profile",
                    template: '<div class="full-height" ui-view></div>'
                })
                .state('profile.view', {
                    url: "/",
                    templateUrl: "html/profile/profile.html",
                    controller: 'profileController',
                    permission:"profile",
                    resolve: {
                        deps: ['$ocLazyLoad', function($ocLazyLoad) {
                            return $ocLazyLoad.load([
                                'js',
                                'multiselect'
                            ], {
                                insertBefore: '#lazyload_placeholder'
                            }).then(function() {
                                return $ocLazyLoad.load([
                                    'assets/js/controllers/profileController.js'
                                ]);
                            });
                        }]
                    }
                })


                //-------------------------------------------- Change Password ----------------------------------------------
                .state('change-password', {
                    url: "/change-password",
                    template: '<div class="full-height" ui-view></div>'
                })
                .state('change-password.view', {
                    url: "/",
                    templateUrl: "html/common/change-password.html",
                    controller: 'changePasswordController',
                    permission: "change-password",
                    resolve: {
                        deps: ['$ocLazyLoad', function($ocLazyLoad) {
                            return $ocLazyLoad.load([
                                'js',
                                'multiselect'
                            ], {
                                insertBefore: '#lazyload_placeholder'
                            }).then(function() {
                                return $ocLazyLoad.load([
                                    'assets/js/controllers/changePasswordController.js'
                                ]);
                            });
                        }]
                    }
                })


        }
    ]); 