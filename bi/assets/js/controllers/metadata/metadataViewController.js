'use strict';
     angular.module('app',['ngSanitize'])
     .controller('metadataViewController', ['$scope','$sce','dataFactory','$rootScope','$filter','$state','$window','$cookieStore','$location',function($scope,$sce,dataFactory,$rootScope,$filter,$state,$window,$cookieStore,$location) {
     	 //--- Top Menu Bar
    	 var viewClass="";
    	 var addClass="";
    	 var data={};
    	 $(".modal-backdrop").hide();
    	 if($location.path()=='/metadata/'){
    	     viewClass='activeli';
         }else{
             addClass='activeli';
         }
         data['navTitle']="METADATA";
         data['icon']="fa fa-database";
         data['navigation']=[
             {
                "name":"Add",
                "url":"#/metadata/add",
                "class":addClass,
             },
             {
                "name":"View",
                "url":"#/metadata/",
                "class":viewClass,
             }
         ];

         var vm = $scope;

        $rootScope.$broadcast('subNavBar', data);
         //End Menu 
    	 //Datasource
    	 $scope.source={};
    	 $scope.trustAsHtml = function(value) {
 			return $sce.trustAsHtml(value); 
 		 };
    	 $scope.selectedConfirm=0;
		 $scope.toggle=function(e,source,index){
             setTimeout(function(){
                 $(function () {
                     $('[data-toggle="tooltip"]').tooltip();
                 })
             },100);

              $('.imageToggle').removeClass('selectedImage');
              source['index']=index;
              $scope.selectedMetadata=source;
              $(e.currentTarget).addClass('selectedImage');
              $scope.selectedConfirm=1;
			}
            // Datasource Name,image and port
            $scope.datasourceTypeIcon = [ {
                "name" : "MySQL",
                "image" : "theme/assets/img/MySQL-Logo.png",
                "port" : 3306
            },{
                "name" : "Mongodb",
                "image" : "theme/assets/img/mongodb.png",
                "port" : 3306
            },{
                "name" : "MsSQL",
                "image" : "theme/assets/img/mssql-server.png",
                "port" : 3306
            },{
                "name" : "Postgres",
                "image" : "theme/assets/img/postgresql.png",
                "port" : 5432
            },{
                "name" : "Cassandra",
                "image" : "theme/assets/img/cassandra-logo.png",
                "port" : 3306
            },{
                "name" : "Excel",
                "image" : "theme/assets/img/excel-logo.png",
                "port" : 3306
            },{
                "name" : "AWS RDS",
                "image" : "theme/assets/img/aws-rds-logo.png",
                "port" : 3306
            },{
                "name" : "Oracle",
                "image" : "theme/assets/img/oracle-logo.png",
                "port" : 3306
            },{
                "name" : "Sybase",
                "image" : "theme/assets/img/sybase-logo.png",
                "port" : 3306
            }];

         $scope.getDatasourceIconPath=function(metaObj){
             try{
                 var datasourceType=JSON.parse(metaObj.metadataObject).connectionObject.datasourceType;
                 var path="assets/img/logo.png";
                 $scope.datasourceTypeIcon.forEach(function(d){
                     if(datasourceType.toLowerCase()==d.name.toLowerCase()){
                         path=d.image;
                     }
                 });
                 return path;
             }catch (e){
                 return "assets/img/logo.png";
             }
         }

    	  /*
          	 * Metadata List
          	*/
    	$scope.metadataListApi=function () {
            dataFactory.request($rootScope.MetadataList_Url,'post',"").then(function(response){
                console.log(response.data.result)
                if(response.data.errorCode==1){
                    $scope.groupFolder = Object.keys(response.data.result);
                    $scope.metaList = response.data.result;
                }else{
                    dataFactory.errorAlert("Check your connection");
                }
            }).then(function(){
                $scope.loading=true;
                if($scope.loading){
                    $(".loaderImage").fadeOut("slow");
                }
                setTimeout(function(){
                    $('[rel="tooltip"]').tooltip();
                },100);
            });
        }
         $scope.metadataListApi();
		 $scope.metaEdit=function(){
			 $state.go("metadata.edit", { 'data': $scope.selectedMetadata.metadata_id });
		 }
         $scope.copy=function(){
             $state.go("metadata.copy", { 'data': $scope.selectedMetadata.metadata_id,'copy': 1 });
         }
		 $scope.IncrementalData=function(){
			 $state.go("metadata.incrementData", { 'data': $scope.selectedMetadata.metadata_id });
		 }






// Metadata Delete
         $scope.showSwal = function(type){
             if(type == 'warning-message-and-cancel') {
                 swal({
                     title: 'Are you sure?',
                     text: 'You want to delete MetaData !',
                     type: 'warning',
                     showCancelButton: true,
                     confirmButtonText: 'Yes',
                     cancelButtonText: 'No',
                     confirmButtonClass: "btn btn-success",
                     cancelButtonClass: "btn btn-danger",
                     buttonsStyling: false,
                 }).then(function(value){
                     if(value){
                         $scope.metadataDelete();
                     }
                 },
                 function (dismiss) {

                 })
             }
         }

		 $scope.metadataDelete=function () {
			var index;
			var id=$scope.selectedMetadata.metadata_id;
             $scope.metadataList.forEach(function (d,i) {
			    if(d.metadata_id==$scope.selectedMetadata.metadata_id){
					index=i;
				}
			});
			var data={
				"id":id
			};
			dataFactory.request($rootScope.MetadataDelete_Url,'post',data).then(function(response){
				if(response.data.errorCode==1){
                    $scope.metadataList.splice(index,1);
					dataFactory.successAlert("Metadata delete successfully");
                    $('.stepFolder2').slideDown('fast');
                    $('.stepFolder1').slideDown('fast');
                    $('.stepFolder3').slideUp('fast');
                    $scope.selectedConfirm = 0;
                    $scope.backBtn = 0;
				}else{
					dataFactory.errorAlert("Check your connection");
				}
			});
		}
// Metadata Delete

        /*
        Report Grp
         */




// sort metadata blending list asc-desc Start
         $scope.metadataList_Order = 'desc';
         $scope.metadata_List_order = '-id';
         $scope.orderMeataDataList = function(){
             if($scope.metadataList_Order == 'desc'){
                 $scope.metadata_List_order = '-id';
             }
             else if($scope.metadataList_Order == 'asc'){
                 $scope.metadata_List_order = 'id';
             }
         }
// sort metadata blending list asc-desc End

         vm.selectedGrpFolder = function(){
             vm.groupFolderList = Object.keys(vm.metaList[vm.sDepartment]);
             setTimeout(function () {
                 $('[rel="tooltip"]').tooltip();
                 $scope.loadingBarDrawer = false;
                 $('.stepFolder1').slideUp('fast');
                 $('.stepFolder2').slideUp('fast');
                 $('.stepLoading').slideUp('fast');
                 $('.stepFolder4').slideDown('fast');
             }, 300);
         }

         vm.loadMetaDataList = function(){
             setTimeout(function(){
                 $(function () {
                     $('[data-toggle="tooltip"]').tooltip();
                 })
             },100);
             $('.imageToggle').removeClass('selectedImage');
             $scope.loadingBarDrawer = true;
             $('.stepFolder1').slideUp('fast');
             $('.stepFolder4').slideUp('fast');
             $('.stepBack').slideUp('fast');
             setTimeout(function () {
                 $scope.loadingBarDrawer = false;
                 $('.stepFolder2').slideUp('fast');
                 $('.stepLoading').slideUp('fast');
                 $('.stepFolder3').slideDown('fast');
                 $('.stepBack').slideDown('fast');
             }, 300);
         }

         vm.toggleGrpFolder = function(e, sView, index){
             vm.selectedSharedViewName = sView;
             vm.metadataList = vm.metaList[vm.sDepartment][sView];
             vm.loadMetaDataList();
         }

         vm.grpFolder = false;
         $scope.metadataFolder = {
             toggleImage: function (e, sView, index) {
                 vm.backBtn = 1;
                 if(sView == 'Public View'){
                     vm.selectedSharedViewName = sView;
                     vm.metadataList = $scope.metaList[$scope.selectedSharedViewName];
                     vm.loadMetaDataList();
                 }else{
                     $scope.grpFolder = true;
                     vm.sDepartment = sView;
                     vm.selectedGrpFolder();
                 }
             },
             backToSharedView: function () {
             	 $scope.search = '';
                 $('.stepFolder2').slideDown('fast');
                 $('.stepFolder1').slideDown('fast');
                 $('.stepFolder3').slideUp('fast');
                 $scope.selectedConfirm = 0;
                 $scope.backBtn = 0;
             }
         };
         /*
          * Export
          */
         /*
		  * Export and import
		  */
         $scope.export=function () {
             var data={
                 "metadataId":$scope.selectedMetadata.metadata_id
             };
             dataFactory.request($rootScope.MetadataExport_Url,'post',data).then(function(response) {
                 if(response.data.errorCode){
                     var link=document.createElement('a');
                     link.href=response.data.result.url;
                     link.download=response.data.result.fileName;
                     link.click();
                     var data={
                         "url":response.data.result.url
                     };
                     dataFactory.request($rootScope.ExportFileDelete_Url,'post',data);
                 }
             })
         }
         /*
          *   Import metadata
          */
         $scope.import=0;
         $scope.importBtn=function () {
             $scope.import=1;
             $('.step1').slideUp('fast');
             $('.step2').slideDown('fast');
             $scope.testsuccess=false;
             $scope.datasourceStatus=0;
             $scope.metadataStatus=0;
         }
         $scope.backToFolder=function () {
             $scope.import=0;
             $('.step2').slideUp('fast');
             $('.step1').slideDown('fast');
         }
         $scope.testImport=function (datasourceImport,type) {
             $(".loaderImage").fadeIn("slow");
             if($scope.metadataImport==undefined || $scope.metadataImport['file']==undefined){
                 dataFactory.errorAlert("File is required");
                 return false;
             }
             var file=$scope.metadataImport['file'];
             var allowedFiles=["think"];
             var file=$scope.metadataImport.file;
             var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:])+(" + allowedFiles.join('|') + ")$");
             if (!regex.test(file.name.toLowerCase())) {
                 dataFactory.errorAlert("Please upload files having extensions: <b>" + allowedFiles.join(', ') + "</b> only.");
                 return false;
             }
             $scope.testBtn="Loading...";
             var url=$rootScope.MetadataImport_Url;
             dataFactory.importData(url,file,type).then(function(response) {
                 if(response.data.errorCode){
                     $(".loadingBar").hide();
                     $scope.Attributes=response.data.result;
                     dataFactory.errorAlert(response.data.message);
                     $scope.importSave=1;
                     $scope.testsuccess=true;
                     $scope.datasourceStatus=response.data.result.datasource;
                     $scope.metadataStatus=response.data.result.metadata;
                     $scope.datasourceOverwrite=true;
                     $scope.metadataOverwrite=true;
                 }else{
                     dataFactory.errorAlert(response.data.message);
                 }
                 setTimeout(function(){
                     $scope.loading=false;
                     if(!$scope.loading){
                         $(".loaderImage").fadeOut("slow");
                     }
                 },1000);
             });
         }
         $scope.checkView=function(page){
             $scope.publicViewType=page;
             if(page=='public_group'){
                 $(".subDiv").hide();
                 $(".footerDiv").show();
             }
             else{
                 $(".subDiv").show();
                 $(".footerDiv").hide();
             }
         }
         $scope.publicViewType="specific_group";
         $scope.reportGrp={};
         dataFactory.request($rootScope.UsergroupList_Url,'post',"").then(function(response){
             $scope.userGroup=response.data.result;
         });
         // dataFactory.request($rootScope.ReportGroupList_Url,'post',"").then(function(response){
         //     $scope.reportGroup=response.data.result;
         // });
         $scope.selectGroup=function(divClass){
             if(divClass=='back'){
                 $(".mainDiv").slideDown('fast');
                 $(".select").slideUp('fast');
                 $(".create").slideUp('fast');
                 $(".footerDiv").slideUp('fast');
             }else{
                 $("."+divClass).slideDown('fast');
                 $(".footerDiv").slideDown('fast');
                 $(".mainDiv").slideUp('fast');
             }
             if(divClass=='select' || divClass=='create'){
                 $scope.reportGrp={};
             }
         }
         $scope.groupTypeModal=function(){
             $('#reportGroup').modal('show');
         }
         $scope.overwrite={};
         $scope.overwrite.datasource=1;
         $scope.overwrite.metadata=1;
         $scope.groupTypeSave=function () {
             $(".loaderImage").show();
             if($scope.metadataImport['file']==undefined){
                 $scope.addClassRemove("File is required");
                 return false;
             }
             var file=$scope.metadataImport['file'];
             var allowedFiles=["think"];
             var file=$scope.metadataImport.file;
             var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:])+(" + allowedFiles.join('|') + ")$");
             if (!regex.test(file.name.toLowerCase())) {
                 $scope.addClassRemove("Please upload files having extensions: <b>" + allowedFiles.join(', ') + "</b> only.");
                 return false;
             }
             var data={
                 "overwrite":$scope.overwrite
             };
             var url=$rootScope.MetadataImportSave_Url;
             dataFactory.importDataSave(url,file,'',$scope.reportGrp,$scope.publicViewType,JSON.stringify(data)).then(function(response) {
                 if(response.data.errorCode){
                     $(".loaderImage").hide();
                     $scope.backToFolder();
                     dataFactory.successAlert(response.data.message);
                     $scope.metadataListApi();
                 }else{
                     dataFactory.errorAlert(response.data.message);
                 }
             });
             $('#reportGroup').modal('hide');

         }
      }]);
