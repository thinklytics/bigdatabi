'use strict';
/* Controllers */
/* ,'ui.layout' */
angular	.module('app', [ 'ngSanitize' ])
    .controller('metadataBlendingEditController',
        ['$scope','$sce','dataFactory','$rootScope','$filter','$window','$stateParams','$cookieStore','$location',
            function($scope, $sce, dataFactory, $rootScope, $filter, $window,$stateParams,$cookieStore,$location) {
                var vm=$scope;

                /*
                 Init variable
                 */
                var drawComponent = {};
                var i = 0;
                var lastX;
                var lastY;
                var root;
                var rect;
                var line;
                // Konvas to draw function call
                var stage = new Konva.Stage({
                    container : 'canvasContainer',
                    width : canvas.canvasWidth,
                    height : canvas.canvasHeight
                });
                var layer = new Konva.Layer();
                /*
                 Excel database check
                 */
                vm.datasourceTypeObject={};
                vm.excelBtnDSType=false;
                vm.dataTypeChangeCheck=false;
                /*
                End
                 */
                imageObj.src = 'assets/img/joins-images/join.svg';
                var drawComponent = {};
                $scope.selectedMetadataId=[];
                // initialize
                Belay.init({
                    strokeWidth : 1
                });
                Belay.set('strokeColor', 'blue');
                vm.metadataBlendingObj={
                    init:function(){
                        //initial variable define
                        $("#chartjs-tooltip").hide();
                        var data={};
                        var addClass='';
                        var viewClass='';
                        if($location.path()=='/metadata/'){
                            viewClass='activeli';
                        }else{
                            addClass='activeli';
                        }
                        data['navTitle']="DATA BLENDING";
                        data['icon']="fa fa-database";
                        data['navigation']=[{
                            "name":"Add",
                            "url":"#/metadata/add",
                            "class":addClass
                        },
                            {
                                "name":"View",
                                "url":"#/metadata/",
                                "class":viewClass
                            }];
                        $rootScope.$broadcast('subNavBar', data);
                        var vm = $scope;
                        //End Menu
                        //State params
                        //End
                        var userId=$cookieStore.get('userId');
                        //Default tab define
                        $scope.tab='home';
                        //End tab
                        $scope.metadataSaveShow = 0;
                        $scope.filteredData = [];
                        $scope.updateBtnOnLoad = false;
                        $scope.tableOutput=false;
                        // Div Height define
                        $("#custHeightFirstdiv").height(
                            $(window).height() / 2 - 68);
                        // $("#custHeightSeconddiv").height($(window).height()/2);
                        // End
                        $scope.Attributes = {};
                        $scope.trustAsHtml = function(value) {
                            return $sce.trustAsHtml(value);
                        };

                        $scope.Form = {};
                        $scope.Form.node = [];
                        $scope.myForm = {};
                        $scope.metadataInfo1 = false;
                        $scope.metadataNamePlaceholder = "Metadata Name....";
                        $scope.nameTypeDimension='database_dimension';
                        $scope.nameTypeMeasure='database_measure';
                        //Metadata List
                        dataFactory.request($rootScope.MetadataList_Url,'post',"").then(function(response) {
                            $scope.loadingBarList=false;
                            if(response.data.errorCode==1){
                                $scope.tableList=[];
                                var tempObj={};
                                var result=response.data.result;
                                Object.keys(result).forEach(function(d){
                                    result[d].forEach(function(k){
                                        if(tempObj[k.name]==undefined){
                                            tempObj[k.name]=true;
                                            $scope.tableList.push(k);
                                        }
                                    });
                                });
                            }else{
                                dataFactory.errorAlert(response.data.message);
                            }
                            $("#datasource").selectpicker();
                        }).then(function(){
                            $scope.tableList.forEach(function (d) {
                                //vm.datasourceTypeObject[d.name]=d.connObject.connectionObject.datasourceType;
                            })
                        })
                        /*
                         Canvas inti variable for join server
                         */
                        // Lisner
                        $scope.renameBackupArray = [];
                        $scope.selectedTableName = [];

                        // jquery
                        $('.joinHover').click(function() {
                            $('.joinActive').removeClass('joinActive');
                            $(this).addClass('joinActive');
                        });
                        /*
                         Insert filter column
                         */
                        $scope.whereCondition="";
                        $scope.whereConditionText="";
                        /*
                         Save filter init variable
                         */
                        $scope.limit="";
                        $scope.limitInput="";
                        $scope.limitCheck=false;
                        /*
                         On drop default variable
                         */
                        $scope.tableArray=[];
                        $scope.noRow=[0];

                        /*
                        Get metadata id
                         */
                        $scope.metadataId = $stateParams.id;
                        vm.metadataBlendingObj.getMetadataObj();
                    },
                    getMetadataObj:function(){
                        //checkbox value define
                        $scope.source = {};
                        $scope.Attributes = {};
                        $scope.Attributes.checkboxModelDimension = {};
                        //Get Metadata list
                        $scope.formCheckbox = {};
                        $scope.selectedTableName = [];
                        $scope.myForm = {};
                        $(".loadingBar").show();

                        var data = {
                            "metadataId": $scope.metadataId
                        };
                        dataFactory.request($rootScope.MetadataGet_Url, 'post', data).then(function (response) {
                            if (response.data.errorCode == 1) {
                                $scope.metaDataObject = JSON.parse(response.data.result.metadataObject);
                                vm.tableColumn = $scope.metaDataObject.column;
                                vm.tableColumnCopy = vm.tableColumn;
                                console.log(vm.tableColumn)

                                if ($scope.metaDataObject.condition) {
                                    $scope.condition = $scope.metaDataObject.condition;
                                    $scope.whereConditionText = $scope.metaDataObject.condition;
                                    $scope.whereCondition = $scope.metaDataObject.condition;
                                }
                                //&& $scope.limitCheck==true
                                if ($scope.metaDataObject.limit) {
                                    $scope.limit = $scope.metaDataObject.limit;
                                    $scope.limitText = $scope.metaDataObject.limit;
                                    $scope.limitCheck = true;
                                }
                                $scope.joinGetDetails = $scope.metaDataObject.joinsDetails;
                                Object.keys($scope.joinGetDetails).forEach(function (d) {
                                    $scope.myForm[d] = $scope.joinGetDetails[d];
                                });
                                $scope.metadataName = response.data.result.name;
                                $scope.groupObject = JSON.parse(response.data.result.group);
                                $scope.dashboard = $scope.metaDataObject.dashboard;
                                $scope.Attributes.checkboxModelDimension[$scope.metaDataObject.rootTable] = true;
                                vm.lastSelectedMetadata=$scope.metaDataObject.selectedMetadata;
                                Object.keys($scope.metaDataObject.joinsDetails).forEach(function (d) {
                                    $scope.Attributes.checkboxModelDimension[d] = true;
                                });
                                vm.metadataBlendingObj.onDrop("", 0, $scope.metaDataObject.rootTable, "", "", "", true);
                                Object.keys($scope.joinGetDetails).forEach(function (d) {
                                    $scope.metadataBlendingObj.onDrop("", 0, d, "", "", "", true);
                                });
                                vm.metadataBlendingObj.getData('default');
                                //Change image canvas
                                for (var i = 0; i < 2; i++) {
                                    Object.keys($scope.joinGetDetails).forEach(function (s) {
                                        $scope.selectjoin = $scope.joinGetDetails[s].join;
                                        layer.children.forEach(function (d) {
                                            d.children.forEach(function (p, index) {
                                                if (p.className == "Image" && p.attrs.id == s) {
                                                    if ($scope.selectjoin == "Inner") {
                                                        p.attrs.image.src = "assets/img/joins-images/join_inner.svg";
                                                        p.attrs.image.srcset = "assets/img/joins-images/join_inner.svg";
                                                    } else if ($scope.selectjoin == "Left") {
                                                        p.attrs.image.src = "assets/img/joins-images/join_left.svg";
                                                        p.attrs.image.srcset = "assets/img/joins-images/join_left.svg";
                                                    } else if ($scope.selectjoin == "Right") {
                                                        p.attrs.image.src = "assets/img/joins-images/join_right.svg";
                                                        p.attrs.image.srcset = "assets/img/joins-images/join_right.svg";
                                                    } else if ($scope.selectjoin == "Outer") {
                                                        p.attrs.image.src = "assets/img/joins-images/join_outer.svg";
                                                        p.attrs.image.srcset = "assets/img/joins-images/join_outer.svg";
                                                    }
                                                    layer.draw();
                                                }
                                            });
                                        });
                                    });
                                }
                                $(".loadingBar").hide();
                            } else {
                                dataFactory.errorAlert("Check your connection");
                            }
                        });
                    },
                    onDrop:function(event, index, item,external, type, allowedType, checboxValue) {
                        if(item.name!=undefined){
                            var tempObj={};
                            tempObj['name']=item.name;
                            tempObj['metadataId']=item.metadata_id;
                            tempObj['connObject']=JSON.parse(item.metadataObject);
                            item=tempObj;
                            var itemObj=item;
                            item=itemObj.name;
                        }
                        else{
                            var itemObj={};
                            itemObj.name=item;
                            itemObj.metadataId=vm.lastSelectedMetadata;
                        }
                        $scope.metadataSaveShow = 0;// save button hide
                        $("#dataTableDiv").empty();
                        var group = new Konva.Group({
                            "id" : item
                        });
                        if (!drawComponent[item]) {
                            drawComponent[item] = {};
                        }
                        if (checboxValue) {
                            if(Array.isArray(itemObj.metadataId))
                                $scope.selectedMetadataId=itemObj.metadataId;
                            else
                                $scope.selectedMetadataId.push(itemObj.metadataId);
                            $scope.selectedTableName.push(item);
                            if (i == 0 || stage.children[0].children.length==0) {
                                root = canvas.getRootRect(item);
                                rect = root;
                            } else {
                                rect = root.attrs.addChild(item);
                                $scope.rootNew=rect;
                                line = canvas.drawLine(root, rect, item);
                                drawComponent[item]["line"] = line;
                                group.add(line);
                                // Add image
                                imageObj.src="assets/img/joins-images/join.svg";
                                imageObj.srcset="assets/img/joins-images/join.svg";
                                var image = canvas.drawImage(rect, item,imageObj);
                                drawComponent[item]['image'] = image;
                                image.on('click', function() {
                                    $scope.popup(item);
                                });
                                group.add(image);
                            }
                            var text = canvas.textDraw(rect, item);
                            drawComponent[item]["rect"] = rect;
                            drawComponent[item]["text"] = text;
                            group.add(rect);
                            group.add(text);
                            // add the layer to the stage
                            layer.add(group);
                            stage.add(layer);
                            i++;
                        } else {
                            if(Object.keys($scope.myForm).length)
                                delete $scope.myForm[item];
                            var deleteIndex=$scope.selectedTableName.indexOf(item);
                            var MetadataIdIndex=$scope.selectedMetadataId.indexOf(itemObj.metadataId);
                            $scope.selectedMetadataId.splice(MetadataIdIndex,1);
                            $scope.selectedTableName.splice(deleteIndex,1);
                            // var tempDeletedItem=drawComponent[item];
                            root.attrs.removeObject();
                            var toDeleteIndex = -1;
                            stage.children[0].children
                                .forEach(function(d, index) {
                                    if (d.attrs.id == item) {
                                        toDeleteIndex = index;
                                    }
                                });
                            var row=stage.children[0].children[toDeleteIndex].find('Rect')[0].attrs.row;
                            if(deleteIndex==0){
                                $scope.selectedTableName=[];
                                $scope.Attributes.checkboxModelDimension={};
                                layer.destroy();
                                i =0;

                            }else{
                                stage.children[0].children.splice(toDeleteIndex, 1);
                                stage.children[0].children.forEach(function(d, index) {
                                    var node=d.find("Rect")[0];
                                    if(row<node.attrs.row)
                                    {
                                        node.attrs.row--;
                                        updateElement(node,d,root);
                                    }
                                });
                            }
                            layer.draw();
                        }
                        if(stage.children[0].children.length!=undefined){
                            if (stage.children[0].children.length) {
                                $scope.updateBtnOnLoad = true;
                            } else {
                                $scope.updateBtnOnLoad = false;
                            }
                        }
                    },
                    popup:function(tableName) {
                        vm.errorText="";
                        vm.excelBtnDSType=false;
                        $(".loadingBar").show();
                        if ($scope.joinGetDetails[tableName]) {
                            $scope.Form = {};
                            $scope.Form.node = [];
                            var joinDetailsObject = $scope.myForm[tableName];
                            $scope.joinConfiguration = true;
                            var join = joinDetailsObject.join;
                            $scope.selectjoin=join;
                            $(".joinHover").removeClass('joinActive');
                            $("." + join).addClass('joinActive');
                            $scope.joinSelected = true;
                            //setTimeout(function () {
                            $scope.Form.leftColumnTable = joinDetailsObject.leftColumnTable;
                            if (!$scope.$$phase) {
                                $scope.$apply();
                            }
                            //},1000);
                            $scope.sourceSelected = true;
                            $scope.noRow = joinDetailsObject.row;
                            $scope.selectTable = tableName;
                            var data = {
                                "metadataId": $scope.selectedMetadataId
                            };
                            dataFactory.request($rootScope.BlendingColumnsInfo_Url, 'post', data).then(function (response) {
                                if (response.data.errorCode == 1) {
                                    var response = response.data.result;
                                    vm.multiSourceColumn = response;
                                    $.each(vm.multiSourceColumn,function(key,value){
                                        var checkObj={};
                                        if((Object.keys(value.columns).length)>1){
                                            var firstTable="";
                                            var i=0;
                                            var changeFirstTable={};
                                            $.each(value.columns,function(columnKey,columnValue){
                                                if(i==0){
                                                    firstTable=columnKey;
                                                }
                                                columnValue.forEach(function(d){
                                                    if(checkObj[d['Field']]==undefined){
                                                        checkObj[d['Field']]=true;
                                                    }else{
                                                        changeFirstTable[d['Field']]=true;
                                                        d['Field']=d['Field']+"("+d['tableName']+")";
                                                    }
                                                });
                                                i++;
                                            });
                                            if(value.type!='mongodb'){
                                                value.columns[firstTable].forEach(function(k){
                                                    if(changeFirstTable[k['Field']]){
                                                        k['Field']=k['Field']+"("+k['tableName']+")";
                                                    }
                                                });
                                            }
                                        }
                                    });
                                    if (!$scope.$$phase) {
                                        $scope.$apply();
                                    }
                                }
                            }).then(function () {
                                vm.selectLeftColumnNameList = $scope.multiSourceColumn[$scope.Form.leftColumnTable].columns;
                                vm.selectedColumnNameList = $scope.multiSourceColumn[$scope.selectTable].columns;
                            }).then(function(){
                                setTimeout(function(){
                                    $scope.myForm[tableName].row.forEach(function(d){
                                        $scope.Form.node[d]={};
                                        var leftJson=JSON.parse($scope.myForm[tableName].node[d].leftColumn);
                                        var leftObj="";
                                        vm.selectLeftColumnNameList[leftJson.tableName].forEach(function(k){
                                            if(k.Field==leftJson.Field){
                                                delete k['$$hashKey'];
                                                leftObj=k;
                                            }
                                        });
                                        $scope.Form.node[d].leftColumn=JSON.stringify(leftObj);
                                        var rightJson=JSON.parse($scope.myForm[tableName].node[d].rightColumn);
                                        var rightObj="";
                                        vm.selectedColumnNameList[rightJson.tableName].forEach(function(k){
                                            if(k.Field==rightJson.Field){
                                                delete k['$$hashKey'];
                                                rightObj=k;
                                            }
                                        });
                                        $scope.Form.node[d].rightColumn=JSON.stringify(rightObj);
                                        if (!$scope.$$phase) {
                                            $scope.$apply();
                                        }
                                    });
                                    $(".loadingBar").hide();
                                },1000);
                            });
                        } else {
                            $scope.tableArray.push(tableName);
                            $scope.joinConfiguration = true;
                            $scope.selectTable = tableName;
                            //$scope.selectjoin = "Join";
                            $(".joinHover").removeClass('joinActive');
                            // Left and Right column
                            if ($scope.tableArray.indexOf(tableName) == -1) {
                                $scope.sourceSelected = false;
                                $scope.joinSelected = false;
                            }
                            if ($scope.myForm[$scope.selectTable] == undefined) {
                                $scope.Form = {};
                                $scope.Form.node = [];
                                $scope.joinSelected = false;
                                $scope.sourceSelected = false;
                            }
                            var data = {
                                "metadataId": $scope.selectedMetadataId
                            };

                            dataFactory.request($rootScope.BlendingColumnsInfo_Url, 'post', data).then(function (response) {
                                if (response.data.errorCode == 1) {
                                    var response = response.data.result;
                                    vm.multiSourceColumn = response;
                                    $.each(vm.multiSourceColumn,function(key,value){
                                        var checkObj={};
                                        if((Object.keys(value.columns).length)>1){
                                            var firstTable="";
                                            var i=0;
                                            var changeFirstTable={};
                                            $.each(value.columns,function(columnKey,columnValue){
                                                if(i==0){
                                                    firstTable=columnKey;
                                                }
                                                columnValue.forEach(function(d){
                                                    if(checkObj[d['Field']]==undefined){
                                                        checkObj[d['Field']]=true;
                                                    }else{
                                                        changeFirstTable[d['Field']]=true;
                                                        d['Field']=d['Field']+"("+d['tableName']+")";
                                                    }
                                                });
                                                i++;
                                            });
                                            if(value.type!='mongodb'){
                                                value.columns[firstTable].forEach(function(k){
                                                    if(changeFirstTable[k['Field']]){
                                                        k['Field']=k['Field']+"("+k['tableName']+")";
                                                    }
                                                });
                                            }
                                        }
                                    });
                                    if (!$scope.$$phase) {
                                        $scope.$apply();
                                    }
                                }
                                $(".loadingBar").hide();
                            });
                        }

                        var modalElem = $('#tableJoin');
                        $('#tableJoin').modal({
                            backdrop : 'static',
                            keyboard : false
                        });
                    },
                    getData:function(type) {
                        var data = {
                            "metadataId": $scope.selectedMetadataId
                        };
                        dataFactory.request($rootScope.BlendingGetData_Url, 'post', data).then(function (response) {
                            if(response.data.errorCode==1){
                                //Response to Result
                                vm.tableBlendingData = response.data.result;
                                var i=1;
                                var keysArray = [];
                                var k = 0;
                                var parseTableData=[];
                                //Take table data and table columns
                                var tableQuery="Select * From ? ";
                                var dataJsonKeyArray=[];
                                var tableDataJson=[];
                                Object.keys(vm.tableBlendingData).forEach(function(data){
                                    window["table"+i+"Data"] = JSON.parse(vm.tableBlendingData[data]['tableData']);
                                    var tableColumn = JSON.parse(vm.tableBlendingData[data]['tableColumn']);
                                    tableDataJson.push(JSON.parse(vm.tableBlendingData[data]['tableData']));
                                    $.each(tableColumn,function(key,d){
                                        keysArray[k] = {
                                            "columType" : d.Type,
                                            "tableName" : d.tableName,
                                            "columnName" : d.Field,
                                            "reName" : d.Field.split('_'+d.tableName)[0],
                                            "reName" : d.Field,
                                            "dataKey" : d.dataType,
                                            "type" : "defined",
                                            "dataType":"table"+i+"Data",
                                            "metadata":data
                                        };
                                        k++;
                                    });

                                    dataJsonKeyArray.push("table"+i+"Data");
                                    if(i==1){
                                        tableQuery+=dataJsonKeyArray[i-1];
                                    }else{
                                        tableQuery+=" Join ? "+dataJsonKeyArray[i-1];
                                        //Table joins
                                        $scope.myForm[data].node.forEach(function(d,index){
                                            var leftTable=JSON.parse(d.leftColumn).tableName;
                                            var rightTable=JSON.parse(d.rightColumn).tableName;
                                            var leftcolumn=JSON.parse(d.leftColumn).Field;
                                            var rightColumn=JSON.parse(d.rightColumn).Field;
                                            if(leftcolumn.match("("+leftTable+")")!=null){
                                                leftcolumn = leftcolumn.replace("("+leftTable+")","_"+leftTable);
                                            }
                                            if(rightColumn.match("("+rightTable+")")!=null){
                                                rightColumn = rightColumn.replace( "("+rightTable+")","_"+rightTable);
                                            }
                                            if(index==0)
                                                tableQuery+=" on "+dataJsonKeyArray[i-2]+"."+leftcolumn+"_"+leftTable+"="+dataJsonKeyArray[i-1]+"."+rightColumn+"_"+rightTable;
                                            else
                                                tableQuery+=" and "+dataJsonKeyArray[i-2]+"."+leftcolumn+"_"+leftTable+"="+dataJsonKeyArray[i-1]+"."+rightColumn+"_"+rightTable;
                                        });
                                    }
                                    i++;
                                });
                                // For check condition
                                vm.dataJsonKeyArray=dataJsonKeyArray;
                                /*customers_id(customers)
                                 WHere condition
                                 */
                                if(vm.whereCondition){
                                    var tempWhere=vm.whereCondition;
                                    //if(tempWhere.match('\(')!=null){
                                    tempWhere=tempWhere.replace("(","_");
                                    tempWhere=tempWhere.replace(")","");
                                    //}
                                    tableQuery+=" where "+tempWhere;
                                }
                                if(vm.limit){
                                    tableQuery+=" LIMIT "+vm.limit;
                                }

                                vm.tableData = alasql(tableQuery,tableDataJson);
                                vm.tableColumn = keysArray;

                                console.log('keysArray', keysArray)
                                console.log('tableColumnCopy', vm.tableColumnCopy)
                                console.log(Object.keys(vm.tableBlendingData))

                                vm.filterTableColumn = [];
                                vm.tableColumn.forEach(function(d,i){
                                    if(d.columnName.match("_"+d.tableName)!=null){
                                        var tempObj={
                                            "columType" : d.columType,
                                            "tableName" : d.tableName,
                                            "columnName" : d.columnName.replace("_"+d.tableName,"("+d.tableName+")"),
                                            "reName" : d.reName.replace("_"+d.tableName,"("+d.tableName+")"),
                                            "dataKey" : d.dataKey,
                                            "type" : "defined",
                                            "dataType" : d.dataType,
                                        };
                                        vm.filterTableColumn.push(tempObj);
                                    }else{
                                        vm.filterTableColumn.push(d);
                                    }
                                });

                                //ENd Table columns
                                //Filter check
                                $scope.loading = false;
                                // $scope.tabledata = true;
                                $scope.savequery = true;
                                $scope.Dttable = true;
                                vm.metadataBlendingObj.dataTableCall();
                                $scope.filterCheck=true;
                            }
                        }).then(function(d){
                            $scope.metadataObject = {};
                            $scope.metadataObject['joinsDetails'] = $scope.myForm;
                            $scope.metadataObject['rootTable'] = $scope.selectedTableName[0];
                            $scope.metadataObject['limit'] = $scope.limit;
                            $scope.metadataObject['limitCheck'] = $scope.limitCheck;
                            $scope.metadataObject['condition'] = $scope.whereCondition;
                            $scope.metadataObject['column'] = vm.tableColumn;
                            $scope.updateBtnOnLoad=false;
                            $("#datatable_btn").click();
                            if(type!="default")
                                dataFactory.successAlert("Data get successfully");
                        });
                    },
                    popOverSave:function(formObj) {
                        var dataTypeCheck=true;
                        formObj.node.forEach(function(d){
                            var leftObj = JSON.parse(d.leftColumn);
                            var rightObj = JSON.parse(d.rightColumn);
                            var tempLeftObj=leftObj.Type.split("(");
                            var leftObjCheck="";
                            if(tempLeftObj)
                                leftObjCheck=tempLeftObj[0];
                            else
                                leftObjCheck=leftObj.Type;
                            var tempRightObj=rightObj.Type.split("(");
                            var rightObjCheck="";
                            if(tempRightObj)
                                rightObjCheck=tempRightObj[0];
                            else
                                rightObjCheck=rightObj.Type;
                            if(leftObjCheck=="int" || leftObjCheck=="integer"){
                                leftObj.Type="int";
                            }
                            if(rightObjCheck=="int" || rightObjCheck=="integer"){
                                rightObj.Type="int";
                            }
                            if(leftObj.Type!=rightObj.Type){
                                vm.modelError=true;
                                if(vm.datasourceTypeObject[vm.Form.leftColumnTable]=="excel"){
                                    vm.excelSelectedDatabase=vm.Form.leftColumnTable;
                                    vm.excelBtnDSType=true;
                                }
                                if(vm.datasourceTypeObject[vm.selectTable]=="excel"){
                                    vm.excelSelectedDatabase=vm.selectTable;
                                    vm.excelBtnDSType=true;
                                }
                                vm.errorText="This two fields data type does not match "+leftObj.Field+"("+leftObj.Type+") "+rightObj.Field+"("+rightObj.Type+")";
                                //dataFactory.errorAlert("This two field data type does not match "+leftObj.Field+"("+leftObj.Type+") &nbsp;"+rightObj.Field+"("+rightObj.Type+") &nbsp;");
                                dataTypeCheck=false;
                            }
                        });
                        if(dataTypeCheck) {
                            $("#dataTableDiv").empty();
                            $scope.updateBtnOnLoad = true;
                            formObj['join'] = $scope.selectjoin;
                            formObj['rightColumnTable'] = $scope.selectTable;
                            var rootTable = $scope.selectedTableName[0];
                            formObj["root"] = $scope.selectedTableName[0];
                            formObj["row"] = $scope.noRow;
                            var myformObj = {};
                            myformObj = formObj;
                            $scope.myForm[$scope.selectTable] = myformObj;
                            layer.children.forEach(function (d) {
                                d.children.forEach(function (p, index) {
                                    if (p.className == "Image" && p.attrs.id == formObj.rightColumnTable) {
                                        if ($scope.selectjoin == "Inner") {
                                            p.attrs.image.src = "assets/img/joins-images/join_inner.svg";
                                            p.attrs.image.srcset = "assets/img/joins-images/join_inner.svg";
                                        } else if ($scope.selectjoin == "Left") {
                                            p.attrs.image.src = "assets/img/joins-images/join_left.svg";
                                            p.attrs.image.srcset = "assets/img/joins-images/join_left.svg";
                                        } else if ($scope.selectjoin == "Right") {
                                            p.attrs.image.src = "assets/img/joins-images/join_right.svg";
                                            p.attrs.image.srcset = "assets/img/joins-images/join_right.svg";
                                        } else if ($scope.selectjoin == "Outer") {
                                            p.attrs.image.src = "assets/img/joins-images/join_outer.svg";
                                            p.attrs.image.srcset = "assets/img/joins-images/join_outer.svg";
                                        }
                                        layer.draw();
                                    }
                                });
                            });
                            vm.metadataBlendingObj.closePopup();
                            layer.draw();
                            $("#metadata_btn").click();
                            $("#tableJoin").modal('hide');
                        }
                    },
                    joinType:function(type){
                        $scope.selectjoin = type;
                        $scope.joinSelected = true;
                        $(".joinHover").removeClass('joinActive');
                        $("."+type).addClass('joinActive');
                    },
                    closePopup:function() {
                        $scope.joinConfiguration = false;
                    },
                    dataTableCall:function(){
                        var tableHeaders = "";
                        var i = 0;
                        var k = 0;
                        $scope.sumLengthCOlumn = 0;
                        $scope.columnObjectTable = [];
                        var Keys = $scope.KeysForTable;

                        for ( var t = 0; t < vm.tableColumn.length; t++) {
                            var tableArray = "";
                            tableHeaders += "<th>"
                                + '<a href="javascript:void(0)" data-ng-click="test()" style="float:right" class="toggle-vis" data-column="'
                                + k
                                + '"><i class="icon mdi mdi-delete" ></i></a>'
                                + vm.tableColumn[t].columType
                                + "<br>"
                                + vm.tableColumn[t].tableName
                                + "<br>"
                                + vm.tableColumn[t].reName
                                + "</th>";
                            k++;
                            $scope.sumLengthCOlumn++;
                        }
                        var tableData = "";
                        var tableData1 = "";
                        var tableBody1 = "";

                        for ( var s = 0; s < vm.tableData.length; s++) {
                            var tableBody = "";
                            for ( var ni = 0; ni < vm.tableColumn.length; ni++) {
                                tableData += '"' + ni + '",';
                                tableBody += "<td>"
                                    + vm.tableData[s][vm.tableColumn[ni].columnName]
                                    + "</td>";
                            }
                            tableData1 += "[" + // tableData.substring(0,tableData.length
                                // - 1)
                                +"],";
                            tableBody1 += "<tr>" + tableBody + "</tr>";
                        }
                        $("#dataTableDiv").empty();
                        $("#dataTableDiv").append('<table id="displayTable" class="table table-striped table-no-bordered table-hover dataTable dtr-inline"><thead><tr>'
                            + tableHeaders
                            + '</tr></thead><tbody>'
                            + tableBody1
                            + '</tbody></table>');
                        $('.toggle-vis').on('click',function(e) {
                            e.preventDefault();
                            var column = table.column($(this).attr('data-column'));
                            vm.columnObjectTable.splice(column[0][0], 1);
                            column.visible(!column.visible());
                        });
                        var table = $('#displayTable').DataTable({
                            "scrollY" : 280,
                            "scrollX" : true,
                            "bFilter" : false,
                            "bSearchable" : false,
                            /*
                             * "pageLength": 1000,
                             */
                            "bInfo" : false,
                            "bLengthChange" : false,
                            "paging" : false,
                            "ordering" : false
                        });
                        if ($scope.lastColumnShow) {
                            var $scrollBody = $(table.table().node())
                                .parent();
                            $scrollBody
                                .scrollLeft($scrollBody.get(0).scrollWidth);
                            $(
                                '#displayTable td:nth-child('
                                + $scope.sumLengthCOlumn
                                + ')').css(
                                'background-color', '#CCC');
                            setTimeout(
                                function() {
                                    $(
                                        '#displayTable td:nth-child('
                                        + $scope.sumLengthCOlumn
                                        + ')')
                                        .animate(
                                            {
                                                backgroundColor : '#f5f5f5'
                                            }, 'slow');
                                }, 2000);
                        }
                        $scope.lastColumnShow = false;
                        $scope.tableOutput=true;
                    },
                    getValueVariable:function() {
                        return new Promise(function (resolve, reject) {
                            $scope.selectedTable = [];
                            Object.keys($scope.Attributes.checkboxModelDimension).forEach(function (d) {
                                $scope.selectedTable.push(d);
                            });
                            // Left and Right column
                            $scope.sourceSelected = true;
                            vm.selectLeftColumnNameList = $scope.multiSourceColumn[$scope.Form.leftColumnTable].columns;
                            vm.selectedColumnNameList = $scope.multiSourceColumn[$scope.selectTable].columns;
                            resolve();
                        });
                    },
                    saveFilter:function(condition,limitInput){
                        $scope.whereCondition=condition;
                        $scope.whereConditionText=condition;
                        $scope.limit=limitInput;
                        $('#myModelFilter').modal('hide');
                        $("#dataTableDiv").empty();
                        $scope.updateBtnOnLoad = true;
                    },
                    metaDataSave:function() {
                        try{
                            if($scope.metadataName){
                                vm.dataJsonKeyArray.forEach(function(d){
                                    var regex = new RegExp(d+".", "g");
                                    $scope.metadataObject.condition=$scope.metadataObject.condition.replace(regex, "");
                                });
                                var tempObj={};
                                $scope.metadataObject.column.forEach(function(d){
                                    if(d.columnName.match("_"+d.tableName)!=null){
                                        d.columnName = d.columnName.replace("_"+d.tableName, "("+d.metadata+")");
                                        d.reName=d.reName.replace("_"+d.tableName, "("+d.metadata+")");
                                        delete d.metadata;
                                    }
                                    if(tempObj[d.columnName]==undefined){
                                        tempObj[d.columnName]=d;
                                    }else{
                                        d.columnName=d.columnName+"("+d.tableName+")";
                                        d.reName=d.reName+"("+d.tableName+")";
                                    }
                                });
                                $scope.metadataObject['selectedMetadata']=$scope.selectedMetadataId;
                                $scope.metadataObject['type']="blending";
                                $scope.metadataObject['dataTypeCheck']=vm.dataTypeChangeCheck;
                                var data={
                                    "id": $scope.metadataId,
                                    "metadataObject":JSON.stringify($scope.metadataObject),
                                    "metadataName":$scope.metadataName,
                                };
                                dataFactory.request($rootScope.MetadataBlendingUpdate_Url,'post',data).then(function(response) {
                                    if(response.data.errorCode==1){
                                        dataFactory.successAlert("Metadata update successfully");
                                        var data={
                                            'metadataId':response.data.result
                                        };
                                        if(response.data.result){
                                            dataFactory.request($rootScope.BlendingSaveData_Url,'post',data).then(function(response) {
                                            });
                                        }
                                        $window.location = '#/metadata/blending/';
                                    }else{
                                        dataFactory.errorAlert("Check your connection");
                                    }
                                });
                            }else{
                                dataFactory.errorAlert("Enter metadata name");
                            }
                        }catch($e){
                            dataFactory.errorAlert("Check your connection");
                        }
                    }
                };
                vm.metadataBlendingObj.init();

                //Default popup
                vm.popup=function(table){
                    vm.metadataBlendingObj.popup(table);
                }

                /*
                 FILTER AND MEASURE DIMENSION OBJECT AND FUNCTION
                 */
                vm.metadataSetting={
                    init:function(){
                        $scope.moveToMdArray = {};
                        $scope.metadataObjColumnName={};
                    },
                    measureDimensionDropdown:function(id, index){
                        $("#" + id).addClass("overflowRemove");
                        if (id == "dimensionDiv"){
                            vm.metadataSetting.dropDownFixPosition($('.buttonDropdownDimension' + index), $('.custDropdownDimension'));
                        }else{
                            vm.metadataSetting.dropDownFixPosition($('.buttonDropdownMeasure' + index), $('.custDropdownMeasure'));
                        }
                    },
                    dropDownFixPosition:function(button, dropdown) {
                        var scrollPos = $(document).scrollTop();
                        var dropDownTop = button.offset().top - scrollPos + button.outerHeight() - 10;
                        dropdown.css('top', dropDownTop + "px");
                        dropdown.css('left', (button.offset().left) - 160 + "px");
                    },
                    deleteColumn:function(columnName) {
                        // filter the array
                        var foundItem = $filter('filter')(
                            $scope.keysArray, {
                                reName : columnName
                            }, true)[0];
                        // get the index
                        var index = $scope.keysArray.indexOf(foundItem);
                        $scope.keysArray.splice(index, 1);
                        $scope.dimensionMeasureRecall();
                        $scope.dataTableCall();
                    },

                    moveToDimMeasure:function(moveTo, columnName) {
                        var is = 0;
                        $scope.keysArray.filter(function(d) {
                            if (d.columnName == columnName) {
                                $scope.keysArray[is].dataKey = moveTo;
                                return true;
                            }
                            $scope.moveToMdArray[columnName] = moveTo
                            is++;
                            return false;
                        });
                        $scope.dimensionMeasureRecall();
                    },
                    renameKeys : function(name, index) {
                        $scope.lastRenameIndex = index;
                        var modalElem = $('#myModal');
                        $('#myModal').modal({
                            backdrop : 'static',
                            keyboard : false
                        });
                        $('#myModal').modal('show');
                        $scope.metadataObjColumnName={};
                        $scope.metadataObjColumnName.reNameInput = name;
                    },
                    saveRename:function(name) {
                        $('#myModal').modal('hide');
                        vm.tableColumn[$scope.lastRenameIndex]['reName'] = name.reNameInput;
                        // $scope.dimensionMeasureRecall();
                        $scope.metadataBlendingObj.dataTableCall();
                    },
                    addRow:function() {
                        $scope.addCount++;
                        $scope.noRow.push($scope.addCount);
                    },
                    removeRow:function(index) {
                        $scope.myForm[$scope.selectTable].node.splice(index, 1);
                        $scope.Form.node.splice(index, 1);
                        $scope.noRow.splice(index, 1);
                    },
                    insertFilterColumn:function(column){
                        if(typeof column === 'object')
                            $("#condition").insertAtCaret(column.dataType+"."+column.columnName);
                        else
                            $("#condition").insertAtCaret(column);
                    },
                    filter:function() {
                        var modalElem = $('#myModelFilter');
                        $('#myModelFilter').modal({
                            backdrop : 'static',
                            keyboard : false
                        });
                        $('#myModelFilter').modal('show');
                    },
                    filter:function() {
                        var modalElem = $('#myModelFilter');
                        $('#myModelFilter').modal({
                            backdrop : 'static',
                            keyboard : false
                        });
                        $('#myModelFilter').modal('show');
                    },
                    changeDataType:function(){
                        vm.excelTableColumnList=vm.multiSourceColumn[vm.excelSelectedDatabase].columns;
                        var modalElem = $('#dataTypeChange');
                        $('#dataTypeChange').modal({
                            backdrop : 'static',
                            keyboard : false
                        });
                        $("#tableJoin").modal('hide');
                        $('#dataTypeChange').modal('show');
                    },
                    dataTypeChange:function (tableName,columnObj,index,type) {
                        vm.excelTableColumnList[tableName][index].Type=type;
                        if(vm.dataTypeChangeObj==undefined){
                            vm.dataTypeChangeObj={};
                        }
                        if(vm.dataTypeChangeObj[tableName]==undefined){
                            vm.dataTypeChangeObj[tableName]=[];
                        }
                        if(vm.dataTypeChangeObj[tableName].indexOf(columnObj)==-1){
                            columnObj.Type=type;
                            vm.dataTypeChangeObj[tableName].push(columnObj);
                        }else{
                            var index =vm.dataTypeChangeObj[tableName].indexOf(columnObj);
                            vm.dataTypeChangeObj[tableName].splice(index,1);
                            columnObj.Type=type;
                            vm.dataTypeChangeObj[tableName].push(columnObj);
                        }
                    },
                    dataTypeChangeSave:function () {
                        if(vm.dataTypeChangeObj!=undefined){
                            var data={
                                "tableObj":vm.dataTypeChangeObj
                            };
                            vm.dataTypeChangeCheck=true;
                            dataFactory.request($rootScope.DataTypeUpdate_Url,'post',data).then(function(response) {
                                $scope.loadingBarList=false;
                                if(response.data.errorCode==1){
                                    dataFactory.successAlert("Datatype update successfully");
                                    $scope.tableList=JSON.parse(response.data.result);
                                }else{
                                    dataFactory.errorAlert(response.data.message);
                                }
                                $('#dataTypeChange').modal('hide');
                                $("#datasource").selectpicker();
                            });
                        }else{
                            dataFactory.errorAlert("You must to change one datatype");
                        }
                    }
                };
                //Inital metadata setting
                vm.metadataSetting.init();
            } ]);