/* Controllers */
/* ,'ui.layout' */
'use strict';
angular.module('app', ['ngSanitize'])
    .controller('metadataIncrementDataController', ['$scope', '$sce', 'dataFactory', '$rootScope', '$filter', '$q', '$stateParams', '$window', '$cookieStore', '$location', function ($scope, $sce, dataFactory, $rootScope, $filter, $q, $stateParams, $window, $cookieStore, $location) {
        // Multiselect query define
        //--- Top Menu Bar
        $("#chartjs-tooltip").hide();
        var data = {};
        var addClass = '';
        var viewClass = '';
        if ($location.path() == '/metadata/') {
            viewClass = 'activeli';
        } else {
            addClass = 'activeli';
        }
        data['navTitle'] = "METADATA";
        data['icon'] = "fa fa-database";
        data['navigation'] = [{
            "name": "Add",
            "url": "#/metadata/add",
            "class": addClass
        },
            {
                "name": "View",
                "url": "#/metadata/",
                "class": viewClass
            }];
        $rootScope.$broadcast('subNavBar', data);
        var userId = $cookieStore.get('userId');
        $scope.metadataId = $stateParams.data;
        $scope.tableNameArr = [];
        $scope.keyArr = [];
        $scope.setDataFlag = false;
        $scope.selected_TableName = '';
        $scope.selected_Key = '';
        $scope.selected_Update = '';
        $scope.incrementObj={};
        $scope.incrementObj.table={};
        var data = {
            "metadataId": $scope.metadataId
        };
        dataFactory.request($rootScope.MetadataGet_Url, 'post', data).then(function (response) {
            if (response.data.errorCode == 1) {
                $scope.metaDataObject = JSON.parse(response.data.result.metadataObject);
                $scope.connectionObject = $scope.metaDataObject.connectionObject;
                $.each($scope.metaDataObject.joinsDetails, function(key, val){
                    $scope.tableNameArr.push(key);
                });
                $scope.tableNameArr.push($scope.metaDataObject.rootTable);
                $scope.setData().then(function () {
                    setTimeout(function () {
                        if(response.data.result.incrementObj){
                            $scope.incrementObj=JSON.parse(response.data.result.incrementObj);
                        }
                        $scope.$apply();
                        $("#keyData").selectpicker();
                    },1000);
                });
            } else {
                dataFactory.errorAlert("Check your connection");
            }
        }).then(function(){

        });
        $scope.setData = function(){
            $scope.setDataFlag = true;
            var data = {
                "tableName": $scope.tableNameArr,
                "connObject": JSON.stringify($scope.connectionObject)
            };
            return dataFactory.request($rootScope.TableMultiColumns_Url, 'post', data).then(function (response) {
                if (response.data.errorCode == 1) {
                    $scope.keyArr = response.data.result;
                    $scope.tableWithKey=response.data.result;
                } else {
                    dataFactory.errorAlert(response.data.message);
                }
            }).then(function () {
                $scope.tableNameArr.forEach(function (d,index) {
                    if($scope.incrementObj.table[index]==undefined){
                        $scope.incrementObj.table[index]={};
                    }
                    $scope.incrementObj.table[index].name=d;
                });
            });
        }
        $scope.saveIncrementedData = function () {
            var data = {
                "metadataId": $scope.metadataId,
                "incrementObj":JSON.stringify($scope.incrementObj)
            };
            dataFactory.request($rootScope.incrementObjSaveList_Url,'post',data).then(function(response){
                if(response.data.errorCode){
                    $window.location = '#/metadata/';
                }
            });
        }
}]);
