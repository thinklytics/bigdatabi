'use strict';
angular.module('app',['ngSanitize'])
    .controller('metadataBlendingListController', ['$scope','$sce','dataFactory','$rootScope','$filter','$state','$window','$cookieStore','$location',function($scope,$sce,dataFactory,$rootScope,$filter,$state,$window,$cookieStore,$location) {
        //--- Top Menu Bar
        var vm=$scope;
        var viewClass="";
        var addClass="";
        var data={};
        if($location.path()=='/metadata/'){
            viewClass='activeli';
        }else{
            addClass='activeli';
        }
        data['navTitle']="DATA BLENDING";
        data['icon']="fa fa-database";
        data['navigation']=[{
            "name":"Add",
            "url":"#/metadata/add",
            "class":addClass
        },
            {
                "name":"View",
                "url":"#/metadata/",
                "class":viewClass
            }];
        $rootScope.$broadcast('subNavBar', data);
        //End Menu
        //Datasource
        $scope.source={};
        $scope.trustAsHtml = function(value) {
            return $sce.trustAsHtml(value);
        };
        /*
         * Datasource list
         */
        /*dataFactory.request($rootScope.datasourceList_Url,'post',"").then(function(response) {
            $scope.loadingBarList=false;
            if(response.data.errorCode==1){
                $scope.sourcedata=response.data.result;
            }else{
                dataFactory.errorAlert(response.data.message);
            }

        }).then(function(){
            setTimeout(function(){
                $('[rel="tooltip"]').tooltip();
            },1000);

        });*/
        $scope.selectedConfirm=0;
        $scope.toggle=function(e,source,index){
            setTimeout(function(){
                $(function () {
                    $('[data-toggle="tooltip"]').tooltip();
                })
            },100);
            $('.imageToggle').removeClass('selectedImage');
            source['index']=index;
            $scope.selectedMetadata=source;
            $(e.currentTarget).addClass('selectedImage');
            $scope.selectedConfirm=1;
        }
        // Datasource Name,image and port
        $scope.datasource = [ {
            "name" : "MySQL",
            "image" : "theme/assets/img/MySQL-Logo.png",
            "port" : 3306
        },{
            "name" : "Mongodb",
            "image" : "theme/assets/img/mongodb.png",
            "port" : 3306
        },{
            "name" : "MsSQL",
            "image" : "theme/assets/img/mssql-server.png",
            "port" : 3306
        },{
            "name" : "Cassandra",
            "image" : "theme/assets/img/cassandra-logo.png",
            "port" : 3306
        },{
            "name" : "Excel",
            "image" : "theme/assets/img/excel-logo.png",
            "port" : 3306
        },{
            "name" : "AWS RDS",
            "image" : "theme/assets/img/aws-rds-logo.png",
            "port" : 3306
        },{
            "name" : "Oracle",
            "image" : "theme/assets/img/oracle-logo.png",
            "port" : 3306
        },{
            "name" : "Sybase",
            "image" : "theme/assets/img/sybase-logo.png",
            "port" : 3306
        }];
        $scope.getDatasourceIconPath=function(datasourceType){
            // var path="theme/assets/img/MySQL-Logo.png";
            var path="theme/assets/img/Blending.png";
            try{
                $scope.datasource.forEach(function(d){
                    if(datasourceType.toLowerCase()==d.name.toLowerCase()){
                        path=d.image;
                    }
                });
            }catch(e){

            }
            return path;
        }

        /*
         * Metadata List
         */


        $scope.metadataBlendingListApi = function(){
            dataFactory.request($rootScope.MetadataBlendingList_Url,'post',"").then(function(response){
                if(response.data.errorCode==1){
                    $scope.groupFolder = Object.keys(response.data.result);
                    $scope.metaList = response.data.result;
                    console.log(vm.groupFolder)
                    console.log(vm.metaList)
                }else{
                    dataFactory.errorAlert("Check your connection");
                }
            }).then(function(){
                $scope.loading=true;
                if($scope.loading){
                    $(".loaderImage").fadeOut("slow");
                }
                setTimeout(function(){
                    $('[rel="tooltip"]').tooltip();
                },100);
            });
        }
        $scope.metadataBlendingListApi();



        $scope.metaEdit=function(){
            $state.go("metadata.blendingEdit", { 'id': $scope.selectedMetadata.metadata_id });
        }



// Metadata Delete
        $scope.showSwal = function(type){
            if(type == 'warning-message-and-cancel') {
                swal({
                    title: 'Are you sure?',
                    text: 'You want to delete MetaData !',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: 'Yes',
                    cancelButtonText: 'No',
                    confirmButtonClass: "btn btn-success",
                    cancelButtonClass: "btn btn-danger",
                    buttonsStyling: false,
                }).then(function(value){
                    if(value){
                        $scope.metadataDelete();
                    }
                },
                function (dismiss) {

                })
            }
        }
        $scope.metadataDelete=function () {
            var index;
            var id=$scope.selectedMetadata.metadata_id;
            $scope.metadataList.forEach(function (d,i) {
                if(d.metadata_id==$scope.selectedMetadata.metadata_id){
                    index=i;
                }
            });
            var data={
                "id":id
            };
            dataFactory.request($rootScope.MetadataDelete_Url,'post',data).then(function(response){
                if(response.data.errorCode==1){
                    $scope.metadataList.splice(index,1);
                    dataFactory.successAlert("Metadata delete successfully");
                }else{
                    dataFactory.errorAlert("Check your connection");
                }
            });
        }
// Metadata Delete



        $scope.syncData=function(){
            var id=$scope.selectedMetadata.metadata_id;
            var data={
                "id":id
            };
            dataFactory.request($rootScope.MetadataBlendingDataSync_Url,'post',data).then(function(response){
                if(response.data.errorCode==1){
                    var data = {
                        "id": id
                    };
                    dataFactory.request($rootScope.clearCache_Url, 'post', data);
                    dataFactory.successAlert("Metadata Sync Successfully");
                }else{
                    dataFactory.errorAlert("Check your connection");
                }
            });
        }


// sort metadata blend list asc-desc Start
        $scope.metadataBlendList_Order = 'desc';
        $scope.metadataBlend_List_order = '-id';
        $scope.orderMeataDataBlendList = function(){
            if($scope.metadataBlendList_Order == 'desc'){
                $scope.metadataBlend_List_order = '-id';
            }
            else if($scope.metadataBlendList_Order == 'asc'){
                $scope.metadataBlend_List_order = 'id';
            }
        }
// sort metadata blend list asc-desc End



        $scope.metadataFolder = {
            toggleImage: function (e, sView, index) {
                setTimeout(function(){
                    $(function () {
                        $('[data-toggle="tooltip"]').tooltip();
                    })
                },100);
                $('.imageToggle').removeClass('selectedImage');
                $scope.loadingBarDrawer = true;
                $scope.selectedSharedViewName = sView;
                $('.stepFolder').slideUp('fast');
                $('.stepBack').slideUp('fast');
                $scope.metadataList = $scope.metaList[$scope.selectedSharedViewName];
                setTimeout(function () {
                    $scope.loadingBarDrawer = false;
                    $('.stepFolder1').slideUp('fast');
                    $('.stepLoading').slideUp('fast');
                    $('.stepFolder2').slideDown('fast');
                    $('.stepBack').slideDown('fast');
                }, 300);
                $scope.backBtn = 1;
            },
            backToSharedView: function () {
                $scope.search = '';
                $('.stepFolder1').slideDown('fast');
                $('.stepFolder').slideDown('fast');
                $('.stepFolder2').slideUp('fast');
                $scope.selectedConfirm = 0;
                $scope.backBtn = 0;
            }
        };
        $scope.import=0;
        $scope.importBtn=function () {
            $scope.import=1;
            $('.step1').slideUp('fast');
            $('.step2').slideDown('fast');
            $scope.testsuccess=false;
            $scope.datasourceStatus=0;
            $scope.metadataStatus=0;
        }
        $scope.backToFolder=function () {
            $scope.import=0;
            $('.step2').slideUp('fast');
            $('.step1').slideDown('fast');
        }
        $scope.testImport=function (datasourceImport,type) {
            $(".loaderImage").fadeIn("slow");
            if($scope.metadataImport==undefined || $scope.metadataImport['file']==undefined){
                dataFactory.errorAlert("File is required");
                return false;
            }
            var file=$scope.metadataImport['file'];
            var allowedFiles=["think"];
            var file=$scope.metadataImport.file;
            var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:])+(" + allowedFiles.join('|') + ")$");
            if (!regex.test(file.name.toLowerCase())) {
                dataFactory.errorAlert("Please upload files having extensions: <b>" + allowedFiles.join(', ') + "</b> only.");
                return false;
            }
            $scope.testBtn="Loading...";
            var url=$rootScope.MetadataImport_Url;
            dataFactory.importData(url,file,type).then(function(response) {
                if(response.data.errorCode){
                    $(".loadingBar").hide();
                    $scope.Attributes=response.data.result;
                    dataFactory.errorAlert(response.data.message);
                    $scope.importSave=1;
                    $scope.testsuccess=true;
                    $scope.datasourceStatus=response.data.result.datasource;
                    $scope.metadataStatus=response.data.result.metadata;
                    $scope.datasourceOverwrite=true;
                    $scope.metadataOverwrite=true;
                }else{
                    dataFactory.errorAlert(response.data.message);
                }
                setTimeout(function(){
                    $scope.loading=false;
                    if(!$scope.loading){
                        $(".loaderImage").fadeOut("slow");
                    }
                },1000);
            });
        }
        $scope.checkView=function(page){
            $scope.publicViewType=page;
            if(page=='public_group'){
                $(".subDiv").hide();
                $(".footerDiv").show();
            }
            else{
                $(".subDiv").show();
                $(".footerDiv").hide();
            }
        }
        $scope.publicViewType="specific_group";
        $scope.reportGrp={};
        dataFactory.request($rootScope.UsergroupList_Url,'post',"").then(function(response){
            $scope.userGroup=response.data.result;
        });
        dataFactory.request($rootScope.ReportGroupList_Url,'post',"").then(function(response){
            $scope.reportGroup=response.data.result;
        });
        $scope.selectGroup=function(divClass){
            if(divClass=='back'){
                $(".mainDiv").slideDown('fast');
                $(".select").slideUp('fast');
                $(".create").slideUp('fast');
                $(".footerDiv").slideUp('fast');
            }else{
                $("."+divClass).slideDown('fast');
                $(".footerDiv").slideDown('fast');
                $(".mainDiv").slideUp('fast');
            }
            if(divClass=='select' || divClass=='create'){
                $scope.reportGrp={};
            }
        }
        $scope.groupTypeModal=function(){
            $('#reportGroup').modal('show');
        }
        $scope.overwrite={};
        $scope.overwrite.datasource=1;
        $scope.overwrite.metadata=1;
        $scope.groupTypeSave=function () {
            $(".loaderImage").show();
            if($scope.metadataImport['file']==undefined){
                $scope.addClassRemove("File is required");
                return false;
            }
            var file=$scope.metadataImport['file'];
            var allowedFiles=["think"];
            var file=$scope.metadataImport.file;
            var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:])+(" + allowedFiles.join('|') + ")$");
            if (!regex.test(file.name.toLowerCase())) {
                $scope.addClassRemove("Please upload files having extensions: <b>" + allowedFiles.join(', ') + "</b> only.");
                return false;
            }
            var data={
                "overwrite":$scope.overwrite,
                "blending" : true
            };
            var url=$rootScope.MetadataImportSave_Url;
            dataFactory.importDataSave(url,file,'',$scope.reportGrp,$scope.publicViewType,JSON.stringify(data)).then(function(response) {
                if(response.data.errorCode){
                    $(".loaderImage").hide();
                    $scope.backToFolder();
                    dataFactory.successAlert(response.data.message);
                    $scope.metadataBlendingListApi();
                }else{
                    dataFactory.errorAlert(response.data.message);
                }
            });
            $('#reportGroup').modal('hide');
        }
        /*
                  * Export
                  */
        /*
         * Export and import
         */
        $scope.export=function () {
            var data={
                "metadataId":$scope.selectedMetadata.metadata_id
            };
            dataFactory.request($rootScope.MetadataExport_Url,'post',data).then(function(response) {
                if(response.data.errorCode){
                    var link=document.createElement('a');
                    link.href=response.data.result.url;
                    link.download=response.data.result.fileName;
                    link.click();
                    var data={
                        "url":response.data.result.url
                    };
                    dataFactory.request($rootScope.ExportFileDelete_Url,'post',data);
                }
            })
        }


//reName Column Start
        vm.nameTypeMeasure = 'database_measure';
        vm.renameColumn = function () {
            $('#blendingRename').modal('show');
            vm.tableColumn = JSON.parse(vm.selectedMetadata.metadataObject).column;
        }

        vm.EditKeyIndex = false;
        vm.editColumnName = function(index, key){
            vm.tabKeyName = key;
            vm.EditKeyName = true;
            vm.EditKeyIndex = index;
        }

        vm.saveColumnName = function(index, key){
            var newTabName = $('#tabEditName_' + index).val();
            if(newTabName.length){
                var flag = false;
                for(var i=0; i<vm.tableColumn.length; i++){
                    if(vm.tableColumn[i].reName == newTabName){
                        flag = true;
                        break;
                    }
                }
                if(flag){
                    dataFactory.errorAlert("Name already present");
                    $('#blendingRename').modal('hide');
                    return;
                }
                vm.tableColumn.forEach(function(d,i){
                    if(index == i){
                        d.reName = newTabName;
                    }
                });
            }
            vm.EditKeyName = false;
        }


        vm.saveBlendingRename = function () {
            $('#blendingRename').modal('hide');
            var temp = JSON.parse(vm.selectedMetadata.metadataObject);
            temp.column = vm.tableColumn;
            var selectedIndex;
            vm.metadataList.forEach(function (d,index) {
                if(vm.selectedMetadata.metadata_id==d.metadata_id){
                    selectedIndex=index;
                }
            });
            var data = {
                "metadataObject":JSON.stringify(temp),
                "metadataId":vm.selectedMetadata.metadata_id,
            };

            dataFactory.request($rootScope.MetadataBlending_Rename,'post',data).then(function(response){
                if(response.data.errorCode==1){
                    dataFactory.successAlert(response.data.message);
                    vm.metadataList[selectedIndex].metadataObject=JSON.stringify(temp);
                }else{
                    dataFactory.errorAlert("Check your connection");
                }
            });
        }
        
//reName Column End












    }]);
