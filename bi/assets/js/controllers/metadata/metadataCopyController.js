/* Controllers */
/* ,'ui.layout' */
'use strict';
angular.module('app', ['ngSanitize'])
    .controller('metadataCopyController', ['$scope', '$sce', 'dataFactory', '$rootScope', '$filter', '$q', '$stateParams', '$window', '$cookieStore', '$location', function ($scope, $sce, dataFactory, $rootScope, $filter, $q, $stateParams, $window, $cookieStore, $location) {
        // Multiselect query define
        //--- Top Menu Bar
        $("#chartjs-tooltip").hide();
        var data = {};
        var addClass = '';
        var viewClass = '';
        var vm = $scope;
        if ($location.path() == '/metadata/') {
            viewClass = 'activeli';
        } else {
            addClass = 'activeli';
        }
        data['navTitle'] = "METADATA";
        data['icon'] = "fa fa-database";
        data['navigation'] = [{
            "name": "Add",
            "url": "#/metadata/add",
            "class": addClass
        },
            {
                "name": "View",
                "url": "#/metadata/",
                "class": viewClass
            }];
        $rootScope.$broadcast('subNavBar', data);
        //End Menu
        //Default tab define
        $scope.tab = false;
        //End tab
        $scope.metadataSaveShow = 0;
        $scope.updateBtnOnLoad = false;
        // Div Height define
        $("#custHeightFirstdiv").height(
            $(window).height() / 2 - 68);
        // $("#custHeightSeconddiv").height($(window).height()/2);
        // End
        $scope.Attributes = {};
        $scope.trustAsHtml = function (value) {
            return $sce.trustAsHtml(value);
        };
        var userId = $cookieStore.get('userId');

        $scope.Form = {};
        $scope.Form.node = [];
        $scope.metadataInfo1 = false;
        $scope.metadataNamePlaceholder = "Metadata Name....";
        $scope.nameTypeDimension = 'database_dimension';
        $scope.nameTypeMeasure = 'database_measure';
        $scope.popOverSave = function (formObj) {
            $scope.updateData = 1;
            $("#dataTableDiv").empty();
            $scope.updateBtnOnLoad = true;
            formObj['join'] = $scope.selectjoin;
            formObj['rightColumnTable'] = $scope.selectTable;
            var rootTable = $scope.selectedTableName[0];
            formObj["root"] = $scope.selectedTableName[0];
            formObj["row"] = $scope.noRow;
            var myformObj = {};
            myformObj = angular.copy(formObj);
            $scope.myForm[$scope.selectTable] = myformObj;
            $scope.joinGetDetails[$scope.selectTable]= myformObj;
            layer.children.forEach(function (d) {
                d.children.forEach(function (p, index) {
                    if (p.className == "Image" && p.attrs.id == formObj.rightColumnTable) {
                        if ($scope.selectjoin == "Inner") {
                            p.attrs.image.src = "assets/img/joins-images/join_inner.svg";
                            p.attrs.image.srcset = "assets/img/joins-images/join_inner.svg";
                        } else if ($scope.selectjoin == "Left") {
                            p.attrs.image.src = "assets/img/joins-images/join_left.svg";
                            p.attrs.image.srcset = "assets/img/joins-images/join_left.svg";
                        } else if ($scope.selectjoin == "Right") {
                            p.attrs.image.src = "assets/img/joins-images/join_right.svg";
                            p.attrs.image.srcset = "assets/img/joins-images/join_right.svg";
                        } else if ($scope.selectjoin == "Outer") {
                            p.attrs.image.src = "assets/img/joins-images/join_outer.svg";
                            p.attrs.image.srcset = "assets/img/joins-images/join_outer.svg";
                        }
                        layer.draw();
                    }
                });
            });
            $scope.closePopup();
            layer.draw();
            $("#tableJoin").modal('hide');
        }

        $scope.showCount = 0;
        $scope.closePopup = function () {
            $scope.joinConfiguration = false;
        }
        $scope.joinType = function (type) {
            $scope.selectjoin = type;
            $scope.joinSelected = true;
            $(".joinHover").removeClass('joinActive');
            $("."+type).addClass('joinActive');
        }
        // Select datasource
        $scope.tab = 'home';
        var promise;
        $scope.indexingBtn=false;
        $scope.sourceSelect = function (sourceoption) {
            var sourceoptionObj=JSON.parse(sourceoption);
            if(sourceoptionObj.Attributes!=undefined && sourceoptionObj.Attributes!="" && sourceoptionObj.datasourceType=="excel"){
                $scope.indexingBtn=true;
            }else{
                $scope.indexingBtn=false;
            }
            $scope.dropTableList = [];
            $scope.dropDetails = {};
            $scope.source = {};
            $("#dataTableDiv").html("");
            //$(".konvajs-content").html("");
            $scope.selectedTableName = [];
            $scope.Attributes.checkboxModelDimension = {};
            layer.destroy();
            i = 0;
            // Konvajs redefine
            if (stage.children[0] != undefined) {

            }
            // ENd
            if(sourceoptionObj.datasourceType=="mongodb"){
                $(".Left").hide();
                $(".Right").hide();
                $(".Outer").hide();
            }else{
                $(".Left").show();
                $(".Right").show();
                $(".Outer").show();
            }
            $scope.loadingBarDrawer = true;
            $scope.metaListData = false;
            $scope.drawerText = true;
            return promise = new Promise(
                function (resolve, reject) {
                    var data = {
                        "sourceObject": sourceoption
                    }
                    dataFactory.request($rootScope.TableList_Url, 'post', data).then(function (response) {
                        if (response.data.errorCode == 1) {
                            var resp = response.data.result;
                            $scope.tableList = [];
                            resp.forEach(function (d) {
                                $scope.tableList.push(d);
                            });
                        } else {
                            dataFactory.errorAlert(response.data.message);
                        }
                        $scope.metadataInfo1 = true;
                    }).then(function () {
                        $scope.loadingBarDrawer = false;
                        resolve();
                    });
                    $scope.metaListData = true;
                });
        }
        //Where condition
        $scope.whereCondition = "";
        $scope.whereConditionText = "";
        $scope.insertFilterColumn = function (column) {
            if (typeof column === 'object')
                $("#condition").insertAtCaret(column.columnName);
            else
                $("#condition").insertAtCaret(column);
        }
        $scope.limit = "";
        $scope.limitInput = "";
        $scope.limitCheck = false;
        $scope.saveFilter = function (condition, limitInput) {
            $scope.whereCondition = condition;
            $scope.whereConditionText = condition;
            $scope.limit = limitInput;
            $('#myModelFilter').modal('hide');
            $("#dataTableDiv").empty();
            $scope.updateBtnOnLoad = true;
        }
        $scope.filter = function () {
            var modalElem = $('#myModelFilter');
            $('#myModelFilter').modal({
                backdrop: 'static',
                keyboard: false
            });
            $('#myModelFilter').modal('show');
        }

        // Update btn true
        // $scope.updateBtnOnLoad=true;
        // Get data from query and create table
        $scope.updateData = 0;
        $scope.getData = function (conId, type) {
            $scope.updateBtnOnLoad = false;
            $scope.loadingBar = true;
            $scope.metadataObject = {};
            $scope.metadataObject['connectionObject'] = JSON.parse(conId);
            $scope.metadataObject['joinsDetails'] = $scope.myForm;
            $scope.metadataObject['rootTable'] = $scope.selectedTableName[0];
            $scope.metadataObject['limit'] = $scope.limit;
            $scope.metadataObject['limitCheck'] = $scope.limitCheck;
            $scope.metadataObject['condition'] = $scope.whereCondition;
            var data = {
                "matadataObject": JSON.stringify($scope.metadataObject)
            }
            if($scope.tableColumns!=undefined){
                vm.default_TableColumns = $scope.tableColumns;
            }
            dataFactory.request($rootScope.MetadataGetData_Url, 'post', data).then(function (response) {
                if (response.data.errorCode == 1) {
                    var response = response.data.result;
                    var keysArray = [];
                    var k = 0;
                    if ($scope.updateData == 0) {
                        $scope.keysArray = $scope.tableColumns;
                        $scope.updateData=1;

                    } else {
                        $.each(JSON.parse(response.tableColumn), function (key, d) {
                            keysArray[k] = {
                                "columType": d.Type,
                                "tableName": d.tableName,
                                "columnName": d.Field,
                                "reName": d.Field,
                                "dataKey": d.dataType,
                                "type": "defined"
                            };
                            k++;
                        });
                        $scope.keysArray = keysArray;
                    }
                    $scope.tableData = JSON.parse(response.tableData);
                    $scope.tableColumns = $scope.keysArray;
                    $scope.metaDataObject.column = $scope.tableColumns;
                    $scope.dimensionMeasureRecall();

                    if(type == 'default'){
                        vm.default_TableColumns = $scope.tableColumns;
                    }else if(type == 'update' || type == 'refresh' ){
                        var columnTempObj={};
                        vm.default_TableColumns.forEach(function (d) {
                            columnTempObj[d.columnName]=d;
                        });
                        vm.tableColumns.forEach(function(d,index){
                            if(columnTempObj[d.columnName] && columnTempObj[d.columnName].reName != d.reName){
                                vm.tableColumns[index]=columnTempObj[d.columnName];
                            }
                        });

                    }
                    // $scope.tableName =
                    // mytable;
                    $scope.loading = false;
                    // $scope.tabledata = true;
                    $scope.savequery = true;
                    $scope.Dttable = true;
                    $scope.dataTableCall();
                    $scope.filterCheck = true;
                } else {
                    dataFactory.errorAlert(response.data.message);
                }
            }).then(function () {
                $scope.loadingBar = false;
                // Meta data Object
                $scope.metadataObject['column'] = $scope.keysArray;
                $scope.metadataSaveShow = 1;
            });
        }
        // Datatable Call
        $scope.dataTableCall = function () {
            var WholeKeys = [];
            var WholeKeysObjectVal = $scope.WholeKeysObjectValForTable
            var tableHeaders = "";
            var i = 0;
            var k = 0;
            $scope.sumLengthCOlumn = 0;
            $scope.columnObjectTable = [];
            var Keys = $scope.KeysForTable;
            for (var t = 0; t < $scope.keysArray.length; t++) {
                // WholeKeys =
                // Object.keys($scope.columnType[Keys[t]]);
                // var WholeKeysObj =
                // $scope.columnType[Keys[t]];
                var tableArray = "";
                // $scope.columnObjectTable.push(WholeKeysObj['Column_tableName']+"."+WholeKeysObj['Column_name']);
                tableHeaders += "<th>"
                    + '<a href="javascript:void(0)" data-ng-click="test()" style="float:right" class="toggle-vis" data-column="'
                    + k
                    + '"><i class="icon mdi mdi-delete" ></i></a>'
                    + $scope.keysArray[t].columType
                    + "<br>"
                    + $scope.keysArray[t].tableName
                    + "<br>"
                    + $scope.keysArray[t].reName
                    + "</th>";
                /*
                 * tableArray += '["' +
                 * WholeKeysObj[$scope.keysArray[t].columType] + ' ' +
                 * WholeKeysObj[$scope.keysArray[t].tableName] + ' ' +
                 * WholeKeysObj[$scope.keysArray[t].columnName] +
                 * '"],';
                 */
                k++;
                $scope.sumLengthCOlumn++;

            }

            var tableData = "";
            var tableData1 = "";
            var tableBody1 = "";
            for (var s = 0; s < $scope.tableData.length; s++) {
                var tableBody = "";

                for (var ni = 0; ni < $scope.tableColumns.length; ni++) {

                    tableData += '"' + ni + '",';
                    tableBody += "<td>"
                        + $scope.tableData[s][$scope.tableColumns[ni].columnName]
                        + "</td>";
                }
                tableData1 += "[" + // tableData.substring(0,tableData.length
                    // - 1)
                    +"],";
                tableBody1 += "<tr>" + tableBody + "</tr>";
            }
            $("#dataTableDiv").empty();
            $("#dataTableDiv")
                .append(
                    '<table id="displayTable" class="table table-bordered bordered table-striped table-condensed datatable"><thead><tr>'
                    + tableHeaders
                    + '</tr></thead><tbody>'
                    + tableBody1
                    + '</tbody></table>');

            $('.toggle-vis').on(
                'click',
                function (e) {
                    e.preventDefault();
                    var column = table.column($(this)
                        .attr('data-column'));
                    $scope.columnObjectTable.splice(
                        column[0][0], 1);
                    column.visible(!column.visible());
                });
            //If no column no data
            if($scope.keysArray.length==0){
                $("#dataTableDiv").append("<h4>No Data Available</h4>");
            }else{
                var table = $('#displayTable').DataTable({
                    "scrollY": 300,
                    "scrollX": true,
                    "bFilter": false,
                    "bSearchable": false,
                    /*
                     * "pageLength": 1000,
                     */
                    "bInfo": false,
                    "bLengthChange": false,
                    "paging": false,
                    "ordering": false
                });
            }
            if ($scope.lastColumnShow) {
                var $scrollBody = $(table.table().node())
                    .parent();
                $scrollBody
                    .scrollLeft($scrollBody.get(0).scrollWidth);
                $(
                    '#displayTable td:nth-child('
                    + $scope.sumLengthCOlumn
                    + ')').css(
                    'background-color', '#CCC');
                setTimeout(
                    function () {
                        $(
                            '#displayTable td:nth-child('
                            + $scope.sumLengthCOlumn
                            + ')')
                            .animate(
                                {
                                    backgroundColor: '#f5f5f5'
                                }, 'slow');
                    }, 2000);
            }

            $scope.lastColumnShow = false;

        }
        /*// save Calculation field
         $scope.fieldName = "";
         $scope.saveCalculationField = function(formula,
         name) {
         if (formula != "" && name != "") {
         var expression;
         var errorMessage;
         // Check express for bracketFp
         function update() {
         expression = true;
         try {
         balanced.matches({
         source : formula,
         open : [ '{', '(', '[' ],
         close : [ '}', ')', ']' ],
         balance : true,
         exceptions : true
         });
         } catch (error) {
         expression = false;
         errorMessage = error.message;
         }
         }
         update();
         if (expression) {
         var concatVar = "";// For
         // Concat
         // function
         // parseInt
         // (1.6);
         // ISNULL()
         var formulaVal = formula.replace(
         "round", "Math.round").replace(
         "isNull", "isEmpty").replace(
         "isNotNull", "isEmpty")
         .replace("floor", "Math.floor")
         .replace("decimalToInt",
         "parseInt").replace(
         "concat",
         "concatVar.concat")
         .replace(/\{/g,
         "parseFloat(d['")
         .replace(/\}/g, "'])");
         var errorMessage = false;
         $scope.tableData.forEach(function(d) {
         // Check express for
         // data
         try {
         var result = eval(formulaVal);
         } catch (error) {
         errorMessage = true;
         errorMessage = error.message;
         }
         d[name] = result;
         });
         function isEmpty(val) {
         return (val === undefined
         || val == null || val.length <= 0) ? true
         : false;
         }
         function replace(expression, substring,
         replacement) {
         return expression.replace(
         substring, replacement);
         }
         function strlen(expression) {
         return expression.length;
         }
         function toLower(expression) {
         return expression.toLowerCase();
         }
         function toString(expression) {
         return expression.toString();
         }
         function trim(expression) {
         return expression.toString();
         }
         function dateDiff(getDate1, getDate2) {
         var date1 = new Date(getDate1);
         var date2 = new Date(getDate2);
         var timeDiff = Math.abs(date2
         .getTime()
         - date1.getTime());
         var diffDays = Math.ceil(timeDiff
         / (1000 * 3600 * 24));
         return diffDays;
         }
         function ltrim(expression) {
         var trimmed = expression.replace(
         /^\s+/g, '');
         return trimmed;
         }
         ;
         function rtrim(expression) {
         var trimmed = expression.replace(
         /\s+$/g, '');
         return trimmed;
         }
         ;
         if (errorMessage == false) {

         $scope.tableColumns.push({
         "columType" : "Long",
         "columnName" : name,
         "dataKey" : "Measure",
         "reName" : name,
         "tableName" : "",
         "formula" : formula,
         "type" : "custom"
         });
         $scope.lastColumnShow = true;
         $("#calculationMeasure").modal(
         'hide');
         $scope.dataTableCall();
         } else {
         $scope.errorCalculation = errorMessage;

         }
         } else {
         $scope.errorCalculation = errorMessage;
         }
         } else if (formula == "") {
         $scope.errorCalculation = "Please select expressions";

         } else if (name == "") {
         $scope.errorCalculation = "Please Enter field name";
         }
         $(".reportDashboard").removeClass(
         "background-container");
         }*/
        // Collepsion
        $scope.showResults = function (index) {
            if ($scope['showResults' + index] == undefined) {
                $scope['showResults' + index] = true;
            } else if ($scope['showResults' + index] == true) {
                $scope['showResults' + index] = false;
            } else {
                $scope['showResults' + index] = true;
            }
        }
        // Sub category
        $scope.selectedGroup = {};
        var custIndex = 0;
        $scope.subCategory = {};
        $scope.addSubCategory = function () {
            if ($scope.selectedValue.length) {
                $(".ms-selected").hide();
                if ($scope.subCategory[custIndex] == undefined) {
                    $scope.subCategory[custIndex] = 'group'
                        + custIndex;
                }
                $scope.selectedGroup['group' + custIndex] = $scope.selectedValue;
                $scope.selectedValue = [];
                custIndex++;
            } else {
                $scope.checkError = 0;
                $scope.categoryError = "Select a value";
            }

        }
        // Save
        $scope.categoryError = "";// For Error
        $scope.categoryGroupObject = {};
        $scope.createCateogrySave = function (categoryData,
                                              categoryName, subCategory) {
            var j = 0;
            if (categoryName) {
                Object
                    .keys(categoryData)
                    .forEach(
                        function (r) {
                            if (categoryData[$scope.subCategory[j]] != categoryData[r]) {
                                categoryData[$scope.subCategory[j]] = categoryData[r];
                                delete categoryData[r];
                            }
                            j++;
                        });
                var lengthCheck = 0;
                $scope.tableData
                    .forEach(function (d) {
                        var keysVal = "";
                        Object
                            .keys(categoryData)
                            .forEach(
                                function (r) {
                                    var index = categoryData[r]
                                        .indexOf(d[$scope.lastCategoryTableColumn]);
                                    if (index != -1) {
                                        keysVal = r;
                                    }
                                });
                        if (keysVal == "") {
                            keysVal = d[$scope.lastCategoryTableColumn];
                        }
                        d[categoryName] = keysVal;
                        lengthCheck++;
                        if (lengthCheck == $scope.tableData.length) {
                            $scope.lastColumnShow = true;
                            $scope.keysArray
                                .push({
                                    "columType": "Long",
                                    "columnName": categoryName,
                                    "dataKey": "Dimension",
                                    "reName": categoryName,
                                    "tableName": "",
                                    "type": "custom",
                                    "id": j
                                });
                            $scope.categoryGroupObject[j] = {};
                            $scope.categoryGroupObject[j]["columnName"] = $scope.lastCategoryTableColumn,
                                $scope.categoryGroupObject[j]["categoryName"] = categoryName,
                                $scope.categoryGroupObject[j]["groupData"] = categoryData
                            $scope.dataTableCall();
                            $("#category").modal('hide');
                        }
                        $scope.selectedGroup = {};
                    });
            } else if (categoryData.length) {
                $scope.checkError = 0;
                $scope.categoryError = "Make sub category";
            } else {
                $scope.checkError = 0;
                $scope.categoryError = "Enter Category Name";
            }

        }
        // Fixed dropdown

        $scope.measureDimensionDropdown = function (id, index) {
            $("#" + id).addClass("overflowRemove");
            if (id == "dimensionDiv") {
                dropDownFixPosition($('.buttonDropdownDimension' + index), $('.custDropdownDimension'));
            } else {
                dropDownFixPosition($('.buttonDropdownMeasure' + index), $('.custDropdownMeasure'));
            }
        }

        function dropDownFixPosition(button, dropdown) {
            var scrollPos = $(document).scrollTop();
            var dropDownTop = button.offset().top - scrollPos + button.outerHeight();
            dropdown.css('top', dropDownTop + "px");
            dropdown.css('left', (button.offset().left) - 160 + "px");
        }


        // Create Category
        $scope.createCategory = function (columnObject) {
            var connectionObject = $scope.source.sourceoption;
            $scope.lastCategoryTableColumn = columnObject.columnName;
            if ($scope.categorySelectColumnData != undefined) {
                $("#my-select").multiSelect("destroy");
            }
            $scope.categorySelectColumnData = [];
            dataFactory
                .categoryColumnData(
                    connectionObject,
                    columnObject)
                .then(function (response) {
                    var itemsProcessed = 0;
                    response.data.result
                        .forEach(function (d) {
                            if (d[$scope.lastCategoryTableColumn]) {
                                $scope.categorySelectColumnData
                                    .push(d[$scope.lastCategoryTableColumn]);
                            }
                            itemsProcessed++;
                            if (itemsProcessed === response.data.result.length) {
                                callBack();
                            }
                        });
                });
            // Close Error
            $scope.notificationClose = function () {
                $scope.checkError = 1;
            }
            var callBack = function () {
                setTimeout(
                    function () {
                        $scope.selectedValue = [];
                        $('#my-select')
                            .multiSelect(
                                {
                                    afterSelect: function (values) {
                                        $scope.selectedValue
                                            .push(values[0]);
                                        $scope.checkError = 1;
                                    },
                                    afterDeselect: function (values) {
                                        var index = $scope.selectedValue
                                            .indexOf(values[0]);
                                        if (index != -1) {
                                            $scope.selectedValue
                                                .splice(
                                                    index,
                                                    1);
                                        }
                                        $scope.checkError = 1;
                                    }
                                });

                    }, 1);
            }
            $('#category').modal({
                backdrop: 'static',
                keyboard: false
            });

        }
        // Delete dimension and measures
        $scope.deleteColumn = function (columnName) {
            // filter the array
            var foundItem = $filter('filter')(
                $scope.keysArray, {
                    reName: columnName
                }, true)[0];
            // get the index
            var index = $scope.keysArray.indexOf(foundItem);
            $scope.keysArray.splice(index, 1);
            $scope.dimensionMeasureRecall();
            $scope.dataTableCall();
        }
        // DImension Measure Call
        $scope.dimensionMeasureRecall = function () {
            // Measure
            $scope.columnTypekeysArrayMeasure = $scope.keysArray
                .filter(function (d) {
                    if (d.dataKey == "Measure") {
                        return true;
                    }
                    return false;
                });

            // Dimension
            $scope.columnTypekeysArrayDimension = $scope.keysArray
                .filter(function (d) {
                    if (d.dataKey == "Dimension") {
                        return true;
                    }
                    return false;
                });
        }
        // Move to measure and dimension
        $scope.moveToMdArray = {};
        $scope.moveToDimMeasure = function (moveTo,columnName) {
            var is = 0;
            $scope.keysArray.filter(function (d) {
                if (d.reName == columnName) {
                    $scope.keysArray[is].dataKey = moveTo;
                    return true;
                }
                $scope.moveToMdArray[columnName] = moveTo
                is++;
                return false;
            });
            $scope.dimensionMeasureRecall();
        }
        // Rename keys
        $scope.metadataObjColumnName = {};
        $scope.renameKeys = function (name, index) {
            $scope.lastRenameIndex = index;
            var modalElem = $('#myModal');
            $('#myModal').modal({
                backdrop: 'static',
                keyboard: false
            });
            $('#myModal').modal('show');
            $scope.metadataObjColumnName = {};
            $scope.metadataObjColumnName.reNameInput = name;
        }
        $scope.saveRename = function (name) {
            // $scope.reName;
            $('#myModal').modal('hide');
            $scope.keysArray[$scope.lastRenameIndex]['reName'] = name.reNameInput;

            $scope.dimensionMeasureRecall();
            $scope.dataTableCall();
        }
        // Create Column Calculation
        $scope.columnCalculation = function () {
            $('#calculationMeasure').modal({
                backdrop: 'static',
                keyboard: false
            });
            $('#calculationMeasure').modal('show');

        }
        // Operators define
        $scope.operators = [{
            "key": "Addition",
            "value": "+"
        }, {
            "key": "Subtraction",
            "value": "-"
        }, {
            "key": "Multiplication",
            "value": "*"
        }, {
            "key": "Division",
            "value": "/"
        }];
        // function list
        $scope.functionList = [
            {
                "key": "ceil",
                "operator": "Math.ceil()",
                "description": "Ceil rounds a decimal value up to the next highest integer.Syntax: ceil(decimal)"
            },
            {
                "key": "concat",
                "operator": "concat()",
                "description": "concat concatenates multiple strings.Syntax: concat(expression, expression, expression... )"
            },
            {
                "key": "dateDiff",
                "operator": "dateDiff('','')",
                "description": "dateDiff evaluates the difference between two date strings in days.Syntax: dateDiff(date, date)"
            },
            {
                "key": "decimalToInt",
                "operator": "decimalToInt()",
                "description": "decimalToInt converts a decimal value to the integer data type by stripping off the decimal point and any numbers after it. Syntax: decimalToInt(decimal)"
            },
            {
                "key": "floor",
                "operator": "floor()",
                "description": "floor rounds a decimal value down to the closest, lowest integer. Syntax: floor(decimal)"
            },
            {
                "key": "formatDate",
                "operator": "formatDate()",
                "description": "formatDate formats a date using a specified pattern.Syntax: formatDate(date, ['format'], ['time_zone'])"
            },
            {
                "key": "intToDecimal",
                "operator": "parseFloat().toFixed(2)",
                "description": "intToDecimal converts an integer value to the decimal data type.Syntax: parseFloat(expression).toFixed(number)"
            },
            {
                "key": "isNotNull",
                "operator": "isNotNull()",
                "description": "isNotNull evaluates an expression and returns true if the expression is not null.Syntax: isNotNull(expression)"
            },
            {
                "key": "isNull",
                "operator": "isNull()",
                "description": "isNull evaluates an expression and returns true if the expression is null.Syntax: isNull(expression)"
            }, /*
             * { "key" : "left", "operator" :
             * "left(,)", "description" : "left
             * returns a specific number of
             * characters starting with the leftmost
             * character.Syntax: left(expression,
             * limit)" },
             */
            {
                "key": "ltrim",
                "operator": "ltrim()",
                "description": "ltrim removes preceding whitespace from a string.Syntax: ltrim(expression)"
            }, /*
             * { "key" : "now", "operator" :
             * "now()", "description" : "now returns
             * the UTC date and time, in the format
             * yyyy-MM-ddTkk:mm:ss:SSSZ.Syntax:
             * now()" },
             */
            {
                "key": "nullIf",
                "operator": "nullIf()",
                "description": "nullIf compares expression 1 and expression 2. If they are equal, then returns null, else returns expression 1.Syntax: nullIf(expression1, expression2)"
            },
            {
                "key": "parseDate",
                "operator": "parseDate()",
                "description": "parseDate parseDate parses a string to determine if it contains a date value. This function returns all rows that contain a date in a valid format and skips any rows that do not, including rows that contain null values.Syntax: parseDate(date, ['format'], ['time_zone'])"
            }, /*
             * { "key" : "parseDecimal", "operator" :
             * "parseDecimal()", "description" :
             * "parseDecimal parses a string to
             * determine if it contains a decimal
             * value.Syntax:
             * parseDecimal(expression)" },
             */
            {
                "key": "replace",
                "operator": "replace()",
                "description": "replace replaces part of a string with another specified string.Syntax: replace(expression, substring, replacement)"
            },
            {
                "key": "round",
                "operator": "round()",
                "description": "round rounds a decimal value to the closest integer, with decimal values of .5 or larger being rounded up. Syntax: round(decimal)"
            },
            {
                "key": "rtrim",
                "operator": "rtrim()",
                "description": "rtrim removes following whitespace from a string.Syntax: rtrim(expression)"
            },
            {
                "key": "strlen",
                "operator": "strlen()",
                "description": "strlen returns the number of characters in a string.Syntax: strlen(expression)"
            },
            {
                "key": "toLower",
                "operator": "toLower()",
                "description": "toLower formats string in all lowercase; skips rows containing null values.Syntax: toLower(expression)"
            },
            {
                "key": "toString",
                "operator": "toString()",
                "description": "toString formats the input expression as a string; skips rows containing null values.Syntax: toString(expression)"
            },
            {
                "key": "trim",
                "operator": "trim()",
                "description": "trim removes both preceding and following whitespace from a string.Syntax: trim(expression)"
            }];
        // Text add value formula field
        // Text add value formula field
        $scope.measureFormula = "";
        //New Calculation
        $scope.calObject = {};
        $scope.insertTextToFormula = function (name, type, functionListObj) {
            if (type && functionListObj == undefined) {
                name = '[' + name + ']';

            } else if (functionListObj) {
                var description = functionListObj.description;
            }
            if ($scope.calObject.measureFormula == undefined) {
                $scope.calObject.measureFormula = "";
            }
            if (name) {
                $scope.calObject.measureFormula += name;
            }
            $scope.descriptionText = description;
        }
        $scope.filteredData = [];
        $scope.saveCalculationField = function (calObj) {
            var name = calObj.fieldName;
            var formula = calObj.measureFormula;
            if (formula != "" && name != "" && name != undefined) {
                var expression;
                var errorMessage;
                // Check express for bracketFp
                function update() {
                    expression = true;
                    try {
                        balanced.matches({
                            source: formula,
                            open: ['{',
                                '(',
                                '['],
                            close: ['}',
                                ')',
                                ']'],
                            balance: true,
                            exceptions: true
                        });
                    } catch (error) {
                        expression = false;
                        errorMessage = error.message;
                    }
                }

                update();
                expression = true;
                if (expression) {
                    var errorMessage = false;
                    if ($scope.filteredData.length != 0) {
                        errorMessage = calculate.processExpression(formula, $scope.filteredData, name);
                    }
                    else {
                        errorMessage = calculate.processExpression(formula, $scope.tableData, name);
                    }
                    var ColumnFlag = false;
                    $.each($scope.tableColumns, function (key, value) {
                        if (value.columnName == name) {
                            ColumnFlag = true;
                        }
                    });
                    if (ColumnFlag) {
                        $scope.errorCalculation = "Column name already exist";
                        return;
                    }
                    if (errorMessage == false) {
                        $scope.tableColumns.push({
                            "columType": "varchar",
                            "columnName": name,
                            "dataKey": "Measure",
                            "reName": name,
                            "tableName": "",
                            "formula": formula,
                            "type": "custom",
                            "createdby": $scope.columnSelectByGroup
                        });
                        //For Edit calculation
                        if ($scope.flagCheck) {
                            var index = $scope.tableColumns.indexOf($scope.calCurrentEditObj);
                            $scope.tableColumns.splice(index, 1);
                        }
                        $scope.lastColumnShow = true;
                        $("#calculationMeasure").modal(
                            'hide');
                        $scope.dataTableCall();
                    } else {
                        $scope.errorCalculation = errorMessage;

                    }
                } else {
                    $scope.errorCalculation = errorMessage;
                }
            } else if (formula === "") {
                $scope.errorCalculation = "Please select expressions";

            } else if (name === "" || name === undefined) {
                $scope.errorCalculation = "Please Enter field name";
            }
        }
        $scope.columnCalculationEdit = function (measure, index) {
            $scope.calCurrentEditObj = measure;
            $scope.flagCheck = 1;
            $scope.calObject = {};
            $scope.errorCalculation = "";
            $scope.descriptionText = "";
            $scope.calObject.fieldName = measure.reName;
            $scope.calObject.measureFormula = measure.formula;
            $('#calculationMeasure').modal({
                backdrop: 'static',
                keyboard: false
            });
            $('#calculationMeasure').modal('show');
            $(".reportDashboard").addClass("background-container");
        }
        // Popup
        $scope.lastIdSelected = "";
        $scope.lastTableSelected = "";
        $scope.tableArray = [];
        $scope.popup = function (tableName) {
            if ($scope.joinGetDetails[tableName]) {
                var joinDetailsObject = $scope.joinGetDetails[tableName];
                $scope.joinConfiguration = true;
                var join = joinDetailsObject.join;
                $scope.selectjoin=join;
                $(".joinHover").removeClass('joinActive');
                $("." + join).addClass('joinActive');
                $scope.joinSelected = true;
                setTimeout(function () {
                    $scope.Form.leftColumnTable = joinDetailsObject.leftColumnTable;
                    if (!$scope.$$phase) {
                        $scope.$apply();
                    }
                },1000);
                $scope.sourceSelected = true;
                $scope.noRow = joinDetailsObject.row;
                $scope.selectTable = tableName;
                var data = {
                    "tableName": tableName,
                    "connObject": $scope.source.sourceoption
                };
                dataFactory.request($rootScope.TableColumnsInfo_Url, 'post', data).then(function (response) {
                    if (response.data.errorCode == 1) {
                        var response = response.data.result;
                        $scope.dropDetails[tableName] = response;
                    } else {
                        dataFactory.errorAlert(response.data.result);
                    }
                }).then(function () {
                    $scope.selectedColumnNameList = $scope.dropDetails[tableName];
                    $scope.rightColumnTempArray=[];
                    $scope.selectedColumnNameList.forEach(function(d){
                        $scope.rightColumnTempArray[d.Field]=d;
                    });
                }).then(function () {
                    $scope.getValueVariable(joinDetailsObject.leftColumnTable).then(function () {
                        setTimeout(function () {
                            $scope.noRow.forEach(function (d, index) {
                                $scope.Form.node[d] = {};
                                if (!$scope.$$phase) {
                                    $scope.$apply();
                                }
                                //Left column Object Change
                                $scope.leftColumnTempArray=[];
                                var leftObj=JSON.parse(joinDetailsObject.node[d].leftColumn);
                                $scope.selectLeftColumnNameList[leftObj.tableName].forEach(function(d){
                                    $scope.leftColumnTempArray[d.Field+"_"+d.tableName]=d;
                                });
                                var leftKeyTable=leftObj.Field+"_"+leftObj.tableName;
                                if($scope.leftColumnTempArray[leftKeyTable].Field==leftObj.Field){
                                    delete $scope.leftColumnTempArray[leftKeyTable]['$$hashKey'];
                                    $scope.Form.node[d].leftColumn = JSON.stringify($scope.leftColumnTempArray[leftKeyTable]);
                                }
                                //Check right join column Object change
                                var rightObj=JSON.parse(joinDetailsObject.node[d].rightColumn);
                                if($scope.rightColumnTempArray[joinDetailsObject.node[d].rightColumn.Field]==rightObj.Filed){
                                    delete $scope.rightColumnTempArray[rightObj.Field]['$$hashKey'];
                                    $scope.Form.node[d].rightColumn = JSON.stringify($scope.rightColumnTempArray[rightObj.Field]);
                                }
                                if (!$scope.$$phase) {
                                    $scope.$apply();
                                }
                            });
                        }, 10);
                    });
                });
            } else {
                // $scope.LastSelectedindex=id;
                $scope.tableArray.push(tableName);
                $scope.joinConfiguration = true;
                $scope.selectTable = tableName;
                //$scope.selectjoin = "Join";
                $(".joinHover").removeClass('joinActive');
                // Left and Right column
                if ($scope.tableArray.indexOf(tableName) == -1) {
                    $scope.sourceSelected = false;
                    $scope.joinSelected = false;
                }
                var data = {
                    "tableName": $scope.selectTable,
                    "connObject": $scope.source.sourceoption
                };
                dataFactory.request($rootScope.TableColumnsInfo_Url, 'post', data).then(function (response) {
                    if (response.data.errorCode == 1) {
                        var response = response.data.result;
                        $scope.dropDetails[tableName] = response;
                    } else {
                        dataFactory.errorAlert(response.data.message);
                    }
                }).then(function () {
                    $scope.selectedColumnNameList = $scope.dropDetails[tableName];
                });
                if ($scope.myForm[$scope.selectTable] != undefined) {
                    $scope.joinSelected = true;
                    $scope.sourceSelected = true;
                    $scope.joinType($scope.myForm[$scope.selectTable].join);
                    $('.joinActive').removeClass('joinActive');
                    $("." + $scope.myForm[$scope.selectTable].join).addClass("joinActive");
                    $scope.Form.leftColumnTable = $scope.myForm[$scope.selectTable].leftColumnTable;
                    $scope.getValueVariable($scope.Form.leftColumnTable);
                    $scope.selectjoin = $scope.myForm[$scope.selectTable].join;
                    $scope.Form.node[0] = {};
                    $scope.Form.node[0].leftColumn = $scope.myForm[$scope.selectTable].node[0].leftColumn;
                    $scope.Form.node[0].rightColumn = $scope.myForm[$scope.selectTable].node[0].rightColumn;
                    $scope.noRow = $scope.myForm[$scope.selectTable].row;
                } else {
                    $scope.addCount = 0;
                    $scope.noRow = [$scope.addCount];
                }
            }
            var modalElem = $('#tableJoin');
            $('#tableJoin').modal({
                backdrop: 'static',
                keyboard: false
            });
        }
        $scope.addRow = function () {
            $scope.noRow.push($scope.Form.node.length);
        }
        $scope.removeRow = function (index) {
            $scope.myForm[$scope.selectTable].node.splice(index,1);
            $scope.Form.node.splice(index,1);
            $scope.noRow.splice(index, 1);
        }
        // Lisner
        $scope.renameBackupArray = [];
        $scope.selectedTableName = [];
        // Konvas to draw function call
        var stage = new Konva.Stage({
            container: 'canvasContainer',
            width: canvas.canvasWidth,
            height: canvas.canvasHeight
        });
        var layer = new Konva.Layer();
        var i = 0;
        var lastX;
        var lastY;
        var root;
        var rect;
        var line;
        $scope.renderCanvas = function () {

        }
        imageObj.src = 'assets/img/joins-images/join.svg';
        var drawComponent = {};
        $scope.onDrop = function (event, index, item, external, type, allowedType, checboxValue,getDirectData) {
            $scope.metadataSaveShow = 0;// save button hide
            if(getDirectData==undefined){
                $scope.updateData=1;
            }
            $("#dataTableDiv").empty();
            var group = new Konva.Group({
                "id": item
            });

            if (!drawComponent[item]) {
                drawComponent[item] = {};
            }
            if (checboxValue) {
                $scope.selectedTableName.push(item);
                if (i == 0 || stage.children[0].children.length == 0) {
                    root = canvas.getRootRect(item);
                    rect = root;

                } else {
                    rect = root.attrs.addChild(item);
                    line = canvas.drawLine(root, rect, item);
                    drawComponent[item]["line"] = line;
                    group.add(line);
                    // Add image
                    var image = canvas
                        .drawImage(rect, item);
                    drawComponent[item]['image'] = image;
                    image.on('click', function () {
                        $scope.popup(item);
                    });
                    group.add(image);
                }
                var text = canvas.textDraw(rect, item);
                drawComponent[item]["rect"] = rect;
                drawComponent[item]["text"] = text;
                group.add(rect);
                group.add(text);
                // add the layer to the stage
                layer.add(group);
                stage.add(layer);
                i++;
            } else {

                if(Object.keys($scope.myForm).length)
                    delete $scope.myForm[item];
                if(Object.keys($scope.joinGetDetails).length)
                    delete $scope.joinGetDetails[item];
                var deleteIndex = $scope.selectedTableName.indexOf(item);
                $scope.selectedTableName.splice(deleteIndex, 1);
                // var tempDeletedItem=drawComponent[item];
                root.attrs.removeObject();
                var toDeleteIndex = -1;
                stage.children[0].children.forEach(function (d, index) {
                    if (d.attrs.id == item) {
                        toDeleteIndex = index;
                    }
                });
                var row = stage.children[0].children[toDeleteIndex].find('Rect')[0].attrs.row;
                if (deleteIndex == 0) {
                    $scope.selectedTableName = [];
                    $scope.Attributes.checkboxModelDimension = {};
                    $scope.myForm={};
                    $scope.joinGetDetails={};
                    layer.destroy();
                    i = 0;
                } else {
                    stage.children[0].children.splice(toDeleteIndex, 1);
                    stage.children[0].children.forEach(function (d, index) {
                        var node = d.find("Rect")[0];
                        if (row < node.attrs.row) {
                            node.attrs.row--;
                            updateElement(node, d, root);
                        }
                    });
                }
                layer.draw();
            }
            if (stage.children[0] && stage.children[0].children.length != undefined) {
                if (stage.children[0].children.length) {
                    $scope.updateBtnOnLoad = true;
                } else {
                    $scope.updateBtnOnLoad = false;
                }
            }
        };
        // initialize
        Belay.init({
            strokeWidth: 1
        });
        Belay.set('strokeColor', 'blue');
        // jquery
        $('.joinHover').click(function () {
            $('.joinActive').removeClass('joinActive');
            $(this).addClass('joinActive');
        });
        // getValueVariable
        $scope.getValueVariable = function (selectColumn) {
            return new Promise(function (resolve, reject) {
                // Left and Right column
                $scope.selectedTable=[];
                Object.keys($scope.Attributes.checkboxModelDimension).forEach(function(d){
                    $scope.selectedTable.push(d);
                });
                $scope.sourceSelected = true;
                var data = {
                    "tableName": $scope.selectedTable,
                    "connObject": $scope.source.sourceoption
                };
                dataFactory.request($rootScope.TableMultiColumns_Url, 'post', data).then(function (response) {
                    if (response.data.errorCode == 1) {
                        var response = response.data.result;
                        $scope.dropDetails[selectColumn] = response;
                    } else {
                        dataFactory.errorAlert(response.data.message);
                    }
                }).then(function () {
                    $scope.selectLeftColumnNameList = $scope.dropDetails[selectColumn];
                    if (!$scope.$$phase) {
                        $scope.$apply();
                    }
                    resolve();
                });
            });
        }
        // Gridster

        $scope.gridsterOptions = {
            margins: [20, 150],
            columns: 3,
            rowHeight: 70,
            resizable: {
                enabled: false
            },
            draggable: {
                enabled: false
            },
            floating: true,
            pushing: false
        };
        $scope.dashboards = {
            '1': {
                id: '1',
                name: 'Home',
                widgets: []
            }
        };
        $scope.clear = function () {
            $scope.dashboard.widgets = [];
        };
        $scope.countWidget = 0;
        $scope.indexWidget = 0;
        $scope.addWidget = function (name, index) {
            var dashboard = true;
            $scope.countWidget++;
            if ($scope.countWidget >= 3) {
                $scope.dashboard.widgets.push({
                    name: name,
                    sizeX: 1,
                    sizeY: 1,
                    row: $scope.countWidget - 2,
                    col: 1,
                    dbTab: name + "." + name,
                    dbName: name,
                    id: $scope.indexWidget

                });
            } else if (dashboard == true) {
                $scope.dashboard.widgets.push({
                    name: name,
                    sizeX: 1,
                    sizeY: 1,
                    dbTab: name,
                    dbName: name,
                    id: $scope.indexWidget
                });
            } else {
                alert("Can not drop same table");
            }
            $scope.indexWidget++;
        };
        $scope.$watch('selectedDashboardId',
            function (newVal, oldVal) {
                if (newVal !== oldVal) {
                    $scope.dashboard = $scope.dashboards[newVal];
                } else {
                    $scope.dashboard = $scope.dashboards[1];
                }
            });
        // init dashboard
        $scope.selectedDashboardId = '1';
        // End Gridster
        // Meta data Save to table
        $scope.metaDataSave = function () {
            try{
                $scope.metadataObject['dashboard'] = $scope.dashboard;
                if($scope.metadataName){
                    var data={
                        "metadataObject":JSON.stringify($scope.metadataObject),
                        "group":JSON.stringify($scope.categoryGroupObject),
                        "metadataName":$scope.metadataName,
                        "reportGroup":$scope.reportGrp,
                        "publicViewType":$scope.publicViewType
                    };
                    dataFactory.request($rootScope.MetadataSave_Url,'post',data).then(function(response) {
                        if(response.data.errorCode==1){
                            $window.location = '#/metadata/';
                            dataFactory.successAlert("Metadata save successfully");
                        }else{
                            dataFactory.errorAlert(response.data.message);
                        }
                    });
                }else{
                    dataFactory.errorAlert("Enter metadata name");
                }
            }catch($e){
                dataFactory.errorAlert("Check your connection");
            }
        };
        $scope.closeMetadata = function () {
            $window.location = '#/metadata/'
        }
        //Get Metadata list
        $scope.metadataId = $stateParams.data;
        //checkbox value define
        $scope.source = {};
        $scope.Attributes = {};
        $scope.Attributes.checkboxModelDimension = {};
        //Get Metadata list
        $scope.formCheckbox = {};
        $scope.selectedTableName = [];
        $scope.myForm = {};
        $(".loadingBar").show();
        dataFactory.request($rootScope.datasourceList_Url, 'post', "").then(function (response) {
            $scope.loadingBarList = false;
            if (response.data.errorCode == 1) {
                $scope.sourcedata=[];
                $.each(response.data.result,function(key,value){
                    value.forEach(function(d){
                        $scope.sourcedata.push(d);
                    });
                });
            } else {
                dataFactory.errorAlert(response.data.message);
            }
        }).then(function () {
            var data = {
                "metadataId": $scope.metadataId
            };
            dataFactory.request($rootScope.MetadataGet_Url, 'post', data).then(function (response) {
                if (response.data.errorCode == 1) {
                    $scope.responseObj=JSON.parse(response.data.result.metadataObject);
                    $scope.metaDataObject = JSON.parse(response.data.result.metadataObject);
                    if($scope.metaDataObject.limit!=undefined)
                        $scope.limitInput=$scope.metaDataObject.limit
                    if($scope.metaDataObject.connectionObject.datasourceType==undefined){
                        $scope.metaDataObject.connectionObject.datasourceType='mysql';
                    }
                    $scope.tableColumns = $scope.metaDataObject.column;
                    if ($scope.metaDataObject.condition) {
                        $scope.condition = $scope.metaDataObject.condition;
                        $scope.whereConditionText = $scope.metaDataObject.condition;
                        $scope.whereCondition = $scope.metaDataObject.condition;
                    }
                    //&& $scope.limitCheck==true
                    if ($scope.metaDataObject.limit) {
                        $scope.limit = $scope.metaDataObject.limit;
                        $scope.limitText = $scope.metaDataObject.limit;
                        $scope.limitCheck = true;
                    }
                    delete $scope.metaDataObject.connectionObject['created_at'];
                    delete $scope.metaDataObject.connectionObject['updated_at'];
                    $scope.source.sourceoption = JSON.stringify($scope.metaDataObject.connectionObject);
                    $scope.joinGetDetails = $scope.metaDataObject.joinsDetails;
                    Object.keys($scope.joinGetDetails).forEach(function (d) {
                        $scope.myForm[d] = $scope.joinGetDetails[d];
                    });
                    $scope.metadataName = response.data.result.name;
                    $scope.groupObject = JSON.parse(response.data.result.group);
                    $scope.dashboard = $scope.metaDataObject.dashboard;
                    $scope.sourceSelect($scope.source.sourceoption).then(function(){
                        $scope.getData(JSON.stringify($scope.metaDataObject.connectionObject), 'default' );
                    });
                    $scope.Attributes.checkboxModelDimension[$scope.metaDataObject.rootTable] = true;
                    Object.keys($scope.metaDataObject.joinsDetails).forEach(function (d) {
                        $scope.Attributes.checkboxModelDimension[d] = true;
                    });
                    $scope.onDrop("", 0, $scope.metaDataObject.rootTable, "", "", "", true,1);
                    Object.keys($scope.joinGetDetails).forEach(function (d) {
                        $scope.onDrop("", 0, d, "", "", "", true,1);
                    });
                    //Change image canvas
                    for (var i = 0; i < 2; i++) {
                        Object.keys($scope.joinGetDetails).forEach(function (s) {
                            $scope.selectjoin = $scope.joinGetDetails[s].join;
                            layer.children.forEach(function (d) {
                                d.children.forEach(function (p, index) {
                                    if (p.className == "Image" && p.attrs.id == s) {
                                        if ($scope.selectjoin == "Inner") {
                                            p.attrs.image.src = "assets/img/joins-images/join_inner.svg";
                                            p.attrs.image.srcset = "assets/img/joins-images/join_inner.svg";
                                        } else if ($scope.selectjoin == "Left") {
                                            p.attrs.image.src = "assets/img/joins-images/join_left.svg";
                                            p.attrs.image.srcset = "assets/img/joins-images/join_left.svg";
                                        } else if ($scope.selectjoin == "Right") {
                                            p.attrs.image.src = "assets/img/joins-images/join_right.svg";
                                            p.attrs.image.srcset = "assets/img/joins-images/join_right.svg";
                                        } else if ($scope.selectjoin == "Outer") {
                                            p.attrs.image.src = "assets/img/joins-images/join_outer.svg";
                                            p.attrs.image.srcset = "assets/img/joins-images/join_outer.svg";
                                        }
                                        layer.draw();
                                    }
                                });
                            });
                        });
                    }
                    promise.then(function () {
                        $(".loadingBar").hide();
                    });
                } else {
                    dataFactory.errorAlert("Check your connection");
                }
            }).then(function () {
                $scope.source.sourceoption=JSON.stringify($scope.responseObj.connectionObject);
            });
        })
        //Excel indexing
        $scope.tableNameObj={};
        $scope.excelIndex=function(){
            setTimeout(function () {
                $scope.tableNameObj={};
                $scope.tableNameObj.tableIndexName='';
                $scope.$apply();
            },1000);

            $scope.tableIndexObj=[];
            $('#indexingModal').modal({
                backdrop : 'static',
                keyboard : false
            });
            $scope.indexTable=false;
        }

        $scope.addIndexingRow=function(){
            if($scope.indexingRow.length)
                $scope.indexingRow.push($scope.indexingRow[$scope.indexingRow.length-1]+1);
            else
                $scope.indexingRow.push(1);
            setTimeout(function(){
                $(".columnIndexSelect").selectpicker();
            },500);
        }
        $scope.deleteIndexingRow=function(index){
            $scope.indexingRow.splice(index,1);
        }
        $scope.indexDelete=function (index,indexColumn) {
            var data={
                "connObject":$scope.source.sourceoption,
                "indexColumn":indexColumn
            };
            dataFactory.request($rootScope.TableIndexDelete_Url,'post',data).then(function(response) {
                if(response.data.errorCode==1){
                    dataFactory.successAlert("Index delete successfully");
                    $scope.indexList.splice(index,1);
                }
            });
        }
        //Get indexs
        $scope.indexTable=false;
        $scope.getIndexs=function(tableName){
            if(tableName==0){
                $scope.indexTable=false;
                return;
            }
            var data={
                "tableName":tableName,
                "connObject":$scope.source.sourceoption
            };
            dataFactory.request($rootScope.TableIndex_Url,'post',data).then(function(response) {
                if(response.data.errorCode==1){
                    $scope.indexTable=true;
                    //$scope.indexList=response.data.result;

                    $scope.indexList = [];
                    var indexCount=1;
                    response.data.result.forEach(function(value) {
                        var existing = $scope.indexList.filter(function(v, i) {
                            return v.Key_name == value.Key_name;
                        });
                        if (existing.length) {
                            var existingIndex = $scope.indexList.indexOf(existing[0]);
                            $scope.indexList[existingIndex].Column_name=$scope.indexList[existingIndex].Column_name.concat(",");
                            $scope.indexList[existingIndex].Column_name = $scope.indexList[existingIndex].Column_name.concat(value.Column_name);
                        } else {
                            if (typeof value.Column_name == 'string')
                                value.Column_name = value.Column_name+",";
                            value.Column_name=value.Column_name.replace(/,\s*$/, "");
                            $scope.indexList.push(value);
                        }
                        indexCount++;
                    });
                    if($scope.indexList.length){
                        $scope.indexingRow=[];
                    }else{
                        $scope.indexingRow=["0"];
                        setTimeout(function() {
                            $(".columnIndexSelect").selectpicker();
                        },500);
                    }
                }else{

                }
            });
            dataFactory.request($rootScope.TableColumnsInfo_Url,'post',data).then(function(response) {
                if(response.data.errorCode==1){
                    $scope.tableIndexColumn= response.data.result;
                }
            })
        }
        $scope.columnCheck=function(column){
            $scope.flag=false;
            $scope.indexList.forEach(function(d){
                if(d.Key_name==column){
                    $scope.flag=true;
                }
            });
        }
        $scope.tableIndexObj=[];
        $scope.tableIndexCreate=function(tableIndexObj,tableIndexName){
            return new  Promise(function(resolve, reject){
                tableIndexObj.forEach(function(d,index){
                    $scope.columnCheck(d.keyName);
                    if(tableIndexObj.length-1==index){
                        if($scope.flag){
                            dataFactory.errorAlert("column already defined");
                        }else{
                            resolve();
                            var data={
                                "tableIndex":tableIndexObj,
                                "connObject":$scope.source.sourceoption,
                                "tableName":tableIndexName
                            };
                            dataFactory.request($rootScope.TableIndexCreate_Url,'post',data).then(function(response) {
                                if(response.data.errorCode==1){
                                    dataFactory.successAlert("Index create successfully");
                                    $('#indexingModal').modal('hide');
                                }
                            });
                        }
                    }
                });
            });
        }
        /*
                 Report Group
                 */
        $scope.publicViewType="specific_group";
        $scope.reportGrp={};
        dataFactory.request($rootScope.UsergroupList_Url,'post',"").then(function(response){
            $scope.userGroup=response.data.result;
        });
        dataFactory.request($rootScope.ReportGroupList_Url,'post',"").then(function(response){
            $scope.reportGroup=response.data.result;
        });
        $scope.selectGroup=function(divClass){
            if(divClass=='back'){
                $(".mainDiv").slideDown('fast');
                $(".select").slideUp('fast');
                $(".create").slideUp('fast');
                $(".footerDiv").slideUp('fast');
            }else{
                $("."+divClass).slideDown('fast');
                $(".footerDiv").slideDown('fast');
                $(".mainDiv").slideUp('fast');
            }
            if(divClass=='select' || divClass=='create'){
                $scope.reportGrp={};
            }
        }
        $scope.checkView=function(page){
            $scope.publicViewType=page;
            if(page=='public_group'){
                $(".subDiv").hide();
                $(".footerDiv").show();
            }
            else{
                $(".subDiv").show();
                $(".footerDiv").hide();
            }
        }
        $scope.groupTypeModal=function(){
            $('#reportGroup').modal('show');
        }
        $scope.groupTypeSave=function(reportGrp){
            if(Object.keys($scope.reportGrp).length==0 && $scope.publicViewType=='sharedview'){
                dataFactory.errorAlert("Select group");
            }else{
                if(reportGrp.name && !reportGrp.userGroup){
                    dataFactory.errorAlert("Select report group");
                    return;
                }
                $scope.reportGrp=reportGrp;
                $('#reportGroup').modal('hide');
                $scope.metaDataSave();
            }
        }

    }]);
