'use strict';
angular.module('app', ['ngSanitize', 'gridster'])
    .controller('dashboardCopyController', ['$scope', '$sce', 'dataFactory', '$filter', '$stateParams', '$location', '$window', '$rootScope', '$cookieStore',
        function ($scope, $sce, dataFactory, $filter, $stateParams, $location, $window, $rootScope, $cookieStore) {
            // Select Box start
            //--- Top Menu Bar
            //--- Top Menu Bar
            $("#chartjs-tooltip").show();
            var data = {};
            var addClass = '';
            var viewClass = '';
            if ($location.path() == '/dashboard/') {
                viewClass = 'activeli';
            } else {
                addClass = 'activeli';
            }
            data['navTitle'] = "DASHBOARD";
            data['icon'] = "fa fa-line-chart";
            data['navigation'] = [{
                "name": "Add",
                "url": "#/dashboard/add",
                "class": addClass
            },
                {
                    "name": "View",
                    "url": "#/dashboard/",
                    "class": viewClass
                },
                {
                    "name": "Close",
                    "url": "#/dashboard/",
                    "class": viewClass
                }];
            $rootScope.$broadcast('subNavBar', data);
            //End Menu
            //Default Tab
            $scope.tab = false;
            //End
            $scope.dashboardId = $stateParams.data;
            // Select Box start
            $scope.trustAsHtml = function (value) {
                return $sce.trustAsHtml(value);
            };
            var vm = $scope;
            vm.selectedChart = {};
            vm.customGroup = [];
            vm.activeXLabel = {};
            vm.activeYLabel = {};
            vm.aggregateText = {};
            vm.showInfo2 = false;
            vm.showInfo1 = true;
            vm.chartTypeLineBar = {};
            sketch = sketch.reset();
            sketchServer = sketchServer.reset();
            $scope.columnSearch = "";
            $(".full-height").addClass("full-height-background");
            // vm.AggregateFunctions=[];
            vm.tableColumns = [];
            $scope.dashName = {};
            $scope.lineBar = {};
            $scope.lastDrawnChart = {};
            $scope.selectedChartInView = {};
            var userId = $cookieStore.get('userId');
            vm.filteredData = [];
            //Next
            vm.cascadeArr = [];
            vm.applyRunningTotal = function (measure, flag) {
                measure["runningTotal"] = flag;
                vm.leftSideController.updateAttributesObject(measure, "measure")
                //  if(vm.leftSideController.updateAttributesObject(measure,"measure")){
                var activeChart = vm.leftSideController.getSelectedChartFromView();
                //check if any chart selected in view
                if (!activeChart) {
                    vm.leftSideController.setChartInView(vm.DashboardModel.Dashboard.activeReport.chart);
                } else {
                    vm.leftSideController.checkAndDrawChart(activeChart);
                }
                //    }
            };

            $scope.$watch('tableColumns', function (model) {
                if (model && model.length) {
                    sketch.columnsByColumnName = {};
                    sketchServer.columnsByColumnName = {};
                    model.forEach(function (d) {
                        sketch.columnsByColumnName[d.columnName] = d;
                        sketchServer.columnsByColumnName[d.columnName] = d;
                    });
                }
            });
            vm.dataType = function (type) {
                if (type == 'measure')
                    return sketch.dataTypeMeasure;
                else
                    return sketch.dataTypeDimension;
            }
            vm.typeCast = function (object, dataType) {
                var index;
                vm.tableColumns.forEach(function (d, arrayIndex) {
                    if (d == object) {
                        index = arrayIndex;
                    }
                });
                vm.tableColumns[index].columType = dataType.type;
            }

            vm.colorPallete = ["#810040", "#DE3B00", "#FF8700", "#FFB500", "#B9E52F", "#2CAD00", "#30CB71", "#33AEB1", "#A8577B", "#EE94C9", "#F6AA54", "#F7E65A              ", "#B6F5AC", "#25F1AA", "#C6F0D7", "#A7DBE2", "#219FD7", "#0251D3", "#863CFF", "#E436BB", "#EA5081", "#8B5011", "#000000", "#AAAAAA", "#9FDEF1", "              #ED90C7", "#6197E2", "#C2C4E3", "#DFC0ED", "#DDCFA4", "#2A5D78", "#EEEEEE"];
            vm.rowsLength = function () {
                var length = vm.colorPallete.length;
                var times = length / 8;
                if (length % 8 == 0)
                    times = length / 8;
                else
                    times = (length / 8) + 1;
                var arr = [];
                for (var i = 0; i < times; i++) {
                    arr.push(i);
                }
                return arr;
            }
            vm.getColorPallete = function (val) {
                return vm.colorPallete.slice(val * 8, val * 8 + 8);
            }
            $scope.$watch("columnSearch", function (model) {

            });
            vm.colorChange = function (color) {
                if (vm.dataCount < $rootScope.localDataLimit) {
                    eChart.addColor(color);
                } else {
                    eChartServer.addColor(color);
                }
            }
            $(".page-content").on('click', function (ev) {
                //      $(".colorPalette").hide();
            });
            vm.applyFilter = function () {
                if (vm.dataCount < $rootScope.localDataLimit) {
                    eChart.applyFilter();
                } else {
                    eChartServer.applyFilter();
                }
                $(".colorPalette").hide();
            }

            vm.openSettingsWindow = function (type) {
                vm.curPlace = "";
                if (type == "Measure") {
                    $scope.measureLi = true;
                    $scope.dimensionLi = false;
                    $scope.typeSelectMD = "measure";
                } else {
                    $scope.measureLi = false;
                    $scope.dimensionLi = true;
                    $scope.typeSelectMD = "category";
                }
                vm.addCategoryContainer = function () {
                    $scope.categoryContainers.push({});
                    /*$('.multielect1').multiselect({
                          includeSelectAllOption: true,
                          enableFiltering: true,
                          maxHeight: 200
                          });*/
                    $(".multielect1").selectpicker();
                }
                vm.removeCategories = function (index) {
                    vm.categoryContainers.splice(index, 1);
                }
                vm.columnTypekeysArray = vm.tableColumns.filter(function (d) {
                    if (d.type.indexOf(type) != -1) {
                        return true;
                    }
                    return false;
                });
                vm.calMeasureShow = [];
                vm.calMeasureDb = [];
                vm.measureAdd = function () {
                    // Formula in array
                    var expression = $scope.measureFormula;
                    var objectNewKeys = {
                        columType: "int",
                        columnName: $scope.measureName,
                        dataKey: "Measure",
                        tableName: "",
                        formula: $scope.measureFormula
                    }
                    $scope.updatebtn = true;
                    $scope.calMeasureFormula
                        .push(objectNewKeys);
                    $scope.tableColumns.push(objectNewKeys);
                    $scope.measureName = "";
                    $scope.measureFormula = "";
                    generate('success', 'Measure create successfully');
                    $scope.lastColumnShow = true;
                }
                $scope.measureFormula = "";

                $scope.insertText = function (elemID, text, tableName) {
                    if (tableName) {
                        $scope.measureFormula += tableName + "." + text;
                    } else {
                        $scope.measureFormula += text;
                    }
                }

                $scope.insertText1 = function (elemID, text, tableName) {
                    if (tableName) {
                        $scope.measureFormula += tableName + "." + text;
                    } else {
                        $scope.measureFormula += text;
                    }
                }

                // vm.categories=[];

            };
            // Next Tab Select
            vm.nextTab = function (columnType) {
                vm.selectDimension = columnType;
                var dataKey = columnType;
                var tempData = [];
                Enumerable.From(vm.tableData).Distinct("$." + dataKey).Select("$." + dataKey).ToArray().forEach(function (e) {
                    tempData.push(e);
                });
                vm.dataItems = tempData;
            }
            //Pivot table
            $scope.pivotTable = function () {
                $('#pivot-table').modal({
                    backdrop: 'static',
                    keyboard: false
                });
                $('#pivot-table').modal('show');
            }
            // Next Tab Select
            vm.nextTabValidation = function (columnType) {
                vm.selectDimension = columnType;
                vm.measureNameValidation = columnType + "WithValidation";
                vm.lastColumSelected = columnType;
                var dataKey = columnType;
                var tempData = [];
                Enumerable.From(vm.tableData).Distinct("$." + dataKey).Select("$." + dataKey)
                    .ToArray().forEach(function (e) {
                    tempData.push(e);
                });
                vm.dataItems = tempData;
            }
            // back Tab
            vm.backTab = function () {

            }
            vm.dimensionCategoryChange = function () {
                var dataKey = vm.categories.column;
                var tempData = [];
                Enumerable.From(sketch.getData()).Distinct(
                    "$." + dataKey).Select("$." + dataKey)
                    .ToArray().forEach(function (e) {
                    tempData.push(e);
                });
                vm.dataItems = tempData;
            }

            // advanced chart settings
            vm.advancedChartSettingModal = {
                dateFormats: sketch._dateFormats.formats,
                groups: sketch._dateFormats.groups,
                Categories: [{}]
            }

            vm.advancedChartSettingController = {
                init: function () {
                    vm.advancedChartSettingView.init();
                    vm.advancedChartSettingView.render();
                },
                fetchDateFormats: function () {
                    return vm.advancedChartSettingModal.dateFormats;
                },
                fetchGroupFormats: function () {
                    return vm.advancedChartSettingModal.groups;
                },
                getDimensionalData: function () {
                    // sketch._
                },
                addCategoryContainer: function () {
                    vm.categoryContainers.push({});
                },
                updateTableData: function (categoryData) {
                    var keys = Object.keys(categoryData.selectedItems);
                    var obj = {};
                    var column = categoryData.column;
                    keys.forEach(function (index) {
                        categoryData.selectedItems[index].forEach(function (d) {
                            obj[d] = categoryData.categoryName[index];
                        });
                    });
                    var newColumnName = categoryData.Name;
                    var updatedData = null;

                    vm.tableData.forEach(function (d) {
                        if (obj[d[column]]) {
                            d[newColumnName] = obj[d[column]];
                        } else {
                            d[newColumnName] = d[column];
                        }
                    });
                },
                saveCategories: function (categoriesData) {
                    var testObject = {
                        "key": "Text",
                        "value": categoriesData.Name,
                        "type": ["DimensionCustom"]
                    };
                    testObject['categoriesData'] = new Object(categoriesData);
                    vm.customGroup.push(testObject);
                    vm.tableColumns.push(testObject);
                    vm.advancedChartSettingController.updateTableData(categoriesData);
                    generate('success', 'Category create successfully');
                },
                removeCategories: function (index) {
                    vm.categoryContainers.splice(index, 1);
                }
            }

            vm.advancedChartSettingView = {
                init: function () {
                    vm.categories = {};
                    vm.categoryContainers = [];
                    vm.addCategoryContainer = vm.advancedChartSettingController.addCategoryContainer;
                    vm.saveCategories = vm.advancedChartSettingController.saveCategories;
                    vm.removeCategory = vm.advancedChartSettingController.removeCategories;
                },
                render: function () {
                    vm.dateFormatList = vm.advancedChartSettingController.fetchDateFormats();
                    vm.dateGroupList = vm.advancedChartSettingController.fetchGroupFormats();
                }
            }
            vm.advancedChartSettingController.init();
            // .............................................
            $scope.edit = function (widget) {
                lastcontainerId = "#chart-" + widget.id;
                if (Object.keys(vm.lastAppliedFilters).length != 0) {
                    if (vm.lastAppliedFilters[lastcontainerId] != undefined) {
                        var config = vm.lastAppliedFilters[lastcontainerId];
                        loadSelectedConfig(config);
                    }
                }
                if (vm.chartType == "Number Widget") {
                    $scope.graphType = true;
                } else {
                    $scope.graphType = false;
                }
            }
            // .................................................................................................
            vm.leftSideModel = {
                chartTypeData: sketch.chartData,
                aggregateData: sketch.AggregatesObject
            };
            vm.leftSideController = {
                init: function () {
                    //Number widget setting
                    vm.numberStyle = {};
                    //Filter Remove

                    //Map Comparison Start
                    vm.Multi_Arr = [0];
                    vm.addRadius = function () {
                        vm.Multi_Arr.push(vm.Multi_Arr.length);
                    }
                    vm.removeRadius = function (index) {
                        if (index) {
                            vm.Multi_Arr.splice(index, 1);
                        }
                    }

                    vm.mapObject = {};
                    // vm.mapObject.multipleTooltip = [];
                    // vm.mapObject.multipleTooltipObj = [];
                    // vm.mapObject.multipleTooltip_Obj = {};

                    var mapTooltipSelector = [];
                    // var mapTooltipData = '';
                    vm.insertTooltipTextMap = function (columnName, index) {
                        var chartID = vm.DashboardModel.Dashboard.activeReport.reportContainer.id;

                        if (vm.mapObject[chartID].multipleTooltip_Obj[index] == undefined) {
                            vm.mapObject[chartID].multipleTooltip_Obj[index] = [];
                        }
                        vm.mapObject[chartID].multipleTooltip_Obj[index].push(columnName);

                        // var temp = columnName.columnName;
                        var temp = columnName.reName;

                        if (columnName.dataKey == 'Measure') {
                            var mapTooltipData = temp + ' : <sum(' + temp + ')><br> \n';
                        } else {
                            var mapTooltipData = temp + ' : <' + temp + '><br> \n';
                        }

                        if (vm.mapObject[chartID].multipleTooltip[index] == undefined) {
                            vm.mapObject[chartID].multipleTooltip[index] = '';
                        } else {
                            vm.mapObject[chartID].multipleTooltip[index] = vm.mapObject[chartID].multipleTooltip[index];
                        }
                        vm.mapObject[chartID].multipleTooltip[index] += mapTooltipData;

                        if (vm.mapObject[chartID].multipleTooltipObj[index] == undefined) {
                            vm.mapObject[chartID].multipleTooltipObj[index] = [];
                        } else {
                            vm.mapObject[chartID].multipleTooltipObj[index] = vm.mapObject[chartID].multipleTooltipObj[index];
                        }
                        vm.mapObject[chartID].multipleTooltipObj[index].push(columnName);
                        mapTooltipSelector.push(columnName);
                    }
                    //Map Comparison End


                    vm.numberSetting = function (numberSetting) {
                        $("#chart-" + vm.DashboardModel.Dashboard.activeReport.reportContainer.id + " > .number-display").attr('style', 'font-size:' + numberSetting.fontSize + 'px;color:' + numberSetting.fontColor);
                        vm.Attributes['numberStyle'] = numberSetting;
                        $('#chartSetting').modal('hide');
                    }


                    //Check Chart type
                    vm.checkChartType = function (equal, notEqual) {
                        var status = false;
                        if (equal.length) {
                            equal.forEach(function (d) {
                                if (vm.settingChartType == d)
                                    status = true;
                            });
                        } else {
                            status = true;
                            notEqual.forEach(function (d) {
                                if (vm.settingChartType == d)
                                    status = false;
                            });
                        }
                        return status;
                    }
                    //Table Static Header
                    $('table').on('scroll', function () {
                        $("table > *").width($("table").width() + $("table").scrollLeft());
                    });
                    //Table Setting
                    vm.tableSetting = {};
                    vm.tableSettingFun = function (setting) {
                        setting['headerGroup'] = vm.selectedGroup;
                        setting['tableColumnOrder'] = vm.tableColumnOrder;
                        vm.Attributes['tableSettting'] = setting;

                        if (vm.tableCount < $rootScope.localDataLimit) {
                            sketch
                                .axisConfig(vm.Attributes)
                                .data(vm.tableData)
                                .container(vm.DashboardModel.Dashboard.activeReport.reportContainer.id)
                                .chartConfig($scope.selectedChartInView[vm.DashboardModel.Dashboard.activeReport.reportContainer.id])
                                .render(function (drawn) {
                                    //$scope.lastDrawnChart[vm.DashboardModel.Dashboard.activeReport.reportContainer.id] = vm.DashboardModel.Dashboard.activeReport.chart.key;
                                });
                        } else {
                            sketchServer
                                .accessToken($rootScope.accessToken)
                                .axisConfig(vm.Attributes)
                                .data([])
                                .container(vm.DashboardModel.Dashboard.activeReport.reportContainer.id)
                                .chartConfig($scope.selectedChartInView[vm.DashboardModel.Dashboard.activeReport.reportContainer.id])
                                .render(function () {

                                    //$scope.lastDrawnChart[vm.DashboardModel.Dashboard.activeReport.reportContainer.id] = vm.DashboardModel.Dashboard.activeReport.chart.key;
                                });
                        }


                        $('#chartSetting').modal('hide');
                    }
                    //Add Backgorund class
                    vm.addBackgroud = function (column) {
                        if ($("#backgroudCheck-" + column).hasClass("changeBg")) {
                            $("#backgroudCheck-" + column).removeClass('changeBg');
                        } else {
                            $("#backgroudCheck-" + column).addClass('changeBg');
                        }
                    }

                    //Reset Chart
                    vm.resetChart = function () {
                        if (vm.dataCount < $rootScope.localDataLimit) {
                            eChart.resetAll();
                        } else {
                            eChartServer.resetAll(vm.metadataId);
                        }
                        dataFactory.successAlert("Chart reset successfully");
                    }
                    vm.leftSideView.render();
                    //Define Chart setting
                    vm.chartSettingObject = {};


                    //creating pivot table
                    $scope.pivotTableSave = function () {
                        $('#commonModals').modal('hide');

                        vm.Attributes['pivotTableObject'] = {};
                        vm.Attributes['pivotTableObject']['dataObject'] = sketch.pivotTableConfig;
                        vm.DashboardModel.Dashboard.activeReport['dataObject'] = vm.Attributes['pivotTableObject'];

                        var activeReport = vm.DashboardModel.Dashboard.activeReportIndex;
                        vm.DashboardModel.Dashboard.Report[activeReport].chart = sketch.chartData[9];
                        vm.DashboardView.updateDashboardObject(sketch.pivotTableConfig);
                        //      vm.DashboardModel.Dashboard.activeReport['dataObject'] = sketch.pivotTableConfig;

                        sketch.axisConfig(vm.Attributes)
                            .container(vm.DashboardModel.Dashboard.activeReport.reportContainer.id)
                            ._pivotTableDraw();

                        // var pivotTableConfig = sketch._pivotTableDraw();
                    }


                    //.....................
                    vm.chartErrorText = {};
                    vm.showInfo = {};
                    vm.showAggregate = {};
                    // Remove Measure checkbox
                    vm.removeMeasure = function (columnName, checkBoxObject) {
                        delete vm.Attributes.checkboxModelMeasure[columnName];
                        vm.chartDraw(checkBoxObject, 'measure');
                    }
                    // Remove Dimension
                    vm.removeDimension = function (columnName, checkBoxObject) {
                        delete vm.Attributes.checkboxModelDimension[columnName];
                        vm.chartDraw(checkBoxObject, 'dimension');
                    }
                    // watching query selector for change
                    $scope.selectDataSource = function (model) {
                        //$('[data-toggle="tooltip"]').tooltip();
                        vm.dashboardValidation = true;
                        vm.loadingBarDrawer = true;
                        vm.tableData = [];
                        vm.tableColumnsMeasure = [];
                        sketch._crossfilter = null;
                        sketch._data = null;
                        vm.filtersToApply = [];
                        lastcount = 0;
                        $scope.clear();
                        // resetAllAxis();
                        if (model != undefined) {
                            vm.ischartTypeShown = true;
                            vm.isQuerySelected = false;
                            vm.graphAxis = false;
                            vm.queryObj = model;
                            vm.dashboardValidation = true;
                            vm.dashboardValidation1 = true;
                            vm.showInfo1 = false;
                            vm.showInfo2 = true;
                            vm.preLoad = true;
                            vm.groupObject = "";
                            vm.metadataObject = model;
                        }
                        vm.moveToMdArray = {};
                        vm.moveToDimMeasure = function (moveTo, columnName, isChecked) {
                            if (!isChecked) {
                                vm.tableColumns.filter(function (d, is) {
                                    if (d.columnName == columnName) {
                                        vm.tableColumns[is].dataKey = moveTo;
                                        return true;
                                    }
                                    $scope.moveToMdArray[columnName] = moveTo
                                    return false;
                                });
                            } else {
                                alert("Can't Move to " + moveTo + " If it's selected");
                            }
                            // $scope.dimensionMeasureRecall();
                        }
                    }
                    vm.periodVal = [];
                    var i = 1;
                    vm.addPeriod = function () {
                        vm.filterPeriod = true;
                        vm.blnkFilter = false;
                        vm.periodVal.push(i);
                        i++;
                    }
                    vm.removePeriod = function (index) {
                        vm.periodVal.splice(index, 1);
                    }
                    //Chart Setting Model
                    vm.selectedGroup = {};
                    var custIndex = 0;
                    vm.subCategory = {};
                    vm.headerGroup = function () {
                        if (vm.tableHeader.length) {
                            $(".ms-selected").hide();
                            if (vm.subCategory[custIndex] == undefined) {
                                vm.subCategory[custIndex] = 'group' + custIndex;
                            }
                            vm.selectedGroup['group' + custIndex] = vm.tableHeader;
                            vm.tableHeader = [];
                            custIndex++;
                        } else {
                            alert("select any one");
                        }
                    }
                    vm.inertTooltipText = function (columnName) {
                        if (columnName.dataKey == "Measure") {
                            vm.chartSettingObject.toolTipFormate += columnName.reName + ' : <sum(' + columnName.reName + ')>\n';
                            //$("#tooltip").insertAtCaret();
                        } else {
                            //$("#tooltip").insertAtCaret(columnName.reName+' : <'+columnName.reName+'>\n');
                            vm.chartSettingObject.toolTipFormate += columnName.reName + ' : <' + columnName.reName + '>\n';
                        }
                        vm.tooltipSelector.push(columnName);
                    }

                    vm.chartSetting = function (chartType) {
                        var container_ID = vm.DashboardModel.Dashboard.activeReport.reportContainer.id;
                        if (chartType == 'Pivot Table Customized') {
                            vm.settingChartType = chartType;
                            vm.pivotContainerID = container_ID;

                            // For selected radio button
                            if (vm.pivotCust_DataObject[container_ID] == undefined) {
                                vm.ptCust_DataObj[container_ID] = {};
                                vm.ptCust_DataObj[container_ID]['data'] = {};

                                vm.ptCust_GrandTotalObj[container_ID] = {};
                                vm.ptCust_GrandTotalObj[container_ID]['grandTotal'] = {};
                                vm.ptCust_GrandTotalObj[container_ID]['grandTotal']['row'] = 'no';
                                vm.ptCust_GrandTotalObj[container_ID]['grandTotal']['column'] = 'no';

                                vm.ptCust_SubTotalObj[container_ID] = {};
                                vm.ptCust_SubTotalObj[container_ID]['subTotal'] = {};
                                vm.ptCust_SubTotalObj[container_ID]['subTotal']['row'] = 'no';
                                vm.ptCust_SubTotalObj[container_ID]['subTotal']['column'] = 'no';

                                vm.ptCust_RunningTotalObj[container_ID] = {};
                                vm.ptCust_RunningTotalObj[container_ID]['runningTotal'] = {};
                                vm.ptCust_RunningTotalObj[container_ID]['runningTotal']['row'] = 'no';
                                vm.ptCust_RunningTotalObj[container_ID]['runningTotal']['column'] = 'no';


                                if (vm.chartPivotObject[container_ID]) {
                                    vm.pt_Selected_Data_Attr = vm.chartPivotObject[container_ID].Data;
                                    vm.pt_Selected_Data_Attr.forEach(function (d) {
                                        vm.ptCust_DataObj[container_ID]['data'][d] = 'sum';
                                    });
                                }
                            } else {
                                vm.ptCust_GrandTotalObj[container_ID] = {};
                                vm.ptCust_GrandTotalObj[container_ID]['grandTotal'] = {};
                                vm.ptCust_GrandTotalObj[container_ID]['grandTotal']['row'] = vm.pivotCust_GrandTotalObject[container_ID]['grandTotal']['row'];
                                vm.ptCust_GrandTotalObj[container_ID]['grandTotal']['column'] = vm.pivotCust_GrandTotalObject[container_ID]['grandTotal']['column'];

                                vm.ptCust_SubTotalObj[container_ID] = {};
                                vm.ptCust_SubTotalObj[container_ID]['subTotal'] = {};
                                vm.ptCust_SubTotalObj[container_ID]['subTotal']['row'] = vm.pivotCust_SubTotalObject[container_ID]['subTotal']['row'];
                                vm.ptCust_SubTotalObj[container_ID]['subTotal']['column'] = vm.pivotCust_SubTotalObject[container_ID]['subTotal']['column'];

                                vm.ptCust_RunningTotalObj[container_ID]['runningTotal']['row'] = vm.ptCust_RunningTotalObj[container_ID]['runningTotal']['row'];
                                vm.ptCust_RunningTotalObj[container_ID]['runningTotal']['column'] = vm.ptCust_RunningTotalObj[container_ID]['runningTotal']['column'];

                                vm.pt_Selected_Data_Attr = [];
                                vm.ptCust_DataObj = {};
                                vm.ptCust_DataObj[container_ID] = {};
                                vm.ptCust_DataObj[container_ID].data = {};
                                $.each(vm.pivotCust_DataObject[container_ID]['data'], function (k, v) {
                                    vm.pt_Selected_Data_Attr.push(k);
                                    vm.ptCust_DataObj[container_ID].data[k] = v;
                                });
                            }
                            $('#chartSetting').modal('show');

                        } else if (chartType == 'SpeedoMeter') {
                            $('#chartSetting').modal('hide');
                            $('#gaugeSetting').modal('show');

                            if (vm.DashboardModel.Dashboard.activeReport.axisConfig && vm.DashboardModel.Dashboard.activeReport.axisConfig.speedoMeterSettingObject != undefined) {
                                vm.speedoMeterSettingObj = {};
                                vm.speedoMeterSettingObj.lowRange = vm.DashboardModel.Dashboard.activeReport.axisConfig.speedoMeterSettingObject.lowRange;
                                vm.speedoMeterSettingObj.midRange = vm.DashboardModel.Dashboard.activeReport.axisConfig.speedoMeterSettingObject.midRange;
                                vm.speedoMeterSettingObj.highRange = vm.DashboardModel.Dashboard.activeReport.axisConfig.speedoMeterSettingObject.highRange;
                            } else {
                                vm.speedoMeterSettingObj = {};
                                vm.speedoMeterSettingObj.lowRange = "";
                                vm.speedoMeterSettingObj.midRange = "";
                                vm.speedoMeterSettingObj.highRange = "";
                            }
                        } else {
                            $('#chartSetting').modal('show');
                            if (!$scope.$$phase) {
                                $scope.$apply();
                            }
                            vm.Attributes.tableColumnOrder = [];
                            $.each(vm.Attributes.checkboxModelMeasure, function (key, value) {
                                vm.Attributes.tableColumnOrder.push(value);
                            });
                            $.each(vm.Attributes.checkboxModelDimension, function (key, value) {
                                vm.Attributes.tableColumnOrder.push(value);
                            });
                            vm.tooltipSelector = [];
                            if (vm.Attributes && vm.Attributes.dataFormate && vm.Attributes.dataFormate.tooltipSelector.length) {
                                vm.tooltipSelector = vm.Attributes.dataFormate.tooltipSelector;
                            }
                            vm.Attributes['tooltipSelector'] = vm.tooltipSelector;
                            /*if((vm.Attributes.setting  && vm.Attributes.setting.tableColumnOrder)){
                                  vm.Attributes.setting.tableColumnOrder.forEach(function(d){
                                  vm.Attributes.tableColumnOrder
                                  });
                                  }*/

                            vm.tableSettingCheckbox = angular.copy(vm.Attributes.checkboxModelMeasure);
                            vm.chartSettingObject = {};
                            if (vm.Attributes.dataFormate) {
                                if (vm.Attributes.dataFormate && vm.Attributes.dataFormate.xAxisCustomFormat) {
                                    vm.chartSettingObject.xAxisCustomDate = vm.Attributes.dataFormate.xAxisCustomFormat;
                                    vm.customDate = true;
                                } else {
                                    vm.customDate = false;
                                }
                                vm.settingChartType = chartType;
                                vm.chartSettingObject.xAxisDataFormate = vm.Attributes.dataFormate.xAxis;
                                vm.chartSettingObject.yAxisDataFormate = vm.Attributes.dataFormate.yAxis;
                                if (vm.Attributes.dataFormate.tooltip) {
                                    vm.chartSettingObject.toolTipFormate = vm.Attributes.dataFormate.tooltip;
                                } else {
                                    vm.chartSettingObject.toolTipFormate = "{title}\n{xLabel}:{yLabel}";
                                }
                                vm.chartSettingObject.groupColor = vm.Attributes.dataFormate.groupColor;
                                vm.chartSettingObject.fontSize = vm.Attributes.dataFormate.fontSize;
                                vm.chartSettingObject.topN = vm.Attributes.dataFormate.topN;
                                vm.chartSettingObject.topNOther = vm.Attributes.dataFormate.topNOther;
                                if (vm.Attributes.dataFormate.canvas != undefined && vm.Attributes.dataFormate.canvas.height != undefined) {
                                    vm.chartSettingObject.canvas = {};
                                    vm.chartSettingObject.canvas.height = vm.Attributes.dataFormate.canvas.height;
                                    vm.chartSettingObject.canvas.width = vm.Attributes.dataFormate.canvas.width;
                                }
                                if (vm.Attributes.dataFormate.chartType) {
                                    vm.chartTypeLineBar = vm.Attributes.dataFormate.chartType;
                                } else {
                                    vm.chartTypeLineBar = {};
                                }
                            } else {
                                vm.settingChartType = chartType;
                                vm.chartSettingObject.toolTipFormate = "{title}\n{xLabel}:{yLabel}"; //tooltip Formate
                                vm.chartSettingObject.fontSize = 13; //Default font size
                            }
                            vm.numberStyle = {};
                            if (vm.Attributes.numberStyle) {
                                vm.numberStyle.fontSize = vm.Attributes.numberStyle.fontSize;
                                vm.numberStyle.fontColor = vm.Attributes.numberStyle.fontColor;
                            } else {
                                vm.numberStyle.fontSize = 13;
                                vm.numberStyle.fontColor = "#757575";
                            }
                            setTimeout(function () {
                                $(".sortable").sortable({
                                    cancel: ".fixed",
                                    stop: function (event, ui) {
                                        vm.tableColumnOrder = $(this).sortable("toArray");
                                    }
                                });
                            }, 100);
                        }

                        vm.tableHeader = [];
                        setTimeout(function () {
                            /*$('#header-group').multiSelect({
                                  afterSelect: function (values) {
                                  vm.tableHeader.push(values[0]);
                                  },
                                  afterDeselect: function (values) {
                                  var index = $scope.tableHeader.indexOf(values[0]);
                                  if (index != -1) {
                                  $scope.tableHeader.splice(index, 1);
                                  }
                                  }
                                  });*/
                            $("#header-group").selectpicker();
                        }, 1);

                    }
                    vm.saveDataFormate = function (chartSetting) {
                        if (vm.settingChartType == 'Pivot Table Customized') {
                            vm.pivotCust_ShowTable(vm.DashboardModel.Dashboard.activeReport.reportContainer.id);
                        } else {
                            var dimensionType;
                            Object.keys(vm.Attributes.checkboxModelDimension).forEach(function (d) {
                                dimensionType = vm.Attributes.checkboxModelDimension[d].columType
                            });
                            var aggregate = ['sum', 'count', 'avg'];
                            var tooltipFormat = $("#tooltip").val();
                            aggregate.forEach(function (d) {
                                if (vm.Attributes.tooltipSelector && vm.Attributes.tooltipSelector.length > 0) {
                                    vm.Attributes.tooltipSelector.forEach(function (p) {
                                        if (tooltipFormat.includes(d + "(" + p.reName + ")")) {
                                            p.aggregate = d;
                                        }
                                    });
                                }
                            });
                            vm.Attributes['dataFormate'] = {};
                            if (chartSetting.xAxisDataFormate == "custom") {
                                vm.Attributes['dataFormate']['xAxis'] = chartSetting.xAxisDataFormate;
                                vm.Attributes['dataFormate']['xAxisCustomFormat'] = chartSetting.xAxisCustomDate;
                            } else {
                                vm.Attributes['dataFormate']['xAxis'] = chartSetting.xAxisDataFormate;
                            }
                            vm.Attributes['dataFormate']['xAxis'] = chartSetting.xAxisDataFormate;
                            vm.Attributes['dataFormate']['yAxis'] = chartSetting.yAxisDataFormate;
                            vm.Attributes['dataFormate']['tooltip'] = chartSetting.toolTipFormate;
                            vm.Attributes['dataFormate']['fontSize'] = chartSetting.fontSize;
                            vm.Attributes['dataFormate']['topN'] = chartSetting.topN;
                            vm.Attributes['dataFormate']['topNOther'] = chartSetting.topNOther;
                            vm.Attributes['dataFormate']['groupColor'] = chartSetting.groupColor;
                            vm.Attributes['dataFormate']['fontFamily'] = chartSetting.fontFamily;
                            vm.Attributes['dataFormate']['chartType'] = vm.chartTypeLineBar;
                            vm.Attributes['dataFormate']['tooltipSelector'] = vm.Attributes.tooltipSelector;

                            if (chartSetting.canvas) {
                                vm.Attributes['dataFormate']['canvas'] = {};
                                if (chartSetting.canvas.height == '' && chartSetting.canvas.width == '') {
                                    delete vm.Attributes['dataFormate']['canvas']['height'];
                                    delete vm.Attributes['dataFormate']['canvas']['width'];
                                } else {
                                    vm.Attributes['dataFormate']['canvas']['height'] = chartSetting.canvas.height;
                                    vm.Attributes['dataFormate']['canvas']['width'] = chartSetting.canvas.width;
                                }
                                /*vm.Attributes['dataFormate']['canvas']['width']=chartSetting.canvas.width;*/
                            }

                            var activeData = {};
                            if (!$.isEmptyObject(vm.filteredData)) {
                                activeData = vm.filteredData;
                            } else {
                                activeData = vm.tableData;
                            }
                            if (vm.dataCount < $rootScope.localDataLimit) {
                                sketch
                                    .axisConfig(vm.Attributes)
                                    .data(activeData)
                                    .container(vm.DashboardModel.Dashboard.activeReport.reportContainer.id)
                                    .chartConfig($scope.selectedChartInView[vm.DashboardModel.Dashboard.activeReport.reportContainer.id])
                                    .render(
                                        function (drawn) {
                                            $scope.lastDrawnChart[vm.DashboardModel.Dashboard.activeReport.reportContainer.id] = vm.DashboardModel.Dashboard.activeReport.chart.key;
                                        });
                            } else {
                                sketchServer
                                    .accessToken($rootScope.accessToken)
                                    .axisConfig(vm.Attributes)
                                    .data([])
                                    .container(vm.DashboardModel.Dashboard.activeReport.reportContainer.id)
                                    .chartConfig($scope.selectedChartInView[vm.DashboardModel.Dashboard.activeReport.reportContainer.id])
                                    .render(
                                        function (drawn) {
                                            $scope.lastDrawnChart[vm.DashboardModel.Dashboard.activeReport.reportContainer.id] = vm.DashboardModel.Dashboard.activeReport.chart.key;
                                        });
                            }
                        }
                        $('#chartSetting').modal('hide');
                    }
                    //Get array list
                    vm.getArrayList = function (numberColumn) {
                        var numberArray = [];
                        for (var i = 1; i <= numberColumn; i++) {
                            numberArray.push(i);
                        }
                        return numberArray;
                    }
                    //Range picker add
                    vm.saveRangedatepicker = function (columnName, numberOfColumn, dateType) {

                        var tempObject = {};
                        vm.rangeObject = {};

                        if (numberOfColumn) {
                            var ColumnName = JSON.parse(columnName);

                            tempObject['key'] = ColumnName;
                            tempObject['period'] = [];
                            for (var i = 0; i < numberOfColumn; i++) {
                                var d = new Date();
                                var n = d.getFullYear();
                                var year = n - i;
                                var dateObject = {
                                    "start": year + "-01-01",
                                    "end": year + "-12-31"
                                };
                                tempObject['period'].push(dateObject);
                            }

                            setTimeout(function () {
                                for (var i = 0; i < numberOfColumn; i++) {
                                    var d = new Date();
                                    var n = d.getFullYear();
                                    var year = n - i;
                                    var setStartDate = '01-01-' + year;
                                    var setEndDate = '31-12-' + year;
                                    $('.rangeStartFilter' + i).val(setStartDate);
                                    $('.rangeEndFilter' + i).val(setEndDate);
                                }
                                var start = $('.dateTimeLineStart').datepicker({
                                    dateFormat: 'dd-mm-yy',
                                    onSelect: function (e) {
                                        var setIndex = vm.filterColumnRange.index;
                                        var startDate = $(start[setIndex]).val();
                                        var endDate = $(end[setIndex]).val();

                                        var tempStart = startDate; //31-12-2017
                                        var tempEnd = endDate;
                                        startDate = tempStart.split("-").reverse().join("-");
                                        endDate = tempEnd.split("-").reverse().join("-");

                                        rangeFilterChange(startDate, endDate);
                                    }
                                });
                                var end = $('.dateTimeLineEnd').datepicker({
                                    dateFormat: 'dd-mm-yy',
                                    onSelect: function (e) {
                                        var setIndex = vm.filterColumnRange.index;
                                        var startDate = $(start[setIndex]).val();
                                        var endDate = $(end[setIndex]).val();

                                        var tempStart = startDate; //31-12-2017
                                        var tempEnd = endDate;

                                        startDate = tempStart.split("-").reverse().join("-");
                                        endDate = tempEnd.split("-").reverse().join("-");

                                        rangeFilterChange(startDate, endDate);
                                    }
                                });
                                /*start.on("change", function (e) {
                                         var setIndex = vm.filterColumnRange.index;
                                         var startDate = $(start[setIndex]).val();
                                         var endDate = $(end[setIndex]).val();

                                         var tempStart = startDate; //31-12-2017
                                         var tempEnd = endDate;
                                         startDate = tempStart.split("-").reverse().join("-");
                                         endDate = tempEnd.split("-").reverse().join("-");

                                         rangeFilterChange(startDate,endDate);
                                     });*/

                                /*end.on("change", function (e) {
                                         var setIndex = vm.filterColumnRange.index;
                                         var startDate = $(start[setIndex]).val();
                                         var endDate = $(end[setIndex]).val();

                                         var tempStart = startDate; //31-12-2017
                                         var tempEnd = endDate;

                                         startDate = tempStart.split("-").reverse().join("-");
                                         endDate = tempEnd.split("-").reverse().join("-");

                                         rangeFilterChange(startDate,endDate);
                                     });*/


                                function rangeFilterChange(startDate, endDate) {
                                    var date = {};
                                    date['start'] = (moment(startDate)).format('YYYY-MM-DD');
                                    date['end'] = (moment(endDate)).format('YYYY-MM-DD');

                                    var columnName = $scope.filterColumnName;

                                    //Date

                                    var columnName = $scope.filterColumnRange.columnName;
                                    var index = $scope.filterColumnRange.index;

                                    vm.rangeObject[columnName].period[index] = date;

                                    //End
                                    //vm.DashboardView.renderChartInActiveContainer();
                                    if (!$.isEmptyObject(vm.rangeObject)) {

                                        vm.Attributes['timeline'] = vm.rangeObject;
                                        var timelineObj = vm.rangeObject;
                                        sketch.getTimeLineFilteredData(vm.tableData, vm.rangeObject, function (data) {
                                            vm.filteredData = data;

                                            vm.DashboardModel.Dashboard.Report.forEach(function (d) {
                                                vm.Attributes = d.axisConfig;
                                                d.axisConfig['timeline'] == vm.rangeObject;
                                                sketch
                                                    .axisConfig(d.axisConfig)
                                                    .data(data)
                                                    .container(d.reportContainer.id)
                                                    .chartConfig(d.chart)
                                                    .render();
                                            });
                                        });
                                    }
                                }

                            }, 1000);

                            tempObject['numberOfColumn'] = numberOfColumn;
                            tempObject['dateType'] = dateType;
                            vm.rangeObject[ColumnName.reName] = tempObject;
                            var rawData = angular.copy(vm.tableData);
                            if (!$.isEmptyObject(vm.rangeObject)) {
                                vm.Attributes['timeline'] = vm.rangeObject;
                                sketch.getTimeLineFilteredData(rawData, vm.rangeObject, function (data) {
                                    vm.filteredData = data;
                                    vm.Attributes['timeline'] = vm.rangeObject;
                                    vm.DashboardModel.Dashboard.Report.forEach(function (d) {
                                        vm.Attributes = d.axisConfig;
                                        d.axisConfig['timeline'] == vm.rangeObject;
                                        sketch
                                            .axisConfig(d.axisConfig)
                                            .data(data)
                                            .container(d.reportContainer.id)
                                            .chartConfig(d.chart)
                                            .render();
                                    });

                                });
                            }
                            setTimeout(function () {
                                /*$('.dateTimeLine').daterangepicker(
                                      {
                                      locale: {
                                      format: 'DD-MM-YYYY'
                                      }
                                      },
                                      function (start, end, label) {
                                      var date = {};
                                      date['start'] = start.format('YYYY-MM-DD');
                                      date['end'] = end.format('YYYY-MM-DD');
                                      var columnName = $scope.filterColumnName;
                                      //Date

                                      var columnName = $scope.filterColumnRange.columnName;
                                      var index = $scope.filterColumnRange.index;
                                      vm.rangeObject[columnName].period[index] = date;
                                      //End
                                      //vm.DashboardView.renderChartInActiveContainer();


                                      if (!$.isEmptyObject(vm.rangeObject)) {
                                      vm.Attributes['timeline'] = vm.rangeObject;

                                      var timelineObj = vm.rangeObject;
                                      sketch.getTimeLineFilteredData(vm.tableData, vm.rangeObject, function (data) {
                                      vm.filteredData = data;

                                      vm.DashboardModel.Dashboard.Report.forEach(function (d) {
                                      vm.Attributes = d.axisConfig;
                                      d.axisConfig['timeline'] == vm.rangeObject;
                                      sketch
                                      .axisConfig(d.axisConfig)
                                      .data(data)
                                      .container(d.reportContainer.id)
                                      .chartConfig(d.chart)
                                      .render();
                                      });
                                      });
                                      }
                                      //sketch.applyFilteralert((date,columnName);
                                      });*/
                            }, 500);
                            $("#dateModal").modal('hide');
                        } else {
                            sketch._timeline = {};
                            vm.DashboardModel.Dashboard.Report.forEach(function (d) {
                                delete d.axisConfig.timeline;
                                sketch
                                    .axisConfig(d.axisConfig)
                                    .data(vm.tableData)
                                    .container(d.reportContainer.id)
                                    .chartConfig(d.chart)
                                    .render();
                            });
                        }
                    }

                    // Create Column Calculation

                    // Create Column Calculation
                    vm.columnCalculation = function () {
                        vm.flagCheck = 0;
                        vm.errorCalculation = "";
                        vm.descriptionText = "";
                        vm.calObject = {};
                        setTimeout(function () {
                            vm.calObject.dataKey = "Measure";
                        }, 1);
                        $('#calculationMeasure').modal({
                            backdrop: 'static',
                            keyboard: false
                        });
                        $('#calculationMeasure').modal('show');
                        $(".reportDashboard").addClass("background-container");
                    }

                    //Calculation edit
                    function escapeRegExp(text) {
                        return text.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, '\\$&');
                    }

                    vm.columnCalculationEdit = function (measure, index) {
                        vm.lastNameOfColumn = measure.reName;
                        var formula=measure.formula;
                        if (measure && measure.formulaObj) {
                            var tempCheck = {};
                            measure.formulaObj.forEach(function (d) {
                                if (d.reName != d.columnName && tempCheck[d.columnName] == undefined) {
                                    tempCheck[d.columnName] = true;

                                    var tempcolumnName = escapeRegExp("[" + d.columnName + "]");

                                    var tempRegex = new RegExp(tempcolumnName, "g");

                                    formula = (measure.formula).replace(tempRegex, '[' + d.reName + ']');
                                }
                            });
                        }
                        vm.calCurrentEditObj = measure;
                        vm.flagCheck = 1;
                        vm.calObject = {};
                        vm.errorCalculation = "";
                        vm.descriptionText = "";
                        vm.calObject.fieldName = measure.reName;
                        vm.calObject.measureFormula = formula;
                        vm.calObject.dataKey = measure.dataKey;
                        $('#calculationMeasure').modal({
                            backdrop: 'static',
                            keyboard: false
                        });
                        $('#calculationMeasure').modal('show');
                        $(".reportDashboard").addClass("background-container");
                    }
                    // Operators define
                    vm.operators = sketch.operatorsList;
                    // Filter


                    // Filter
                    vm.filterAdd = function () {
                        if (vm.dataCount < $rootScope.localDataLimit) {
                            //Reset All Chart
                            eChart.resetAll();
                            //ENd
                            vm.filerTemp = [];
                            vm.filtersToApply = [];
                            vm.filterHSApply = true;
                            var columnObj = "";
                            if (vm.checkboxModel != undefined && vm.checkboxModel != "{}") {
                                Object.keys(vm.checkboxModel).forEach(function (d) {
                                    columnObj = JSON.parse(vm.checkboxModel[d]);
                                    var keyValue = {};
                                    keyValue['key'] = columnObj.columnName;
                                    keyValue['value'] = columnObj.columType;
                                    vm.filerTemp.push(keyValue);

                                    vm.filtersTo[columnObj.columnName] = [];
                                    Enumerable.From(vm.tableData)
                                        .Distinct(function (x) {
                                            return x[columnObj.columnName];
                                        })
                                        .Select(function (x) {
                                            return x[columnObj.columnName];
                                        })
                                        .ToArray()
                                        .forEach(function (e) {
                                            vm.filtersTo[columnObj.columnName].push(e);
                                        });
                                });
                            }

                            setTimeout(function () {
                                $(".filterSelect").selectpicker();

                                function startChange(startDate, endDate) {
                                    var startDate = (moment(startDate)).format('YYYY-MM-DD');
                                    var endDate = (moment(endDate)).format('YYYY-MM-DD');
                                    if (!endDate) {
                                        endDate = new Date();
                                        endDate = (moment(endDate)).format('YYYY-MM-DD');
                                    }
                                    var date = {};
                                    date['start'] = startDate;
                                    date['end'] = endDate;
                                    var columnName = $scope.filterColumnName;

                                    if (vm.dataCount < $rootScope.localDataLimit) {
                                        sketch.applyFilter(date, columnName);
                                    } else {
                                        sketchServer.applyFilter(date, columnName, vm.metadataId);
                                    }
                                }

                                function endChange(startDate, endDate) {

                                    var startDate = (moment(startDate)).format('YYYY-MM-DD');
                                    var endDate = (moment(endDate)).format('YYYY-MM-DD');
                                    if (!endDate) {
                                        endDate = new Date();
                                        endDate = (moment(endDate)).format('YYYY-MM-DD');
                                    }

                                    var date = {};
                                    date['start'] = startDate;
                                    date['end'] = endDate;

                                    var columnName = $scope.filterColumnName;
                                    if (vm.dataCount < $rootScope.localDataLimit) {
                                        sketch.applyFiltuer(date, columnName);
                                    } else {
                                        sketchServer.applyFilter(date, columnName, vm.metadataId);
                                    }

                                }


                                var setDate = new Date();
                                var year = setDate.getFullYear();
                                var setStartDate = '01-01-' + year;
                                var setEndDate = '31-12-' + year;
                                var start = $('.startdate').datepicker({
                                    dateFormat: 'dd-mm-yy',
                                    onSelect: function (e) {
                                        var tempIndex = (start.length) - 1;
                                        var startDate = $(start[tempIndex]).val();
                                        var endDate = $(end[tempIndex]).val();
                                        var tempStart = startDate; //31-12-2017
                                        var tempEnd = endDate;
                                        startDate = tempStart.split("-").reverse().join("-");
                                        endDate = tempEnd.split("-").reverse().join("-");
                                        startChange(startDate, endDate);
                                    }
                                });
                                var end = $('.enddate').datepicker({
                                    dateFormat: 'dd-mm-yy',
                                    onSelect: function (e) {
                                        var tempIndex = (end.length) - 1;
                                        var startDate = $(start[tempIndex]).val();
                                        var endDate = $(end[tempIndex]).val();
                                        var tempStart = startDate; //31-12-2017
                                        var tempEnd = endDate;
                                        startDate = tempStart.split("-").reverse().join("-");
                                        endDate = tempEnd.split("-").reverse().join("-");
                                        endChange(startDate, endDate);
                                    }
                                });
                                $('.startdate').val(setStartDate);
                                $('.enddate').val(setEndDate);


                                /*start.on("change", function (e) {
                                         var tempIndex = (start.length) - 1;
                                         var startDate = $(start[tempIndex]).val();
                                         var endDate = $(end[tempIndex]).val();
                                         var tempStart = startDate; //31-12-2017
                                         var tempEnd = endDate;
                                         startDate = tempStart.split("-").reverse().join("-");
                                         endDate = tempEnd.split("-").reverse().join("-");

                                         startChange(startDate, endDate);
                                     });*/

                                /*end.on("change", function (e) {
                                         var tempIndex = (end.length) - 1;
                                         var startDate = $(start[tempIndex]).val();
                                         var endDate = $(end[tempIndex]).val();
                                         var tempStart = startDate; //31-12-2017
                                         var tempEnd = endDate;
                                         startDate = tempStart.split("-").reverse().join("-");
                                         endDate = tempEnd.split("-").reverse().join("-");
                                         endChange(startDate, endDate);
                                     });*/
                                if (vm.checkboxModel != undefined && vm.checkboxModel != "{}") {
                                    Object.keys(vm.checkboxModel).forEach(function (d) {
                                        columnObj = JSON.parse(vm.checkboxModel[d]);
                                        if (columnObj.columType == "int" || filterObj.value=="bigint" || columnObj.columType == "float" || columnObj.columType == "decimal" || columnObj.columType == "double") {
                                            var minObject = _.min(vm.tableData, function (o) {
                                                return o[columnObj.columnName];
                                            });
                                            var maxObject = _.max(vm.tableData, function (o) {
                                                return o[columnObj.columnName];
                                            });
                                            var min = minObject[columnObj.columnName];
                                            var max = maxObject[columnObj.columnName];
                                            var columnObject = {};
                                            columnObject['key'] = columnObj.columnName;
                                            columnObject['value'] = columnObj.columType;
                                            if (min == null) {
                                                min = 0;
                                            }
                                            // add the slider object to the slider element
                                            // using data for future reference
                                            /* noUiSlideur.create($("#"+columnObj.columnName.replace("(","\\(").replace(")","\\)")+ "-Filter")[0], {
                                                  animate: true,
                                                  start: [min, max] ,
                                                  connect: true,
                                                  range: {
                                                  min: parseInt(min),
                                                  max: parseInt(max)
                                                  },
                                                  step: 1,
                                                  tooltips: true
                                                  });*/
                                            setTimeout(function () {
                                                var nonLinearSlider = document.getElementById(columnObj.columnName.replace("(", "(").replace(")", ")") + "-Filter");
                                                noUiSlider.create(nonLinearSlider, {
                                                    animate: true,
                                                    start: [min, max],
                                                    connect: true,
                                                    range: {
                                                        min: parseInt(min),
                                                        max: parseInt(max)
                                                    },
                                                    step: 1,
                                                    tooltips: [wNumb({
                                                        decimals: 0
                                                    }), wNumb({
                                                        decimals: 0
                                                    })]
                                                });
                                                nonLinearSlider.noUiSlider.on('update', function (values, handle) {
                                                    sketch.applyFilter(values, columnObject);
                                                });
                                            }, 1000);

                                            /* $("#"+columnObj.columnName.replace("(","\\(").replace(")","\\)")+ "-Filter").noUiSlider({
                                                  animate: true,
                                                  start: [min, max] ,
                                                  connect: true,
                                                  range: {
                                                  min: parseInt(min),
                                                  max: parseInt(max)
                                                  },
                                                  step: 1,
                                                  pips: { // Show a scale with the slider
                                                  mode: 'steps',
                                                  stepped: true,
                                                  density: 4
                                                  },
                                                  tooltips: true,
                                                  }).on('slide', function(){
                                                  sketch.applyFilter($(this).val(),columnObject);
                                                  });*/
                                            /*$("#"+columnObj.columnName.replace("(","\\(").replace(")","\\)")+ "-Filter").ionRangeSlider({
                                                  type: "double",
                                                  grid: true,
                                                  min: min,
                                                  max: max,
                                                  from: min,
                                                  step: 1,
                                                  id: columnObject,
                                                  onFinish: function (data) {
                                                  sketch.applyFilter(data, this.id);
                                                  }
                                                  });*/
                                        }
                                    });
                                }

                            }, 1000);
                        } else {
                            /*
                                  * Server filter
                                  */
                            /*
                                  * For server side filter
                                  */
                            //Reset All Chart
                            var p = Promise.resolve();
                            eChartServer.resetAll(vm.metadataId);
                            //ENd
                            vm.filerTemp = [];
                            vm.filtersToApply = [];
                            vm.filterHSApply = true;
                            var promiseArray = [];
                            vm.allFilters = {};
                            if (vm.checkboxModel && vm.checkboxModel.length) {
                                Object.keys(vm.checkboxModel).forEach(function (d,index) {
                                    // var columnObj = JSON.parse(vm.checkboxModel[d]);
                                    try {
                                        var columnObj = JSON.parse(vm.checkboxModel[d]);
                                    } catch (e) {
                                        var columnObj = vm.checkboxModel[d];
                                    }

                                    vm.allFilters[columnObj.columnName] = {};
                                    var keyValue = {};
                                    keyValue['key'] = columnObj.columnName;
                                    keyValue['value'] = columnObj.columType;
                                    keyValue['cascade'] = true;
                                    vm.filerTemp.push(keyValue);
                                    sketchServer.updateFilters(vm.filerTemp);
                                    vm.filtersTo[columnObj.columnName] = [];
                                    p = p.then(new Promise(function (resolve) {
                                        var data = {
                                            columnObject: columnObj,
                                            metadataId: vm.metadataId,
                                            sessionId: $rootScope.accessToken
                                        };
                                        dataFactory.nodeRequest('getUniqueDataFieldsOfColumn', 'post', data).then(function (data) {
                                            vm.filtersTo[columnObj.columnName] = data;
                                            vm.allFilters[columnObj.columnName]['columnObj'] = keyValue;
                                            vm.allFilters[columnObj.columnName]['filterValue'] = [];
                                            if (keyValue.value != 'datetime' && keyValue.value != 'date')
                                                vm.allFilters[columnObj.columnName]['filterValue'] = data;
                                            resolve();
                                            $scope.$apply();
                                            setTimeout(function () {


                                                /*
                                                           For date change
                                                          */
                                                function startChangeServer(startDate, endDate) {
                                                    var startDate = (moment(startDate)).format('YYYY-MM-DD');
                                                    var endDate = (moment(endDate)).format('YYYY-MM-DD');
                                                    if (!endDate) {
                                                        endDate = new Date();
                                                        endDate = (moment(endDate)).format('YYYY-MM-DD');
                                                    }
                                                    var date = {};
                                                    date['start'] = startDate;
                                                    date['end'] = endDate;
                                                    var columnName = $scope.filterColumnName;
                                                    if (vm.dataCount < $rootScope.localDataLimit) {
                                                        sketch.applyFilter(date, columnName, vm.metadataId);
                                                    } else {
                                                        sketchServer.applyFilter(date, columnName, vm.metadataId, function (data, filterObj, status) {
                                                            if (status) {

                                                                $("[id^=filterSelect_]").each(function () {
                                                                    $(this).selectpicker('destroy', true);
                                                                })
                                                                vm.filtersTo[filterObj.key] = data;
                                                                $scope.$apply();
                                                                setTimeout(function () {
                                                                    $("[id^=filterSelect_]").each(function () {
                                                                        $(this).selectpicker();
                                                                    })
                                                                }, 1000);
                                                            }
                                                        });
                                                    }
                                                }

                                                function endChangeServer(startDate, endDate) {
                                                    var startDate = (moment(startDate)).format('YYYY-MM-DD');
                                                    var endDate = (moment(endDate)).format('YYYY-MM-DD');
                                                    if (!endDate) {
                                                        endDate = new Date();
                                                        endDate = (moment(endDate)).format('YYYY-MM-DD');
                                                    }
                                                    var date = {};
                                                    date['start'] = startDate;
                                                    date['end'] = endDate;
                                                    var columnName = $scope.filterColumnName;
                                                    if (vm.dataCount < $rootScope.localDataLimit) {
                                                        sketch.applyFilter(date, columnName, vm.metadataId);
                                                    } else {
                                                        sketchServer.applyFilter(date, columnName, vm.metadataId, function (data, filterObj, status) {
                                                            if (status) {

                                                                $("[id^=filterSelect_]").each(function () {
                                                                    $(this).selectpicker('destroy', true);
                                                                })
                                                                vm.filtersTo[filterObj.key] = data;
                                                                $scope.$apply();
                                                                setTimeout(function () {
                                                                    $("[id^=filterSelect_]").each(function () {
                                                                        $(this).selectpicker();
                                                                    })
                                                                }, 1000);
                                                            }
                                                        });
                                                    }
                                                }

                                                /*
                                                         FOr date time function
                                                          */
                                                function startTimeChangeServer(startDate, endDate) {
                                                    var startDate = (moment(startDate)).format('YYYY-MM-DD HH:mm');
                                                    var endDate = (moment(endDate)).format('YYYY-MM-DD HH:mm');

                                                    if (!endDate) {
                                                        endDate = new Date();
                                                        endDate = (moment(endDate)).format('YYYY-MM-DD  HH:mm');
                                                    }
                                                    var date = {};
                                                    date['start'] = startDate;
                                                    date['end'] = endDate;
                                                    var columnName = $scope.filterColumnName;
                                                    if (vm.dataCount < $rootScope.localDataLimit) {
                                                        sketch.applyFilter(date, columnName, vm.metadataId);
                                                    } else {
                                                        sketchServer.applyFilter(date, columnName, vm.metadataId, function (data, filterObj, status) {
                                                            if (status) {

                                                                $("[id^=filterSelect_]").each(function () {
                                                                    $(this).selectpicker('destroy', true);
                                                                })
                                                                vm.filtersTo[filterObj.key] = data;
                                                                $scope.$apply();
                                                                setTimeout(function () {
                                                                    $("[id^=filterSelect_]").each(function () {
                                                                        $(this).selectpicker();
                                                                    })
                                                                }, 1000);
                                                            }
                                                        });
                                                    }
                                                }

                                                function endTimeChangeServer(startDate, endDate) {
                                                    var startDate = (moment(startDate)).format('YYYY-MM-DD HH:mm');
                                                    var endDate = (moment(endDate)).format('YYYY-MM-DD HH:mm');
                                                    if (!endDate) {
                                                        endDate = new Date();
                                                        endDate = (moment(endDate)).format('YYYY-MM-DD HH:mm');
                                                    }
                                                    var date = {};
                                                    date['start'] = startDate;
                                                    date['end'] = endDate;
                                                    var columnName = $scope.filterColumnName;
                                                    if (vm.dataCount < $rootScope.localDataLimit) {
                                                        sketch.applyFilter(date, columnName, vm.metadataId);
                                                    } else {
                                                        sketchServer.applyFilter(date, columnName, vm.metadataId, function (data, filterObj, status) {
                                                            if (status) {
                                                                $("[id^=filterSelect_]").each(function () {
                                                                    $(this).selectpicker('destroy', true);
                                                                })
                                                                vm.filtersTo[filterObj.key] = data;
                                                                $scope.$apply();
                                                                setTimeout(function () {
                                                                    $("[id^=filterSelect_]").each(function () {
                                                                        $(this).selectpicker();
                                                                    })
                                                                }, 1000);
                                                            }
                                                        });
                                                    }
                                                }

                                                if (columnObj.columType == "int" || filterObj.value=="bigint" || columnObj.columType == "float" || columnObj.columType == "decimal" || columnObj.columType == "double") {
                                                    var min = Math.min.apply(Math, data);
                                                    var max = Math.max.apply(Math, data);
                                                    if (min && max) {
                                                        var columnObject = {};
                                                        columnObject['key'] = columnObj.columnName;
                                                        columnObject['value'] = columnObj.columType;
                                                        if (min == null) {
                                                            min = 0;
                                                        }
                                                        var nonLinearSlider = document.getElementById(columnObj.columnName.replace("(", "(").replace(")", ")") + "-Filter");
                                                        noUiSlider.create(nonLinearSlider, {
                                                            animate: true,
                                                            start: [min, max],
                                                            connect: true,
                                                            range: {
                                                                min: parseInt(min),
                                                                max: parseInt(max)
                                                            },
                                                            step: 1,
                                                            tooltips: [wNumb({
                                                                decimals: 0
                                                            }), wNumb({
                                                                decimals: 0
                                                            })]
                                                        });
                                                        vm.allFilters[columnObject.key]['filterValue'] = [min, max];
                                                        nonLinearSlider.noUiSlider.on('update', function (values, handle) {
                                                            // sketchServer.accessToken($rootScope.accessToken).applyFilter(value, columnObject.key, vm.metadataId,function(data,filterObj,status){
                                                            //     if(status){
                                                            //         $('.filterSelect').selectpicker('destroy', true);
                                                            //         vm.filtersTo[filterObj.key]=data;
                                                            //         $scope.$apply();
                                                            //         setTimeout(function(){
                                                            //             $('.filterSelect').selectpicker();
                                                            //         },1000);
                                                            //     }
                                                            // });
                                                            vm.allFilters[columnObject.key]['filterValue'] = values;
                                                        });
                                                    }
                                                } else if (columnObj.columType == "date") {
                                                    var setDate = new Date();
                                                    var year = setDate.getFullYear();
                                                    var setStartDate = '01-01-' + year;
                                                    var setEndDate = '31-12-' + year;
                                                    var today = new Date();
                                                    var date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
                                                    // var time = today.getHours() + ":" + today.getMinutes();
                                                    var CurrentDateTime = date;
                                                    vm.allFilters[columnObj.columnName]['filterValue'] = {};
                                                    vm.allFilters[columnObj.columnName]['filterValue']['start'] = CurrentDateTime;
                                                    vm.allFilters[columnObj.columnName]['filterValue']['end'] = CurrentDateTime;

                                                    var rangeStartDate = $('.startdate').datepicker({
                                                        dateFormat: 'dd-mm-yy',
                                                        onSelect: function (e) {
                                                            var d = new Date(e);
                                                            var tempIndex = (rangeEndDate.length) - 1;
                                                            var endDate = $(rangeStartDate[tempIndex]).val();
                                                            var startDate = e.substr(6) + e.substr(2, 4) + e.substr(0, 2);
                                                            var temp = endDate; //31-12-2017
                                                            endDate = temp.split("-").reverse().join("-");
                                                            vm.allFilters[$scope.filterColumnName.key]['filterValue']['start'] = startDate;
                                                            //setDateFilter(startDate,endDate);
                                                        }
                                                    });
                                                    var rangeEndDate = $('.enddate').datepicker({
                                                        dateFormat: 'dd-mm-yy',
                                                        onSelect: function (e) {
                                                            var d = new Date(e);
                                                            var tempIndex = (rangeEndDate.length) - 1;
                                                            var startDate = $(rangeStartDate[tempIndex]).val();
                                                            var endDate = e.substr(6) + e.substr(2, 4) + e.substr(0, 2);
                                                            var temp = startDate; //31-12-2017
                                                            startDate = temp.split("-").reverse().join("-");
                                                            vm.allFilters[$scope.filterColumnName.key]['filterValue']['end'] = endDate;
                                                            //setDateFilter(startDate,endDate);
                                                        }
                                                    });


                                                    /*
                                                                                                            var start = $('.startdate').datepicker({
                                                                                                                dateFormat: 'dd-mm-yy',
                                                                                                                onSelect: function(e) {
                                                                                                                    var tempIndex = (start.length) - 1;
                                                                                                                    var startDate = $(start[tempIndex]).val();
                                                                                                                    var endDate = $(end[tempIndex]).val();
                                                                                                                    var tempStart = startDate; //31-12-2017
                                                                                                                    var tempEnd = endDate;
                                                                                                                    startDate = tempStart.split("-").reverse().join("-");
                                                                                                                    endDate = tempEnd.split("-").reverse().join("-");
                                                                                                                    //startChangeServer(startDate,endDate);
                                                                                                                    vm.allFilters[$scope.filterColumnName.key]['filterValue']['start']=startDate;
                                                                                                                    // vm.allFilters[$scope.filterColumnName.key]['filterValue']['end']=endDate;
                                                                                                                }
                                                                                                            });
                                                                                                            var end = $('.enddate').datepicker({
                                                                                                                dateFormat: 'dd-mm-yy',
                                                                                                                onSelect: function(e) {
                                                                                                                    var tempIndex = (end.length) - 1;
                                                                                                                    var startDate = $(start[tempIndex]).val();
                                                                                                                    var endDate = $(end[tempIndex]).val();
                                                                                                                    var tempStart = startDate; //31-12-2017
                                                                                                                    var tempEnd = endDate;
                                                                                                                    startDate = tempStart.split("-").reverse().join("-");
                                                                                                                    endDate = tempEnd.split("-").reverse().join("-");
                                                                                                                    // vm.allFilters[$scope.filterColumnName.key]['filterValue']['start']=startDate;
                                                                                                                    vm.allFilters[$scope.filterColumnName.key]['filterValue']['end']=endDate;
                                                                                                                    //endChangeServer(startDate,endDate);
                                                                                                                }
                                                                                                            });
                                                        */


                                                    $('.startdate').val(setStartDate);
                                                    $('.enddate').val(setEndDate);
                                                } else if (columnObj.columType == "datetime") {
                                                    var today = new Date();
                                                    var date = today.getDate() + '-' + (today.getMonth() + 1) + '-' + today.getFullYear();
                                                    var time = today.getHours() + ":" + today.getMinutes();
                                                    var CurrentDateTime = date + ' ' + time;
                                                    vm.allFilters[columnObj.columnName]['filterValue'] = {};
                                                    vm.allFilters[columnObj.columnName]['filterValue']['start'] = CurrentDateTime;
                                                    vm.allFilters[columnObj.columnName]['filterValue']['end'] = CurrentDateTime;
                                                    var start = $('.startdatetime').datetimepicker({
                                                        defaultDate: new Date(),
                                                        format: "DD-MM-YYYY HH:mm:ss"
                                                    }).on('dp.show', function (e) {
                                                        var ele = $(e.target).data('DateTimePicker');
                                                        //ele.widget.css('left', 10);
                                                    }).on('dp.change', function (e) {


                                                        var tempIndex = (start.length) - 1;
                                                        var startDate = $(start[tempIndex]).val();
                                                        var endDate = $(end[tempIndex]).val();
                                                        var startDateArr = startDate.split(" ");
                                                        var tempStartDate = startDateArr[0]; //31-12-2017
                                                        var tempStartTime = startDateArr[1];
                                                        var endDateArr = endDate.split(" ");
                                                        var tempEndDate = endDateArr[0]; //31-12-2017
                                                        var tempEndTime = endDateArr[1];
                                                        startDate = tempStartDate.split("-").reverse().join("-");
                                                        endDate = tempEndDate.split("-").reverse().join("-");
                                                        vm.allFilters[$scope.filterColumnName.key]['filterValue']['start'] = startDate + " " + tempStartTime;
                                                        vm.allFilters[$scope.filterColumnName.key]['filterValue']['end'] = endDate + " " + tempEndTime;
                                                    });
                                                    var end = $('.enddatetime').datetimepicker({
                                                        defaultDate: new Date(),
                                                        format: "DD-MM-YYYY HH:mm:ss"
                                                    }).on('dp.change', function (e) {
                                                        var tempIndex = (start.length) - 1;
                                                        var startDate = $(start[tempIndex]).val();
                                                        var endDate = $(end[tempIndex]).val();
                                                        var startDateArr = startDate.split(" ");
                                                        var tempStartDate = startDateArr[0]; //31-12-2017
                                                        var tempStartTime = startDateArr[1];
                                                        var endDateArr = endDate.split(" ");
                                                        var tempEndDate = endDateArr[0]; //31-12-2017
                                                        var tempEndTime = endDateArr[1];
                                                        startDate = tempStartDate.split("-").reverse().join("-");
                                                        endDate = tempEndDate.split("-").reverse().join("-");
                                                        vm.allFilters[$scope.filterColumnName.key]['filterValue']['start'] = startDate + " " + tempStartTime;
                                                        vm.allFilters[$scope.filterColumnName.key]['filterValue']['end'] = endDate + " " + tempEndTime;
                                                    });
                                                }else {
                                                    $("#filterSelect_"+index).selectpicker();
                                                }
                                                //});
                                            }, 1000);
                                        });
                                    }));
                                });
                            }
                        }

                    }
                    vm.cascadeDateFilter = function (date, columnName) {
                        date = date['filterValue'];
                        $(".loadingBar").show();
                        sketchServer.applyFilter(date, columnName, vm.metadataId, function (data, filterObj, status) {
                            if (status) {
                                $("[id^=filterSelect_]").each(function () {
                                    $(this).selectpicker('destroy', true);
                                })
                                vm.allFilters[filterObj.key]['filterValue'] = [];
                                vm.filtersTo[filterObj.key] = data;
                                vm.allFilters[filterObj.key]['filterValue'] = data;
                                $scope.$apply();
                                setTimeout(function () {
                                    $("[id^=filterSelect_]").each(function () {
                                        $(this).selectpicker();
                                    })
                                    $(".loadingBar").hide();
                                }, 1000);
                            } else {
                                setTimeout(function () {
                                    $(".loadingBar").hide();
                                }, 1000);
                            }
                        });
                    }
                    //Filter column Name
                    $scope.filterColumn = function (columnName) {
                        $scope.filterColumnName = columnName;
                    }
                    //Remove filter
                    vm.custFilterRemove = function (index) {
                        vm.filerTemp.splice(index, 1);
                    }
                    vm.timeLineDelete = function (rangePicker) {
                        vm.saveRangedatepicker(JSON.stringify(rangePicker.key), rangePicker.period.length - 1);
                    }
                    /*$scope.filterColumn = function (columnName) {

                          $scope.filterColumnName = columnName;
                          }*/
                    //Range Column and index get
                    $scope.filterRangeColumn = function (index, columnName) {

                        $scope.filterColumnRange = {};
                        $scope.filterColumnRange['index'] = index;
                        $scope.filterColumnRange['columnName'] = columnName;
                    }
                    $scope.functionList = sketch.calculateFunctionList;
                    // Save
                    vm.categoryError = "";// For Error
                    vm.categoryGroupObject = {};
                    var groupIndex = 0;
                    vm.createCateogrySave = function (categoryData, categoryName, subCategory) {
                        var j = 0;
                        var columnType;
                        vm.tableColumns.forEach(function (d) {
                            if (d.reName == vm.lastCategoryTableColumn) {
                                columnType = d.columType;
                            }
                        });
                        if (categoryName) {
                            Object.keys(categoryData)
                                .forEach(function (r) {
                                    if (categoryData[$scope.subCategory[j]] != categoryData[r]) {
                                        categoryData[$scope.subCategory[j]] = categoryData[r];
                                        delete categoryData[r];
                                    }
                                    j++;
                                });
                            var lengthCheck = 0;
                            if (vm.dataCount < $rootScope.localDataLimit) {
                                vm.tableData.forEach(function (d) {
                                    var keysVal = "";
                                    Object.keys(categoryData).forEach(function (r) {
                                        var index = categoryData[r].indexOf(d[vm.lastCategoryTableColumn]);
                                        if (index != -1) {
                                            keysVal = r;
                                        }
                                    });
                                    if (keysVal == "") {
                                        keysVal = d[vm.lastCategoryTableColumn];
                                    }
                                    d[categoryName] = keysVal;
                                    lengthCheck++
                                    if (lengthCheck == vm.tableData.length) {
                                        vm.lastColumnShow = true;
                                        vm.tableColumns.push({
                                            "columType": columnType,
                                            "columnName": categoryName,
                                            "dataKey": "Dimension",
                                            "reName": categoryName,
                                            "tableName": "",
                                            "type": "custom",
                                            "id": j,
                                            "createdby": vm.columnSelectByGroup
                                        });
                                        setTimeout(function () {
                                            /*$("#filter").multiselect("destroy");
                                                  $("#filter").multiselect({
                                                  includeSelectAllOption: false,
                                                  enableFiltering: true,
                                                  maxHeight: 200
                                                  });*/
                                            $("#filter").selectpicker('destroy');
                                            $("#filter").selectpicker();
                                        }, 100);
                                        if (vm.categoryGroupObject[groupIndex] != undefined) {
                                            groupIndex = _.keys(vm.categoryGroupObject).length;
                                        }
                                        vm.categoryGroupObject[groupIndex] = {};
                                        vm.categoryGroupObject[groupIndex]["columnName"] = $scope.lastCategoryTableColumn;
                                        vm.categoryGroupObject[groupIndex]["categoryName"] = categoryName;
                                        vm.categoryGroupObject[groupIndex]["groupData"] = categoryData;
                                        groupIndex++;
                                        $("#category").modal('hide');
                                    }
                                    $scope.selectedGroup = {};
                                    $(".reportDashboard").removeClass("background-container");
                                });
                            } else {
                                vm.lastColumnShow = true;
                                vm.tableColumns.push({
                                    "columType": columnType,
                                    "columnName": categoryName,
                                    "dataKey": "Dimension",
                                    "reName": categoryName,
                                    "tableName": "",
                                    "type": "custom",
                                    "id": '',
                                    "createdby": vm.columnSelectByGroup
                                });
                                var data = {
                                    columnName: vm.lastCategoryTableColumn,
                                    metadataId: vm.metadataId,
                                    sessionId: $rootScope.accessToken,
                                    categoryData: categoryData,
                                    categoryName: categoryName
                                };
                                vm.categorySelectColumnData = [];
                                dataFactory.nodeRequest('groupColumnCreate', 'post', data).then(function (response) {
                                    if (response) {

                                    } else {
                                        dataFactory.errorAlert("Check your connection");
                                    }
                                });
                                vm.categoryGroupObject[groupIndex] = {};
                                vm.categoryGroupObject[groupIndex]["columnName"] = $scope.lastCategoryTableColumn;
                                vm.categoryGroupObject[groupIndex]["categoryName"] = categoryName;
                                vm.categoryGroupObject[groupIndex]["groupData"] = categoryData;
                                groupIndex++;
                                $("#category").modal('hide');
                                $scope.selectedGroup = {};
                                $(".reportDashboard").removeClass("background-container");
                            }
                        } else if (categoryData.length) {
                            $scope.checkError = 0;
                            $scope.categoryError = "Make sub category";
                        } else {
                            $scope.checkError = 0;
                            $scope.categoryError = "Enter Category Name";
                        }
                    }
                    // Sub category
                    vm.selectedGroup = {};
                    var custIndex = 0;
                    vm.subCategory = {};
                    //Toggle category group
                    vm.clicked = function ($event) {
                        var a = jQuery($event.target);
                        var ul = a.next();
                        ul.slideToggle();
                    }
                    vm.deleteGroupValue = function (key, index, value) {

                        if ((vm.selectedGroup[key].length) > 1) {
                            var indexCategory = vm.categorySelectColumnData.indexOf(value);
                            vm.selectedGroup[key].splice(index, 1);

                            $($(".ms-list > .ms-elem-selectable")[indexCategory]).show()
                        } else {
                            alert("You can't delete all sub category");
                        }

                    }
                    vm.deleteGroup = function (key, index) {
                        delete vm.subCategory[index];
                        var tempObj = {};
                        if (_.keys(vm.subCategory).length > 0) {
                            Object.keys(vm.subCategory).forEach(function (key, index) {
                                tempObj[index] = vm.subCategory[key];
                            });
                            vm.subCategory = tempObj;
                        }
                        vm.selectedGroup[key].forEach(function (d) {
                            var indexCategory = vm.categorySelectColumnData.indexOf(d);
                            $($(".ms-list > .ms-elem-selectable")[indexCategory]).show();
                        });
                        delete vm.selectedGroup[key];
                    }
                    vm.addSubCategory = function () {
                        if (vm.selectedValue.length) {
                            $(".ms-selected").hide();
                            if (vm.subCategory[custIndex] == undefined) {
                                vm.subCategory[custIndex] = 'group'
                                    + custIndex;
                            }

                            vm.selectedGroup['group' + custIndex] = vm.selectedValue;
                            vm.selectedValue = [];
                            custIndex++;
                        } else {
                            vm.checkError = 0;
                            vm.categoryError = "Select a value";
                        }
                    }
                    // Text add value formula field
                    $scope.measureFormula = "";
                    vm.calObject = {};
                    vm.temp_BusinessName = {};
                    $scope.insertTextToFormula = function (name, type, keys, functionListObj) {
                        if (keys && keys.columnName)
                            vm.temp_BusinessName[keys.columnName] = keys;
                        if (type == 'fieldName') {
                            name = '[' + name + ']';
                        } else if (type == 'functionList') {
                            if (functionListObj && functionListObj.description)
                                var description = functionListObj.description;
                        } else {
                            name = name;
                            var description = "";
                            if (functionListObj && functionListObj.description)
                                var description = functionListObj.description;
                        }
                        if (vm.calObject.measureFormula == undefined) {
                            vm.calObject.measureFormula = "";
                        }
                        if (name) {
                            vm.calObject.measureFormula += name;
                        }
                        vm.descriptionText = description;
                    }
                    //Update Group Category
                    vm.categoryError = "";// For Error
                    vm.categoryGroupObject = {};
                    var groupIndex = 0;
                    vm.cateogryUpdate = function (categoryData, categoryName, subCategory) {
                        var j = 0;
                        var columnType;
                        vm.tableColumns.forEach(function (d) {
                            if (d.reName == vm.lastCategoryTableColumn) {
                                columnType = d.columType;
                            }
                        });
                        if (categoryName) {
                            Object.keys(categoryData)
                                .forEach(function (r) {
                                    if (categoryData[$scope.subCategory[j]] != categoryData[r]) {
                                        categoryData[$scope.subCategory[j]] = categoryData[r];
                                        delete categoryData[r];
                                    }
                                    j++;
                                });
                            if (vm.dataCount < $rootScope.localDataLimit) {
                                var lengthCheck = 0;
                                vm.tableData.forEach(function (d) {
                                    var keysVal = "";
                                    Object.keys(categoryData).forEach(function (r) {
                                        var index = categoryData[r].indexOf(d[vm.lastCategoryTableColumn]);
                                        if (index != -1) {
                                            keysVal = r;
                                        }
                                    });
                                    if (keysVal == "") {
                                        keysVal = d[vm.lastCategoryTableColumn];
                                    }
                                    d[categoryName] = keysVal;
                                    lengthCheck++
                                    if (lengthCheck == vm.tableData.length) {
                                        vm.lastColumnShow = true;
                                        vm.tableColumns.push({
                                            "columType": columnType,
                                            "columnName": categoryName,
                                            "dataKey": "Dimension",
                                            "reName": categoryName,
                                            "tableName": "",
                                            "type": "custom",
                                            "id": j,
                                            "createdby": vm.columnSelectByGroup
                                        });
                                        setTimeout(function () {
                                            $("#filter").selectpicker('destroy');
                                            $("#filter").selectpicker();
                                        }, 100);
                                        vm.categoryGroupObject[groupIndex] = {};
                                        vm.categoryGroupObject[groupIndex]["columnName"] = $scope.lastCategoryTableColumn;
                                        vm.categoryGroupObject[groupIndex]["categoryName"] = categoryName;
                                        vm.categoryGroupObject[groupIndex]["groupData"] = categoryData;
                                        groupIndex++;
                                        $("#categoryEdit").modal('hide');
                                    }
                                    $scope.selectedGroup = {};
                                    $(".reportDashboard").removeClass("background-container");


                                });
                                var lastObj = vm.categoryGroupObject[vm.editGroupIndex];
                                vm.tableData.forEach(function (d, index) {
                                    if (lastObj.columnName == d.columnName) {
                                        delete d[index];
                                    }
                                });
                            } else {
                                vm.lastColumnShow = true;
                                vm.tableColumns.push({
                                    "columType": columnType,
                                    "columnName": categoryName,
                                    "dataKey": "Dimension",
                                    "reName": categoryName,
                                    "tableName": "",
                                    "type": "custom",
                                    "id": '',
                                    "createdby": vm.columnSelectByGroup
                                });
                                var lastObj = vm.categoryGroupObject[vm.editGroupIndex];
                                var data = {
                                    columnName: vm.lastCategoryTableColumn,
                                    metadataId: vm.metadataId,
                                    sessionId: $rootScope.accessToken,
                                    categoryData: categoryData,
                                    categoryName: categoryName,
                                    lastObj: lastObj
                                };
                                vm.categorySelectColumnData = [];
                                dataFactory.nodeRequest('groupColumnUpdate', 'post', data).then(function (response) {
                                    if (response) {

                                    } else {
                                        dataFactory.errorAlert("Check your connection");
                                    }
                                });
                                vm.categoryGroupObject[groupIndex] = {};
                                vm.categoryGroupObject[groupIndex]["columnName"] = $scope.lastCategoryTableColumn;
                                vm.categoryGroupObject[groupIndex]["categoryName"] = categoryName;
                                vm.categoryGroupObject[groupIndex]["groupData"] = categoryData;
                                groupIndex++;
                                $("#categoryEdit").modal('hide');
                                $scope.selectedGroup = {};
                                $(".reportDashboard").removeClass("background-container");
                            }
                            var columnIndex = "";
                            vm.tableColumns.forEach(function (d, index) {
                                if (d.reName == lastObj.categoryName) {
                                    columnIndex = index;
                                }
                            });
                            vm.tableColumns.splice(columnIndex, 1);
                            delete vm.categoryGroupObject[vm.editGroupIndex];
                        } else if (categoryData.length) {
                            $scope.checkError = 0;
                            $scope.categoryError = "Make sub category";
                        } else {
                            $scope.checkError = 0;
                            $scope.categoryError = "Enter Category Name";
                        }
                    }
                    //Edit category
                    vm.editGroupCategory = function (columnObject) {
                        vm.categoryError = '';
                        vm.categoryEdit = {};
                        vm.categoryEdit.categoryName = columnObject.reName;
                        var modalElem = $('#categoryEdit');
                        $('#categoryEdit').modal({
                            backdrop: 'static',
                            keyboard: false
                        });
                        var editIndex = 0;
                        $.each(vm.categoryGroupObject, function (key, val) {
                            if (val.categoryName == columnObject.reName) {
                                vm.editGroupIndex = editIndex;
                            }
                            editIndex++;
                        });
                        vm.columnSelectByGroup = columnObject;
                        vm.selectedGroup = {};
                        $(".reportDashboard").addClass("background-container");
                        var connectionObject = vm.metadataObject.connObject.connectionObject;
                        vm.lastCategoryTableColumn = JSON.parse(columnObject.createdby).columnName;
                        if (vm.categorySelectColumnData != undefined) {
                            $("#groupEditCategory").multiSelect("destroy");
                        }
                        vm.categorySelectColumnData = [];
                        var categoryIndex = 0, objIndex = 0;
                        try {
                            var columnObj = JSON.parse(columnObject.createdby);
                        } catch (e) {
                            var columnObj = columnObject.createdby;
                        }
                        $.each(vm.categoryGroupObject, function (key, value) {
                            if (value.columnName == columnObj.reName) {
                                categoryIndex = key;
                            }
                        });
                        vm.selectedGroup = vm.categoryGroupObject[categoryIndex].groupData;
                        vm.subCategory = {};
                        var selectedGroup = {};
                        Object.keys(vm.selectedGroup).forEach(function (key, index) {
                            vm.subCategory[index] = key;
                            selectedGroup[key] = true;
                        });
                        if (vm.dataCount < $rootScope.localDataLimit) {
                            vm.tableData.forEach(function (d) {
                                if (d[vm.lastCategoryTableColumn] && tempObj[d[vm.lastCategoryTableColumn]] == undefined) {
                                    tempObj[d[vm.lastCategoryTableColumn]] = "";
                                    $scope.categorySelectColumnData.push(d[vm.lastCategoryTableColumn]);
                                }
                                itemsProcessed++;
                                if (itemsProcessed === vm.tableData.length) {
                                    callBack();
                                }
                            });
                        } else {
                            var data = {
                                columnName: columnObject,
                                metadataId: vm.metadataId,
                                sessionId: $rootScope.accessToken,
                                selectedGroupKey: selectedGroup
                            };
                            vm.categorySelectColumnData = [];
                            dataFactory.nodeRequest('groupDataEditList', 'post', data).then(function (response) {
                                if (response) {
                                    response.forEach(function (d) {
                                        vm.categorySelectColumnData.push(d);
                                    });
                                    $scope.$apply();
                                } else {
                                    dataFactory.errorAlert("Check your connection");
                                }
                            }).then(function () {
                                callBack();
                            });
                        }
                        var callBack = function () {
                            setTimeout(
                                function () {
                                    $scope.selectedValue = [];
                                    $('#groupEditCategory').multiSelect({
                                        afterSelect: function (values) {
                                            $scope.selectedValue.push(values[0]);
                                            $scope.checkError = 1;
                                        },
                                        afterDeselect: function (values) {
                                            var index = $scope.selectedValue.indexOf(values[0]);
                                            if (index != -1) {
                                                $scope.selectedValue.splice(index, 1);
                                            }
                                            $scope.checkError = 1;
                                        }
                                    });

                                }, 1000);
                        }
                    }
                    // Create Category
                    vm.createCategoryModel = function () {
                        var modalElem = $('#category');
                        $('#category').modal({
                            backdrop: 'static',
                            keyboard: false
                        });
                        setTimeout(function () {
                            $(".step1").show();
                            $(".step2").hide();
                        }, 100);
                    }
                    vm.categoryGroupObj = {};
                    vm.createCategory = function (columnObject) {
                        $(".step1").hide();
                        $(".step2").show();
                        vm.categoryGroupObj = {};
                        vm.columnSelectByGroup = columnObject;
                        vm.selectedGroup = {};
                        $(".reportDashboard").addClass("background-container");
                        var connectionObject = vm.metadataObject.connObject.connectionObject;
                        var columnObject = JSON.parse(columnObject)
                        vm.lastCategoryTableColumn = columnObject.columnName;
                        if (vm.categorySelectColumnData != undefined) {
                            $("#my-select").multiSelect("destroy");
                        }
                        if (vm.dataCount < $rootScope.localDataLimit) {
                            var itemsProcessed = 0;
                            var tempObj = {};
                            vm.tableData.forEach(function (d) {
                                if (d[vm.lastCategoryTableColumn] && tempObj[d[vm.lastCategoryTableColumn]] == undefined) {
                                    tempObj[d[vm.lastCategoryTableColumn]] = "";
                                    $scope.categorySelectColumnData.push(d[vm.lastCategoryTableColumn]);
                                }
                                itemsProcessed++;
                                if (itemsProcessed === vm.tableData.length) {
                                    callBack();
                                }
                            });
                        } else {
                            var data = {
                                columnName: columnObject,
                                metadataId: vm.metadataId,
                                sessionId: $rootScope.accessToken
                            };
                            vm.categorySelectColumnData = [];
                            dataFactory.nodeRequest('groupData', 'post', data).then(function (response) {
                                if (response) {
                                    response.forEach(function (d) {
                                        vm.categorySelectColumnData.push(d);
                                    });
                                    $scope.$apply();
                                } else {
                                    dataFactory.errorAlert("Check your connection");
                                }
                            }).then(function () {
                                callBack();
                            });
                        }
                        var callBack = function () {
                            setTimeout(
                                function () {
                                    $scope.selectedValue = [];
                                    $('#my-select').multiSelect({
                                        afterSelect: function (values) {
                                            $scope.selectedValue.push(values[0]);
                                            $scope.checkError = 1;
                                        },
                                        afterDeselect: function (values) {
                                            var index = $scope.selectedValue.indexOf(values[0]);
                                            if (index != -1) {
                                                $scope.selectedValue.splice(index, 1);
                                            }
                                            $scope.checkError = 1;
                                        }
                                    });
                                }, 1000);
                        }
                        // Close Error
                        vm.notificationClose = function () {
                            vm.checkError = 1;
                        }
                    }
                    // Create Category
                    vm.createCategoryModel = function () {
                        var modalElem = $('#category');
                        $('#category').modal({
                            backdrop: 'static',
                            keyboard: false
                        });
                        setTimeout(function () {
                            $(".step1").show();
                            $(".step2").hide();
                        }, 100);
                    }

                    vm.categoryGroupObj = {};
                    vm.createCategory = function (columnObject) {
                        $(".step1").hide();
                        $(".step2").show();
                        vm.categoryGroupObj = {};
                        vm.columnSelectByGroup = columnObject;
                        vm.selectedGroup = {};
                        $(".reportDashboard").addClass("background-container");
                        var connectionObject = vm.metadataObject.connObject.connectionObject;
                        var columnObject = JSON.parse(columnObject)
                        vm.lastCategoryTableColumn = columnObject.columnName;
                        if (vm.categorySelectColumnData != undefined) {
                            $("#my-select").multiSelect("destroy");
                        }
                        if (vm.dataCount < $rootScope.localDataLimit) {
                            var itemsProcessed = 0;
                            var tempObj = {};
                            vm.tableData.forEach(function (d) {
                                if (d[vm.lastCategoryTableColumn] && tempObj[d[vm.lastCategoryTableColumn]] == undefined) {
                                    tempObj[d[vm.lastCategoryTableColumn]] = "";
                                    $scope.categorySelectColumnData.push(d[vm.lastCategoryTableColumn]);
                                }
                                itemsProcessed++;
                                if (itemsProcessed === vm.tableData.length) {
                                    callBack();
                                }
                            });
                        } else {
                            var data = {
                                columnName: columnObject,
                                metadataId: vm.metadataId,
                                sessionId: $rootScope.accessToken
                            };
                            vm.categorySelectColumnData = [];
                            dataFactory.nodeRequest('groupData', 'post', data).then(function (response) {
                                if (response) {
                                    response.forEach(function (d) {
                                        vm.categorySelectColumnData.push(d);
                                    });
                                    $scope.$apply();
                                } else {
                                    dataFactory.errorAlert("Check your connection");
                                }
                            }).then(function () {
                                callBack();
                            });
                        }
                        var callBack = function () {
                            setTimeout(
                                function () {
                                    $scope.selectedValue = [];
                                    $('#my-select').multiSelect({
                                        afterSelect: function (values) {
                                            $scope.selectedValue.push(values[0]);
                                            $scope.checkError = 1;
                                        },
                                        afterDeselect: function (values) {
                                            var index = $scope.selectedValue.indexOf(values[0]);
                                            if (index != -1) {
                                                $scope.selectedValue.splice(index, 1);
                                            }
                                            $scope.checkError = 1;
                                        }
                                    });
                                }, 1000);
                        }
                        // Close Error
                        vm.notificationClose = function () {
                            vm.checkError = 1;
                        }
                    }
                    // Delete dimension and measures
                    vm.columnDeleted = [];
                    vm.deleteColumn = function (columnName) {
                        // filter the array
                        var foundItem = $filter('filter')(
                            vm.tableColumns, {
                                reName: columnName
                            }, true)[0];
                        // get the index
                        var index = vm.tableColumns.indexOf(foundItem);
                        vm.columnDeleted.push(vm.tableColumns[index]);
                        vm.tableColumns.splice(index, 1);
                    }
                    // save Calculation field
                    $scope.fieldName = "";
                    $scope.saveCalculationField = function (calObj) {
                        var temp_data = calObj.measureFormula;
                        var regexExpr = /\[(.*?)\]/gm;
                        var calSelectedColumn = {};
                        let m;
                        while ((m = regexExpr.exec(temp_data)) !== null) {
                            calSelectedColumn[m[1]] = true;
                        }
                        var calObjectSelected = {};
                        var formulaColumnArray = [];
                        vm.tableColumns.forEach(function (d) {
                            if (calSelectedColumn[d.reName]) {
                                calObjectSelected[d.columnName] = d;
                                formulaColumnArray.push(d);
                            }
                        });
                        calObjectSelected = Object.keys(calObjectSelected)
                            .map(function (k) {
                                return {key: k, value: calObjectSelected[k]};
                            })
                            .sort(function (a, b) {
                                return b.key.length - a.key.length;
                            });
                        var tempCheck = {};
                        calObjectSelected.forEach(function (d) {
                            if (tempCheck[d.key] == undefined) {
                                tempCheck[d.key] = true;
                                var key = d.value;
                                if (key && key.reName != key.columnName) {
                                    var temp_reName = key.reName;
                                    var temp_columnName = key.columnName;
                                    if (temp_data) {
                                        temp_reName = escapeRegExp(temp_reName);
                                        var tempRename = new RegExp(temp_reName, 'g');
                                        temp_data = temp_data.replace(tempRename, temp_columnName);
                                    }
                                } else {
                                    temp_data = temp_data;
                                }
                            }
                        });
                        var name = calObj.fieldName;
                        var formula = temp_data;
                        if (formula != "" && name != "" && name != undefined) {
                            var expression;
                            var errorMessage;

                            // Check express for bracketFp
                            function update() {
                                expression = true;
                                try {
                                    balanced.matches({
                                        source: formula,
                                        open: ['{',
                                            '(',
                                            '['],
                                        close: ['}',
                                            ')',
                                            ']'],
                                        balance: true,
                                        exceptions: truesaveCalculationField
                                    });
                                } catch (error) {
                                    expression = false;
                                    errorMessage = error.message;
                                }
                            }

                            update();
                            expression = true;
                            if (expression) {
                                var errorMessage = false;
                                if (vm.dataCount < $rootScope.localDataLimit) {
                                    if (vm.filteredData.length != 0) {
                                        errorMessage = calculate.processExpression(formula, vm.filteredData, name);
                                    } else {
                                        errorMessage = calculate.processExpression(formula, vm.tableData, name);
                                    }
                                } else {
                                    errorMessage = calculateServer.processExpression(formula, [], name, {
                                        "columType": "varchar",
                                        "columnName": name,
                                        "dataKey": calObj.dataKey,
                                        "reName": name,
                                        "tableName": "",
                                        "formula": formula,
                                        "type": "custom",
                                        'form': "group"
                                    }, $rootScope.accessToken, vm.metadataId, $rootScope.serverSide_Url);
                                    errorMessage = false;
                                }
                                //For Edit calculation
                                if (vm.flagCheck) {
                                    var index = $scope.tableColumns.indexOf(vm.calCurrentEditObj);
                                    $scope.tableColumns.splice(index, 1);
                                }
                                var ColumnFlag = 0;
                                $.each($scope.tableColumns, function (key, value) {
                                    if (value.columnName == name) {
                                        ColumnFlag++;
                                    }
                                });
                                if (ColumnFlag) {
                                    $scope.errorCalculation = "Column name already exist";
                                    return;
                                }
                                //Check formula column
                                if (errorMessage == false) {
                                    if (vm.lastNameOfColumn != null) {
                                        var prevRename = vm.lastNameOfColumn;
                                        vm.tableColumns.forEach(function (column) {

                                            if (column && column.formulaObj) {
                                                column.formulaObj.forEach(function (d) {
                                                    if (d.reName != d.columnName && d.reName == prevRename) {
                                                        d.reName = name;
                                                    }
                                                });
                                            }
                                        });
                                        vm.lastNameOfColumn = null;
                                    }
                                    if (formula.includes("group")) {
                                        $scope.tableColumns.push({
                                            "columType": "varchar",
                                            "columnName": name,
                                            "dataKey": calObj.dataKey,
                                            "reName": name,
                                            "tableName": "",
                                            "formula": formula,
                                            "type": "custom",
                                            'form': "group",
                                            "formulaObj": formulaColumnArray
                                        });
                                        var data = {
                                            "sessionId": $rootScope.accessToken,
                                            "metadataId": vm.metadataId,
                                            "tableColumn": [{
                                                "columType": "varchar",
                                                "columnName": name,
                                                "dataKey": calObj.dataKey,
                                                "reName": name,
                                                "tableName": "",
                                                "formula": formula,
                                                "type": "custom",
                                                'form': "group",
                                                "formulaObj": formulaColumnArray
                                            }]
                                        }
                                        dataFactory.nodeRequest('addTableColumn', 'post', data);
                                    } else {
                                        $scope.tableColumns.push({
                                            "columType": "varchar",
                                            "columnName": name,
                                            "dataKey": calObj.dataKey,
                                            "reName": name,
                                            "tableName": "",
                                            "formula": formula,
                                            "type": "custom",
                                            'form': "simple",
                                            "formulaObj": formulaColumnArray
                                        });
                                        var data = {
                                            "sessionId": $rootScope.accessToken,
                                            "metadataId": vm.metadataId,
                                            "tableColumn": [{
                                                "columType": "varchar",
                                                "columnName": name,
                                                "dataKey": calObj.dataKey,
                                                "reName": name,
                                                "tableName": "",
                                                "formula": formula,
                                                "type": "custom",
                                                'form': "simple",
                                                "formulaObj": formulaColumnArray
                                            }]
                                        }
                                        dataFactory.nodeRequest('addTableColumn', 'post', data);
                                    }
                                    setTimeout(function () {
                                        $("#filter").multiselect("destroy");
                                        $("#filter").multiselect({
                                            includeSelectAllOption: false,
                                            enableFiltering: true,
                                            maxHeight: 200
                                        });
                                    }, 100);
                                    $scope.lastColumnShow = true;
                                    $("#calculationMeasure").modal('hide');
                                } else {
                                    $scope.errorCalculation = errorMessage;
                                }
                            } else {
                                $scope.errorCalculation = errorMessage;
                            }
                        } else if (formula === "") {
                            $scope.errorCalculation = "Please select expressions";
                        } else if (name === "" || name === undefined) {
                            $scope.errorCalculation = "Please Enter field name";
                        }
                        $(".reportDashboard").removeClass("background-container");
                    }
                    $scope.modal = {};
                    $scope.modal.slideUp = "default";
                    $scope.modal.stickUp = "default";
                    $scope.addChartIconBorder = function (chart) {

                        $(".custBorderHover").removeClass('activeChart');
                        $("#" + chart.key).addClass('activeChart');
                    }
                    vm.onAggregateSelect = function (measure, aggregate) {
                        //vm.Attributes.aggregateModel[measure.reName] = aggregate;
                        vm.Attributes.aggregateModel[measure.columnName] = aggregate;
                    }
                    vm.chartSelect = function (index, chart) {
                        vm.dashboardPrototype.reportContainers[vm.DashboardModel.Dashboard.activeReportIndex]['chartType'] = chart.name;
                        if (chart.key === "_compositeLineBarChart") {
                            var modalElem1 = $('#barLineModal');
                            $('#barLineModal').modal({
                                backdrop: 'static',
                                keyboard: false
                            });
                        }
                        $scope.addChartIconBorder(chart);

                    }
                    vm.aggeregateSelect = function (aggregate, containerId, measure) {
                        vm.aggregateText[containerId] = aggregate.value;
                        if (vm.Attributes.aggregateModel)
                            vm.Attributes.aggregateModel[measure.reName] = aggregate;
                    }
                    vm.checkboxModelMeasure = [];
                    vm.checkboxModelDimension = [];
                },
                getCharts: function () {
                    return vm.leftSideModel.chartTypeData;
                },
                getAggregates: function () {
                    return vm.leftSideModel.aggregateData;

                },

                senseDefaultChart: function () {
                    if (Object.keys(vm.Attributes.checkboxModelDimension).length == 1
                        && Object
                            .keys(vm.Attributes.checkboxModelMeasure).length == 1) {
                        $scope.addChartIconBorder(sketch.chartData[0]);
                        return sketch.chartData[0];

                    } else if (Object
                            .keys(vm.Attributes.checkboxModelDimension) == 1
                        && Object
                            .key(vm.Attributes.checkboxModelMeasure).length == 2) {
                        return sketch.chartData[5];
                    } else {
                        return false;
                    }

                },

                requiredAttributesFound: function () {

                    if (vm.Attributes.checkboxModelDimension && vm.Attributes.checkboxModelMeasure)
                        if (Object.keys(vm.Attributes.checkboxModelDimension).length > 0 && Object.keys(vm.Attributes.checkboxModelMeasure).length > 0) {
                            return true;
                        }
                    return false;

                },
                showChartErrorText: function (chart) {
                    try {
                        if (chart != null) {
                            vm.blnkRep = true;

                            //  vm.showInfovm.DashboardModel.Dashboard.activeReport.reportContainer.id=true;
                            $("#chart-" + vm.DashboardModel.Dashboard.activeReport.reportContainer.id).html("");
                            vm.chartErrorText[vm.DashboardModel.Dashboard.activeReport.reportContainer.id] = "You need to select max "
                                + chart.required.Dimension
                                + " dimension"
                                + " and min "
                                + chart.required.Measure[0]
                                + " measure to draw "
                                + chart.name;

                        } else {
                            vm.blnkRep = true;

                            vm.chartErrorText[vm.DashboardModel.Dashboard.activeReport.reportContainer.id] = "You need to select atleast 1 dimension and 1 measure to draw any chart";
                        }
                    } catch (e) {
                        vm.chartErrorText[vm.DashboardModel.Dashboard.activeReport.reportContainer.id] = "Some Error Occured....";
                    }
                },
                checkChartCanBeDrawn: function (chart) {
                    if (chart != null)
                        if (chart.name == "DataTable")
                            return true;
                    if (vm.Attributes.checkboxModelDimension && vm.Attributes.checkboxModelMeasure) {


                        if ((Object.keys(vm.Attributes.checkboxModelDimension).length == chart.required.Dimension) && ((chart.required.Measure[0] <= Object.keys(vm.Attributes.checkboxModelMeasure).length) && (chart.required.Measure[1] >= Object.keys(vm.Attributes.checkboxModelMeasure).length))) {
                            vm.showInfo[vm.DashboardModel.Dashboard.activeReport.reportContainer.id] = false;
                            return true;

                        }
                    }
                    vm.showInfo1 = true;
                    vm.showInfo2 = true;
                    vm.showInfo[vm.DashboardModel.Dashboard.activeReport.reportContainer.id] = true;
                    return false;
                },
                updateAttributesObject: function (attr, type, chartType) {
                    var checkType = true;
                    if (chartType == "_dataTable") {
                        return true;
                    }
                    if (type == "dimension") {
                        if ($scope.Attributes.checkboxModelDimension[attr.reName] == 0) {
                            checkType = false;
                            delete $scope.Attributes.checkboxModelDimension[attr.reName];
                            // return false;
                        } else {
                            $scope.Attributes.checkboxModelDimension[attr.reName].columType = attr.columType;
                            $scope.Attributes.checkboxModelDimension[attr.reName].reName = attr.reName;
                        }
                    }

                    if (type == "measure") {
                        if ($scope.Attributes.checkboxModelMeasure[attr.reName] == 0) {
                            checkType = false;
                            delete $scope.Attributes.checkboxModelMeasure[attr.reName];
                            // return false;
                        } else {
                            $scope.Attributes.checkboxModelMeasure[attr.reName].runningTotal = attr.runningTotal;
                            $scope.Attributes.checkboxModelMeasure[attr.reName].columType = attr.columType;
                            $scope.Attributes.checkboxModelMeasure[attr.reName].reName = attr.reName;
                        }
                    }

                    if (attr.key == '_PivotCustomized' || vm.DashboardModel.Dashboard.activeReport.chart.key == "_PivotCustomized") {
                        vm.pivotAttributes(checkType, attr.columnName, attr);
                    }


                    if (type == "chart" && !vm.leftSideController.checkChartCanBeDrawn(attr)) {
                        if (attr.key != '_pivotTable' && attr.key != '_maleFemaleChartJs' && attr.key != '_floorPlanJs') {
                            vm.leftSideController.showChartErrorText(attr);
                        }
                        return false;
                    }
                    return true;
                },
                addNewContainer: function () {
                    sketchServer._totalReportCount++;
                    vm.Attributes = {aggregateModel: {}};
                    vm.DashboardController.addReportContainer(sketch.chartData[0]);
                    vm.DashboardModel.Dashboard.activeReport.chart = sketch.chartData[0];
                    $scope.selectedChartInView[vm.DashboardModel.Dashboard.activeReport.reportContainer.id] = null;
                    $(".custBorderHover").removeClass('activeChart');
                    // $("#" + index).addClass('activeChart');

                },
                setDefaultAggregate: function (measure) {


                    //Initializing Aggregate Model In Case Not Initialized
                    if (!vm.Attributes.aggregateModel)
                        vm.Attributes.aggregateModel = {};


                    vm.Attributes.aggregateModel[measure.columnName] = {
                        "key": "sumIndex",
                        "value": "Sum",
                        "type": "Aggregate"
                    };

                },
                setChartInView: function (chart) {
                    $scope.selectedChartInView[vm.DashboardModel.Dashboard.activeReport.reportContainer.id] = chart;
                },
                getSelectedChartFromView: function () {
                    return $scope.selectedChartInView[vm.DashboardModel.Dashboard.activeReport.reportContainer.id];
                },
                checkAndDrawChart: function (chart) {
                    var isChartBeDrawn = sketch.axisConfig(vm.Attributes).chartConfig(chart)._isAllFieldsPassed();
                    if (isChartBeDrawn) {
                        vm.showInfo[vm.DashboardModel.Dashboard.activeReport.reportContainer.id] = false;
                        vm.leftSideController.showProgressBar(true);
                        vm.DashboardView.renderChartInActiveContainer();
                        vm.DashboardModel.Dashboard.activeReport.chart = chart;
                    } else {
                        vm.leftSideController.showChartErrorText(vm.DashboardModel.Dashboard.activeReport.chart);
                        vm.showInfo[vm.DashboardModel.Dashboard.activeReport.reportContainer.id] = true;
                        $(".label_legends" + vm.DashboardModel.Dashboard.activeReport.reportContainer.id).hide();
                        $("#left" + vm.DashboardModel.Dashboard.activeReport.reportContainer.id).html("");
                    }
                },
                switchChartContainer: function (attr, callback) {
                    var activeReportContainerId = vm.DashboardModel.Dashboard.activeReport.reportContainer.id;
                    var activeContainer = $scope.dashboardPrototype.reportContainers[$scope.findIndexOfReportContainer(activeReportContainerId)];
                    if (attr.key != "_numberWidget") {
                        if ($scope.lastDrawnChart[vm.DashboardModel.Dashboard.activeReport.reportContainer.id] == "_numberWidget") {
                            var obj = vm.DashboardController.getCommonContainerSettings();

                            activeContainer['name'] = 'Report';
                            activeContainer['minSizeX'] = obj.minSizeX;
                            activeContainer['minSizeY'] = obj.minSizeY;
                            activeContainer['maxSizeX'] = obj.maxSizeX;
                            activeContainer['maxSizeY'] = obj.maxSizeY;
                            activeContainer['sizeX'] = obj.sizeX;
                            activeContainer['sizeY'] = obj.sizeY;

                            // $.each(obj,function (key,value) {
                            //     activeContainer[key] = value;
                            // });

                        }
                    } else {
                        var obj = vm.DashboardController.getNumberContainerSettings();
                        $.each(obj, function (key, value) {
                            activeContainer[key] = value;
                        });
                    }
                    vm.DashboardModel.Dashboard.activeReport.reportContainer = activeContainer;
                    var indexOfReport = $scope.findIndexOfReportFromReportContainer(activeReportContainerId);
                    vm.DashboardModel.Dashboard.Report[indexOfReport]['reportContainer'] = activeContainer;
                    setTimeout(function () {
                        callback();
                    }, 1000);
                },
                showProgressBar: function (state) {
                    var variable = "reportLoading_" + vm.DashboardModel.Dashboard.activeReport.reportContainer.id;
                    vm[variable] = state;
                },
                onAttributeSelect: function (attr, type) {
                    if(vm.DashboardModel.Dashboard.Report[vm.DashboardModel.Dashboard.activeReportIndex].axisConfig && vm.DashboardModel.Dashboard.Report[vm.DashboardModel.Dashboard.activeReportIndex].axisConfig['excludeKey'] && (type==="dimension" || type==="chart")){
                        vm.DashboardModel.Dashboard.Report[vm.DashboardModel.Dashboard.activeReportIndex].axisConfig['excludeKey']=[];
                    }
                    //Updating Attribute Object According to selections
                    vm.leftSideController.updateAttributesObject(attr, type);
                    if (type === "measure") {
                        //Sets default aggregate for each measure
                        vm.leftSideController.setDefaultAggregate(attr);
                    }

                    if (type === "dimension" || type === "measure" || type === "aggregate") {
                        var activeChart = vm.leftSideController.getSelectedChartFromView();
                        //check if any chart selected in view
                        if (!activeChart) {
                            vm.leftSideController.setChartInView(vm.DashboardModel.Dashboard.activeReport.chart);
                        } else {
                            vm.leftSideController.checkAndDrawChart(activeChart);
                        }
                    }

                    if (type === "chart") {
                        vm.leftSideController.switchChartContainer(attr, function () {
                            if (attr.key == "_pivotTable" && vm.DashboardModel.Dashboard.Report[vm.DashboardModel.Dashboard.activeReportIndex].chartObject && vm.DashboardModel.Dashboard.Report[vm.DashboardModel.Dashboard.activeReportIndex].chartObject.config) {

                                var activeReportIndex = vm.DashboardModel.Dashboard.activeReportIndex;
                                var dataObj = vm.DashboardModel.Dashboard.Report[activeReportIndex].dataObject;

                                vm.DashboardModel.Dashboard.Report[activeReportIndex].chartObject['recall'] = true;
                                sketch.pivotTableConfig = Object.assign({}, dataObj);

                                sketch.pivotTableConfig['recall'] = true;

                                var reportContainerId = vm.DashboardModel.Dashboard.activeReport.reportContainer.id;
                                var pivotTable = sketch.data(vm.tableData).container(reportContainerId)._pivotTable();
                            } else if (attr.key == "_pivotTable") {

                                sketch.pivotTableConfig = undefined;
                                var reportContainerId = vm.DashboardModel.Dashboard.activeReport.reportContainer.id;
                                var pivotTable = sketch.data(vm.tableData).container(reportContainerId)._pivotTable();
                            }
                            vm.leftSideController.setChartInView(attr);
                            vm.leftSideController.checkAndDrawChart(attr);

                        });
                    }

                }
            }
            vm.leftSideView = {
                render: function () {
                    vm.Attributes = {};
                    vm.chartDraw = vm.leftSideController.onAttributeSelect;
                    // vm.chartSelect=vm.leftSideController.updateAttribute;//
                    $scope.charts = vm.leftSideController.getCharts();
                    $scope.aggregates = vm.leftSideController.getAggregates();
                    $scope.addReport = vm.leftSideController.addNewContainer;
                }
            };

            vm.leftSideController.init();

            // ..................Right side
            // .....................................................

            vm.rightSideModel = {
                axisObject: sketch.axisData,
                AggregateObject: sketch.AggregatesObject,
                axisData: sketch.attributesData,
                activeAxisConfig: null
            }
            vm.rightSideController = {
                init: function () {
                    vm.rightSideView.init();
                    vm.checkboxModel = {};
                    vm.checkboxMD = {};
                    vm.filtersToApply = [];
                    vm.filtersTo = [];
                    var newDim = [];
                    vm.styleSettings = {};
                    vm.columnAddValue = function (type, value) {
                        var tableColumnIndex = vm.rightSideController
                            .findIndexofActiveContainer(
                                vm.tableColumns,
                                "value", value);
                        vm.tableColumns[tableColumnIndex].type = [];
                        vm.tableColumns[tableColumnIndex].type
                            .push(type);
                        // $('.advChartSettingsModal').drawer('close');
                        generate("success", "Move To Column Successfully");
                    }
                    vm.chartStyle = function (reportContainer) {
                        vm.styleSettings = {};
                        vm.DashboardController
                            .onReportContainerClick(reportContainer.id);
                        if (vm.DashboardModel.Dashboard.Report[vm.DashboardModel.Dashboard.activeReportIndex].chart.styleSettings) {
                            vm.styleSettings = vm.DashboardModel.Dashboard.Report[vm.DashboardModel.Dashboard.activeReportIndex].chart.styleSettings;
                            $('.chartStyle').drawer('open');
                        } else if (vm.DashboardModel.Dashboard.Report[vm.DashboardModel.Dashboard.activeReportIndex].chartObject != undefined) {
                            vm.cWidthshow = false;
                            vm.styleSettings.rotateLabel = 0;
                            vm.styleSettings.rotateChart = 0;
                            vm.styleSettings.resizable = "false";
                            vm.styleSettings.translateX = 0;
                            vm.styleSettings.translateY = 0;
                            var index = reportContainer.id - 1;
                            var chart = vm.DashboardModel.Dashboard.Report[vm.DashboardModel.Dashboard.activeReportIndex].chart;
                            var chartObj = vm.DashboardModel.Dashboard.Report[vm.DashboardModel.Dashboard.activeReportIndex].chartObject;
                            // Push Value to input fields
                            vm.styleSettings.chartColor = "#1f77b4";
                            if (typeof chartObj !== 'undefined'
                                && chartObj.margins) {
                                vm.styleSettings.xAxisMargin = chartObj
                                    .margins().bottom;
                                vm.styleSettings.yAxisMargin = chartObj
                                    .margins().left;
                            }
                            vm.styleSettings.cHeight = $(
                                "#chart-"
                                + reportContainer.id)
                                .height();
                            vm.styleSettings.cWidth = $(
                                "#chart-"
                                + reportContainer.id)
                                .width();
                            vm.styleSettings.xAxisLabelName = $(
                                "#chart-"
                                + reportContainer.id)
                                .find(".x-axis-label")
                                .text();
                            vm.styleSettings.yAxisLabelName = $(
                                "#chart-"
                                + reportContainer.id)
                                .find(".y-axis-label")
                                .text();
                            vm.DashboardModel.Dashboard.Report[vm.DashboardModel.Dashboard.activeReportIndex].chart.styleSettings = vm.styleSettings;

                            $('.chartStyle').drawer('open');
                            var processedData = [];
                            vm.rightSideModel.axisData
                                .forEach(function (obj) {
                                    chart.attributes
                                        .forEach(function (chartType) {
                                            if (chartType.indexOf(obj.Name) != -1) {
                                                // chartSettingsObj[obj.modelName]='{"key":"","value":"","type":[""]}';
                                                obj.visible = true;
                                                processedData.push(obj);
                                            }
                                        });
                                });
                            vm.processStyle = processedData;
                        }
                    }
                },
                getMeasures: function () {
                    var tempArray = [];
                    vm.tableColumns.forEach(function (obj) {
                        if (obj.type == "Measure")
                            tempArray.push(obj);
                        if (obj.type == "MeasureCustom")
                            tempArray.push(obj);
                    });
                    return tempArray;
                },

                setActiveAxisConfig: function (config) {
                    vm.rightSideModel.activeAxisConfig = config;
                },
                getActiveAxisConfig: function () {
                    return vm.rightSideModel.activeAxisConfig;
                },
                getDimensions: function () {
                    var tempArray = [];
                    var pushSkip = 0;
                    vm.tableColumns.forEach(function (obj) {

                        if (obj.type == "Dimension")
                            tempArray.push(obj);
                        if (obj.type == "DimensionCustom")
                            tempArray.push(obj);

                    });
                    return tempArray;
                },
                columnDataPush: function (obj) {
                    if (obj.columnType == "Measure") {
                        obj.columnData = vm.rightSideController
                            .getMeasures();
                        return obj;
                    } else if (obj.columnType == "Dimension") {
                        obj.columnData = vm.rightSideController
                            .getDimensions();
                        return obj;

                    } else if (obj.columnType == "Aggregate") {
                        obj.columnData = vm.rightSideModel.AggregateObject;
                        return obj;
                    }
                },

                getAxisRenderObj: function (selected, type) {
                    if (type == "chart") {

                    }
                    var chartSettingsObj = {};
                    var dimensionArray = [];
                    chartSettingsObj['dimension'] = vm.checkboxModelDimension;
                    chartSettingsObj['measure'] = vm.checkboxModelMeasure;
                    chartSettingsObj['aggregate'] = [{
                        "key": "sumIndex",
                        "value": "Sum",
                        "type": "Aggregate"
                    }];
                    var processedData = [];
                    vm.chartSettings = chartSettingsObj;
                    vm.DashboardModel.Dashboard.activeReport.chart = selected;
                    vm.rightSideController.onAxisValueChange();
                    // return finalData;

                    /*
                          * vm.rightSideModel.axisObject.forEach(function(obj) {
                          *
                          * chartTypeObj.requiredAxis.forEach(function(chartType) {
                          *
                          * if (chartType.indexOf(obj.Name) != -1) { //
                          * chartSettingsObj[obj.modelName]='{"key":"","value":"","type":[""]}';
                          * obj.visible = true;
                          * processedData.push(obj); } }
                          *
                          * chartTypeObj.requiredAxis.forEach(function(chartType) {
                          * if(obj.Name==chartType){ //
                          * chartSettingsObj[obj.modelName]='{"key":"","value":"","type":[""]}';
                          * obj.visible=true;
                          * processedData.push(obj); } } );
                          *
                          * });
                          */

                    /*
                          * var finalData = [];
                          * processedData.forEach(function(obj) {
                          * finalData.push(vm.rightSideController.columnDataPush(obj)); //
                          * vm.chartSettings();
                          *
                          * });
                          */

                },

                findIndexofActiveContainer: function (arraytosearch, key, valuetosearch) {
                    for (var i = 0; i < arraytosearch.length; i++) {

                        if (arraytosearch[i][key] == valuetosearch) {
                            return i;
                        }
                    }
                    return null;

                },

                onAxisValueChange: function () {

                    /*
                          * if
                          * (JSON.parse(vm.chartSettings["Xaxis"]).key ==
                          * "DATETIME")
                          * $("#dateFormatChangeModal").modal();
                          */
                    var drawn = sketch
                        .axisConfig(vm.chartSettings)
                        .data(vm.tableData)
                        .container(vm.DashboardModel.Dashboard.activeReport.reportContainer.id)
                        .chartConfig(vm.DashboardModel.Dashboard.activeReport.chart)
                        .render();

                    var index = vm.rightSideController
                        .findIndexofActiveContainer(
                            $scope.dashboardPrototype.reportContainers,
                            "id",
                            vm.DashboardModel.Dashboard.activeReport.reportContainer.id);

                    if (drawn) {
                        $scope.filterSave = true;
                        $scope.saveState = 1;

                        $scope.dashboardPrototype.reportContainers[index]['image'] = null;

                    }

                    vm.rightSideController
                        .setActiveAxisConfig(vm.chartSettings);
                    var indexOfReport = vm.DashboardController
                        .findIndexofActiveContainer(
                            vm.DashboardModel.Dashboard.Report,
                            "id",
                            vm.DashboardModel.Dashboard.activeReport.reportContainer.id);


                    vm.DashboardModel.Dashboard.Report[indexOfReport].axisConfig = new Object(
                        vm.chartSettings);
                    if (drawn) {
                        drawn["chartTypeAttribute"] = vm.DashboardModel.Dashboard.activeReport.chart.name;
                        drawn["resize"] = "ON";
                        vm.DashboardModel.Dashboard.Report[indexOfReport]["chartObject"] = drawn;

                    }
                    vm.DashboardModel.Dashboard.activeReport = vm.DashboardModel.Dashboard.Report[indexOfReport];

                    vm.DashboardModel.Dashboard.activeReportIndex = indexOfReport;

                }
            }

            vm.rightSideView = {
                // this part can be improved
                init: function () {
                    // vm.chartDraw=vm.rightSideController.getAxisRenderObj;
                    vm.refreshChartContainer = vm.rightSideController.onAxisValueChange;
                    vm.columnTypeObj = {};
                    $scope.addColumnTypeRightSide = function (columnObj) {

                        vm.hiddenAxis = columnObj.Name;
                        $("#myModal").modal();

                        if (columnObj.columnType
                                .indexOf("Dimension") != -1) {
                            vm.columnTypeObj = vm.rightSideController
                                .getMeasures();

                        } else if (columnObj.columnType
                                .indexOf("Measure") != -1) {
                            vm.columnTypeObj = vm.rightSideController
                                .getDimensions();
                        }

                    }
                    // Calculation part
                    $scope.measureSetting = function (columnObj) {
                        $("#measureModel").modal();
                        columnObj.columnType = "Dimension";
                        if (columnObj.columnType == "Dimension") {
                            vm.columnTypeObj = vm.rightSideController
                                .getMeasures();
                        } else if (columnObj.columnType == "Measure") {
                            vm.columnTypeObj = vm.rightSideController
                                .getDimensions();
                        }
                    }
                    $scope.calMeasureShow = [];
                    $scope.calMeasureDb = [];
                    $scope.measureAdd = function () {
                        // Formula in array

                        $scope.measureName = "";
                        $scope.measureFormula = "";
                    }
                    $scope.measureFormula = "";
                    $scope.insertText = function (elemID, text) {
                        $scope.measureFormula += text;

                        // $scope.measureVal.measureFormula
                        // +=text
                    }
                    // Close Style Type Drawer
                    $scope.styleTypeDrawerClose = function () {
                        $('.chartStyle').drawer('close');
                    }

                    vm.calMeasureArrayForCompare = [];
                    // Measure Add Calculation
                    vm.customMeasure = [];
                    vm.measureAddCal = function () {
                        // sketch.setGroupExpression($scope.expression);
                        var expression = $scope.measureFormula;
                        vm.FormulaArray = expression
                            .split(/([\+\-\*\/])/);
                        vm.expression = expression.replace(
                            /\b[a-z]\w*/ig, "v['$&']")
                            .replace(/[\(|\|\.)]/g, "");
                        vm.calMeasureShow
                            .push({
                                'measureName': $scope.measureName,
                                'measureFormula': $scope.measureFormula
                            });
                        vm.calMeasureDb
                            .push({
                                'measureName': $scope.measureName,
                                'measureFormula': $scope.FormulaArray
                            });
                        vm.calMeasureShow
                            .forEach(function (d) {
                                if (vm.calMeasureArrayForCompare
                                        .indexOf(d.measureName) == -1) {
                                    var newExpression = d.measureFormula
                                        .replace(
                                            /\b[a-z]\w*/ig,
                                            "v['$&']")
                                        .replace(
                                            /[\(|\|\.)]/g,
                                            "");

                                    var testObject = {
                                        "key": "INT",
                                        "value": d.measureName,
                                        "type": ["MeasureCustom"],
                                        "formula": newExpression
                                    };
                                    vm.customMeasure
                                        .push(testObject);
                                    vm.tableColumns
                                        .push(testObject);
                                    $(
                                        '.advChartSettingsModal')
                                        .drawer('close');
                                    generate("success",
                                        "Measure created successfully");
                                } else {
                                    generate("error",
                                        "You Calculation column match to other column");
                                }
                            });
                    }
                    // Measure Validation
                    vm.measureAddValidation = function () {
                        /*
                              * var parts =
                              * $scope.measureFormula.split(/[[\]]{1,2}/);
                              * parts.length--; // the last entry is
                              * dummy, need to take it out var
                              * valExp=parts.join('');
                              */
                        var newExpression = $scope.measureFormula
                            .replace(/\b[a-z]\w*/ig,
                                "v['$&']").replace(
                                /[\(|\|\.)]/g, "");
                        var formula = "v['"
                            + vm.lastColumSelected + "']";
                        var testObject = {
                            "key": "INT",
                            "value": $scope.measureNameValidation,
                            "type": ["MeasureCustom"],
                            "validation": newExpression,
                            "formula": formula
                        };
                        vm.tableColumns.push(testObject);

                        $('.advChartSettingsModal').drawer(
                            'close');
                        generate("success",
                            "Measure created successfully");
                    }
                    $scope.addColumnType = function (columnObj) {
                        vm.hiddenAxis = columnObj.Name;

                        $("#myModal").modal();

                        if (columnObj.columnType == "Dimension") {
                            vm.columnTypeObj = vm.rightSideController
                                .getMeasures();
                        } else if (columnObj.columnType == "Measure") {
                            vm.columnTypeObj = vm.rightSideController
                                .getDimensions();
                        }
                    }
                    // End Calculation Part

                },
                render: function (chartTypeObj) {
                    // vm.chartSettings={};

                    this.data = vm.rightSideController
                        .getAxisRenderObj(chartTypeObj);
                    // vm.rightSideView.resetView(this.data);
                    // $(".select2").select2('remove');

                    vm.axisRenderArray = this.data;

                    setTimeout(function () {
                        $('select').select2();

                    }, 100);

                },
                chartSelectView: {
                    update: function (chartInfo) {
                        $scope.chartType = JSON.stringify(chartInfo);
                    }

                }

            }

            // vm.rightSideController.init();

            // .................................................End
            // Right Side
            // View...........................................................................

            // ...........................Dashboard
            // View...............................................................................
            var maxSizeY;
            if ($stateParams.type == 1) {
                maxSizeY = 2;
            } else {
                maxSizeY = 18;
            }
            vm.DashboardModel = {
                currentActiveGrid: null,
                dashboardPrototypeObject: {
                    id: '1',
                    name: 'Home',
                    reportContainers: []
                },
                reportPrototypeObject: {
                    reportContainer: null,
                    chart: null,
                    axisConfig: null
                },
                gridSettings: {
                    margins: [20, 15],
                    columns: 30,
                    pushing: true,
                    floating: true,
                    swapping: true,
                    maxRows: 10000,
                    /*maxSizeY: maxSizeY,*/
                    rowHeight: '30',
                    /*rows : 20,
                          rowHeight : 50,
                          pushing : true,
                          floating : true,
                          swapping : true,
                          sparse : true,*/

                    collision: {
                        on_overlap_start: function (collider_data) {

                        }
                    },
                    draggable: {
                        enabled: true, // whether dragging
                        // items is supported
                        handle: '.my-class',
                        start: function (event, $element, widget) {

                        },
                        resize: function (event, $element, widget) {

                        },
                        stop: function (event, $element, widget) {
                            /*$(".hiddenBorder").removeClass(
                                  "box-border");*/
                        }
                    },
                    resizable: {
                        enabled: true,
                        handles: ['n', 'e', 's', 'w', 'ne',
                            'se', 'sw', 'nw'],
                        stop: function (event, uiWidget,
                                        $element) {

                            var getHeight = ($("#li_" + vm.DashboardModel.Dashboard.activeReport.reportContainer.id).height()) - 97;
                            var getWidth = $("#li_" + vm.DashboardModel.Dashboard.activeReport.reportContainer.id).width() - 60;
                            //var newHeight = this.resize_coords.data.height;
                            //chart Loading Bar True
                            $("#chart-" + vm.DashboardModel.Dashboard.activeReport.reportContainer.id).height(getHeight);
                            $("#chart-" + vm.DashboardModel.Dashboard.activeReport.reportContainer.id).width(getWidth);
                            //chart Loading Bar True
                            var legendHeight = $("#li_" + vm.DashboardModel.Dashboard.activeReport.reportContainer.id).height();
                            $("#left" + vm.DashboardModel.Dashboard.activeReport.reportContainer.id).height(legendHeight * 60 / 100);
                            var variable = "reportLoading_" + vm.DashboardModel.Dashboard.activeReport.reportContainer.id;
                            vm[variable] = false;
                            //Chart Hide
                            $("#chart-" + vm.DashboardModel.Dashboard.activeReport.reportContainer.id).show();
                            //$("#chart-"+$element.id).width(width).height(height);
                            //                            if (width > 560 && height > 280) {
                            //                                $(".label_legends" + vm.DashboardModel.Dashboard.activeReport.reportContainer.id).show();
                            //                                $("#left" + vm.DashboardModel.Dashboard.activeReport.reportContainer.id).show();
                            //                            }
                            var activeReport = vm.DashboardModel.Dashboard.Report[vm.DashboardModel.Dashboard.activeReportIndex];
                            $("#li_" + uiWidget[0].firstElementChild.id).addClass("li-box-border");
                            $scope.chartLoading = false;
                            $("#chart-" + $element.id).show();
                            setTimeout(function () {
                                var reportContainerId = 'chart-' + vm.DashboardModel.Dashboard.activeReport.reportContainer.id;
                                if (vm.dataCount < $rootScope.localDataLimit) {
                                    eChart.resetChart(reportContainerId);
                                } else {
                                    eChartServer.resetChart(reportContainerId);
                                }
                            }, 300);


                        },
                        start: function (event, uiWidget, $element) {
                            $("#chart-" + $element.id).hide();
                            //chart Loading Bar True
                            var variable = "reportLoading_" + vm.DashboardModel.Dashboard.activeReport.reportContainer.id;
                            vm[variable] = true;
                            //Chart Hide
                            $(".label_legends" + vm.DashboardModel.Dashboard.activeReport.reportContainer.id).hide();
                            $("#chart-" + vm.DashboardModel.Dashboard.activeReport.reportContainer.id).hide();
                            $("#left" + vm.DashboardModel.Dashboard.activeReport.reportContainer.id).hide();
                        }
                    }
                },
                numberWidgetContainerSettings: {
                    id: 0,
                    name: "Number Widget",
                    sizeX: 9,
                    sizeY: 8,
                    minSizeX: 3,
                    minSizeY: 3,
                    maxSizeX: 9,
                    maxSizeY: 8
                },
                commonContainersSettings: {
                    id: 0,
                    name: "Report",
                    sizeX: 6,
                    sizeY: 8,
                    minSizeX: 6,
                    minSizeY: 8
                },
                Dashboard: {
                    activeReportIndex: null,
                    activeReport: null,
                    lastReportId: 0,
                    Report: []
                }
            };
            var report = [];
            vm.DashboardController = {
                init: function () {
                    vm.DashboardView.init();
                    vm.chartLoading = false;
                    vm.checkboxModel = {};
                    //
                    vm.dateRangeArray = [];
                    vm.dateModal = function () {
                        $('#dateModal').modal({
                            backdrop: 'static',
                            keyboard: false
                        });
                        $('#dateModal').modal('show');
                    }
                    var i = 0;
                    vm.filterApplyAdd = function (keys) {
                        vm.blnkFilter = false;
                        var filerTemp = [];
                        vm.filtersToApply = [];
                        vm.filterHSApply = true;
                        var columnObj = "";
                        vm.checkboxModel[keys.columnName] = keys;
                        Object
                            .keys(vm.checkboxModel)
                            .forEach(
                                function (d) {
                                    if (vm.checkboxModel[d] != "0") {
                                        vm.filtersTo[d] = [];
                                        columnObj = vm.checkboxModel[d];
                                        var keyValue = {};
                                        keyValue['key'] = columnObj.columnName;
                                        keyValue['value'] = columnObj.columType;
                                        filerTemp.push(keyValue);

                                        Enumerable.From(vm.tableData)
                                            .Distinct(function (x) {
                                                return x[d];
                                            })
                                            .Select(function (x) {
                                                return x[d];
                                            })
                                            .ToArray()
                                            .forEach(
                                                function (e) {
                                                    vm.filtersTo[d]
                                                        .push(e);
                                                });
                                        // var filterValues={};
                                        // vm.tableData.forEach(function(d){
                                        //     if(!filterValues[d[columnObj.columnName]])
                                        //     {
                                        //        // vm.filtersTo[columnObj.columnName].push(e);
                                        //         filterValues[d[columnObj.columnName]]=true;
                                        //     }
                                        // });
                                        // vm.filtersTo[columnObj.columnName]=Object.keys(filterValues);


                                    }
                                });
                        if (filerTemp.length) {
                            $scope.onApplyFilter = true;
                            $("#dash-border").css("margin-top", "110px");
                        } else {
                            $scope.onApplyFilter = false;
                            $("#dash-border").css("margin-top", "60px");
                        }

                        vm.filtersToApply = filerTemp;

                        setTimeout(function () {
                            /*$('.multielect').multiselect(
                                  {
                                  includeSelectAllOption: true,
                                  enableFiltering: true,
                                  maxHeight: 200
                                  });*/
                            $(".multielect").selectpicker();
                        }, 1000);
                    }
                    vm.filter_add = function () {
                        $('.drawer1').drawer('open');
                    }
                    // Filter Hide Show
                    $scope.filterHideShow = function () {
                        $scope.filterHide = !$scope.filterHide;
                        $scope.filterShow = !$scope.filterShow;
                        $scope.filterHSApply = !$scope.filterHSApply;

                        if ($scope.filterShowHidden) {
                            $scope.filterShowHidden = 0
                        } else {
                            $scope.filterShowHidden = 1;
                        }
                    }
                },
                findIndexofActiveContainer: function (arraytosearch, key, valuetosearch) {

                    for (var i = 0; i < arraytosearch.length; i++) {

                        if (arraytosearch[i].reportContainer[key] == valuetosearch) {

                            return i;
                        }
                    }
                    return null;
                },
                getGridSettings: function () {
                    return vm.DashboardModel.gridSettings;
                },
                getCommonContainerSettings: function () {
                    return {
                        name: "Report",
                        sizeX: 20,
                        sizeY: 15,
                        minSizeX: 7,
                        minSizeY: 5,
                        maxSizeX: 100,
                        maxSizeY: 100
                    };
                },
                getNumberContainerSettings: function () {
                    return {
                        name: "Number Widget",
                        sizeX: 8,
                        sizeY: 4,
                        minSizeX: 3,
                        minSizeY: 3,
                        maxSizeX: 16,
                        maxSizeY: 8,
                        chartType: "Number Widget"
                    };
                },
                onChartDropToContainer: function (event, index, item, external, type, allowedType) {
                    var view = vm.DashboardView;
                    dc.filterAll();
                    vm.isParameterShown = true;
                    $('.drawer').drawer('open');
                    view.removeGridBorder();
                    vm.rightSideView.render(item);
                    vm.DashboardController.addReportContainer(item);
                    vm.rightSideView.chartSelectView.update(item);
                    vm.dimesionColumn = vm.rightSideController.getDimensions();
                },
                onReportContainerClick: function (id) {
                    var activeIndex = "";
                    vm.DashboardModel.Dashboard.Report.forEach(function (d, index) {
                        if (d.reportContainer.id == id) {
                            activeIndex = index;
                        }
                    });
                    if (vm.DashboardModel.Dashboard.Report[activeIndex].chart.name == 'SpeedoMeter') {
                        var active_report = vm.DashboardModel.Dashboard.activeReportIndex;
                        var speedo_ValObj = vm.dashboard_Obj.Report[activeIndex].axisConfig.speedoMeterSettingObject;
                        if (speedo_ValObj != undefined) {
                            vm.speedoMeterSettingObj.lowRange = speedo_ValObj.lowRange;
                            vm.speedoMeterSettingObj.midRange = speedo_ValObj.midRange;
                            vm.speedoMeterSettingObj.highRange = speedo_ValObj.highRange;
                        }
                    }
                    var index = vm.DashboardController.findIndexofActiveContainer(vm.DashboardModel.Dashboard.Report, "id", id);
                    vm.DashboardModel.Dashboard.activeReportIndex = index;
                    $scope.addChartIconBorder($scope.selectedChartInView[id]);
                    $.each(vm.DashboardModel.Dashboard.Report[index].axisConfig.checkboxModelDimension, function (key, value) {
                        if (value.key) {
                            delete value.key;
                            delete value.value;
                        }
                    });
                    $.each(vm.DashboardModel.Dashboard.Report[index].axisConfig.checkboxModelMeasure, function (key, value) {
                        if (value.key) {
                            delete value.key;
                            delete value.value;
                        }
                    });
                    vm.Attributes = vm.DashboardModel.Dashboard.Report[index].axisConfig;
                    vm.DashboardModel.Dashboard.activeReport = vm.DashboardModel.Dashboard.Report[index];
                    vm.selectedChart = vm.DashboardModel.Dashboard.Report[index].chart;
                },
                getDashboardProto: function () {
                    return vm.DashboardModel.dashboardPrototypeObject;
                },
                getReportProto: function () {

                    return {
                        reportContainer: {},
                        chart: {},
                        axisConfig: {}
                    };

                },
                addReportContainer: function (chartInfo) {
                    var flag = true;
                    if ($scope.dashboardPrototype.reportContainers.length != 0)
                        vm.DashboardModel.Dashboard.Report.forEach(function (d, index) {
                            if (!d.chartObject) {
                                //flag = false;
                                flag = true;
                            }
                        });
                    /*if(flag)
                          {*/

                    var reportObject = vm.DashboardController.getReportProto();

                    reportObject.chart = chartInfo;

                    vm.DashboardModel.Dashboard.lastReportId++;
                    this.lastId = vm.DashboardModel.Dashboard.lastReportId;
                    vm.dashboardPrototype.reportContainers['chartType'] = chartInfo.name;
                    if (chartInfo.name == "Number Widget") {
                        vm.DashboardModel.numberWidgetContainerSettings.id = this.lastId;
                        var obj = vm.DashboardController
                            .getNumberContainerSettings();
                        obj['id'] = this.lastId;
                        //obj['chartType'] = chartInfo.name;
                        obj['row'] = 0;
                        obj['col'] = 0;
                        reportObject.reportContainer = obj;
                        vm.DashboardModel.Dashboard.activeReport.reportContainer = reportObject.reportContainer;
                        $scope.selectedChartInView[this.lastId] = sketch.chartData[0];
                    } else {
                        vm.DashboardModel.commonContainersSettings.id = this.lastId;
                        var obj = vm.DashboardController.getCommonContainerSettings();
                        obj['row'] = 0;
                        obj['col'] = 0;
                        obj['id'] = this.lastId;
                        obj['chartType'] = chartInfo.name;
                        reportObject.reportContainer = obj;

                        $scope.selectedChartInView[this.lastId] = sketch.chartData[0];
                        setTimeout(function () {
                            $(".li_box > .handle-se").removeClass("handle-se-hover");
                            $(".li_box").removeClass("li-box-border");
                            $("#li_" + vm.DashboardModel.commonContainersSettings.id + "> .handle-se").addClass("handle-se-hover");
                            $("#li_" + vm.DashboardModel.commonContainersSettings.id).addClass("li-box-border");
                        }, 1);

                    }
                    /*
                          * var $target = $('html,body');
                          * $target.animate({scrollTop:
                          * $target.height()}, 1000);
                          */
                    /*if (flag) {*/
                    reportObject.reportContainer['image'] = dataFactory
                            .baseUrlData()
                        + chartInfo.image;
                    vm.DashboardModel.Dashboard.activeReport = reportObject;
                    vm.DashboardView.addReportContainerView(reportObject.reportContainer);
                    vm.aggregateText[reportObject.reportContainer.id] = "Sum";
                    report.push(reportObject);
                    vm.DashboardModel.Dashboard.Report
                        .push(reportObject);
                    vm.DashboardModel.Dashboard.activeReportIndex = vm.DashboardModel.Dashboard.Report.length - 1;
                    /*}
                          else {
                          dataFactory.errorAlert("can't add more reports till current is empty");
                          }*/

                    var chartID = vm.DashboardModel.Dashboard.activeReport.reportContainer.id;
                    vm.mapObject[chartID] = {};
                    vm.mapObject[chartID].multipleTooltip = [];
                    vm.mapObject[chartID].multipleTooltipObj = [];
                    vm.mapObject[chartID].multipleTooltip_Obj = {};

                },

                updateReport: function () {

                    vm.DashboardModel.Dashboard.activeReport.chart = JSON
                        .parse(vm.chartType);
                    // vm.DashboardModel.Dashboard.activeReportIndex

                },

                getActiveReport: function () {
                    return vm.DashboardModel.Dashboard.activeReport;
                }
            }


            // setting previous data to modal
            vm.setPreviousDataToModal = function (chartData, chartName, dashboardModelData) {
                // setting values to gender Comparison
                if (chartName == 'Male & Female Comparison') {
                    if (chartData) {
                        $scope.genderObject.dim_gender = chartData.dim_gender;
                        $scope.genderObject.mea_gender = chartData.mea_gender;
                        $scope.genderObject.male = chartData.female;
                        $scope.genderObject.female = chartData.male;
                    } else {
                        $scope.genderObject.dim_gender = '';
                        $scope.genderObject.mea_gender = '';
                        $scope.genderObject.male = '';
                        $scope.genderObject.female = '';
                    }
                }

                // setting values to map


                if (chartName == 'Map') {
                    if (chartData) {
                        setTimeout(function () {
                            vm.Multi_Arr = [];
                            var map_Multi_Arr = Object.keys(chartData.measure_map).length - 1;
                            for (var i = 0; i <= map_Multi_Arr; i++) {
                                vm.Multi_Arr.push(i);
                            }
                            vm.addRadius = function () {
                                vm.Multi_Arr.push(vm.Multi_Arr.length);
                            }
                            vm.removeRadius = function (index) {
                                if (index) {
                                    vm.Multi_Arr.splice(index, 1);
                                    $.each(mapObj.measure_map, function (k, v) {
                                        delete mapObj.measure_map[index];
                                    });
                                    $.each(mapObj.multiGroupColor_map, function (k, v) {
                                        delete mapObj.multiGroupColor_map[index];
                                    });
                                    mapObj.multipleTooltip.forEach(function (d, i) {
                                        mapObj.multipleTooltip.splice(index, 1);
                                    });
                                    mapObj.multipleTooltipObj.forEach(function (d, i) {
                                        mapObj.multipleTooltipObj.splice(index, 1)
                                    });
                                }
                            }

                            if ($scope.mapObject == undefined) {
                                $scope.mapObject = {};
                            }

                            $scope.mapObject.measure_map = {};
                            $scope.mapObject.multiGroupColor_map = {};
                            $scope.mapObject.multipleTooltip = {};
                            vm.Attributes['mapObject'] = {};
                            vm.Attributes['mapObject']['measure_map'] = {};
                            vm.Attributes['mapObject']['multiGroupColor_map'] = {};
                            vm.Attributes['mapObject']['multipleTooltip'] = [];

                            vm.mapObject.multipleTooltipObj = chartData.multipleTooltipObj;
                            $scope.$apply();

                            vm.mapObject.multipleTooltip = [];

                            $scope.mapObject.lat_map = chartData.lat_map;
                            $scope.mapObject.long_map = chartData.long_map;

                            $.each(chartData.measure_map, function (key, val) {
                                if (vm.Attributes['mapObject']['measure_map'][key] == undefined) {
                                    $scope.mapObject.measure_map[key] = chartData.measure_map[key];
                                }
                            });

                            $.each(chartData.multiGroupColor_map, function (key, val) {
                                if (vm.Attributes['mapObject']['multiGroupColor_map'][key] == undefined) {
                                    $scope.mapObject.multiGroupColor_map[key] = chartData.multiGroupColor_map[key];
                                }
                            });
                            chartData.multipleTooltip.forEach(function (d, i) {
                                if (vm.Attributes['mapObject']['multipleTooltip'][i] == undefined) {
                                    $scope.mapObject.multipleTooltip[i] = chartData.multipleTooltip[i];
                                }
                            });

                            vm.mapObject.multipleTooltipObj = chartData.multipleTooltipObj;
                            vm.Attributes['mapObject'] = $scope.mapObject;
                            $scope.$apply();

                        }, 300);
                    } else {
                        vm.Multi_Arr = [0];
                        $scope.mapObject.lat_map = '';
                        $scope.mapObject.long_map = '';
                        if ($scope.mapObject.measure_map) {
                            $scope.mapObject.measure_map[0] = '';
                        }
                        if ($scope.mapObject.multiGroupColor_map) {
                            $scope.mapObject.multiGroupColor_map[0] = '';
                        }
                        if ($scope.mapObject.multipleTooltip) {
                            $scope.mapObject.multipleTooltip[0] = '';
                        }
                    }
                }


                // setting values to FloorPlan Comparison
                if (chartName == 'Floor Plan') {
                    if (chartData) {
                        $scope.FloorPlanObject.height = chartData.height;
                        $scope.FloorPlanObject.width = chartData.width;
                        $scope.FloorPlanObject.image = chartData.image;
                        $scope.FloorPlanObject.tooltip = chartData.tooltipData;
                    } else {
                        $scope.FloorPlanObject.height = '';
                        $scope.FloorPlanObject.width = '';
                        $scope.FloorPlanObject.image = '';
                        $scope.FloorPlanObject.tooltip = '';
                        $('#svgFloorImage').val('');
                    }
                }

                // setting values to Pivot Table
                //                          if(chartName == 'Pivot Table'){
                //                          if(chartData){

                //                          }else{
                //                              var clearPivotTable = '';
                //                              $('#output').append(clearPivotTable);
                //                          }
                //                      }


            }


            vm.DashboardView = {

                init: function () {
                    // Gridster Settings
                    $scope.dashboardCommonSettings = vm.DashboardController
                        .getGridSettings();
                    $scope.onDrop = vm.DashboardController.onChartDropToContainer;
                    vm.DashboardView.initDashboardProto();
                    $scope.addBorder = function (id) {
                        /*$(".box").removeClass("box-border");*/
                        $(".li_box").removeClass("li-box-border");
                        $(".li_box > .handle-se").removeClass("handle-se-hover");
                        $("#li_" + id).addClass("li-box-border");
                        $("#li_" + id + "> .handle-se").addClass("handle-se-hover");
                        /*$("#" + id).addClass("box-border");*/
                        vm.DashboardController.onReportContainerClick(id);
                    };
                    vm.numberFormate = function (style) {
                        var styleSettings = {};
                        styleSettings["NumberFormate"] = style;
                        styleSettings["type"] = "Number Widget";
                        var activeDashboardIndex = vm.DashboardModel.Dashboard.activeReportIndex;
                        vm.DashboardModel.Dashboard.Report[activeDashboardIndex]['attributes'] = "Number Widget";
                        sketch.renderAttributes(vm.DashboardModel.Dashboard.Report[activeDashboardIndex].chartObject, styleSettings);
                    }
                    vm.saveChartStyle = function (styleSettings) {

                        var activeDashboardIndex = vm.DashboardModel.Dashboard.activeReportIndex;
                        vm.DashboardModel.Dashboard.Report[activeDashboardIndex]['attributes'] = styleSettings;

                        sketch
                            .renderAttributes(
                                vm.DashboardModel.Dashboard.Report[activeDashboardIndex].chartObject,
                                styleSettings);
                        generate('success',
                            'Chart style apply successfully');
                    }
                },
                autoAdjustChartView: function () {
                    sketch.selfAdjustChart(vm.DashboardModel.Dashboard.activeReport);
                },
                render: function () {
                    vm.chartPivotObject = {};
                    vm.pivotCust_DataObject = {};
                    vm.pivotCust_GrandTotalObject = {};
                    vm.pivotCust_SubTotalObject = {};

                    vm.DashboardModel.Dashboard.Report.forEach(function (d, i) {
                        vm.DashboardView.addReportContainerView(d.reportContainer);
                        $scope.selectedChartInView[d.reportContainer.id] = d.chart;
                        if (d.chart.key == "_pivotTable") {
                            sketch.pivotTableConfig = d.chartObject;
                        }
                        var variable = "reportLoading_" + d.reportContainer.id;
                        vm[variable] = true;
                    });
                    setTimeout(function () {
                        sketchServer._totalReportCount = vm.DashboardModel.Dashboard.Report.length;
                        vm.DashboardModel.Dashboard.Report.forEach(function (d, index) {
                            var variable = "reportLoading_" + d.reportContainer.id;
                            setTimeout(function () {
                                if (d.chart.key != "_numberWidget") {
                                    var getHeight = ($("#li_" + d.reportContainer.id).height()) - 97;
                                    var getWidth = $("#li_" + d.reportContainer.id).width() - 60;
                                    $("#chart-" + d.reportContainer.id).height(getHeight);
                                    $("#chart-" + d.reportContainer.id).width(getWidth);
                                    $("#chart-" + d.reportContainer.id).show();
                                    vm[variable] = false;
                                }
                                if (d.chart.key == "_floorPlanJs") {
                                    sketch.axisConfig(d.axisConfig)
                                        .data(vm.tableData)
                                        .container(d.reportContainer.id)
                                        ._floorPlanDraw();
                                    vm[variable] = false;
                                }

                                // else if (d.chart.key == "_pivotTable") {
                                //     sketch.axisConfig(d.axisConfig)
                                //         .data(vm.tableData)
                                //         .container(d.reportContainer.id)
                                //         ._pivotTableDraw();
                                //     vm[variable]=false;
                                // }
                                else if (d.chart.key == "_mapChartJs") {
                                    if (vm.dataCount < $rootScope.localDataLimit) {
                                        sketch.axisConfig(d.axisConfig)
                                            .container(d.reportContainer.id)
                                            ._mapChartDraw();
                                    } else {
                                        sketchServer.axisConfig(d.axisConfig)
                                            .accessToken($rootScope.accessToken)
                                            .axisConfig(d.axisConfig)
                                            .data(vm.filteredData)
                                            .container(d.reportContainer.id)
                                            .chartConfig(d.chart)
                                            ._mapChartDraw();
                                    }
                                    vm[variable] = false;
                                } else if (d.chart.key == "_maleFemaleChartJs") {
                                    if (vm.dataCount < $rootScope.localDataLimit) {
                                        sketch.axisConfig(d.axisConfig)
                                            .container(d.reportContainer.id)
                                            ._maleFemaleChartDraw();
                                    } else {
                                        sketchServer.axisConfig(d.axisConfig)
                                            .accessToken($rootScope.accessToken)
                                            .axisConfig(d.axisConfig)
                                            .data(vm.filteredData)
                                            .container(d.reportContainer.id)
                                            .chartConfig(d.chart)
                                            ._maleFemaleChartDraw();
                                    }
                                    vm[variable] = false;
                                } else if (d.chart.key == "_PivotCustomized") {
                                    var c_ID = d.reportContainer.id;

                                    $('#pivot_DragDrop_Attr_Area_' + c_ID).css('display', 'none');
                                    // $('#pivot_Setting_Area_' + c_ID).css('display', 'none');

                                    vm.chartPivotObject[c_ID] = d.pivotCust_Object;
                                    vm.pivotCust_DataObject[c_ID] = d.pivotCust_DataObject;
                                    vm.pivotCust_GrandTotalObject[c_ID] = d.pivotCust_GrandTotalObject;
                                    vm.pivotCust_SubTotalObject[c_ID] = d.pivotCust_SubTotalObject;

                                    // if(vm.chartPivotObject == undefined){
                                    // vm.chartPivotObject = {};
                                    // vm.ptCust_DataObj = {};
                                    // vm.ptCust_GrandTotalObj = {};
                                    // vm.ptCust_SubTotalObj = {};
                                    // }

                                    if (vm.chartPivotObject[c_ID] == undefined) {
                                        vm.chartPivotObject[c_ID] = {};
                                        vm.chartPivotObject[c_ID] = d.pivotCust_Object;

                                        vm.ptCust_DataObj[c_ID] = {};
                                        vm.ptCust_DataObj[c_ID] = d.pivotCust_DataObject;
                                        vm.ptCust_GrandTotalObj[c_ID] = {};
                                        vm.ptCust_GrandTotalObj[c_ID] = d.pivotCust_GrandTotalObject;
                                        vm.ptCust_SubTotalObj[c_ID] = {};
                                        vm.ptCust_SubTotalObj[c_ID] = d.pivotCust_SubTotalObject;

                                        var pivotObject = d.axisConfig.pivotCust_Object;
                                        $(".removeClass_" + c_ID).html("");
                                        $("#origin_" + c_ID).html("");
                                        pivotObject.Field.forEach(function (e) {
                                            var tempDiv = '<span class="col-sm-12 draggable_' + c_ID + '  ptCust_Attr_DragDrop ui-draggable ui-draggable-handle " id="' + e + '" title="' + e + '" >' +
                                                '<div class="ng-binding">' + e + '<input type="hidden" value="' + e + '"></div></span>';
                                            $("#origin_" + c_ID).append(tempDiv);
                                        });
                                        pivotObject.Column.forEach(function (e) {
                                            var tempDiv = '<span class="col-sm-12 draggable_' + c_ID + '  removeClass_' + c_ID + ' ptCust_Attr_DragDrop ui-draggable ui-draggable-handle " id="' + e + '" title="' + e + '" >' +
                                                '<div class="ng-binding">' + e + '<input type="hidden" value="' + e + '"></div></span>';
                                            $("#drop_Column_" + c_ID + " > " + ".col-sm-11").append(tempDiv);
                                        });
                                        pivotObject.Row.forEach(function (e) {
                                            var tempDiv = '<span class="col-sm-12 draggable_' + c_ID + ' removeClass_' + c_ID + ' ptCust_Attr_DragDrop ui-draggable ui-draggable-handle " id="' + e + '" title="' + e + '" >' +
                                                '<div class="ng-binding">' + e + '<input type="hidden" value="' + e + '"></div></span>';
                                            $("#drop_Row_" + c_ID + " > " + ".col-sm-11").append(tempDiv);
                                        })
                                        pivotObject.Data.forEach(function (e) {
                                            var tempDiv = '<span class="col-sm-12 draggable_' + c_ID + ' removeClass_' + c_ID + ' ptCust_Attr_DragDrop ui-draggable ui-draggable-handle " id="' + e + '" title="' + e + '" >' +
                                                '<div class="ng-binding">' + e + '<input type="hidden" value="' + e + '"></div></span>';
                                            $("#drop_Data_" + c_ID + " > " + ".col-sm-11").append(tempDiv);
                                        });
                                    }

                                    if (vm.dataCount < $rootScope.localDataLimit) {
                                        sketch.axisConfig(d.axisConfig)
                                            .container(d.reportContainer.id)
                                            .pivotCust_Draw();
                                    } else {
                                        sketchServer.axisConfig(d.axisConfig)
                                            .accessToken($rootScope.accessToken)
                                            .axisConfig(d.axisConfig)
                                            .data(vm.filteredData)
                                            .container(d.reportContainer.id)
                                            .chartConfig(d.chart)
                                            .pivotCust_Draw();
                                    }
                                    vm[variable] = false;
                                } else {
                                    if (vm.dataCount < $rootScope.localDataLimit) {
                                        setTimeout(function () {
                                            if (vm.colorObject && vm.colorObject['chart-' + d.reportContainer.id]) {
                                                eChart.chartRegistry.registerPublicViewColor("chart-" + d.reportContainer.id, vm.colorObject['chart-' + d.reportContainer.id]);
                                            }
                                            sketch
                                                .axisConfig(d.axisConfig)
                                                .data(vm.tableData)
                                                .container(d.reportContainer.id)
                                                .chartConfig(d.chart)
                                                .render(function () {
                                                    vm[variable] = false;
                                                    $scope.$apply();
                                                });
                                        }, 500);
                                    } else {
                                        if (vm.colorObject && vm.colorObject['chart-' + d.reportContainer.id]) {
                                            eChartServer.chartRegistry.registerPublicViewColor("chart-" + d.reportContainer.id, vm.colorObject['chart-' + d.reportContainer.id]);
                                        }
                                        setTimeout(function () {
                                            sketchServer
                                                .accessToken($rootScope.accessToken)
                                                .axisConfig(d.axisConfig)
                                                .data(vm.filteredData)
                                                .container(d.reportContainer.id)
                                                .chartConfig(d.chart)
                                                .render(function () {
                                                    vm[variable] = false;
                                                    $scope.$apply();
                                                });
                                        }, 500);
                                    }
                                }
                                /*if (index == 0) {
                                         vm.Attributes = d.axisConfig;
                                     }*/
                                d.reportContainer.chartType = d.chart.name;
                                if ($stateParams.type == 1) {
                                    var width = $("#li_" + d.reportContainer.id).width();
                                    var height = $("#li_" + d.reportContainer.id).height();
                                    if (width < 600) {
                                        $("#left" + d.reportContainer.id).hide();
                                        $(".label_legends" + d.reportContainer.id).hide();
                                    }
                                } else {
                                    var width = $("#li_" + d.reportContainer.id).width();
                                    var height = $("#li_" + d.reportContainer.id).height();
                                    if (width < 450) {
                                        $("#left" + d.reportContainer.id).hide();
                                        $(".label_legends" + d.reportContainer.id).hide();
                                    }
                                }
                            }, 500 * index);
                        });
                        $('[rel="tooltip"]').tooltip({placement: 'bottom'});
                    }, 1000);
                },
                updateDashboardObject: function (drawn) {
                    var index = vm.rightSideController.findIndexofActiveContainer(
                        $scope.dashboardPrototype.reportContainers, "id",
                        vm.DashboardModel.Dashboard.activeReport.reportContainer.id);
                    if (drawn) {
                        $scope.filterSave = true;
                        $scope.saveState = 1;
                        $scope.dashboardPrototype.reportContainers[index]['image'] = null;
                    }
                    // vm.rightSideController.setActiveAxisConfig(vm.chartSettings);
                    var indexOfReport = vm.DashboardController
                        .findIndexofActiveContainer(
                            vm.DashboardModel.Dashboard.Report,
                            "id",
                            vm.DashboardModel.Dashboard.activeReport.reportContainer.id);

                    vm.DashboardModel.Dashboard.Report[indexOfReport].axisConfig = new Object(
                        vm.Attributes);

                    vm.DashboardModel.Dashboard.Report[indexOfReport].axisConfig["xLabel"] = angular
                        .copy(vm.activeXLabel[vm.DashboardModel.Dashboard.activeReport.reportContainer.id]);
                    vm.DashboardModel.Dashboard.Report[indexOfReport].axisConfig["yLabel"] = angular
                        .copy(vm.activeYLabel[vm.DashboardModel.Dashboard.activeReport.reportContainer.id]);

                    if (drawn) {
                        // drawn["chartTypeAttribute"]=vm.DashboardModel.Dashboard.activeReport.chart.name;
                        // drawn["resize"]="ON";
                        vm.DashboardModel.Dashboard.Report[indexOfReport]["chartObject"] = drawn;
                    }

                    vm.DashboardModel.Dashboard.activeReport = vm.DashboardModel.Dashboard.Report[indexOfReport];
                    vm.DashboardModel.Dashboard.activeReportIndex = indexOfReport;

                },
                resetDataAndChart: function () {
                    return new Promise(
                        function (resolve, reject) {

                            eChart.resetAll();
                            setTimeout(function () {
                                resolve();
                            }, 100);
                        }
                    );
                },


                processChart: function () {

                    /*vm.chartLoading = false;

                         var modelRequired = vm.DashboardModel.Dashboard.activeReport.chart.required.modelRequired;
                         if(modelRequired){
                             vm.chartName = vm.DashboardModel.Dashboard.activeReport.chart.name;
                             var chartData = vm.DashboardModel.Dashboard.activeReport.dataObject;
                             $('#commonModals').modal('show');
                             if(vm.chartName == "Pivot Table" ){

                                 var pivotTableDataConfig = vm.DashboardModel.Dashboard.activeReport.chartObject.config;
     //                          $("#output").pivotUI(vm.tableData, pivotTableDataConfig, true);
                             }


                             vm.chartLoading = true;
                             vm.setPreviousDataToModal(chartData, vm.chartName, vm.DashboardModel);
                         }else{
                             $('#commonModals').modal('hide');
                             vm.chartLoading = false;

                             return new Promise(
                                 function (resolve, reject) {
                                     if(vm.DashboardModel.Dashboard.activeReport.chart.key!='_numberWidget'){
                                         vm.DashboardModel.Dashboard.activeReport.reportContainer.sizeX=vm.DashboardModel.Dashboard.activeReport.chart.containerSize.x;
                                         vm.DashboardModel.Dashboard.activeReport.reportContainer.sizeY=vm.DashboardModel.Dashboard.activeReport.chart.containerSize.y;
                                         $("#chart-" + vm.DashboardModel.Dashboard.activeReport.reportContainer.id).hide();
                                         setTimeout(function(){
                                             var getHeight= ($("#li_"+vm.DashboardModel.Dashboard.activeReport.reportContainer.id).height())-97;
                                             var getWidth= $("#li_"+vm.DashboardModel.Dashboard.activeReport.reportContainer.id).width()-60;
                                             $("#chart-"+vm.DashboardModel.Dashboard.activeReport.reportContainer.id).height(getHeight);
                                             $("#chart-"+vm.DashboardModel.Dashboard.activeReport.reportContainer.id).width(getWidth);
                                             $("#chart-" +vm.DashboardModel.Dashboard.activeReport.reportContainer.id).show();
                                         },1000);
                                     }
                                     if (!$.isEmptyObject(vm.rangeObject)) {
                                         vm.Attributes['timeline'] === vm.rangeObject;
                                         sketch
                                             .axisConfig(vm.Attributes)
                                             .data(vm.filteredData)
                                             .container(vm.DashboardModel.Dashboard.activeReport.reportContainer.id)
                                             .chartConfig($scope.selectedChartInView[vm.DashboardModel.Dashboard.activeReport.reportContainer.id])
                                             .render(function (drawn) {
                                                 $scope.lastDrawnChart[vm.DashboardModel.Dashboard.activeReport.reportContainer.id] = vm.DashboardModel.Dashboard.activeReport.chart.key;
                                                 resolve(drawn);
                                             });
                                     }
                                     else {
                                         sketch
                                             .axisConfig(vm.Attributes)
                                             .data(vm.tableData)
                                             .container(vm.DashboardModel.Dashboard.activeReport.reportContainer.id)
                                             .chartConfig($scope.selectedChartInView[vm.DashboardModel.Dashboard.activeReport.reportContainer.id])
                                             .render(
                                                 function (drawn) {
                                                     $scope.lastDrawnChart[vm.DashboardModel.Dashboard.activeReport.reportContainer.id] = vm.DashboardModel.Dashboard.activeReport.chart.key;
                                                     resolve(drawn);
                                                 });
                                     }
                                 }
                             );
                         }*/
                    if (vm.dataCount < $rootScope.localDataLimit) {
                        //Without server side
                        vm.chartLoading = false;
                        if (vm.DashboardModel.Dashboard.activeReport.chart.key != '_numberWidget') {
                            vm.DashboardModel.Dashboard.activeReport.reportContainer.sizeX = vm.DashboardModel.Dashboard.activeReport.chart.containerSize.x;
                            vm.DashboardModel.Dashboard.activeReport.reportContainer.sizeY = vm.DashboardModel.Dashboard.activeReport.chart.containerSize.y;
                            $scope.$apply();
                            $("#chart-" + vm.DashboardModel.Dashboard.activeReport.reportContainer.id).hide();
                            setTimeout(function () {
                                var getHeight = ($("#li_" + vm.DashboardModel.Dashboard.activeReport.reportContainer.id).height()) - 97;
                                var getWidth = $("#li_" + vm.DashboardModel.Dashboard.activeReport.reportContainer.id).width() - 60;
                                $("#chart-" + vm.DashboardModel.Dashboard.activeReport.reportContainer.id).height(getHeight);
                                $("#chart-" + vm.DashboardModel.Dashboard.activeReport.reportContainer.id).width(getWidth);
                                $("#chart-" + vm.DashboardModel.Dashboard.activeReport.reportContainer.id).show();
                                $scope.$apply();
                            }, 1000);
                        }

                        var modelRequired = vm.DashboardModel.Dashboard.activeReport.chart.required.modelRequired;
                        if (modelRequired) {
                            $('#commonModals').modal('show');
                            if (vm.chartName == "Pivot Table") {
                                if (vm.DashboardModel.Dashboard.activeReport.chartObject && vm.DashboardModel.Dashboard.activeReport.chartObject.recall) {
                                    var pivotTableDataConfig = vm.DashboardModel.Dashboard.activeReport.chartObject.config;
                                    $("#output").pivotUI(vm.tableData, pivotTableDataConfig, true);
                                }
                            }
                            vm.chartLoading = true;
                            vm.chartName = vm.DashboardModel.Dashboard.activeReport.chart.name;
                            var chartData = vm.DashboardModel.Dashboard.activeReport.dataObject;
                            vm.setPreviousDataToModal(chartData, vm.chartName, vm.DashboardModel);
                        } else {
                            $('#commonModals').modal('hide');
                            vm.chartLoading = false;
                            return new Promise(
                                function (resolve, reject) {
                                    try {
                                        setTimeout(function () {
                                            if (!$.isEmptyObject(vm.rangeObject)) {
                                                vm.Attributes['timeline'] === vm.rangeObject;
                                                sketch
                                                    .axisConfig(vm.Attributes)
                                                    .data(vm.filteredData)
                                                    .container(vm.DashboardModel.Dashboard.activeReport.reportContainer.id)
                                                    .chartConfig($scope.selectedChartInView[vm.DashboardModel.Dashboard.activeReport.reportContainer.id])
                                                    .render(
                                                        function (drawn) {
                                                            $scope.lastDrawnChart[vm.DashboardModel.Dashboard.activeReport.reportContainer.id] = vm.DashboardModel.Dashboard.activeReport.chart.key;
                                                            resolve(drawn);
                                                        });
                                            } else {

                                                sketch
                                                    .axisConfig(vm.Attributes)
                                                    .data(vm.tableData)
                                                    .container(vm.DashboardModel.Dashboard.activeReport.reportContainer.id)
                                                    .chartConfig($scope.selectedChartInView[vm.DashboardModel.Dashboard.activeReport.reportContainer.id])
                                                    .render(
                                                        function (drawn) {
                                                            $scope.lastDrawnChart[vm.DashboardModel.Dashboard.activeReport.reportContainer.id] = vm.DashboardModel.Dashboard.activeReport.chart.key;
                                                            resolve(drawn);
                                                        });
                                            }
                                            $("#chart-" + vm.DashboardModel.Dashboard.activeReport.reportContainer.id).show();
                                        }, 1200);
                                    } catch (e) {
                                    }
                                }
                            );
                        }
                    } else {
                        /*
                              *   Server side
                              */
                        var variable = "reportLoading_" + vm.DashboardModel.Dashboard.activeReport.reportContainer.id;
                        vm[variable] = true;
                        vm.chartLoading = false;
                        vm.Attributes['queryObj'] = JSON.stringify(vm.queryObj);

                        if (vm.DashboardModel.Dashboard.activeReport.chart.key != '_numberWidget') {
                            vm.DashboardModel.Dashboard.activeReport.reportContainer.sizeX = vm.DashboardModel.Dashboard.activeReport.chart.containerSize.x;
                            vm.DashboardModel.Dashboard.activeReport.reportContainer.sizeY = vm.DashboardModel.Dashboard.activeReport.chart.containerSize.y;
                            $scope.$apply();
                            $("#chart-" + vm.DashboardModel.Dashboard.activeReport.reportContainer.id).hide();
                            setTimeout(function () {
                                var getHeight = ($("#li_" + vm.DashboardModel.Dashboard.activeReport.reportContainer.id).height()) - 97;
                                var getWidth = $("#li_" + vm.DashboardModel.Dashboard.activeReport.reportContainer.id).width() - 60;
                                $("#chart-" + vm.DashboardModel.Dashboard.activeReport.reportContainer.id).height(getHeight);
                                $("#chart-" + vm.DashboardModel.Dashboard.activeReport.reportContainer.id).width(getWidth);
                                $("#chart-" + vm.DashboardModel.Dashboard.activeReport.reportContainer.id).show();
                            }, 1000);
                        }

                        var modelRequired = vm.DashboardModel.Dashboard.activeReport.chart.required.modelRequired;

                        if (modelRequired) {
                            $('#commonModals').modal('show');
                            if (vm.chartName == "Pivot Table") {
                                if (vm.DashboardModel.Dashboard.activeReport.chartObject && vm.DashboardModel.Dashboard.activeReport.chartObject.recall) {
                                    var pivotTableDataConfig = vm.DashboardModel.Dashboard.activeReport.chartObject.config;
                                    $("#output").pivotUI(vm.tableData, pivotTableDataConfig, true);
                                }
                            }
                            vm.chartLoading = true;
                            vm.chartName = vm.DashboardModel.Dashboard.activeReport.chart.name;
                            var chartData = vm.DashboardModel.Dashboard.activeReport.dataObject;
                            vm.setPreviousDataToModal(chartData, vm.chartName, vm.DashboardModel);
                        } else {
                            $('#commonModals').modal('hide');
                            vm.chartLoading = false;
                            return new Promise(
                                function (resolve, reject) {
                                    try {
                                        setTimeout(function () {
                                            if (!$.isEmptyObject(vm.rangeObject)) {
                                                vm.Attributes['timeline'] === vm.rangeObject;
                                                sketchServer
                                                    .accessToken($rootScope.accessToken)
                                                    .axisConfig(vm.Attributes)
                                                    .data(vm.filteredData)
                                                    .container(vm.DashboardModel.Dashboard.activeReport.reportContainer.id)
                                                    .chartConfig($scope.selectedChartInView[vm.DashboardModel.Dashboard.activeReport.reportContainer.id])
                                                    .render(
                                                        function (drawn) {
                                                            $scope.lastDrawnChart[vm.DashboardModel.Dashboard.activeReport.reportContainer.id] = vm.DashboardModel.Dashboard.activeReport.chart.key;
                                                            resolve(drawn);
                                                        });
                                            } else {
                                                // vm.Attributes['queryObj']=vm.Attributes['queryObj'];
                                                // vm.Attributes['queryObj'] = JSON.stringify(vm.Attributes['queryObj']);
                                                sketchServer
                                                    .accessToken($rootScope.accessToken)
                                                    .axisConfig(vm.Attributes)
                                                    .data([])
                                                    .container(vm.DashboardModel.Dashboard.activeReport.reportContainer.id)
                                                    .chartConfig($scope.selectedChartInView[vm.DashboardModel.Dashboard.activeReport.reportContainer.id])
                                                    .render(
                                                        function (drawn) {
                                                            $scope.lastDrawnChart[vm.DashboardModel.Dashboard.activeReport.reportContainer.id] = vm.DashboardModel.Dashboard.activeReport.chart.key;
                                                            resolve(drawn);
                                                            var variable = "reportLoading_" + vm.DashboardModel.Dashboard.activeReport.reportContainer.id;
                                                            vm[variable] = false;
                                                        });
                                            }
                                            $("#chart-" + vm.DashboardModel.Dashboard.activeReport.reportContainer.id).show();
                                        }, 1200);
                                    } catch (e) {

                                    }
                                }
                            );
                        }
                    }


                },


                renderChartInActiveContainer: function () {
                    // sketch.pivotTableConfig = undefined;
                    try {
                        vm.DashboardView
                            .resetDataAndChart()
                            .then(vm.DashboardView.processChart)
                            .then(vm.DashboardView.updateDashboardObject)
                            .then(function () {
                                vm.chartLoading = false;
                            })
                            .then(function () {
                                var variable = "reportLoading_" + vm.DashboardModel.Dashboard.activeReport.reportContainer.id;
                                vm[variable] = false;
                                $scope.$apply();
                                $(".label_legends" + vm.DashboardModel.Dashboard.activeReport.reportContainer.id).show();
                                return true;
                            });
                    } catch (e) {
                        vm.chartErrorText = "Some Error occured while drawing this chart";
                        vm.chartLoading = false;
                    }
                    return false;
                },

                initDashboardProto: function () {
                    $scope.dashboardPrototype = vm.DashboardController
                        .getDashboardProto();

                },
                // render dashboard view
                addReportContainerView: function (reportContainerObject) {
                    $scope.dashboardPrototype.reportContainers.push(reportContainerObject);
                },

                selectReportContainerView: function (reportContainerObject) {

                },

                showGridBorder: function () {
                    $("#dash-border").addClass("dash-border");
                },

                removeGridBorder: function () {
                    $("#dash-border")
                        .removeClass("dash-border");

                }
            }
            vm.DashboardController.init();

            $("#loadscreen").show();
            $scope.listdata = [];
            $scope.listOf = false;
            $scope.dashName.dashboardName = "";
            $scope.dashboardDesc = "Dashboard Description"

            vm.containerLoaded = false;
            vm.isSmallSidebar = true;
            vm.isParameterShown = false;
            vm.isNoContainerLoaded = true;
            vm.ischartTypeShown = false;
            vm.isQuerySelected = true;
            vm.placeholder = true;
            vm.filtersTo = [];
            vm.lastAppliedFilters = {};
            vm.saveState = -1;
            // charts list
            vm.MetaDataLoader = true;
            // Function for fetching metadata list
            var dashboardView = function () {
                var promise = new Promise(
                    function (resolve, reject) {

                        /*
                              * dataFactory
                              .dashboardViewList()
                              .then(
                              function(response) {

                              $scope.viewLoaded = true;
                              $scope.viewLoad = true;
                              vm.dashboardList = response.data.data;
                              resolve();
                              },
                              function(error) {
                              $scope.status = 'Unable to load customer data: '
                              + error.message;
                              });*/
                    });
                return promise;
            };
            dashboardView().then(function () {
                vm.MetaDataLoader = false;
            });
            vm.toggleDrawer = function () {
                $(".loadingBar").show();
                $(".sideSubBarWidth1").toggleClass("drawerClose", 'drawerOpen');
                $(".sideSubBarWidth2").toggleClass("drawerFullSize", '');
                setTimeout(function () {
                    $(window).resize();
                }, 800);
                setTimeout(function () {
                    vm.DashboardModel.Dashboard.Report.forEach(function (d) {
                        var getHeight = ($("#li_" + d.reportContainer.id).height()) - 97;
                        var getWidth = ($("#li_" + d.reportContainer.id).width()) - 60;
                        //chart Loading Bar True
                        //$("#chart-"+d.reportContainer.id).height(getHeight);
                        //$("#chart-"+d.reportContainer.id).width(getWidth);
                    });
                }, 2000);
                setTimeout(function () {
                    //$(window).trigger('resize');
                }, 4000);
                $(".loadingBar").hide();
            }
            $scope.fullNavShow = function () {
                $scope.fullNav = true;
                $scope.smallNav = true;
                $(".be-content").removeClass("toggleNav");
                onresize();
            }
            $scope.fullNavHide = function () {
                $scope.fullNav = false;
                $scope.smallNav = false;
                $(".be-content").addClass("toggleNav");
                onresize();
            }

            $scope.publicDashboard = function (d) {
                $location.path('/public-view').search({
                    param: JSON.stringify(d)
                });
            }
            $scope.viewDashboard = function (d) {
                $state.go("app.view-dashboard", {
                    param: JSON.stringify(d)
                });
            }
            $scope.EditDashboard = function (d) {
                $state.go("app.dashboard-edit", {
                    param: JSON.stringify(d)
                });
            }
            // Api For Getting Connection

            var fetchDataAndColumns = function () {
                var promise = new Promise(
                    function (resolve, reject) {
                        var data = {
                            "matadataObject": JSON.stringify(vm.metadataObject),
                            "type": 'dashboard'
                        };
                        dataFactory.request($rootScope.DashboardData_Url, 'post', data).then(function (response) {
                            if (response.data.errorCode == 1) {
                                vm.blnkData = true;
                                vm.blnkFilter = true;
                                vm.blnkChart = true;
                                vm.blnkSource = true;
                                vm.tableData = JSON.parse(response.data.result.tableData);
                                vm.metadataObject = vm.metadataObject;
                                if (vm.metadataObject.connObject.column != undefined)
                                    vm.tableColumns = Object.assign([], vm.metadataObject.connObject.column);
                                else
                                    vm.tableColumns = Object.assign([], vm.metadataObject.column);
                                // $scope.groupObject
                                calculation
                                    .oldObject(JSON.parse(response.data.result.tableColumn))
                                    .newObject(vm.tableColumns)
                                    .tabledata(vm.tableData)
                                    .uniqueArray()
                                    .columnCal()
                                    .groupObject(vm.categoryGroupObject)
                                    .columnCategory();
                                vm.tableData = calculation._tabledata;

                                setTimeout(function () {
                                    resolve();
                                }, 6000);
                                //Filter multiselect
                            }
                        });
                    });
                return promise;

            };
            /*
                  *  Server side Fetch Column
                  */
            var fetchDataAndColumnsServer = function () {
                var promise = new Promise(
                    function (resolve, reject) {
                        var data = {
                            "matadataObject": JSON.stringify(vm.metadataObject),
                            "type": 'dashboard'
                        };
                        /*
                              * Only for cache data to server
                              */
                        var responseColumn = "";
                        dataFactory.request($rootScope.DashboardDataCacheToRedis_Url, 'post', data).then(function (response) {
                            if (response.data.errorCode == 1) {
                                vm.blnkData = true;
                                vm.blnkFilter = true;
                                vm.blnkChart = true;
                                vm.blnkSource = true;
                                responseColumn = JSON.parse(response.data.result.tableColumn);
                                //vm.metadataObject = vm.metadataObject);
                                /*if(vm.metadataObject.connObject.type!=undefined){
                                         var columns = JSON.parse(response.data.result.tableColumn);
                                         var keysArray = [];
                                         var k = 0;
                                         $.each(columns,function(key,d){
                                             keysArray[k] = {
                                                 "columType" : d.Type,
                                                 "tableName" : d.tableName,
                                                 "columnName" : d.Field,
                                                 "reName" : d.Field,
                                                 "dataKey" : d.dataType,
                                                 "type" : "defined"
                                             };
                                             k++;
                                         });
                                         vm.tableColumns=keysArray;
                                     }
                                     else{
                                         vm.tableColumns = vm.metadataObject.connObject.column;
                                     }*/
                                var columns = vm.metadataObject.connObject.column;
                                var data = {
                                    metadataId: vm.metadataId,
                                    dashboardId: $scope.dashboardId,
                                    columns: columns
                                };
                                dataFactory.request($rootScope.GetColumn_Url, 'post', data).then(function (response) {
                                    //vm.tableColumns = ;//Object.assign([],vm.metadataObject.connObject.column);
                                    var obj = response.data.result;
                                    var arr = Object.keys(obj).map(function (key) {
                                        return obj[key];
                                    });
                                    arr.forEach(function (d) {
                                        delete d['$$hashKey'];
                                    });
                                    vm.tableColumns = arr;
                                }).then(function () {
                                    /*if(vm.metadataObject.connObject.column!=undefined)
                                             vm.tableColumns = Object.assign([],vm.metadataObject.connObject.column);
                                         else
                                             vm.tableColumns = Object.assign([],vm.metadataObject.column);*/
                                    $rootScope.initializeNodeClient(vm.metadataObject).then(function () {
                                        calculationServer
                                            .oldObject(responseColumn)
                                            .newObject(vm.tableColumns)
                                            .dataGroupCalculation(vm.metadataId, $rootScope.accessToken, vm.categoryGroupObject);
                                        resolve();
                                    });
                                    setTimeout(function () {
                                        $("#filter").selectpicker('destroy');
                                        $("#filter").selectpicker();
                                        vm.chartSettingObject = {};
                                    }, 1000);
                                });
                                /*
                                      *  Resolve table column to show
                                      */

                            } else {
                                dataFactory.errorAlert(response.data.errorCode);
                            }
                        })
                    });
                return promise;
            };
            // Floor Plan Comparison

            vm.FloorPlanData = function (FloorPlanData) {
                vm.Attributes.FloorPlanObject = {};
                var f = document.getElementById('svgFloorImage').files[0];
                var r = new FileReader();
                r.onloadend = function (e) {
                    $scope.data = e.target.result;
                    var img = document.getElementById('svgFloorImage');
                    vm.imageUrl = 'data:image/svg+xml;base64,' + btoa(e.target.result);

                    vm.setFloorPlanData(vm.imageUrl);
                    img.imageSrc = 'data:image/svg+xml;base64,' + btoa(e.target.result);
                }
                r.readAsBinaryString(f);
            }

            // Floor Plan Data
            vm.setFloorPlanData = function (imgUrl) {

                var measure = Object.values(vm.Attributes.checkboxModelMeasure)[0];
                var dimension = Object.values(vm.Attributes.checkboxModelDimension)[0];

                var yGrp = {}, grpData = [];
                vm.DimensionFloorArr = [];
                var xDim = sketch._createDimension(dimension);

                yGrp[measure.columnName] = sketch._createGroup(measure, xDim);
                yGrp[measure.columnName].all().forEach(function (d) {
                    grpData.push(d)
                    vm.DimensionFloorArr.push(d.key);
                });

                if (vm.Attributes.FloorPlanObject) {
                    vm.Attributes['FloorPlanObject']['height'] = vm.FloorPlanObject.height;
                    vm.Attributes['FloorPlanObject']['width'] = vm.FloorPlanObject.width;
                    vm.Attributes['FloorPlanObject']['image'] = imgUrl;
                    vm.Attributes['FloorPlanObject']['measure'] = measure;
                    vm.Attributes['FloorPlanObject']['grpData'] = grpData;
                    vm.Attributes['FloorPlanObject']['tooltip'] = sketch.FloorToolTip;
                    vm.Attributes['FloorPlanObject']['tooltipData'] = vm.FloorPlanObject.tooltip;
                } else {
                    vm.Attributes['FloorPlanObject']['height'] = 400;
                    vm.Attributes['FloorPlanObject']['width'] = 600;
                    vm.Attributes['FloorPlanObject']['image'] = imgUrl;
                    vm.Attributes['FloorPlanObject']['measure'] = measure;
                    vm.Attributes['FloorPlanObject']['grpData'] = grpData;
                    vm.Attributes['FloorPlanObject']['tooltip'] = sketch.FloorTooltip;
                    vm.Attributes['FloorPlanObject']['tooltipData'] = vm.FloorPlanObject.tooltip;
                }
                $('#commonModals').modal('hide');

                vm.DashboardModel.Dashboard.activeReport['dataObject'] = vm.Attributes['FloorPlanObject'];

                sketch.axisConfig(vm.Attributes)
                    .container(vm.DashboardModel.Dashboard.activeReport.reportContainer.id)
                    ._floorPlanDraw(vm.Attributes.FloorPlanObject, measure, grpData);
            }


            // Building Comparison
            vm.BuildingObject = {};
            vm.BuildingData = function (BuildingData) {
                vm.Attributes.BuildingObject = {};
                if (vm.Attributes.BuildingObject) {
                    vm.Attributes.BuildingObject.row = vm.BuildingObject.row;
                    vm.Attributes.BuildingObject.column = vm.BuildingObject.column;
                } else {
                    vm.Attributes.BuildingObject.row = 3;
                    vm.Attributes.BuildingObject.column = 7;
                }

                sketch.axisConfig(vm.Attributes)
                    .container(vm.DashboardModel.Dashboard.activeReport.reportContainer.id)
                    ._buildingChartDraw(vm.Attributes.BuildingObject.row, vm.Attributes.BuildingObject.column);

                $('#Building-Comparison').modal('hide');
            }


            // pivotCustomized Start

            // selected Measure_Dimension Start
            vm.chartPivotObject = {};
            vm.pivotAttributes = function (checkType, attr, attrObj) {
                var container_ID = vm.DashboardModel.Dashboard.activeReport.reportContainer.id;
                $('#pivotCustomized_TableDiv_' + container_ID).css('display', 'none');
                // $('#pivot_Setting_Area_' + container_ID).css('display', 'none');
                $('#pivot_DragDrop_Attr_Area_' + container_ID).css('display', 'block');
                if (vm.chartPivotObject[container_ID] == undefined) {
                    vm.chartPivotObject[container_ID] = {};
                    vm.chartPivotObject[container_ID]['Field'] = [];
                    vm.chartPivotObject[container_ID]['Column'] = [];
                    vm.chartPivotObject[container_ID]['Row'] = [];
                    vm.chartPivotObject[container_ID]['Data'] = [];
                }
                if (attrObj.dataKey != undefined) {
                    if (checkType) {
                        vm.chartPivotObject[container_ID]['Field'].push(attr);
                    } else {
                        //$("#li_" + container_ID).find("#" + escape(attr)).remove();
                        $("[id='" + attr + "']").remove();
                        if (vm.chartPivotObject[container_ID]['Field'].includes(attr)) {
                            var attrIndex = vm.chartPivotObject[container_ID]['Field'].indexOf(attr);
                            vm.chartPivotObject[container_ID]['Field'].splice(attrIndex, 1);
                        } else if (vm.chartPivotObject[container_ID]['Column'].includes(attr)) {
                            var attrIndex = vm.chartPivotObject[container_ID]['Column'].indexOf(attr);
                            vm.chartPivotObject[container_ID]['Column'].splice(attrIndex, 1);
                        } else if (vm.chartPivotObject[container_ID]['Row'].includes(attr)) {
                            var attrIndex = vm.chartPivotObject[container_ID]['Row'].indexOf(attr);
                            vm.chartPivotObject[container_ID]['Row'].splice(attrIndex, 1);
                        } else if (vm.chartPivotObject[container_ID]['Data'].includes(attr)) {
                            var attrIndex = vm.chartPivotObject[container_ID]['Data'].indexOf(attr);
                            vm.chartPivotObject[container_ID]['Data'].splice(attrIndex, 1);
                        }
                        vm.pivotCust_EditDragDrop(container_ID);
                    }
                    // Append to origin
                    vm.set_Origin(container_ID);
                    vm.setCommonDrop(container_ID);
                    if (vm.chartPivotObject[container_ID]['Field'].length) {
                        $("#origin_" + container_ID).html("");
                        vm.chartPivotObject[container_ID]['Field'].forEach(function (e) {
                            var tempDiv = '<span class="col-sm-12 draggable_' + container_ID + ' removeClass ptCust_Attr_DragDrop ui-draggable ui-draggable-handle" id="' + e + '" title="' + e + '" >' +
                                '<div class="ng-binding">' + e + '<input type="hidden" value="' + e + '"></div></span>';
                            $("#origin_" + container_ID).append(tempDiv);
                        });
                        vm.draggablePivot(container_ID);
                    }
                } else {
                    var ptCust_Measure = vm.Attributes.checkboxModelMeasure;
                    var ptCust_Dimension = vm.Attributes.checkboxModelDimension;
                    var ptCust_Mea_Arr = [], ptCust_Dim_Arr = [];
                    if (ptCust_Measure != undefined) {
                        ptCust_Mea_Arr = Object.keys(ptCust_Measure);
                    }
                    if (ptCust_Dimension != undefined) {
                        ptCust_Dim_Arr = Object.keys(ptCust_Dimension);
                    }
                    var ptCust_Dim_Mea_Arr = ptCust_Mea_Arr.concat(ptCust_Dim_Arr);
                    ptCust_Dim_Mea_Arr.forEach(function (d) {
                        if (!vm.chartPivotObject[container_ID]['Field'].includes(d) && !vm.chartPivotObject[container_ID]['Column'].includes(d) && !vm.chartPivotObject[container_ID]['Row'].includes(d) && !vm.chartPivotObject[container_ID]['Data'].includes(d)) {
                            vm.chartPivotObject[container_ID]['Field'].push(d);
                        }
                    });
                    vm.set_Origin(container_ID);
                    vm.setCommonDrop(container_ID);
                    if (vm.chartPivotObject[container_ID]['Field'].length) {
                        setTimeout(function () {
                            $("#origin_" + container_ID).html("");
                            vm.chartPivotObject[container_ID]['Field'].forEach(function (e) {
                                var tempDiv = '<span class="col-sm-12 draggable_' + container_ID + ' removeClass ptCust_Attr_DragDrop ui-draggable ui-draggable-handle ptCust_Draggable_ClassName" id="' + e + '" title="' + e + '" >' +
                                    '<div class="ng-binding">' + e + '<input type="hidden" value="' + e + '"></div></span>';
                                $("#origin_" + container_ID).append(tempDiv);
                            });
                            vm.draggablePivot(container_ID);
                        }, 1000);
                    }
                }
            }
            // selected Measure_Dimension End

            // drag-drop start
            vm.setCommonDrop = function (container_ID) {
                var ptCust_Attr_ClassName = 'ptCust_Attr_Container_DragDrop_' + container_ID;
                var ptCust_Draggable_ClassName = 'draggable_' + container_ID;
                $('.' + ptCust_Attr_ClassName).droppable({
                    accept: "." + ptCust_Draggable_ClassName,
                    greedy: true,
                    drop: function (event, ui) {
                        var col_Attr = 'drop_Column_' + container_ID;
                        var row_Attr = 'drop_Row_' + container_ID;
                        var data_Attr = 'drop_Data_' + container_ID;
                        $(this).removeClass("border").removeClass("over");
                        var dropped = ui.draggable;
                        var div = $(ui.draggable)[0];

                        var selected_Attr = $(div).find('input').val();

                        if (col_Attr == $(this)[0].id) {
                            vm.chartPivotObject[container_ID]['Column'].push(selected_Attr);
                            // if(!vm.chartPivotObject[container_ID]['Column'].includes(selected_Attr)){
                            //     vm.chartPivotObject[container_ID]['Column'].push(selected_Attr);
                            // }
                            if (vm.chartPivotObject[container_ID]['Field'].includes(selected_Attr)) {
                                var attrIndex = vm.chartPivotObject[container_ID]['Field'].indexOf(selected_Attr);
                                vm.chartPivotObject[container_ID]['Field'].splice(attrIndex, 1);
                            } else if (vm.chartPivotObject[container_ID]['Data'].includes(selected_Attr)) {
                                var attrIndex = vm.chartPivotObject[container_ID]['Data'].indexOf(selected_Attr);
                                vm.chartPivotObject[container_ID]['Data'].splice(attrIndex, 1);
                            } else if (vm.chartPivotObject[container_ID]['Row'].includes(selected_Attr)) {
                                var attrIndex = vm.chartPivotObject[container_ID]['Row'].indexOf(selected_Attr);
                                vm.chartPivotObject[container_ID]['Row'].splice(attrIndex, 1);
                            } else {
                                var attrIndex = vm.chartPivotObject[container_ID]['Column'].indexOf(selected_Attr);
                                vm.chartPivotObject[container_ID]['Column'].splice(attrIndex, 1);
                            }
                        } else if (row_Attr == $(this)[0].id) {
                            vm.chartPivotObject[container_ID]['Row'].push(selected_Attr);
                            // if(!vm.chartPivotObject[container_ID]['Row'].includes(selected_Attr)){
                            //     vm.chartPivotObject[container_ID]['Row'].push(selected_Attr);
                            // }
                            if (vm.chartPivotObject[container_ID]['Field'].includes(selected_Attr)) {
                                var attrIndex = vm.chartPivotObject[container_ID]['Field'].indexOf(selected_Attr);
                                vm.chartPivotObject[container_ID]['Field'].splice(attrIndex, 1);
                            } else if (vm.chartPivotObject[container_ID]['Data'].includes(selected_Attr)) {
                                var attrIndex = vm.chartPivotObject[container_ID]['Data'].indexOf(selected_Attr);
                                vm.chartPivotObject[container_ID]['Data'].splice(attrIndex, 1);
                            } else if (vm.chartPivotObject[container_ID]['Column'].includes(selected_Attr)) {
                                var attrIndex = vm.chartPivotObject[container_ID]['Column'].indexOf(selected_Attr);
                                vm.chartPivotObject[container_ID]['Column'].splice(attrIndex, 1);
                            } else {
                                var attrIndex = vm.chartPivotObject[container_ID]['Row'].indexOf(selected_Attr);
                                vm.chartPivotObject[container_ID]['Row'].splice(attrIndex, 1);
                            }
                        } else if (data_Attr == $(this)[0].id) {
                            vm.chartPivotObject[container_ID]['Data'].push(selected_Attr);
                            // if(!vm.chartPivotObject[container_ID]['Data'].includes(selected_Attr)){
                            //     vm.chartPivotObject[container_ID]['Data'].push(selected_Attr);
                            // }
                            if (vm.chartPivotObject[container_ID]['Field'].includes(selected_Attr)) {
                                var attrIndex = vm.chartPivotObject[container_ID]['Field'].indexOf(selected_Attr);
                                vm.chartPivotObject[container_ID]['Field'].splice(attrIndex, 1);
                            } else if (vm.chartPivotObject[container_ID]['Row'].includes(selected_Attr)) {
                                var attrIndex = vm.chartPivotObject[container_ID]['Row'].indexOf(selected_Attr);
                                vm.chartPivotObject[container_ID]['Row'].splice(attrIndex, 1);
                            } else if (vm.chartPivotObject[container_ID]['Column'].includes(selected_Attr)) {
                                var attrIndex = vm.chartPivotObject[container_ID]['Column'].indexOf(selected_Attr);
                                vm.chartPivotObject[container_ID]['Column'].splice(attrIndex, 1);
                            } else {
                                var attrIndex = vm.chartPivotObject[container_ID]['Data'].indexOf(selected_Attr);
                                vm.chartPivotObject[container_ID]['Data'].splice(attrIndex, 1);
                            }
                        }

                        var droppedOn = $($(this)[0].childNodes[3]).addClass('dropped');
                        $(dropped).detach().addClass('dropped').css({top: 0, left: 0}).appendTo(droppedOn);
                        setTimeout(function () {
                            $('.dropped').removeClass('dropped');
                        }, 2000);


                        // $('#drop_Column_' + container_ID).droppable("destroy");
                        // $('#origin_' + container_ID).droppable("destroy");
                        // $('#drop_Row_' + container_ID).droppable("destroy");
                        // $('#drop_Data_' + container_ID).droppable("destroy");

                    },
                    over: function (event, elem) {
                        $(this).addClass("over");
                    },

                    out: function (event, elem) {
                        $(this).removeClass("over");
                    }
                });
            }

            vm.set_Origin = function (container_ID) {
                var ptCust_Draggable_ClassName = 'draggable_' + container_ID;
                $("#origin_" + container_ID).droppable({
                    accept: "." + ptCust_Draggable_ClassName,
                    greedy: true,
                    drop: function (event, ui) {
                        $(this).removeClass("border").removeClass("over");

                        var dropped = ui.draggable;
                        var div = $(ui.draggable)[0];
                        var selected_Attr = $(div).find('input').val();

                        if (!vm.chartPivotObject[container_ID]['Field'].includes(selected_Attr)) {
                            vm.chartPivotObject[container_ID]['Field'].push(selected_Attr);
                        }
                        if (vm.chartPivotObject[container_ID]['Data'].includes(selected_Attr)) {
                            var attrIndex = vm.chartPivotObject[container_ID]['Data'].indexOf(selected_Attr);
                            vm.chartPivotObject[container_ID]['Data'].splice(attrIndex, 1);
                        } else if (vm.chartPivotObject[container_ID]['Row'].includes(selected_Attr)) {
                            var attrIndex = vm.chartPivotObject[container_ID]['Row'].indexOf(selected_Attr);
                            vm.chartPivotObject[container_ID]['Row'].splice(attrIndex, 1);
                        } else if (vm.chartPivotObject[container_ID]['Column'].includes(selected_Attr)) {
                            var attrIndex = vm.chartPivotObject[container_ID]['Column'].indexOf(selected_Attr);
                            vm.chartPivotObject[container_ID]['Column'].splice(attrIndex, 1);
                        } else {
                            var attrIndex = vm.chartPivotObject[container_ID]['Field'].indexOf(selected_Attr);
                            vm.chartPivotObject[container_ID]['Field'].splice(attrIndex, 1);
                        }

                        var droppedOn = $(this).addClass('dropped2');
                        $(dropped).detach().addClass('dropped2').css({top: 0, left: 0}).prependTo(droppedOn);

                        setTimeout(function () {
                            $('.dropped2').removeClass('dropped2');

                        }, 2000);
                        $('#origin_' + container_ID).droppable("destroy");
                    }
                });
            }
            // drag-drop end


            //CustomPivot setting start
            vm.ptCust_DataObj = {};
            vm.ptCust_SubTotalObj = {};
            vm.ptCust_GrandTotalObj = {};
            vm.pivotCust_Setting = function (container_ID) {
                if (vm.ptCust_DataObj[container_ID] == undefined) {
                    vm.ptCust_DataObj[container_ID] = {};
                    vm.ptCust_DataObj[container_ID]['data'] = {};

                    vm.ptCust_GrandTotalObj[container_ID] = {};
                    vm.ptCust_GrandTotalObj[container_ID]['grandTotal'] = {};
                    vm.ptCust_GrandTotalObj[container_ID]['grandTotal']['row'] = 'no';
                    vm.ptCust_GrandTotalObj[container_ID]['grandTotal']['column'] = 'no';

                    vm.ptCust_SubTotalObj[container_ID] = {};
                    vm.ptCust_SubTotalObj[container_ID]['subTotal'] = {};
                    vm.ptCust_SubTotalObj[container_ID]['subTotal']['row'] = 'yes';
                    vm.ptCust_SubTotalObj[container_ID]['subTotal']['column'] = 'yes';
                }

                $('#pivot_DragDrop_Attr_Area_' + container_ID).css('display', 'none');
                $('#pivotCustomized_TableDiv_' + container_ID).css('display', 'none');
                // $('#pivot_Setting_Area_' + container_ID).css('display', 'block');

                vm.pt_Selected_Data_Attr = vm.chartPivotObject[container_ID].Data;
                vm.pt_Selected_Data_Attr.forEach(function (d) {
                    vm.ptCust_DataObj[container_ID]['data'][d] = 'sum';
                });
            }

            //CustomPivot setting end


            // Pivot Running Total Start
            vm.PtCust_Running_Total = false;
            vm.pivotCust_RunningTotalOption = function () {
                var show_runTotal_Data = $("input[name='runTotal_Pivot']:checked").val();
                var run_Total_Data = $("input[name='runTotal_RowColumn']:checked").val();

                if (show_runTotal_Data == 'yes') {
                    vm.PtCust_Running_Total = true;
                } else if (show_runTotal_Data == 'no') {
                    vm.PtCust_Running_Total = false;
                }
            }
            // Pivot Running Total End


            // CustomPivotDrawTable Start
            vm.pivotCust_ShowTable = function (container_ID) {
                var show_runTotal_Data = $("input[name='runTotal_Pivot']:checked").val();
                var run_Total_Data = $("input[name='runTotal_RowColumn']:checked").val();

                if (vm.ptCust_RunningTotalObj == undefined) {
                    vm.ptCust_RunningTotalObj = [];
                }
                if (vm.ptCust_RunningTotalObj[container_ID] == undefined) {
                    vm.ptCust_RunningTotalObj[container_ID] = {};
                }
                if (show_runTotal_Data == 'yes') {
                    if (run_Total_Data == 'row') {
                        vm.ptCust_RunningTotalObj[container_ID]['runningTotal']['row'] = 'yes';
                        vm.ptCust_RunningTotalObj[container_ID]['runningTotal']['column'] = 'no';
                    } else if (run_Total_Data == 'column') {
                        vm.ptCust_RunningTotalObj[container_ID]['runningTotal']['row'] = 'no';
                        vm.ptCust_RunningTotalObj[container_ID]['runningTotal']['column'] = 'yes';
                    }
                }

                vm.Attributes['pivotCust_Object'] = vm.chartPivotObject[container_ID];
                vm.Attributes['pivotCust_DataObject'] = vm.ptCust_DataObj[container_ID];
                vm.Attributes['pivotCust_GrandTotalObject'] = vm.ptCust_GrandTotalObj[container_ID];
                vm.Attributes['pivotCust_SubTotalObject'] = vm.ptCust_SubTotalObj[container_ID];
                vm.Attributes['pivotCust_RunningTotalObject'] = vm.ptCust_RunningTotalObj[container_ID];

                if (vm.Attributes['pivotCust_Object'].Column.length == 0) {
                    dataFactory.errorAlert("Column can't be empty for Pivot Table");
                    return;
                } else if (vm.Attributes['pivotCust_Object'].Row.length == 0) {
                    dataFactory.errorAlert("Row can't be empty for Pivot Table");
                    return;
                } else if (vm.Attributes['pivotCust_Object'].Data.length == 0) {
                    dataFactory.errorAlert("Data can't be empty for Pivot Table");
                    return;
                }

                $('#pivot_DragDrop_Attr_Area_' + container_ID).css('display', 'none');
                // $('#pivot_Setting_Area_' + container_ID).css('display', 'none');

                //sketch.data(vm.tableData);
                var containerIndex = "";
                vm.DashboardModel.Dashboard.Report.forEach(function (d, index) {
                    if (d.reportContainer.id == container_ID) {
                        containerIndex = index;
                    }
                });
                vm.DashboardModel.Dashboard.Report[containerIndex]['pivotCust_Object'] = vm.Attributes['pivotCust_Object'];
                vm.DashboardModel.Dashboard.Report[containerIndex]['pivotCust_DataObject'] = vm.Attributes['pivotCust_DataObject'];
                vm.DashboardModel.Dashboard.Report[containerIndex]['pivotCust_GrandTotalObject'] = vm.Attributes['pivotCust_GrandTotalObject'];
                vm.DashboardModel.Dashboard.Report[containerIndex]['pivotCust_SubTotalObject'] = vm.Attributes['pivotCust_SubTotalObject'];
                vm.DashboardModel.Dashboard.activeReport['pivotCust_RunningTotalObject'] = vm.Attributes['pivotCust_RunningTotalObject'];

                /* vm.DashboardModel.Dashboard.Report['pivotCust_Object'] = vm.Attributes['pivotCust_Object'];
                      vm.DashboardModel.Dashboard.activeReport['pivotCust_DataObject'] = vm.Attributes['pivotCust_DataObject'];
                      vm.DashboardModel.Dashboard.activeReport['pivotCust_GrandTotalObject'] = vm.Attributes['pivotCust_GrandTotalObject'];*/

                if (vm.dataCount < $rootScope.localDataLimit) {
                    sketch.axisConfig(vm.Attributes)
                        .container(vm.DashboardModel.Dashboard.activeReport.reportContainer.id)
                        .pivotCust_Draw();
                } else {
                    sketchServer.axisConfig(vm.Attributes)
                        .container(vm.DashboardModel.Dashboard.activeReport.reportContainer.id)
                        .pivotCust_Draw();
                }

            }
            // CustomPivotDrawTable End


            // draggableDropable_Pivot Start
            vm.draggablePivot = function (container_ID) {
                var ptCust_Draggable_ClassName = 'draggable_' + container_ID;
                $('.' + ptCust_Draggable_ClassName).draggable({
                    cursor: "move",
                    revert: "invalid",
                    start: function (e, ui) {
                        vm.set_Origin(container_ID);
                        vm.setCommonDrop(container_ID);
                        $(ui.draggable).css('z-index', 1001);
                    }
                });
            }
            // draggableDropable_Pivot End


            // EditDragDrop Option Start
            vm.pivotCust_EditDragDrop = function (container_ID) {
                // var tempDiv='<span class="col-sm-12 draggable_' + container_ID + ' ptCust_Attr_DragDrop ui-draggable ui-draggable-handle " id="'+e+'" title="'+e+'" >' + '<div class="ng-binding">'+e+'<input type="hidden" value="'+e+'"></div></span>';

                if (vm.chartPivotObject[container_ID]['Field'].length) {
                    $("#origin_" + container_ID).html("");
                    vm.chartPivotObject[container_ID]['Field'].forEach(function (e) {
                        var tempDiv = '<span class="col-sm-12 draggable_' + container_ID + ' ptCust_Attr_DragDrop ui-draggable ui-draggable-handle " id="' + e + '" title="' + e + '" >' + '<div class="ng-binding">' + e + '<input type="hidden" value="' + e + '"></div></span>';
                        // var tempDiv = '<div class="col-sm-1"><p class="ptCust_Label_Attr">Field</p></div><div class="col-sm-11"><span class="col-sm-12 draggable_'+container_ID+' removeClass ptCust_Attr_DragDrop ui-draggable ui-draggable-handle ptCust_Draggable_ClassName" id="'+e+'" title="'+e+'" >' + '<div class="ng-binding">'+e+'<input type="hidden" value="'+e+'"></div></span></div>';
                        $("#origin_" + container_ID).append(tempDiv);
                    });
                }
                if (vm.chartPivotObject[container_ID]['Column'].length) {
                    $("#drop_C_PT_" + container_ID).html("");
                    vm.chartPivotObject[container_ID]['Column'].forEach(function (e) {
                        var tempDiv = '<span class="col-sm-12 draggable_' + container_ID + ' ptCust_Attr_DragDrop ui-draggable ui-draggable-handle " id="' + e + '" title="' + e + '" >' + '<div class="ng-binding">' + e + '<input type="hidden" value="' + e + '"></div></span>';
                        // var tempDiv = '<div class="col-sm-1"><p class="ptCust_Label_Attr">Column</p></div><div class="col-sm-11"><span class="col-sm-12 draggable_'+container_ID+' removeClass ptCust_Attr_DragDrop ui-draggable ui-draggable-handle ptCust_Draggable_ClassName" id="'+e+'" title="'+e+'" >' + '<div class="ng-binding">'+e+'<input type="hidden" value="'+e+'"></div></span></div>';
                        $("#drop_C_PT_" + container_ID).append(tempDiv);
                    });
                }
                if (vm.chartPivotObject[container_ID]['Row'].length) {
                    $("#drop_R_PT_" + container_ID).html("");
                    vm.chartPivotObject[container_ID]['Row'].forEach(function (e) {
                        var tempDiv = '<span class="col-sm-12 draggable_' + container_ID + ' ptCust_Attr_DragDrop ui-draggable ui-draggable-handle " id="' + e + '" title="' + e + '" >' + '<div class="ng-binding">' + e + '<input type="hidden" value="' + e + '"></div></span>';
                        // var tempDiv = '<div class="col-sm-1"><p class="ptCust_Label_Attr">Row</p></div><div class="col-sm-11"><span class="col-sm-12 draggable_'+container_ID+' removeClass ptCust_Attr_DragDrop ui-draggable ui-draggable-handle ptCust_Draggable_ClassName" id="'+e+'" title="'+e+'" >' + '<div class="ng-binding">'+e+'<input type="hidden" value="'+e+'"></div></span></div>';
                        $("#drop_R_PT_" + container_ID).append(tempDiv);
                    });
                }
                if (vm.chartPivotObject[container_ID]['Data'].length) {
                    // $("#drop_Data_"+container_ID).html("");
                    $("#drop_D_PT_" + container_ID).html("");
                    vm.chartPivotObject[container_ID]['Data'].forEach(function (e) {
                        var tempDiv = '<span class="col-sm-12 draggable_' + container_ID + ' ptCust_Attr_DragDrop ui-draggable ui-draggable-handle " id="' + e + '" title="' + e + '" >' + '<div class="ng-binding">' + e + '<input type="hidden" value="' + e + '"></div></span>';
                        // var tempDiv = '<div class="col-sm-1"><p class="ptCust_Label_Attr">Data</p></div><div class="col-sm-11"><span class="col-sm-12 draggable_'+container_ID+' removeClass ptCust_Attr_DragDrop ui-draggable ui-draggable-handle ptCust_Draggable_ClassName" id="'+e+'" title="'+e+'" >' + '<div class="ng-binding">'+e+'<input type="hidden" value="'+e+'"></div></span></div>';
                        $("#drop_D_PT_" + container_ID).append(tempDiv);
                        // $("#drop_Data_"+container_ID).append(tempDiv);
                    });
                }

                setTimeout(function () {
                    vm.set_Origin(container_ID);
                    vm.draggablePivot(container_ID);
                }, 300);
                $('#pivot_DragDrop_Attr_Area_' + container_ID).css('display', 'block');
                $('#pivotCustomized_TableDiv_' + container_ID).css('display', 'none');
                // $('#pivot_Setting_Area_' + container_ID).css('display', 'none');
            }
            // EditDragDrop Option End


            // Pivot Hedaer Re-order start
            vm.pivotCust_Header_ShowOrder = function (container_ID) {
                $('#customPT_ReOrder_Headers').modal('show');
                vm.pivotCust_ShowHeader_Order();
            }

            vm.pivotCust_ShowHeader_Order = function () {
                vm.Group_DataRow_Arr = sketchServer.Group_Data_Row_Arr;
                vm.Group_DataCol_Arr = sketchServer.Group_Data_Col_Arr;

                vm.PtCust_Header_Row = false;
                vm.PtCust_Header_Col = false;

                // Added Later Start
                vm.Sub_Group_DataRow = false;
                vm.Last_Group_DataRow = false;
                vm.Last_Level_Group_DataRow = false;
                vm.Sub_Group_DataCol = false;
                vm.Last_Group_DataCol = false;
                vm.Last_Level_Group_DataCol = false;
                // Added Later End

                var show_Head_Data = $("input[name='ReOrder_Column_Header']:checked").val();
                if (show_Head_Data == 'Row') {
                    vm.PtCust_Header_Row = true;
                    vm.PtCust_Header_Col = false;
                } else if (show_Head_Data == 'Column') {
                    vm.PtCust_Header_Col = true;
                    vm.PtCust_Header_Row = false;
                }
                // Row Start
                vm.Main_Group_DataRow = true;
                vm.Row_PT_Level = 0;
                vm.Group_DataRow_Arr.forEach(function (d, i) {
                    if (i == 0) {
                        get_PT_RowLevel(d);
                    }
                });

                function get_PT_RowLevel(d) {
                    if (!$.isEmptyObject(d)) {
                        vm.Row_PT_Level = d.rowLevel;
                    }
                    if (d.values != undefined) {
                        get_PT_RowLevel(d.values[0])
                    }
                }

                setTimeout(function () {
                    $(".PT_Row_Header_Sort_Main").sortable({
                        start: function (event, ui) {
                            ui.item.startPos = ui.item.index();
                        },
                        stop: function (event, ui) {
                            var start_Swap_Row_Main = ui.item.startPos;
                            var end_Swap_Row_Main = ui.item.index();
                            vm.Group_DataRow_Arr[start_Swap_Row_Main] = vm.Group_DataRow_Arr.splice(end_Swap_Row_Main, 1, vm.Group_DataRow_Arr[start_Swap_Row_Main])[0];
                            return;
                        }
                    });
                }, 800);
                // Row End

                // Col Start
                vm.Main_Group_DataCol = true;
                vm.Col_PT_Level = 0;
                vm.Group_DataCol_Arr.forEach(function (d, i) {
                    if (i == 0) {
                        get_PT_ColLevel(d);
                    }
                });

                function get_PT_ColLevel(d) {
                    if (!$.isEmptyObject(d)) {
                        vm.Col_PT_Level = d.colLevel;
                    }
                    if (d.values != undefined) {
                        get_PT_ColLevel(d.values[0])
                    }
                }

                setTimeout(function () {
                    $(".PT_Col_Header_Sort_Main").sortable({
                        start: function (event, ui) {
                            ui.item.startPos = ui.item.index();
                        },
                        stop: function (event, ui) {
                            var start_Swap_Col_Main = ui.item.startPos;
                            var end_Swap_Col_Main = ui.item.index();
                            vm.Group_DataCol_Arr[start_Swap_Col_Main] = vm.Group_DataCol_Arr.splice(end_Swap_Col_Main, 1, vm.Group_DataCol_Arr[start_Swap_Col_Main])[0];
                        }
                    });
                }, 800);
                // Col End
            }


            // Row Start
            vm.pivotCust_Header_Forward_Row = function (data) {
                vm.Group_DataRow_Key_Val = data;
                vm.Group_DataRow_Val_Arr = data.values;
                vm.Main_Group_DataRow = false;
                vm.Sub_Group_DataRow = true;
                vm.Last_Group_DataRow = true;
                vm.Last_Level_Group_DataRow = true;

                if (vm.Row_PT_Level == vm.Group_DataRow_Val_Arr[0].rowLevel) {
                    vm.Last_Level_Group_DataRow = false;
                }
                if (vm.Group_DataRow_Val_Arr[0].rowLevel == undefined) {
                    vm.Last_Group_DataRow = false;
                }
                setTimeout(function () {
                    $(".PT_Row_Header_Sort_Child").sortable({
                        start: function (event, ui) {
                            ui.item.startPos = ui.item.index();
                        },
                        stop: function (event, ui) {
                            var start_Swap_Row_Child = ui.item.startPos;
                            var end_Swap_Row_Child = ui.item.index();
                            vm.Group_DataRow_Val_Arr[start_Swap_Row_Child] = vm.Group_DataRow_Val_Arr.splice(end_Swap_Row_Child, 1, vm.Group_DataRow_Val_Arr[start_Swap_Row_Child])[0];
                        }
                    });
                }, 800);
            }

            vm.pivotCust_Header_Backward_Row = function (data) {

            }
            // Row End


            // Col Start
            vm.pivotCust_Header_Forward_Col = function (data) {
                vm.Group_DataCol_Key_Val = data;
                vm.Group_DataCol_Val_Arr = data.values;
                vm.Main_Group_DataCol = false;
                vm.Sub_Group_DataCol = true;
                vm.Last_Group_DataCol = true;
                vm.Last_Level_Group_DataCol = true;

                if (vm.Col_PT_Level == vm.Group_DataCol_Val_Arr[0].colLevel) {
                    vm.Last_Level_Group_DataCol = false;
                }
                if (vm.Group_DataCol_Val_Arr[0].colLevel == undefined) {
                    vm.Last_Group_DataCol = false;
                }
                setTimeout(function () {
                    $(".PT_Col_Header_Sort_Child").sortable({
                        start: function (event, ui) {
                            ui.item.startPos = ui.item.index();
                        },
                        stop: function (event, ui) {
                            var start_Swap_Col_Child = ui.item.startPos;
                            var end_Swap_Col_Child = ui.item.index();
                            vm.Group_DataCol_Val_Arr[start_Swap_Col_Child] = vm.Group_DataCol_Val_Arr.splice(end_Swap_Col_Child, 1, vm.Group_DataCol_Val_Arr[start_Swap_Col_Child])[0];
                        }
                    });
                }, 800);
            }

            vm.pivotCust_Header_Backward_Col = function (data) {

            }
            // Col End

            vm.PivotHeaderReorder_Cancel = function () {
                vm.Group_DataRow_Arr = sketchServer.Group_Data_Row_Arr;
                vm.Group_DataCol_Arr = sketchServer.Group_Data_Col_Arr;

                vm.Main_Group_DataRow = false;
                vm.Sub_Group_DataRow = false;
                vm.Last_Group_DataRow = false;
                vm.Last_Level_Group_DataRow = false;

                vm.Main_Group_DataCol = false;
                vm.Sub_Group_DataCol = false;
                vm.Last_Group_DataCol = false;
                vm.Last_Level_Group_DataCol = false;
            }

            vm.PivotHeaderReorder_Save = function () {
                $('#customPT_ReOrder_Headers').modal('hide');

                vm.Attributes['pivotCust_CustomHeader_Object'] = {};
                vm.Attributes['pivotCust_CustomHeader_Object']['Row'] = vm.Group_DataRow_Arr;
                vm.Attributes['pivotCust_CustomHeader_Object']['Col'] = vm.Group_DataCol_Arr;
                vm.DashboardModel.Dashboard.activeReport['pivotCust_CustomHeader_Object'] = vm.Attributes['pivotCust_CustomHeader_Object'];

                if (vm.dataCount < $rootScope.localDataLimit) {
                    sketch.axisConfig(vm.Attributes)
                        .container(vm.DashboardModel.Dashboard.activeReport.reportContainer.id)
                        .pivotCust_Draw();
                } else {
                    sketchServer.axisConfig(vm.Attributes)
                        .container(vm.DashboardModel.Dashboard.activeReport.reportContainer.id)
                        .pivotCust_Draw();
                }
            }

            // Pivot Hedaer Re-order end


            // Pivot Hide/Show Header start
            vm.pivotCust_HideShow_Header = function (container_ID) {
                $('#customPT_Display_Headers').modal('show');

                var PT_Row_Header_Arr = sketchServer.PT_Row_Table_Arr;
                vm.PT_Row_CustomHeader_Arr = [];
                for (var i = 0; i < PT_Row_Header_Arr.length; i++) {
                    var tempRowArr = [];
                    $.each(PT_Row_Header_Arr[i], function (i, el) {
                        if ($.inArray(el, tempRowArr) === -1) tempRowArr.push(el);
                    });
                    if (tempRowArr.includes('SubRow')) {
                        var attr_index = tempRowArr.indexOf('SubRow');
                        tempRowArr.splice(attr_index, 1);
                    }
                    if (tempRowArr.includes('GrandRow')) {
                        var attr_index = tempRowArr.indexOf('GrandRow');
                        tempRowArr.splice(attr_index, 1);
                    }
                    if (i == 0) {
                        vm.PT_Row_CustomHeader_Arr.push(tempRowArr);
                    }
                }

                var PT_Col_Header_Arr = sketchServer.PT_Col_Table_Arr;
                vm.PT_Col_CustomHeader_Arr = [];
                for (var i = 0; i < PT_Col_Header_Arr.length; i++) {
                    var temp_ColArr = [];
                    $.each(PT_Col_Header_Arr[i], function (i, el) {
                        if ($.inArray(el, temp_ColArr) === -1) temp_ColArr.push(el);
                    });
                    if (temp_ColArr.includes('GrandColumn')) {
                        var attr_index = temp_ColArr.indexOf('GrandColumn');
                        temp_ColArr.splice(attr_index, 1);
                    }
                    if (temp_ColArr.includes('SubColumn')) {
                        var attr_index = temp_ColArr.indexOf('SubColumn');
                        temp_ColArr.splice(attr_index, 1);
                    }
                    if (i == 0) {
                        vm.PT_Col_CustomHeader_Arr.push(temp_ColArr);
                    }
                }

                vm.PT_Row_Display_Header_Arr = [];
                vm.PT_Col_Display_Header_Arr = [];

                if (vm.Attributes['pivotCust_Display_Header_Object'] == undefined) {
                    vm.PT_Row_CustomHeader_Arr[0].forEach(function (d, i) {
                        var temp_Arr = [], temp_Obj = {};
                        temp_Obj['key'] = d;
                        temp_Obj['val'] = false;
                        temp_Arr.push(temp_Obj);
                        vm.PT_Row_Display_Header_Arr.push(temp_Obj);
                    });
                    vm.PT_Col_CustomHeader_Arr[0].forEach(function (d, i) {
                        var temp_Arr = [], temp_Obj = {};
                        temp_Obj['key'] = d;
                        temp_Obj['val'] = false;
                        temp_Arr.push(temp_Obj);
                        vm.PT_Col_Display_Header_Arr.push(temp_Obj);
                    });
                } else {
                    vm.PT_Row_Display_Header_Arr = vm.Attributes['pivotCust_Display_Header_Object']['Row'];
                    vm.PT_Col_Display_Header_Arr = vm.Attributes['pivotCust_Display_Header_Object']['Col'];
                }
                vm.pivotCust_DisplayHeader_Order();
            }

            vm.pivotCust_DisplayHeader_Order = function () {
                var display_Head_Data = $("input[name='Display_Table_Header']:checked").val();
                if (display_Head_Data == 'Row') {
                    vm.PtCust_Display_Header_Row = true;
                    vm.PtCust_Display_Header_Col = false;
                } else if (display_Head_Data == 'Column') {
                    vm.PtCust_Display_Header_Col = true;
                    vm.PtCust_Display_Header_Row = false;
                }
            }

            vm.PivotHeaderDisplay_Cancel = function () {
                vm.pivotCust_DisplayHeader_Order();
            }

            vm.PivotHeaderDisplay_Save = function () {
                $('#customPT_Display_Headers').modal('hide');
                vm.PT_Row_Display_Header_Arr.forEach(function (d, i) {
                    var chkbox_Val = $("[id='" + d.key + "']").is(':checked');
                    if (d.val != chkbox_Val) {
                        d.val = chkbox_Val;
                    }
                });
                vm.PT_Col_Display_Header_Arr.forEach(function (d, i) {
                    var chkbox_Val = $("[id='" + d.key + "']").is(':checked');
                    if (d.val != chkbox_Val) {
                        d.val = chkbox_Val;
                    }
                });
                vm.Attributes['pivotCust_Display_Header_Object'] = {};
                vm.Attributes['pivotCust_Display_Header_Object']['Row'] = vm.PT_Row_Display_Header_Arr;
                vm.Attributes['pivotCust_Display_Header_Object']['Col'] = vm.PT_Col_Display_Header_Arr;
                vm.DashboardModel.Dashboard.activeReport['pivotCust_Display_Header_Object'] = vm.Attributes['pivotCust_Display_Header_Object'];

                if (vm.dataCount < $rootScope.localDataLimit) {
                    sketch.axisConfig(vm.Attributes)
                        .container(vm.DashboardModel.Dashboard.activeReport.reportContainer.id)
                        .pivotCust_Draw();
                } else {
                    sketchServer.axisConfig(vm.Attributes)
                        .container(vm.DashboardModel.Dashboard.activeReport.reportContainer.id)
                        .pivotCust_Draw();
                }

            }

            // Pivot Hide/Show Header end


            // pivotCustomized End


            // speedometer Setting data start
            vm.speedoMeterSettingObj = {};
            vm.speedoMeterSettingData = function (speedData) {
                $('#gaugeSetting').modal('hide');

                vm.Attributes['speedoMeterSettingObject'] = {};
                vm.Attributes['speedoMeterSettingObject']['lowRange'] = vm.speedoMeterSettingObj.lowRange;
                vm.Attributes['speedoMeterSettingObject']['midRange'] = vm.speedoMeterSettingObj.midRange;
                vm.Attributes['speedoMeterSettingObject']['highRange'] = vm.speedoMeterSettingObj.highRange;

                if (vm.dataCount < $rootScope.localDataLimit) {
                    sketch.axisConfig(vm.Attributes)
                        .container(vm.DashboardModel.Dashboard.activeReport.reportContainer.id)
                        ._speedoMeterChart();
                } else {
                    sketchServer.axisConfig(vm.Attributes)
                        .accessToken($rootScope.accessToken)
                        .container(vm.DashboardModel.Dashboard.activeReport.reportContainer.id)
                        ._speedoMeterChart();
                }
            }
            // speedometer Setting data end


            // Gender Comparison Start
            vm.PopulateGenderData = function (GenderData) {
                $("#male_MultiSelect").selectpicker('destroy');
                $("#female_MultiSelect").selectpicker('destroy');

                setTimeout(function () {
                    $("#male_MultiSelect").selectpicker();
                    $("#female_MultiSelect").selectpicker();
                }, 1000);

                var bind_gender_data;
                vm.Attributes['genderObject'] = {};
                vm.Attributes['genderObject']['dim_gender'] = vm.genderObject.dim_gender;
                vm.Attributes['genderObject']['mea_gender'] = vm.genderObject.mea_gender;
                sketch.data(vm.tableData);

                if (vm.dataCount < $rootScope.localDataLimit) {
                    $scope.bind_gender_data = sketch._populateMaleFemaleChart(vm.Attributes['genderObject']);
                } else {
                    sketchServer._populateMaleFemaleChart(vm.Attributes['genderObject']).done(function (data) {
                        $scope.bind_gender_data = data;
                        $scope.$apply();
                    });
                }
            }

            //male-female data
            vm.genderObject = {};
            vm.GenderData = function (GenderData) {
                $('#commonModals').modal('hide');

                vm.Attributes['genderObject'] = {};
                vm.Attributes['genderObject']['dim_gender'] = vm.genderObject.dim_gender;
                vm.Attributes['genderObject']['mea_gender'] = vm.genderObject.mea_gender;
                vm.Attributes['genderObject']['male'] = vm.genderObject.male;
                vm.Attributes['genderObject']['female'] = vm.genderObject.female;

                vm.DashboardModel.Dashboard.activeReport['dataObject'] = vm.Attributes['genderObject'];

                if (vm.dataCount < $rootScope.localDataLimit) {
                    sketch.axisConfig(vm.Attributes)
                        .container(vm.DashboardModel.Dashboard.activeReport.reportContainer.id)
                        ._maleFemaleChartDraw();
                } else {
                    sketchServer.axisConfig(vm.Attributes)
                        .accessToken($rootScope.accessToken)
                        .container(vm.DashboardModel.Dashboard.activeReport.reportContainer.id)
                        ._maleFemaleChartDraw();
                }
            }
            // Gender Comparison End


            // Map Start
            vm.MapData = function (MapData) {
                $('#commonModals').modal('hide');

                var chartID = vm.DashboardModel.Dashboard.activeReport.reportContainer.id;

                vm.Attributes['mapObject'] = {};
                vm.Attributes['mapObject']['lat_map'] = vm.mapObject[chartID].lat_map;
                vm.Attributes['mapObject']['long_map'] = vm.mapObject[chartID].long_map;
                vm.Attributes['mapObject']['measure_map'] = vm.mapObject[chartID].measure_map;
                vm.Attributes['mapObject']['multiGroupColor_map'] = vm.mapObject[chartID].multiGroupColor_map;
                vm.Attributes['mapObject']['multipleTooltipObj'] = vm.mapObject[chartID].multipleTooltipObj;
                vm.Attributes['mapObject']['multipleTooltip'] = vm.mapObject[chartID].multipleTooltip;

                // vm.Attributes['mapObject'] = {};
                // vm.Attributes['mapObject']['lat_map'] = vm.mapObject.lat_map;
                // vm.Attributes['mapObject']['long_map'] = vm.mapObject.long_map;
                // vm.Attributes['mapObject']['measure_map'] = vm.mapObject.measure_map;
                // vm.Attributes['mapObject']['multiGroupColor_map'] = vm.mapObject.multiGroupColor_map;
                // vm.Attributes['mapObject']['multipleTooltipObj'] = vm.mapObject.multipleTooltipObj;
                // vm.Attributes['mapObject']['multipleTooltip'] = vm.mapObject.multipleTooltip;

                sketch.data(vm.tableData);
                vm.DashboardModel.Dashboard.activeReport['dataObject'] = vm.Attributes['mapObject'];

                if (vm.dataCount < $rootScope.localDataLimit) {
                    sketch.axisConfig(vm.Attributes)
                        .container(vm.DashboardModel.Dashboard.activeReport.reportContainer.id)
                        ._mapChartDraw();
                } else {
                    sketchServer.axisConfig(vm.Attributes)
                        .accessToken($rootScope.accessToken)
                        .container(vm.DashboardModel.Dashboard.activeReport.reportContainer.id)
                        ._mapChartDraw();
                }
            }
            // Map End


            $(document)
                .mouseup(
                    function (e) {
                        var container = $(".dc-chart");
                        var handle_s = $(".handle-s");
                        var handle_e = $(".handle-e");
                        var handle_n = $(".handle-n");
                        var handle_se = $(".handle-se");
                        var handle_w = $(".handle-w");
                        if (!(handle_s.is(e.target)
                                || handle_e
                                    .is(e.target)
                                || handle_n
                                    .is(e.target)
                                || handle_se
                                    .is(e.target) || handle_w
                                    .is(e.target))) {
                            if (!container.is(e.target) && container.has(e.target).length === 0) // ...
                            // nor
                            // a
                            // descendant
                            // of
                            // the
                            // container
                            {
                                /*$(".box").removeClass(
                                      "box-border");*/
                                /*$(".li_box")
                                      .removeClass(
                                      "li-box-border");*/
                            }
                        }
                    });

            /*
                  * $('html').click(function() {
                  * $(".box").removeClass("box-border"); });
                  */

            $scope.chartClicked = function (id) {

            };

            $scope.clear = function () {
                if (!($scope.dashboardPrototype == undefined)) {
                    $scope.dashboardPrototype.reportContainers = [];
                    vm.DashboardModel.Dashboard.activeReport = {};
                    vm.DashboardModel.Dashboard.Report = [];
                }
            };

            // creating widgets
            var lastcontainerId;
            var lastcount = 0;
            var newDim = [];
            // Filter Drawer
            vm.filterBy = function (item, filter) {
                if (vm.tableData < $rootScope.localDataLimit) {
                    sketch.applyFilter(item, filter);
                } else {
                    $(".loadingBar").show();
                    sketchServer.applyFilter(item, filter, vm.metadataId,totalLength,index,function (data, filterObj, status) {
                        if (status) {
                            try{
                                vm.filtersTo[filterObj.key] = data;
                                vm.allFilters[filterObj.key].filterValue = data;
                                $scope.$apply();
                                // vm.allFilters[filterObj.key]['filterValue']=data;
                                if(filterObj.value=="varchar" || filterObj.value=="char" || filterObj.value=="text"){
                                    $("[id^=filterSelect_]").each(function () {
                                        if($(this).attr('id')!="filterSelect_"+index) {
                                            $(this).selectpicker('destroy', true);
                                        }
                                    })
                                    setTimeout(function () {
                                        $("[id^=filterSelect_]").each(function () {
                                            if($(this).attr('id')!="filterSelect_"+index){
                                                $(this).selectpicker();
                                            }
                                        });
                                        $(".loadingBar").hide();
                                    }, 1000);
                                }else if(filterObj.value=="decimal" || filterObj.value=="int" || filterObj.value=="bigint" || filterObj.value=="double" || filterObj.value=="float"){
                                    /*
                                       Range filter
                                    */
                                    setTimeout(function () {
                                        var min = Math.min.apply(Math,data);
                                        var max = Math.max.apply(Math,data);
                                        if(min && max){
                                            if (min == null) {
                                                min = 0;
                                            }
                                            if(min==max){
                                                max=max+1;
                                            }
                                            var config = {
                                                orientation: "horizontal",
                                                start: [min,max],
                                                range: {
                                                    min: min,
                                                    max: max,
                                                },
                                                connect: 'lower',
                                                direction: "ltr",
                                                step: 10,
                                            };
                                            var nonLinearSlider = document.getElementById(filterObj.key.replace("(","(").replace(")",")")+ "-Filter");
                                            document.getElementById(filterObj.key.replace("(","(").replace(")",")")+ "-Filter").noUiSlider.destroy();
                                            noUiSlider.create(nonLinearSlider, {
                                                animate: true,
                                                start: [min, max],
                                                connect: true,
                                                range: {
                                                    min: parseInt(min),
                                                    max: parseInt(max)
                                                },
                                                step: 1,
                                                tooltips: [wNumb({
                                                    decimals: 0
                                                }), wNumb({
                                                    decimals: 0
                                                })]
                                            });
                                            vm.allFilters[filterObj.key]['filterValue'] = [min, max];
                                            nonLinearSlider.noUiSlider.on('update', function (values, handle) {
                                                vm.allFilters[filterObj.key]['filterValue'] = values;

                                            });
                                            nonLinearSlider.noUiSlider.on('change', function (values, handle) {
                                                vm.filterBy(values,filterObj);
                                            });
                                        }
                                        $(".loadingBar").hide();
                                    },1000);
                                    /*
                                      End range filter
                                     */
                                }
                            }catch(e){

                            }
                        }else{
                            setTimeout(function () {
                                $(".loadingBar").hide();
                            }, 1000);
                        }
                    });
                }
            }
            var allAxisSelected = function () {
                if (vm.Xaxis == undefined
                    || vm.Yaxis == undefined
                    || vm.Maxis == undefined) {

                    return false;
                }
                return true;
            };

            $scope.imageTest = function () {
                html2canvas($("#dashboard-image"),
                    {
                        background: '#FFFFFF',
                        onrendered: function (canvas) {
                            // restore the old offscreen
                            // position

                            var img = canvas
                                .toDataURL("image/png");
                            var output = encodeURIComponent(img);
                            var newDate = new Date();
                            var image_name = $scope.dashboardName
                                + new Date().getTime();
                            var a = document
                                .createElement('a');
                            // toDataURL defaults to png,  so
                            // we need to request a jpeg,
                            // then convert for file
                            // download.
                            a.href = canvas.toDataURL(
                                "image/png").replace(
                                "image/png",
                                "image/octet-stream");
                            a.download = 'somefilename.jpg';
                            a.click();
                            var Parameters = "image="
                                + output
                                + "&image_name="
                                + image_name;
                            $.ajax(
                                {
                                    type: "POST",
                                    url: "http://bi.thinklayer.com/api/newbi/data/dashboard/save-image.php",
                                    data: Parameters,
                                    success: function (data) {
                                        alert(data);
                                    }
                                }).done(
                                function () {
                                    // $('body').html(data);
                                });
                            // Convert and download as image
                            // Canvas2Image.saveAsPNG(canvas);
                            // $("#img-out").append(canvas);
                            // Clean up
                            // document.body.removeChild(canvas);
                        }
                    });
            }
            //Sort By
            vm.sortBy = function (type) {
                if (vm.dataCount < $rootScope.localDataLimit)
                    eChart.sortData(type);
                else
                    eChartServer.sortData(type);
            }
            vm.rename = {};
            $scope.renameKeys = function (Obj, type) {
                var i = 0;
                $scope.typeRename = type;
                vm.lastRenameObj = Obj;

                $.each(vm.tableColumns, function (index, value) {
                    if (value.reName == Obj.reName) {
                        $scope.lastRenameIndex = i;
                        $scope.typeRename = type;
                        $scope.typeAxis = value.dataKey;
                    }
                    i++;
                });
                var modalElem = $('#myModal');
                $('#myModal').modal({
                    backdrop: 'static',
                    keyboard: false
                });
                $('#myModal').modal('show');
                vm.rename.reNameInput = Obj.reName;
                $(".page").addClass("background-container");
            }

            $scope.deleteGraphKeys = function (id) {
                if (vm.dataCount < $rootScope.localDataLimit) {
                    $scope.deleteReportContainer(id);
                    var listAttr = eChart.chartRegistry.listAttributes();
                    if (listAttr) {
                        delete listAttr["chart-" + id];
                    }
                    dataFactory.successAlert("Report Deleted Successfully");
                } else {
                    var data = {reportId: id, metadataId: vm.metadataId, sessionId: $rootScope.accessToken};
                    dataFactory.nodeRequest('deleteReport', 'post', data).then(function (response) {
                        if (response) {
                            $scope.deleteReportContainer(id);
                            var listAttr = eChartServer.chartRegistry.listAttributes();
                            if (listAttr) {
                                delete listAttr["chart-" + id];
                            }
                            dataFactory.successAlert("Report Deleted Successfully");
                        } else {
                            dataFactory.errorAlert("Check your connection");
                        }
                    });
                }

                // delete notification mail
                var data = {
                    'dashboardId': vm.dashboardId,
                    'containerId': id
                };

                dataFactory.request($rootScope.EmailNotificationDelete_Url, 'post', data).then(function (response) {
                    if (response.data.errorCode == 1) {
                        //dataFactory.successAlert("Notification Email Deleted Successfully");
                    }
                });


            }

            $scope.findIndexOfReportContainer = function (id) {
                var index = -1;
                var i = 0;
                $scope.dashboardPrototype.reportContainers.forEach(function (d) {
                    if (d.id == id)
                        index = i;
                    i++;
                });
                return index;
            }

            $scope.findIndexOfReportFromReportContainer = function (id) {
                var index = -1;
                var i = 0;
                vm.DashboardModel.Dashboard.Report.forEach(function (d) {
                    if (d.reportContainer.id == id)
                        index = i;
                    i++;
                });
                return index;
            }

            $scope.deleteReportContainer = function (id) {
                if (sketchServer._totalReportCount) {
                    sketchServer._totalReportCount--;
                }
                var index = $scope.findIndexOfReportContainer(id);
                vm.Attributes = {};
                $scope.dashboardPrototype.reportContainers.splice(index, 1);
                vm.DashboardModel.Dashboard.Report.splice(index, 1);
            }

            vm.saveRename = function (rename) {

                if ($scope.typeRename == "Axis") {
                    var tempVar = {};
                    if ($scope.typeAxis == "Measure") {
                        tempVar = angular.copy(vm.activeYLabel[vm.DashboardModel.Dashboard.activeReport.reportContainer.id]);
                        tempVar.reName = rename;
                        vm.activeYLabel[vm.DashboardModel.Dashboard.activeReport.reportContainer.id] = tempVar;
                    } else if ($scope.typeAxis == "Dimension") {
                        tempVar = angular.copy(vm.activeXLabel[vm.DashboardModel.Dashboard.activeReport.reportContainer.id]);
                        tempVar.reName = rename;
                        vm.activeXLabel[vm.DashboardModel.Dashboard.activeReport.reportContainer.id] = tempVar;
                    }
                } else if ($scope.typeRename == "mainContainer") {
                    vm.DashboardModel.Dashboard.activeReport.reportContainer.name = rename;
                } else {
                    var Obj = vm.tableColumns[$scope.lastRenameIndex];
                    var prevRename = Obj.reName;
                    vm.tableColumns.forEach(function (column) {

                        if (column && column.formulaObj) {
                            column.formulaObj.forEach(function (d) {
                                if (d.reName != d.columnName && d.reName == prevRename) {
                                    d.reName = rename;
                                }
                            });
                        }
                    });
                    vm.tableColumns[$scope.lastRenameIndex]['reName'] = rename;
                }
                $('#myModal').modal('hide');
                $(".reportDashboard").removeClass('background-container');
            }
            $scope.groupTypeModal=function(){
                $('#reportGroup').modal('show');
            }
            $scope.publicViewType="specific_group";
            $scope.reportGrp={};
            dataFactory.request($rootScope.UsergroupList_Url,'post',"").then(function(response){
                $scope.userGroup=response.data.result;
            });
            dataFactory.request($rootScope.ReportGroupList_Url,'post',"").then(function(response){
                $scope.reportGroup=response.data.result;
            });
            $scope.selectGroup=function(divClass){
                if(divClass=='back'){
                    $(".mainDiv").slideDown('fast');
                    $(".select").slideUp('fast');
                    $(".create").slideUp('fast');
                    $(".footerDiv").slideUp('fast');
                }else{
                    $("."+divClass).slideDown('fast');
                    $(".footerDiv").slideDown('fast');
                    $(".mainDiv").slideUp('fast');
                }
                if(divClass=='select' || divClass=='create'){
                    $scope.reportGrp={};
                }
            }
            $scope.checkView=function(page){
                $scope.publicViewType=page;
                if(page=='public_group'){
                    $(".subDiv").hide();
                    $(".footerDiv").show();
                }
                else{
                    $(".subDiv").show();
                    $(".footerDiv").hide();
                }
            }
            /*
            Notification
             */
            $scope.groupTypeSave=function(reportGrp){
                if(Object.keys($scope.reportGrp).length==0 && $scope.publicViewType=='sharedview'){
                    dataFactory.errorAlert("Select group");
                }else{
                    if(reportGrp.name && !reportGrp.userGroup){
                        dataFactory.errorAlert("Select report group");
                        return;
                    }
                    $scope.reportGrp=reportGrp;
                    $('#reportGroup').modal('hide');
                    vm.saveDashboard();
                }
            }


            vm.notificationReportObject = {};
            vm.notificationEmail={
                init:function(){
                    vm.frequencyTime=true;
                    vm.frequencyMin=true;
                    vm.emailTypeSelect=false;
                },
                modal:function(){
                    vm.nEmail={};
                    vm.nEmail.frequency="everyhours";
                    vm.nEmail.emailInterval="once";
                    $('#notificationEmail').modal('show');
                    $('#timepicker').datetimepicker({
                        format: 'HH:mm'
                    }).on('dp.change', function (e) {
                        vm.nEmail.time=$("#timepicker").val();
                    });
                    if(vm.notificationReportObject['chart-'+vm.DashboardModel.Dashboard.activeReport.reportContainer.id] != undefined){
                        vm.nEmail = vm.notificationReportObject['chart-'+vm.DashboardModel.Dashboard.activeReport.reportContainer.id];
                    }
                },
                frequency:function(type){
                    vm.frequencyTime=true;
                    vm.frequencyMin=true;
                    if(type=="24hours"){
                        vm.frequencyTime=false;
                    }else if(type=="everyhours"){
                        vm.frequencyTime=true;
                    }else{
                        vm.frequencyMin=false;
                    }
                },
                emailInterval:function(type){
                    if(type=="timeInterval"){
                        vm.emailInterval=true;
                    }else{
                        vm.emailInterval=false;
                    }
                },
                emailSelectType:function(emailType){
                    if(emailType=="specific"){
                        // All Users
                        vm.emailTypeGrp=false;
                        vm.emailTypeSelect=true;
                        dataFactory.request($rootScope.UsersEmailList_Url,'post',"").then(function(response){
                            if(response.data.errorCode==1){
                                vm.usersEmailList=response.data.result;
                                if(vm.notificationObj!=undefined){
                                    vm.nEmail.emailId=vm.notificationObj.emailId;
                                }
                                $("#emailId").selectpicker('destroy');
                                setTimeout(function(){
                                    $("#emailId").selectpicker();
                                    $scope.$apply();
                                },1000);
                            }
                        });
                    }else if(emailType=="group"){
                        //Group
                        vm.emailTypeSelect=false;
                        dataFactory.request($rootScope.EmailGrpList_Url,'post',"").then(function(response){
                            if(response.data.errorCode==1){
                                vm.emailGrpList=response.data.result;
                                setTimeout(function(){
                                    $("#emailIdGrp").selectpicker('destroy');
                                    $("#emailIdGrp").selectpicker();
                                    vm.emailTypeGrp=true;
                                    $scope.$apply();
                                },1000);
                            }
                        });
                    }else{
                        vm.emailTypeSelect=false;
                    }
                },
                save:function(nEmailObj){
                    $('#notificationEmail').modal('hide');
                    var data={
                        "emailObj":JSON.stringify(nEmailObj),
                        "connection":JSON.stringify(vm.DashboardModel.Dashboard.activeReport.axisConfig),
                        "reportId":vm.DashboardModel.Dashboard.activeReport.reportContainer.id
                    };
                    dataFactory.successAlert("Notification Email Saved successfully");
                    vm.notificationReportObject['chart-' + vm.DashboardModel.Dashboard.activeReport.reportContainer.id] = data;
                }
            }
            vm.notificationEmail.init();
            vm.saveDashboard = function () {
                if ($scope.dashName.dashboardName == "" || $scope.dashName.dashboardName == undefined) {
                    dataFactory.errorAlert("Please Enter Dashboard Name");
                    return;
                }
                $(".loadingBar").show();
                $scope.metadataObject['connObject']['column'] = $scope.tableColumns;
                vm.DashboardModel.Dashboard['customMeasure'] = vm.customMeasure;
                vm.DashboardModel.Dashboard['customGroup'] = vm.customGroup;
                var reportsObject = vm.DashboardModel.Dashboard;
                var newDate = new Date();
                var image_name = $scope.dashName.dashboardName + new Date().getTime();
                html2canvas($("#dashboard-image"),
                    {
                        onrendered: function (canvas) {
                            var theCanvas = canvas;
                            // document.getElementById("dash-border").appendChild(canvas);
                            var img = canvas.toDataURL("image/png");
                            var output = encodeURIComponent(img);
                            var data = {
                                "image": img,
                                "image_name": image_name
                            };
                            dataFactory.request($rootScope.DashboardImageSave_Url, 'post', data).then(function (response) {
                                if (response.data.errorCode == 1) {
                                    if (vm.checkboxModel && vm.checkboxModel.lenght) {
                                        var tempCheckboxModel = [];
                                        vm.checkboxModel.forEach(function (d) {
                                            tempCheckboxModel.push(JSON.parse(d));
                                        });
                                        vm.checkboxModel = tempCheckboxModel;
                                    }
                                    var dashSaveObj = {};
                                    var dashRepObj = {};
                                    var dObj = "";
                                    reportsObject['metadataObject'] = $scope.metadataObject;
                                    reportsObject['connectionObject'] = $scope.metadataObject.connObject.connectionObject;
                                    if (vm.dataCount < $rootScope.localDataLimit) {
                                        reportsObject['colorObject'] = eChart.chartRegistry.listAttributes();
                                    } else {
                                        reportsObject['colorObject'] = eChartServer.chartRegistry.listAttributes();
                                    }
                                    reportsObject['columnDeleted'] = vm.columnDeleted;
                                    var data = {
                                        "name":$scope.dashName.dashboardName,
                                        "dashboardDesc":$scope.dashboardDesc,
                                        "repotsObject":JSON.stringify(reportsObject),
                                        "filterShow":$scope.filterShowHidden,
                                        "filterBy":JSON.stringify(vm.checkboxModel),
                                        "image":image_name,
                                        "group":JSON.stringify(vm.categoryGroupObject),
                                        "reportGroup":$scope.reportGrp,
                                        "publicViewType":$scope.publicViewType,
                                        "notificationObj":vm.notificationReportObject
                                    };
                                    dataFactory.request($rootScope.DashboardSave_Url, 'post', data).then(function (response) {
                                        if (response.data.errorCode == 1) {
                                            dataFactory.successAlert(response.data.message);
                                            var id = response.data.result;
                                            $window.location = '#/dashboard/view/' + id + '/1';
                                            $(".loadingBar").hide();
                                        } else {
                                            dataFactory.errorAlert(response.data.message);
                                            $(".loadingBar").hide();
                                        }
                                    });
                                }
                            });
                        }
                    });
            };
            $scope.closedashboard = function () {
                $window.location = '#/dashboard/';
            }
            vm.measureDimensionDropdown = function (id, index) {
                $("#" + id).addClass("overflowRemove");
                if (id == "dimensionDiv") {
                    dropDownFixPosition($('.buttonDropdownDimension' + index), $('.custDropdownDimension'));
                } else {
                    dropDownFixPosition($('.buttonDropdownMeasure' + index), $('.custDropdownMeasure'));
                }
            }
            vm.dateFormatCheck = function () {
                if (vm.chartSettingObject.xAxisDataFormate == "custom") {
                    vm.customDate = true;
                } else {
                    vm.customDate = false;
                }
            }

            function dropDownFixPosition(button, dropdown) {
                var scrollPos = $(document).scrollTop();
                var dropDownTop = button.offset().top - scrollPos - 40;

                //                var dropDownTop = button.offset().top - scrollPos + button.outerHeight();

                dropdown.css('top', dropDownTop + "px");
                dropdown.css('left', (button.offset().left) - 160 + "px");
            }

            $(window).click(
                function () {
                    $("#measureDiv").removeClass(
                        "overflowRemove");
                    $("#dimensionDiv").removeClass(
                        "overflowRemove");
                });

            function onscrollDrawer() {
                $("#measureDiv").removeClass("overflowRemove");
                $("#dimensionDiv").removeClass("overflowRemove");
                $(".dropdown-menu").hide();
            }

            vm.removeFilter = function (index) {
                vm.filtersToApply.splice(index, 1);
            }
            vm.applyLineBar = function (lineBar) {
                sketch._expandLineBarAxisConfig(lineBar);
            }
            //Model box close
            $scope.modelClose = function () {
                $(".reportDashboard").removeClass(
                    "background-container");
            }
            //On scroll to hide dropdown
            $(window).scroll(function () {
                $(".colorPalette").hide();
            });
            $(document).click(function (e) {
                e.stopPropagation();
                var container = $(".widget-content");
                //check if the clicked area is dropDown or not
                if (container.has(e.target).length === 0) {
                    $('.colorPalette').hide();
                }
            });
            //Dashboard edit to fetch data
            //Get all data and chart draw
            vm.isTimeObjectFound = function (reportArray) {
                var flag = -1;
                reportArray.forEach(function (d, index) {
                    if (d.axisConfig.timeline != undefined) {
                        flag = index;
                    }
                });
                return flag;
            }
            $(".loadingBar").show();
            var data = {
                "id": $scope.dashboardId
            };
            dataFactory.request($rootScope.DashboardIdToObject_Url, 'post', data).then(function (response) {
                if (response.data.errorCode == 1) {
                    vm.dashboard_Obj = JSON.parse(response.data.result.reportObject);
                    vm.filter_Obj = JSON.parse(response.data.result.filterBy);
                    try {
                        $scope.dashName.dashboardName = response.data.result.name;
                        var reportObject = JSON.parse(response.data.result.reportObject);
                        vm.colorObject = reportObject.colorObject;
                        var index = vm.isTimeObjectFound(reportObject.Report);
                        if (reportObject.Report[index] && reportObject.Report[index].axisConfig != undefined)
                            vm.rangeObject = reportObject.Report[index].axisConfig.timeline;
                        if (index != -1) {
                            vm.rangeObject = reportObject.Report[index].axisConfig.timeline;
                        }
                        vm.colorObject = reportObject.colorObject;
                        eChart.chartRegistry.registerAllAttributes(reportObject.colorObject);
                    } catch (e) {

                    }
                    //delete reportObject.metadataObject.column;
                    vm.querySelector = reportObject.metadataObject;
                    vm.metadataSelect = {};
                    vm.selectDataSource(vm.querySelector);
                    vm.DashboardModel.Dashboard = reportObject;
                    vm.DashboardModel.Dashboard.Report.forEach(function (d) {
                        delete d.chart['$$hashKey'];
                        if (d.axisConfig.xLabel) {
                            delete d.axisConfig.xLabel.key;
                            delete d.axisConfig.xLabel.value;
                            delete d.axisConfig.yLabel.key;
                            delete d.axisConfig.yLabel.value;
                        }
                    });
                    vm.metadataObject = reportObject.metadataObject;
                    vm.metadataId = vm.metadataObject.metadataId;
                    vm.categoryGroupObject = JSON.parse(response.data.result.group);

                    /*
                             Check memory
                          */
                    var data = {
                        "matadataObject": JSON.stringify(vm.metadataObject),
                        "type": 'dashboard'
                    };
                    var fetchColumn;
                    dataFactory.request($rootScope.MetadataGetLength_Url, 'post', data).then(function (response) {
                        if (response.data.errorCode == 1) {
                            vm.dataCount = response.data.result;
                            if (vm.dataCount < $rootScope.localDataLimit) {
                                fetchColumn = fetchDataAndColumns();
                            } else {
                                fetchColumn = fetchDataAndColumnsServer();
                            }
                        }

                    }).then(function () {

                        dataFactory.request($rootScope.MetadataDashboardList_Url, 'post', "").then(function (response) {
                            if (response.data.errorCode == 1) {
                                vm.queryList = JSON.parse(response.data.result);
                            } else {
                                dataFactory.errorAlert("Check your connection");
                            }
                        }).then(function () {
                            if ($stateParams.data != undefined) {
                                var i = 0;
                                var index;
                                $scope.queryList.forEach(function (d, indexArray) {
                                    if (d.metadataId == vm.metadataId) {
                                        index = indexArray;
                                        vm.SelectedNameShow = d.name;
                                        $scope.$applyAsync
                                    }
                                });
                            }
                        });
                        fetchColumn.then(function () {
                            vm.DashboardView.render();
                            vm.preLoad = false;
                            vm.graphAxis = true;
                            vm.loadingBarDrawer = false;
                            $scope.loadingBar = true;
                            $scope.loadingBarView = true;
                            var rawData = vm.tableData;
                            vm.metadataSelect.name = reportObject.metadataObject.metadataId;
                            var rangeData = vm.rangeObject;
                            vm.checkboxModel = JSON.parse(response.data.result.filterBy);
                            vm.filterAdd();
                            if (!$scope.$$phase) {
                                $scope.$apply();
                            }
                            setTimeout(function () {
                                $("#filter").selectpicker();
                                if (!$.isEmptyObject(vm.rangeObject)) {
                                    var totalCol = rangeData[Object.keys(rangeData)[0]].numberOfColumn;
                                    for (var i = 0; i < totalCol; i++) {
                                        var d = new Date();
                                        var n = d.getFullYear();
                                        var year = n - i;
                                        var setStartDate = '01-01-' + year;
                                        var setEndDate = '31-12-' + year;
                                        $('.rangeStartFilter' + i).val(setStartDate);
                                        $('.rangeEndFilter' + i).val(setEndDate);
                                    }
                                    var rangeStartDate = $('.dateTimeLineStart').datepicker({
                                        dateFormat: 'dd-mm-yy',
                                        onSelect: function (e) {
                                            var setIndex = vm.filterColumnRange.index;
                                            var startDate = $(rangeStartDate[setIndex]).val();
                                            var endDate = $(rangeEndDate[setIndex]).val();
                                            var tempStart = startDate; //31-12-2017
                                            var tempEnd = endDate;
                                            startDate = tempStart.split("-").reverse().join("-");
                                            endDate = tempEnd.split("-").reverse().join("-");
                                            rangeFilterEditChange(startDate, endDate);
                                        }
                                    });
                                    var rangeEndDate = $('.dateTimeLineEnd').datepicker({
                                        dateFormat: 'dd-mm-yy',
                                        onSelect: function (e) {
                                            var setIndex = vm.filterColumnRange.index;
                                            var startDate = $(rangeStartDate[setIndex]).val();
                                            var endDate = $(rangeEndDate[setIndex]).val();
                                            var tempStart = startDate; //31-12-2017
                                            var tempEnd = endDate;
                                            startDate = tempStart.split("-").reverse().join("-");
                                            endDate = tempEnd.split("-").reverse().join("-");
                                            rangeFilterEditChange(startDate, endDate);
                                        }
                                    });


                                    function rangeFilterEditChange(startDate, endDate) {
                                        var date = {};
                                        date['start'] = (moment(startDate)).format('YYYY-MM-DD');
                                        date['end'] = (moment(endDate)).format('YYYY-MM-DD');
                                        //Date
                                        var columnName = $scope.filterColumnRange.columnName;
                                        var index = $scope.filterColumnRange.index;
                                        vm.rangeObject[columnName].period[index] = date;
                                        //End
                                        //vm.DashboardView.renderChartInActiveContainer();
                                        if (!$.isEmptyObject(vm.rangeObject)) {
                                            vm.Attributes['timeline'] = vm.rangeObject;
                                            var timelineObj = vm.rangeObject;
                                            sketch.getTimeLineFilteredData(vm.tableData, vm.rangeObject, function (data) {
                                                vm.filteredData = data;

                                                vm.DashboardModel.Dashboard.Report.forEach(function (d) {
                                                    vm.Attributes = d.axisConfig;
                                                    d.axisConfig['timeline'] == vm.rangeObject;
                                                    sketch
                                                        .axisConfig(d.axisConfig)
                                                        .data(data)
                                                        .container(d.reportContainer.id)
                                                        .chartConfig(d.chart)
                                                        .render();
                                                });
                                            });
                                        }
                                    }

                                    sketch.getTimeLineFilteredData(vm.tableData, vm.rangeObject, function (data) {
                                        vm.DashboardModel.Dashboard.Report.forEach(function (d, i) {
                                            d.axisConfig['timeline'] = vm.rangeObject;
                                            sketch
                                                .axisConfig(d.axisConfig)
                                                .data(data)
                                                .container(d.reportContainer.id)
                                                .chartConfig(d.chart)
                                                .render();
                                        });
                                    });
                                }
                                if (vm.rangeObject)
                                    vm.Attributes['timeline'] = vm.rangeObject;
                            }, 500);
                            vm.filterTab = true;
                            setTimeout(function () {
                                $(".loadingBar").hide();
                            }, 1000);
                        });
                    })
                } else {
                    dataFactory.errorAlert("check your connection");
                }
            });
            $(function () {
                setTimeout(function () {
                    $('img.svg').each(function () {
                        var $img = jQuery(this);
                        var imgID = $img.attr('id');
                        var imgClass = $img.attr('class');
                        var imgURL = $img.attr('src');

                        jQuery.get(imgURL, function (data) {
                            // Get the SVG tag, ignore the rest
                            var $svg = jQuery(data).find('svg');

                            // Add replaced image's ID to the new SVG
                            if (typeof imgID !== 'undefined') {
                                $svg = $svg.attr('id', imgID);
                            }
                            // Add replaced image's classes to the new SVG
                            if (typeof imgClass !== 'undefined') {
                                $svg = $svg.attr('class', imgClass + ' replaced-svg');
                            }
                            // Remove any invalid XML tags as per http://validator.w3.org
                            $svg = $svg.removeAttr('xmlns:a');
                            // Check if the viewport is set, else we gonna set it if we can.
                            if (!$svg.attr('viewBox') && $svg.attr('height') && $svg.attr('width')) {
                                $svg.attr('viewBox', '0 0 ' + $svg.attr('height') + ' ' + $svg.attr('width'))
                            }
                            // Replace image with new SVG
                            $img.replaceWith($svg);
                        }, 'xml');
                    });
                }, 100)

            });
            $scope.updateMetadata = function () {
                if (vm.metadataObject.connObject && vm.metadataObject.connObject.type) {
                    $window.location = "#/dashboard/metadata/blending/" + $scope.dashboardId;
                } else {
                    $window.location = "#/dashboard/metadata/" + $scope.dashboardId;
                }
            }
            /*
                 * Apply all filter
                 */
            vm.allFilterApply = function () {
                $(".loadingBar").show();
                sketchServer.applyAllFilter(vm.allFilters, vm.metadataId);
            }
            vm.notificationEmail = {
                init: function () {
                    vm.frequencyTime = true;
                    vm.frequencyMin = true;
                    vm.emailTypeSelect = false;
                },
                modal: function () {
                    vm.nEmail = {};
                    vm.nEmail.frequency = "everyhours";
                    vm.nEmail.emailInterval = "once";
                    var data = {
                        'dashboardId': vm.dashboardId,
                        'containerId': vm.DashboardModel.Dashboard.activeReport.reportContainer.id
                    };
                    dataFactory.request($rootScope.emailNotificationGetData_Url, 'post', data).then(function (response) {
                        if (response.data.errorCode == 1) {
                            vm.notificationObj = JSON.parse(response.data.result.notificationObj);
                            vm.nEmail = JSON.parse(response.data.result.notificationObj);
                            vm.notificationEmail.frequency(vm.nEmail.frequency);
                            if (vm.nEmail.emailType == "specific") {
                                vm.notificationEmail.emailSelectType(vm.nEmail.emailType);
                            }
                            if (vm.nEmail.emailInterval == "timeInterval") {
                                vm.notificationEmail.emailInterval(vm.nEmail.emailInterval);
                            }
                        }
                    }).then(function () {
                        $('#notificationEmail').modal('show');
                        $('#timepicker').datetimepicker({
                            format: 'HH:mm'
                        });
                    });
                },
                frequency: function (type) {
                    vm.frequencyTime = true;
                    vm.frequencyMin = true;
                    if (type == "24hours") {
                        vm.frequencyTime = false;
                    } else if (type == "everyhours") {
                        vm.frequencyTime = true;
                    } else {
                        vm.frequencyMin = false;
                    }
                },
                emailInterval: function (type) {
                    if (type == "timeInterval") {
                        vm.emailInterval = true;
                    } else {
                        vm.emailInterval = false;
                    }

                },
                emailSelectType: function (emailType) {
                    if (emailType == "specific") {
                        // All Users
                        vm.emailTypeGrp = false;
                        vm.emailTypeSelect = true;
                        dataFactory.request($rootScope.UsersEmailList_Url, 'post', "").then(function (response) {
                            if (response.data.errorCode == 1) {
                                vm.usersEmailList = response.data.result;
                                if (vm.notificationObj != undefined) {
                                    vm.nEmail.emailId = vm.notificationObj.emailId;
                                }
                                $("#emailId").selectpicker('destroy');
                                setTimeout(function () {
                                    $("#emailId").selectpicker();
                                    $scope.$apply();
                                }, 1000);
                            }
                        });
                    } else if (emailType == "group") {
                        //Group
                        vm.emailTypeSelect = false;
                        dataFactory.request($rootScope.EmailGrpList_Url, 'post', "").then(function (response) {
                            if (response.data.errorCode == 1) {
                                vm.emailGrpList = response.data.result;
                                setTimeout(function () {
                                    $("#emailIdGrp").selectpicker('destroy');
                                    $("#emailIdGrp").selectpicker();
                                    vm.emailTypeGrp = true;
                                    $scope.$apply();
                                }, 1000);
                            }
                        });
                    } else {
                        vm.emailTypeSelect = false;
                    }
                },
                save: function (nEmailObj) {
                    var data = {
                        "emailObj": JSON.stringify(nEmailObj),
                        "connection": JSON.stringify(vm.DashboardModel.Dashboard.activeReport.axisConfig),
                        "dashboardId": $scope.dashboardId,
                        "reportId": vm.DashboardModel.Dashboard.activeReport.reportContainer.id
                    };
                    dataFactory.request($rootScope.EmailNotificationSave_Url, 'post', data).then(function (response) {
                        if (response.data.errorCode == 1) {
                            dataFactory.successAlert("Email notification save succesfully");
                            $('#notificationEmail').modal('hide');
                        }
                    });
                }
            }
            vm.notificationEmail.init();


            // Select Cascade Filter Start
            vm.checkCascadeFilter = function (filterName) {
                var filterVal = $("input[id='" + filterName + "']").is(":checked");
                vm.filerTemp.forEach(function (d) {
                    if (filterName == d.key) {
                        if (filterVal == true || filterVal == 'true') {
                            d.cascade = true;
                        } else {
                            d.cascade = false;
                        }
                    }
                });
            }
            // Select Cascade Filter End


            // Order Cascade Filter Start
            vm.cascadeFilterArray = [];
            vm.OrderCascadeFilter = function () {
                vm.filerTemp.forEach(function (d) {
                    if (d.cascade == true && !vm.cascadeFilterArray.includes(d.key)) {
                        vm.cascadeFilterArray.push(d.key);
                    }
                    if (d.cascade == false) {
                        var attrIndex = vm.cascadeFilterArray.indexOf(d.key);
                        vm.cascadeFilterArray.splice(attrIndex, 1);
                    }
                });

                $('#orderCascade').modal('show');
                $(".sortable").sortable({
                    cancel: ".fixed",
                    stop: function (event, ui) {
                        vm.cascadeFilterArray = $(this).sortable("toArray");
                    }
                });
            }

            vm.SaveOrderCascadeFilter = function () {
                $('#orderCascade').modal('hide');
                var tempCheckboxModel = [];
                vm.cascadeFilterArray.forEach(function (data, index) {
                    vm.checkboxModel.forEach(function (d) {
                        var dataObj = JSON.parse(d);
                        if (data == dataObj.columnName) {
                            dataObj['filterOrder'] = index;
                            tempCheckboxModel.push(JSON.stringify(dataObj));
                        }
                    });
                });
                vm.checkboxModel = tempCheckboxModel;
                vm.filterAdd();
            }
            // Order Cascade Filter End
// Include Exclude Keys Start
            vm.excludeIncludeKey={
                addRemove:function(){
                    $('#Exclude_Include_Key').modal('show');
                    /*
                     * Chart js data
                     */
                    var chart = eChartServer.chartRegistry.get("chart-"+vm.DashboardModel.Dashboard.activeReport.reportContainer.id);
                    var arr1 = chart._data.labels;
                    var arr2 = [];
                    var tempArr = vm.DashboardModel.Dashboard.Report[vm.DashboardModel.Dashboard.activeReportIndex].axisConfig['excludeKey'];
                    if(tempArr != undefined){
                        var lengthCount=tempArr.length;
                        tempArr.forEach(function(d,index){
                            arr2.push(d);
                            setTimeout(function(){
                                if(tempArr != undefined && lengthCount-1==index){
                                    tempArr.forEach(function(d){
                                        $("[id='"+ d +"']").prop('checked', true);
                                    });
                                }
                            }, 100);
                        });
                    }
                    if(arr2.length){
                        vm.Exclude_Key_Data = arr2.concat(arr1);
                    }else{
                        vm.Exclude_Key_Data = arr1;
                    }
                    if(tempArr==undefined || tempArr.length==0){
                        setTimeout(function(){
                            arr1.forEach(function(d,index){
                                $("[id='"+ d +"']").prop('checked', false);
                            });
                        },100);
                    }
                },
                save:function () {
                    $('#Exclude_Include_Key').modal('hide');
                    if(vm.DashboardModel.Dashboard.Report[vm.DashboardModel.Dashboard.activeReportIndex].axisConfig['excludeKey']==undefined){
                        vm.DashboardModel.Dashboard.Report[vm.DashboardModel.Dashboard.activeReportIndex].axisConfig['excludeKey']=[];
                    }
                    var dataArr = [];
                    vm.Exclude_Key_Data.forEach(function (d){
                        var chkbox_Val = $("[id='"+ d +"']").is(':checked');
                        if(chkbox_Val){
                            dataArr.push(d);
                        }
                    });
                    vm.DashboardModel.Dashboard.Report[vm.DashboardModel.Dashboard.activeReportIndex].axisConfig['excludeKey'] = dataArr;
                    vm.DashboardView.processChart();
                }
            };
            // Include Exclude Keys End

        }]);