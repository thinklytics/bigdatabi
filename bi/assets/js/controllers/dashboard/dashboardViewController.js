'use strict';
/* Controllers */
angular.module('app', [ 'ngSanitize','gridster' ])
    .controller('dashboardViewController',['$scope','$sce','dataFactory','$rootScope','$window','$q','$stateParams','$cookieStore','$location',function($scope,$sce,dataFactory,$rootScope,$window,$q,$stateParams,$cookieStore,$location) {
    	//--- Top Menu Bar
    	var data={};
		var addClass='';
		var viewClass='';
        var vm = $scope;
		if($location.path()=='/dashboard/'){
			viewClass='activeli'; 
		}else{
			addClass='activeli';
		}
		data['navTitle']="DASHBOARD";
		data['icon']="fa fa-line-chart";
		data['navigation']=[
		    {
				"name":"Add",
				"url":"#/dashboard/add",
				"class":addClass
    		},
            {
                "name":"View",
                "url":"#/dashboard/",
                "class":viewClass
            },
            {
                "name":"Close",
                "url":"#/dashboard/",
                "class":viewClass
            }
        ];
		$rootScope.$broadcast('subNavBar', data);
        //End Menu
        $scope.permissionCheck=function(permission){
        	return dataFactory.checkPermission(permission);
        }
        var vm = $scope;

        $scope.imageUrlBaseUrl=dataFactory.baseUrlData()+"dashboardImage/";
    	var dashboardId=$stateParams.data;
    	var userId=$cookieStore.get('userId');
    	if(userId==undefined){
    	    userId=1;
    	}

        $scope.dashboardListObj=function () {
            var dashboardListObjLoad = new Promise(function (resolve, reject) {
                dataFactory.request($rootScope.DashboardList_Url,'post',"").then(function(response) {
                    console.log(response.data.result);
                    resolve(response);
                    if(response.data.errorCode==1){
                        $scope.departmentFolder = Object.keys(response.data.result);
                        $scope.dashboardList = response.data.result;
                    }else{
                        dataFactory.errorAlert(response.data.message);
                    }
                });
            });
            dashboardListObjLoad.then(temp_one).then(temp_two);
        }
        $scope.dashboardListObj();

        function temp_one() {
            $scope.dataLoaded=true;
            $scope.loading=true;
            $scope.$apply();
            if($scope.loading){
                $(".loaderImage").fadeOut("slow");
            }
            setTimeout(function(){
                $('[rel="tooltip"]').tooltip();
            },100);
        }

        function temp_two() {

        }


        $scope.addDashboard = function(){
            $window.location = '#/dashboard/add';
        };



// Dashboard Delete
        $scope.showSwal = function(type){
            if(type == 'warning-message-and-cancel') {
                swal({
                    title: 'Are you sure?',
                    text: 'You want to delete Dashboard !',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: 'Yes',
                    cancelButtonText: 'No',
                    confirmButtonClass: "btn btn-success",
                    cancelButtonClass: "btn btn-danger",
                    buttonsStyling: false,
                }).then(function(value){
                    if(value){
                        $scope.delete();
                    }
                },
                function (dismiss) {

                })
            }
        }

        $scope.delete = function(){
            var index; 
            var id=$scope.dashboardId;
            $scope.dashboardSelectedFromList.forEach(function (d,i) {
                if(d.dashboard_id==id){
                    index=i;
                    var data={
                        "id":id
                    };
                    dataFactory.request($rootScope.DashboardDelete_Url,'post',data).then(function(response) {
                        if(response.data.errorCode==1){
                            dataFactory.successAlert("Delete Successfully");
                            $scope.dashboardSelectedFromList.splice(index,1);
                            $('.stepFolder1').slideDown('fast');
                            $('.stepFolder').slideDown('fast');
                            $('.stepFolder3').slideUp('fast');
                            $scope.selectedConfirm = 0;
                            $scope.backBtn = 0;
                        }else{
                            dataFactory.errorAlert(response.data.message);
                        }
                    });
                    $scope.selectedConfirm=0;
                }
            });
        }
// Dashboard Delete







// sort dashboard list asc-desc Start
        $scope.dashboardList_Order = 'desc';
        $scope.dashboard_List_order = '-id';
        $scope.orderDashboardList = function(){
            if($scope.dashboardList_Order == 'desc'){
                $scope.dashboard_List_order = '-id';
            }
            else if($scope.dashboardList_Order == 'asc'){
                $scope.dashboard_List_order = 'id';
            }
        }
// sort dashboard list asc-desc End

        vm.loadDashboardList = function(){
            setTimeout(function(){
                $('[data-toggle="popover"]').popover({
                    container: 'body',
                    html: true,
                    placement: 'auto',
                    trigger: 'hover',
                    content: function() {
                        var url = $(this).data('full');
                        return '<img src="' + url + '" onError="this.src = \'assets/img/logo.jpg\';" style="width:200px;height:200px;">'
                    }
                });
            },500);
            $('.imageToggle').removeClass('selectedImage');
            $scope.loadingBarDrawer = true;
            $('.stepFolder').slideUp('fast');
            $('.stepBack').slideUp('fast');
            setTimeout(function(){
                $(function () {
                    $('[data-toggle="tooltip"]').tooltip();
                })
            },100);
            setTimeout(function () {
                $scope.loadingBarDrawer = false;
                $('.stepFolder1').slideUp('fast');
                $('.stepFolder2').slideUp('fast');
                $('.stepLoading').slideUp('fast');
                $('.stepFolder3').slideDown('fast');
                $('.stepBack').slideDown('fast');
            }, 300);
            $scope.backBtn = 1;
        }

        vm.selectedGrpFolder = function(){
            vm.groupFolderList = Object.keys(vm.dashboardList[vm.sDepartment]);
            $scope.backBtn = 1;
            setTimeout(function () {
                $('[rel="tooltip"]').tooltip();
                $scope.loadingBarDrawer = false;
                $('.stepFolder1').slideUp('fast');
                $('.stepLoading').slideUp('fast');
                $('.stepFolder2').slideDown('fast');
            }, 300);
        }

        vm.toggleGrpFolder = function(e, sView, index){
            vm.selectedSharedViewName = sView;
            vm.dashboardSelectedFromList = vm.dashboardList[vm.sDepartment][sView];
            vm.loadDashboardList();
        }





        $scope.toggle = function(e,dashboardObject,index){
            setTimeout(function(){
                $(function () {
                    $('[data-toggle="tooltip"]').tooltip();
                })
            },100);
            $('.imageToggle').removeClass('selectedImage');
            $scope.selectedIndex=index;
            $scope.dashboardId=dashboardObject.dashboard_id;
            $(e.currentTarget).addClass('selectedImage');
            $scope.selectedConfirm=1;
        }
        $scope.selectedConfirm=0;
        vm.grpFolder = false;
        $scope.Folder = {
            toggleImage: function (e, sView, index) {
                if(sView == 'Public View'){
                    $scope.selectedSharedViewName = sView;
                    $scope.dashboardSelectedFromList = $scope.dashboardList[$scope.selectedSharedViewName];
                    vm.loadDashboardList();
                }else{
                    $scope.grpFolder = true;
                    vm.sDepartment = sView;
                    vm.selectedGrpFolder();
                }
            },
            backToFolderView: function () {
                $scope.search = '';
                $('.stepFolder1').slideDown('fast');
                $('.stepFolder').slideDown('fast');
                $('.stepFolder3').slideUp('fast');
                $scope.selectedConfirm = 0;
                $scope.backBtn = 0;
            }
        };

        $scope.exportImport={
            init:function () {
                $scope.import=0;
                $scope.overwrite={};
                $scope.overwrite.datasource=1;
                $scope.overwrite.metadata=1;
                $scope.overwrite.dashboard=1;
                /*
                  Which folder to save
                 */
                // Call modal
                $scope.groupTypeModal=function () {
                    $scope.publicViewType="specific_group";
                    $(".footerDiv").hide();
                    $scope.reportGrp={};
                    dataFactory.request($rootScope.UsergroupList_Url,'post',"").then(function(response){
                        $scope.userGroup=response.data.result;
                    });
                    dataFactory.request($rootScope.ReportGroupList_Url,'post',"").then(function(response){
                        $scope.reportGroup=response.data.result;
                    });
                    $('#reportGroup').modal('show');
                }
                /*
                  Folder to save function
                 */
                $scope.groupTypeSave=function () {
                    $(".loaderImage").show();
                    if($scope.metadataImport['file']==undefined){
                        $scope.addClassRemove("File is required");
                        return false;
                    }
                    var file=$scope.metadataImport['file'];
                    var allowedFiles=["think"];
                    var file=$scope.metadataImport.file;
                    var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:])+(" + allowedFiles.join('|') + ")$");
                    if (!regex.test(file.name.toLowerCase())) {
                        $scope.addClassRemove("Please upload files having extensions: <b>" + allowedFiles.join(', ') + "</b> only.");
                        return false;
                    }
                    var data={
                        "overwrite":$scope.overwrite
                    };
                    var url=$rootScope.DashboardImportSave_Url;
                    dataFactory.importDataSave(url,file,'',$scope.reportGrp,$scope.publicViewType,JSON.stringify(data)).then(function(response) {
                        if(response.data.errorCode){
                            $(".loaderImage").hide();
                            $scope.exportImport.backToFolder();
                            dataFactory.successAlert(response.data.message);
                            $scope.dashboardListObj();
                        }else{
                            dataFactory.errorAlert(response.data.message);
                        }
                    });
                    $('#reportGroup').modal('hide');

                }
                $scope.selectGroup=function(divClass){
                    if(divClass=='back'){
                        $(".mainDiv").slideDown('fast');
                        $(".select").slideUp('fast');
                        $(".create").slideUp('fast');
                        $(".footerDiv").slideUp('fast');
                    }else{
                        $("."+divClass).slideDown('fast');
                        $(".footerDiv").slideDown('fast');
                        $(".mainDiv").slideUp('fast');
                    }
                    if(divClass=='select' || divClass=='create'){
                        $scope.reportGrp={};
                    }
                }
                $scope.checkView=function(page){
                    $scope.publicViewType=page;
                    if(page=='public_group'){
                        $(".subDiv").hide();
                        $(".footerDiv").show();
                    }
                    else{
                        $(".subDiv").show();
                        $(".footerDiv").hide();
                    }
                }
            },
            importBtn:function () {
                $scope.import=1;
                $('.step1').slideUp('fast');
                $('.step2').slideDown('fast');
                $scope.testsuccess=false;
                $scope.datasourceStatus=0;
                $scope.metadataStatus=0;
            },
            testImport:function (Import,type) {
                $(".loaderImage").fadeIn("slow");
                if($scope.metadataImport==undefined || $scope.metadataImport['file']==undefined){
                    dataFactory.errorAlert("File is required");
                    return false;
                }
                var file=$scope.metadataImport['file'];
                var allowedFiles=["think"];
                var file=$scope.metadataImport.file;
                var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:])+(" + allowedFiles.join('|') + ")$");
                if (!regex.test(file.name.toLowerCase())) {
                    dataFactory.errorAlert("Please upload files having extensions: <b>" + allowedFiles.join(', ') + "</b> only.");
                    return false;
                }
                $scope.testBtn="Loading...";
                var url=$rootScope.DashboardImport_Url;
                dataFactory.importData(url,file,type).then(function(response) {
                    if(response.data.errorCode){
                        $(".loadingBar").hide();
                        $scope.Attributes=response.data.result;
                        dataFactory.errorAlert(response.data.message);
                        $scope.importSave=1;
                        $scope.testsuccess=true;
                        $scope.datasourceStatus=response.data.result.datasource;
                        $scope.metadataStatus=response.data.result.metadata;
                        $scope.dashboardStatus=response.data.result.dashboard;
                        $scope.datasourceOverwrite=true;
                        $scope.metadataOverwrite=true;
                    }else{
                        dataFactory.errorAlert(response.data.message);
                    }
                    setTimeout(function(){
                        $scope.loading=false;
                        if(!$scope.loading){
                            $(".loaderImage").fadeOut("slow");
                        }
                    },1000);
                });
            },
            backToFolder:function () {
                $scope.import=0;
                $('.step2').slideUp('fast');
                $('.step1').slideDown('fast');
            },
            export:function () {
                var data={
                    "dashboard_id":$scope.dashboardId
                };
                dataFactory.request($rootScope.DashboardExport_Url,'post',data).then(function(response) {
                    if(response.data.errorCode){
                        var link=document.createElement('a');
                        link.href=response.data.result.url;
                        link.download=response.data.result.fileName;
                        link.click();
                        var data={
                            "url":response.data.result.url
                        };
                        dataFactory.request($rootScope.ExportFileDelete_Url,'post',data);
                    }
                })
            }
        };
        $scope.exportImport.init();

        
        
        
        
    }]); 