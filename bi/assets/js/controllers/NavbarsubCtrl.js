'use strict';
angular.module('app')
.controller('NavbarsubCtrl', ['$scope', '$location','AuthenticationService','dataFactory','$rootScope','$state','$compile', function($scope, $location,AuthenticationService,dataFactory,$rootScope,$state,$compile) {
	$scope.subNavBarMenu=false;
	var vm=$scope;
	$scope.$on('subNavBar', function (event, data) {
		$scope.navTitle=data.navTitle; 
		$scope.icon=data.icon; 
		$scope.navigation=data.navigation;
		if(data.subNavBarMenu==undefined)
			$scope.subNavBarMenu=true;
		else
			$scope.subNavBarMenu=false;
	});
	$scope.toggleDrawer=function(){
    	$(".sideSubBarWidth1").toggleClass("drawerClose",'drawerOpen');
    	$(".sidebar").toggleClass("sideBarHide",'sideBarShow');   
    	$(".sideSubBarWidth2").toggleClass("drawerFullSize",'');  
    	setTimeout(function(){
    		$(window).resize();
    	},300);
    }
	$scope.logout =function(){
		 $location.path('/Login');
		 AuthenticationService.ClearCredentials();
		 $scope.navBarMenu=false;
	}
	$scope.changeNav=function(){
		window.location.reload();
	}
	vm.url={
		"datasource":{
			"add":"#/datasource/add",
			"list":"#/datasource/"
		},
		"metadata":{
			"add":"#/metadata/add",
			"list":"#/metadata/"
		},
		"ml":{
			"add":"#/machine/add",
			"list":"#/machine/"
		},
		"dashboard":{
			"add":"#/dashboard/add",
			"list":"#/dashboard/"
		},
		"sharedview":{
			"add":"#/sharedView/add/1",
			"list":"#/sharedView/"
		}
	};
	
	vm.menuObject={};
	vm.state=$state;
	setTimeout(function(){
		rolesCheck();
	},1000);
	function rolesCheck(){
        var locationUrl=$location.path().split("/");
		var urlAllow="";
        if(locationUrl[1]=="publicView" || locationUrl[1]=="sharedView" && (locationUrl[2]=="show" || locationUrl[2]=="mobileView"))
			urlAllow="/"+locationUrl[1]+"/"+locationUrl[2];
        if(urlAllow=="/publicView/show" || urlAllow=="/sharedView/show" || urlAllow=="/publicView/mobileView" || urlAllow=="/sharedView/mobileView"){
        	return true;
		}
		dataFactory.request($rootScope.Loginuserrole_Url,'post',"").then(function(response){
			if(response.data.errorCode==1){
				vm.roles=response.data.result;
				$rootScope.roleObj=vm.roles;
				if(vm.roles[0]["roles"] && vm.roles[0]["roles"][0] && vm.roles[0]["roles"][0]["name"]!=undefined){
					$rootScope.role=vm.roles[0]["roles"][0]["name"];
				}else{
					dataFactory.errorAlert("You have no permission");
					$location.path("#/Login");
					return false; 
				}
				var permissionCheck=false;
                vm.roles[0]["roles"][0]["perms"].sort(function(a, b){
                    return a.order-b.order;
                });

				vm.roles[0]["roles"][0]["perms"].forEach(function(d){
					if($state.current.permission==d.name || $state.current.permission==undefined){
						permissionCheck=true;
					}
					var pageArray = d.name.split("-");
					if(vm.menuObject[pageArray[0]]==undefined){
						vm.menuObject[pageArray[0]]=[];
						if(pageArray[1]=="list" || pageArray[1]=="add" || pageArray[1]=="setting")
						vm.menuObject[pageArray[0]].push(pageArray[1]);
					}else{
						if(pageArray[1]=="list" || pageArray[1]=="add" || pageArray[1]=="setting")
						vm.menuObject[pageArray[0]].push(pageArray[1]);
					}
				}); 
				if(permissionCheck==false && $state.current.permission==undefined){
					$location.path("#/Login");
				}
				var p = "";
				Object.keys(vm.menuObject).forEach(function(key) {
					if(vm.menuObject[key].length){
						if(key=='ml'){
							/*
							 * For ml modal static
							 */
							var tempKey="machine";
                            p+="<li class='dropdown' ng-class='mainMenuActive(\""+tempKey+"\")\' ><a href='javascript:void(0)' class='dropdown-toggle' data-toggle='dropdown' aria-expanded='true'>m/l<b class='caret'></b></a>";
						}else{
                            p+="<li class='dropdown' ng-class='mainMenuActive(\""+key+"\")\' ><a href='javascript:void(0)' class='dropdown-toggle' data-toggle='dropdown' aria-expanded='true'>"+key+"<b class='caret'></b></a>";
						}

						p +="<ul class='dropdown-menu'>";
						vm.menuObject[key].forEach(function(d){
                            var urlStr=vm.url[key][d];
							p+='<li ng-class="subMenuActive(\''+urlStr+'\')"><a href="'+vm.url[key][d]+'" style="text-transform: capitalize;">'+d+'</a></li>';
						});
                        if(key=="metadata"){
                            p+="<li class='dropdown-submenu'><a href='javascript:void(0)' class='dropdown-toggle' data-toggle='dropdown' aria-expanded='true'>Data Blending</a>";
                            p +="<ul class='dropdown-menu'>";
                            p+="<li><a href='#/metadata/blending/add' class='dropdown-toggle' data-toggle='dropdown' aria-expanded='true'>Add</a></li>";
                            p+="<li><a href='#/metadata/blending/' class='dropdown-toggle' data-toggle='dropdown' aria-expanded='true'>List</a></li>";
                            p +="</ul></li>";
                        }
						p+="</ul>";
						p+="</li>";
					}else if(key=="setting"){
						p+="<li ng-class='mainMenuActive(\""+key+"\")\'><a href='#/setting/role_assign'>Administration</a>";
						p+="</li>";
					}
				});
				//$("#main_menu").append(p);
                angular.element(document.getElementById('main_menu')).append( $compile(p)($scope) )
			}else if(response.data.errorCode==2){
				var p = "<li class='dropdown' ><a href='javascript:void(0)' class='dropdown-toggle' data-toggle='dropdown' aria-expanded='true'> Company &nbsp;&nbsp;" +
					    "<b class='caret'></b></a><ul class='dropdown-menu'><li><a href='#/company/add' class='dropdown-toggle' data-toggle='dropdown' aria-expanded='true'>Add</a></li>" +
						"<li><a href='#/company/details' class='dropdown-toggle' data-toggle='dropdown' aria-expanded='true'>View</a></li>" +
						"</ul></li>" +
					    "<li class='dropdown' ><a href='javascript:void(0)' class='dropdown-toggle' data-toggle='dropdown' aria-expanded='true'> User &nbsp;&nbsp;" +
					    "<b class='caret'></b></a><ul class='dropdown-menu'><li><a href='#/user/add' class='dropdown-toggle' data-toggle='dropdown' aria-expanded='true'>Add</a></li>" +
                        "<li><a href='#/user/list' class='dropdown-toggle' data-toggle='dropdown' aria-expanded='true'>View</a></li>"+
                        "</ul></li>" +
						"<li class='dropdown' ><a href='javascript:void(0)' class='dropdown-toggle' data-toggle='dropdown' aria-expanded='true'> Enquiry &nbsp;&nbsp;" +
						"<b class='caret'></b></a><ul class='dropdown-menu'><li><a href='#/enquiry/add' class='dropdown-toggle' data-toggle='dropdown' aria-expanded='true'>Add</a></li>" +
						"<li><a href='#/enquiry/details' class='dropdown-toggle' data-toggle='dropdown' aria-expanded='true'>View</a></li>" +
						"</ul></li>" ;
                //$("#main_menu").append(p);
                angular.element(document.getElementById('main_menu')).append( $compile(p)($scope) )
			}
			else
				dataFactory.errorAlert("Check your connection");
		});
	}
    $scope.subMenuActive = function (path) {
        return (("#"+$location.path().substr(0, path.length)).toLowerCase() === path.toLowerCase()) ? 'active' : '';
    }
    $scope.mainMenuActive = function (path) {
		var urlArr=$location.path().split("/");
        return (urlArr[1].toLowerCase() === path.toLowerCase()) ? 'active' : '';
    }
}]);