'use strict';
/* Controllers */
angular.module('app', ['ngSanitize', 'gridster'])
    .controller('publicViewCopyController', ['$scope', '$sce', 'dataFactory', '$filter', '$stateParams', '$location', '$window', '$rootScope','$cookieStore',
        function ($scope, $sce, dataFactory, $filter, $stateParams, $location, $window, $rootScope,$cookieStore) {
            // Select Box start
            //--- Top Menu Bar
            //--- Top Menu Bar
            $(".loadingBar").show();
            $("#chartjs-tooltip").show();
            //End Menu
            //Default Tab
            $scope.tab=false;
            //End
            var auth_key = $stateParams.auth_key;
            // Select Box start
            $scope.trustAsHtml = function(value) {
                return $sce.trustAsHtml(value);
            };
            var data={};
            var addClass="";
            var viewClass="";
            if($location.path()=='/dashboard/'){
                viewClass='activeli';
            }else{
                addClass='activeli';
            }
            data['navTitle']="SHARED VIEW";
            data['icon']="fa fa-line-chart";
            data['navigation']=[
                {
                    "name":"Add",
                    "url":"#/dashboard/add",
                    "class":addClass
                },
                {
                    "name":"View",
                    "url":"#/dashboard/",
                    "class":viewClass
                },
                {
                    "name":"Close",
                    "url":"#/dashboard/",
                    "class":viewClass
                }
            ];
            $rootScope.$broadcast('subNavBar', data);
            var vm = $scope;
            vm.selectedChart = {};
            vm.customGroup = [];
            vm.activeXLabel = {};
            vm.activeYLabel = {};
            vm.aggregateText = {};
            vm.showInfo2 = false;
            vm.showInfo1 = true;
            vm.conatinerFilter=[];
            vm.conatinerFilterTo=[];
            sketch=sketch.reset();
            sketchServer=sketchServer.reset();
            $scope.columnSearch = "";
            $(".full-height").addClass("full-height-background");
            // vm.AggregateFunctions=[];
            vm.tableColumns = [];
            $scope.dashName = {};
            $scope.lineBar = {};
            $scope.lastDrawnChart = {};
            $scope.selectedChartInView = {};
            var userId=$cookieStore.get('userId');
            vm.filteredData = [];
            //Api call's for dashboard list
            dataFactory.request($rootScope.DashboardSharedViewList_Url,'post',"").then(function(response) {
                if(response.data.errorCode==1){
                    var result=response.data.result;
                    vm.dashboardList=[];
                    Object.keys(result).forEach(function(key){
                        result[key].forEach(function(d){
                            vm.dashboardList.push(d);
                        })
                    });
                }else{
                    dataFactory.errorAlert(response.data.message);
                }
            }).then(function(){
                setTimeout(function(){
                    vm.dashboardSelectId=vm.dashboardId;
                    $scope.$apply();
                },100);
            });
            // advanced chart settings
            vm.advancedChartSettingModal = {
                dateFormats: sketch._dateFormats.formats,
                groups: sketch._dateFormats.groups,
                Categories: [{}]
            }

//            Rename filter
            vm.reNameFilter=function(filter,index) {
                vm.filterName={};
                $('#renameFilter').modal({
                    backdrop: 'static',
                    keyboard: false
                });
                setTimeout(function(){
                    vm.filterName.name=filter.reName;
                    vm.reNameFilterIndex=index;
                    $scope.$apply();
                },1000);

            }
            // change filter name
            vm.changeFilterName=function(filterName,index) {
                vm.conatinerFilter[vm.reNameFilterIndex].reName=filterName;
                $('#renameFilter').modal('hide');
            }


            // delete Filter

            vm.deleteFilter=function(filter,index) {
                vm.conatinerFilter.splice(index,1)
            }

            vm.saveText=function(){
                var textareaValue = $('#summernote').summernote('code');
                $('#dashboardListView').modal('hide');
                var reportIndex;
                vm.DashboardModel.Dashboard.Report.forEach(function(d,index){
                    if(d.reportContainer.id==vm.DashboardModel.Dashboard.activeReport.reportContainer.id){
                        reportIndex=index;
                    }
                });
                var chartObject={};
                chartObject['attributes']="";
                chartObject['id']=0;
                chartObject['image']=0;
                chartObject['key']="_text";
                chartObject['name']="Text";
                chartObject['required']="Text";
                chartObject['value']="";
                vm.DashboardModel.Dashboard.Report[reportIndex].chart=chartObject;
                vm.DashboardModel.Dashboard.Report[reportIndex].chart.value=textareaValue;
                setTimeout(function(){
                    $("#chart-"+vm.DashboardModel.Dashboard.activeReport.reportContainer.id).html(textareaValue);
                },500);
            }

            vm.advancedChartSettingController = {
                init: function () {
                    vm.advancedChartSettingView.init();
                    vm.advancedChartSettingView.render();
                },
                fetchDateFormats: function () {
                    return vm.advancedChartSettingModal.dateFormats;
                },
                fetchGroupFormats: function () {
                    return vm.advancedChartSettingModal.groups;
                },
                getDimensionalData: function () {
                    // sketch._
                },
                addCategoryContainer: function () {
                    vm.categoryContainers.push({});
                },
                updateTableData: function (categoryData) {
                    var keys = Object.keys(categoryData.selectedItems);
                    var obj = {};
                    var column = categoryData.column;
                    keys.forEach(function (index) {
                        categoryData.selectedItems[index].forEach(function (d) {
                            obj[d] = categoryData.categoryName[index];
                        })
                    });
                    var newColumnName = categoryData.Name;
                    var updatedData = null;
                    vm.tableData.forEach(function (d) {
                        if (obj[d[column]]) {
                            d[newColumnName] = obj[d[column]];
                        } else {
                            d[newColumnName] = d[column];
                        }
                    });
                }
            }
            vm.advancedChartSettingView = {
                init: function () {
                    vm.categories = {};
                    vm.categoryContainers = [];
                    vm.addCategoryContainer = vm.advancedChartSettingController.addCategoryContainer;
                    vm.saveCategories = vm.advancedChartSettingController.saveCategories;
                    vm.removeCategory = vm.advancedChartSettingController.removeCategories;
                },
                render: function () {
                    vm.dateFormatList = vm.advancedChartSettingController.fetchDateFormats();
                    vm.dateGroupList = vm.advancedChartSettingController.fetchGroupFormats();
                }
            }
            vm.advancedChartSettingController.init();
            // .................................................................................................
            vm.leftSideModel = {
                chartTypeData: sketch.chartData,
                aggregateData: sketch.AggregatesObject
            };
            vm.leftSideController = {
                init: function () {
                    //Number widget setting
                    vm.numberStyle={};
                    // Comapny Setting
                    vm.companySet={};
                    vm.company={};
                    vm.company.logoWidth="50px";
                    vm.companyHeaderSet = function(){
                        if(vm.company && vm.company.name)
                            vm.companySet.name=vm.company.name;
                        if(vm.company && vm.company.description)
                            vm.companySet.description=vm.company.description;
                        if(vm.company && vm.company.log)
                            vm.companySet.log=vm.company.log;
                        if(vm.company && vm.company.logoWidth!="50px")
                            vm.companySet.logoWidth="50px";
                        $('#companyHeaderSet').modal('show');
                    }
                    vm.companyHeaderSave=function(company){
                        if(company && company.name)
                            vm.company.name=company.name;
                        if(company && company.description)
                            vm.company.description=company.description;
                        if(company && company.logoWidth)
                            vm.company.logoWidth=company.logoWidth;
                        if(company && company.logo)
                            vm.company.log=company.logo;
                        $('#companyHeaderSet').modal('hide');
                    }
                    //Reset Chart
                    vm.resetChart=function(){
                        eChartServer.resetAll();
                        eChart.resetAll();
                        dataFactory.successAlert("Chart reset successfully");
                    }
                    vm.leftSideView.render();
                    //Define Chart setting
                    vm.chartSettingObject = {};
                    //creating pivot table
                    //.....................
                    vm.chartErrorText = {};
                    vm.showInfo = {};
                    vm.showAggregate = {};
                    vm.periodVal = [];
                    var i = 1;
                    //Chart Setting Model
                    vm.selectedGroup = {};
                    var custIndex = 0;
                    vm.subCategory = {};
                    vm.headerGroup = function () {
                        if (vm.tableHeader.length) {
                            $(".ms-selected").hide();
                            if (vm.subCategory[custIndex] == undefined) {
                                vm.subCategory[custIndex] = 'group'+ custIndex;
                            }
                            vm.selectedGroup['group' + custIndex] = vm.tableHeader;
                            vm.tableHeader=[];
                            custIndex++;
                        } else {
                            alert("select any one");
                        }
                    }

                    vm.chartSetting = function (chartType) {
                        vm.Attributes.tableColumnOrder=[];
                        $.each(vm.Attributes.checkboxModelMeasure,function(key,value){
                            vm.Attributes.tableColumnOrder.push(value);
                        });
                        $.each(vm.Attributes.checkboxModelDimension,function(key,value){
                            vm.Attributes.tableColumnOrder.push(value);
                        });
                        vm.tableSettingCheckbox=angular.copy(vm.Attributes.checkboxModelMeasure);
                        vm.chartSettingObject = {};
                        if(vm.Attributes.dataFormate){
                            vm.chartSettingObject.xAxisDataFormate=vm.Attributes.dataFormate.xAxis;
                            vm.chartSettingObject.yAxisDataFormate=vm.Attributes.dataFormate.yAxis;
                            vm.chartSettingObject.toolTipFormate=vm.Attributes.dataFormate.tooltip;
                            vm.chartSettingObject.groupColor=vm.Attributes.dataFormate.groupColor;
                            vm.chartSettingObject.fontSize=vm.Attributes.dataFormate.fontSize;
                        }else{
                            vm.settingChartType=chartType;
                            vm.chartSettingObject.toolTipFormate = "{xLabel}:{yLabel}"; //tooltip Formate
                            vm.chartSettingObject.fontSize = 13; //Default font size
                        }
                        vm.numberStyle={};
                        if(vm.Attributes.numberStyle){
                            vm.numberStyle.fontSize=vm.Attributes.numberStyle.fontSize;
                            vm.numberStyle.fontColor=vm.Attributes.numberStyle.fontColor;
                        }else{
                            vm.numberStyle.fontSize=13;
                            vm.numberStyle.fontColor="#757575";
                        }
                        $('#chartSetting').modal({
                            backdrop: 'static',
                            keyboard: false
                        });
                        setTimeout(function(){
                            $(".sortable").sortable({
                                cancel: ".fixed",
                                stop : function(event, ui){
                                    vm.tableColumnOrder=$(this).sortable("toArray");
                                }
                            });
                        },100);
                        $('#chartSetting').modal('show');
                        vm.tableHeader=[];
                        setTimeout(function(){
                            $('#header-group').multiSelect({
                                afterSelect: function (values) {
                                    vm.tableHeader.push(values[0]);
                                },
                                afterDeselect: function (values) {
                                    var index = $scope.tableHeader.indexOf(values[0]);
                                    if (index != -1) {
                                        $scope.tableHeader.splice(index, 1);
                                    }
                                }
                            });
                        },1);
                        $('#chartSetting').modal({
                            backdrop: 'static',
                            keyboard: false
                        });
                        $('#chartSetting').modal('show');
                    }
                    vm.saveDataFormate = function (chartSetting) {
                        var dimensionType;
                        Object.keys(vm.Attributes.checkboxModelDimension).forEach(function (d) {
                            dimensionType = vm.Attributes.checkboxModelDimension[d].columType
                        });
                        vm.Attributes['dataFormate'] = {};
                        vm.Attributes['dataFormate']['xAxis'] = chartSetting.xAxisDataFormate;
                        vm.Attributes['dataFormate']['yAxis'] = chartSetting.yAxisDataFormate;
                        vm.Attributes['dataFormate']['tooltip'] = chartSetting.toolTipFormate;
                        vm.Attributes['dataFormate']['fontSize'] = chartSetting.fontSize;
                        vm.Attributes['dataFormate']['groupColor'] = chartSetting.groupColor;
                        vm.Attributes['dataFormate']['fontFamily'] = chartSetting.fontFamily;
                        var activeData = {};
                        if (!$.isEmptyObject(vm.filteredData)) {
                            activeData = vm.filteredData;
                        }
                        else {
                            activeData = vm.tableData;
                        }
                        sketch
                            .axisConfig(vm.Attributes)
                            .data(activeData)
                            .container(vm.DashboardModel.Dashboard.activeReport.reportContainer.id)
                            .chartConfig($scope.selectedChartInView[vm.DashboardModel.Dashboard.activeReport.reportContainer.id])
                            .render(function (drawn) {
                                $scope.lastDrawnChart[vm.DashboardModel.Dashboard.activeReport.reportContainer.id] = vm.DashboardModel.Dashboard.activeReport.chart.key;
                            });
                        $('#chartSetting').modal('hide');
                    }

                    $scope.modal = {};
                    $scope.modal.slideUp = "default";
                    $scope.modal.stickUp = "default";
                    $scope.addChartIconBorder = function (chart) {
                        $(".custBorderHover").removeClass('activeChart');
                        $("#" + chart.key).addClass('activeChart');
                    }
                },
                getCharts: function () {
                    return vm.leftSideModel.chartTypeData;
                },
                getAggregates: function () {
                    return vm.leftSideModel.aggregateData;
                },

                senseDefaultChart: function () {
                    if (Object.keys(vm.Attributes.checkboxModelDimension).length == 1 && Object.keys(vm.Attributes.checkboxModelMeasure).length == 1) {
                        $scope.addChartIconBorder(sketch.chartData[0]);
                        return sketch.chartData[0];
                    } else if (Object.keys(vm.Attributes.checkboxModelDimension) == 1 && Object.key(vm.Attributes.checkboxModelMeasure).length == 2) {
                        return sketch.chartData[5];
                    } else {
                        return false;
                    }
                },
                requiredAttributesFound: function () {
                    if (vm.Attributes.checkboxModelDimension && vm.Attributes.checkboxModelMeasure)
                        if (Object.keys(vm.Attributes.checkboxModelDimension).length > 0 && Object.keys(vm.Attributes.checkboxModelMeasure).length > 0) {
                            return true;
                        }
                    return false;
                },
                showChartErrorText: function (chart) {
                    try {
                        if (chart != null) {
                            vm.blnkRep = true;
                            //	vm.showInfovm.DashboardModel.Dashboard.activeReport.reportContainer.id=true;
                            $("#chart-" + vm.DashboardModel.Dashboard.activeReport.reportContainer.id).html("");
                            vm.chartErrorText[vm.DashboardModel.Dashboard.activeReport.reportContainer.id] = "You need to select max " + chart.required.Dimension + " dimension" + " and min " + chart.required.Measure[0] + " measure to draw " + chart.name;
                        } else {
                            vm.blnkRep = true;
                            vm.chartErrorText[vm.DashboardModel.Dashboard.activeReport.reportContainer.id] = "You need to select atleast 1 dimension and 1 measure to draw any chart";
                        }
                    } catch (e) {
                        vm.chartErrorText[vm.DashboardModel.Dashboard.activeReport.reportContainer.id] = "Some Error Occured....";
                    }
                },
                checkChartCanBeDrawn: function (chart) {
                    if (chart != null)
                        if (chart.name == "DataTable")
                            return true;
                    if (vm.Attributes.checkboxModelDimension && vm.Attributes.checkboxModelMeasure) {
                        if ((Object.keys(vm.Attributes.checkboxModelDimension).length == chart.required.Dimension) && ((chart.required.Measure[0] <= Object.keys(vm.Attributes.checkboxModelMeasure).length) && (chart.required.Measure[1] >= Object.keys(vm.Attributes.checkboxModelMeasure).length))) {
                            vm.showInfo[vm.DashboardModel.Dashboard.activeReport.reportContainer.id] = false;
                            return true;
                        }
                    }
                    vm.showInfo1 = true;
                    vm.showInfo2 = true;
                    vm.showInfo[vm.DashboardModel.Dashboard.activeReport.reportContainer.id] = true;
                    return false;
                },
                updateAttributesObject: function (attr, type, chartType) {
                    if (chartType == "_dataTable") {
                        return true;
                    }
                    if (type == "dimension")
                        if ($scope.Attributes.checkboxModelDimension[attr.columnName] == 0) {
                            delete $scope.Attributes.checkboxModelDimension[attr.columnName];
                            return false;
                        }
                    if (type == "measure")
                        if ($scope.Attributes.checkboxModelMeasure[attr.columnName] == 0) {
                            delete $scope.Attributes.checkboxModelMeasure[attr.columnName];
                            return false;
                        }
                    if (type == "chart" && !vm.leftSideController.checkChartCanBeDrawn(attr)) {
                        vm.leftSideController.showChartErrorText(attr);
                        return false;
                    }
                    return true;
                },
                addNewContainer: function (chartObj) {
                    vm.Attributes = {aggregateModel: {}};
                    if(chartObj){
                        sketch.chartData[0]['key']=chartObj.key;
                        sketch.chartData[0]['name']=chartObj.name;
                    }

                    vm.DashboardController.addReportContainer(sketch.chartData[0]);
                    vm.DashboardModel.Dashboard.activeReport.chart = sketch.chartData[0];
                    $scope.selectedChartInView[vm.DashboardModel.Dashboard.activeReport.reportContainer.id] = null;
                    $(".custBorderHover").removeClass('activeChart');
                    // $("#" + index).addClass('activeChart');
                },
                setDefaultAggregate:function(measure){

                    //Initializing Aggregate Model In Case Not Initialized
                    if(!vm.Attributes.aggregateModel)
                        vm.Attributes.aggregateModel={};

                    vm.Attributes.aggregateModel[measure.columnName] = {
                        "key": "sumIndex",
                        "value": "Sum",
                        "type": "Aggregate"
                    };

                },
                setChartInView:function(chart){
                    $scope.selectedChartInView[vm.DashboardModel.Dashboard.activeReport.reportContainer.id]=chart;
                },
                getSelectedChartFromView:function(){
                    return $scope.selectedChartInView[vm.DashboardModel.Dashboard.activeReport.reportContainer.id];
                },
                checkAndDrawChart:function(chart){
                    var isChartBeDrawn = sketch.axisConfig(vm.Attributes).chartConfig(chart)._isAllFieldsPassed();
                    if(isChartBeDrawn){
                        vm.showInfo[vm.DashboardModel.Dashboard.activeReport.reportContainer.id] = false;
                        vm.leftSideController.showProgressBar(true);
                        vm.DashboardView.renderChartInActiveContainer();
                        vm.DashboardModel.Dashboard.activeReport.chart = chart;
                    }
                    else {
                        vm.leftSideController.showChartErrorText(vm.DashboardModel.Dashboard.activeReport.chart);
                        vm.showInfo[vm.DashboardModel.Dashboard.activeReport.reportContainer.id] = true;
                        $(".label_legends" + vm.DashboardModel.Dashboard.activeReport.reportContainer.id).hide();
                        $("#left" + vm.DashboardModel.Dashboard.activeReport.reportContainer.id).html("");
                    }
                },
                switchChartContainer:function(attr,callback){
                    var activeReportContainerId = vm.DashboardModel.Dashboard.activeReport.reportContainer.id;
                    var activeContainer = $scope.dashboardPrototype.reportContainers[$scope.findIndexOfReportContainer(activeReportContainerId)];
                    if(attr.key!="_numberWidget") {
                        if ($scope.lastDrawnChart[vm.DashboardModel.Dashboard.activeReport.reportContainer.id] == "_numberWidget") {
                            var obj = vm.DashboardController.getCommonContainerSettings();
                            $.each(obj,function (key,value) {
                                activeContainer[key] = value;
                            });
                        }
                    }
                    else{
                        var obj = vm.DashboardController.getNumberContainerSettings();
                        $.each(obj, function (key, value) {
                            activeContainer[key] = value;
                        });
                    }
                    vm.DashboardModel.Dashboard.activeReport.reportContainer = activeContainer;
                    var indexOfReport = $scope.findIndexOfReportFromReportContainer(activeReportContainerId);
                    vm.DashboardModel.Dashboard.Report[indexOfReport] = activeContainer;
                    setTimeout(function(){
                        callback();
                    },1000);
                },
                showProgressBar:function(state){
                    var variable = "reportLoading_" + vm.DashboardModel.Dashboard.activeReport.reportContainer.id;
                    vm[variable] = state;
                },
                onAttributeSelect: function (attr, type) {
                    //Updating Attribute Object According to selections
                    vm.leftSideController.updateAttributesObject(attr,type);
                    if (type === "measure") {
                        //Sets default aggregate for each measure
                        vm.leftSideController.setDefaultAggregate(attr);
                    }

                    if(type==="dimension" || type==="measure" || type==="aggregate"){
                        var activeChart=vm.leftSideController.getSelectedChartFromView();
                        //check if any chart selected in view
                        if(!activeChart){
                            vm.leftSideController.setChartInView(vm.DashboardModel.Dashboard.activeReport.chart);
                        }else{
                            vm.leftSideController.checkAndDrawChart(activeChart);
                        }
                    }

                    if(type==="chart"){
                        vm.leftSideController.switchChartContainer(attr,function () {
                            if(attr.key=="_pivotTable" && vm.DashboardModel.Dashboard.Report[vm.DashboardModel.Dashboard.activeReportIndex].chartObject.config){
                                vm.DashboardModel.Dashboard.Report[vm.DashboardModel.Dashboard.activeReportIndex].chartObject['recall']=true;
                                sketch.pivotTableConfig=Object.assign({},vm.DashboardModel.Dashboard.Report[vm.DashboardModel.Dashboard.activeReportIndex].chartObject);
                            }else if(attr.key=="_pivotTable"){
                                sketch.pivotTableConfig=undefined;
                            }
                            vm.leftSideController.setChartInView(attr);
                            vm.leftSideController.checkAndDrawChart(attr);
                        });
                    }
                }
            }


            vm.leftSideView = {
                render: function () {
                    vm.Attributes = {};
                    vm.chartDraw = vm.leftSideController.onAttributeSelect;
                    // vm.chartSelect=vm.leftSideController.updateAttribute;
                    $scope.charts = vm.leftSideController
                        .getCharts();
                    $scope.aggregates = vm.leftSideController
                        .getAggregates();
                    $scope.addReport = vm.leftSideController.addNewContainer;
                }
            };

            vm.leftSideController.init();

            // ..................Right side
            // .....................................................

            vm.rightSideModel = {
                axisObject: sketch.axisData,
                AggregateObject: sketch.AggregatesObject,
                axisData: sketch.attributesData,
                activeAxisConfig: null
            }
            vm.rightSideController = {
                init: function () {
                    vm.rightSideView.init();
                    vm.checkboxModel = {};
                    vm.checkboxMD = {};
                    vm.filtersToApply = [];
                    vm.filtersTo = [];
                    var newDim = [];
                    vm.styleSettings = {};
                    vm.columnAddValue = function (type, value) {
                        var tableColumnIndex = vm.rightSideController
                            .findIndexofActiveContainer(
                                vm.tableColumns,
                                "value", value);
                        vm.tableColumns[tableColumnIndex].type = [];
                        vm.tableColumns[tableColumnIndex].type
                            .push(type);
                        // $('.advChartSettingsModal').drawer('close');
                        generate("success","Move To Column Successfully");
                    }
                    vm.chartStyle = function (reportContainer) {
                        vm.styleSettings = {};
                        vm.DashboardController
                            .onReportContainerClick(reportContainer.id);
                        if (vm.DashboardModel.Dashboard.Report[vm.DashboardModel.Dashboard.activeReportIndex].chart.styleSettings) {
                            vm.styleSettings = vm.DashboardModel.Dashboard.Report[vm.DashboardModel.Dashboard.activeReportIndex].chart.styleSettings;
                            $('.chartStyle').drawer('open');
                        } else if (vm.DashboardModel.Dashboard.Report[vm.DashboardModel.Dashboard.activeReportIndex].chartObject != undefined) {
                            vm.cWidthshow = false;
                            vm.styleSettings.rotateLabel = 0;
                            vm.styleSettings.rotateChart = 0;
                            vm.styleSettings.resizable = "false";
                            vm.styleSettings.translateX = 0;
                            vm.styleSettings.translateY = 0;
                            var index = reportContainer.id - 1;
                            var chart = vm.DashboardModel.Dashboard.Report[vm.DashboardModel.Dashboard.activeReportIndex].chart;
                            var chartObj = vm.DashboardModel.Dashboard.Report[vm.DashboardModel.Dashboard.activeReportIndex].chartObject;
                            // Push Value to input fields
                            vm.styleSettings.chartColor = "#1f77b4";
                            if (typeof chartObj !== 'undefined'
                                && chartObj.margins) {
                                vm.styleSettings.xAxisMargin = chartObj
                                    .margins().bottom;
                                vm.styleSettings.yAxisMargin = chartObj
                                    .margins().left;
                            }
                            vm.styleSettings.cHeight = $(
                                "#chart-"
                                + reportContainer.id)
                                .height();
                            vm.styleSettings.cWidth = $(
                                "#chart-"
                                + reportContainer.id)
                                .width();
                            vm.styleSettings.xAxisLabelName = $(
                                "#chart-"
                                + reportContainer.id)
                                .find(".x-axis-label")
                                .text();
                            vm.styleSettings.yAxisLabelName = $(
                                "#chart-"
                                + reportContainer.id)
                                .find(".y-axis-label")
                                .text();
                            vm.DashboardModel.Dashboard.Report[vm.DashboardModel.Dashboard.activeReportIndex].chart.styleSettings = vm.styleSettings;

                            $('.chartStyle').drawer('open');
                            var processedData = [];
                            vm.rightSideModel.axisData
                                .forEach(function (obj) {
                                    chart.attributes
                                        .forEach(function (chartType) {
                                            if (chartType.indexOf(obj.Name) != -1) {
                                                // chartSettingsObj[obj.modelName]='{"key":"","value":"","type":[""]}';
                                                obj.visible = true;
                                                processedData.push(obj);
                                            }
                                        });
                                });
                            vm.processStyle = processedData;
                        }
                    }
                },
                getMeasures: function () {
                    var tempArray = [];

                    vm.tableColumns.forEach(function (obj) {

                        if (obj.type == "Measure")
                            tempArray.push(obj);
                        if (obj.type == "MeasureCustom")
                            tempArray.push(obj);

                    });
                    return tempArray;
                },

                setActiveAxisConfig: function (config) {
                    vm.rightSideModel.activeAxisConfig = config;
                },
                getActiveAxisConfig: function () {
                    return vm.rightSideModel.activeAxisConfig;
                },
                getDimensions: function () {
                    var tempArray = [];
                    var pushSkip = 0;
                    vm.tableColumns.forEach(function (obj) {
                        if (obj.type == "Dimension")
                            tempArray.push(obj);
                        if (obj.type == "DimensionCustom")
                            tempArray.push(obj);
                    });
                    return tempArray;
                },
                columnDataPush: function (obj) {
                    if (obj.columnType == "Measure") {
                        obj.columnData = vm.rightSideController.getMeasures();
                        return obj;
                    } else if (obj.columnType == "Dimension") {
                        obj.columnData = vm.rightSideController.getDimensions();
                        return obj;
                    } else if (obj.columnType == "Aggregate") {
                        obj.columnData = vm.rightSideModel.AggregateObject;
                        return obj;
                    }
                },

                getAxisRenderObj: function (selected, type) {
                    if (type == "chart") {

                    }
                    var chartSettingsObj = {};
                    var dimensionArray = [];
                    chartSettingsObj['dimension'] = vm.checkboxModelDimension;
                    chartSettingsObj['measure'] = vm.checkboxModelMeasure;
                    chartSettingsObj['aggregate'] = [{
                        "key": "sumIndex",
                        "value": "Sum",
                        "type": "Aggregate"
                    }];
                    var processedData = [];
                    vm.chartSettings = chartSettingsObj;
                    vm.DashboardModel.Dashboard.activeReport.chart = selected;
                    vm.rightSideController.onAxisValueChange();
                    // return finalData;

                    /*
                     * vm.rightSideModel.axisObject.forEach(function(obj) {
                     *
                     * chartTypeObj.requiredAxis.forEach(function(chartType) {
                     *
                     * if (chartType.indexOf(obj.Name) != -1) { //
                     * chartSettingsObj[obj.modelName]='{"key":"","value":"","type":[""]}';
                     * obj.visible = true;
                     * processedData.push(obj); } }
                     *
                     * chartTypeObj.requiredAxis.forEach(function(chartType) {
                     * if(obj.Name==chartType){ //
                     * chartSettingsObj[obj.modelName]='{"key":"","value":"","type":[""]}';
                     * obj.visible=true;
                     * processedData.push(obj); } } );
                     *
                     * });
                     */

                    /*
                     * var finalData = [];
                     * processedData.forEach(function(obj) {
                     * finalData.push(vm.rightSideController.columnDataPush(obj)); //
                     * vm.chartSettings();
                     *
                     * });
                     */

                },

                findIndexofActiveContainer: function (arraytosearch, key, valuetosearch) {
                    for (var i = 0; i < arraytosearch.length; i++) {
                        if (arraytosearch[i][key] == valuetosearch) {
                            return i;
                        }
                    }
                    return null;
                },

                onAxisValueChange: function () {

                    /*
                     * if
                     * (JSON.parse(vm.chartSettings["Xaxis"]).key ==
                     * "DATETIME")
                     * $("#dateFormatChangeModal").modal();
                     */

                    var drawn = sketch
                        .axisConfig(vm.chartSettings)
                        .data(vm.tableData)
                        .container(vm.DashboardModel.Dashboard.activeReport.reportContainer.id)
                        .chartConfig(vm.DashboardModel.Dashboard.activeReport.chart)
                        .render();

                    var index = vm.rightSideController
                        .findIndexofActiveContainer(
                            $scope.dashboardPrototype.reportContainers,
                            "id",
                            vm.DashboardModel.Dashboard.activeReport.reportContainer.id);

                    if (drawn) {
                        $scope.filterSave = true;
                        $scope.saveState = 1;
                        $scope.dashboardPrototype.reportContainers[index]['image'] = null;
                    }

                    vm.rightSideController.setActiveAxisConfig(vm.chartSettings);
                    var indexOfReport = vm.DashboardController.findIndexofActiveContainer(vm.DashboardModel.Dashboard.Report, "id", vm.DashboardModel.Dashboard.activeReport.reportContainer.id);
                    vm.DashboardModel.Dashboard.Report[indexOfReport].axisConfig = new Object(vm.chartSettings);
                    if (drawn) {
                        drawn["chartTypeAttribute"] = vm.DashboardModel.Dashboard.activeReport.chart.name;
                        drawn["resize"] = "ON";
                        vm.DashboardModel.Dashboard.Report[indexOfReport]["chartObject"] = drawn;
                    }
                    vm.DashboardModel.Dashboard.activeReport = vm.DashboardModel.Dashboard.Report[indexOfReport];
                    vm.DashboardModel.Dashboard.activeReportIndex = indexOfReport;

                }
            }

            vm.rightSideView = {
                // this part can be improved
                init: function () {
                    // vm.chartDraw=vm.rightSideController.getAxisRenderObj;
                    vm.refreshChartContainer = vm.rightSideController.onAxisValueChange;
                    vm.columnTypeObj = {};
                    $scope.addColumnTypeRightSide = function (columnObj) {
                        vm.hiddenAxis = columnObj.Name;
                        $("#myModal").modal();
                        if (columnObj.columnType.indexOf("Dimension") != -1) {
                            vm.columnTypeObj = vm.rightSideController.getMeasures();
                        } else if (columnObj.columnType.indexOf("Measure") != -1) {
                            vm.columnTypeObj = vm.rightSideController.getDimensions();
                        }
                    }
                    // Calculation part
                    $scope.measureSetting = function (columnObj) {
                        $("#measureModel").modal();
                        columnObj.columnType = "Dimension";
                        if (columnObj.columnType == "Dimension") {
                            vm.columnTypeObj = vm.rightSideController.getMeasures();
                        } else if (columnObj.columnType == "Measure") {
                            vm.columnTypeObj = vm.rightSideController.getDimensions();
                        }
                    }
                    $scope.calMeasureShow = [];
                    $scope.calMeasureDb = [];
                    $scope.measureAdd = function () {
                        // Formula in array

                        $scope.measureName = "";
                        $scope.measureFormula = "";
                    }
                    $scope.measureFormula = "";
                    $scope.insertText = function (elemID, text) {
                        $scope.measureFormula += text;

                        // $scope.measureVal.measureFormula
                        // +=text
                    }
                    // Close Style Type Drawer
                    $scope.styleTypeDrawerClose = function () {
                        $('.chartStyle').drawer('close');
                    }

                    vm.calMeasureArrayForCompare = [];
                    // Measure Add Calculation
                    vm.customMeasure = [];
                    vm.measureAddCal = function () {
                        // sketch.setGroupExpression($scope.expression);
                        var expression = $scope.measureFormula;
                        vm.FormulaArray = expression
                            .split(/([\+\-\*\/])/);
                        vm.expression = expression.replace(
                            /\b[a-z]\w*/ig, "v['$&']")
                            .replace(/[\(|\|\.)]/g, "");
                        vm.calMeasureShow
                            .push({
                                'measureName': $scope.measureName,
                                'measureFormula': $scope.measureFormula
                            });
                        vm.calMeasureDb
                            .push({
                                'measureName': $scope.measureName,
                                'measureFormula': $scope.FormulaArray
                            });
                        vm.calMeasureShow
                            .forEach(function (d) {
                                if (vm.calMeasureArrayForCompare
                                        .indexOf(d.measureName) == -1) {
                                    var newExpression = d.measureFormula
                                        .replace(
                                            /\b[a-z]\w*/ig,
                                            "v['$&']")
                                        .replace(
                                            /[\(|\|\.)]/g,
                                            "");

                                    var testObject = {
                                        "key": "INT",
                                        "value": d.measureName,
                                        "type": ["MeasureCustom"],
                                        "formula": newExpression
                                    };
                                    vm.customMeasure
                                        .push(testObject);
                                    vm.tableColumns
                                        .push(testObject);
                                    $(
                                        '.advChartSettingsModal')
                                        .drawer('close');
                                    generate("success",
                                        "Measure created successfully");
                                } else {
                                    generate("error",
                                        "You Calculation column match to other column");
                                }
                            });
                    }
                    // Measure Validation
                    vm.measureAddValidation = function () {

                        /*
                         * var parts =
                         * $scope.measureFormula.split(/[[\]]{1,2}/);
                         * parts.length--; // the last entry is
                         * dummy, need to take it out var
                         * valExp=parts.join('');
                         */
                        var newExpression = $scope.measureFormula
                            .replace(/\b[a-z]\w*/ig,
                                "v['$&']").replace(
                                /[\(|\|\.)]/g, "");
                        var formula = "v['"
                            + vm.lastColumSelected + "']";
                        var testObject = {
                            "key": "INT",
                            "value": $scope.measureNameValidation,
                            "type": ["MeasureCustom"],
                            "validation": newExpression,
                            "formula": formula
                        };
                        vm.tableColumns.push(testObject);

                        $('.advChartSettingsModal').drawer(
                            'close');
                        generate("success",
                            "Measure created successfully");
                    }
                    $scope.addColumnType = function (columnObj) {
                        vm.hiddenAxis = columnObj.Name;

                        $("#myModal").modal();

                        if (columnObj.columnType == "Dimension") {
                            vm.columnTypeObj = vm.rightSideController
                                .getMeasures();
                        } else if (columnObj.columnType == "Measure") {
                            vm.columnTypeObj = vm.rightSideController
                                .getDimensions();
                        }
                    }
                    // End Calculation Part

                },
                render: function (chartTypeObj) {
                    // vm.chartSettings={};

                    this.data = vm.rightSideController
                        .getAxisRenderObj(chartTypeObj);
                    // vm.rightSideView.resetView(this.data);
                    // $(".select2").select2('remove');

                    vm.axisRenderArray = this.data;

                    setTimeout(function () {
                        $('select').select2();

                    }, 100);

                },
                chartSelectView: {
                    update: function (chartInfo) {
                        $scope.chartType = JSON.stringify(chartInfo);
                    }

                }

            }

            // vm.rightSideController.init();

            // .................................................End
            // Right Side
            // View...........................................................................

            // ...........................Dashboard
            // View...............................................................................
            var maxSizeY;
            vm.DashboardModel = {
                currentActiveGrid: null,
                dashboardPrototypeObject: {
                    id: '1',
                    name: 'Home',
                    reportContainers: []
                },
                reportPrototypeObject: {
                    reportContainer: null,
                    chart: null,
                    axisConfig: null
                },
                gridSettings: {
                    margins: [20, 15],
                    columns: 30,
                    pushing: true,
                    floating: true,
                    maxRows:10000,
                    swapping: true,
                    rowHeight: '30',
                    collision: {
                        on_overlap_start: function (collider_data) {

                        }
                    },
                    draggable: {
                        enabled: true, // whether dragging
                        // items is supported
                        handle: '.my-class',
                        start: function (event, $element, widget) {

                        },
                        resize: function (event, $element, widget) {

                        },
                        stop: function (event, $element, widget) {
                            /*$(".hiddenBorder").removeClass(
                             "box-border");*/
                        }
                    },
                    resizable: {
                        enabled: true,
                        handles: ['n', 'e', 's', 'w', 'ne',
                            'se', 'sw', 'nw'],
                        stop: function (event, uiWidget,$element) {
                            var getHeight = ($("#li_"+vm.DashboardModel.Dashboard.activeReport.reportContainer.id).height())-97;
                            var getWidth = $("#li_"+vm.DashboardModel.Dashboard.activeReport.reportContainer.id).width()-40;

                            if(vm.DashboardModel.Dashboard.activeReport.chart.key!="_numberWidget"){
//                                var getHeight = ($("#li_"+vm.DashboardModel.Dashboard.activeReport.reportContainer.id).height())-97;
//                                var getWidth = $("#li_"+vm.DashboardModel.Dashboard.activeReport.reportContainer.id).width()-40;

                                $("#chart-"+vm.DashboardModel.Dashboard.activeReport.reportContainer.id).height(getHeight);
                                $("#chart-"+vm.DashboardModel.Dashboard.activeReport.reportContainer.id).width(getWidth);
                                //document.getElementById("left"+vm.DashboardModel.Dashboard.activeReport.reportContainer.id).style.height = height;
                                var legendHeight=$("#li_"+vm.DashboardModel.Dashboard.activeReport.reportContainer.id).height();
                                $("#left"+vm.DashboardModel.Dashboard.activeReport.reportContainer.id).height(legendHeight*60/100);
                                //Chart Hide
                                var activeReport = vm.DashboardModel.Dashboard.Report[vm.DashboardModel.Dashboard.activeReportIndex];
                                $("#li_" + uiWidget[0].firstElementChild.id).addClass("li-box-border");
                                if(vm.DashboardModel.Dashboard.activeReport.chart.key!="_mapChartjs"){
                                    $("#mapbox-"+vm.DashboardModel.Dashboard.activeReport.reportContainer.id).height(getHeight);
                                    $("#mapbox-"+vm.DashboardModel.Dashboard.activeReport.reportContainer.id).width(getWidth);
                                }
                            }
                            $("#chart-" + vm.DashboardModel.Dashboard.activeReport.reportContainer.id).show();
                            var variable = "reportLoading_" + vm.DashboardModel.Dashboard.activeReport.reportContainer.id;
                            vm[variable] = false;
                            $scope.chartLoading = false;
                            $("#chart-" + $element.id).show();
                            setTimeout(function(){
                                var reportContainerId = 'chart-' + vm.DashboardModel.Dashboard.activeReport.reportContainer.id;
                                if(vm.dataCount<$rootScope.localDataLimit){
                                    eChart.resetChart(reportContainerId);
                                }else{
                                    eChartServer.resetChart(reportContainerId);
                                }
                            },300);                        },
                        start: function (event, uiWidget, $element) {
                            $("#chart-" + $element.id).hide();
                            //chart Loading Bar True
                            var variable = "reportLoading_" + vm.DashboardModel.Dashboard.activeReport.reportContainer.id;
                            vm[variable] = true;
                            //Chart Hide start
                            $(".label_legends" + vm.DashboardModel.Dashboard.activeReport.reportContainer.id).hide();
                            $("#chart-" + vm.DashboardModel.Dashboard.activeReport.reportContainer.id).hide();
                            $("#left" + vm.DashboardModel.Dashboard.activeReport.reportContainer.id).hide();
                        }
                    }
                },
                numberWidgetContainerSettings: {
                    id: 0,
                    name: "Number Widget",
                    sizeX: 9,
                    sizeY: 8,
                    minSizeX: 3,
                    minSizeY: 3,
                    maxSizeX: 9,
                    maxSizeY: 8
                },
                commonContainersSettings: {
                    id: 0,
                    name: "Report",
                    sizeX: 6,
                    sizeY: 8,
                    minSizeX: 6,
                    minSizeY: 8
                },
                Dashboard: {
                    activeReportIndex: null,
                    activeReport: null,
                    lastReportId: 0,
                    Report: []
                }
            };
            var report = [];
            vm.DashboardController = {
                init: function () {
                    vm.DashboardView.init();
                    vm.chartLoading = false;
                    vm.checkboxModel = {};
                    //
                    vm.dateRangeArray = [];
                    vm.dateModal = function () {
                        $('#dateModal').modal({
                            backdrop: 'static',
                            keyboard: false
                        });
                        $('#dateModal').modal('show');
                    }
                    var i = 0;
                    vm.filterApplyAdd = function (keys) {
                        vm.blnkFilter = false;
                        var filerTemp = [];
                        vm.filtersToApply = [];
                        vm.filterHSApply = true;
                        var columnObj = "";
                        vm.checkboxModel[keys.columnName] = keys;
                        Object.keys(vm.checkboxModel).forEach(function (d) {
                            if (vm.checkboxModel[d] != "0") {
                                vm.filtersTo[d] = [];
                                // columnObj = vm.checkboxModel[d];
                                try{
                                    var columnObj = JSON.parse(vm.checkboxModel[d]);
                                }catch (e){
                                    var columnObj = vm.checkboxModel[d];
                                }

                                var keyValue = {};
                                keyValue['key'] = columnObj.columnName;
                                keyValue['value'] = columnObj.columType;
                                filerTemp.push(keyValue);

                                if(vm.dataCount<$rootScope.localDataLimit){
                                    Enumerable.From(vm.tableData).Distinct(function (x) { return x[d]; })
                                        .Select(function (x) {
                                            return x[d]; })
                                        .ToArray()
                                        .forEach(
                                            function (e) {
                                                vm.filtersTo[d]
                                                    .push(e);
                                            });
                                }else{
                                    var data={columnObject:columnObj,metadataId:vm.metadataId,sessionId:$rootScope.accessToken};
                                    dataFactory.nodeRequest('getUniqueDataFieldsOfColumn','post',data).then(function(data){
                                        data.forEach(function(k){
                                            vm.filtersTo[d]
                                                .push(k);
                                        })
                                    });
                                }
                            }
                        });
                        if (filerTemp.length) {
                            $scope.onApplyFilter = true;
                            $("#dash-border").css("margin-top", "110px");
                        } else {
                            $scope.onApplyFilter = false;
                            $("#dash-border").css("margin-top", "60px");
                        }

                        vm.filtersToApply = filerTemp;
                        setTimeout(function () {
                            $('.multielect').multiselect(
                                {
                                    includeSelectAllOption: true,
                                    enableFiltering: true,
                                    maxHeight: 200
                                });
                        }, 1000);
                    }
                    vm.filter_add = function () {
                        $('.drawer1').drawer('open');
                    }
                    // Filter Hide Show
                    $scope.filterHideShow = function () {
                        $scope.filterHide = !$scope.filterHide;
                        $scope.filterShow = !$scope.filterShow;
                        $scope.filterHSApply = !$scope.filterHSApply;

                        if ($scope.filterShowHidden) {
                            $scope.filterShowHidden = 0
                        } else {
                            $scope.filterShowHidden = 1;
                        }
                    }
                },
                findIndexofActiveContainer: function (arraytosearch, key, valuetosearch) {

                    for (var i = 0; i < arraytosearch.length; i++) {

                        if (arraytosearch[i].reportContainer[key] == valuetosearch) {

                            return i;
                        }
                    }
                    return null;
                },
                getGridSettings: function () {
                    return vm.DashboardModel.gridSettings;
                },
                getCommonContainerSettings: function () {
                    return {
                        name: "Report",
                        sizeX: 20,
                        sizeY: 15,
                        minSizeX: 3,
                        minSizeY: 3
                        /*maxSizeX: 12,
                         maxSizeY: 6*/
                    };
                },
                getTextContainerSettings:function(){
                    return {
                        name: "Text",
                        sizeX: 8,
                        sizeY: 4,
                        minSizeX: 2,
                        minSizeY: 2,
                        /* maxSizeX: 30,
                         maxSizeY: 15,*/
                        chartType: "Text"
                    };
                },
                getNumberContainerSettings: function () {
                    return {
                        name: "Number Widget",
                        sizeX: 8,
                        sizeY: 4,
                        minSizeX: 1,
                        minSizeY: 1,
                        /* maxSizeX: 16,
                         maxSizeY: 8,*/
                        chartType: "Number Widget"
                    };
                },
                onChartDropToContainer: function (event, index, item, external, type, allowedType) {
                    var view = vm.DashboardView;
                    dc.filterAll();
                    vm.isParameterShown = true;
                    $('.drawer').drawer('open');
                    view.removeGridBorder();
                    vm.rightSideView.render(item);
                    //vm.DashboardController.addReportContainer(item);
                    vm.rightSideView.chartSelectView.update(item);
                    vm.dimesionColumn = vm.rightSideController.getDimensions();
                },

                onReportContainerClick: function (id) {
                    var index = vm.DashboardController.findIndexofActiveContainer(vm.DashboardModel.Dashboard.Report,"id", id);
                    vm.DashboardModel.Dashboard.activeReportIndex=index;
                    //$scope.addChartIconBorder($scope.selectedChartInView[id]);
                    /*$.each(vm.DashboardModel.Dashboard.Report[index].axisConfig.checkboxModelDimension,function(key,value){
                        if(value.key){
                            delete value.key;
                            delete value.value;
                        }
                    });*/
                    if(vm.DashboardModel.Dashboard.Report[index]){
                        $.each(vm.DashboardModel.Dashboard.Report[index].axisConfig.checkboxModelMeasure,function(key,value){
                            if(value.key){
                                delete value.key;
                                delete value.value;
                            }
                        });
                    }
                    vm.Attributes = vm.DashboardModel.Dashboard.Report[index].axisConfig;
                    vm.DashboardModel.Dashboard.activeReport = vm.DashboardModel.Dashboard.Report[index];
                    vm.selectedChart = vm.DashboardModel.Dashboard.Report[index].chartObject;
                },
                getDashboardProto: function () {
                    return vm.DashboardModel.dashboardPrototypeObject;
                },
                getReportProto: function () {
                    return {
                        reportContainer: {},
                        chart: {},
                        axisConfig: {}
                    };
                },
                addReportContainer: function (chartInfo) {
                    var flag = true;
                    if ($scope.dashboardPrototype.reportContainers.length != 0)
                        vm.DashboardModel.Dashboard.Report.forEach(function (d, index) {
                            if (!d.chartObject) {
                                //flag = false;
                                flag = true;
                            }
                        });
                    /*if(flag)
                     {*/

                    var reportObject = vm.DashboardController.getReportProto();

                    reportObject.chart = chartInfo;
                    vm.DashboardModel.Dashboard.lastReportId++;
                    this.lastId = vm.DashboardModel.Dashboard.lastReportId;
                    vm.dashboardPrototype.reportContainers['chartType'] = chartInfo.name;
                    if (chartInfo.name == "Number Widget") {
                        vm.DashboardModel.numberWidgetContainerSettings.id = this.lastId;

                        var obj = vm.DashboardController.getNumberContainerSettings();
                        //obj['chartType'] = chartInfo.name;
                        obj['id'] = this.lastId;
                        obj['row'] = 0;
                        obj['col'] = 0;
                        reportObject.reportContainer = obj;

                        // vm.DashboardModel.Dashboard.activeReport.reportContainer = reportObject.reportContainer;


                        $scope.selectedChartInView[this.lastId] = sketch.chartData[0];
                    } else {
                        vm.DashboardModel.commonContainersSettings.id = this.lastId;
                        var obj = vm.DashboardController.getCommonContainerSettings();
                        obj['row'] = 0;
                        obj['col'] = 0;
                        obj['id'] = this.lastId;
                        obj['chartType'] = chartInfo.name;
                        reportObject.reportContainer = obj;
                        $scope.selectedChartInView[this.lastId] = sketch.chartData[0];
                        setTimeout(function () {
                            $(".li_box > .handle-se").removeClass("handle-se-hover");
                            $(".li_box").removeClass("li-box-border");
                            $("#li_" + vm.DashboardModel.commonContainersSettings.id+"> .handle-se").addClass("handle-se-hover");
                            $("#li_" + vm.DashboardModel.commonContainersSettings.id).addClass("li-box-border");
                        }, 1);
                    }
                    /*
                     * var $target = $('html,body');
                     * $target.animate({scrollTop:
                     * $target.height()}, 1000);
                     */
                    if (flag) {
                        reportObject.reportContainer['image'] = dataFactory.baseUrlData() + chartInfo.image;
                        vm.DashboardModel.Dashboard.activeReport = reportObject;
                        vm.DashboardView.addReportContainerView(reportObject.reportContainer);
                        vm.aggregateText[reportObject.reportContainer.id] = "Sum";
                        report.push(reportObject);
                        vm.DashboardModel.Dashboard.Report.push(reportObject);
                        vm.DashboardModel.Dashboard.activeReportIndex = vm.DashboardModel.Dashboard.Report.length - 1;
                    }else {
                        dataFactory.errorAlert("can't add more reports till current is empty");
                    }
                },

                updateReport: function () {
                    vm.DashboardModel.Dashboard.activeReport.chart = JSON.parse(vm.chartType);
                    // vm.DashboardModel.Dashboard.activeReportIndex
                },

                getActiveReport: function () {
                    return vm.DashboardModel.Dashboard.activeReport;
                }

            }

            vm.DashboardView = {
                init: function () {
                    vm.reportContainerPublicView=[];
                    // Gridster Settings
                    $scope.dashboardCommonSettings = vm.DashboardController.getGridSettings();
                    $scope.onDrop = vm.DashboardController.onChartDropToContainer;
                    vm.DashboardView.initDashboardProto();
                    $scope.addBorder = function (id) {
                        /*$(".box").removeClass("box-border");*/
                        $(".li_box").removeClass("li-box-border");
                        $(".li_box > .handle-se").removeClass("handle-se-hover");
                        $("#li_" + id).addClass("li-box-border");
                        $("#li_" + id+"> .handle-se").addClass("handle-se-hover");
                        /*$("#" + id).addClass("box-border");*/
                        vm.DashboardController.onReportContainerClick(id);
                    };
                    vm.numberFormate = function (style) {
                        var styleSettings = {};
                        styleSettings["NumberFormate"] = style;
                        styleSettings["type"] = "Number Widget";
                        var activeDashboardIndex = vm.DashboardModel.Dashboard.activeReportIndex;
                        vm.DashboardModel.Dashboard.Report[activeDashboardIndex]['attributes'] = "Number Widget";
                        sketch.renderAttributes(vm.DashboardModel.Dashboard.Report[activeDashboardIndex].chartObject, styleSettings);
                    }
                    vm.saveChartStyle = function (styleSettings) {
                        var activeDashboardIndex = vm.DashboardModel.Dashboard.activeReportIndex;
                        vm.DashboardModel.Dashboard.Report[activeDashboardIndex]['attributes'] = styleSettings;
                        sketch.renderAttributes(vm.DashboardModel.Dashboard.Report[activeDashboardIndex].chartObject,styleSettings);
                        generate('success','Chart style apply successfully');
                    }
                },
                activeTab :function(tabObj){
                    vm.activeTab=tabObj.key;
                    $(".loadingBar").show();
                    $(window).trigger('resize');
                    setTimeout(function(){
                        $(".loadingBar").hide();
                    },1000);
                },
                autoAdjustChartView: function () {
                    sketch.selfAdjustChart(vm.DashboardModel.Dashboard.activeReport);
                },
                render: function () {
                    vm.DashboardModel.Dashboard.Report.forEach(function(d,i) {
                        vm.DashboardView.addReportContainerView(d.reportContainer);
                        $scope.selectedChartInView[d.reportContainer.id]=d.chart;
                        if(d.chart.key=="_pivotTable"){
                            sketch.pivotTableConfig=d.chartObject;
                        }
                        $scope.$apply();
                        //Render chart
                        if(d.chart.key=="_text"){
                            sketch.container(d.reportContainer.id)._text(d.chart.value);
                        }else{
                            sketch
                                .axisConfig(d.axisConfig)
                                .data(vm.tableData)
                                .container(d.reportContainer.id)
                                .chartConfig(d.chart)
                                .render();
                        }
                        if(i==0){
                            vm.Attributes=d.axisConfig;
                        }
                        d.reportContainer.chartType=d.chart.name;
                        var width=$("#dashboardContainer_"+d.reportContainer.id).width();
                        var height=$("#dashboardContainer_"+d.reportContainer.id).height();
                        /*if(width<600){
                         $("#left"+d.reportContainer.id).hide();
                         $(".label_legends"+d.reportContainer.id).hide();
                         }*/
                    });
                },
                updateDashboardObject: function (drawn) {
                    var index = vm.rightSideController.findIndexofActiveContainer(
                        $scope.dashboardPrototype.reportContainers, "id",
                        vm.DashboardModel.Dashboard.activeReport.reportContainer.id);
                    if (drawn) {
                        $scope.filterSave = true;
                        $scope.saveState = 1;
                        $scope.dashboardPrototype.reportContainers[index]['image'] = null;
                    }

                    // vm.rightSideController.setActiveAxisConfig(vm.chartSettings);
                    var indexOfReport = vm.DashboardController.findIndexofActiveContainer(vm.DashboardModel.Dashboard.Report, "id", vm.DashboardModel.Dashboard.activeReport.reportContainer.id);
                    vm.DashboardModel.Dashboard.Report[indexOfReport].axisConfig = new Object(vm.Attributes);
                    vm.DashboardModel.Dashboard.Report[indexOfReport].axisConfig["xLabel"] = angular.copy(vm.activeXLabel[vm.DashboardModel.Dashboard.activeReport.reportContainer.id]);
                    vm.DashboardModel.Dashboard.Report[indexOfReport].axisConfig["yLabel"] = angular.copy(vm.activeYLabel[vm.DashboardModel.Dashboard.activeReport.reportContainer.id]);

                    if (drawn) {
                        // drawn["chartTypeAttribute"]=vm.DashboardModel.Dashboard.activeReport.chart.name;
                        // drawn["resize"]="ON";

                        vm.DashboardModel.Dashboard.Report[indexOfReport]["chartObject"] = drawn;
                    }
                    vm.DashboardModel.Dashboard.activeReport = vm.DashboardModel.Dashboard.Report[indexOfReport];
                    vm.DashboardModel.Dashboard.activeReportIndex = indexOfReport;
                },
                resetDataAndChart:function(){
                    return new Promise(
                        function (resolve, reject) {
                            eChart.resetAll();
                            setTimeout(function(){
                                resolve();
                            },100);
                        }
                    );
                },
                processChart: function () {
                    vm.chartLoading = false;

                    return new Promise(
                        function (resolve, reject) {
                            try {

                                if (!$.isEmptyObject(vm.rangeObject)) {

                                    vm.Attributes['timeline'] === vm.rangeObject;

                                    sketch
                                        .axisConfig(vm.Attributes)
                                        .data(vm.filteredData)
                                        .container(vm.DashboardModel.Dashboard.activeReport.reportContainer.id)
                                        .chartConfig($scope.selectedChartInView[vm.DashboardModel.Dashboard.activeReport.reportContainer.id])
                                        .render(
                                            function (drawn) {

                                                $scope.lastDrawnChart[vm.DashboardModel.Dashboard.activeReport.reportContainer.id] = vm.DashboardModel.Dashboard.activeReport.chart.key;
                                                resolve(drawn);
                                            });
                                }
                                else {
                                    sketch
                                        .axisConfig(vm.Attributes)
                                        .data(vm.tableData)
                                        .container(vm.DashboardModel.Dashboard.activeReport.reportContainer.id)
                                        .chartConfig($scope.selectedChartInView[vm.DashboardModel.Dashboard.activeReport.reportContainer.id])
                                        .render(
                                            function (drawn) {
                                                $scope.lastDrawnChart[vm.DashboardModel.Dashboard.activeReport.reportContainer.id] = vm.DashboardModel.Dashboard.activeReport.chart.key;
                                                resolve(drawn);
                                            });

                                }


                            } catch (e) {

                            }
                        }
                    );
                },
                renderChartInActiveContainer: function () {
                    sketch.pivotTableConfig = undefined;
                    try {
                        vm.DashboardView
                            .resetDataAndChart()
                            .then(vm.DashboardView.processChart)
                            .then(vm.DashboardView.updateDashboardObject)
                            .then(function () {
                                vm.chartLoading = false;

                            })
                            .then(function () {

                                var variable = "reportLoading_" + vm.DashboardModel.Dashboard.activeReport.reportContainer.id;
                                vm[variable] = false;
                                $scope.$apply();
                                $(".label_legends" + vm.DashboardModel.Dashboard.activeReport.reportContainer.id).show();
                                return true;

                            });
                    } catch (e) {

                        vm.chartErrorText = "Some Error occured while drawing this chart";
                        vm.chartLoading = false;

                    }
                    return false;

                },

                initDashboardProto: function () {
                    $scope.dashboardPrototype = vm.DashboardController
                        .getDashboardProto();

                },
                // render dashboard view
                addReportContainerView: function (reportContainerObject) {
                    if(reportContainerObject.tab==undefined){
                        reportContainerObject['tab']=vm.activeTab;
                        if($scope.dashboardPrototype.reportWithTab[vm.activeTab]==undefined){
                            $scope.dashboardPrototype.reportWithTab[vm.activeTab]=[];
                        }
                        $scope.dashboardPrototype.reportWithTab[vm.activeTab].push(reportContainerObject);

                    }else{
                        if($scope.dashboardPrototype.reportWithTab==undefined){
                            $scope.dashboardPrototype.reportWithTab={};
                        }
                        if($scope.dashboardPrototype.reportWithTab[reportContainerObject.tab]==undefined){
                            $scope.dashboardPrototype.reportWithTab[reportContainerObject.tab]=[];
                        }
                        $scope.dashboardPrototype.reportWithTab[reportContainerObject.tab].push(reportContainerObject);
                    }
                    $scope.dashboardPrototype.reportContainers.push(reportContainerObject);
                },
                selectReportContainerView: function (reportContainerObject) {

                },
                showGridBorder: function () {
                    $("#dash-border").addClass("dash-border");
                },
                removeGridBorder: function () {
                    $("#dash-border").removeClass("dash-border");
                }
            }
            vm.DashboardController.init();


            $("#loadscreen").show();
            $scope.listdata = [];
            $scope.listOf = false;
            $scope.dashName.dashboardName = "";
            $scope.dashboardDesc = "Dashboard Description"

            vm.containerLoaded = false;
            vm.isSmallSidebar = true;
            vm.isParameterShown = false;
            vm.isNoContainerLoaded = true;
            vm.ischartTypeShown = false;
            vm.isQuerySelected = true;
            vm.placeholder = true;
            vm.filtersTo = [];
            vm.lastAppliedFilters = {};
            vm.saveState = -1;
            // charts list
            vm.MetaDataLoader = true;
            // Function for fetching metadata list
            var dashboardView = function () {
                var promise = new Promise(
                    function (resolve, reject) {
                        dataFactory.request($rootScope.DashboardList_Url,'post',"").then(function(response) {
                                if(response.data.errorCode==1){
                                    $scope.viewLoaded = true;
                                    $scope.viewLoad = true;
                                    vm.dashboardList = response.data.data;
                                    resolve();
                                }else{
                                    dataFactory.errorAler(response.message);
                                }
                            },
                            function(error) {
                                $scope.status = 'Unable to load customer data: '+ error.message;
                            });
                    });
                return promise;
            };

            vm.companyHeader='no';
            vm.CommonSettingData = function(CommonSettingObject){
                var mainHeader = CommonSettingObject.main_Header;
                var reportHeader = CommonSettingObject.report_Header;
                var headerTextcolor = CommonSettingObject.headerTextcolor;
                var headerBgcolor = CommonSettingObject.headerBgcolor;
                var companyHeaderBg=CommonSettingObject.companyBackground;
                var companyHeaderColor=CommonSettingObject.companyColor;
                vm.checkSettingSave=true;
                var style = {};
                style.mainHeader = mainHeader;
                style.reportHeader = reportHeader;
                style.headerTextcolor = headerTextcolor;
                style.headerBgcolor = headerBgcolor;
                style.companyHeaderBg=companyHeaderBg;
                style.companyHeaderColor=companyHeaderColor;
                style.border = CommonSettingObject.border;
                style.borderColor = CommonSettingObject.border_color;
                style.borderWidth = CommonSettingObject.border_width;
                //Save common setting object
                vm.DashboardModel.Dashboard['commonStyle']=style;
                vm.DashboardModel.Dashboard['companyHeader']=vm.company;
                if(reportHeader == 'yes' || reportHeader == 'Yes'){
                    var common_Heading = { backgroundColor : headerBgcolor + '!important', color : headerTextcolor + '!important' };
                    $(".Common_Head").css(common_Heading);
                }else{
                    //$(".Common_Head").css("display", "none");
                }
                if(mainHeader=="yes"){
                    var common_Heading = { backgroundColor : CommonSettingObject.companyBackground, color : CommonSettingObject.companyColor};
                    $(".companyHeader").css(common_Heading);
                    vm.companyHeader='yes';
                    $(".companyHeader").show();
                }else{
                    vm.companyHeader='no';
                    $(".companyHeader").hide();
                }
                if(CommonSettingObject.border=='yes'){
                    var common_Heading = { border : CommonSettingObject.border_width+" solid "+" "+CommonSettingObject.border_color};
                    $(".li_box").css(common_Heading);
                }
                $('#Common_View_Setting').modal('hide');
                if(vm.company==undefined){
                    vm.company={};
                }
                if(vm.company.name==undefined || vm.company.name==""){
                    vm.company.name="Thinklytics";
                }
                if(vm.company.description==undefined || vm.company.description==""){
                    vm.company.description="Business intelligence and analytics";
                }
            }
            // Custom Setting
            vm.viewCustomSetting = function(index){
                vm.Custom_Container_Id = index;
                $('#Custom_View_Setting').modal('show');
            }

            // Comapny Setting
            vm.companySet={};
            vm.dataimg = 'none';
            vm.logoCheck=false;
            vm.companyHeaderSave=function(company){
                vm.logoCheck=true;
                var f = document.getElementById('logo').files[0];
                var r = new FileReader();
                r.onloadend = function(e) {
                    $scope.data = e.target.result;
                    var img = document.getElementById('img');
                    vm.logo = 'data:image/jpeg;base64,' + btoa(e.target.result);
                    img.src = 'data:image/jpeg;base64,' + btoa(e.target.result);

                    //$("#img").appendChild(img);
                }
                r.readAsBinaryString(f);
                if(vm.company && vm.company.name)
                    vm.company.name=company.name;
                if(vm.company && vm.company.description)
                    vm.company.description=company.description;
                //vm.company.log=company.logo;
                $('#companyHeaderSet').modal('hide');
            }
            // Api For Getting Connection
            dataFactory.request($rootScope.MetadataList_Url,'post',"").then(function (response) {
                if(response.data.errorCode==1){
                    //vm.queryList = JSON.parse(response.data.result);
                    vm.queryList = [];
                    Object.keys(response.data.result).forEach(function(key){
                        response.data.result[key].forEach(function(d){
                            vm.queryList.push(d);
                        });
                    });
                }else{
                    dataFactory.errorAlert(response.data.message);
                }
            }).then(function () {
                if ($stateParams.id != undefined) {
                    var metadataId = $stateParams.id;
                    var i = 0;
                    var index;
                    vm.queryList.forEach(function (d, indexArray) {
                        if (d.metadataId == metadataId) {
                            index = indexArray;
                        }
                    });
                    $scope.querySelector = JSON.stringify($scope.queryList[index]);
                    //$scope.selectDataSource(JSON.stringify($scope.queryList[index]));
                    $(".modal-backdrop").hide();
                }
            });
            // creating widgets
            var lastcontainerId;
            var lastcount = 0;
            var newDim = [];


            // Filter Drawer
            vm.filterBy = function (item, filter) {
                var totalLength=Object.keys(vm.allFilters).length;
                if(index==undefined){
                    Object.keys(vm.allFilters).forEach(function (d,filterIndex) {
                        if(d==filter.key){
                            index=filterIndex;
                        }
                    })
                }
                if(vm.dataCount<$rootScope.localDataLimit) {
                    sketch.applyFilter(item, filter);
                }else if(totalLength-1!=index){
                    $(".loadingBar").show();
                    sketchServer.applyFilter(item, filter, vm.metadataId,totalLength,index,function (data, filterObj, status) {
                        if (status) {
                            try{

                                // vm.allFilters[filterObj.key]['filterValue']=data;
                                if(filterObj.value=="varchar" || filterObj.value=="char" || filterObj.value=="text"){
                                    vm.conatinerFilterTo[filterObj.key]=data;
                                    vm.allFilters[filterObj.key].filterValue = data;
                                    $scope.$apply();
                                    $("[id^=filterSelect_]").each(function () {
                                        if($(this).attr('id')!="filterSelect_"+index) {
                                            $(this).selectpicker('destroy', true);
                                        }
                                    })
                                    setTimeout(function () {
                                        $("[id^=filterSelect_]").each(function () {
                                            if($(this).attr('id')!="filterSelect_"+index){
                                                $(this).selectpicker();
                                            }
                                        });
                                        $(".loadingBar").hide();
                                    }, 1000);
                                }else if(filterObj.value=="decimal" || filterObj.value=="int" || filterObj.value=="bigint" || filterObj.value=="double" || filterObj.value=="float"){
                                    /*
                                       Range filter
                                    */
                                    setTimeout(function () {
                                        var min = Math.min.apply(Math,data);
                                        var max = Math.max.apply(Math,data);
                                        if(min && max){
                                            if (min == null) {
                                                min = 0;
                                            }
                                            if(min==max){
                                                max=max+1;
                                            }
                                            var config = {
                                                orientation: "horizontal",
                                                start: [min,max],
                                                range: {
                                                    min: min,
                                                    max: max,
                                                },
                                                connect: 'lower',
                                                direction: "ltr",
                                                step: 10,
                                            };
                                            var nonLinearSlider = document.getElementById(filterObj.key.replace("(","(").replace(")",")")+ "-Filter");
                                            document.getElementById(filterObj.key.replace("(","(").replace(")",")")+ "-Filter").noUiSlider.destroy();
                                            noUiSlider.create(nonLinearSlider, {
                                                animate: true,
                                                start: [min, max],
                                                connect: true,
                                                range: {
                                                    min: parseInt(min),
                                                    max: parseInt(max)
                                                },
                                                step: 1,
                                                tooltips: [wNumb({
                                                    decimals: 0
                                                }), wNumb({
                                                    decimals: 0
                                                })]
                                            });
                                            vm.allFilters[filterObj.key]['filterValue'] = [min, max];
                                            nonLinearSlider.noUiSlider.on('update', function (values, handle) {
                                                vm.allFilters[filterObj.key]['filterValue'] = values;

                                            });
                                            nonLinearSlider.noUiSlider.on('change', function (values, handle) {
                                                vm.filterBy(values,filterObj);
                                            });
                                        }
                                        $(".loadingBar").hide();
                                    },1000);
                                    /*
                                      End range filter
                                     */
                                }
                            }catch(e){

                            }
                        }else{
                            setTimeout(function () {
                                $(".loadingBar").hide();
                            }, 1000);
                        }
                    });
                }
            }


            // delete bhavesh
            $scope.deleteGraphKeys = function (id) {
                if(vm.dataCount<$rootScope.localDataLimit){
                    $scope.deleteReportContainer(id);
                    $scope.$apply();
                    var listAttr=eChart.chartRegistry.listAttributes();
                    if(listAttr){
                        delete listAttr["chart-"+id];
                    }
                    dataFactory.successAlert("Report Deleted Successfully");
                }else{
                    var data={reportId:id,metadataId:vm.metadataId,sessionId:$rootScope.accessToken};
                    dataFactory.nodeRequest('deleteReport','post',data).then(function(response){
                        if(response.success){
                            $scope.deleteReportContainer(id);
                            var listAttr=eChartServer.chartRegistry.listAttributes();
                            if(listAttr){
                                delete listAttr["chart-"+id];
                            }
                            dataFactory.successAlert("Report Deleted Successfully");
                            $scope.$apply();
                        }else{
                            dataFactory.errorAlert("Check your connection");
                        }
                    });
                }

            }
            $scope.findIndexOfReportContainer = function (id) {
                var index = -1;
                var i = 0;
                $scope.dashboardPrototype.reportWithTab[vm.activeTab]
                    .forEach(function (d) {
                        if (d.id == id)
                            index = i;
                        i++;
                    });

                return index;
            }
            $scope.findIndexOfReportFromReportContainer = function (id) {
                var index = -1;
                vm.DashboardModel.Dashboard.Report.forEach(function (d,i) {
                    if (d.reportContainer.id == id){
                        index = i;
                    }
                });
                return index;
            }
            $scope.deleteReportContainer = function (id) {
                if(sketchServer._totalReportCount){
                    sketchServer._totalReportCount--;
                }
                var index1 = $scope.findIndexOfReportFromReportContainer(id);
                var index = $scope.findIndexOfReportContainer(id);
                vm.Attributes = {};
                $scope.dashboardPrototype.reportWithTab[vm.activeTab].splice(index, 1);
                $scope.$apply();
                $scope.dashboardPrototype.reportContainers.splice(index, 1);
                vm.DashboardModel.Dashboard.Report.splice(index1, 1);
            }
            /*
                 * Report group
                 */
            vm.publicViewType="sharedview"
            vm.reportGrp={};
            dataFactory.request($rootScope.UsergroupList_Url,'post',"").then(function(response){
                vm.userGroup=response.data.result;
            });
            dataFactory.request($rootScope.ReportGroupList_Url,'post',"").then(function(response){
                vm.reportGroup=response.data.result;
            });
            vm.reportGroupFun=function(){
                vm.realTime={};
                vm.realTime.flag=false;
                var flag=0;
                var tabKey="";
                vm.tabs.forEach(function(d){
                    if($scope.dashboardPrototype.reportWithTab==undefined || $scope.dashboardPrototype.reportWithTab[d.key]==undefined || $scope.dashboardPrototype.reportWithTab[d.key].length==0){
                        flag=1;
                        tabKey=d.key;
                    }
                })
                if(flag==0){
                    $('#reportGroup').modal('show');
                    $("#tabBtn_"+vm.tabs[0].key).trigger('click');
                }else{
                    dataFactory.errorAlert(tabKey+" tab can't be empty");
                }
            }
            vm.selectGroup=function(divClass){
                if(divClass=='back'){
                    $(".mainDiv").slideDown('fast');
                    $(".select").slideUp('fast');
                    $(".create").slideUp('fast');
                    $(".footerDiv").slideUp('fast');
                }else{
                    $("."+divClass).slideDown('fast');
                    $(".footerDiv").slideDown('fast');
                    $(".mainDiv").slideUp('fast');
                }
            }
            /*
                 * Check view
                 */
            vm.checkView=function(page){
                vm.publicViewType=page;
                if(page=='publicview'){
                    $(".subDiv").hide();
                    $(".footerDiv").show();
                }
                else{
                    $(".subDiv").show();
                    $(".footerDiv").hide();
                }
            }
            vm.save = function (reportGrp) {
                if(vm.dashName.dashboardName!=undefined && vm.dashName.dashboardName!="" && vm.DashboardModel.Dashboard.Report.length>0){
                    $('#reportGroup').modal('hide');
                    $(".loadingBar").show();
                    var metadataObject = $scope.metadataObject;
                    metadataObject['column'] = vm.tableColumns;
                    vm.DashboardModel.Dashboard['customMeasure'] = vm.customMeasure;
                    vm.DashboardModel.Dashboard['customGroup'] = vm.customGroup;
                    var reportsObject = vm.DashboardModel.Dashboard;
                    reportsObject['tabs']=vm.tabs;
                    var newDate = new Date();
                    var image_name = new Date().getTime();
                    html2canvas($("#tab"+vm.tabs[0].key+'> div'),
                        {
                            onrendered: function (canvas) {
                                var theCanvas = canvas;
                                var img = canvas.toDataURL("image/png");
                                var output = encodeURIComponent(img);
                                var data={
                                    "image":img,
                                    "image_name":image_name,
                                    "logo":vm.logo
                                };
                                dataFactory.request($rootScope.sharedViewImage_Url,'post',data).then(function(response){
                                    if(response.data.errorCode==1){
                                        var dashSaveObj = {};
                                        var dashRepObj = {};
                                        var dObj = "";
                                        reportsObject['metadataObject'] = $scope.metadataObject;
                                        reportsObject['connectionObject'] = $scope.metadataObject.connObject.connectionObject;
                                        reportsObject['colorObject'] = eChart.chartRegistry.listAttributes();
                                        reportsObject['realtime']=vm.realTime;
                                        var data={
                                            "name":vm.dashName.dashboardName,
                                            "dashboardDesc":$scope.dashboardDesc,
                                            "repotsObject":JSON.stringify(reportsObject),
                                            "filterShow":vm.filterShowHidden,
                                            "filterBy":JSON.stringify(vm.conatinerFilter),
                                            "image":image_name,
                                            "logo":"logo_"+image_name,
                                            "group":JSON.stringify(vm.group),
                                            "dashboardId":vm.dashboardId,
                                            "reportGroup":reportGrp,
                                            "publicViewType":vm.publicViewType
                                        };

                                        dataFactory.request($rootScope.sharedViewSave_Url,'post',data).then(function(response){
                                            if (response.data.errorCode==1) {
                                                $(".loadingBar").hide();
                                                dataFactory.successAlert(response.data.message);
                                                $window.location = "#/sharedView/";
                                            } else {
                                                dataFactory.errorAlert(response.data.message);
                                                $(".loadingBar").hide();
                                            }
                                        })
                                    }else{
                                        dataFactory.errorAlert(response.data.message);
                                    }
                                });
                            }
                        });
                }else{
                    dataFactory.errorAlert("Name and minimum one report required");
                }
            };
            //End draw chart andcontainer
            //for range object
            //Get array list
            vm.getArrayList = function (numberColumn) {
                var numberArray = [];
                for (var i = 1; i <= numberColumn; i++) {
                    numberArray.push(i);
                }
                return numberArray;
            }
            $scope.filterRangeColumn = function (index, columnName) {
                $scope.filterColumnRange = {};
                $scope.filterColumnRange['index'] = index;
                $scope.filterColumnRange['columnName'] = columnName;
            }
            //ENd
            // Filter column Name
            $scope.filterColumn = function (columnName) {
                $scope.filterColumnName = columnName;
            }
            //Remove filter
            vm.custFilterRemove = function (index) {
                vm.filerTemp.splice(index,1);
            }
            //New dashboard edit
            vm.dashboardEditDraw={
                init:function(){
                    //For Filter
                    vm.filerTemp = [];
                    vm.filtersToApply = [];
                    var data={
                        "auth_key":auth_key
                    }
                    var publicViewObjectLoad = new Promise(function(resolve, reject){
                        dataFactory.request($rootScope.PublicViewRetrieveEdit_Url,'post',data).then(function(response){
                            vm.dashboardId=response.data.result.dashboard_id;
                            vm.commonSettingData = JSON.parse(response.data.result.reportObject)['commonStyle'];
                            vm.categoryGroupObject=JSON.parse(response.data.result.group);
                            vm.tabs=JSON.parse(response.data.result.reportObject).tabs;
                            vm.realTime = {};
                            vm.realTime.flag = JSON.parse(response.data.result.reportObject).realtime.flag;
                            vm.realTime.time = parseInt(JSON.parse(response.data.result.reportObject).realtime.time);

                            resolve(response);
                        });
                    });
                    function processAfterPublicViewObjectLoad(response){
                        if(response.data.errorCode==1){
                            if(response.data.result && response.data.result.name){
                                $scope.dashName.dashboardName = response.data.result.name;
                            }
                            var reportObject=JSON.parse(response.data.result.reportObject);
                            vm.reportObject = reportObject;
                            vm.logoPath=dataFactory.baseUrlData()+"../app/dashboardImage/"+response.data.result.logo+".png";
                            vm.dataimg = vm.logoPath;
                            if(vm.logoPath){
                                vm.logoCheck = true;
                            }else{
                                vm.logoCheck = false;
                            }
                            if(reportObject.companyHeader!=undefined){
                                vm.company=reportObject.companyHeader;
                                vm.company.logo = vm.logoPath;
                            }
                            //CHeck range object
                            var index =vm.dashboardEditDraw.isTimeObjectFound(reportObject.Report);
                            if(index!=-1){
                                vm.rangeObject = reportObject.Report[index].axisConfig.timeline;
                                Object.keys(vm.rangeObject).forEach(function(d){
                                    vm.dashboardEditDraw.rangeFilter(vm.rangeObject[d].key.columnName, vm.rangeObject[d].numberOfColumn);
                                });
                            }
                            vm.tempFilter=JSON.parse(response.data.result.filterBy);
                            //Change object for different between object
                            vm.selectedFilter=[];
                            vm.allFilters={};
                            if(vm.tempFilter.length){
                                vm.tempFilter.forEach(function(d){
                                    vm.allFilters[d.key]=[];
                                    if(d.key==undefined){
                                        var tempObj={};
                                        tempObj['key']=d.columnName;
                                        tempObj['value']=d.columType
                                        vm.selectedFilter.push(tempObj);
                                    }else{
                                        vm.selectedFilter.push(d);
                                    }
                                });
                            }
                            vm.querySelector = reportObject.metadataObject;
                            vm.metadataSelect={};
                            vm.dashboardEditDraw.selectDataSource(vm.querySelector);
                            vm.DashboardModel.Dashboard=Object.assign({},reportObject);
                            vm.metadataObject=reportObject.metadataObject;
                            vm.metadataId=vm.metadataObject.metadataId;
                            vm.group=JSON.parse(response.data.result.group);
                            vm.dashboardId=response.data.result.dashboard_id;
                        }else{
                            dataFactory.errorAlert(response.data.message);
                        }
                    }
                    function temp_one(){
                        return new Promise(function(resolve, reject){
                            var data = {
                                "matadataObject":JSON.stringify(vm.metadataObject),
                                "type":'dashboard'
                            };
                            dataFactory.request($rootScope.MetadataGetLength_Url,'post',data).then(function(response) {
                                if (response.data.errorCode == 1) {
                                    vm.dataCount = response.data.result;
                                    if(vm.dataCount<$rootScope.localDataLimit){
                                        vm.dashboardEditDraw.fetchDataAndColumns().then(function () {
                                            var data = {
                                                "id": $scope.dashboardId
                                            };
                                            resolve(data);
                                        });
                                    }else{
                                        vm.dashboardEditDraw.fetchDataServer().then(function () {
                                            var data = {
                                                "id": $scope.dashboardId
                                            };
                                            resolve(data);
                                        });
                                    }
                                }
                            });
                        });
                    }

                    function temp_two(data){
                        return new Promise(function(resolve, reject){
                            dataFactory.request($rootScope.DashboardIdToObject_Url,'post',data).then(function(response){
                                if(response.data.errorCode){
                                    vm.metadataObject=(JSON.parse(response.data.result.reportObject)).metadataObject;
                                    vm.metadataId=vm.metadataObject.metadataId;
                                    vm.reports=(JSON.parse(response.data.result.reportObject)).Report;
                                    if((JSON.parse(response.data.result.reportObject)).colorObject){
                                        vm.reports.forEach(function(d){
                                            if((JSON.parse(response.data.result.reportObject)).colorObject["chart-"+d.reportContainer.id]){
                                                d["colorObject"]=(JSON.parse(response.data.result.reportObject)).colorObject["chart-"+d.reportContainer.id];
                                            }
                                        });
                                    }
                                    eChart.chartRegistry.registerAllAttributes((JSON.parse(response.data.result.reportObject)).colorObject);
                                    vm.checkboxModel = JSON.parse(response.data.result.filterBy);
                                    vm.dashboardEditDraw.filterAddDashboard();
                                }else{
                                    dataFactory.errorAlert(response.data.message);
                                }
                                vm.dashboardEditDraw.addContainerDrawChart(vm.reportObject);
                                resolve(response);
                            });
                        })
                    }

                    function temp_three(response){
                        return new Promise(function(resolve, reject){
                            vm.checkboxModel = JSON.parse(response.data.result.filterBy);
                            //vm.selectedFilter = JSON.parse(response.data.result.filterBy);
                            if(vm.checkboxModel != null || vm.checkboxModel != undefined){
                                if(vm.checkboxModel.length==0){
                                    $(".filter").hide();
                                }
                            }
                            vm.dashboardEditDraw.simpleFilterContainerAdd();
                            vm.preLoad = false;
                            vm.graphAxis = true;
                            vm.loadingBarDrawer = false;
                            vm.loadingBar = true;
                            vm.loadingBarView = true;
                            var rawData = vm.tableData;
                            resolve();
                        })
                    }

                    function temp_four(){
                        return new Promise(function(resolve, reject){
                            if(vm.dataCount<$rootScope.localDataLimit){
                                vm.dashboardEditDraw.reportDragDrop();
                            }else{
                                // vm.dashboardEditDraw.reportDragDropServer();
                            }
                            // vm.dashboardEditDraw.addContainerDrawChart(vm.reportObject);
                            vm.company=vm.reportObject.companyHeader;
                            vm.CommonSettingObject={};
                            vm.dashboardEditDraw.drawHeader(vm.reportObject);
                            resolve();
                        })
                    }

                    function temp_five(){
                        vm.CommonSettingObject = {};
                        if(vm.commonSettingData != undefined){
                            vm.CommonSettingObject['report_Header'] = vm.commonSettingData.reportHeader;
                            vm.CommonSettingObject['headerTextcolor'] = vm.commonSettingData.headerTextcolor;
                            vm.CommonSettingObject['headerBgcolor'] = vm.commonSettingData.headerBgcolor;
                            vm.CommonSettingObject['border'] = vm.commonSettingData.border;
                            vm.CommonSettingObject['border_color'] = vm.commonSettingData.borderColor;
                            vm.CommonSettingObject['border_width'] = vm.commonSettingData.borderWidth;
                            vm.CommonSettingObject['main_Header'] = vm.commonSettingData.mainHeader;
                            vm.CommonSettingObject['companyColor'] = vm.commonSettingData.companyHeaderColor;
                            vm.CommonSettingObject['companyBackground'] = vm.commonSettingData.companyHeaderBg;
                            $scope.$apply();
                        }
                    }

                    publicViewObjectLoad.then(processAfterPublicViewObjectLoad).then(temp_one).then(temp_two).then(temp_three).then(temp_four).then(temp_five);
                },
                fetchDataServer:function(){
                    var promise = new Promise(
                        function (resolve, reject) {
                            var data = {
                                "matadataObject":JSON.stringify(vm.metadataObject),
                                "type":'dashboard'
                            };
                            /*
                             * Only for cache data to server
                             */
                            var responseColumn;
                            dataFactory.request($rootScope.DashboardDataCacheToRedis_Url,'post',data).then(function(response){
                                responseColumn=response.data.result.tableColumn;
                            }).then(function(){
                                var columns=Object.assign([],vm.metadataObject.connObject.column);
                                var data={
                                    metadataId:vm.metadataId,
                                    dashboardId:vm.dashboardId,
                                    columns:columns
                                };
                                dataFactory.request($rootScope.GetColumn_Url,'post',data).then(function(response){
                                    vm.tableColumns = response.data.result;
                                }).then(function() {
                                    $rootScope.initializeNodeClient(vm.metadataObject).then(function (response) {

                                        setTimeout(function () {
                                            calculationServer
                                                .oldObject(responseColumn)
                                                .newObject(vm.tableColumns)
                                                .dataGroupCalculation(vm.metadataId, $rootScope.accessToken, vm.categoryGroupObject);
                                            resolve();
                                        }, 2000);
                                    });
                                });
                            });
                        });
                    return promise;
                },
                fetchDataAndColumns:function () {
                    var promise = new Promise(
                        function (resolve, reject) {
                            var data = {
                                "matadataObject":JSON.stringify(vm.metadataObject),
                                "type":'dashboard'
                            };
                            dataFactory.request($rootScope.DashboardData_Url,'post',data).then(function(response){
                                if(response.data.errorCode==1) {
                                    vm.blnkData = true;
                                    vm.blnkFilter = true;
                                    vm.blnkChart = true;
                                    vm.blnkSource = true;
                                    vm.tableData = JSON.parse(response.data.result.tableData);
                                    vm.metadataObject = vm.metadataObject;
                                    vm.tableColumns = vm.metadataObject.connObject.column;
                                    // $scope.groupObject
                                    calculation
                                        .oldObject(JSON.parse(response.data.result.tableColumn))
                                        .newObject(vm.tableColumns)
                                        .tabledata(vm.tableData)
                                        .uniqueArray()
                                        .columnCal()
                                        .groupObject(vm.group)
                                        .columnCategory();
                                    vm.tableData = calculation._tabledata;
                                    setTimeout(function () {
                                        $("#filter").multiselect("destroy");
                                        $("#filter").multiselect({
                                            includeSelectAllOption: false,
                                            enableFiltering: true,
                                            maxHeight: 200
                                        });
                                        resolve();
                                    }, 1000);
                                }
                            });
                        });
                    return promise;
                },

                selectDataSource:function (model) {
                    vm.dashboardValidation = true;
                    vm.loadingBarDrawer = true;
                    vm.tableData = [];
                    vm.tableColumnsMeasure = [];
                    sketch._crossfilter = null;
                    sketch._data = null;
                    vm.filtersToApply = [];
                    lastcount = 0;
                    // resetAllAxis();
                    if (model != undefined) {
                        vm.ischartTypeShown = true;
                        vm.isQuerySelected = false;
                        vm.graphAxis = false;
                        vm.queryObj = model;
                        vm.dashboardValidation = true;
                        vm.dashboardValidation1 = true;
                        vm.showInfo1 = false;
                        vm.showInfo2 = true;
                        vm.preLoad = true;
                        vm.groupObject = "";
                        vm.metadataObject = model;
                    }
                    vm.moveToMdArray = {};
                    vm.moveToDimMeasure = function (moveTo, columnName, isChecked) {
                        if(!isChecked){
                            vm.tableColumns.filter(function (d, is) {
                                if (d.columnName == columnName) {
                                    vm.tableColumns[is].dataKey = moveTo;
                                    return true;
                                }
                                $scope.moveToMdArray[columnName] = moveTo

                                return false;
                            });
                        }else{
                            alert("Can't Move to "+moveTo+" If it's selected");
                        }
                    }
                    $("#filter").multiselect("destroy");
                    $("#filter").multiselect({
                        includeSelectAllOption: false,
                        enableFiltering: true,
                        maxHeight: 200
                    });
                },
                drawHeader:function(reportObject){
                    if(reportObject.commonStyle && reportObject.commonStyle.reportHeader=='yes'){
                        var common_Heading = { backgroundColor : reportObject.commonStyle.headerBgcolor, color : reportObject.commonStyle.headerTextcolor};
                        setTimeout(function(){
                            vm.CommonSettingObject.report_Header=reportObject.commonStyle.reportHeader;
                            vm.CommonSettingObject.headerTextcolor=reportObject.commonStyle.headerTextcolor;
                            vm.CommonSettingObject.headerBgcolor=reportObject.commonStyle.headerBgcolor;
                            $(".Common_Head").css(common_Heading);
                        },1);
                    }else if(reportObject.commonStyle && reportObject.commonStyle.reportHeader=='no'){
                        vm.CommonSettingObject.report_Header="no";
                        $(".commonStyle").css("display", "none");
                    }
                    if(reportObject.commonStyle && reportObject.commonStyle.mainHeader=="yes"){
                        var company_Heading = { backgroundColor : reportObject.commonStyle.companyHeaderBg, color : reportObject.commonStyle.companyHeaderColor};
                        vm.companyHeader='yes';
                        vm.CommonSettingObject.main_Header=reportObject.commonStyle.mainHeader;
                        vm.CommonSettingObject.companyColor=reportObject.commonStyle.companyHeaderColor;
                        vm.CommonSettingObject.companyBackgroud=reportObject.commonStyle.companyHeaderBg;
                        $(".companyHeader").css(company_Heading);
                        $(".defaultHeader").hide();
                    }else{
                        vm.CommonSettingObject.main_Header='no';
                        $(".companyHeader").css("display", "none");
                    }
                    setTimeout(function(){
                        if(vm.CommonSettingObject && vm.CommonSettingObject.headerBgcolor){
                            if(vm.CommonSettingObject.headerBgcolor.match(/fff/g)){
                                $(".defaultColor").css("background-color", "#CCC");
                            }else{
                                $(".defaultColor").css("background-color", "inherit");
                            }
                        }else{
                            $(".defaultColor").css("background-color", "#CCC !important");
                        }
                    },1000);
                    vm.CommonSettingObject.border_width="1px";
                    vm.CommonSettingObject.border='no';
                    if(reportObject.commonStyle && reportObject.commonStyle.border=='yes'){
                        var common_border = { border : reportObject.commonStyle.borderWidth+" solid "+" "+reportObject.commonStyle.borderColor};
                        setTimeout(function(){
                            vm.CommonSettingObject.border=reportObject.commonStyle.border;
                            vm.CommonSettingObject.border_color=reportObject.commonStyle.borderColor;
                            vm.CommonSettingObject.border_width=reportObject.commonStyle.borderWidth;
                            $(".li_box").css(common_border);
                        },1);
                    }

                    vm.colorObject=reportObject.colorObject;
                    eChart.chartRegistry.registerAllAttributes(reportObject.colorObject);
                },
                loadingBar:function(variable){
                    vm.reportResponseCount++;
                    setTimeout(function(){
                        vm[variable] = false;
                        if(!$scope.$$phase) {
                            $scope.$apply();
                        }
                    },1000);

                    if(vm.reportResponseCount == vm.reportCount){
                        setTimeout(function(){
                            $( "#tabBtn_"+vm.tabs[0].key ).trigger( "click" );
                            vm.dashboardEditDraw.reportDragDropServer();
                        },4000);
                    }

                    //setTimeout(function(){
                    //  $(".loadingBar").hide();
                    //},6000);
                },

                addContainerDrawChart:function(reportObject){
                    var reportCount=reportObject.Report.length;
                    vm.reportCount=reportObject.Report.length;
                    sketchServer._totalReportCount=reportCount;
                    var j=0;
                    vm.reportResponseCount = 0;
                    reportObject.Report.forEach(function(d,i) {
                        var variable = 'reportLoading_'+d.reportContainer.id;
                        vm[variable] = true;
                        delete d.reportContainer['$$hashKey'];
                        vm.DashboardView.addReportContainerView(d.reportContainer);
                        $scope.$apply();
                        vm.dashboardEditDraw.checkContainerSize(d);
                        if(!$scope.$$phase) {
                            $scope.$apply();
                        }
                        if(vm.dataCount<$rootScope.localDataLimit){
                            if(d.chart.key=="_pivotTable"){
                                sketch.pivotTableConfig=d.chartObject;
                            }
                            //Render chart
                            if(d.chart.key=="_text"){
                                sketch
                                    .container(d.reportContainer.id)
                                    ._text(d.chart.value);
                                vm.dashboardEditDraw.loadingBar(variable);
                            }
                            else if(d.chart.key=="_maleFemaleChartJs"){
                                sketch.axisConfig(d.axisConfig)
                                    .data(vm.tableData)
                                    .container(d.reportContainer.id)
                                    ._maleFemaleChartDraw();
                                vm.dashboardEditDraw.loadingBar(variable);
                            }
                            else if(d.chart.key=="_PivotCustomized"){
                                sketch.axisConfig(d.axisConfig)
                                    .data(vm.tableData)
                                    .container(d.reportContainer.id)
                                    .pivotCust_Draw();
                                vm.dashboardEditDraw.loadingBar(variable);
                            }
                            else if(d.chart.key=="_mapChartJs"){
                                sketch.axisConfig(d.axisConfig)
                                    .data(vm.tableData)
                                    .container(d.reportContainer.id)
                                    ._mapChartDraw();
                                vm.dashboardEditDraw.loadingBar(variable);
                            }
                            // else if(d.chart.key=="_pivotTable"){
                            //     sketch.axisConfig(d.axisConfig)
                            //         .data(vm.tableData)
                            //         .container(d.reportContainer.id)
                            //         ._pivotTableDraw();
                            //     vm.dashboardEditDraw.loadingBar(variable);
                            // }
                            else if(d.chart.key=="_floorPlanJs"){
                                sketch.axisConfig(d.axisConfig)
                                    .data(vm.tableData)
                                    .container(d.reportContainer.id)
                                    ._floorPlanDraw();
                                vm.dashboardEditDraw.loadingBar(variable);
                            }
                            else{
                                if(d.colorObject && d.colorObject["chart-"+d.reportContainer.id])
                                    eChart.chartRegistry.registerPublicViewColor("chart-"+d.reportContainer.id,d.colorObject["chart-"+d.reportContainer.id]);
                                sketch
                                    .axisConfig(d.axisConfig)
                                    .data(vm.tableData)
                                    .container(d.reportContainer.id)
                                    .chartConfig(d.chart)
                                    .render(function(redraw){
                                        vm.dashboardEditDraw.loadingBar(variable);
                                    });
                            }
                            if(i==0){
                                vm.Attributes=d.axisConfig;
                            }
                            d.reportContainer.chartType=d.chart.name;
                            /*var width=$("#dashboardContainer_"+d.reportContainer.id).width();
                             var height=$("#dashboardContainer_"+d.reportContainer.id).height();*/
                        }else{
                            if(d.chart.key=="_pivotTable"){
                                sketch.pivotTableConfig=d.chartObject;
                            }
                            //Render chart
                            if(d.chart.key=="_text"){
                                sketch
                                    .container(d.reportContainer.id)
                                    ._text(d.chart.value);
                                vm.dashboardEditDraw.loadingBar();
                            }
                            else if(d.chart.key=="_maleFemaleChartJs"){
                                sketchServer
                                    .accessToken($rootScope.accessToken)
                                    .axisConfig(d.axisConfig)
                                    .data([])
                                    .container(d.reportContainer.id)
                                    .chartConfig(d.chart)
                                    ._maleFemaleChartDraw();
                                vm.dashboardEditDraw.loadingBar(variable);
                            }
                            else if(d.chart.key=="_PivotCustomized"){
                                //alert("pivot");
                                sketchServer
                                    .accessToken($rootScope.accessToken)
                                    .axisConfig(d.axisConfig)
                                    .data([])
                                    .container(d.reportContainer.id)
                                    .chartConfig(d.chart)
                                    .pivotCust_Draw();
                                vm.dashboardEditDraw.loadingBar(variable);
                            }
                            else if(d.chart.key=="_mapChartJs"){
                                sketchServer
                                    .accessToken($rootScope.accessToken)
                                    .axisConfig(d.axisConfig)
                                    .data([])
                                    .container(d.reportContainer.id)
                                    .chartConfig(d.chart)
                                    ._mapChartDraw();
                                vm.dashboardEditDraw.loadingBar(variable);
                            }

                            // else if(d.chart.key=="_pivotTable"){
                            //     sketch.axisConfig(d.axisConfig)
                            //         .data(vm.tableData)
                            //         .container(d.reportContainer.id)
                            //         ._pivotTableDraw();
                            //     vm.dashboardEditDraw.loadingBar(variable);
                            // }

                            else if(d.chart.key=="_floorPlanJs"){
                                sketch.axisConfig(d.axisConfig)
                                    .data(vm.tableData)
                                    .container(d.reportContainer.id)
                                    ._floorPlanDraw();
                                vm.dashboardEditDraw.loadingBar(variable);
                            }
                            else{
                                if(d.colorObject && d.colorObject["chart-"+d.reportContainer.id]){
                                    setTimeout(function () {
                                        eChartServer.chartRegistry.registerPublicViewColor("chart-" + d.reportContainer.id, d.colorObject["chart-" + d.reportContainer.id]);
                                        sketchServer
                                            .accessToken($rootScope.accessToken)
                                            .axisConfig(d.axisConfig)
                                            .data([])
                                            .container(d.reportContainer.id)
                                            .chartConfig(d.chart)
                                            .render(function (redraw) {
                                                vm.dashboardEditDraw.loadingBar(variable);
                                            });
                                    },500);
                                }else{
                                    setTimeout(function () {
                                        sketchServer
                                            .accessToken($rootScope.accessToken)
                                            .axisConfig(d.axisConfig)
                                            .data([])
                                            .container(d.reportContainer.id)
                                            .chartConfig(d.chart)
                                            .render(function (redraw) {
                                                vm.dashboardEditDraw.loadingBar(variable);
                                            });
                                    },500)
                                }
                            }
                            if(i==0){
                                vm.Attributes = d.axisConfig;
                            }
                            d.reportContainer.chartType=d.chart.name;
                            // if(reportCount==i+1){
                            // 	setTimeout(function(){
                            // },4000);
                            // }
                        }
                    });
                },
                checkContainerSize:function(d){
                    if(d.chart.key!="_numberWidget"){
                        //alert("height");
                        var getHeight= ($("#li_"+d.reportContainer.id).height())-97;
                        var getWidth= $("#li_"+d.reportContainer.id).width()-40;
                        $("#chart-"+d.reportContainer.id).height(getHeight);
                        $("#chart-"+d.reportContainer.id).width(getWidth);
                        //document.getElementById("left"+vm.DashboardModel.Dashboard.activeReport.reportContainer.id).style.height = height;
                        var legendHeight=$("#li_"+d.reportContainer.id).height();
                        $("#left"+d.reportContainer.id).height(legendHeight*60/100);
                        //Chart Hide
                        var activeReport = vm.DashboardModel.Dashboard.Report[d.reportContainer.id];
                        if(vm.DashboardModel.Dashboard.activeReport.chart.key!="_mapChartjs"){
                            $("#mapbox-"+d.reportContainer.id).height(getHeight);
                            $("#mapbox-"+d.reportContainer.id).width(getWidth);
                        }
                        $("#chart-" + d.reportContainer.id).show();
                        var variable = "reportLoading_" + d.reportContainer.id;
                        vm[variable] = false;
                        $scope.chartLoading = false;
                        $("#chart-" + d.reportContainer.id).show();
                        $scope.selectedChartInView[d.reportContainer.id] = d.chart;
                    }
                },
                isTimeObjectFound:function(reportArray){
                    var flag=-1;
                    reportArray.forEach(function(d,index){
                        if(d.axisConfig.timeline!=undefined){
                            flag=index;
                        }
                    });
                    return flag;
                },
                filterAddDashboard:function () {
                    //ENd
                    vm.filerTemp = [];
                    vm.filtersToApply = [];
                    vm.filterHSApply = true;

                    if(vm.checkboxModel != null || vm.checkboxModel != undefined){
                        Object.keys(vm.checkboxModel).forEach(function (d) {
                            // columnObj = JSON.parse(vm.checkboxModel[d]);
                            // columnObj = vm.checkboxModel[d];

                            try{
                                var columnObj = JSON.parse(vm.checkboxModel[d]);
                            }catch (e){
                                var columnObj = vm.checkboxModel[d];
                            }

                            var tempObj={};
                            tempObj['key']=columnObj.columnName;
                            tempObj['reName']=columnObj.columnName;
                            tempObj['value']=columnObj.columType;
                            vm.filerTemp.push(tempObj);
                        });
                    }

                    if(!$scope.$$phase) {
                        $scope.$apply();
                    }
                    setTimeout(function(){
                        vm.dashboardEditDraw.filterDargDrop();
                    },2000);
                },
                reportDragDropServer:function(){
                    $('.draggableArea').draggable({
                        cancel : "a.ui-icon", // clicking an icon
                        revert : true, // bounce back when dropped
                        helper : "clone", // create "copy" with
                        cursor : "move",
                        revertDuration : 0
                        // immediate snap
                    });
                    $('.droppableArea').droppable({
                        accept : ".draggableArea",
                        activeClass : "ui-state-highlight",
                        drop : function(event, ui) {
                            sketchServer._totalReportCount++;
                            var div = $(ui.draggable)[0];
                            //Chart type check
                            if($(div).find('input').val()=="Text"){
                                var chartObject={};
                                chartObject['attributes']="";
                                chartObject['id']=0;
                                chartObject['image']=0;
                                chartObject['key']="_text";
                                chartObject['name']="Text";
                                chartObject['required']="Text";
                                chartObject['value']="";
                                vm.leftSideController.addNewContainer(chartObject);
                                if(vm.Attributes.axisConfig==undefined){
                                    $('#dashboardListView').modal({
                                        backdrop: 'static',
                                        keyboard: false
                                    });
                                    $(".label_legends"+vm.DashboardModel.Dashboard.activeReport.reportContainer.id).hide();
                                    setTimeout(function(){
                                        $('#summernote').summernote({
                                            'disableResizeEditor':false,
                                            'height':'250px'
                                        });
                                    },1);
                                }
                            }else{
                                var reportObj=JSON.parse($(div).find('input').val());
                                var axisConfig=Object.assign({},reportObj.axisConfig);
                                var chartObject=Object.assign({},reportObj.chart);
                                var colorObject=Object.assign({},reportObj.colorObject);
                                vm.leftSideController.addNewContainer(chartObject);
                                $scope.$apply();

                                vm.dashboardPrototype.reportContainers[vm.dashboardPrototype.reportContainers.length-1].name=reportObj.reportContainer.name;
                                if(chartObject.key=="_numberWidget" || chartObject.name=="_dataTable"){
                                    $(".label_legends"+vm.DashboardModel.Dashboard.activeReport.reportContainer.id).hide();
                                }else if(chartObject.key=="_pivotTable"){
                                    sketch.pivotTableConfig=reportObj.chartObject;
                                }else{
                                    $(".label_legends"+vm.DashboardModel.Dashboard.activeReport.reportContainer.id).show();
                                }
                                //Change chart color
                                if(colorObject){
                                    eChartServer.chartRegistry.registerPublicViewColor("chart-"+vm.DashboardModel.Dashboard.activeReport.reportContainer.id,colorObject);
                                    var colorObj={};
                                    colorObj["chart-"+vm.DashboardModel.Dashboard.activeReport.reportContainer.id]=colorObject;
                                }
                                //Draw chart in container
                                $scope.$apply();
                                //Draw chart
                                if(chartObject.key!='_numberWidget'){
                                    if(chartObject.containerSize!=undefined){//for old object save code
                                        //vm.DashboardModel.Dashboard.activeReport.reportContainer.sizeX=chartObject.containerSize.x;
                                        //vm.DashboardModel.Dashboard.activeReport.reportContainer.sizeY=chartObject.containerSize.y;
                                    }
                                    $("#chart-" + vm.DashboardModel.Dashboard.activeReport.reportContainer.id).hide();
                                    var getHeight= ($("#li_"+vm.DashboardModel.Dashboard.activeReport.reportContainer.id).height())-97;
                                    var getWidth= $("#li_"+vm.DashboardModel.Dashboard.activeReport.reportContainer.id).width()-40;
                                    $("#chart-"+vm.DashboardModel.Dashboard.activeReport.reportContainer.id).height(getHeight);
                                    $("#chart-"+vm.DashboardModel.Dashboard.activeReport.reportContainer.id).width(getWidth);
                                    $("#chart-" + vm.DashboardModel.Dashboard.activeReport.reportContainer.id).show();
                                }
                                if(chartObject.key=="_maleFemaleChartJs"){
                                    sketchServer
                                        .accessToken($rootScope.accessToken)
                                        .axisConfig(axisConfig)
                                        .data([])
                                        .container(vm.DashboardModel.Dashboard.activeReport.reportContainer.id)
                                        ._maleFemaleChartDraw();
                                }
                                else if(chartObject.key=="_PivotCustomized"){
                                    sketchServer
                                        .accessToken($rootScope.accessToken)
                                        .axisConfig(axisConfig)
                                        .data([])
                                        .container(vm.DashboardModel.Dashboard.activeReport.reportContainer.id)
                                        .pivotCust_Draw();
                                }
                                else if(chartObject.key=="_mapChartJs"){
                                    sketchServer
                                        .accessToken($rootScope.accessToken)
                                        .axisConfig(axisConfig)
                                        .data([])
                                        .container(vm.DashboardModel.Dashboard.activeReport.reportContainer.id)
                                        ._mapChartDraw();
                                    setTimeout(function(){
                                        var getHeight= ($("#li_"+vm.DashboardModel.Dashboard.activeReport.reportContainer.id).height())-97;
                                        var getWidth= $("#li_"+vm.DashboardModel.Dashboard.activeReport.reportContainer.id).width()-30;
                                        $("#mapbox-" + vm.DashboardModel.Dashboard.activeReport.reportContainer.id).height(getHeight);
                                        $("#mapbox-" + vm.DashboardModel.Dashboard.activeReport.reportContainer.id).width(getWidth);
                                    },500);
                                }
                                // else if(chartObject.key=="_pivotTable"){
                                //     sketch.axisConfig(axisConfig)
                                //         .data(vm.tableData)
                                //         .container(vm.DashboardModel.Dashboard.activeReport.reportContainer.id)
                                //         ._pivotTableDraw();
                                // }
                                else if(chartObject.key=="_floorPlanJs"){
                                    sketch.axisConfig(axisConfig)
                                        .data(vm.tableData)
                                        .container(vm.DashboardModel.Dashboard.activeReport.reportContainer.id)
                                        ._floorPlanDraw();
                                }
                                else{
                                    //Check container Size and draw chart
                                    setTimeout(function(){
                                        sketchServer
                                            .accessToken($rootScope.accessToken)
                                            .axisConfig(axisConfig)
                                            .data([])
                                            .container(vm.DashboardModel.Dashboard.activeReport.reportContainer.id)
                                            .chartConfig(chartObject)
                                            .render(function (drawn) {
                                                $("#chart-" + vm.DashboardModel.Dashboard.activeReport.reportContainer.id).show();
                                            });
                                    },1200);
                                    //Check container size make size
                                }
                                var reportIndex;
                                vm.DashboardModel.Dashboard.Report.forEach(function(d,index){
                                    if(d.reportContainer.id==vm.DashboardModel.Dashboard.activeReport.reportContainer.id){
                                        reportIndex=index;
                                    }
                                });
                                vm.DashboardModel.Dashboard.Report[reportIndex].axisConfig=axisConfig;
                                chartObject.id=vm.DashboardModel.Dashboard.activeReport.reportContainer.id;
                                vm.DashboardModel.Dashboard.Report[reportIndex].chart=chartObject;
                                if(colorObj){
                                    vm.DashboardModel.Dashboard.Report[reportIndex]['colorObject']=colorObj;
                                }


                                // vm.DashboardModel.Dashboard.activeReport.reportContainer = reportObj.reportContainer;
                                // vm.DashboardModel.Dashboard.Report[reportIndex].reportContainer = reportObj.reportContainer;
                            }
                        }
                    });
                },
                reportDragDrop:function(){
                    $('.draggableArea').draggable({
                        cancel : "a.ui-icon", // clicking an icon
                        revert : true, // bounce back when dropped
                        helper : "clone", // create "copy" with
                        cursor : "move",
                        revertDuration : 0
                        // immediate snap
                    });
                    $('.droppableArea').droppable({
                        accept : ".draggableArea",
                        activeClass : "ui-state-highlight",
                        drop : function(event, ui) {
                            var div = $(ui.draggable)[0];
                            //Chart type check
                            if($(div).find('input').val()=="Text"){
                                var chartObject={};
                                chartObject['attributes']="";
                                chartObject['id']=0;
                                chartObject['image']=0;
                                chartObject['key']="_text";
                                chartObject['name']="Text";
                                chartObject['required']="Text";
                                chartObject['value']="";
                                vm.leftSideController.addNewContainer(chartObject);
                                if(vm.Attributes.axisConfig==undefined){
                                    $('#dashboardListView').modal({
                                        backdrop: 'static',
                                        keyboard: false
                                    });
                                    $(".label_legends"+vm.DashboardModel.Dashboard.activeReport.reportContainer.id).hide();
                                    setTimeout(function(){
                                        $('#summernote').summernote({
                                            'disableResizeEditor':false,
                                            'height':'250px'
                                        });
                                    },1);
                                }
                            }else{
                                var reportObj=JSON.parse($(div).find('input').val());
                                var axisConfig=Object.assign({},reportObj.axisConfig);
                                var chartObject=Object.assign({},reportObj.chart);
                                var colorObject = Object.assign({},reportObj.colorObject);
                                vm.leftSideController.addNewContainer(chartObject);
                                if(!$scope.$$phase) {
                                    $scope.$apply();
                                }
                                if(vm.reportObject.commonStyle){
                                    var common_Heading = { backgroundColor : vm.reportObject.commonStyle.headerBgcolor, color : vm.reportObject.commonStyle.headerTextcolor};
                                    $(".Common_Head").css(common_Heading);
                                }
                                vm.dashboardPrototype.reportContainers[vm.dashboardPrototype.reportContainers.length-1].name=reportObj.reportContainer.name;
                                if(chartObject.key=="_numberWidget" || chartObject.name=="_dataTable"){
                                    $(".label_legends"+vm.DashboardModel.Dashboard.activeReport.reportContainer.id).hide();
                                }else if(chartObject.key=="_pivotTable"){
                                    sketch.pivotTableConfig=reportObj.chartObject;
                                }else{
                                    $(".label_legends"+vm.DashboardModel.Dashboard.activeReport.reportContainer.id).show();
                                }
                                //Change chart color
                                if(colorObject){
                                    eChart.chartRegistry.registerPublicViewColor("chart-"+vm.DashboardModel.Dashboard.activeReport.reportContainer.id,colorObject);
                                    var colorObj={};
                                    colorObj["chart-"+vm.DashboardModel.Dashboard.activeReport.reportContainer.id]=colorObject;
                                }
                                //Draw chart in container

                                //Draw chart
                                if(chartObject.key!='_numberWidget'){
                                    if(chartObject.containerSize!=undefined){//for old object save code
                                        //vm.DashboardModel.Dashboard.activeReport.reportContainer.sizeX=chartObject.containerSize.x;
                                        //vm.DashboardModel.Dashboard.activeReport.reportContainer.sizeY=chartObject.containerSize.y;
                                    }
                                    $("#chart-" + vm.DashboardModel.Dashboard.activeReport.reportContainer.id).hide();
                                    var getHeight = ($("#li_"+vm.DashboardModel.Dashboard.activeReport.reportContainer.id).height())-97;
                                    var getWidth = $("#li_"+vm.DashboardModel.Dashboard.activeReport.reportContainer.id).width()-40;

                                    $("#chart-"+vm.DashboardModel.Dashboard.activeReport.reportContainer.id).height(getHeight);
                                    $("#chart-"+vm.DashboardModel.Dashboard.activeReport.reportContainer.id).width(getWidth);
                                    $("#chart-" + vm.DashboardModel.Dashboard.activeReport.reportContainer.id).show();
                                }
                                if(chartObject.key=="_maleFemaleChartJs"){
                                    sketch.axisConfig(axisConfig)
                                        .data(vm.tableData)
                                        .container(vm.DashboardModel.Dashboard.activeReport.reportContainer.id)
                                        ._maleFemaleChartDraw();
                                }
                                else if(chartObject.key=="_PivotCustomized"){
                                    sketch.axisConfig(axisConfig)
                                        .data(vm.tableData)
                                        .container(vm.DashboardModel.Dashboard.activeReport.reportContainer.id)
                                        .pivotCust_Draw();
                                }
                                else if(chartObject.key=="_mapChartJs"){
                                    sketch.axisConfig(axisConfig)
                                        .data(vm.tableData)
                                        .container(vm.DashboardModel.Dashboard.activeReport.reportContainer.id)
                                        ._mapChartDraw();
                                    setTimeout(function(){
                                        var getHeight= ($("#li_"+vm.DashboardModel.Dashboard.activeReport.reportContainer.id).height())-97;
                                        var getWidth= $("#li_"+vm.DashboardModel.Dashboard.activeReport.reportContainer.id).width()-30;
                                        $("#mapbox-" + vm.DashboardModel.Dashboard.activeReport.reportContainer.id).height(getHeight);
                                        $("#mapbox-" + vm.DashboardModel.Dashboard.activeReport.reportContainer.id).width(getWidth);
                                    },500);
                                }
                                else if(chartObject.key=="_floorPlanJs"){
                                    sketch.axisConfig(axisConfig)
                                        .data(vm.tableData)
                                        .container(vm.DashboardModel.Dashboard.activeReport.reportContainer.id)
                                        ._floorPlanDraw();
                                }
                                else{
                                    //Check container Size and draw chart
                                    setTimeout(function(){
                                        sketch
                                            .axisConfig(axisConfig)
                                            .data(vm.tableData)
                                            .container(vm.DashboardModel.Dashboard.activeReport.reportContainer.id)
                                            .chartConfig(chartObject)
                                            .render();
                                        $("#chart-" + vm.DashboardModel.Dashboard.activeReport.reportContainer.id).show();
                                    },1200);
                                    //Check container size make size
                                }
                                var reportIndex;
                                vm.DashboardModel.Dashboard.Report.forEach(function(d,index){
                                    if(d.reportContainer.id==vm.DashboardModel.Dashboard.activeReport.reportContainer.id){
                                        reportIndex=index;
                                    }
                                });

                                vm.DashboardModel.Dashboard.Report[reportIndex].axisConfig=axisConfig;
                                vm.DashboardModel.Dashboard.Report[reportIndex].chart=chartObject;
                                if(colorObj){
                                    vm.DashboardModel.Dashboard.Report[reportIndex]['colorObject']=colorObj;
                                }
                            }
                        }
                    });
                },
                filterDargDrop:function(){
                    $('.droppableAddFilter').draggable({
                        cancel : "a.ui-icon", // clicking an icon
                        revert : true, // bounce back when dropped
                        helper : "clone", // create "copy" with
                        cursor : "move",
                        revertDuration : 0
                    });
                    $('.droppableFilter').droppable({
                        accept : ".droppableAddFilter",
                        activeClass : "ui-state-highlight",
                        drop : function(event, ui) {
                            var div = $(ui.draggable)[0];
                            vm.selectedFilter=[];
                            var filter = JSON.parse($(div).find('input').val());
                            vm.selectedFilter.push(filter);
                            vm.dashboardEditDraw.simpleFilterContainerAdd();
                        }
                    });
                },
                simpleFilterContainerAdd:function(){
                    //vm.filtersTo[columnObj.columnName] = [];
                    if(vm.dataCount<$rootScope.localDataLimit){
                        var columnObj = "";
                        Object.keys(vm.selectedFilter).forEach(function (d) {
                            columnObj = vm.selectedFilter[d];
                            var keyValue = columnObj;
                            vm.conatinerFilter.push(keyValue);
                            vm.conatinerFilterTo[columnObj.key] = [];
                            Enumerable.From(vm.tableData)
                                .Distinct(function(x){
                                    return x[columnObj.key];
                                })
                                .Select(function(x){return x[columnObj.key];})
                                .ToArray()
                                .forEach(function (e) {
                                    vm.conatinerFilterTo[columnObj.key].push(e);
                                });
                        });
                        $scope.$apply();
                        setTimeout(function () {
                            setTimeout(function(){
                                $(".filterSelect").selectpicker();
                            },1000);
                            Object.keys(vm.selectedFilter).forEach(function (d) {
                                columnObj= vm.selectedFilter[d];
                                if(columnObj.value == "int" || columnObj.value=="bigint" || columnObj.value == "float" || columnObj.value == "decimal" || columnObj.value == "double") {
                                    var minObject = _.min(vm.tableData, function (o) {
                                        return o[columnObj.key];
                                    });
                                    var maxObject = _.max(vm.tableData, function (o) {
                                        return o[columnObj.key];
                                    });
                                    var min = minObject[columnObj.key];
                                    var max = maxObject[columnObj.key];
                                    var columnObject = {};
                                    columnObject['key'] = columnObj.key;
                                    columnObject['value'] = columnObj.value;
                                    if(min == null) {
                                        min = 0;
                                    }
                                    if(min==max){
                                        min=min-1;
                                    }
                                    var nonLinearSlider = document.getElementById(columnObj.key.replace("(","(").replace(")",")")+ "-Filter");
                                    noUiSlider.create(nonLinearSlider, {
                                        animate: true,
                                        start: [min, max] ,
                                        connect: true,
                                        range: {
                                            min: parseInt(min),
                                            max: parseInt(max)
                                        },
                                        step: 1,
                                        tooltips: [wNumb({
                                            decimals: 0
                                        }), wNumb({
                                            decimals: 0
                                        })]
                                    });
                                    nonLinearSlider.noUiSlider.on('update', function(values, handle) {
                                        sketch.applyFilter(values,columnObject);
                                    });
                                }else if(columnObj.value == "date"){
                                    vm.dashboardEditDraw.simpleDateFilter(columnObj);
                                }else if(columnObj.value == "datetime"){
                                    vm.dashboardEditDraw.simpleDateTimeFilter(columnObj);
                                }
                            });
                        }, 1000);
                    }else{
                        var p=Promise.resolve();
                        eChartServer.resetAll();
                        vm.allFilters={};
                        Object.keys(vm.selectedFilter).forEach(function (d) {
                            var columnObj= vm.selectedFilter[d];
                            var tempObj={};
                            tempObj['columType']=columnObj['value'];
                            tempObj['tableName']='';
                            tempObj['columnName']=columnObj['key'];
                            tempObj['reName']=columnObj['key'];
                            tempObj['dataKey']='';
                            tempObj['type']='';
                            sketchServer.updateFilters(vm.conatinerFilter);
                            p = p.then(new Promise(function(resolve) {
                                var data = {
                                    columnObject: tempObj,
                                    metadataId: vm.metadataId,
                                    sessionId: $rootScope.accessToken
                                };
                                vm.conatinerFilter.push(columnObj);
                                dataFactory.nodeRequest('getUniqueDataFieldsOfColumn','post',data).then(function(data){
                                    var keyValue = columnObj;
                                    vm.conatinerFilterTo[columnObj.key] = [];
                                    vm.allFilters[columnObj.key]={};
                                    vm.allFilters[columnObj.key]['columnObj']=columnObj;
                                    vm.allFilters[columnObj.key]['filterValue']=[];
                                    data.forEach(function(x){
                                        if(columnObj.value!='datetime' && columnObj.value!='date')
                                            vm.allFilters[columnObj.key]['filterValue'].push(x);
                                        vm.conatinerFilterTo[columnObj.key].push(x);
                                    });
                                    $scope.$apply();
                                    setTimeout(function () {

                                        if (columnObj.value == "int" || columnObj.value=="bigint" || columnObj.value == "float" || columnObj.value == "decimal" || columnObj.value == "double") {
                                            var min = Math.min.apply(Math,data);
                                            var max = Math.max.apply(Math,data);
                                            var columnObject = {};
                                            columnObject['key'] = columnObj.key;
                                            columnObject['value'] = columnObj.value;
                                            if (min == null) {
                                                min = 0;
                                            }
                                            if (min == max) {
                                                min = min - 1;
                                            }
                                            var nonLinearSlider = document.getElementById(columnObj.key.replace("(", "(").replace(")", ")") + "-Filter");
                                            noUiSlider.create(nonLinearSlider, {
                                                animate: true,
                                                start: [min, max],
                                                connect: true,
                                                range: {
                                                    min: parseInt(min),
                                                    max: parseInt(max)
                                                },
                                                step: 1,
                                                tooltips: [wNumb({
                                                    decimals: 0
                                                }), wNumb({
                                                    decimals: 0
                                                })]
                                            });
                                            vm.allFilters[columnObject.key]['filterValue']=[min, max];
                                            nonLinearSlider.noUiSlider.on('update', function (values, handle) {
                                                vm.allFilters[columnObject.key]['filterValue']=values;
                                                // sketchServer.applyFilter(values, columnObject,vm.metadataId);
                                            });
                                        } else if(columnObj.value == "date") {
                                            vm.dashboardEditDraw.simpleDateFilter(columnObj);
                                        }else if(columnObj.value == "datetime"){
                                            vm.dashboardEditDraw.simpleDateTimeFilter(columnObj);
                                        }else{
                                            setTimeout(function () {
                                                setTimeout(function () {
                                                    $("[id^=filterSelect_]").each(function () {
                                                        $(this).selectpicker();
                                                    })
                                                }, 1000);
                                            }, 1000);
                                        }
                                    }, 1000);
                                });
                            }));
                        });
                    }
                },
                rangeFilter:function(colName,numberOfColumn){
                    vm.filtersToApply = [];
                    vm.filterHSApply = true;
                    var columnObj = "";
                    var tempObject = {};
                    var ColumnName = colName.columnName;
                    tempObject['key'] = ColumnName;
                    tempObject['period'] = [];
                    setTimeout(function () {
                        for(var i = 0; i < numberOfColumn; i++){
                            var d = new Date();
                            var n = d.getFullYear();
                            var year = n - i;
                            var dateObject = {
                                "start": year+"-01-01",
                                "end": year+"-12-31"
                            };
                            tempObject['period'].push(dateObject);
                            $(".rangeStartFilter" + i).val("01-01-" + year);
                            $(".rangeEndFilter" + i).val("31-12-" + year);
                        }
                        var setDate = new Date();
                        var rangeStartDate = $('.dateTimeLineStart').datepicker({
                            dateFormat: 'dd-mm-yy',
                            onSelect: function(e) {
                                var setIndex = vm.filterColumnRange.index;
                                var startDate = $(rangeStartDate[setIndex]).val();
                                var endDate = $(rangeEndDate[setIndex]).val();
                                var tempStart = startDate; //31-12-2017
                                var tempEnd = endDate;
                                startDate = tempStart.split("-").reverse().join("-");
                                endDate = tempEnd.split("-").reverse().join("-");
                                setRangeFilter(startDate,endDate);
                            }
                        });
                        var rangeEndDate = $('.dateTimeLineEnd').datepicker({
                            dateFormat: 'dd-mm-yy',
                            onSelect: function(e) {
                                var setIndex = vm.filterColumnRange.index;
                                var startDate = $(rangeStartDate[setIndex]).val();
                                var endDate = $(rangeEndDate[setIndex]).val();
                                var tempStart = startDate; //31-12-2017
                                var tempEnd = endDate;
                                startDate = tempStart.split("-").reverse().join("-");
                                endDate = tempEnd.split("-").reverse().join("-");
                                setRangeFilter(startDate,endDate);
                            }
                        });
                        /*rangeStartDate.on("change", function (e) {
                            var setIndex = vm.filterColumnRange.index;
                            var startDate = $(rangeStartDate[setIndex]).val();
                            var endDate = $(rangeEndDate[setIndex]).val();
                            var tempStart = startDate; //31-12-2017
                            var tempEnd = endDate;
                            startDate = tempStart.split("-").reverse().join("-");
                            endDate = tempEnd.split("-").reverse().join("-");
                            setRangeFilter(startDate,endDate);
                        });
                        rangeEndDate.on("change", function (e) {
                            var setIndex = vm.filterColumnRange.index;
                            var startDate = $(rangeStartDate[setIndex]).val();
                            var endDate = $(rangeEndDate[setIndex]).val();
                            var tempStart = startDate; //31-12-2017
                            var tempEnd = endDate;
                            startDate = tempStart.split("-").reverse().join("-");
                            endDate = tempEnd.split("-").reverse().join("-");
                            setRangeFilter(startDate,endDate);
                        });*/
                        function setRangeFilter(startdate,enddate) {
                            var date = {};
                            date['start'] = (moment(startdate)).format('YYYY-MM-DD');
                            date['end'] = (moment(enddate)).format('YYYY-MM-DD');
                            var columnName = $scope.filterColumnRange.columnName;
                            var index = $scope.filterColumnRange.index;
                            vm.rangeObject[columnName].period[index] = date;
                            if (!$.isEmptyObject(vm.rangeObject)) {
                                vm.Attributes['timeline'] = vm.rangeObject;
                                var timelineObj = vm.rangeObject;
                                sketch.getTimeLineFilteredData(vm.tableData, vm.rangeObject, function (data) {
                                    vm.filteredData = data;
                                    vm.DashboardModel.Dashboard.Report.forEach(function (d) {
                                        vm.Attributes = d.axisConfig;
                                        d.axisConfig['timeline'] == vm.rangeObject;
                                        sketch.axisConfig(d.axisConfig)
                                            .data(vm.tableData)
                                            .container(d.reportContainer.id)
                                            .chartConfig(d.chart)
                                            .render();
                                    });
                                });
                            }
                        }
                    }, 1000);
                },
                simpleDateFilter:function(columnObj){
                    // var columnObj = "";
                    setTimeout(function(){
                        //var colName = vm.rangeObject;
                        //var totalCol = (colName[Object.keys(colName)[0]].period).length;
                        var today = new Date();
                        var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
                        var time = today.getHours() + ":" + today.getMinutes() + ":" + "00";
                        var CurrentDateTime = date+' '+time;
                        vm.allFilters[columnObj.key]['columnObj']=columnObj;
                        vm.allFilters[columnObj.key]['filterValue']={};
                        vm.allFilters[columnObj.key]['filterValue']['start']=CurrentDateTime;
                        vm.allFilters[columnObj.key]['filterValue']['end']=CurrentDateTime;
                        var totalCol = 2;
                        for(var i = 0; i < totalCol-1; i++){
                            var d = new Date();
                            var n = d.getFullYear();
                            var year = n - i;
                            $(".startdate").val("01-01-" + year);
                            $(".enddate").val("31-12-" + year);
                        }
                        var setDate = new Date();
                        var rangeStartDate = $('.startdate').datepicker({
                            dateFormat: 'dd-mm-yy',
                            onSelect: function(e) {
                                var d = new Date(e);
                                var tempIndex = (rangeEndDate.length) - 1;
                                var endDate = $(rangeStartDate[tempIndex]).val();
                                var startDate = e.substr(6) + e.substr(2,4) + e.substr(0,2);
                                var temp = endDate; //31-12-2017
                                endDate = temp.split("-").reverse().join("-");
                                vm.allFilters[$scope.filterColumnName.key]['filterValue']['start']=startDate;
                                //setDateFilter(startDate,endDate);
                            }
                        });
                        var rangeEndDate = $('.enddate').datepicker({
                            dateFormat: 'dd-mm-yy',
                            onSelect: function(e) {
                                var d = new Date(e);
                                var tempIndex = (rangeEndDate.length) - 1;
                                var startDate = $(rangeStartDate[tempIndex]).val();
                                var endDate = e.substr(6) + e.substr(2,4) + e.substr(0,2);
                                var temp = startDate; //31-12-2017
                                startDate = temp.split("-").reverse().join("-");
                                vm.allFilters[$scope.filterColumnName.key]['filterValue']['end']=endDate;
                                //setDateFilter(startDate,endDate);
                            }
                        });
                        function setDateFilter(startdate, enddate) {
                            var date = {};
                            date['start'] = startdate;
                            date['end'] = enddate;
                            var columnName = $scope.filterColumnName;
                            if(vm.dataCount<$rootScope.localDataLimit) {
                                sketch.applyFilter(date, columnName);
                            }else{
                                /*sketchServer.applyFilter(date, columnName,vm.metadataId,function(data,filterObj,status){
                                    if(status){
                                        $('.filterSelect').selectpicker('destroy', true);
                                        vm.allFilters[filterObj.key]['filterValue']=[];
                                        vm.conatinerFilterTo[filterObj.key]=data;
                                        vm.allFilters[filterObj.key]['filterValue']=data;
                                        $scope.$apply();
                                        setTimeout(function(){
                                            $('.filterSelect').selectpicker();
                                            $(".loadingBar").hide();
                                        },1000);
                                    }
                                });*/
                            }

                        }
                    }, 200);
                },
                simpleDateTimeFilter:function(columnObj){
                    setTimeout(function(){
                        var today = new Date();
                        var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
                        var time = today.getHours() + ":" + today.getMinutes() + ":" + "00";
                        var CurrentDateTime = date+' '+time;
                        vm.allFilters[columnObj.key]['columnObj']=columnObj;
                        vm.allFilters[columnObj.key]['filterValue']={};
                        vm.allFilters[columnObj.key]['filterValue']['start']=CurrentDateTime;
                        vm.allFilters[columnObj.key]['filterValue']['end']=CurrentDateTime;
                        var start=$('.startdatetime').datetimepicker({
                            defaultDate: new Date(),
                            format : "DD-MM-YYYY HH:mm:ss"
                        }).on('dp.change', function (e) {
                            var d = new Date(e.date);
                            var selectedDate = new Date(d);
                            selectedDate = selectedDate.getDate() + "-" +
                                ("0" + (selectedDate.getMonth() + 1)).slice(-2) + "-" +
                                ("0" + selectedDate.getFullYear()).slice(-4) + " " + ("0" + selectedDate.getHours()).slice(-2) + ":" +
                                ("0" + selectedDate.getMinutes()).slice(-2)+ ":" +
                                ("0" + selectedDate.getSeconds()).slice(-2);
                            var tempIndex = (start.length) - 1;
                            var startDate = selectedDate;
                            var endDate = $(end[tempIndex]).val();
                            var startDateArr=startDate.split(" ");
                            var tempStartDate = startDateArr[0]; //31-12-2017
                            var tempStartTime = startDateArr[1];
                            var endDateArr=endDate.split(" ");
                            var tempEndDate = endDateArr[0]; //31-12-2017
                            var tempEndTime = endDateArr[1];
                            startDate = tempStartDate.split("-").reverse().join("-");
                            endDate = tempEndDate.split("-").reverse().join("-");
                            vm.allFilters[$scope.filterColumnName.key]['filterValue']['start']=startDate+" "+tempStartTime;
                            setTimeChangeServer(startDate+" "+tempStartTime,endDate+" "+tempEndTime);
                        });
                        var end=$('.enddatetime').datetimepicker({
                            defaultDate: new Date(),
                            format : "DD-MM-YYYY HH:mm:ss"
                        }).on('dp.change', function (e) {
                            var d = new Date(e.date);
                            var selectedDate = new Date(d);
                            selectedDate = selectedDate.getDate() + "-" +
                                ("0" + (selectedDate.getMonth() + 1)).slice(-2) + "-" +
                                ("0" + selectedDate.getFullYear()).slice(-4) + " " + ("0" + selectedDate.getHours()).slice(-2) + ":" +
                                ("0" + selectedDate.getMinutes()).slice(-2)+ ":" +
                                ("0" + selectedDate.getSeconds()).slice(-2);
                            var tempIndex = (start.length) - 1;
                            var startDate = $(start[tempIndex]).val();
                            var endDate = selectedDate;
                            var startDateArr=startDate.split(" ");
                            var tempStartDate = startDateArr[0]; //31-12-2017
                            var tempStartTime = startDateArr[1];
                            var endDateArr=endDate.split(" ");
                            var tempEndDate = endDateArr[0]; //31-12-2017
                            var tempEndTime = endDateArr[1];
                            startDate = tempStartDate.split("-").reverse().join("-");
                            endDate = tempEndDate.split("-").reverse().join("-");
                            vm.allFilters[$scope.filterColumnName.key]['filterValue']['end']=endDate+" "+tempEndTime;
                            setTimeChangeServer(startDate+" "+tempStartTime,endDate+" "+tempEndTime);
                        });
                        function setTimeChangeServer(startdate, enddate) {
                            var date = {};
                            date['start'] = startdate;
                            date['end'] = enddate;
                            // var columnName = $scope.filterColumnName.key;
                            var columnName = $scope.filterColumnName;
                            if(vm.dataCount<$rootScope.localDataLimit) {
                                sketch.applyFilter(date, columnName);
                            }else{
                                /*sketchServer.applyFilter(date, columnName,vm.metadataId,function(data,filterObj,status){
                                    if(status){
                                        $('.filterSelect').selectpicker('destroy', true);
                                        vm.allFilters[filterObj.key]['filterValue']=[];
                                        vm.conatinerFilterTo[filterObj.key]=data;
                                        vm.allFilters[filterObj.key]['filterValue']=data;
                                        $scope.$apply();
                                        setTimeout(function(){
                                            $('.filterSelect').selectpicker();
                                            $(".loadingBar").hide();
                                        },1000);
                                    }
                                });*/
                            }
                        }
                    }, 200);
                }
            }
            vm.dashboardEditDraw.init();
            /*
              For all tab function
             */
            vm.addViewTab = function(){
                vm.sharedViewTabName = {};
                vm.sharedViewTabName['name'] = '';
                $('#addSharedViewTab').modal('show');
            }

            vm.saveAddSharedViewTab = function(obj){
                var tempObj = {};
                tempObj['key'] = vm.tabs[vm.tabs.length-1].key+1;
                tempObj['name'] = obj.name;
                if(obj.name.length){
                    vm.tabs.push(tempObj);
                }else{
                    dataFactory.errorAlert("Tab Name Can't Be Empty");
                }
                $('#addSharedViewTab').modal('hide');
                setTimeout(function(){
                    vm.dashboardEditDraw.reportDragDropServer();
                },1000);
            }

            vm.editDeleteViewTab = function(){
                $('#EditDeleteSharedViewTab').modal('show');
            }

            vm.EditKeyIndex = false;
            vm.editSharedViewTab = function(index, key){
                vm.tabKeyName = key;
                vm.EditKeyName = true;
                vm.EditKeyIndex = index;
            }
            vm.saveSharedViewTab = function(index,key){
                var newTabName = $('#tabEditName_' + index).val();
                vm.tabs.forEach(function(d,i){
                    if(index == i){
                        d.name = newTabName;
                    }
                });
                vm.EditKeyName = false;
            }
            vm.deleteSharedViewTab = function(index,key){
                var tabKey = vm.tabs[index].key;
                delete $scope.dashboardPrototype.reportWithTab[tabKey];
                vm.DashboardModel.Dashboard.Report.forEach(function(d,indx){
                    if(d.reportContainer.tab==tabKey){
                        vm.DashboardModel.Dashboard.Report.splice(indx,1);
                    }
                });
                vm.tabs.splice(index,1);
            }
            vm.reorderSharedViewTab = function(){
                $('#ReorderSharedViewTab').modal('show');
                $(".sortable").sortable({
                    cancel: ".fixed",
                    start: function(event, ui) {
                        ui.item.startPos = ui.item.index();
                    },
                    stop : function(event, ui){
                        var startIndex = ui.item.startPos;
                        var endIndex = ui.item.index();
                        var sortedArr = $(this).sortable("toArray");
                        sortedArr.forEach(function (data,index) {
                            if(startIndex == index){
                                vm.tabs.forEach(function(d,i){
                                    if(index == i){
                                        d.name = data;
                                    }
                                });
                            }
                            if(endIndex == index){
                                vm.tabs.forEach(function(d,i){
                                    if(index == i){
                                        d.name = data;
                                    }
                                });
                            }
                        });
                    }
                });
            }
            vm.saveReorderSharedViewTab = function(){
                $('#ReorderSharedViewTab').modal('hide');
            }
            vm.allFilterApply=function(){
                $(".loadingBar").show();
                sketchServer.applyAllFilter(vm.allFilters,vm.metadataId);
                vm.filterModalClose();
            }
            vm.filterModalClose=function () {
                $scope.filterApply=false;
                $(window).trigger('resize');
            }
            vm.cascadeFilter=function(date,columnName,index){
                date=date['filterValue'];
                $(".loadingBar").show();
                var totalLength=Object.keys(vm.allFilters).length;
                sketchServer.applyFilter(date, columnName, vm.metadataId,totalLength,index,function(data,filterObj,status){
                    if (status) {
                        try{

                            // vm.allFilters[filterObj.key]['filterValue']=data;
                            if(filterObj.value=="varchar" || filterObj.value=="char" || filterObj.value=="text"){
                                vm.filtersTo[filterObj.key] = data;
                                vm.allFilters[filterObj.key].filterValue = data;
                                $scope.$apply();
                                $("[id^=filterSelect_]").each(function () {
                                    if($(this).attr('id')!="filterSelect_"+index) {
                                        $(this).selectpicker('destroy', true);
                                    }
                                })
                                setTimeout(function () {
                                    $("[id^=filterSelect_]").each(function () {
                                        if($(this).attr('id')!="filterSelect_"+index){
                                            $(this).selectpicker();
                                        }
                                    });
                                    $(".loadingBar").hide();
                                }, 1000);
                            }else if(filterObj.value=="decimal" || filterObj.value=="int" || filterObj.value=="bigint" || filterObj.value=="double" || filterObj.value=="float"){
                                /*
                                   Range filter
                                */
                                setTimeout(function () {
                                    var min = Math.min.apply(Math,data);
                                    var max = Math.max.apply(Math,data);
                                    if(min && max){
                                        if (min == null) {
                                            min = 0;
                                        }
                                        if(min==max){
                                            max=max+1;
                                        }
                                        var config = {
                                            orientation: "horizontal",
                                            start: [min,max],
                                            range: {
                                                min: min,
                                                max: max,
                                            },
                                            connect: 'lower',
                                            direction: "ltr",
                                            step: 10,
                                        };
                                        var nonLinearSlider = document.getElementById(filterObj.key.replace("(","(").replace(")",")")+ "-Filter");
                                        document.getElementById(filterObj.key.replace("(","(").replace(")",")")+ "-Filter").noUiSlider.destroy();
                                        noUiSlider.create(nonLinearSlider, {
                                            animate: true,
                                            start: [min, max],
                                            connect: true,
                                            range: {
                                                min: parseInt(min),
                                                max: parseInt(max)
                                            },
                                            step: 1,
                                            tooltips: [wNumb({
                                                decimals: 0
                                            }), wNumb({
                                                decimals: 0
                                            })]
                                        });
                                        vm.allFilters[filterObj.key]['filterValue'] = [min, max];
                                        nonLinearSlider.noUiSlider.on('update', function (values, handle) {
                                            vm.allFilters[filterObj.key]['filterValue'] = values;

                                        });
                                        nonLinearSlider.noUiSlider.on('change', function (values, handle) {
                                            vm.filterBy(values,filter);
                                        });
                                    }
                                    $(".loadingBar").hide();
                                },1000);
                                /*
                                  End range filter
                                 */
                            }
                        }catch(e){

                        }
                    }else{
                        setTimeout(function () {
                            $(".loadingBar").hide();
                        }, 1000);
                    }
                });
            }









        }]);