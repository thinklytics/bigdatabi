'use strict';
/* Controllers */
angular.module('app', ['ngSanitize', 'gridster'])
    .controller('publicViewSnap', ['$scope', '$sce', 'dataFactory', '$filter', '$stateParams', '$location', '$window', '$rootScope','$cookieStore',
        function ($scope, $sce, dataFactory, $filter, $stateParams, $location, $window, $rootScope,$cookieStore) {
            // Select Box start
            //--- Top Menu Bar
            //--- Top Menu Bar  start
            $(".loadingBar").show();
            $("#chartjs-tooltip").show();
            //End Menu
            //Default Tab
            var vm = $scope;
            $scope.tab=false;
            //End
            var auth_key = $stateParams.auth_key;
            vm.tabKeyActive = $stateParams.tab;
            var comId = $stateParams.comId;
            var userPort = $stateParams.userPort;
            $cookieStore.put('companyId',comId);
            $cookieStore.put('userPort',parseInt(userPort));
            // Select Box start
            $scope.trustAsHtml = function(value) {
                return $sce.trustAsHtml(value);
            };
            var data={};
            var addClass="";
            var viewClass="";
            /*$(window).bind("resize", resizeWindow);
             function resizeWindow() {
             window.location.reload(true);
             };*/

            if($location.path()=='/dashboard/'){
                viewClass='activeli';
            }else{
                addClass='activeli';
            }
            data['navTitle']="SHARED VIEW";
            data['icon']="fa fa-line-chart";
            data['subNavBarMenu']=false;
            data['navigation']=[
                {
                    "name":"Add",
                    "url":"#/dashboard/add",
                    "class":addClass
                },
                {
                    "name":"View",
                    "url":"#/dashboard/",
                    "class":viewClass
                },
                {
                    "name":"Close",
                    "url":"#/dashboard/",
                    "class":viewClass
                }
            ];
            $rootScope.$broadcast('subNavBar', data);

            vm.selectedChart = {};
            vm.customGroup = [];
            vm.activeXLabel = {};
            vm.activeYLabel = {};
            vm.aggregateText = {};
            vm.showInfo2 = false;
            vm.showInfo1 = true;
            vm.conatinerFilter=[];
            vm.conatinerFilterTo=[];
            sketch=sketch.reset();
            sketchServer=sketchServer.reset();
            $scope.columnSearch = "";
            vm.fixedDropdown=function(e){
                e.stopPropagation();
            }
            $(".full-height").addClass("full-height-background");
            // vm.AggregateFunctions=[];
            vm.tableColumns = [];
            $scope.dashName = {};
            $scope.lineBar = {};
            $scope.lastDrawnChart = {};
            $scope.selectedChartInView = {};
            $scope.filterApply=false;
            var userId=$cookieStore.get('userId');


            vm.filteredData = [];
            // advanced chart settings
            vm.advancedChartSettingModal = {
                dateFormats: sketch._dateFormats.formats,
                groups: sketch._dateFormats.groups,
                Categories: [{}]
            }
            var locationUrl=$location.path().split("/");

            //http://113.193.127.177/demo2_thinklytics_io/bi/#/publicView/show/A2qmgM7YkKQzccLwzDuskLe96iyvJw7p/1/1/COM101/3002
            if(/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
                if(locationUrl[2]!='mobileView'){

                  //  $location.path('/publicView/mobileView/'+auth_key+'/'+comId+'/'+userPort+'/1');
                }
            }else{
                if(locationUrl[2]!='show'){
                   // $location.path('/publicView/show/'+auth_key+'/'+comId+'/'+userPort+'/1');
                }
            }
            /*if($location.path()!=""){
             var urlAllow="";
             if(locationUrl[1]=="sharedView" && locationUrl[2]=="show")
             urlAllow="/"+locationUrl[1]+"/"+locationUrl[2];*/
            vm.advancedChartSettingController = {
                init: function () {
                    vm.advancedChartSettingView.init();
                    vm.advancedChartSettingView.render();
                },
                fetchDateFormats: function () {
                    return vm.advancedChartSettingModal.dateFormats;
                },
                fetchGroupFormats: function () {
                    return vm.advancedChartSettingModal.groups;
                },
                getDimensionalData: function () {
                    // sketch._
                },
                addCategoryContainer: function () {
                    vm.categoryContainers.push({});

                },
                updateTableData: function (categoryData) {
                    var keys = Object.keys(categoryData.selectedItems);
                    var obj = {};
                    var column = categoryData.column;
                    keys.forEach(function (index) {
                        categoryData.selectedItems[index].forEach(function (d) {
                            obj[d] = categoryData.categoryName[index];
                        });
                    });
                    var newColumnName = categoryData.Name;
                    var updatedData = null;
                    vm.tableData.forEach(function (d) {
                        if (obj[d[column]]) {
                            d[newColumnName] = obj[d[column]];
                        } else {
                            d[newColumnName] = d[column];
                        }
                    });
                }
            }
            vm.advancedChartSettingView = {
                init: function () {
                    vm.categories = {};
                    vm.categoryContainers = [];
                    vm.addCategoryContainer = vm.advancedChartSettingController.addCategoryContainer;
                    vm.saveCategories = vm.advancedChartSettingController.saveCategories;
                    vm.removeCategory = vm.advancedChartSettingController.removeCategories;
                },
                render: function () {
                    vm.dateFormatList = vm.advancedChartSettingController.fetchDateFormats();
                    vm.dateGroupList = vm.advancedChartSettingController.fetchGroupFormats();
                }
            }


            $scope.$watch('tableColumns',function(model){
                sketch.columnsByColumnName={};
                model.forEach(function(d){
                    sketch.columnsByColumnName[d.columnName]=d;
                });
            });
            vm.advancedChartSettingController.init();
            // .................................................................................................
            vm.leftSideModel = {
                chartTypeData: sketch.chartData,
                aggregateData: sketch.AggregatesObject
            };
            vm.leftSideController = {
                init: function () {
                    //Number widget setting
                    vm.numberStyle={};
                    // Comapny Setting
                    vm.companySet={};
                    vm.company={};
                    vm.company.logoWidth="50px";
                    vm.companyHeaderSet = function(){
                        vm.companySet.name=vm.company.name;
                        vm.companySet.description=vm.company.description;
                        vm.companySet.log=vm.company.log;
                        if(vm.company.logoWidth!="50px")
                            vm.companySet.logoWidth="50px";
                        $('#companyHeaderSet').modal('show');
                    }
                    vm.companyHeaderSave=function(company){
                        vm.company.name=company.name;
                        vm.company.description=company.description;
                        vm.company.logoWidth=company.logoWidth;
                        vm.company.log=company.logo;
                        $('#companyHeaderSet').modal('hide');
                    }
                    //Reset Chart
                    vm.resetChart=function(){
                        if(vm.dataCount<$rootScope.localDataLimit){
                            eChart.resetAll();
                            dataFactory.successAlert("Chart reset successfully");
                        }else{
                            eChartServer.resetAll();
                            dataFactory.successAlert("Chart reset successfully");
                        }
                    }


// Export Tab PDF Reports Start
                    vm.Tab_PDFAllCheckBox = function(){
                        var chkBoxVal = $('.allTabPdfCheckBox').is(':checked');
                        if(chkBoxVal){
                            $('.tabPDFCheckBox').prop('checked', true);
                        }else{
                            $('.tabPDFCheckBox').prop('checked', false);
                        }
                    }

                    vm.TabPDFCheck = function(pdfReport){
                        var chkBoxVal = $('#PDFtab_' + pdfReport.id).is(':checked');
                        var selectedReportName = [], unSelectedReportName = [];
                        vm.tab_PDF_Report.forEach(function(d){
                            var chkBoxVal = $('#PDFtab_' + d.id).is(':checked');
                            if(chkBoxVal){
                                selectedReportName.push(d);
                            }else{
                                unSelectedReportName.push(d)
                            }
                        });
                        if(unSelectedReportName.length == 0){
                            $('.allTabPdfCheckBox').prop('checked', true);
                        }else{
                            $('.allTabPdfCheckBox').prop('checked', false);
                        }
                    }

                    vm.download_TabPDF = function(data){
                        vm.tabPDFReportClass = true;
                        $(".loadingBar").show();

                        $('.resetMeasureBtn').css('display', 'none');
                        $('.filterBtn').css('display', 'none');
                        $('.exportViewBtn').css('display', 'none');
                        // $('#filterContainer').css('margin-top', '1%');
                        $('input').removeClass('form-control');
                        $('div').removeClass('input-group date');
                        $('.startdatetime').css('border', 'none');
                        $('.enddatetime').css('border', 'none');
                        $('div').removeClass('form-material');
                        $('button').removeClass('select-with-transition');
                        if(vm.conatinerFilter == 0){
                            $('#filterContainer').css('margin-top', '2%');
                        }else{
                            $('#filterContainer').css('margin-top', '1%');
                        }

                        var chkBoxVal = $('.allTabPdfCheckBox').is(':checked');

                        if(chkBoxVal){
                            setTimeout(function () {
                                var scaleBy = 5;
                                var div = document.querySelector('#sharedViewImage');
                                var canvas = document.createElement('canvas');
                                var context = canvas.getContext('2d');
                                context.scale(scaleBy, scaleBy);
                                html2canvas($('#sharedViewImage'), {
                                    scale: 2,
                                    dpi: 144,
                                    background: '#eee',
                                    width: $(document).width(),
                                    useCORS: true,
                                    onrendered: function(canvas) {
                                        var imgData = canvas.toDataURL("image/jpeg");
                                        var im = new Image;
                                        im.src = imgData;
                                        setTimeout(function () {
                                            var width = im.width;
                                            var height = im.height;
                                            if(height>1500){
                                                var doc = new jsPDF('p', 'mm', [width, height]);
                                                doc.addImage(imgData, 'jpeg', 0, 0, width, height);
                                            }else{
                                                var doc = new jsPDF('l', 'mm', [width, height]);
                                                doc.addImage(imgData, 'jpeg', 0, 0, width, height);
                                            }
                                            var pdfName = vm.dashName.dashboardName + '-' + data.name + '.pdf';
                                            doc.save(pdfName);
                                            $(".loadingBar").hide();
                                            vm.tabPDFReportClass = false;
                                            $scope.$apply();
                                            $('.resetMeasureBtn').css('display', 'inline');
                                            $('.filterBtn').css('display', 'block');
                                            $('.exportViewBtn').css('display', 'block');
                                            $('#filterContainer').css('margin-top', '4%');
                                            $('input').addClass('form-control');
                                            $('.startdatetime').css('border', 'block');
                                            $('.enddatetime').css('border', 'block');
                                        },500);
                                    }
                                });
                            }, 1000);
                        }else{
                            $('.dashboardName').css('text-align', 'left');
                            $('.dashboardName').css('margin-left', '25%');
                            var selectedReportArr = [], selectedImgDataArr = [];
                            vm.tab_PDF_Report.forEach(function(d){
                                var chkBoxVal = $('#PDFtab_' + d.id).is(':checked');
                                if(chkBoxVal){
                                    selectedReportArr.push(d);
                                }
                            });
                            if(selectedReportArr.length == 1){
                                var imgRender_Header_Promise, imgRender_Report_Promise;
                                imgRender_Header_Promise = new Promise(function(resolve, reject){
                                    (function(){
                                        html2canvas( $("#ViewHeader"), {
                                            background: '#FFFFFF',
                                            useCORS: true,
                                            onrendered: function(canvas) {
                                                var imgData = canvas.toDataURL("image/jpeg");
                                                var ImgObj = {};
                                                ImgObj['Type'] = 'Header';
                                                ImgObj['Img'] = imgData;
                                                ImgObj['ReportName'] = '';
                                                selectedImgDataArr.push(ImgObj);
                                                resolve();
                                            }
                                        });
                                    })();
                                });
                                imgRender_Header_Promise.then(function(){
                                    imgRender_Report_Promise = new Promise(function(resolve, reject){
                                        selectedReportArr.forEach(function(d){
                                            if(d.chartType == 'Map'){
                                                var imgCounter = 0;
                                                (function(){
                                                    html2canvas( $("#ReportHeader_" + d.id), {
                                                        background: '#FFFFFF',
                                                        useCORS: true,
                                                        onrendered: function(canvas) {
                                                            var imgData = canvas.toDataURL("image/jpeg");
                                                            var ImgObj = {};
                                                            ImgObj['Type'] = 'Map Report Header';
                                                            ImgObj['Img'] = imgData;
                                                            ImgObj['ReportName'] = d.name;
                                                            selectedImgDataArr.push(ImgObj);
                                                            setTimeout(function(){
                                                                $.each(sketchServer._MapImgObject, function(k,v){
                                                                    var ImgObj = {};
                                                                    ImgObj['Type'] = d.chartType;
                                                                    ImgObj['Img'] = v;
                                                                    ImgObj['ReportName'] = d.name;
                                                                    selectedImgDataArr.push(ImgObj);
                                                                    resolve();
                                                                });
                                                            }, 500);
                                                        }
                                                    });
                                                })();
                                            }else{
                                                (function(){
                                                    var scaleBy = 5;
                                                    var div = document.querySelector('#sharedViewImage');
                                                    var canvas = document.createElement('canvas');
                                                    var context = canvas.getContext('2d');
                                                    context.scale(scaleBy, scaleBy);
                                                    html2canvas($('#li_' + d.id), {
                                                        background: '#FFFFFF',
                                                        useCORS: true,
                                                        onrendered: function(canvas) {
                                                            var imgData = canvas.toDataURL("image/jpeg");
                                                            var ImgObj = {};
                                                            ImgObj['Type'] = d.chartType;
                                                            ImgObj['Img'] = imgData;
                                                            ImgObj['ReportName'] = d.name;
                                                            selectedImgDataArr.push(ImgObj);
                                                            resolve();
                                                        }
                                                    });
                                                })();
                                            }
                                        });
                                    });
                                    imgRender_Report_Promise.then(function(){
                                        var doc = new jsPDF('l', 'mm', 'a4');
                                        var ReportName = '';
                                        selectedImgDataArr.forEach(function (d,index){
                                            var x = 0, y = 0, height = 0, width = 0;
                                            if(index == 0){
                                                x = 0;
                                                y = 0;
                                                height = 15;
                                                width = 500;
                                            }else if(index == 1){
                                                x = 0;
                                                y = 16;
                                                if(d.Type == 'Number Widget'){
                                                    height = 40;
                                                    width = 130;
                                                }else if(d.Type == 'Pie Chart'){
                                                    height = 150;
                                                    width = 200;
                                                }else if(d.Type == 'SpeedoMeter'){
                                                    height = 100;
                                                    width = 200;
                                                }else if(d.Type == 'Map Report Header'){
                                                    height = 10;
                                                    width = 300;
                                                }else{
                                                    height = 150;
                                                    width = 200;
                                                }
                                                ReportName = d.ReportName;
                                            }else if(index == 2){
                                                if(d.Type == 'Map') {
                                                    y = 27;
                                                    height = 100;
                                                    width = 220;
                                                    // height = 150;
                                                    // width = 300;
                                                }
                                            }
                                            doc.addImage(d.Img, 'jpeg', x, y, width, height);
                                        });
                                        var pdfName = vm.dashName.dashboardName + ' - ' + ReportName + '.pdf';
                                        doc.save(pdfName);

                                        $('.dashboardName').css('text-align', 'center');
                                        $('.dashboardName').css('margin-left', '0px');
                                        $(".loadingBar").hide();
                                        vm.tabPDFReportClass = false;
                                        $('.resetMeasureBtn').css('display', 'inline');
                                        $('.filterBtn').css('display', 'block');
                                        $('.exportViewBtn').css('display', 'block');
                                        $('#filterContainer').css('margin-top', '4%');
                                        $('input').addClass('form-control');
                                        $('.startdatetime').css('border', 'block');
                                        $('.enddatetime').css('border', 'block');
                                    });
                                });
                            }else{
                                $('.dashboardName').css('text-align', 'center');
                                $('.dashboardName').css('margin-left', '0px');
                                $(".loadingBar").hide();
                                vm.tabPDFReportClass = false;
                                $('.resetMeasureBtn').css('display', 'inline');
                                $('.filterBtn').css('display', 'block');
                                $('.exportViewBtn').css('display', 'block');
                                $('#filterContainer').css('margin-top', '4%');
                                $('input').addClass('form-control');
                                $('.startdatetime').css('border', 'block');
                                $('.enddatetime').css('border', 'block');

                                dataFactory.errorAlert("Select One or All Reports");
                            }
                        }
                    }
// Export Tab PDF Reports End





// Export Tab Excel Reports Start
                    vm.download_TabExcel = function(report){
                        $(".loadingBar").show();
                        var chartID = '';
                        if(report.chartType == "DataTable"){
                            $("#table-chart-" + report.id).attr('border', '1px');
                            chartID = 'chart-' + report.id;
                        }else if(report.chartType == "Pivot Table Customized"){
                            $("#pt_Col_Table_" + report.id).attr('border', '1px');
                            $("#pt_Row_Table_" + report.id).attr('border', '1px');
                            $("#pt_Data_Table_" + report.id).attr('border', '1px');
                            $("#pivot_Data_Table_" + report.id).attr('border', '1px');
                            $("#pivot_MainTable_" + report.id).css('border', '1px');
                            $("#pivot_MainTable_" + report.id).attr('border', '1px');

                            $("#pivot_MainTable_" + report.id).css('border-top', '2px');
                            $("#pivot_MainTable_" + report.id).css('border-left', '2px');
                            $("#pivot_MainTable_" + report.id).attr('border-right', '1px');
                            $("#pivot_MainTable_" + report.id).attr('border-bottom', '1px');

                            $("#pt_Header_Table_" + report.id).attr('border-top', '1px solid black');
                            $("#pt_Row_Table_" + report.id).attr('border-bottom', '1px solid black');
                            $("#pt_Data_Table_" + report.id).attr('border-bottom', '1px solid black');
                            $("#pt_Data_Table_" + report.id).attr('border-top', '1px solid black');
                            $("#pivot_Data_Table_" + report.id).attr('border-top', '1px solid black');
                            chartID = 'pivot_MainTable_' + report.id;

                            var rowObj = {}, colObj = {};
                            $('#pt_Row_Header_Table_' + report.id + ' td').each(function() {
                                var spanID = $(this)[0].childNodes[0].id;
                                var spanVal = spanID.split('_-_')[1];
                                $("[id='" + spanID + "']").text(spanVal);
                            });
                            $('#pt_Col_Header_Table_' + report.id + ' td').each(function() {
                                var spanID = $(this)[0].childNodes[0].id;
                                var spanVal = spanID.split('_-_')[1];
                                $("[id='" + spanID + "']").text(spanVal);
                            });
                        }

                        var a = document.createElement('a');
                        var data_type = 'data:application/vnd.ms-excel';
                        var table_div = document.getElementById(chartID);
                        var table_html = table_div.outerHTML.replace(/ /g, '%20');
                        a.href = data_type + ', ' + table_html;
                        var excelName = vm.dashName.dashboardName + ' - ' + report.name + '.xls';
                        a.download = excelName;
                        a.click();

                        $(window).trigger('resize');
                        $window.location.reload();

                        setTimeout(function(){

                        }, 1000);

                        $(".loadingBar").hide();
                    }
// Export Tab Excel Reports End






// Export View PDF start
                    vm.pdf_AllCheckBox = function(){
                        var chkBoxVal = $('.allPdfCheckBox').is(':checked');
                        if(chkBoxVal){
                            $('.pdfCheckBox').prop('checked', true);
                        }else{
                            $('.pdfCheckBox').prop('checked', false);
                        }
                    }

                    vm.pdfCheck = function(pdfReport){
                        var chkBoxVal = $('#' + pdfReport.id).is(':checked');
                        var selectedReportName = [], unSelectedReportName = [];
                        vm.pdf_Reports.forEach(function(d){
                            var chkBoxVal = $('#' + d.id).is(':checked');
                            if(chkBoxVal){
                                selectedReportName.push(d);
                            }else{
                                unSelectedReportName.push(d)
                            }
                        });
                        if(unSelectedReportName.length == 0){
                            $('.allPdfCheckBox').prop('checked', true);
                        }else{
                            $('.allPdfCheckBox').prop('checked', false);
                        }
                    }

                    vm.PDF_Report_Download = function(){
                        vm.tabPDFReportClass = true;
                        $(".loadingBar").show();
                        $('.resetMeasureBtn').css('display', 'none');
                        $('.filterBtn').css('display', 'none');
                        $('.exportViewBtn').css('display', 'none');
                        $('#filterContainer').css('margin-top', '1%');
                        $('input').removeClass('form-control');
                        $('div').removeClass('input-group date');
                        $('.startdatetime').css('border', 'none');
                        $('.enddatetime').css('border', 'none');
                        $('div').removeClass('form-material');
                        $('button').removeClass('select-with-transition');

                        var chkBoxVal = $('.allPdfCheckBox').is(':checked');

                        if(chkBoxVal){
                            var imgRenderData, imgCounter = 0, tabReport_Arr = [], selectedImgObj = {};
                            vm.tabs.forEach(function(d, i){
                                $( "#tabBtn_" + d.key +"> a" ).trigger("click");
                                $(window).trigger('resize');
                                $scope.$apply();
                                setTimeout(function(){
                                    $(".loadingBar").show();
                                    $scope.$apply();
                                },1005);
                                imgRenderData = new Promise(function(resolve, reject){
                                    (function(){
                                        html2canvas( $("#sharedViewImage"), {
                                            background: '#eee',
                                            useCORS: true,
                                            width: $(document).width(),
                                            onrendered: function(canvas) {
                                                var imgData = canvas.toDataURL("image/jpeg");
                                                selectedImgObj[i] = imgData;
                                                imgCounter++;
                                                if(imgCounter == vm.tabs.length){
                                                    resolve();
                                                }
                                            }
                                        });
                                    })();
                                }).then(function(){
                                    var count = 0;
                                    var newReportArr=[];
                                    Object.values(selectedImgObj).forEach(function(d, index){
                                        var imgRenderSize = new Promise(function(resolve, reject){
                                            var img = new Image;
                                            img.src = d;
                                            setTimeout(function(){
                                                count++;
                                                var obj = {};
                                                obj['width'] = img.width;
                                                obj['height'] = img.height;
                                                obj['img'] = d;
                                                tabReport_Arr.push(obj);
                                                newReportArr.push(d);
                                                if(count == Object.values(selectedImgObj).length){
                                                    resolve();
                                                }
                                            }, 100);
                                        }).then(function(){
                                            var newImgArr = [];
                                            newReportArr.forEach(function(d,i){
                                                var imgResizeData = new Promise(function(resolve, reject){
                                                    var oldBase64 = d.substring(23);
                                                    function resizeBase64Img(base64, width, height) {
                                                        var canvas = document.createElement("canvas");
                                                        canvas.width = width;
                                                        canvas.height = height;
                                                        var context = canvas.getContext("2d");
                                                        var deferred = $.Deferred();
                                                        $("<img />").attr("src", "data:image/gif;base64," + base64).load(function(){
                                                            context.scale(width/this.width, height/this.height);
                                                            context.drawImage(this, 0, 0);
                                                            deferred.resolve($("<img />").attr("src", canvas.toDataURL()));
                                                        });
                                                        return deferred.promise();
                                                    }
                                                    resizeBase64Img(oldBase64, 1000, 1000).then(function(newImg){
                                                        var ImgSrc = $(newImg[0]).attr('src');
                                                        newImgArr.push(ImgSrc);
                                                        if(newImgArr.length == Object.values(selectedImgObj).length){
                                                            resolve();
                                                        }
                                                    });
                                                }).then(function(){
                                                    var doc = jsPDF('l', 'mm');
                                                    newImgArr.forEach(function(d, i){
                                                        if(i != 0){
                                                            doc.addPage();
                                                        }
                                                        // doc.addImage(d, 'jpeg', 20, 20, 260, 180);
                                                        // doc.addImage(d, 'jpeg', 10, 10, 275, 190);
                                                        doc.addImage(d, 'jpeg', 5, 5, 285, 200);
                                                    });
                                                    setTimeout(function(){
                                                        var pdfName = vm.dashName.dashboardName + '.pdf';
                                                        doc.save(pdfName);
                                                        $('.dashboardName').css('text-align', 'center');
                                                        $('.dashboardName').css('margin-left', '0px');
                                                        vm.tabPDFReportClass = false;
                                                        $('.resetMeasureBtn').css('display', 'inline');
                                                        $('.filterBtn').css('display', 'block');
                                                        $('.exportViewBtn').css('display', 'block');
                                                        $('#filterContainer').css('margin-top', '4%');
                                                        $('input').addClass('form-control');
                                                        $('.startdatetime').css('border', 'block');
                                                        $('.enddatetime').css('border', 'block');
                                                        setTimeout(function(){
                                                            $(".loadingBar").hide();
                                                        }, 500);
                                                    }, 200);
                                                });
                                            });
                                        });
                                    });
                                });
                            });
                        }else{
                            var selectedReportArr = [], selectedImgObj = {};
                            vm.tabs.forEach(function(d){
                                var chkBoxValReport = $('#TabViewReport_PDF_' + d.key).is(':checked');
                                if(chkBoxValReport){
                                    selectedReportArr.push(d.key);
                                }
                            });
                            var imgRenderData, imgCounter = 0, tabReport_Arr = [];
                            vm.tabs.forEach(function(data, index){
                                selectedReportArr.forEach(function(d, i){
                                    if(data.key == d){
                                        $( "#tabBtn_" + d +"> a" ).trigger("click");
                                        $(window).trigger('resize');
                                        $scope.$apply();
                                        setTimeout(function(){
                                            $(".loadingBar").show();
                                            $scope.$apply();
                                        },1005);
                                        imgRenderData = new Promise(function(resolve, reject){
                                            (function(){
                                                html2canvas( $("#sharedViewImage"), {
                                                    background: '#eee',
                                                    useCORS: true,
                                                    onrendered: function(canvas) {
                                                        var imgData = canvas.toDataURL("image/jpeg");
                                                        selectedImgObj[i] = imgData;
                                                        imgCounter++;
                                                        if(imgCounter == selectedReportArr.length){
                                                            resolve();
                                                        }
                                                    }
                                                });
                                            })();
                                        }).then(function(){
                                            var count = 0;
                                            Object.values(selectedImgObj).forEach(function(d, index){
                                                var imgRenderSize = new Promise(function(resolve, reject){
                                                    var img = new Image;
                                                    img.src = d;
                                                    setTimeout(function(){
                                                        count++;
                                                        var obj = {};
                                                        obj['width'] = img.width;
                                                        obj['height'] = img.height;
                                                        obj['img'] = d;
                                                        tabReport_Arr.push(obj);
                                                        if(count == Object.values(selectedImgObj).length){
                                                            resolve();
                                                        }
                                                    }, 100);
                                                }).then(function(){
                                                    var doc = jsPDF('p', 'mm', [1, 1]);
                                                    var imgApplyData = new Promise(function(resolve, reject){
                                                        var imgDataCounter = 0;
                                                        (function(){
                                                            tabReport_Arr.forEach(function(d, index){
                                                                var height = d.height, width = d.width;
                                                                if(height > 1500){
                                                                    var docType = 'portrait';
                                                                }else{
                                                                    var docType = 'landscape';
                                                                }
                                                                doc.addPage([width/2, height], docType);
                                                                doc.addImage(d.img, 'jpeg', 20, 20, width, height-50);
                                                                imgDataCounter++;
                                                                if(imgDataCounter == Object.values(selectedImgObj).length){
                                                                    resolve();
                                                                }
                                                            });
                                                        })();
                                                    }).then(function(){
                                                        setTimeout(function(){
                                                            var pdfName = vm.dashName.dashboardName + '.pdf';
                                                            doc.save(pdfName);
                                                            $('.dashboardName').css('text-align', 'center');
                                                            $('.dashboardName').css('margin-left', '0px');
                                                            vm.tabPDFReportClass = false;
                                                            $('.resetMeasureBtn').css('display', 'inline');
                                                            $('.filterBtn').css('display', 'block');
                                                            $('.exportViewBtn').css('display', 'block');
                                                            $('#filterContainer').css('margin-top', '4%');
                                                            $('input').addClass('form-control');
                                                            $('.startdatetime').css('border', 'block');
                                                            $('.enddatetime').css('border', 'block');
                                                            setTimeout(function(){
                                                                $(".loadingBar").hide();
                                                            }, 500);
                                                        }, 200);
                                                    });
                                                });
                                            });
                                        });
                                    }
                                });
                            });
                        }
                    }

// Export View PDF start


























                    vm.leftSideView.render();
                    //Define Chart setting
                    vm.chartSettingObject = {};
                    //creating pivot table
                    //.....................
                    vm.chartErrorText = {};
                    vm.showInfo = {};
                    vm.showAggregate = {};
                    vm.periodVal = [];
                    var i = 1;
                    //Chart Setting Model
                    vm.selectedGroup = {};
                    var custIndex = 0;
                    vm.subCategory = {};
                    vm.headerGroup = function () {
                        if (vm.tableHeader.length) {
                            $(".ms-selected").hide();
                            if (vm.subCategory[custIndex] == undefined) {
                                vm.subCategory[custIndex] = 'group'+ custIndex;
                            }
                            vm.selectedGroup['group' + custIndex] = vm.tableHeader;
                            vm.tableHeader=[];
                            custIndex++;
                        } else {
                            alert("select any one");
                        }
                    }

                    vm.chartSetting = function (chartType) {
                        vm.Attributes.tableColumnOrder=[];
                        $.each(vm.Attributes.checkboxModelMeasure,function(key,value){
                            vm.Attributes.tableColumnOrder.push(value);
                        });
                        $.each(vm.Attributes.checkboxModelDimension,function(key,value){
                            vm.Attributes.tableColumnOrder.push(value);
                        });
                        vm.tableSettingCheckbox=angular.copy(vm.Attributes.checkboxModelMeasure);
                        vm.chartSettingObject = {};
                        if(vm.Attributes.dataFormate){
                            vm.chartSettingObject.xAxisDataFormate=vm.Attributes.dataFormate.xAxis;
                            vm.chartSettingObject.yAxisDataFormate=vm.Attributes.dataFormate.yAxis;
                            vm.chartSettingObject.toolTipFormate=vm.Attributes.dataFormate.tooltip;
                            vm.chartSettingObject.groupColor=vm.Attributes.dataFormate.groupColor;
                            vm.chartSettingObject.fontSize=vm.Attributes.dataFormate.fontSize;
                        }else{
                            vm.settingChartType=chartType;
                            vm.chartSettingObject.toolTipFormate = "{xLabel}:{yLabel}"; //tooltip Formate
                            vm.chartSettingObject.fontSize = 13; //Default font size
                        }
                        vm.numberStyle={};
                        if(vm.Attributes.numberStyle){
                            vm.numberStyle.fontSize=vm.Attributes.numberStyle.fontSize;
                            vm.numberStyle.fontColor=vm.Attributes.numberStyle.fontColor;
                        }else{
                            vm.numberStyle.fontSize=13;
                            vm.numberStyle.fontColor="#757575";
                        }
                        $('#chartSetting').modal({
                            backdrop: 'static',
                            keyboard: false
                        });
                        setTimeout(function(){
                            $(".sortable").sortable({
                                cancel: ".fixed",
                                stop : function(event, ui){
                                    vm.tableColumnOrder=$(this).sortable("toArray");
                                }
                            });
                        },100);
                        $('#chartSetting').modal('show');
                        vm.tableHeader=[];
                        setTimeout(function(){
                            $('#header-group').multiSelect({
                                afterSelect: function (values) {
                                    vm.tableHeader.push(values[0]);
                                },
                                afterDeselect: function (values) {
                                    var index = $scope.tableHeader.indexOf(values[0]);
                                    if (index != -1) {
                                        $scope.tableHeader.splice(index, 1);
                                    }
                                }
                            });
                        },1);
                        $('#chartSetting').modal({
                            backdrop: 'static',
                            keyboard: false
                        });
                        $('#chartSetting').modal('show');
                    }
                    vm.saveDataFormate = function (chartSetting) {
                        var dimensionType;
                        Object.keys(vm.Attributes.checkboxModelDimension).forEach(function (d) {
                            dimensionType = vm.Attributes.checkboxModelDimension[d].columType
                        });
                        vm.Attributes['dataFormate'] = {};
                        vm.Attributes['dataFormate']['xAxis'] = chartSetting.xAxisDataFormate;
                        vm.Attributes['dataFormate']['yAxis'] = chartSetting.yAxisDataFormate;
                        vm.Attributes['dataFormate']['tooltip'] = chartSetting.toolTipFormate;
                        vm.Attributes['dataFormate']['fontSize'] = chartSetting.fontSize;
                        vm.Attributes['dataFormate']['groupColor'] = chartSetting.groupColor;
                        vm.Attributes['dataFormate']['fontFamily'] = chartSetting.fontFamily;
                        var activeData = {};
                        if (!$.isEmptyObject(vm.filteredData)) {
                            activeData = vm.filteredData;
                        }else {
                            activeData = vm.tableData;
                        }
                        sketch
                            .axisConfig(vm.Attributes)
                            .data(activeData)
                            .container(vm.DashboardModel.Dashboard.activeReport.reportContainer.id)
                            .chartConfig($scope.selectedChartInView[vm.DashboardModel.Dashboard.activeReport.reportContainer.id])
                            .render(function (drawn) {
                                $scope.lastDrawnChart[vm.DashboardModel.Dashboard.activeReport.reportContainer.id] = vm.DashboardModel.Dashboard.activeReport.chart.key;
                            });
                        $('#chartSetting').modal('hide');
                    }

                    $scope.modal = {};
                    $scope.modal.slideUp = "default";
                    $scope.modal.stickUp = "default";
                    $scope.addChartIconBorder = function (chart) {
                        $(".custBorderHover").removeClass('activeChart');
                        $("#" + chart.key).addClass('activeChart');
                    }
                },
                getCharts: function () {
                    return vm.leftSideModel.chartTypeData;
                },
                getAggregates: function () {
                    return vm.leftSideModel.aggregateData;
                },

                senseDefaultChart: function () {
                    if (Object.keys(vm.Attributes.checkboxModelDimension).length == 1
                        && Object
                            .keys(vm.Attributes.checkboxModelMeasure).length == 1) {
                        $scope.addChartIconBorder(sketch.chartData[0]);
                        return sketch.chartData[0];

                    } else if (Object
                            .keys(vm.Attributes.checkboxModelDimension) == 1
                        && Object
                            .key(vm.Attributes.checkboxModelMeasure).length == 2) {
                        return sketch.chartData[5];
                    } else {
                        return false;

                    }

                },
                requiredAttributesFound: function () {
                    if (vm.Attributes.checkboxModelDimension && vm.Attributes.checkboxModelMeasure)
                        if (Object.keys(vm.Attributes.checkboxModelDimension).length > 0 && Object.keys(vm.Attributes.checkboxModelMeasure).length > 0) {
                            return true;
                        }
                    return false;

                },
                showChartErrorText: function (chart) {
                    try {
                        if (chart != null) {
                            vm.blnkRep = true;

                            //	vm.showInfovm.DashboardModel.Dashboard.activeReport.reportContainer.id=true;
                            $("#chart-" + vm.DashboardModel.Dashboard.activeReport.reportContainer.id).html("");
                            vm.chartErrorText[vm.DashboardModel.Dashboard.activeReport.reportContainer.id] = "You need to select max "
                                + chart.required.Dimension
                                + " dimension"
                                + " and min "
                                + chart.required.Measure[0]
                                + " measure to draw "
                                + chart.name;

                        } else {
                            vm.blnkRep = true;

                            vm.chartErrorText[vm.DashboardModel.Dashboard.activeReport.reportContainer.id] = "You need to select atleast 1 dimension and 1 measure to draw any chart";
                        }
                    } catch (e) {
                        vm.chartErrorText[vm.DashboardModel.Dashboard.activeReport.reportContainer.id] = "Some Error Occured....";
                    }
                },
                checkChartCanBeDrawn: function (chart) {
                    if (chart != null)
                        if (chart.name == "DataTable")
                            return true;
                    if (vm.Attributes.checkboxModelDimension && vm.Attributes.checkboxModelMeasure) {


                        if ((Object.keys(vm.Attributes.checkboxModelDimension).length == chart.required.Dimension) && ((chart.required.Measure[0] <= Object.keys(vm.Attributes.checkboxModelMeasure).length) && (chart.required.Measure[1] >= Object.keys(vm.Attributes.checkboxModelMeasure).length))) {
                            vm.showInfo[vm.DashboardModel.Dashboard.activeReport.reportContainer.id] = false;
                            return true;

                        }
                    }
                    vm.showInfo1 = true;
                    vm.showInfo2 = true;
                    vm.showInfo[vm.DashboardModel.Dashboard.activeReport.reportContainer.id] = true;
                    return false;
                },
                updateAttributesObject: function (attr, type, chartType) {

                    if (chartType == "_dataTable") {
                        return true;
                    }
                    if (type == "dimension")
                        if ($scope.Attributes.checkboxModelDimension[attr.columnName] == 0) {
                            delete $scope.Attributes.checkboxModelDimension[attr.columnName];
                            return false;
                        }
                    if (type == "measure")
                        if ($scope.Attributes.checkboxModelMeasure[attr.columnName] == 0) {
                            delete $scope.Attributes.checkboxModelMeasure[attr.columnName];
                            return false;
                        }

                    if (type == "chart"
                        && !vm.leftSideController
                            .checkChartCanBeDrawn(attr)) {
                        vm.leftSideController
                            .showChartErrorText(attr);
                        return false;
                    }

                    return true;
                },
                loadingBar:function(variable){
                    vm.reportResponseCount++;
                    setTimeout(function(){
                        vm[variable] = false;
                        if(!$scope.$$phase) {
                            $scope.$apply();
                        }
                    },1000);
                    if(vm.reportResponseCount == vm.reportCount){
                        setTimeout(function(){
                            $( "#tabBtn_"+vm.tabKeyActive+"> a" ).trigger( "click" );
                        },4000);
                    }
                },
                addNewContainer: function () {
                    vm.Attributes = {aggregateModel: {}};
                    vm.DashboardController
                        .addReportContainer(sketch.chartData[0]);
                    vm.DashboardModel.Dashboard.activeReport.chart = sketch.chartData[0];
                    $scope.selectedChartInView[vm.DashboardModel.Dashboard.activeReport.reportContainer.id] = null;
                    $(".custBorderHover").removeClass(
                        'activeChart');
                    // $("#" + index).addClass('activeChart');

                },
                setDefaultAggregate:function(measure){


                    //Initializing Aggregate Model In Case Not Initialized
                    if(!vm.Attributes.aggregateModel)
                        vm.Attributes.aggregateModel={};



                    vm.Attributes.aggregateModel[measure.columnName] = {
                        "key": "sumIndex",
                        "value": "Sum",
                        "type": "Aggregate"
                    };

                },
                setChartInView:function(chart){
                    $scope.selectedChartInView[vm.DashboardModel.Dashboard.activeReport.reportContainer.id]=chart;
                },
                getSelectedChartFromView:function(){
                    return $scope.selectedChartInView[vm.DashboardModel.Dashboard.activeReport.reportContainer.id];
                },
                checkAndDrawChart:function(chart){
                    var isChartBeDrawn = sketch.axisConfig(vm.Attributes).chartConfig(chart)._isAllFieldsPassed();
                    if(isChartBeDrawn){
                        vm.showInfo[vm.DashboardModel.Dashboard.activeReport.reportContainer.id] = false;
                        vm.leftSideController.showProgressBar(true);
                        vm.DashboardView.renderChartInActiveContainer();
                        vm.DashboardModel.Dashboard.activeReport.chart = chart;
                    }else {
                        vm.leftSideController.showChartErrorText(vm.DashboardModel.Dashboard.activeReport.chart);
                        vm.showInfo[vm.DashboardModel.Dashboard.activeReport.reportContainer.id] = true;
                        $(".label_legends" + vm.DashboardModel.Dashboard.activeReport.reportContainer.id).hide();
                        $("#left" + vm.DashboardModel.Dashboard.activeReport.reportContainer.id).html("");
                    }
                },
                switchChartContainer:function(attr,callback){
                    var activeReportContainerId = vm.DashboardModel.Dashboard.activeReport.reportContainer.id;
                    var activeContainer = $scope.dashboardPrototype.reportContainers[$scope.findIndexOfReportContainer(activeReportContainerId)];

                    if(attr.key!="_numberWidget") {
                        if ($scope.lastDrawnChart[vm.DashboardModel.Dashboard.activeReport.reportContainer.id] == "_numberWidget") {
                            var obj = vm.DashboardController.getCommonContainerSettings();
                            $.each(obj,function (key,value) {
                                activeContainer[key] = value;
                            });
                        }
                    }else{
                        var obj = vm.DashboardController.getNumberContainerSettings();
                        $.each(obj, function (key, value) {
                            activeContainer[key] = value;

                        });
                    }
                    vm.DashboardModel.Dashboard.activeReport.reportContainer = activeContainer;
                    var indexOfReport = $scope.findIndexOfReportFromReportContainer(activeReportContainerId);
                    vm.DashboardModel.Dashboard.Report[indexOfReport] = activeContainer;
                    setTimeout(function(){

                        callback();
                    },1000);
                },
                showProgressBar:function(state){
                    var variable = "reportLoading_" + vm.DashboardModel.Dashboard.activeReport.reportContainer.id;
                    vm[variable] = state;
                },
                onAttributeSelect: function (attr, type) {
                    //Updating Attribute Object According to selections
                    vm.leftSideController.updateAttributesObject(attr,type);
                    if (type === "measure") {
                        //Sets default aggregate for each measure
                        vm.leftSideController.setDefaultAggregate(attr);
                    }
                    if(type==="dimension" || type==="measure" || type==="aggregate"){
                        var activeChart=vm.leftSideController.getSelectedChartFromView();
                        //check if any chart selected in view
                        if(!activeChart){
                            vm.leftSideController.setChartInView(vm.DashboardModel.Dashboard.activeReport.chart);
                        }else{
                            vm.leftSideController.checkAndDrawChart(activeChart);
                        }
                    }
                    if(type==="chart"){
                        vm.leftSideController.switchChartContainer(attr,function () {
                            if(attr.key=="_pivotTable" && vm.DashboardModel.Dashboard.Report[vm.DashboardModel.Dashboard.activeReportIndex].chartObject.config){
                                vm.DashboardModel.Dashboard.Report[vm.DashboardModel.Dashboard.activeReportIndex].chartObject['recall']=true;
                                sketch.pivotTableConfig=Object.assign({},vm.DashboardModel.Dashboard.Report[vm.DashboardModel.Dashboard.activeReportIndex].chartObject);
                            }else if(attr.key=="_pivotTable"){
                                sketch.pivotTableConfig=undefined;
                            }
                            vm.leftSideController.setChartInView(attr);
                            vm.leftSideController.checkAndDrawChart(attr);
                        });
                    }
                }
            }

            vm.leftSideView = {
                render: function () {
                    vm.Attributes = {};
                    vm.chartDraw = vm.leftSideController.onAttributeSelect;
                    // vm.chartSelect=vm.leftSideController.updateAttribute;
                    $scope.charts = vm.leftSideController.getCharts();
                    $scope.aggregates = vm.leftSideController.getAggregates();
                    $scope.addReport = vm.leftSideController.addNewContainer;
                }
            };

            vm.leftSideController.init();

            // ..................Right side
            // .....................................................

            vm.rightSideModel = {
                axisObject: sketch.axisData,
                AggregateObject: sketch.AggregatesObject,
                axisData: sketch.attributesData,
                activeAxisConfig: null
            }
            vm.rightSideController = {
                init: function () {
                    vm.rightSideView.init();
                    vm.checkboxModel = {};
                    vm.checkboxMD = {};
                    vm.filtersToApply = [];
                    vm.filtersTo = [];
                    var newDim = [];
                    vm.styleSettings = {};
                    vm.columnAddValue = function (type, value) {
                        var tableColumnIndex = vm.rightSideController.findIndexofActiveContainer(vm.tableColumns,"value", value);
                        vm.tableColumns[tableColumnIndex].type = [];
                        vm.tableColumns[tableColumnIndex].type.push(type);
                        // $('.advChartSettingsModal').drawer('close');
                        generate("success","Move To Column Successfully");
                    }
                    vm.chartStyle = function (reportContainer) {
                        vm.styleSettings = {};
                        vm.DashboardController.onReportContainerClick(reportContainer.id);
                        if (vm.DashboardModel.Dashboard.Report[vm.DashboardModel.Dashboard.activeReportIndex].chart.styleSettings) {
                            vm.styleSettings = vm.DashboardModel.Dashboard.Report[vm.DashboardModel.Dashboard.activeReportIndex].chart.styleSettings;
                            $('.chartStyle').drawer('open');
                        } else if (vm.DashboardModel.Dashboard.Report[vm.DashboardModel.Dashboard.activeReportIndex].chartObject != undefined) {
                            vm.cWidthshow = false;
                            vm.styleSettings.rotateLabel = 0;
                            vm.styleSettings.rotateChart = 0;
                            vm.styleSettings.resizable = "false";
                            vm.styleSettings.translateX = 0;
                            vm.styleSettings.translateY = 0;
                            var index = reportContainer.id - 1;
                            var chart = vm.DashboardModel.Dashboard.Report[vm.DashboardModel.Dashboard.activeReportIndex].chart;
                            var chartObj = vm.DashboardModel.Dashboard.Report[vm.DashboardModel.Dashboard.activeReportIndex].chartObject;
                            // Push Value to input fields
                            vm.styleSettings.chartColor = "#1f77b4";
                            if (typeof chartObj !== 'undefined'
                                && chartObj.margins) {
                                vm.styleSettings.xAxisMargin = chartObj
                                    .margins().bottom;
                                vm.styleSettings.yAxisMargin = chartObj
                                    .margins().left;
                            }
                            vm.styleSettings.cHeight = $(
                                "#chart-"
                                + reportContainer.id)
                                .height();
                            vm.styleSettings.cWidth = $(
                                "#chart-"
                                + reportContainer.id)
                                .width();
                            vm.styleSettings.xAxisLabelName = $(
                                "#chart-"
                                + reportContainer.id)
                                .find(".x-axis-label")
                                .text();
                            vm.styleSettings.yAxisLabelName = $(
                                "#chart-"
                                + reportContainer.id)
                                .find(".y-axis-label")
                                .text();
                            vm.DashboardModel.Dashboard.Report[vm.DashboardModel.Dashboard.activeReportIndex].chart.styleSettings = vm.styleSettings;

                            $('.chartStyle').drawer('open');
                            var processedData = [];
                            vm.rightSideModel.axisData
                                .forEach(function (obj) {
                                    chart.attributes
                                        .forEach(function (chartType) {
                                            if (chartType.indexOf(obj.Name) != -1) {
                                                // chartSettingsObj[obj.modelName]='{"key":"","value":"","type":[""]}';
                                                obj.visible = true;
                                                processedData.push(obj);
                                            }
                                        });
                                });
                            vm.processStyle = processedData;
                        }
                    }
                },
                getMeasures: function () {
                    var tempArray = [];

                    vm.tableColumns.forEach(function (obj) {

                        if (obj.type == "Measure")
                            tempArray.push(obj);
                        if (obj.type == "MeasureCustom")
                            tempArray.push(obj);

                    });
                    return tempArray;
                },

                setActiveAxisConfig: function (config) {
                    vm.rightSideModel.activeAxisConfig = config;
                },
                getActiveAxisConfig: function () {
                    return vm.rightSideModel.activeAxisConfig;
                },
                getDimensions: function () {
                    var tempArray = [];
                    var pushSkip = 0;
                    vm.tableColumns.forEach(function (obj) {

                        if (obj.type == "Dimension")
                            tempArray.push(obj);
                        if (obj.type == "DimensionCustom")
                            tempArray.push(obj);

                    });
                    return tempArray;
                },
                columnDataPush: function (obj) {
                    if (obj.columnType == "Measure") {
                        obj.columnData = vm.rightSideController
                            .getMeasures();
                        return obj;
                    } else if (obj.columnType == "Dimension") {
                        obj.columnData = vm.rightSideController
                            .getDimensions();
                        return obj;

                    } else if (obj.columnType == "Aggregate") {
                        obj.columnData = vm.rightSideModel.AggregateObject;
                        return obj;
                    }
                },

                getAxisRenderObj: function (selected, type) {
                    if (type == "chart") {

                    }
                    var chartSettingsObj = {};
                    var dimensionArray = [];
                    chartSettingsObj['dimension'] = vm.checkboxModelDimension;
                    chartSettingsObj['measure'] = vm.checkboxModelMeasure;
                    chartSettingsObj['aggregate'] = [{
                        "key": "sumIndex",
                        "value": "Sum",
                        "type": "Aggregate"
                    }];
                    var processedData = [];
                    vm.chartSettings = chartSettingsObj;
                    vm.DashboardModel.Dashboard.activeReport.chart = selected;
                    vm.rightSideController.onAxisValueChange();
                    // return finalData;

                    /*
                     * vm.rightSideModel.axisObject.forEach(function(obj) {
                     *
                     * chartTypeObj.requiredAxis.forEach(function(chartType) {
                     *
                     * if (chartType.indexOf(obj.Name) != -1) { //
                     * chartSettingsObj[obj.modelName]='{"key":"","value":"","type":[""]}';
                     * obj.visible = true;
                     * processedData.push(obj); } }
                     *
                     * chartTypeObj.requiredAxis.forEach(function(chartType) {
                     * if(obj.Name==chartType){ //
                     * chartSettingsObj[obj.modelName]='{"key":"","value":"","type":[""]}';
                     * obj.visible=true;
                     * processedData.push(obj); } } );
                     *
                     * });
                     */

                    /*
                     * var finalData = [];
                     * processedData.forEach(function(obj) {
                     * finalData.push(vm.rightSideController.columnDataPush(obj)); //
                     * vm.chartSettings();
                     *
                     * });
                     */

                },

                findIndexofActiveContainer: function (arraytosearch, key, valuetosearch) {
                    for (var i = 0; i < arraytosearch.length; i++) {
                        if (arraytosearch[i][key] == valuetosearch) {
                            return i;
                        }
                    }
                    return null;
                },

                onAxisValueChange: function () {

                    /*
                     * if
                     * (JSON.parse(vm.chartSettings["Xaxis"]).key ==
                     * "DATETIME")
                     * $("#dateFormatChangeModal").modal();
                     */

                    var drawn = sketch
                        .axisConfig(vm.chartSettings)
                        .data(vm.tableData)
                        .container(vm.DashboardModel.Dashboard.activeReport.reportContainer.id)
                        .chartConfig(vm.DashboardModel.Dashboard.activeReport.chart)
                        .render();

                    var index = vm.rightSideController
                        .findIndexofActiveContainer(
                            $scope.dashboardPrototype.reportContainers,
                            "id",
                            vm.DashboardModel.Dashboard.activeReport.reportContainer.id);

                    if (drawn) {
                        $scope.filterSave = true;
                        $scope.saveState = 1;

                        $scope.dashboardPrototype.reportContainers[index]['image'] = null;

                    }

                    vm.rightSideController
                        .setActiveAxisConfig(vm.chartSettings);
                    var indexOfReport = vm.DashboardController
                        .findIndexofActiveContainer(
                            vm.DashboardModel.Dashboard.Report,
                            "id",
                            vm.DashboardModel.Dashboard.activeReport.reportContainer.id);


                    vm.DashboardModel.Dashboard.Report[indexOfReport].axisConfig = new Object(
                        vm.chartSettings);
                    if (drawn) {
                        drawn["chartTypeAttribute"] = vm.DashboardModel.Dashboard.activeReport.chart.name;
                        drawn["resize"] = "ON";
                        vm.DashboardModel.Dashboard.Report[indexOfReport]["chartObject"] = drawn;

                    }
                    vm.DashboardModel.Dashboard.activeReport = vm.DashboardModel.Dashboard.Report[indexOfReport];

                    vm.DashboardModel.Dashboard.activeReportIndex = indexOfReport;

                }
            }

            vm.rightSideView = {
                // this part can be improved
                init: function () {
                    // vm.chartDraw=vm.rightSideController.getAxisRenderObj;
                    vm.refreshChartContainer = vm.rightSideController.onAxisValueChange;
                    vm.columnTypeObj = {};
                    $scope.addColumnTypeRightSide = function (columnObj) {

                        vm.hiddenAxis = columnObj.Name;
                        $("#myModal").modal();

                        if (columnObj.columnType
                                .indexOf("Dimension") != -1) {
                            vm.columnTypeObj = vm.rightSideController
                                .getMeasures();

                        } else if (columnObj.columnType
                                .indexOf("Measure") != -1) {
                            vm.columnTypeObj = vm.rightSideController
                                .getDimensions();
                        }

                    }
                    // Calculation part
                    $scope.measureSetting = function (columnObj) {
                        $("#measureModel").modal();
                        columnObj.columnType = "Dimension";
                        if (columnObj.columnType == "Dimension") {
                            vm.columnTypeObj = vm.rightSideController
                                .getMeasures();
                        } else if (columnObj.columnType == "Measure") {
                            vm.columnTypeObj = vm.rightSideController
                                .getDimensions();
                        }
                    }
                    $scope.calMeasureShow = [];
                    $scope.calMeasureDb = [];
                    $scope.measureAdd = function () {
                        // Formula in array

                        $scope.measureName = "";
                        $scope.measureFormula = "";
                    }
                    $scope.measureFormula = "";
                    $scope.insertText = function (elemID, text) {
                        $scope.measureFormula += text;

                        // $scope.measureVal.measureFormula
                        // +=text
                    }
                    // Close Style Type Drawer
                    $scope.styleTypeDrawerClose = function () {
                        $('.chartStyle').drawer('close');
                    }

                    vm.calMeasureArrayForCompare = [];
                    // Measure Add Calculation
                    vm.customMeasure = [];
                    vm.measureAddCal = function () {
                        // sketch.setGroupExpression($scope.expression);
                        var expression = $scope.measureFormula;
                        vm.FormulaArray = expression
                            .split(/([\+\-\*\/])/);
                        vm.expression = expression.replace(
                            /\b[a-z]\w*/ig, "v['$&']")
                            .replace(/[\(|\|\.)]/g, "");
                        vm.calMeasureShow
                            .push({
                                'measureName': $scope.measureName,
                                'measureFormula': $scope.measureFormula
                            });
                        vm.calMeasureDb
                            .push({
                                'measureName': $scope.measureName,
                                'measureFormula': $scope.FormulaArray
                            });
                        vm.calMeasureShow
                            .forEach(function (d) {
                                if (vm.calMeasureArrayForCompare
                                        .indexOf(d.measureName) == -1) {
                                    var newExpression = d.measureFormula
                                        .replace(
                                            /\b[a-z]\w*/ig,
                                            "v['$&']")
                                        .replace(
                                            /[\(|\|\.)]/g,
                                            "");

                                    var testObject = {
                                        "key": "INT",
                                        "value": d.measureName,
                                        "type": ["MeasureCustom"],
                                        "formula": newExpression
                                    };
                                    vm.customMeasure.push(testObject);
                                    vm.tableColumns.push(testObject);
                                    $('.advChartSettingsModal').drawer('close');
                                    generate("success","Measure created successfully");
                                } else {
                                    generate("error","You Calculation column match to other column");
                                }
                            });
                    }
                    // Measure Validation
                    vm.measureAddValidation = function () {
                        /*
                         * var parts =
                         * $scope.measureFormula.split(/[[\]]{1,2}/);
                         * parts.length--; // the last entry is
                         * dummy, need to take it out var
                         * valExp=parts.join('');
                         */
                        var newExpression = $scope.measureFormula
                            .replace(/\b[a-z]\w*/ig,
                                "v['$&']").replace(
                                /[\(|\|\.)]/g, "");
                        var formula = "v['"
                            + vm.lastColumSelected + "']";
                        var testObject = {
                            "key": "INT",
                            "value": $scope.measureNameValidation,
                            "type": ["MeasureCustom"],
                            "validation": newExpression,
                            "formula": formula
                        };
                        vm.tableColumns.push(testObject);
                        $('.advChartSettingsModal').drawer('close');
                        generate("success", "Measure created successfully");
                    }
                    $scope.addColumnType = function (columnObj) {
                        vm.hiddenAxis = columnObj.Name;
                        $("#myModal").modal();

                        if (columnObj.columnType == "Dimension") {
                            vm.columnTypeObj = vm.rightSideController.getMeasures();
                        } else if (columnObj.columnType == "Measure") {
                            vm.columnTypeObj = vm.rightSideController.getDimensions();
                        }
                    }
                    // End Calculation Part

                },
                render: function (chartTypeObj) {
                    // vm.chartSettings={};
                    this.data = vm.rightSideController
                        .getAxisRenderObj(chartTypeObj);
                    // vm.rightSideView.resetView(this.data);
                    // $(".select2").select2('remove');

                    vm.axisRenderArray = this.data;
                    setTimeout(function () {
                        $('select').select2();
                    }, 100);
                },
                chartSelectView: {
                    update: function (chartInfo) {
                        $scope.chartType = JSON.stringify(chartInfo);
                    }
                }

            }

            // vm.rightSideController.init();

            // .................................................End
            // Right Side
            // View...........................................................................

            // ...........................Dashboard
            // View...............................................................................
            var maxSizeY;
            vm.DashboardModel = {
                currentActiveGrid: null,
                dashboardPrototypeObject: {
                    id: '1',
                    name: 'Home',
                    reportContainers: []
                },
                reportPrototypeObject: {
                    reportContainer: null,
                    chart: null,
                    axisConfig: null
                },
                gridSettings: {
                    margins: [20, 15],
                    columns: 30,
                    pushing: true,
                    maxRows:10000,
                    floating: true,
                    swapping: true,
                    rowHeight: '30',
                    collision: {
                        on_overlap_start: function (collider_data) {

                        }
                    },
                    draggable: {
                        enabled: true, // whether dragging
                        // items is supported
                        handle: '.my-class',
                        start: function (event, $element, widget) {

                        },
                        resize: function (event, $element, widget) {

                        },
                        stop: function (event, $element, widget) {
                            /*$(".hiddenBorder").removeClass(
                             "box-border");*/
                        }
                    },
                    resizable: {
                        enabled: true,
                        handles: ['n', 'e', 's', 'w', 'ne',
                            'se', 'sw', 'nw'],
                        stop: function (event, uiWidget,$element) {
                            if(vm.DashboardModel.Dashboard.activeReport.chart.key!="_numberWidget"){
                                var getHeight= ($("#li_"+vm.DashboardModel.Dashboard.activeReport.reportContainer.id).height())-97;
                                var getWidth= $("#li_"+vm.DashboardModel.Dashboard.activeReport.reportContainer.id).width()-40;
                                $("#chart-"+vm.DashboardModel.Dashboard.activeReport.reportContainer.id).height(getHeight);
                                $("#chart-"+vm.DashboardModel.Dashboard.activeReport.reportContainer.id).width(getWidth);
                                //document.getElementById("left"+vm.DashboardModel.Dashboard.activeReport.reportContainer.id).style.height = height;
                                var legendHeight=$("#li_"+vm.DashboardModel.Dashboard.activeReport.reportContainer.id).height();
                                $("#left"+vm.DashboardModel.Dashboard.activeReport.reportContainer.id).height(legendHeight*60/100);
                                //Chart Hide
                                var activeReport = vm.DashboardModel.Dashboard.Report[vm.DashboardModel.Dashboard.activeReportIndex];
                                $("#li_" + uiWidget[0].firstElementChild.id).addClass("li-box-border");
                                if(vm.DashboardModel.Dashboard.activeReport.chart.key!="_mapChartjs"){
                                    $("#mapbox-"+vm.DashboardModel.Dashboard.activeReport.reportContainer.id).height(getHeight);
                                    $("#mapbox-"+vm.DashboardModel.Dashboard.activeReport.reportContainer.id).width(getWidth);
                                }
                            }
                            $("#chart-" + vm.DashboardModel.Dashboard.activeReport.reportContainer.id).show();
                            var variable = "reportLoading_" + vm.DashboardModel.Dashboard.activeReport.reportContainer.id;
                            vm[variable] = false;
                            $scope.chartLoading = false;
                            $("#chart-" + $element.id).show();
                        },
                        start: function (event, uiWidget, $element) {
                            $("#chart-" + $element.id).hide();
                            //chart Loading Bar True
                            var variable = "reportLoading_" + vm.DashboardModel.Dashboard.activeReport.reportContainer.id;
                            vm[variable] = true;
                            //Chart Hide
                            $(".label_legends" + vm.DashboardModel.Dashboard.activeReport.reportContainer.id).hide();
                            $("#chart-" + vm.DashboardModel.Dashboard.activeReport.reportContainer.id).hide();
                            $("#left" + vm.DashboardModel.Dashboard.activeReport.reportContainer.id).hide();
                        }
                    }
                },
                numberWidgetContainerSettings: {
                    id: 0,
                    name: "Number Widget",
                    sizeX: 9,
                    sizeY: 8,
                    minSizeX: 3,
                    minSizeY: 3,
                    maxSizeX: 9,
                    maxSizeY: 8
                },
                commonContainersSettings: {
                    id: 0,
                    name: "Report",
                    sizeX: 6,
                    sizeY: 8,
                    minSizeX: 6,
                    minSizeY: 8
                },
                Dashboard: {
                    activeReportIndex: null,
                    activeReport: null,
                    lastReportId: 0,
                    Report: []
                }
            };
            var report = [];
            vm.DashboardController = {
                init: function () {
                    vm.DashboardView.init();
                    vm.chartLoading = false;
                    vm.checkboxModel = {};
                    vm.reportMobileIndex=0;
                    //
                    vm.dateRangeArray = [];
                    vm.dateModal = function () {
                        $('#dateModal').modal({
                            backdrop: 'static',
                            keyboard: false
                        });
                        $('#dateModal').modal('show');
                    }
                    var i = 0;
                    vm.filterApplyAdd = function (keys) {
                        vm.blnkFilter = false;
                        var filerTemp = [];
                        vm.filtersToApply = [];
                        vm.filterHSApply = true;
                        var columnObj = "";
                        vm.checkboxModel[keys.columnName] = keys;
                        Object.keys(vm.checkboxModel).forEach(function (d) {
                            if (vm.checkboxModel[d] != "0") {
                                vm.filtersTo[d] = [];
                                columnObj = vm.checkboxModel[d];
                                var keyValue = {};
                                keyValue['key'] = columnObj.columnName;
                                keyValue['value'] = columnObj.columType;
                                filerTemp.push(keyValue);
                                Enumerable.From(vm.tableData)
                                    .Distinct(function (x) { return x[d]; })
                                    .Select(function (x) {
                                        return x[d]; })
                                    .ToArray()
                                    .forEach(
                                        function (e) {
                                            vm.filtersTo[d]
                                                .push(e);
                                        });
                            }
                        });
                        if (filerTemp.length) {
                            $scope.onApplyFilter = true;
                            $("#dash-border").css("margin-top", "110px");
                        } else {
                            $scope.onApplyFilter = false;
                            $("#dash-border").css("margin-top", "60px");
                        }
                        vm.filtersToApply = filerTemp;
                        setTimeout(function () {
                            $('.multielect').multiselect(
                                {
                                    includeSelectAllOption: true,
                                    enableFiltering: true,
                                    maxHeight: 200
                                });
                        }, 1000);
                    }
                    vm.filter_add = function () {
                        $('.drawer1').drawer('open');
                    }
                    // Filter Hide Show
                    $scope.filterHideShow = function () {
                        $scope.filterHide = !$scope.filterHide;
                        $scope.filterShow = !$scope.filterShow;
                        $scope.filterHSApply = !$scope.filterHSApply;

                        if ($scope.filterShowHidden) {
                            $scope.filterShowHidden = 0
                        } else {
                            $scope.filterShowHidden = 1;
                        }
                    }
                },
                findIndexofActiveContainer: function (arraytosearch, key, valuetosearch) {
                    for (var i = 0; i < arraytosearch.length; i++) {
                        if (arraytosearch[i].reportContainer[key] == valuetosearch) {
                            return i;
                        }
                    }
                    return null;
                },
                getGridSettings: function () {
                    return vm.DashboardModel.gridSettings;
                },
                getCommonContainerSettings: function () {
                    return {
                        name: "Report",
                        sizeX: 12,
                        sizeY: 6,
                        minSizeX: 3,
                        minSizeY: 3
                        /*maxSizeX: 12,
                         maxSizeY: 6*/
                    };
                },
                getTextContainerSettings:function(){
                    return {
                        name: "Text",
                        sizeX: 8,
                        sizeY: 4,
                        minSizeX: 2,
                        minSizeY: 2,
                        /* maxSizeX: 30,
                         maxSizeY: 15,*/
                        chartType: "Text"
                    };
                },
                getNumberContainerSettings: function () {
                    return {
                        name: "Number Widget",
                        sizeX: 8,
                        sizeY: 4,
                        minSizeX: 1,
                        minSizeY: 1,
                        /* maxSizeX: 16,
                         maxSizeY: 8,*/
                        chartType: "Number Widget"
                    };
                },
                onChartDropToContainer: function (event, index, item, external, type, allowedType) {
                    var view = vm.DashboardView;
                    dc.filterAll();
                    vm.isParameterShown = true;
                    $('.drawer').drawer('open');
                    view.removeGridBorder();
                    vm.rightSideView.render(item);
                    vm.DashboardController.addReportContainer(item);
                    vm.rightSideView.chartSelectView.update(item);
                    vm.dimesionColumn = vm.rightSideController.getDimensions();
                },

                onReportContainerClick: function (id) {
                    var index = vm.DashboardController.findIndexofActiveContainer(vm.DashboardModel.Dashboard.Report,"id", id);
                    vm.DashboardModel.Dashboard.activeReportIndex=index;
                    $scope.addChartIconBorder($scope.selectedChartInView[id]);
                    $.each(vm.DashboardModel.Dashboard.Report[index].axisConfig.checkboxModelDimension,function(key,value){
                        if(value.key){
                            delete value.key;
                            delete value.value;
                        }
                    });
                    $.each(vm.DashboardModel.Dashboard.Report[index].axisConfig.checkboxModelMeasure,function(key,value){
                        if(value.key){
                            delete value.key;
                            delete value.value;
                        }
                    });
                    vm.Attributes = vm.DashboardModel.Dashboard.Report[index].axisConfig;
                    vm.DashboardModel.Dashboard.activeReport = vm.DashboardModel.Dashboard.Report[index];
                    vm.selectedChart = vm.DashboardModel.Dashboard.Report[index].chartObject;
                },
                getDashboardProto: function () {
                    return vm.DashboardModel.dashboardPrototypeObject;
                },
                getReportProto: function () {
                    return {
                        reportContainer: {},
                        chart: {},
                        axisConfig: {}
                    };
                },
                addReportContainer: function (chartInfo) {
                    var flag = true;
                    if ($scope.dashboardPrototype.reportContainers.length != 0)
                        vm.DashboardModel.Dashboard.Report.forEach(function (d, index) {
                            if (!d.chartObject) {
                                //flag = false;
                                flag = true;
                            }
                        });
                    /*if(flag)
                     {*/

                    var reportObject = vm.DashboardController.getReportProto();
                    reportObject.chart = chartInfo;
                    vm.DashboardModel.Dashboard.lastReportId++;
                    this.lastId = vm.DashboardModel.Dashboard.lastReportId;
                    vm.dashboardPrototype.reportContainers['chartType'] = chartInfo.name;
                    if (chartInfo.name == "Number Widget") {
                        vm.DashboardModel.numberWidgetContainerSettings.id = this.lastId;
                        var obj = vm.DashboardController.getNumberContainerSettings();
                        obj['id'] = this.lastId;
                        //obj['chartType'] = chartInfo.name;
                        obj['row'] = 0;
                        obj['col'] = 0;
                        reportObject.reportContainer = obj;
                        vm.DashboardModel.Dashboard.activeReport.reportContainer = reportObject.reportContainer;
                        $scope.selectedChartInView[this.lastId] = sketch.chartData[0];
                    } else {
                        vm.DashboardModel.commonContainersSettings.id = this.lastId;
                        var obj = vm.DashboardController.getCommonContainerSettings();
                        obj['row'] = 0;
                        obj['col'] = 0;
                        obj['id'] = this.lastId;
                        obj['chartType'] = chartInfo.name;
                        reportObject.reportContainer = obj;
                        $scope.selectedChartInView[this.lastId] = sketch.chartData[0];
                        setTimeout(function () {
                            $(".li_box > .handle-se").removeClass("handle-se-hover");
                            $(".li_box").removeClass("li-box-border");
                            $("#li_" + vm.DashboardModel.commonContainersSettings.id+"> .handle-se").addClass("handle-se-hover");
                            $("#li_" + vm.DashboardModel.commonContainersSettings.id).addClass("li-box-border");
                        }, 1);
                    }
                    /*
                     * var $target = $('html,body');
                     * $target.animate({scrollTop:
                     * $target.height()}, 1000);
                     */
                    if (flag) {
                        reportObject.reportContainer['image'] = dataFactory.baseUrlData() + chartInfo.image;
                        vm.DashboardModel.Dashboard.activeReport = reportObject;
                        vm.DashboardView.addReportContainerView(reportObject.reportContainer);
                        vm.aggregateText[reportObject.reportContainer.id] = "Sum";
                        report.push(reportObject);
                        vm.DashboardModel.Dashboard.Report.push(reportObject);
                        vm.DashboardModel.Dashboard.activeReportIndex = vm.DashboardModel.Dashboard.Report.length - 1;
                    }else {
                        dataFactory.errorAlert("can't add more reports till current is empty");
                    }
                },

                updateReport: function () {
                    vm.DashboardModel.Dashboard.activeReport.chart = JSON.parse(vm.chartType);
                    // vm.DashboardModel.Dashboard.activeReportIndex
                },

                getActiveReport: function () {
                    return vm.DashboardModel.Dashboard.activeReport;
                }

            }

            vm.DashboardView = {
                init: function () {
                    vm.reportContainerMobile=[];
                    vm.reportContainerPublicView=[];
                    // Gridster Settings
                    $scope.dashboardCommonSettings = vm.DashboardController.getGridSettings();
                    $scope.onDrop = vm.DashboardController.onChartDropToContainer;
                    vm.DashboardView.initDashboardProto();
                    $scope.addBorder = function (id) {
                        /*$(".box").removeClass("box-border");*/
                        $(".li_box").removeClass("li-box-border");
                        $(".li_box > .handle-se").removeClass("handle-se-hover");
                        $("#li_" + id).addClass("li-box-border");
                        $("#li_" + id+"> .handle-se").addClass("handle-se-hover");
                        /*$("#" + id).addClass("box-border");*/
                        vm.DashboardController.onReportContainerClick(id);
                    };
                    vm.numberFormate = function (style) {
                        var styleSettings = {};
                        styleSettings["NumberFormate"] = style;
                        styleSettings["type"] = "Number Widget";
                        var activeDashboardIndex = vm.DashboardModel.Dashboard.activeReportIndex;
                        vm.DashboardModel.Dashboard.Report[activeDashboardIndex]['attributes'] = "Number Widget";
                        sketch.renderAttributes(vm.DashboardModel.Dashboard.Report[activeDashboardIndex].chartObject, styleSettings);
                    }
                    vm.saveChartStyle = function (styleSettings) {
                        var activeDashboardIndex = vm.DashboardModel.Dashboard.activeReportIndex;
                        vm.DashboardModel.Dashboard.Report[activeDashboardIndex]['attributes'] = styleSettings;
                        sketch.renderAttributes(vm.DashboardModel.Dashboard.Report[activeDashboardIndex].chartObject,styleSettings);
                        generate('success','Chart style apply successfully');
                    }
                },

                activeTab :function(data){
                    $('.allTabPdfCheckBox').prop('checked', false);

// Export Tab Reports Start
                    vm.tab_Id_Report = data;
                    vm.tab_PDF_Report = [];
                    vm.tab_Excel_Report = [];
                    vm.dashboardPrototype.reportWithTab[data.key].forEach(function(d){
                        vm.tab_PDF_Report.push(d);
                        if(d.chartType == 'DataTable' || d.chartType == 'Pivot Table Customized'){
                            vm.tab_Excel_Report.push(d);
                        }
                        // else{
                        //     vm.tab_PDF_Report.push(d);
                        // }
                    });
// Export Tab Reports End

                    $(".loadingBar").show();
                    $(window).trigger('resize');
                    setTimeout(function(){
                        $(".loadingBar").hide();
                    },1000);
                },
                autoAdjustChartView: function () {
                    sketch.selfAdjustChart(vm.DashboardModel.Dashboard.activeReport);
                },
                render: function () {
                    vm.DashboardModel.Dashboard.Report.forEach(function(d,i) {
                        vm.DashboardView.addReportContainerView(d.reportContainer);
                        $scope.selectedChartInView[d.reportContainer.id]=d.chart;
                        //Render chart
                        if(d.chart.key=="_text"){
                            sketch
                                .container(d.reportContainer.id)
                                ._text(d.chart.value);
                        }else{
                            sketch
                                .axisConfig(d.axisConfig)
                                .data(vm.tableData)
                                .container(d.reportContainer.id)
                                .chartConfig(d.chart)
                                .render();
                        }
                        if(i==0){
                            vm.Attributes=d.axisConfig;
                        }
                        d.reportContainer.chartType=d.chart.name;
                        var width=$("#dashboardContainer_"+d.reportContainer.id).width();
                        var height=$("#dashboardContainer_"+d.reportContainer.id).height();
                        /*if(width<600){
                         $("#left"+d.reportContainer.id).hide();
                         $(".label_legends"+d.reportContainer.id).hide();
                         }*/
                    });
                },
                updateDashboardObject: function (drawn) {
                    var index = vm.rightSideController.findIndexofActiveContainer($scope.dashboardPrototype.reportContainers, "id", vm.DashboardModel.Dashboard.activeReport.reportContainer.id);
                    if (drawn) {
                        $scope.filterSave = true;
                        $scope.saveState = 1;
                        $scope.dashboardPrototype.reportContainers[index]['image'] = null;
                    }
                    // vm.rightSideController.setActiveAxisConfig(vm.chartSettings);
                    var indexOfReport = vm.DashboardController.findIndexofActiveContainer(vm.DashboardModel.Dashboard.Report,"id",vm.DashboardModel.Dashboard.activeReport.reportContainer.id);
                    vm.DashboardModel.Dashboard.Report[indexOfReport].axisConfig = new Object(vm.Attributes);
                    vm.DashboardModel.Dashboard.Report[indexOfReport].axisConfig["xLabel"] = angular.copy(vm.activeXLabel[vm.DashboardModel.Dashboard.activeReport.reportContainer.id]);
                    vm.DashboardModel.Dashboard.Report[indexOfReport].axisConfig["yLabel"] = angular.copy(vm.activeYLabel[vm.DashboardModel.Dashboard.activeReport.reportContainer.id]);
                    if (drawn) {
                        // drawn["chartTypeAttribute"]=vm.DashboardModel.Dashboard.activeReport.chart.name;
                        // drawn["resize"]="ON";
                        vm.DashboardModel.Dashboard.Report[indexOfReport]["chartObject"] = drawn;
                    }
                    vm.DashboardModel.Dashboard.activeReport = vm.DashboardModel.Dashboard.Report[indexOfReport];
                    vm.DashboardModel.Dashboard.activeReportIndex = indexOfReport;
                },
                resetDataAndChart:function(){
                    return new Promise(
                        function (resolve, reject) {
                            eChart.resetAll();
                            setTimeout(function(){
                                resolve();
                            },100);
                        }
                    );
                },
                processChart: function () {
                    vm.chartLoading = false;
                    return new Promise(
                        function (resolve, reject) {
                            try {
                                if (!$.isEmptyObject(vm.rangeObject)) {
                                    vm.Attributes['timeline'] === vm.rangeObject;
                                    sketch
                                        .axisConfig(vm.Attributes)
                                        .data(vm.filteredData)
                                        .container(vm.DashboardModel.Dashboard.activeReport.reportContainer.id)
                                        .chartConfig($scope.selectedChartInView[vm.DashboardModel.Dashboard.activeReport.reportContainer.id])
                                        .render(
                                            function (drawn) {
                                                $scope.lastDrawnChart[vm.DashboardModel.Dashboard.activeReport.reportContainer.id] = vm.DashboardModel.Dashboard.activeReport.chart.key;
                                                resolve(drawn);
                                            });
                                }else {
                                    sketch
                                        .axisConfig(vm.Attributes)
                                        .data(vm.tableData)
                                        .container(vm.DashboardModel.Dashboard.activeReport.reportContainer.id)
                                        .chartConfig($scope.selectedChartInView[vm.DashboardModel.Dashboard.activeReport.reportContainer.id])
                                        .render(
                                            function (drawn) {
                                                $scope.lastDrawnChart[vm.DashboardModel.Dashboard.activeReport.reportContainer.id] = vm.DashboardModel.Dashboard.activeReport.chart.key;
                                                resolve(drawn);
                                            });
                                }
                            } catch (e) {

                            }
                        }
                    );
                },
                renderChartInActiveContainer: function () {
                    sketch.pivotTableConfig = undefined;
                    try {
                        vm.DashboardView
                            .resetDataAndChart()
                            .then(vm.DashboardView.processChart)
                            .then(vm.DashboardView.updateDashboardObject)
                            .then(function () {
                                vm.chartLoading = false;
                            })
                            .then(function () {
                                var variable = "reportLoading_" + vm.DashboardModel.Dashboard.activeReport.reportContainer.id;
                                vm[variable] = false;
                                $(".label_legends" + vm.DashboardModel.Dashboard.activeReport.reportContainer.id).show();
                                return true;
                            });
                    } catch (e) {
                        vm.chartErrorText = "Some Error occured while drawing this chart";
                        vm.chartLoading = false;
                    }
                    return false;
                },
                initDashboardProto: function () {
                    $scope.dashboardPrototype = vm.DashboardController.getDashboardProto();
                },
                // render dashboard view
                addReportContainerView: function (reportContainerObject) {
                    if(/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
                        if(reportContainerObject.chartType!="Number Widget"){
                            reportContainerObject.minSizeX = 30;
                            reportContainerObject.minSizeY = 25;
                        }else{
                            reportContainerObject.minSizeY = 10;
                        }
                    }

                    if(reportContainerObject.chartType == 'Male & Female Comparison'){
                        // eChart.resetAll();
                    }
                    if($scope.dashboardPrototype.reportWithTab==undefined){
                        $scope.dashboardPrototype.reportWithTab={};
                    }
                    if($scope.dashboardPrototype.reportWithTab[reportContainerObject.tab]==undefined){
                        $scope.dashboardPrototype.reportWithTab[reportContainerObject.tab]=[];
                    }
                    $scope.dashboardPrototype.reportWithTab[reportContainerObject.tab].push(reportContainerObject)
                    $scope.dashboardPrototype.reportContainers.push(reportContainerObject);
                    /*if(reportContainerObject.chartType=="Number Widget"){
                        if(vm.reportContainerMobile[0]==undefined){
                            vm.reportContainerMobile[0]=[];
                            vm.reportContainerMobile[0].push(reportContainerObject);
                        }else if(vm.reportContainerMobile[0] instanceof Array){
                            vm.reportContainerMobile[0].push(reportContainerObject);
                        }else{
                            vm.reportContainerMobile.push(vm.reportContainerMobile[0]);
                            vm.reportContainerMobile[0]=[];
                            vm.reportContainerMobile[0].push(reportContainerObject);
                        }
                    }else{
                        vm.reportContainerMobile.push(reportContainerObject);
                    }*/
                },
                selectReportContainerView: function (reportContainerObject) {

                },
                showGridBorder: function () {
                    $("#dash-border").addClass("dash-border");
                },
                removeGridBorder: function () {
                    $("#dash-border").removeClass("dash-border");
                }
            }
            vm.DashboardController.init();

            $("#loadscreen").show();
            $scope.listdata = [];
            $scope.listOf = false;
            $scope.dashName.dashboardName = "";
            $scope.dashboardDesc = "Dashboard Description"
            vm.containerLoaded = false;
            vm.isSmallSidebar = true;
            vm.isParameterShown = false;
            vm.isNoContainerLoaded = true;
            vm.ischartTypeShown = false;
            vm.isQuerySelected = true;
            vm.placeholder = true;
            vm.filtersTo = [];
            vm.lastAppliedFilters = {};
            vm.saveState = -1;
            // charts list
            vm.MetaDataLoader = true;
            // Function for fetching metadata list
            var dashboardView = function () {
                var promise = new Promise(
                    function (resolve, reject) {
                        /*dataFactory.request($rootScope.DashboardList_Url,'post',"").then(function(response) {
                         if(response.data.errorCode==1){
                         $scope.viewLoaded = true;
                         $scope.viewLoad = true;
                         vm.dashboardList = response.data.data;
                         resolve();
                         }else{
                         dataFactory.errorAler(response.message);
                         }
                         },
                         function(error) {
                         $scope.status = 'Unable to load customer data: '+ error.message;
                         });*/
                    });
                return promise;
            };
            vm.companyHeader='no';
            vm.CommonSettingData = function(CommonSettingObject){
                //define style
                var mainHeader = CommonSettingObject.main_Header;
                var reportHeader = CommonSettingObject.report_Header;
                var headerTextcolor = CommonSettingObject.headerTextcolor;
                var headerBgcolor = CommonSettingObject.headerBgcolor;
                var companyHeaderBg=CommonSettingObject.companyBackground;
                var companyHeaderColor=CommonSettingObject.companyColor;
                vm.checkSettingSave=true;
                var style = {};
                style.mainHeader = mainHeader;
                style.reportHeader = reportHeader;
                style.headerTextcolor = headerTextcolor;
                style.headerBgcolor = headerBgcolor;
                style.companyHeaderBg=companyHeaderBg;
                style.companyHeaderColor=companyHeaderColor;
                style.border = CommonSettingObject.border;
                style.borderColor = CommonSettingObject.border_color;
                style.borderWidth = CommonSettingObject.border_width;
                //Save common setting object
                vm.DashboardModel.Dashboard['commonStyle']=style;
                vm.DashboardModel.Dashboard['companyHeader']=vm.company;
                if(reportHeader == 'yes' || reportHeader == 'Yes'){
                    var common_Heading = { backgroundColor : headerBgcolor + '!important', color : headerTextcolor + '!important' };
                    $(".Common_Head").css(common_Heading);
                }else{
                    //$(".Common_Head").css("display", "none");
                }
                if(mainHeader=="yes"){
                    var common_Heading = { backgroundColor : CommonSettingObject.companyBackground, color : CommonSettingObject.companyColor};
                    $(".companyHeader").css(common_Heading);
                    vm.companyHeader='yes';
                    $(".companyHeader").show();
                }else{
                    vm.companyHeader='no';
                    $(".companyHeader").hide();
                }
                if(CommonSettingObject.border=='yes'){
                    var common_Heading = { border : CommonSettingObject.border_width+" solid "+" "+CommonSettingObject.border_color};
                    $(".li_box").css(common_Heading);
                }
                $('#Common_View_Setting').modal('hide');
                if(vm.company==undefined){
                    vm.company={};
                }
                if(vm.company.name==undefined || vm.company.name==""){
                    vm.company.name="Thinklytics";
                }
                if(vm.company.description==undefined || vm.company.description==""){
                    vm.company.description="Business intelligence and analytics";
                }
            }
            // Custom Setting
            vm.viewCustomSetting = function(index){
                vm.Custom_Container_Id = index;
                $('#Custom_View_Setting').modal('show');
            }
            // Comapny Setting
            vm.companySet={};
            vm.dataimg = 'none';
            vm.logoCheck=false;
            vm.companyHeaderSave=function(company){
                vm.logoCheck=true;
                var f = document.getElementById('logo').files[0];
                var r = new FileReader();
                r.onloadend = function(e) {
                    $scope.data = e.target.result;
                    var img = document.getElementById('img');
                    vm.logo = 'data:image/jpeg;base64,' + btoa(e.target.result);
                    img.src = 'data:image/jpeg;base64,' + btoa(e.target.result);
                    //$("#img").appendChild(img);
                }
                r.readAsBinaryString(f);
                vm.company.name=company.name;
                vm.company.description=company.description;
                vm.company.log=company.logo;
                $('#companyHeaderSet').modal('hide');
            }
            // Api For Getting Connection

            /*dataFactory.request($rootScope.MetadataList_Url,'post',"").then(function (response) {
             if(response.data.errorCode==1){
             vm.queryList = JSON.parse(response.data.result);
             }else{
             dataFactory.errorAlert(response.data.message);
             }
             }).then(function () {
             if ($stateParams.id != undefined) {
             var metadataId = $stateParams.id;

             var i = 0;
             var index;
             $scope.queryList.forEach(function (d, indexArray) {
             if (d.metadataId == metadataId) {
             index = indexArray;
             }
             });
             $scope.querySelector = JSON.stringify($scope.queryList[index]);
             //$scope.selectDataSource(JSON.stringify($scope.queryList[index]));
             $(".modal-backdrop").hide();
             }
             });*/
            // creating widgets
            var lastcontainerId;
            var lastcount = 0;
            var newDim = [];
            // Filter Drawer
            vm.filterBy = function (item, filter,index) {
                var totalLength=Object.keys(vm.allFilters).length;
                if(vm.tableData<$rootScope.localDataLimit){
                    sketch.applyFilter(item, filter);
                }else{
                    sketchServer.applyFilter(item, filter, vm.metadataId,totalLength,index,function(data,filterObj,status){
                        if(status){
                            vm.conatinerFilterTo[filterObj.key]=data;
                            vm.allFilters[filterObj.key].filterValue = data;
                            // vm.allFilters[filterObj.key]['filterValue']=data;
                            $scope.$apply();
                            $("[id^=filterSelect_]").each(function () {
                                if($(this).attr('id')!="filterSelect_"+index) {
                                    $(this).selectpicker('destroy', true);
                                }
                            })

                            setTimeout(function(){
                                $("[id^=filterSelect_]").each(function () {
                                    if($(this).attr('id')!="filterSelect_"+index){
                                        $(this).selectpicker();
                                    }
                                })
                            },1000);
                        }
                    });
                }
            }

            // delete bhavesh
            $scope.deleteGraphKeys = function (id) {
                $scope.deleteReportContainer(id);
                dataFactory.successAlert("Report deleted successfully");
            }
            $scope.findIndexOfReportContainer = function (id) {
                var index = -1;
                var i = 0;
                $scope.dashboardPrototype.reportContainers.forEach(function (d) {
                    if (d.id == id)
                        index = i;
                    i++;
                });
                return index;
            }
            $scope.findIndexOfReportFromReportContainer = function (id) {
                var index = -1;
                var i = 0;
                vm.DashboardModel.Dashboard.Report.forEach(function (d) {
                    if (d.id == id)
                        index = i;
                    i++;
                });
                return index;
            }
            $scope.deleteReportContainer = function (id) {
                var index = $scope.findIndexOfReportContainer(id);
                vm.Attributes = {};
                $scope.dashboardPrototype.reportContainers.splice(index, 1);
                vm.DashboardModel.Dashboard.Report.splice(index, 1);
            }
            // Filter column Name
            $scope.filterColumn = function (columnName) {
                $scope.filterColumnName = columnName;
            }

            //End draw chart andcontainer
            //for range object
            //Get array list
            vm.getArrayList = function (numberColumn) {
                var numberArray = [];
                for (var i = 1; i <= numberColumn; i++) {
                    numberArray.push(i);
                }
                return numberArray;
            }
            $scope.filterRangeColumn = function (index, columnName) {
                $scope.filterColumnRange = {};
                $scope.filterColumnRange['index'] = index;
                $scope.filterColumnRange['columnName'] = columnName;
            }
            //ENd

            //New dashboard edit
            vm.dashboardEditDraw={
                init:function(){
                    //For Filter
                    vm.filerTemp = [];
                    vm.filtersToApply = [];
                    var data={
                        "auth_key":auth_key,
                        "comId":comId
                    };
                    dataFactory.request($rootScope.PublicViewObject_url,'post',data).then(function(response){
                        /*
                         * Company create and update time
                         *
                         */
                        vm.sharedViewResponse = JSON.parse(response.data.result.reportObject);
                        vm.categoryGroupObject=JSON.parse(response.data.result.group);
                        vm.sharedViewReport = vm.sharedViewResponse.Report;
                        vm.tabs=vm.sharedViewResponse.tabs;
                        if(vm.tabs==undefined){
                            vm.tabs=[0];
                        }
                        vm.pdf_Reports = [], vm.excel_Reports = [];
                        vm.sharedViewReport.forEach(function(d){
                            if(d.chart.key == '_pivotTable' || d.chart.key == '_dataTable'){
                                vm.excel_Reports.push(d.reportContainer);
                            }else{
                                vm.pdf_Reports.push(d.reportContainer);
                            }
                        });

                        if(response.data.errorCode==1){
                            vm.companyDateObj={};
                            if(response.data.result.created_at){
                                vm.companyDateObj.created_at=$filter('date')(new Date(response.data.result.created_at.split('-').join('/')), "d/M/yyyy");
                                vm.companyDateObj.updated_at=$filter('date')(new Date(response.data.result.updated_at.split('-').join('/')), "d/M/yyyy");
                            }else{
                                vm.companyDateObj.created_at="";
                                vm.companyDateObj.updated_at="";
                            }
                            $scope.dashName.dashboardName = response.data.result.name;
                            var reportObject=JSON.parse(response.data.result.reportObject);
                            vm.sharedViewId=response.data.result.id;
                            vm.logoPath=dataFactory.baseUrlData()+"dashboardImage/"+response.data.result.logo+".png";
                            if(reportObject.companyHeader!=undefined){
                                vm.company=reportObject.companyHeader;
                                vm.company.logo = vm.logoPath;
                            }
                            //CHeck range object
                            var index =vm.dashboardEditDraw.isTimeObjectFound(reportObject.Report);
                            if(index!=-1){
                                vm.rangeObject = reportObject.Report[index].axisConfig.timeline;
                                Object.keys(vm.rangeObject).forEach(function(d){
                                    vm.dashboardEditDraw.rangeFilter(vm.rangeObject[d].key.columnName, vm.rangeObject[d].numberOfColumn);
                                });
                            }
                            vm.tempFilter=JSON.parse(response.data.result.filterBy);

                            //Change object for different between object
                            vm.selectedFilter=[];
                            vm.allFilters={};
                            if(vm.tempFilter.length){
                                vm.tempFilter.forEach(function(d){
                                    vm.allFilters[d.key]=[];
                                    if(d.key==undefined){
                                        var tempObj={};
                                        tempObj['key']=d.columnName;
                                        tempObj['value']=d.columType
                                        vm.selectedFilter.push(tempObj);
                                    }else{
                                        vm.selectedFilter.push(d);
                                    }
                                });
                            }
                            vm.querySelector=reportObject.metadataObject;
                            vm.metadataSelect={};
                            vm.dashboardEditDraw.selectDataSource(vm.querySelector);
                            vm.DashboardModel.Dashboard=Object.assign({},reportObject);
                            vm.metadataObject=reportObject.metadataObject;
                            vm.metadataId=vm.metadataObject.metadataId;
                            vm.group=JSON.parse(response.data.result.group);
                            vm.dashboardId=response.data.result.dashboard_id;
                            vm.dashboardEditDraw.fetchDataAndColumns().then(function(){
                                //Add Conatiner draw chart
                                vm.dashboardEditDraw.addContainerDrawChart(reportObject);
                                vm.company=reportObject.companyHeader;
                                vm.CommonSettingObject={};
                                //Draw header
                                vm.dashboardEditDraw.drawHeader(reportObject);
                            }).then(function(){
                                vm.checkboxModel = JSON.parse(response.data.result.filterBy);
                                //vm.selectedFilter = JSON.parse(response.data.result.filterBy);
                                if(vm.checkboxModel.length==0){
                                    $(".filter").hide();
                                }
                                vm.dashboardEditDraw.simpleFilterContainerAdd();
                                vm.preLoad = false;
                                vm.graphAxis = true;
                                vm.loadingBarDrawer = false;
                                vm.loadingBar=true;
                                vm.loadingBarView=true;
                                var rawData=vm.tableData;
                                //Filter Draw call
                            })
                        }else{
                            dataFactory.errorAlert(response.data.message);
                        }
                    });

                },
                fetchDataAndColumns:function () {
                    var promise = new Promise(
                        function (resolve, reject) {
                            vm.metadataObject['sharedViewId']=vm.sharedViewId;
                            var data = {
                                "matadataObject":JSON.stringify(vm.metadataObject),
                                "type":'dashboard'
                            };
                            /*
                                Get length for server side check
                             */
                            dataFactory.request($rootScope.dataLength_Url,'post',data).then(function(response) {
                                vm.dataCount=response.data.result;
                                if(vm.dataCount<$rootScope.localDataLimit){
                                    var data = {
                                        "matadataObject":JSON.stringify(vm.metadataObject),
                                        "type":'dashboard'
                                    };
                                    dataFactory.request($rootScope.PublicviewDashboardData_Url,'post',data).then(function(response){
                                        if(response.data.errorCode==1){
                                            vm.blnkData = true;
                                            vm.blnkFilter = true;
                                            vm.blnkChart = true;
                                            vm.blnkSource = true;
                                            vm.tableData = JSON.parse(response.data.result.tableData);
                                            vm.metadataObject = vm.metadataObject;
                                            vm.tableColumns = vm.metadataObject.connObject.column;
                                            // $scope.groupObject
                                            calculation
                                                .oldObject(JSON.parse(response.data.result.tableColumn))
                                                .newObject(vm.tableColumns)
                                                .tabledata(vm.tableData)
                                                .uniqueArray()
                                                .columnCal()
                                                .groupObject(vm.group)
                                                .columnCategory();
                                            vm.tableData = calculation._tabledata;
                                            setTimeout(function () {
                                                $("#filter").multiselect("destroy");
                                                $("#filter").multiselect({
                                                    includeSelectAllOption: false,
                                                    enableFiltering: true,
                                                    maxHeight: 200
                                                });
                                            }, 1000);
                                            resolve();
                                        }else{
                                            dataFactory.errorAlert(response.data.message);
                                        }
                                    });
                                }else{
                                    new Fingerprint2().get(function(result, components) {
                                        $rootScope.accessToken = result;
                                    });
                                    var data = {
                                        "matadataObject":JSON.stringify(vm.metadataObject),
                                        "type":'dashboard',
                                        "comId":comId
                                    };
                                    /*
                                     * Only for cache data to server
                                     */
                                    var responseColumn="";
                                    dataFactory.request($rootScope.DashboardDataCacheToServer_Url,'post',data).then(function(response){
                                        if(response.data.errorCode==1){
                                            vm.blnkData = true;
                                            vm.blnkFilter = true;
                                            vm.blnkChart = true;
                                            vm.blnkSource = true;
                                            responseColumn=JSON.parse(response.data.result.tableColumn);
                                            /*
                                             *  Resolve table column to show
                                             */
                                        }else{
                                            dataFactory.errorAlert(response.data.errorCode);
                                        }
                                    }).then(function(){
                                        function checkPort(url){
                                            var port=configData.nodeUrl.split(":");
                                            if(port[2]==""){
                                                return true;
                                            }
                                            return false;
                                        }
                                        if(checkPort(configData.nodeUrl)){
                                            configData.nodeUrl=configData.nodeUrl+""+userPort;
                                        }
                                        /*
                                         * Update all url port
                                         */
                                        sketchServer.nodeApiUrl=configData.nodeUrl;
                                        eChartServer.nodeApiUrl=configData.nodeUrl;
                                        calculateServer.apiUrl=configData.nodeUrl;
                                        calculationServer.apiUrl=configData.nodeUrl;
                                        $rootScope.initializeNodeClient(vm.metadataObject).then(function(){
                                            if(vm.metadataObject.connObject.column!=undefined)
                                                vm.tableColumns = Object.assign([],vm.metadataObject.connObject.column);
                                            else
                                                vm.tableColumns = Object.assign([],vm.metadataObject.column);
                                            if(vm.categoryGroupObject==undefined)
                                            {
                                                vm.categoryGroupObject={};
                                            }
                                            calculationServer
                                                .oldObject(responseColumn)
                                                .newObject(vm.tableColumns)
                                                .dataGroupCalculation(vm.metadataId,$rootScope.accessToken,vm.categoryGroupObject);
                                        }).then(function(){
                                            resolve(vm.metadataObject);
                                        });
                                    });
                                }
                            });
                        });
                    return promise;
                },
                selectDataSource:function (model) {
                    vm.dashboardValidation = true;
                    vm.loadingBarDrawer = true;
                    vm.tableData = [];
                    vm.tableColumnsMeasure = [];
                    sketch._crossfilter = null;
                    sketch._data = null;
                    vm.filtersToApply = [];
                    lastcount = 0;
                    // resetAllAxis();
                    if (model != undefined) {
                        vm.ischartTypeShown = true;
                        vm.isQuerySelected = false;
                        vm.graphAxis = false;
                        vm.queryObj = model;
                        vm.dashboardValidation = true;
                        vm.dashboardValidation1 = true;
                        vm.showInfo1 = false;
                        vm.showInfo2 = true;
                        vm.preLoad = true;
                        vm.groupObject = "";
                        vm.metadataObject = model;
                    }
                    vm.moveToMdArray = {};
                    vm.moveToDimMeasure = function (moveTo, columnName, isChecked) {
                        if(!isChecked){
                            vm.tableColumns.filter(function (d, is) {
                                if (d.columnName == columnName) {
                                    vm.tableColumns[is].dataKey = moveTo;
                                    return true;
                                }
                                $scope.moveToMdArray[columnName] = moveTo
                                return false;
                            });
                        }else{
                            alert("Can't Move to "+moveTo+" If it's selected");
                        }
                    }
                    $("#filter").multiselect("destroy");
                    $("#filter").multiselect({
                        includeSelectAllOption: false,
                        enableFiltering: true,
                        maxHeight: 200
                    });
                },
                drawHeader:function(reportObject){
                    if(reportObject.commonStyle && reportObject.commonStyle.reportHeader=='yes'){
                        var common_Heading = { backgroundColor : reportObject.commonStyle.headerBgcolor, color : reportObject.commonStyle.headerTextcolor};
                        setTimeout(function(){
                            vm.CommonSettingObject.report_Header=reportObject.commonStyle.reportHeader;
                            vm.CommonSettingObject.headerTextcolor=reportObject.commonStyle.headerTextcolor;
                            vm.CommonSettingObject.headerBgcolor=reportObject.commonStyle.headerBgcolor;
                            $(".Common_Head").css(common_Heading);
                        },1);
                    }else if(reportObject.commonStyle && reportObject.commonStyle.reportHeader=='no'){
                        vm.CommonSettingObject.report_Header="no";
                        $(".commonStyle").css("display", "none");
                    }else{
                        $(".defaultColor").css("background-color", "#CCC");
                    }
                    setTimeout(function(){
                        if(vm.CommonSettingObject && vm.CommonSettingObject.headerBgcolor){
                            if(vm.CommonSettingObject.headerBgcolor.match(/fff/g)){
                                $(".defaultColor").css("background-color", "#CCC");
                            }else{

                                $(".defaultColor").css("background-color", "inherit");
                            }
                        }else{
                            $(".defaultColor").css("background-color", "#CCC !important");
                        }
                    },1000);
                    if(reportObject.commonStyle && reportObject.commonStyle.mainHeader=="yes"){
                        var company_Heading = { backgroundColor : reportObject.commonStyle.companyHeaderBg, color : reportObject.commonStyle.companyHeaderColor};
                        vm.companyHeader='yes';
                        vm.CommonSettingObject.main_Header=reportObject.commonStyle.mainHeader;
                        vm.CommonSettingObject.companyColor=reportObject.commonStyle.companyHeaderColor;
                        vm.CommonSettingObject.companyBackgroud=reportObject.commonStyle.companyHeaderBg;
                        $(".companyHeader").css(company_Heading);
                        $(".defaultHeader").hide();
                    }else{
                        vm.CommonSettingObject.main_Header='no';
                        $(".companyHeader").css("display", "none");
                    }
                    vm.CommonSettingObject.border_width="1px";
                    vm.CommonSettingObject.border='no';
                    if(reportObject.commonStyle && reportObject.commonStyle.border=='yes'){
                        var common_border = { border : reportObject.commonStyle.borderWidth+" solid "+" "+reportObject.commonStyle.borderColor};
                        setTimeout(function(){
                            vm.CommonSettingObject.border=reportObject.commonStyle.border;
                            vm.CommonSettingObject.border_color=reportObject.commonStyle.borderColor;
                            vm.CommonSettingObject.border_width=reportObject.commonStyle.borderWidth;
                            $(".li_box").css(common_border);
                        },1);
                    }
                    vm.colorObject=reportObject.colorObject;
                    eChart.chartRegistry.registerAllAttributes(reportObject.colorObject);
                },
                loadingBar:function(variable){
                    vm.reportResponseCount++;
                    setTimeout(function(){
                        vm[variable] = false;
                        if(!$scope.$$phase) {
                            $scope.$apply();
                        }
                    },1000);
                    if(vm.reportResponseCount == vm.reportCount){
                        setTimeout(function(){
                            $( "#tabBtn_"+vm.tabKeyActive+"> a" ).trigger( "click" );
                        },2000);
                    }
                    //setTimeout(function(){
                    //  $(".loadingBar").hide();
                    //},6000);
                },
                addContainerDrawChart:function(reportObject){
                    var reportCount = reportObject.Report.length;
                    vm.reportCount = reportObject.Report.length;
                    sketchServer._totalReportCount = reportCount;
                    var j = 0;
                    vm.reportResponseCount = 0;
                    reportObject.Report.forEach(function(d,i) {
                        var variable = 'reportLoading_'+d.reportContainer.id;
                        vm[variable] = true;
                        vm.DashboardView.addReportContainerView(d.reportContainer);
                        $scope.$apply();
                        vm.dashboardEditDraw.checkContainerSize(d);
                        if(!$scope.$$phase){
                            $scope.$apply();
                        }
                        if(vm.dataCount<$rootScope.localDataLimit){
                            //Render chart
                            if(d.chart.key=="_text"){
                                sketch
                                    .container(d.reportContainer.id)
                                    ._text(d.chart.value);
                                vm.dashboardEditDraw.loadingBar(variable);
                            }
                            else if(d.chart.key=="_maleFemaleChartJs"){
                                sketch.axisConfig(d.axisConfig)
                                    .data(vm.tableData)
                                    .container(d.reportContainer.id)
                                    ._maleFemaleChartDraw();
                                vm.dashboardEditDraw.loadingBar(variable);
                            }
                            else if(d.chart.key=="_PivotCustomized"){
                                sketch.axisConfig(d.axisConfig)
                                    .data(vm.tableData)
                                    .container(d.reportContainer.id)
                                    .pivotCust_Draw();
                                vm.dashboardEditDraw.loadingBar(variable);
                            }
                            else if(d.chart.key=="_mapChartJs"){
                                sketch.axisConfig(d.axisConfig)
                                    .data(vm.tableData)
                                    .container(d.reportContainer.id)
                                    ._mapChartDraw();
                                vm.dashboardEditDraw.loadingBar(variable);
                            }
                            else if(d.chart.key=="_bubbleChartJs"){
                                if(d.colorObject && d.colorObject["chart-"+d.reportContainer.id]){
                                    eChart.chartRegistry.registerPublicViewColor("chart-"+d.reportContainer.id,d.colorObject["chart-"+d.reportContainer.id]);
                                    sketch.axisConfig(d.axisConfig)
                                        .data(vm.tableData)
                                        .container(d.reportContainer.id)
                                        ._bubbleChartJs();
                                    vm.dashboardEditDraw.loadingBar(variable);
                                }
                            }
                            else if(d.chart.key=="_floorPlanJs"){
                                sketch.axisConfig(d.axisConfig)
                                    .data(vm.tableData)
                                    .container(d.reportContainer.id)
                                    ._floorPlanDraw();
                                vm.dashboardEditDraw.loadingBar(variable);
                            }
                            else{
                                if(d.colorObject && d.colorObject["chart-"+d.reportContainer.id])
                                    eChart.chartRegistry.registerPublicViewColor("chart-"+d.reportContainer.id,d.colorObject["chart-"+d.reportContainer.id]);
                                sketch
                                    .axisConfig(d.axisConfig)
                                    .data(vm.tableData)
                                    .container(d.reportContainer.id)
                                    .chartConfig(d.chart)
                                    .render(function(redraw){
                                        vm.dashboardEditDraw.loadingBar(variable);
                                    });
                            }
                            if(i==0){
                                vm.Attributes=d.axisConfig;
                            }
                            d.reportContainer.chartType=d.chart.name;
                            /*var width=$("#dashboardContainer_"+d.reportContainer.id).width();
                             var height=$("#dashboardContainer_"+d.reportContainer.id).height();*/
                        }else{
                            //Render chart
                            if(d.chart.key=="_text"){
                                sketch
                                    .container(d.reportContainer.id)
                                    ._text(d.chart.value);
                                vm.dashboardEditDraw.loadingBar(variable);
                            }
                            else if(d.chart.key=="_maleFemaleChartJs"){
                                sketchServer
                                    .accessToken($rootScope.accessToken)
                                    .axisConfig(d.axisConfig)
                                    .data([])
                                    .container(d.reportContainer.id)
                                    .chartConfig(d.chart)
                                    ._maleFemaleChartDraw();
                                vm.dashboardEditDraw.loadingBar(variable);
                            }
                            else if(d.chart.key=="_PivotCustomized"){
                                sketchServer
                                    .accessToken($rootScope.accessToken)
                                    .axisConfig(d.axisConfig)
                                    .data([])
                                    .container(d.reportContainer.id)
                                    .chartConfig(d.chart)
                                    .pivotCust_Draw();
                                vm.dashboardEditDraw.loadingBar(variable);
                            }
                            else if(d.chart.key=="_mapChartJs"){
                                sketchServer
                                    .accessToken($rootScope.accessToken)
                                    .axisConfig(d.axisConfig)
                                    .data([])
                                    .container(d.reportContainer.id)
                                    .chartConfig(d.chart)
                                    ._mapChartDraw();
                                vm.dashboardEditDraw.loadingBar(variable);
                            }
                            else if(d.chart.key=="_bubbleChartJs"){
                                if(d.colorObject && d.colorObject["chart-"+d.reportContainer.id] && Object.keys(d.colorObject["chart-"+d.reportContainer.id]).length) {
                                    eChartServer.chartRegistry.registerPublicViewColor("chart-" + d.reportContainer.id, d.colorObject["chart-" + d.reportContainer.id]);
                                }
                                sketchServer
                                    .accessToken($rootScope.accessToken)
                                    .axisConfig(d.axisConfig)
                                    .data([])
                                    .container(d.reportContainer.id)
                                    .chartConfig(d.chart)
                                    ._bubbleChartJs();
                                vm.dashboardEditDraw.loadingBar(variable);

                            }
                            else if(d.chart.key=="_floorPlanJs"){
                                sketch.axisConfig(d.axisConfig)
                                    .data(vm.tableData)
                                    .container(d.reportContainer.id)
                                    ._floorPlanDraw();
                                vm.dashboardEditDraw.loadingBar(variable);
                            }
                            else{
                                if(d.colorObject && d.colorObject["chart-"+d.reportContainer.id] && Object.keys(d.colorObject["chart-"+d.reportContainer.id]).length){
                                    setTimeout(function () {
                                        eChartServer.chartRegistry.registerPublicViewColor("chart-" + d.reportContainer.id, d.colorObject["chart-" + d.reportContainer.id]);
                                        sketchServer
                                            .accessToken($rootScope.accessToken)
                                            .axisConfig(d.axisConfig)
                                            .data([])
                                            .container(d.reportContainer.id)
                                            .chartConfig(d.chart)
                                            .render(function (redraw) {
                                                vm.dashboardEditDraw.loadingBar(variable);
                                            });
                                    },500);
                                }else{
                                    setTimeout(function () {
                                        sketchServer
                                            .accessToken($rootScope.accessToken)
                                            .axisConfig(d.axisConfig)
                                            .data([])
                                            .container(d.reportContainer.id)
                                            .chartConfig(d.chart)
                                            .render(function (redraw) {
                                                vm.dashboardEditDraw.loadingBar(variable);
                                            });
                                    },500)
                                }
                            }
                            if(i==0){
                                vm.Attributes = d.axisConfig;
                            }
                            d.reportContainer.chartType=d.chart.name;
                            // if(reportCount==i+1){
                            // 	setTimeout(function(){
                            // },4000);
                            // }
                        }
                    });
                },
                checkContainerSize:function(d){
                    if(d.chart.key!="_numberWidget"){
                        /*if(/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
                            var windowWidth=$(window).width()*98/100;
                            var windowHeight=$(window).height()*70/100;
                            $("#li_"+d.reportContainer.id).height(windowHeight);
                            $("#li_"+d.reportContainer.id).width(windowWidth);
                            var getHeight= windowHeight-130;
                            var getWidth= windowWidth-40;
                        }else{*/
                        var getHeight= ($("#li_"+d.reportContainer.id).height())-97;
                        var getWidth= $("#li_"+d.reportContainer.id).width()-40;
                        /*}*/
                        $("#chart-"+d.reportContainer.id).height(getHeight);
                        $("#chart-"+d.reportContainer.id).width(getWidth);
                        //document.getElementById("left"+vm.DashboardModel.Dashboard.activeReport.reportContainer.id).style.height = height;
                        var legendHeight=$("#li_"+d.reportContainer.id).height();
                        $("#left"+d.reportContainer.id).height(legendHeight*60/100);
                        //Chart Hide
                        var activeReport = vm.DashboardModel.Dashboard.Report[d.reportContainer.id];
                        if(vm.DashboardModel.Dashboard.activeReport.chart.key!="_mapChartjs"){
                            $("#mapbox-"+d.reportContainer.id).height(getHeight);
                            $("#mapbox-"+d.reportContainer.id).width(getWidth);
                        }
                        $("#chart-" + d.reportContainer.id).show();
                        var variable = "reportLoading_" + d.reportContainer.id;
                        //vm[variable] = false;
                        $scope.chartLoading = false;
                        $("#chart-" + d.reportContainer.id).show();
                        $scope.selectedChartInView[d.reportContainer.id] = d.chart;
                    }
                },
                isTimeObjectFound:function(reportArray){
                    var flag=-1;
                    reportArray.forEach(function(d,index){
                        if(d.axisConfig.timeline!=undefined){

                            flag=index;
                        }
                    });
                    return flag;
                },
                filterAddDashboard:function () {
                    vm.filerTemp = [];
                    vm.filtersToApply = [];
                    vm.filterHSApply = true;
                    Object.keys(vm.checkboxModel).forEach(function (d) {
                        var columnObj = "";
                        columnObj = JSON.parse(vm.checkboxModel[d]);
                        var keyValue = {};
                        keyValue['key'] = columnObj.columnName;
                        keyValue['value'] = columnObj.columType;
                        vm.filerTemp.push(keyValue);
                    });
                    vm.dashboardEditDraw.filterDargDrop();
                },
                reportDragDrop:function(){

                },
                filterDargDrop:function(){
                    $('.droppableAddFilter').draggable({
                        cancel : "a.ui-icon", // clicking an icon
                        revert : true, // bounce back when dropped
                        helper : "clone", // create "copy" with
                        cursor : "move",
                        revertDuration : 0
                    });
                    $('.droppableFilter').droppable({
                        accept : ".droppableAddFilter",
                        activeClass : "ui-state-highlight",
                        drop : function(event, ui) {
                            var div = $(ui.draggable)[0];
                            vm.selectedFilter=[];
                            var filter = JSON.parse($(div).find('input').val());
                            vm.selectedFilter.push(filter);
                            vm.dashboardEditDraw.simpleFilterContainerAdd();
                        }
                    });
                },
                simpleFilterContainerAdd:function(){
                    //vm.filtersTo[columnObj.columnName] = [];
                    if(vm.dataCount<$rootScope.localDataLimit){
                        var columnObj = "";
                        Object.keys(vm.selectedFilter).forEach(function (d) {
                            columnObj = vm.selectedFilter[d];
                            var keyValue = columnObj;
                            vm.conatinerFilter.push(keyValue);
                            vm.conatinerFilterTo[columnObj.key] = [];
                            Enumerable.From(vm.tableData)
                                .Distinct(function(x){
                                    return x[columnObj.key];
                                })
                                .Select(function(x){return x[columnObj.key];})
                                .ToArray()
                                .forEach(function (e) {
                                    vm.conatinerFilterTo[columnObj.key].push(e);
                                });
                        });
                        $scope.$apply();
                        setTimeout(function () {
                            setTimeout(function(){
                                $(".filterSelect").selectpicker();
                            },1000);
                            Object.keys(vm.selectedFilter).forEach(function (d) {
                                columnObj = vm.selectedFilter[d];
                                if(columnObj.value == "int" || columnObj.value == "float" || columnObj.value == "decimal" || columnObj.value == "double") {
                                    var minObject = _.min(vm.tableData, function (o) {
                                        return o[columnObj.key];
                                    });
                                    var maxObject = _.max(vm.tableData, function (o) {
                                        return o[columnObj.key];
                                    });
                                    var min = minObject[columnObj.key];
                                    var max = maxObject[columnObj.key];
                                    var columnObject = {};
                                    columnObject['key'] = columnObj.key;
                                    columnObject['value'] = columnObj.value;
                                    if(min == null) {
                                        min = 0;
                                    }
                                    if(min==max){
                                        min=min-1;
                                    }
                                    var nonLinearSlider = document.getElementById(columnObj.key.replace("(","(").replace(")",")")+ "-Filter");
                                    noUiSlider.create(nonLinearSlider, {
                                        animate: true,
                                        start: [min, max] ,
                                        connect: true,
                                        range: {
                                            min: parseInt(min),
                                            max: parseInt(max)
                                        },
                                        step: 1,
                                        tooltips: [wNumb({
                                            decimals: 0
                                        }), wNumb({
                                            decimals: 0
                                        })]
                                    });
                                    nonLinearSlider.noUiSlider.on('update', function(values, handle) {
                                        sketch.applyFilter(values,columnObject);
                                    });
                                }else if(columnObj.value == "date"){
                                    vm.dashboardEditDraw.simpleDateFilter();
                                }else if(columnObj.value == "datetime"){
                                    vm.dashboardEditDraw.simpleDateTimeFilter(columnObj);
                                }
                            });
                        }, 1000);
                    }else{
                        var p=Promise.resolve();
                        eChartServer.resetAll();
                        Object.keys(vm.selectedFilter).forEach(function (d,index) {
                            var columnObj= vm.selectedFilter[d];
                            var tempObj={};
                            tempObj['columType']=columnObj['value'];
                            tempObj['tableName']='';
                            tempObj['columnName']=columnObj['key'];
                            tempObj['reName']=columnObj['key'];
                            tempObj['dataKey']='';
                            tempObj['type']='';
                            vm.conatinerFilter.push(columnObj);
                            sketchServer.updateFilters(vm.conatinerFilter);
                            p = p.then(new Promise(function(resolve) {
                                var data = {
                                    columnObject: tempObj,
                                    metadataId: vm.metadataId,
                                    sessionId: $rootScope.accessToken
                                };
                                dataFactory.nodeRequest('getUniqueDataFieldsOfColumn','post',data).then(function(data){
                                    var keyValue = columnObj;
                                    vm.conatinerFilterTo[columnObj.key] = [];
                                    vm.allFilters[columnObj.key]={};
                                    vm.allFilters[columnObj.key]['columnObj']=columnObj;
                                    vm.allFilters[columnObj.key]['filterValue']=[];
                                    data.forEach(function(x){
                                        if(columnObj.value!='datetime' && columnObj.value!='date')
                                            vm.allFilters[columnObj.key]['filterValue'].push(x);
                                        vm.conatinerFilterTo[columnObj.key].push(x);
                                    });
                                    $scope.$apply();
                                    setTimeout(function () {
                                        if (columnObj.value == "int" || columnObj.value == "float" || columnObj.value == "decimal" || columnObj.value == "double") {
                                            var min = Math.min.apply(Math,data);
                                            var max = Math.max.apply(Math,data);
                                            var columnObject = {};
                                            columnObject['key'] = columnObj.key;
                                            columnObject['value'] = columnObj.value;
                                            if (min == null) {
                                                min = 0;
                                            }
                                            if (min == max) {
                                                min = min - 1;
                                            }
                                            var nonLinearSlider = document.getElementById(columnObj.key.replace("(", "(").replace(")", ")") + "-Filter");
                                            noUiSlider.create(nonLinearSlider, {
                                                animate: true,
                                                start: [min, max],
                                                connect: true,
                                                range: {
                                                    min: parseInt(min),
                                                    max: parseInt(max)
                                                },
                                                step: 1,
                                                tooltips: [wNumb({
                                                    decimals: 0
                                                }), wNumb({
                                                    decimals: 0
                                                })]
                                            });
                                            vm.allFilters[columnObject.key]['filterValue']=[min, max];
                                            nonLinearSlider.noUiSlider.on('update', function (values, handle) {
                                                vm.allFilters[columnObject.key]['filterValue']=values;
                                                // sketchServer.applyFilter(values, columnObject,vm.metadataId);
                                            });
                                        } else if (columnObj.value == "date") {
                                            vm.dashboardEditDraw.simpleDateFilter();
                                        }else if(columnObj.value == "datetime"){
                                            vm.dashboardEditDraw.simpleDateTimeFilter(columnObj);
                                        }else{
                                            setTimeout(function () {
                                                $("#filterSelect_"+index).selectpicker();
                                            }, 1000);
                                        }
                                    }, 1000);
                                });
                            }));
                        });
                    }
                },
                rangeFilter:function(colName,numberOfColumn){
                    vm.filtersToApply = [];
                    vm.filterHSApply = true;
                    var columnObj = "";
                    var tempObject = {};
                    var ColumnName = colName.columnName;
                    tempObject['key'] = ColumnName;
                    tempObject['period'] = [];
                    setTimeout(function () {
                        for(var i = 0; i < numberOfColumn; i++){
                            var d = new Date();
                            var n = d.getFullYear();
                            var year = n - i;
                            var dateObject = {
                                "start": year+"-01-01",
                                "end": year+"-12-31"
                            };
                            tempObject['period'].push(dateObject);
                            $(".rangeStartFilter" + i).val("01-01-" + year);
                            $(".rangeEndFilter" + i).val("31-12-" + year);
                        }
                        var setDate = new Date();
                        var rangeStartDate = $('.dateTimeLineStart').datepicker({
                            dateFormat: 'dd-mm-yy',
                            onSelect: function(e) {
                                var setIndex = vm.filterColumnRange.index;
                                var startDate = $(rangeStartDate[setIndex]).val();
                                var endDate = $(rangeEndDate[setIndex]).val();
                                var tempStart = startDate; //31-12-2017
                                var tempEnd = endDate;
                                startDate = tempStart.split("-").reverse().join("-");
                                endDate = tempEnd.split("-").reverse().join("-");
                                setRangeFilter(startDate,endDate);
                            }
                        });
                        var rangeEndDate = $('.dateTimeLineEnd').datepicker({
                            dateFormat: 'dd-mm-yy',
                            onSelect: function(e) {
                                var setIndex = vm.filterColumnRange.index;
                                var startDate = $(rangeStartDate[setIndex]).val();
                                var endDate = $(rangeEndDate[setIndex]).val();
                                var tempStart = startDate; //31-12-2017
                                var tempEnd = endDate;
                                startDate = tempStart.split("-").reverse().join("-");
                                endDate = tempEnd.split("-").reverse().join("-");
                                setRangeFilter(startDate,endDate);
                            }
                        });
                        /*rangeStartDate.on("change", function (e) {
                            var setIndex = vm.filterColumnRange.index;
                            var startDate = $(rangeStartDate[setIndex]).val();
                            var endDate = $(rangeEndDate[setIndex]).val();
                            var tempStart = startDate; //31-12-2017
                            var tempEnd = endDate;
                            startDate = tempStart.split("-").reverse().join("-");
                            endDate = tempEnd.split("-").reverse().join("-");
                            setRangeFilter(startDate,endDate);
                        });
                        rangeEndDate.on("change", function (e) {
                            var setIndex = vm.filterColumnRange.index;
                            var startDate = $(rangeStartDate[setIndex]).val();
                            var endDate = $(rangeEndDate[setIndex]).val();
                            var tempStart = startDate; //31-12-2017
                            var tempEnd = endDate;
                            startDate = tempStart.split("-").reverse().join("-");
                            endDate = tempEnd.split("-").reverse().join("-");
                            setRangeFilter(startDate,endDate);
                        });*/
                        function setRangeFilter(startdate,enddate) {
                            var date = {};
                            date['start'] = (moment(startdate)).format('YYYY-MM-DD');
                            date['end'] = (moment(enddate)).format('YYYY-MM-DD');
                            var columnName = $scope.filterColumnRange.columnName;
                            var index = $scope.filterColumnRange.index;
                            vm.rangeObject[columnName].period[index] = date;
                            if (!$.isEmptyObject(vm.rangeObject)) {
                                vm.Attributes['timeline'] = vm.rangeObject;
                                var timelineObj = vm.rangeObject;
                                sketch.getTimeLineFilteredData(vm.tableData, vm.rangeObject, function (data) {
                                    vm.filteredData = data;
                                    vm.DashboardModel.Dashboard.Report.forEach(function (d) {
                                        vm.Attributes = d.axisConfig;
                                        d.axisConfig['timeline'] == vm.rangeObject;
                                        sketch.axisConfig(d.axisConfig)
                                            .data(vm.tableData)
                                            .container(d.reportContainer.id)
                                            .chartConfig(d.chart)
                                            .render();
                                    });
                                });
                            }
                        }
                    }, 1000);
                },
                simpleDateFilter:function(columnObj){
                    setTimeout(function(){
                        //var colName = vm.rangeObject;
                        //var totalCol = (colName[Object.keys(colName)[0]].period).length;
                        var today = new Date();
                        var date = today.getDate()+'-'+(today.getMonth()+1)+'-'+today.getFullYear();
                        var time = today.getHours() + ":" + today.getMinutes();
                        var CurrentDateTime = date+' '+time;
                        vm.allFilters[columnObj.key]['columnObj']=columnObj;
                        vm.allFilters[columnObj.key]['filterValue']={};
                        vm.allFilters[columnObj.key]['filterValue']['start']=CurrentDateTime;
                        vm.allFilters[columnObj.key]['filterValue']['end']=CurrentDateTime;
                        var totalCol = 2;
                        for(var i = 0; i < totalCol-1; i++){
                            var d = new Date();
                            var n = d.getFullYear();
                            var year = n - i;
                            $(".startdate").val("01-01-" + year);
                            $(".enddate").val("31-12-" + year);
                        }
                        var setDate = new Date();
                        var rangeStartDate = $('.startdate').datepicker({
                            dateFormat: 'dd-mm-yy',
                            onSelect: function(e) {
                                var d = new Date(e);
                                var tempIndex = (rangeEndDate.length) - 1;
                                var endDate = $(rangeStartDate[tempIndex]).val();
                                var startDate = e.substr(6) + e.substr(2,4) + e.substr(0,2);
                                var temp = endDate; //31-12-2017
                                endDate = temp.split("-").reverse().join("-");
                                vm.allFilters[$scope.filterColumnName.key]['filterValue']['start']=startDate;
                                //setDateFilter(startDate,endDate);
                            }
                        });
                        var rangeEndDate = $('.enddate').datepicker({
                            dateFormat: 'dd-mm-yy',
                            onSelect: function(e) {
                                var d = new Date(e);
                                var tempIndex = (rangeEndDate.length) - 1;
                                var startDate = $(rangeStartDate[tempIndex]).val();
                                var endDate = e.substr(6) + e.substr(2,4) + e.substr(0,2);
                                var temp = startDate; //31-12-2017
                                startDate = temp.split("-").reverse().join("-");
                                vm.allFilters[$scope.filterColumnName.key]['filterValue']['end']=endDate;
                                //setDateFilter(startDate,endDate);
                            }
                        });



                        /*rangeStartDate.on("change", function (e) {
                            var tempIndex = (rangeStartDate.length) - 1;
                            var startDate = $(rangeStartDate[tempIndex]).val();
                            var endDate = $(rangeEndDate[tempIndex]).val();
                            var tempStart = startDate; //31-12-2017
                            var tempEnd = endDate;
                            startDate = tempStart.split("-").reverse().join("-");
                            endDate = tempEnd.split("-").reverse().join("-");
                            setDateFilter(startDate,endDate);
                        });
                        rangeEndDate.on("change", function (e) {
                            var tempIndex = (rangeEndDate.length) - 1;
                            var startDate = $(rangeStartDate[tempIndex]).val();
                            var endDate = $(rangeEndDate[tempIndex]).val();
                            var tempStart = startDate; //31-12-2017
                            var tempEnd = endDate;
                            startDate = tempStart.split("-").reverse().join("-");
                            endDate = tempEnd.split("-").reverse().join("-");
                            setDateFilter(startDate,endDate);
                        });*/
                        function setDateFilter(startdate, enddate) {
                            var date = {};
                            date['start'] = startdate;
                            date['end'] = enddate;
                            var columnName = $scope.filterColumnName;
                            if(vm.dataCount<$rootScope.localDataLimit) {
                                sketch.applyFilter(date, columnName);
                            }else{
                                /*sketchServer.applyFilter(date, columnName,vm.metadataId,function(data,filterObj,status){
                                    if(status){
                                        $('.filterSelect').selectpicker('destroy', true);
                                        vm.allFilters[filterObj.key]['filterValue']=[];
                                        vm.conatinerFilterTo[filterObj.key]=data;
                                        vm.allFilters[filterObj.key]['filterValue']=data;
                                        $scope.$apply();
                                        setTimeout(function(){
                                            $('.filterSelect').selectpicker();
                                            $(".loadingBar").hide();
                                        },1000);
                                    }
                                });*/
                            }

                        }
                    }, 200);
                },
                simpleDateTimeFilter:function(columnObj){
                    setTimeout(function(){
                        //vm.allFilters[columnObj.key]['start']=new Date("DD-MM-YYYY HH:mm:ss");
                        //vm.allFilters[columnObj.key]['end']=new Date("DD-MM-YYYY HH:mm:ss");
                        var today = new Date();
                        var date = today.getDate()+'-'+(today.getMonth()+1)+'-'+today.getFullYear();
                        var time = today.getHours() + ":" + today.getMinutes();
                        var CurrentDateTime = date+' '+time;
                        vm.allFilters[columnObj.key]['columnObj']=columnObj;
                        vm.allFilters[columnObj.key]['filterValue']={};
                        vm.allFilters[columnObj.key]['filterValue']['start']=CurrentDateTime;
                        vm.allFilters[columnObj.key]['filterValue']['end']=CurrentDateTime;
                        var start=$('.startdatetime').datetimepicker({
                            defaultDate: new Date(),
                            format : "DD-MM-YYYY HH:mm:ss"
                        }).on('dp.change', function (e) {
                            var d = new Date(e.date);
                            var selectedDate = new Date(d);
                            selectedDate = selectedDate.getDate() + "-" +
                                ("0" + (selectedDate.getMonth() + 1)).slice(-2) + "-" +
                                ("0" + selectedDate.getFullYear()).slice(-2) + " " + ("0" + selectedDate.getHours()).slice(-2) + ":" +
                                ("0" + selectedDate.getMinutes()).slice(-2)+ ":" +
                                ("0" + selectedDate.getSeconds()).slice(-2);
                            var tempIndex = (start.length) - 1;
                            var startDate = selectedDate;
                            var endDate = $(end[tempIndex]).val();
                            var startDateArr=startDate.split(" ");
                            var tempStartDate = startDateArr[0]; //31-12-2017
                            var tempStartTime = startDateArr[1];
                            var endDateArr=endDate.split(" ");
                            var tempEndDate = endDateArr[0]; //31-12-2017
                            var tempEndTime = endDateArr[1];
                            startDate = tempStartDate.split("-").reverse().join("-");
                            endDate = tempEndDate.split("-").reverse().join("-");
                            vm.allFilters[$scope.filterColumnName.key]['filterValue']['start']=startDate+" "+tempStartTime;
                            vm.allFilters[$scope.filterColumnName.key]['filterValue']['end']=endDate+" "+tempEndTime;
                            setTimeChangeServer(startDate+" "+tempStartTime,endDate+" "+tempEndTime);
                        });
                        var end=$('.enddatetime').datetimepicker({
                            defaultDate: new Date(),
                            format : "DD-MM-YYYY HH:mm:ss"
                        }).on('dp.change', function (e) {
                            var d = new Date(e.date);
                            var selectedDate = new Date(d);
                            selectedDate = selectedDate.getDate() + "-" +
                                ("0" + (selectedDate.getMonth() + 1)).slice(-2) + "-" +
                                ("0" + selectedDate.getFullYear()).slice(-2) + " " + ("0" + selectedDate.getHours()).slice(-2) + ":" +
                                ("0" + selectedDate.getMinutes()).slice(-2)+ ":" +
                                ("0" + selectedDate.getSeconds()).slice(-2);

                            var tempIndex = (start.length) - 1;
                            var startDate = $(start[tempIndex]).val();
                            var endDate = selectedDate;
                            var startDateArr=startDate.split(" ");
                            var tempStartDate = startDateArr[0]; //31-12-2017
                            var tempStartTime = startDateArr[1];
                            var endDateArr=endDate.split(" ");
                            var tempEndDate = endDateArr[0]; //31-12-2017
                            var tempEndTime = endDateArr[1];
                            startDate = tempStartDate.split("-").reverse().join("-");
                            endDate = tempEndDate.split("-").reverse().join("-");
                            vm.allFilters[$scope.filterColumnName.key]['filterValue']['start']=startDate+" "+tempStartTime;
                            vm.allFilters[$scope.filterColumnName.key]['filterValue']['end']=endDate+" "+tempEndTime;
                            setTimeChangeServer(startDate+" "+tempStartTime,endDate+" "+tempEndTime);
                        });
                        function setTimeChangeServer(startdate, enddate) {
                            var date = {};
                            date['start'] = startdate;
                            date['end'] = enddate;
                            // var columnName = $scope.filterColumnName.key;
                            var columnName = $scope.filterColumnName;
                            if(vm.dataCount<$rootScope.localDataLimit) {
                                sketch.applyFilter(date, columnName);
                            }else{
                                /*sketchServer.applyFilter(date, columnName,vm.metadataId,function(data,filterObj,status){
                                    if(status){
                                        $('.filterSelect').selectpicker('destroy', true);
                                        vm.allFilters[filterObj.key]['filterValue']=[];
                                        vm.conatinerFilterTo[filterObj.key]=data;
                                        vm.allFilters[filterObj.key]['filterValue']=data;
                                        $scope.$apply();
                                        setTimeout(function(){
                                            $('.filterSelect').selectpicker();
                                            $(".loadingBar").hide();
                                        },1000);
                                    }
                                });*/
                            }
                        }
                    }, 200);
                }
            }
            vm.dashboardEditDraw.init();

            vm.tabActive=function(id){
                var chartId = 'chart-' + id;
                eChart.resetChart(chartId);
                $(".listStyle").removeClass("active");
                $("#li_button_"+id).addClass("active");
            }
            vm.filterModal=function(){
                $scope.filterApply=true;
            }
            vm.filterModalClose=function () {
                $scope.filterApply=false;
                $(window).trigger('resize');
            }
            vm.cascadeFilter=function(date,columnName){
                date=date['filterValue'];
                $(".loadingBar").show();
                sketchServer.applyFilter(date, columnName, vm.metadataId,function(data,filterObj,status){
                    if(status){
                        $("[id^=filterSelect_]").each(function () {
                            $(this).selectpicker('destroy', true);
                        })
                        vm.allFilters[filterObj.key]['filterValue']=[];
                        vm.conatinerFilterTo[filterObj.key]=data;
                        vm.allFilters[filterObj.key]['filterValue']=data;
                        $scope.$apply();
                        setTimeout(function(){
                            $("[id^=filterSelect_]").each(function () {
                                $(this).selectpicker();
                            });
                            $(".loadingBar").hide();
                        },1000);
                    }else {
                        setTimeout(function(){
                            $(".loadingBar").hide();
                        },1000);
                    }
                });
            }
            vm.allFilterApply=function(){
                $(".loadingBar").show();
                sketchServer.applyAllFilter(vm.allFilters,vm.metadataId);
                vm.filterModalClose();
            }

        }]);
