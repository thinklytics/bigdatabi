'use strict';

/* Controllers */
angular.module('app',[ 'ngSanitize', 'gridster' ])
    .controller('publicViewAddController',['$scope','$sce','dataFactory','$filter','$stateParams','$location','$window','$rootScope',
        '$cookieStore',
        function($scope, $sce, dataFactory, $filter,$stateParams, $location, $window, $rootScope,$cookieStore) {
            var vm = $scope;
            vm.tab = false;
            $("#chartjs-tooltip").show();
            // --- Top Menu Bar
            var data={};
            var addClass="";
            var viewClass="";
            if($location.path()=='/dashboard/'){
                viewClass='activeli';
            }else{
                addClass='activeli';
            }
            data['navTitle']="SHARED VIEW";
            data['icon']="fa fa-line-chart";
            data['navigation']=[
                {
                    "name":"Add",
                    "url":"#/dashboard/add",
                    "class":addClass
                },
                {
                    "name":"View",
                    "url":"#/dashboard/",
                    "class":viewClass
                },
                {
                    "name":"Close",
                    "url":"#/dashboard/",
                    "class":viewClass
                }
            ];
            $rootScope.$broadcast('subNavBar', data);
            sketch=sketch.reset();
            sketchServer=sketchServer.reset();
            vm.dashName={};
            //Api call's for dashboard list
            $(".loadingBar").show();
            dataFactory.request($rootScope.DashboardSharedViewList_Url,'post',"").then(function(response) {
                if(response.data.errorCode==1){
                    var result=response.data.result;
                    vm.dashboardList=[];
                    Object.keys(result).forEach(function(key){
                        result[key].forEach(function(d){
                            vm.dashboardList.push(d);
                        })
                    });
                }else{
                    dataFactory.errorAlert(response.data.message);
                    $(".loadingBar").hide();
                }
            }).then(function(){
                setTimeout(function(){
                    $(".commonSelect").selectpicker({
                        enableFiltering: true
                    });
                    $(".loadingBar").hide();
                },200);
            });
            //Api call's for Reports
            //Common setting
            /*
               Initital Client
             */
            //Default value common setting
            vm.CommonSettingObject = {};
            vm.CommonSettingObject.viewBodyTextcolor="#000000";
            vm.CommonSettingObject.viewBodyBgcolor="#EEEEEE";
            vm.CommonSettingObject.viewBodyTextcolor="#000000";
            vm.CommonSettingObject.viewBodyBgcolor="#EEEEEE";
            vm.CommonSettingObject.report_Header='yes';
            vm.CommonSettingObject.fontSize='14px';
            vm.CommonSettingObject.headerTextcolor="#000000";
            vm.CommonSettingObject.headerBgcolor="#FFFFFF";
            vm.CommonSettingObject.reportBgcolor="#FFFFFF";
            vm.CommonSettingObject.border='yes';
            vm.CommonSettingObject.border_color='#000000';
            vm.CommonSettingObject.border_width='1px';
            vm.CommonSettingObject.main_Header='yes';
            vm.CommonSettingObject.companyColor='#000000';
            vm.CommonSettingObject.companyBackground='#2196F3';
            vm.CommonSettingObject.tabColor='#000000';
            vm.CommonSettingObject.tabBackground='#FFFFFF';
            vm.CommonSettingObject.filterColor='#000000';
            vm.CommonSettingObject.filterBackground='#FFFFFF';
            vm.CommonSettingObject.marignLeft=20;
            vm.CommonSettingObject.marignRight=20;

            vm.logo="";
            vm.company={};
            vm.company.name="Thinklytics";
            vm.company.description="Business intelligence and analytics";
            vm.company.date=new Date();
            //Filter Container Add
            vm.conatinerFilter=[];
            vm.conatinerFilterTo=[];
            vm.reports=[];
            vm.dashboardSelected=true;
            vm.checkSettingSave=false;

//Confirmation Delete Start
            $scope.showSwal = function(type, callType, id, index){
                if(type == 'warning-message-and-cancel') {
                    swal({
                        title: 'Are you sure?',
                        text: 'You want to delete !',
                        type: 'warning',
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        cancelButtonText: 'No',
                        confirmButtonClass: "btn btn-success",
                        cancelButtonClass: "btn btn-danger",
                        buttonsStyling: false,
                    }).then(function(value){
                        if(value){
                            if(callType == 'report'){
                                vm.deleteGraphKeys(id);
                            }
                            else if(callType == 'filter'){
                                vm.deleteFilter(id, index);
                            }
                            else if(callType == 'tab'){
                                vm.deleteSharedViewTab(id, index);
                            }
                        }
                    },
                    function (dismiss) {

                    })
                }
            }
//Confirmation Delete End


            //Side menu
            vm.filterBy = function (item, filter,index) {
                var totalLength=Object.keys(vm.allFilters).length;
                if(index==undefined){
                    Object.keys(vm.allFilters).forEach(function (d,filterIndex) {
                        if(d==filter.key){
                            index=filterIndex;
                        }
                    })
                }
                if(vm.dataCount<$rootScope.localDataLimit) {
                    sketch.applyFilter(item, filter);
                }else if(totalLength-1!=index){
                    $(".loadingBar").show();
                    sketchServer.applyFilter(item, filter, vm.metadataId,totalLength,index,function (data, filterObj, status) {
                        if (status) {
                            try{

                                // vm.allFilters[filterObj.key]['filterValue']=data;
                                if(filterObj.value=="varchar" || filterObj.value=="char" || filterObj.value=="text"){
                                    //vm.filtersTo[filterObj.key] = data;
                                    vm.conatinerFilterTo[filterObj.key]=data;
                                    vm.allFilters[filterObj.key].filterValue = data;
                                    $scope.$apply();
                                    try{
                                        $("[id^=filterSelect_]").each(function () {
                                            if($(this).attr('id')!="filterSelect_"+index) {
                                                $(this).selectpicker('destroy', true);
                                            }
                                        })
                                    }catch (e){

                                    }

                                    setTimeout(function () {
                                        $("[id^=filterSelect_]").each(function () {
                                            if($(this).attr('id')!="filterSelect_"+index){
                                                $(this).selectpicker();
                                            }
                                        });
                                        $(".loadingBar").hide();
                                    }, 1000);
                                }else if(filterObj.value=="decimal" || filterObj.value=="int" || filterObj.value=="bigint" || filterObj.value=="double" || filterObj.value=="float"){
                                    /*
                                       Range filter
                                    */
                                    setTimeout(function () {
                                        var min = Math.min.apply(Math,data);
                                        var max = Math.max.apply(Math,data);
                                        if(min && max){
                                            if (min == null) {
                                                min = 0;
                                            }
                                            if(min==max){
                                                max=max+1;
                                            }
                                            var config = {
                                                orientation: "horizontal",
                                                start: [min,max],
                                                range: {
                                                    min: min,
                                                    max: max,
                                                },
                                                connect: 'lower',
                                                direction: "ltr",
                                                step: 10,
                                            };
                                            var nonLinearSlider = document.getElementById(filterObj.key.replace("(","(").replace(")",")")+ "-Filter");
                                            document.getElementById(filterObj.key.replace("(","(").replace(")",")")+ "-Filter").noUiSlider.destroy();
                                            noUiSlider.create(nonLinearSlider, {
                                                animate: true,
                                                start: [min, max],
                                                connect: true,
                                                range: {
                                                    min: parseInt(min),
                                                    max: parseInt(max)
                                                },
                                                step: 1,
                                                tooltips: [wNumb({
                                                    decimals: 0
                                                }), wNumb({
                                                    decimals: 0
                                                })]
                                            });
                                            vm.allFilters[filterObj.key]['filterValue'] = [min, max];
                                            nonLinearSlider.noUiSlider.on('update', function (values, handle) {
                                                vm.allFilters[filterObj.key]['filterValue'] = values;

                                            });
                                            nonLinearSlider.noUiSlider.on('change', function (values, handle) {
                                                vm.filterBy(values,filterObj);
                                            });
                                        }
                                        $(".loadingBar").hide();
                                    },1000);
                                    /*
                                      End range filter
                                     */
                                }
                            }catch(e){

                            }
                        }else{
                            setTimeout(function () {
                                $(".loadingBar").hide();
                            }, 1000);
                        }
                    });
                }
            }



            /*
                 * Range Filter
                 */
            // newRangeFilter
            /*
                *  Dynamic tab
                */
            vm.tabsInit=function(){
                vm.tabs=[];
                var tempObj={};
                tempObj['key']=1;
                tempObj['name']="Tab";
                vm.tabs.push(tempObj);
            }
            vm.tabsInit();
            vm.addViewTab = function(){
                vm.sharedViewTabName = {};
                vm.sharedViewTabName['name'] = '';
                $('#addSharedViewTab').modal('show');
            }

            vm.saveAddSharedViewTab = function(obj){
                var tempObj = {};
                tempObj['key'] = vm.tabs[vm.tabs.length-1].key+1;
                tempObj['name'] = obj.name;
                if(obj.name.length){
                    vm.tabs.push(tempObj);
                }else{
                    dataFactory.errorAlert("Tab Name Can't Be Empty");
                }
                $('#addSharedViewTab').modal('hide');
                setTimeout(function(){
                    vm.reportDroppableServerArea();
                },1000);
            }

            vm.editDeleteViewTab = function(){
                $('#EditDeleteSharedViewTab').modal('show');
            }

            vm.EditKeyIndex = false;
            vm.editSharedViewTab = function(index, key){
                vm.tabKeyName = key;
                vm.EditKeyName = true;
                vm.EditKeyIndex = index;
            }

            vm.saveSharedViewTab = function(index,key){
                var newTabName = $('#tabEditName_' + index).val();
                vm.tabs.forEach(function(d,i){
                    if(index == i){
                        d.name = newTabName;
                    }
                });
                vm.EditKeyName = false;
            }

            vm.deleteSharedViewTab = function(index,key){
                vm.tabs.splice(index,1);
                $scope.$apply();
            }

            vm.reorderSharedViewTab = function(){
                vm.backupTab=[];
                vm.tabs.forEach(function (d) {
                    vm.backupTab.push(d.name);
                });
                vm.tempTab=vm.tabs;
                $('#ReorderSharedViewTab').modal('show');
                $(".sortable").sortable({
                    cancel: ".fixed",
                    start: function(event, ui) {
                        var old_position = $(this).sortable('toArray');
                        ui.item.startPos = ui.item.index();
                    },
                    stop : function(event, ui){
                        var startIndex = ui.item.startPos;
                        var endIndex = ui.item.index();
                        var sortedArr = $(this).sortable("toArray");
                        vm.tabs=[];
                        sortedArr.forEach(function (data,index) {
                            /*if(startIndex == index){
                                vm.tabs.forEach(function(d,i){
                                    if(index == i){
                                        d.name = data;
                                    }
                                });
                            }
                            if(endIndex == index){
                                vm.tabs.forEach(function(d,i){
                                    if(index == i){
                                        d.name = data;
                                    }
                                });
                            }*/
                            var tempObj={};
                            tempObj['key']=index+1;
                            tempObj['name']=data;
                            vm.tabs.push(tempObj);
                        });
                    }
                });
            }
            vm.saveReorderSharedViewTab = function(){
                var changeIndex=[];
                var tabObj={};
                vm.tabs.forEach(function (p,i) {
                    tabObj[p.name]=p;
                });
                vm.backupTab.forEach(function (d,index) {
                    changeIndex[d]={};
                    changeIndex[d]['oldIndex']=index+1;
                    changeIndex[d]['newIndex']=tabObj[d].key;
                });
                var backupContainerObj = angular.copy($scope.dashboardPrototype.reportWithTab);
                Object.keys(changeIndex).forEach(function (d) {
                    $scope.dashboardPrototype.reportWithTab[changeIndex[d].newIndex] = backupContainerObj[changeIndex[d].oldIndex];
                    if($scope.dashboardPrototype.reportWithTab[changeIndex[d].newIndex]){
                        $scope.dashboardPrototype.reportWithTab[changeIndex[d].newIndex].forEach(function (p) {
                            p.tab=changeIndex[d].newIndex;
                        });
                    }
                });
                /*
                 * All Chart draw again
                 */
                setTimeout(function () {
                    $(".cust-tab-pane").addClass("active");
                    $scope.$apply();
                    //$("#tabBtn_1").trigger('click');
                },10);
                setTimeout(function () {
                    vm.DashboardModel.Dashboard.Report.forEach(function(d,i) {
                        var indexCheck=0;
                        Object.keys(changeIndex).forEach(function (e) {
                            if (d.reportContainer.tab==changeIndex[e].oldIndex && indexCheck==0) {
                                d.reportContainer.tab=changeIndex[e].newIndex;
                                indexCheck++;
                            }
                        });
                        if(d.chart.key!="_numberWidget"){
                            var getHeight= ($("#li_"+d.reportContainer.id).height())-65;
                            var getWidth= $("#li_"+d.reportContainer.id).width()-vm.chartWidth(vm.DashboardModel.Dashboard.activeReport.chart.key);
                            $("#chart-"+d.reportContainer.id).height(getHeight);
                            $("#chart-"+d.reportContainer.id).width(getWidth);
                        }

                        if(vm.dataCount<$rootScope.localDataLimit){
                            if(d.colorObject && d.colorObject["chart-"+d.reportContainer.id]){
                                eChart.chartRegistry.registerPublicViewColor("chart-"+d.reportContainer.id,d.colorObject["chart-"+d.reportContainer.id]);
                            }

                            //Render chart
                            if(d.chart.key=="_text"){
                                sketch
                                    .container(d.reportContainer.id)
                                    ._text(d.chart.value);
                                //vm.dashboardEditDraw.loadingBar(variable);
                            }
                            else if(d.chart.key=="_maleFemaleChartJs"){
                                sketch.axisConfig(d.axisConfig)
                                    .data(vm.tableData)
                                    .container(d.reportContainer.id)
                                    ._maleFemaleChartDraw();
                                //vm.dashboardEditDraw.loadingBar(variable);
                            }
                            else if(d.chart.key=="_PivotCustomized"){
                                sketch.axisConfig(d.axisConfig)
                                    .data(vm.tableData)
                                    .container(d.reportContainer.id)
                                    .pivotCust_Draw();
                                //vm.dashboardEditDraw.loadingBar(variable);
                            }
                            else if(d.chart.key=="_mapChartJs"){
                                sketch.axisConfig(d.axisConfig)
                                    .data(vm.tableData)
                                    .container(d.reportContainer.id)
                                    ._mapChartDraw();
                                //vm.dashboardEditDraw.loadingBar(variable);
                            }
                            else if(d.chart.key=="_floorPlanJs"){
                                sketch.axisConfig(d.axisConfig)
                                    .data(vm.tableData)
                                    .container(d.reportContainer.id)
                                    ._floorPlanDraw();
                                //vm.dashboardEditDraw.loadingBar(variable);
                            }
                            else if(d.chart.key=="_bubbleChartJs"){
                                sketch.axisConfig(d.axisConfig)
                                    .data(vm.tableData)
                                    .container(d.reportContainer.id)
                                    ._bubbleChartJs();
                                //vm.dashboardEditDraw.loadingBar(variable);
                            }
                            else{
                                sketch
                                    .axisConfig(d.axisConfig)
                                    .data(vm.tableData)
                                    .container(d.reportContainer.id)
                                    .chartConfig(d.chart)
                                    .render(function(redraw){
                                        //vm.dashboardEditDraw.loadingBar(variable);
                                    });
                            }
                            if(i==0){
                                vm.Attributes=d.axisConfig;
                            }
                            d.reportContainer.chartType=d.chart.name;
                        }else{
                            if(d.colorObject && d.colorObject["chart-"+d.reportContainer.id]) {
                                eChartServer.chartRegistry.registerPublicViewColor("chart-" + d.reportContainer.id, d.colorObject["chart-" + d.reportContainer.id]);
                            }

                            //Render chart
                            if(d.chart.key=="_text"){
                                sketch
                                    .container(d.reportContainer.id)
                                    ._text(d.chart.value);
                                //vm.dashboardEditDraw.loadingBar();
                            }
                            else if(d.chart.key=="_maleFemaleChartJs"){
                                sketchServer
                                    .accessToken($rootScope.accessToken)
                                    .axisConfig(d.axisConfig)
                                    .data([])
                                    .container(d.reportContainer.id)
                                    .chartConfig(d.chart)
                                    ._maleFemaleChartDraw();
                                //vm.dashboardEditDraw.loadingBar(variable);
                            }
                            else if(d.chart.key=="_PivotCustomized"){
                                //alert("pivot");
                                sketchServer
                                    .accessToken($rootScope.accessToken)
                                    .axisConfig(d.axisConfig)
                                    .data([])
                                    .container(d.reportContainer.id)
                                    .chartConfig(d.chart)
                                    .pivotCust_Draw();
                                //vm.dashboardEditDraw.loadingBar(variable);
                            }
                            else if(d.chart.key=="_mapChartJs"){
                                sketchServer
                                    .accessToken($rootScope.accessToken)
                                    .axisConfig(d.axisConfig)
                                    .data([])
                                    .container(d.reportContainer.id)
                                    .chartConfig(d.chart)
                                    ._mapChartDraw();
                                //vm.dashboardEditDraw.loadingBar(variable);

                            }
                            else if(d.chart.key=="_bubbleChartJs"){
                                if(d.axisConfig.dataFormat == undefined){
                                    d.axisConfig.dataFormat = {};
                                }
                                if(d.axisConfig.CustomSettingObject != undefined){
                                    d.axisConfig.dataFormat['axisColor'] = d.axisConfig.CustomSettingObject.axisColor;
                                    d.axisConfig.dataFormat['axisLabelColor'] = d.axisConfig.CustomSettingObject.axisLabelColor;
                                }
                                sketchServer
                                    .accessToken($rootScope.accessToken)
                                    .axisConfig(d.axisConfig)
                                    .data([])
                                    .container(d.reportContainer.id)
                                    .chartConfig(d.chart)
                                    ._bubbleChartJs();
                                //vm.dashboardEditDraw.loadingBar(variable);

                            }
                            else if(d.chart.key=="_floorPlanJs"){
                                sketch.axisConfig(d.axisConfig)
                                    .data(vm.tableData)
                                    .container(d.reportContainer.id)
                                    ._floorPlanDraw();
                                //vm.dashboardEditDraw.loadingBar(variable);
                            }
                            else{
                                setTimeout(function () {
                                    if(d.axisConfig.dataFormat == undefined){
                                        d.axisConfig.dataFormat = {};
                                    }
                                    if(d.axisConfig.CustomSettingObject != undefined){
                                        d.axisConfig.dataFormat['axisColor'] = d.axisConfig.CustomSettingObject.axisColor;
                                        d.axisConfig.dataFormat['axisLabelColor'] = d.axisConfig.CustomSettingObject.axisLabelColor;
                                    }

                                    sketchServer
                                        .accessToken($rootScope.accessToken)
                                        .axisConfig(d.axisConfig)
                                        .data([])
                                        .container(d.reportContainer.id)
                                        .chartConfig(d.chart)
                                        .render(function (redraw) {
                                            //vm.dashboardEditDraw.loadingBar(variable);
                                        });
                                },500)
                            }
                            if(i==0){
                                vm.Attributes = d.axisConfig;
                            }
                            d.reportContainer.chartType=d.chart.name;
                            if(vm.DashboardModel.Dashboard.Report.length==i+1){
                                vm.DashboardModel.Dashboard.Report.forEach(function(s) {
                                    var getHeight= ($("#li_"+s.reportContainer.id).height())-65;
                                    var getWidth= $("#li_"+s.reportContainer.id).width()-vm.chartWidth(vm.DashboardModel.Dashboard.activeReport.chart.key);
                                    if(d.chart.key=="_mapChartJs"){
                                        setTimeout(function () {
                                            $("#mapbox-"+s.reportContainer.id).height(getHeight);
                                            $("#mapbox-"+s.reportContainer.id).width(getWidth);
                                        },10);
                                    }
                                });
                                setTimeout(function () {
                                    $(window).trigger('resize');
                                    //$(".cust-tab-pane").addClass("active");
                                    vm.reportDroppableServerArea();
                                    $("#tabBtn_1").trigger('click');
                                },1000);
                            }
                        }
                    });
                },100);

                $('#ReorderSharedViewTab').modal('hide');
            }
            vm.range = function(){
                var columnObj = "";
                setTimeout(function(){
                    var colName = vm.rangeObject;
                    var totalCol = (colName[Object.keys(colName)[0]].period).length;
                    for(var i = 0; i < totalCol-1; i++){
                        var d = new Date();
                        var n = d.getFullYear();
                        var year = n - i;
                        $(".rangeStartFilter" + i).val("01-01-" + year);
                        $(".rangeEndFilter" + i).val("31-12-" + year);
                    }
                    var setDate = new Date();
                    var start = $('.dateTimeLineStart').datepicker({
                        dateFormat: 'dd-mm-yy',
                        onSelect: function(e) {
                            var tempIndex = (start.length) - 1;
                            var startDate = $(start[tempIndex]).val();
                            var endDate = $(end[tempIndex]).val();
                            var tempStart = startDate; //31-12-2017
                            var tempEnd = endDate;
                            startDate = tempStart.split("-").reverse().join("-");
                            endDate = tempEnd.split("-").reverse().join("-");
                            setRangeFilter(startDate,endDate);
                        }
                    });
                    var end = $('.dateTimeLineEnd').datepicker({
                        dateFormat: 'dd-mm-yy',
                        onSelect: function(e) {
                            var tempIndex = (end.length) - 1;
                            var startDate = $(start[tempIndex]).val();
                            var endDate = $(end[tempIndex]).val();
                            var tempStart = startDate; //31-12-2017
                            var tempEnd = endDate;
                            startDate = tempStart.split("-").reverse().join("-");
                            endDate = tempEnd.split("-").reverse().join("-");
                            setRangeFilter(startDate,endDate);
                        }
                    });
                    /*start.on("change", function (e) {
                            var tempIndex = (start.length) - 1;
                            var startDate = $(start[tempIndex]).val();
                            var endDate = $(end[tempIndex]).val();
                            var tempStart = startDate; //31-12-2017
                            var tempEnd = endDate;
                            startDate = tempStart.split("-").reverse().join("-");
                            endDate = tempEnd.split("-").reverse().join("-");
                            setRangeFilter(startDate,endDate);
                        });

                        end.on("change", function (e) {
                            var tempIndex = (end.length) - 1;
                            var startDate = $(start[tempIndex]).val();
                            var endDate = $(end[tempIndex]).val();
                            var tempStart = startDate; //31-12-2017
                            var tempEnd = endDate;
                            startDate = tempStart.split("-").reverse().join("-");
                            endDate = tempEnd.split("-").reverse().join("-");
                            setRangeFilter(startDate,endDate);
                        });*/

                    function setRangeFilter(startdate,enddate) {
                        var date = {};
                        date['start'] = (moment(startdate)).format('YYYY-MM-DD');
                        date['end'] = (moment(enddate)).format('YYYY-MM-DD');
                        var columnName = $scope.filterColumnName;
                        var columnName = $scope.filterColumnRange.columnName;
                        var index = $scope.filterColumnRange.index;
                        vm.rangeObject[columnName].period[index] = date;
                        if (!$.isEmptyObject(vm.rangeObject)) {
                            vm.Attributes['timeline'] = vm.rangeObject;
                            var timelineObj = vm.rangeObject;
                            sketch.getTimeLineFilteredData(vm.tableData, vm.rangeObject, function (data) {
                                vm.filteredData = data;
                                vm.DashboardModel.Dashboard.Report.forEach(function (d) {
                                    vm.Attributes = d.axisConfig;
                                    d.axisConfig['timeline'] == vm.rangeObject;
                                    sketch.axisConfig(d.axisConfig)
                                        .data(data)
                                        .container(d.reportContainer.id)
                                        .chartConfig(d.chart)
                                        .render();
                                });
                            });
                        }
                    }
                }, 200);
                Object.keys(vm.selectedFilter).forEach(function (d) {
                    columnObj = vm.selectedFilter[d];
                    var keyValue = columnObj;
                    keyValue['reName']=keyValue.reName;
                    vm.conatinerFilter.push(keyValue);
                    vm.conatinerFilterTo[columnObj.key] = [];
                    Enumerable.From(vm.tableData)
                        .Distinct(function(x){
                            return x[columnObj.key];
                        })
                        .Select(function(x){return x[columnObj.key];})
                        .ToArray()
                        .forEach(function (e) {
                            vm.conatinerFilterTo[columnObj.key].push(e);
                        });
                });
                $scope.$apply();
                setTimeout(function () {
                    setTimeout(function(){
                        $(".filterSelect").selectpicker();
                    },1000);

                    Object.keys(vm.selectedFilter).forEach(function (d) {
                        columnObj = vm.selectedFilter[d];
                        if(columnObj.value == "int" || filterObj.value=="bigint" || columnObj.value == "float" || columnObj.value == "decimal" || columnObj.value == "double") {
                            var minObject = _.min(vm.tableData, function (o) {
                                return o[columnObj.key];
                            });
                            var maxObject = _.max(vm.tableData, function (o) {
                                return o[columnObj.key];
                            });
                            var min = minObject[columnObj.key];
                            var max = maxObject[columnObj.key];
                            var columnObject = {};
                            columnObject['key'] = columnObj.key;
                            columnObject['value'] = columnObj.value;
                            if(min == null) {
                                min = 0;
                            }
                            var nonLinearSlider = document.getElementById(columnObj.columnName.replace("(","(").replace(")",")")+ "-Filter");
                            noUiSlider.create(nonLinearSlider, {
                                animate: true,
                                start: [min, max] ,
                                connect: true,
                                range: {
                                    min: parseInt(min),
                                    max: parseInt(max)
                                },
                                step: 1,
                                tooltips: [wNumb({
                                    decimals: 0
                                }), wNumb({
                                    decimals: 0
                                })]
                            });
                            nonLinearSlider.noUiSlider.on('update', function(values, handle) {
                                sketch.applyFilter(values,columnObject);
                            });
                        }
                    });
                }, 1000);
            }
            // RangeFilter
            vm.rangeFilterContainerAdd = function(colName, numberOfColumn, dateType){
                var columnObj = "";
                var tempObject = {};
                var ColumnName = colName.columnName;
                tempObject['key'] = ColumnName;
                tempObject['period'] = [];
                for (var i = 0; i < numberOfColumn; i++) {
                    var d = new Date();
                    var n = d.getFullYear();
                    var year = n - i;
                    var dateObject = {
                        "start": year+"-01-01",
                        "end": year+"-12-31"
                    };
                    tempObject['period'].push(dateObject);
                }
                setTimeout(function(){
                    var colName = vm.rangeObject;
                    var totalCol = (colName[Object.keys(colName)[0]].period).length;
                    for(var i = 0; i < totalCol-1; i++){
                        var d = new Date();
                        var n = d.getFullYear();
                        var year = n - i;
                        $(".dateTimeLineStart").val("01-01-" + year);
                        $(".dateTimeLineEnd").val("31-12-" + year);
                    }
                    var setDate = new Date();
                    var rangeStartDate = $('.dateTimeLineStart').datepicker({
                        dateFormat: 'dd-mm-yy',
                        onSelect: function(e) {
                            var tempIndex = (rangeStartDate.length) - 1;
                            var startDate = $(rangeStartDate[tempIndex]).val();
                            var endDate = $(rangeEndDate[tempIndex]).val();
                            var tempStart = startDate; //31-12-2017
                            var tempEnd = endDate;
                            startDate = tempStart.split("-").reverse().join("-");
                            endDate = tempEnd.split("-").reverse().join("-");
                            setRangeFilter(startDate,endDate);
                        }
                    });
                    var rangeEndDate = $('.dateTimeLineEnd').datepicker({
                        dateFormat: 'dd-mm-yy',
                        onSelect: function(e) {
                            var tempIndex = (rangeEndDate.length) - 1;
                            var startDate = $(rangeStartDate[tempIndex]).val();
                            var endDate = $(rangeEndDate[tempIndex]).val();
                            var tempStart = startDate; //31-12-2017
                            var tempEnd = endDate;
                            startDate = tempStart.split("-").reverse().join("-");
                            endDate = tempEnd.split("-").reverse().join("-");
                            setRangeFilter(startDate,endDate);
                        }
                    });

                    /*rangeStartDate.on("change", function (e) {
                            var tempIndex = (rangeStartDate.length) - 1;
                            var startDate = $(rangeStartDate[tempIndex]).val();
                            var endDate = $(rangeEndDate[tempIndex]).val();
                            var tempStart = startDate; //31-12-2017
                            var tempEnd = endDate;
                            startDate = tempStart.split("-").reverse().join("-");
                            endDate = tempEnd.split("-").reverse().join("-");
                            setRangeFilter(startDate,endDate);
                        });

                        rangeEndDate.on("change", function (e) {
                            var tempIndex = (rangeEndDate.length) - 1;
                            var startDate = $(rangeStartDate[tempIndex]).val();
                            var endDate = $(rangeEndDate[tempIndex]).val();
                            var tempStart = startDate; //31-12-2017
                            var tempEnd = endDate;
                            startDate = tempStart.split("-").reverse().join("-");
                            endDate = tempEnd.split("-").reverse().join("-");
                            setRangeFilter(startDate,endDate);
                        });*/

                    function setRangeFilter(startdate,enddate) {
                        var date = {};
                        date['start'] = (moment(startdate)).format('YYYY-MM-DD');
                        date['end'] = (moment(enddate)).format('YYYY-MM-DD');
                        var columnName = $scope.filterColumnName;
                        var columnName = $scope.filterColumnRange.columnName;
                        var index = $scope.filterColumnRange.index;
                        vm.rangeObject[0].period[index] = date;

                        //                            vm.rangeObject[columnName].period[index] = date;

                        if (!$.isEmptyObject(vm.rangeObject)) {
                            vm.Attributes['timeline'] = vm.rangeObject;
                            var timelineObj = vm.rangeObject;
                            sketch.getTimeLineFilteredData(vm.tableData, vm.rangeObject, function (data) {
                                vm.filteredData = data;
                                vm.DashboardModel.Dashboard.Report.forEach(function (d) {
                                    vm.Attributes = d.axisConfig;
                                    d.axisConfig['timeline'] == vm.rangeObject;
                                    sketch.axisConfig(d.axisConfig)
                                        .data(data)
                                        .container(d.reportContainer.id)
                                        .chartConfig(d.chart)
                                        .render();
                                });
                            });
                        }
                    }
                }, 200);
                Object.keys(vm.selectedFilter).forEach(function (d) {
                    columnObj = vm.selectedFilter[d];
                    var keyValue = columnObj;
                    keyValue['reName']=keyValue.key;
                    vm.conatinerFilter.push(keyValue);
                    vm.conatinerFilterTo[columnObj.key] = [];
                    Enumerable.From(vm.tableData)
                        .Distinct(function(x){
                            return x[columnObj.key];
                        })
                        .Select(function(x){return x[columnObj.key];})
                        .ToArray()
                        .forEach(function (e) {
                            vm.conatinerFilterTo[columnObj.key].push(e);
                        });
                });
                setTimeout(function () {
                    setTimeout(function(){
                        try{
                            $("[id^=filterSelect_]").each(function () {
                                $(this).selectpicker('destroy', true);
                            })
                        }catch(e) {

                        }

                    },1000);
                    Object.keys(vm.selectedFilter).forEach(function (d) {
                        columnObj = vm.selectedFilter[d];
                        if(columnObj.value == "int" || filterObj.value=="bigint" || columnObj.value == "float" || columnObj.value == "decimal" || columnObj.value == "double") {
                            var minObject = _.min(vm.tableData, function (o) {
                                return o[columnObj.key];
                            });
                            var maxObject = _.max(vm.tableData, function (o) {
                                return o[columnObj.key];
                            });
                            var min = minObject[columnObj.key];
                            var max = maxObject[columnObj.key];
                            var columnObject = {};
                            columnObject['key'] = columnObj.key;
                            columnObject['value'] = columnObj.value;
                            if(min == null) {
                                min = 0;
                            }
                            var nonLinearSlider = document.getElementById(columnObj.columnName.replace("(","\\(").replace(")","\\)")+ "-Filter");
                            noUiSlider.create(nonLinearSlider, {
                                animate: true,
                                start: [min, max] ,
                                connect: true,
                                range: {
                                    min: parseInt(min),
                                    max: parseInt(max)
                                },
                                step: 1,
                                tooltips: [wNumb({
                                    decimals: 0
                                }), wNumb({
                                    decimals: 0
                                })]
                            });
                            nonLinearSlider.noUiSlider.on('update', function(values, handle) {
                                sketch.applyFilter(values,columnObject);
                            });
                        }
                    });
                }, 1000);
            }
            //For Server side simple filter
            vm.allFilters={};
            vm.simpleFilterServerSide=function(columnObj){
                var p=Promise.resolve();
                eChartServer.resetAll();
                vm.filtersToApply = [];
                vm.filterHSApply = true;
                var promiseArray=[];
                var columnObj = "";
                Object.keys(vm.selectedFilter).forEach(function (d) {
                    // var columnObj = JSON.parse(vm.checkboxModel[d]);
                    var columnObj = vm.checkboxModel[d];
                    columnObj = vm.selectedFilter[d];
                    var keyValue = columnObj;
                    keyValue['reName']=keyValue.reName;
                    vm.conatinerFilter.push(keyValue);
                    $scope.$apply();
                    vm.filtersTo[columnObj.columnName] = [];
                    p.then(new Promise(function(resolve) {
                        columnObj = keyValue;
                        vm.conatinerFilterTo[columnObj.key] = [];
                        vm.allFilters[columnObj.key]={};
                        vm.allFilters[columnObj.key]['columnObj']=columnObj;
                        vm.allFilters[columnObj.key]['filterValue']=[];
                        sketchServer.updateFilters(vm.conatinerFilter);
                        if(columnObj.value == "int" || columnObj.value=="bigint" || columnObj.value == "float" || columnObj.value == "decimal" || columnObj.value == "double") {
                            var tempObj={};
                            tempObj['columType']=columnObj['value'];
                            tempObj['tableName']='';
                            tempObj['columnName']=columnObj['key'];
                            tempObj['reName']=columnObj['key'];
                            tempObj['dataKey']='';
                            tempObj['type']='';
                            var data = {
                                columnObject: tempObj,
                                metadataId: vm.metadataId,
                                sessionId: $rootScope.accessToken
                            };
                            dataFactory.nodeRequest('getUniqueDataFieldsOfColumn', 'post', data).then(function (data) {
                                var keyValue = columnObj;
                                data.forEach(function(x){
                                    if(columnObj.value!='datetime' && columnObj.value!='date')
                                        vm.allFilters[columnObj.key]['filterValue'].push(x);
                                    vm.conatinerFilterTo[columnObj.key].push(x);
                                });
                                if (columnObj.value == "int" || columnObj.value=="bigint" || columnObj.value == "float" || columnObj.value == "decimal" || columnObj.value == "double") {
                                    var min = Math.min.apply(Math, data);
                                    var max = Math.max.apply(Math, data);
                                    var columnObject = {};
                                    columnObject['key'] = columnObj.key;
                                    columnObject['value'] = columnObj.value;
                                    if (min == null) {
                                        min = 0;
                                    }
                                    var nonLinearSlider = $("[id='"+columnObj.key.replace("(", "(").replace(")", ")") + "-Filter"+"']")[0];
                                    noUiSlider.create(nonLinearSlider, {
                                        animate: true,
                                        start: [min, max],
                                        connect: true,
                                        range: {
                                            min: parseInt(min),
                                            max: parseInt(max)
                                        },
                                        step: 1,
                                        tooltips: [wNumb({
                                            decimals: 0
                                        }), wNumb({
                                            decimals: 0
                                        })]
                                    });
                                    vm.allFilters[columnObject.key]['filterValue']=[min, max];
                                    nonLinearSlider.noUiSlider.on('update', function (values, handle) {
                                        vm.allFilters[columnObject.key]['filterValue']=values;
                                        // sketchServer.applyFilter(values, columnObject,vm.metadataId);
                                    });
                                    nonLinearSlider.noUiSlider.on('change', function (values, handle) {
                                        vm.filterBy(values,columnObject);
                                    });
                                }
                            })
                        }else if(columnObj.value == "date") {
                            setTimeout(function(){
                                //var colName = vm.rangeObject;
                                //var totalCol = (colName[Object.keys(colName)[0]].period).length;
                                var today = new Date();
                                var startDate = "01-01-" + year;
                                var endDate = "31-01-" + year;
                                //var time = today.getHours() + ":" + today.getMinutes() + ":" + "00";
                                vm.allFilters[columnObj.key]['columnObj']=columnObj;
                                vm.allFilters[columnObj.key]['filterValue']={};
                                vm.allFilters[columnObj.key]['filterValue']['start']=startDate;
                                vm.allFilters[columnObj.key]['filterValue']['end']=endDate;

                                var totalCol = 2;
                                for(var i = 0; i < totalCol-1; i++){
                                    var d = new Date();
                                    var n = d.getFullYear();
                                    var year = n - i;
                                    $(".startdate").val("01-01-" + year);
                                    $(".enddate").val("31-12-" + year);
                                }
                                var setDate = new Date();
                                var rangeStartDate = $('.startdate').datepicker({
                                    dateFormat: 'dd-mm-yy',
                                    onSelect: function(e) {
                                        var d = new Date(e);
                                        var tempIndex = (rangeEndDate.length) - 1;
                                        var endDate = $(rangeStartDate[tempIndex]).val();
                                        var startDate = e.substr(6) + e.substr(2,4) + e.substr(0,2);
                                        var temp = endDate; //31-12-2017
                                        endDate = temp.split("-").reverse().join("-");
                                        vm.allFilters[$scope.filterColumnName.key]['filterValue']['start']=startDate;
                                        //setDateFilter(startDate,endDate);
                                    }
                                });
                                var rangeEndDate = $('.enddate').datepicker({
                                    dateFormat: 'dd-mm-yy',
                                    onSelect: function(e) {
                                        var d = new Date(e);
                                        var tempIndex = (rangeEndDate.length) - 1;
                                        var startDate = $(rangeStartDate[tempIndex]).val();
                                        var endDate = e.substr(6) + e.substr(2,4) + e.substr(0,2);
                                        var temp = startDate; //31-12-2017
                                        startDate = temp.split("-").reverse().join("-");
                                        vm.allFilters[$scope.filterColumnName.key]['filterValue']['end']=endDate;
                                        //setDateFilter(startDate,endDate);
                                    }
                                });

                                /*function setDateFilter(startdate, enddate) {
                                        var date = {};
                                        date['start'] = startdate;
                                        date['end'] = enddate;
                                        var columnName = $scope.filterColumnName;
                                        if(vm.dataCount<$rootScope.localDataLimit) {
                                            sketch.applyFilter(date, columnName);
                                        }
                                    }*/
                            }, 200);
                        }else if(columnObj.value == "datetime") {
                            setTimeout(function(){
                                var today = new Date();
                                var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
                                var time = today.getHours() + ":" + today.getMinutes() + ":" + "00";
                                var CurrentDateTime = date+' '+time;
                                vm.allFilters[columnObj.key]['columnObj']=columnObj;
                                vm.allFilters[columnObj.key]['filterValue']={};
                                vm.allFilters[columnObj.key]['filterValue']['start']=CurrentDateTime;
                                vm.allFilters[columnObj.key]['filterValue']['end']=CurrentDateTime;

                                var start=$('.startdatetime').datetimepicker({
                                    defaultDate: new Date(),
                                    format : "DD-MM-YYYY HH:mm:ss"
                                }).on('dp.change', function (e) {
                                    var d = new Date(e.date);
                                    var selectedDate = new Date(d);
                                    selectedDate = selectedDate.getDate() + "-" +
                                        ("0" + (selectedDate.getMonth() + 1)).slice(-2) + "-" +
                                        ("0" + selectedDate.getFullYear()).slice(-4) + " " + ("0" + selectedDate.getHours()).slice(-2) + ":" +
                                        ("0" + selectedDate.getMinutes()).slice(-2)+ ":" +
                                        ("0" + selectedDate.getSeconds()).slice(-2);
                                    var tempIndex = (start.length) - 1;
                                    var startDate = selectedDate;
                                    var endDate = $(end[tempIndex]).val();
                                    var startDateArr=startDate.split(" ");
                                    var tempStartDate = startDateArr[0]; //31-12-2017
                                    var tempStartTime = startDateArr[1];
                                    var endDateArr=endDate.split(" ");
                                    var tempEndDate = endDateArr[0]; //31-12-2017
                                    var tempEndTime = endDateArr[1];
                                    startDate = tempStartDate.split("-").reverse().join("-");
                                    endDate = tempEndDate.split("-").reverse().join("-");
                                    vm.allFilters[$scope.filterColumnName.key]['filterValue']['start']=startDate+" "+tempStartTime;
                                    setTimeChangeServer(startDate+" "+tempStartTime,endDate+" "+tempEndTime);
                                });
                                var end=$('.enddatetime').datetimepicker({
                                    defaultDate: new Date(),
                                    format : "DD-MM-YYYY HH:mm:ss"
                                }).on('dp.change', function (e) {
                                    var d = new Date(e.date);
                                    var selectedDate = new Date(d);
                                    selectedDate = selectedDate.getDate() + "-" +
                                        ("0" + (selectedDate.getMonth() + 1)).slice(-2) + "-" +
                                        ("0" + selectedDate.getFullYear()).slice(-4) + " " + ("0" + selectedDate.getHours()).slice(-2) + ":" +
                                        ("0" + selectedDate.getMinutes()).slice(-2)+ ":" +
                                        ("0" + selectedDate.getSeconds()).slice(-2);
                                    var tempIndex = (start.length) - 1;
                                    var startDate = $(start[tempIndex]).val();
                                    var endDate = selectedDate;
                                    var startDateArr=startDate.split(" ");
                                    var tempStartDate = startDateArr[0]; //31-12-2017
                                    var tempStartTime = startDateArr[1];
                                    var endDateArr=endDate.split(" ");
                                    var tempEndDate = endDateArr[0]; //31-12-2017
                                    var tempEndTime = endDateArr[1];
                                    startDate = tempStartDate.split("-").reverse().join("-");
                                    endDate = tempEndDate.split("-").reverse().join("-");
                                    vm.allFilters[$scope.filterColumnName.key]['filterValue']['end']=endDate+" "+tempEndTime;
                                    setTimeChangeServer(startDate+" "+tempStartTime,endDate+" "+tempEndTime);
                                });
                                function setTimeChangeServer(startdate, enddate) {
                                    var date = {};
                                    date['start'] = startdate;
                                    date['end'] = enddate;
                                    // var columnName = $scope.filterColumnName.key;
                                    var columnName = $scope.filterColumnName;
                                    if(vm.dataCount<$rootScope.localDataLimit) {
                                        sketch.applyFilter(date, columnName);
                                    }
                                }
                            }, 200);
                        }else{
                            var tempObj={};
                            tempObj['columType']=columnObj['value'];
                            tempObj['tableName']='';
                            tempObj['columnName']=columnObj['key'];
                            tempObj['reName']=columnObj['key'];
                            tempObj['dataKey']='';
                            tempObj['type']='';
                            var data = {
                                columnObject: tempObj,
                                metadataId: vm.metadataId,
                                sessionId: $rootScope.accessToken
                            };
                            dataFactory.nodeRequest('getUniqueDataFieldsOfColumn', 'post', data).then(function (data) {
                                vm.conatinerFilterTo[columnObj.key] = [];
                                data.forEach(function (k) {
                                    vm.conatinerFilterTo[columnObj.key].push(k);
                                });
                                $scope.$apply();
                                setTimeout(function(){
                                    try{
                                        $("[id^=filterSelect_]").each(function () {
                                            $(this).selectpicker();
                                        })
                                    }catch (e){

                                    }

                                },1000)
                            });
                        }
                    }));
                });
            }
            //Simple Filter Add
            vm.simpleFilterContainerAdd = function(){
                var columnObj = "";
                setTimeout(function(){
                    if(vm.rangeObject){
                        var colName = vm.rangeObject;
                        var totalCol = 2;
                        for(var i = 0; i < totalCol; i++){
                            var d = new Date();
                            var n = d.getFullYear();
                            var year = n - i;
                            $(".startdate").val("01-01-" + year);
                            $(".enddate").val("31-12-" + year);
                        }
                        var setDate = new Date();
                        var rangeStartDate = $('.startdate').datepicker({
                            dateFormat: 'dd-mm-yy',
                            onSelect: function(e) {
                                var tempIndex = (rangeStartDate.length) - 1;
                                var startDate = $(rangeStartDate[tempIndex]).val();
                                var endDate = $(rangeEndDate[tempIndex]).val();
                                var tempStart = startDate; //31-12-2017
                                var tempEnd = endDate;
                                startDate = tempStart.split("-").reverse().join("-");
                                endDate = tempEnd.split("-").reverse().join("-");
                                setDateFilter(startDate,endDate);
                            }
                        });
                        var rangeEndDate = $('.enddate').datepicker({
                            dateFormat: 'dd-mm-yy',
                            onSelect: function(e) {
                                var tempIndex = (rangeEndDate.length) - 1;
                                var startDate = $(rangeStartDate[tempIndex]).val();
                                var endDate = $(rangeEndDate[tempIndex]).val();
                                var tempStart = startDate; //31-12-2017
                                var tempEnd = endDate;
                                startDate = tempStart.split("-").reverse().join("-");
                                endDate = tempEnd.split("-").reverse().join("-");
                                setDateFilter(startDate,endDate);
                            }
                        });
                        /*rangeStartDate.on("change", function (e) {
                                var tempIndex = (rangeStartDate.length) - 1;
                                var startDate = $(rangeStartDate[tempIndex]).val();
                                var endDate = $(rangeEndDate[tempIndex]).val();
                                var tempStart = startDate; //31-12-2017
                                var tempEnd = endDate;
                                startDate = tempStart.split("-").reverse().join("-");
                                endDate = tempEnd.split("-").reverse().join("-");
                                setDateFilter(startDate,endDate);
                            });*/

                        /*rangeEndDate.on("change", function (e) {
                                var tempIndex = (rangeEndDate.length) - 1;
                                var startDate = $(rangeStartDate[tempIndex]).val();
                                var endDate = $(rangeEndDate[tempIndex]).val();
                                var tempStart = startDate; //31-12-2017
                                var tempEnd = endDate;
                                startDate = tempStart.split("-").reverse().join("-");
                                endDate = tempEnd.split("-").reverse().join("-");
                                setDateFilter(startDate,endDate);
                            });*/
                        function setDateFilter(startdate, enddate) {
                            var date = {};
                            date['start'] = startdate;
                            date['end'] = enddate;
                            // var columnName = $scope.filterColumnName.key;
                            var columnName = $scope.filterColumnName;
                            if(vm.dataCount<$rootScope.localDataLimit) {
                                sketch.applyFilter(date, columnName);
                            }else{
                                //sketchServer.applyFilter(date, columnName,vm.metadataId);
                            }
                        }
                    }
                }, 200);
                Object.keys(vm.selectedFilter).forEach(function (d) {
                    columnObj = vm.selectedFilter[d];
                    var keyValue = columnObj;
                    keyValue['reName']=keyValue.reName;
                    vm.conatinerFilter.push(keyValue);
                    vm.conatinerFilterTo[columnObj.key] = [];
                    Enumerable.From(vm.tableData)
                        .Distinct(function(x){
                            return x[columnObj.key];
                        })
                        .Select(function(x){return x[columnObj.key];})
                        .ToArray()
                        .forEach(function (e) {
                            vm.conatinerFilterTo[columnObj.key].push(e);
                        });
                });
                $scope.$apply();
                setTimeout(function () {
                    Object.keys(vm.selectedFilter).forEach(function (d,index) {
                        columnObj = vm.selectedFilter[d];
                        if(columnObj.value == "int" || columnObj.value=="bigint" || columnObj.value == "float" || columnObj.value == "decimal" || columnObj.value == "double") {
                            var minObject = _.min(vm.tableData, function (o) {
                                return o[columnObj.key];
                            });
                            var maxObject = _.max(vm.tableData, function (o) {
                                return o[columnObj.key];
                            });
                            var min = minObject[columnObj.key];
                            var max = maxObject[columnObj.key];
                            var columnObject = {};
                            columnObject['key'] = columnObj.key;
                            columnObject['value'] = columnObj.value;
                            if(min == null) {
                                min = 0;
                            }
                            var nonLinearSlider = document.getElementById(columnObj.key.replace("(","\\(").replace(")","\\)")+ "-Filter");
                            noUiSlider.create(nonLinearSlider, {
                                animate: true,
                                start: [min, max] ,
                                connect: true,
                                range: {
                                    min: parseInt(min),
                                    max: parseInt(max)
                                },
                                step: 1,
                                tooltips: [wNumb({
                                    decimals: 0
                                }), wNumb({
                                    decimals: 0
                                })]
                            });
                            nonLinearSlider.noUiSlider.on('update', function(values, handle) {
                                sketch.applyFilter(values,columnObject);
                            });
                        }else{
                            $("#filterSelect_"+index).selectpicker();
                        }
                    });
                }, 1000);
            }

            //            Rename filter

            vm.reNameFilter=function(filter,index) {
                vm.filterName={};
                $('#renameFilter').modal({
                    backdrop: 'static',
                    keyboard: false
                });
                setTimeout(function(){
                    vm.filterName.name=filter.reName;
                    vm.reNameFilterIndex=index;
                    $scope.$apply();
                },1000);
            }

            // change filter name
            vm.changeFilterName=function(filterName,index) {
                vm.conatinerFilter[vm.reNameFilterIndex].reName=filterName;
                $('#renameFilter').modal('hide');
            }


            // delete Filter

            vm.deleteFilter=function(filter,index) {
                vm.conatinerFilter.splice(index,1);
                $scope.$apply();
            }

            //Filter Server Side
            vm.filerTemp = [];
            vm.filtersToApply = [];
            vm.filterAddServer = function (colName, numberOfColumn, dataType) {
                var data = {
                    metadataId: vm.metadataId,
                    sessionId: $rootScope.accessToken
                };
                dataFactory.nodeRequest('cascadeReinitialize', 'post', data).then(function (response) {
                    //Reset All Chart
                    eChartServer.resetAll();
                    //ENd
                    vm.filtersToApply = [];
                    vm.filterHSApply = true;
                    var columnObj = "";
                    var tempObject = {};
                    var ColumnName = colName.columnName;
                    tempObject['key'] = ColumnName;
                    tempObject['period'] = [];
                    for (var i = 0; i < numberOfColumn; i++) {
                        var d = new Date();
                        var n = d.getFullYear();
                        var year = n - i;
                        var dateObject = {
                            "start": year+"-01-01",
                            "end": year+"-12-31"
                        };
                        tempObject['period'].push(dateObject);
                    }
                    Object.keys(vm.checkboxModel).forEach(function (d) {
                        columnObj = JSON.parse(vm.checkboxModel[d]);
                        var keyValue = {};
                        keyValue['key'] = columnObj.columnName;
                        keyValue['value'] = columnObj.columType;

                        vm.filtersTo[columnObj.columnName] = [];
                        Enumerable.From(vm.tableData)
                            .Distinct(function (x) { return x[columnObj.columnName]; })
                            .Select(function (x) {
                                return x[columnObj.columnName]; })
                            .ToArray()
                            .forEach(function (e) {
                                vm.filtersTo[columnObj.columnName].push(e);
                            });
                    });


                    setTimeout(function () {

                        $(".filterSelect").selectpicker();

                        for(var i = 0; i < numberOfColumn; i++){
                            var d = new Date();
                            var n = d.getFullYear();
                            var year = n - i;
                            $(".rangeStartFilter" + i).val("01-01-" + year);
                            $(".rangeEndFilter" + i).val("31-12-" + year);
                        }

                        var setDate = new Date();
                        var rangeStartDate = $('.dateTimeLineStart').datepicker({
                            dateFormat: 'dd-mm-yy',
                            onSelect: function(e) {
                                var setIndex = vm.filterColumnRange.index;
                                var startDate = $(rangeStartDate[setIndex]).val();
                                var endDate = $(rangeEndDate[setIndex]).val();
                                var tempStart = startDate; //31-12-2017
                                var tempEnd = endDate;
                                startDate = tempStart.split("-").reverse().join("-");
                                endDate = tempEnd.split("-").reverse().join("-");
                                setRangeFilter(startDate,endDate);
                            }
                        });
                        var rangeEndDate = $('.dateTimeLineEnd').datepicker({
                            dateFormat: 'dd-mm-yy',
                            onSelect: function(e) {
                                var setIndex = vm.filterColumnRange.index;
                                var startDate = $(rangeStartDate[setIndex]).val();
                                var endDate = $(rangeEndDate[setIndex]).val();
                                var tempStart = startDate; //31-12-2017
                                var tempEnd = endDate;
                                startDate = tempStart.split("-").reverse().join("-");
                                endDate = tempEnd.split("-").reverse().join("-");
                                setRangeFilter(startDate,endDate);
                            }
                        });

                        /*rangeStartDate.on("change", function (e) {
                                var setIndex = vm.filterColumnRange.index;
                                var startDate = $(rangeStartDate[setIndex]).val();
                                var endDate = $(rangeEndDate[setIndex]).val();
                                var tempStart = startDate; //31-12-2017
                                var tempEnd = endDate;
                                startDate = tempStart.split("-").reverse().join("-");
                                endDate = tempEnd.split("-").reverse().join("-");
                                setRangeFilter(startDate,endDate);
                            });*/
                        /*rangeEndDate.on("change", function (e) {
                                var setIndex = vm.filterColumnRange.index;
                                var startDate = $(rangeStartDate[setIndex]).val();
                                var endDate = $(rangeEndDate[setIndex]).val();
                                var tempStart = startDate; //31-12-2017
                                var tempEnd = endDate;
                                startDate = tempStart.split("-").reverse().join("-");
                                endDate = tempEnd.split("-").reverse().join("-");
                                setRangeFilter(startDate,endDate);
                            });*/

                        function setRangeFilter(startdate,enddate) {
                            var date = {};
                            date['start'] = (moment(startdate)).format('YYYY-MM-DD');
                            date['end'] = (moment(enddate)).format('YYYY-MM-DD');
                            var columnName = $scope.filterColumnName;
                            var columnName = $scope.filterColumnRange.columnName;
                            var index = $scope.filterColumnRange.index;
                            vm.rangeObject[0].period[index] = date;

                            if (!$.isEmptyObject(vm.rangeObject)) {
                                vm.Attributes['timeline'] = vm.rangeObject;
                                var timelineObj = vm.rangeObject;
                                sketch.getTimeLineFilteredData(vm.tableData, vm.rangeObject, function (data) {
                                    vm.filteredData = data;
                                    vm.DashboardModel.Dashboard.Report.forEach(function (d) {
                                        vm.Attributes = d.axisConfig;
                                        d.axisConfig['timeline'] == vm.rangeObject;
                                        sketch.axisConfig(d.axisConfig)
                                            .data(vm.tableData)
                                            .container(d.reportContainer.id)
                                            .chartConfig(d.chart)
                                            .render();
                                    });
                                });
                            }
                        }



                        Object.keys(vm.checkboxModel).forEach(function (d) {
                            columnObj = JSON.parse(vm.checkboxModel[d]);
                            if(columnObj.columType == "int" || columnObj.value=="bigint" || columnObj.columType == "float" || columnObj.columType == "decimal" || columnObj.columType == "double") {
                                var minObject = _.min(vm.tableData, function (o) {
                                    return o[columnObj.columnName];
                                });
                                var maxObject = _.max(vm.tableData, function (o) {
                                    return o[columnObj.columnName];
                                });
                                var min = minObject[columnObj.columnName];
                                var max = maxObject[columnObj.columnName];
                                var columnObject = {};
                                columnObject['key'] = columnObj.columnName;
                                columnObject['value'] = columnObj.columType;
                                if (min == null) {
                                    min = 0;
                                }
                                var nonLinearSlider = document.getElementById(columnObj.columnName.replace("(","\\(").replace(")","\\)")+ "-Filter");
                                noUiSlider.create(nonLinearSlider, {
                                    animate: true,
                                    start: [min, max] ,
                                    connect: true,
                                    range: {
                                        min: parseInt(min),
                                        max: parseInt(max)
                                    },
                                    step: 1,
                                    tooltips: [wNumb({
                                        decimals: 0
                                    }), wNumb({
                                        decimals: 0
                                    })]
                                });
                                nonLinearSlider.noUiSlider.on('update', function(values, handle) {
                                    sketch.applyFilter(values,columnObject);
                                });
                            }
                        });
                    }, 1000);
                });

            }


            // Filter Add

            vm.filterAdd = function (colName, numberOfColumn, dateType) {
                //Reset All Chart
                eChart.resetAll();
                //ENd
                vm.filtersToApply = [];
                vm.filterHSApply = true;
                var columnObj = "";
                var tempObject = {};
                var ColumnName = colName.columnName;
                tempObject['key'] = ColumnName;
                tempObject['period'] = [];
                for (var i = 0; i < numberOfColumn; i++) {
                    var d = new Date();
                    var n = d.getFullYear();
                    var year = n - i;
                    var dateObject = {
                        "start": year+"-01-01",
                        "end": year+"-12-31"
                    };
                    tempObject['period'].push(dateObject);
                }
                Object.keys(vm.checkboxModel).forEach(function (d) {
                    columnObj = JSON.parse(vm.checkboxModel[d]);
                    var keyValue = {};
                    keyValue['key'] = columnObj.columnName;
                    keyValue['value'] = columnObj.columType;
                    // vm.filerTemp.push(keyValue);
                    vm.filtersTo[columnObj.columnName] = [];
                    Enumerable.From(vm.tableData)
                        .Distinct(function (x) { return x[columnObj.columnName]; })
                        .Select(function (x) {
                            return x[columnObj.columnName]; })
                        .ToArray()
                        .forEach(function (e) {
                            vm.filtersTo[columnObj.columnName].push(e);
                        });
                });
                setTimeout(function () {
                    $(".filterSelect").selectpicker();
                    for(var i = 0; i < numberOfColumn; i++){
                        var d = new Date();
                        var n = d.getFullYear();
                        var year = n - i;
                        $(".rangeStartFilter" + i).val("01-01-" + year);
                        $(".rangeEndFilter" + i).val("31-12-" + year);
                    }
                    var setDate = new Date();
                    var rangeStartDate = $('.dateTimeLineStart').datepicker({
                        dateFormat: 'dd-mm-yy',
                        onSelect: function(e) {
                            var setIndex = vm.filterColumnRange.index;
                            var startDate = $(rangeStartDate[setIndex]).val();
                            var endDate = $(rangeEndDate[setIndex]).val();
                            var tempStart = startDate; //31-12-2017
                            var tempEnd = endDate;
                            startDate = tempStart.split("-").reverse().join("-");
                            endDate = tempEnd.split("-").reverse().join("-");
                            setRangeFilter(startDate,endDate);
                        }
                    });
                    var rangeEndDate = $('.dateTimeLineEnd').datepicker({
                        dateFormat: 'dd-mm-yy',
                        onSelect: function(e) {
                            var setIndex = vm.filterColumnRange.index;
                            var startDate = $(rangeStartDate[setIndex]).val();
                            var endDate = $(rangeEndDate[setIndex]).val();
                            var tempStart = startDate; //31-12-2017
                            var tempEnd = endDate;
                            startDate = tempStart.split("-").reverse().join("-");
                            endDate = tempEnd.split("-").reverse().join("-");
                            setRangeFilter(startDate,endDate);
                        }
                    });

                    /*rangeStartDate.on("change", function (e) {
                            var setIndex = vm.filterColumnRange.index;
                            var startDate = $(rangeStartDate[setIndex]).val();
                            var endDate = $(rangeEndDate[setIndex]).val();
                            var tempStart = startDate; //31-12-2017
                            var tempEnd = endDate;
                            startDate = tempStart.split("-").reverse().join("-");
                            endDate = tempEnd.split("-").reverse().join("-");
                            setRangeFilter(startDate,endDate);
                        });*/
                    /*rangeEndDate.on("change", function (e) {
                            var setIndex = vm.filterColumnRange.index;
                            var startDate = $(rangeStartDate[setIndex]).val();
                            var endDate = $(rangeEndDate[setIndex]).val();
                            var tempStart = startDate; //31-12-2017
                            var tempEnd = endDate;
                            startDate = tempStart.split("-").reverse().join("-");
                            endDate = tempEnd.split("-").reverse().join("-");
                            setRangeFilter(startDate,endDate);
                        });*/

                    function setRangeFilter(startdate,enddate) {
                        var date = {};
                        date['start'] = (moment(startdate)).format('YYYY-MM-DD');
                        date['end'] = (moment(enddate)).format('YYYY-MM-DD');
                        var columnName = $scope.filterColumnName;
                        var columnName = $scope.filterColumnRange.columnName;
                        var index = $scope.filterColumnRange.index;
                        vm.rangeObject[0].period[index] = date;
                        // vm.rangeObject[columnName].period[index] = date;
                        if (!$.isEmptyObject(vm.rangeObject)) {
                            vm.Attributes['timeline'] = vm.rangeObject;
                            var timelineObj = vm.rangeObject;
                            sketch.getTimeLineFilteredData(vm.tableData, vm.rangeObject, function (data) {
                                vm.filteredData = data;
                                vm.DashboardModel.Dashboard.Report.forEach(function (d) {
                                    vm.Attributes = d.axisConfig;
                                    d.axisConfig['timeline'] == vm.rangeObject;
                                    sketch.axisConfig(d.axisConfig)
                                        .data(vm.tableData)
                                        .container(d.reportContainer.id)
                                        .chartConfig(d.chart)
                                        .render();
                                });
                            });
                        }
                    }



                    Object.keys(vm.checkboxModel).forEach(function (d) {
                        columnObj = JSON.parse(vm.checkboxModel[d]);
                        if(columnObj.columType == "int" || columnObj.value=="bigint" || columnObj.columType == "float" || columnObj.columType == "decimal" || columnObj.columType == "double") {
                            var minObject = _.min(vm.tableData, function (o) {
                                return o[columnObj.columnName];
                            });
                            var maxObject = _.max(vm.tableData, function (o) {
                                return o[columnObj.columnName];
                            });
                            var min = minObject[columnObj.columnName];
                            var max = maxObject[columnObj.columnName];
                            var columnObject = {};
                            columnObject['key'] = columnObj.columnName;
                            columnObject['value'] = columnObj.columType;
                            if (min == null) {
                                min = 0;
                            }
                            var nonLinearSlider = document.getElementById(columnObj.columnName.replace("(","\\(").replace(")","\\)")+ "-Filter");
                            noUiSlider.create(nonLinearSlider, {
                                animate: true,
                                start: [min, max] ,
                                connect: true,
                                range: {
                                    min: parseInt(min),
                                    max: parseInt(max)
                                },
                                step: 1,
                                tooltips: [wNumb({
                                    decimals: 0
                                }), wNumb({
                                    decimals: 0
                                })]
                            });
                            nonLinearSlider.noUiSlider.on('update', function(values, handle) {
                                sketch.applyFilter(values,columnObject);
                            });
                            // $("#"+columnObj.columnName.replace("(","\\(").replace(")","\\)")+ "-Filter").noUiSlider({
                            //     animate: true,
                            //     start: [min, max] ,
                            //     connect: true,
                            //     range: {
                            //         min: parseInt(min),
                            //         max: parseInt(max)
                            //     },
                            //     step: 1,
                            //     pips: { // Show a scale with the slider
                            //         mode: 'steps',
                            //         stepped: true,
                            //         density: 4
                            //     },
                            //     tooltips: true,
                            // }).on('slide', function(){
                            //     sketch.applyFilter($(this).val(),columnObject);
                            // });
                        }
                    });
                }, 1000);
            }


            $scope.deleteGraphKeys = function (id) {
                if(vm.dataCount<$rootScope.localDataLimit){
                    $scope.deleteReportContainer(id);
                    var listAttr=eChart.chartRegistry.listAttributes();
                    if(listAttr){
                        delete listAttr["chart-"+id];
                    }
                    $scope.$apply();
                    dataFactory.successAlert("Report Deleted Successfully");
                }else{
                    $scope.deleteReportContainer(id);
                    $scope.$apply();
                    var listAttr=eChartServer.chartRegistry.listAttributes();
                    if(listAttr){
                        delete listAttr["chart-"+id];
                    }
                    var data={reportId:id,metadataId:vm.metadataId,sessionId:$rootScope.accessToken};
                    dataFactory.nodeRequest('deleteReport','post',data).then(function(response){
                        if(response){
                            dataFactory.successAlert("Report Deleted Successfully");
                        }else{
                            dataFactory.errorAlert("Check your connection");
                        }
                    });
                }
            }
            $scope.findIndexOfReportContainer=function (id) {
                var index = -1;
                var i = 0;
                $scope.dashboardPrototype.reportWithTab[vm.activeTab]
                    .forEach(function (d) {
                        if (d.id == id)
                            index = i;
                        i++;
                    });

                return index;
            }
            $scope.findIndexOfReportFromReportContainer = function (id) {
                var index = -1, i = 0;
                vm.DashboardModel.Dashboard.Report.forEach(function (d) {
                    if (d.id == id)
                        index = i;
                    i++;
                });
                return index;
            }
            $scope.deleteReportContainer = function (id) {
                if(sketchServer._totalReportCount){
                    sketchServer._totalReportCount--;
                }
                var index1 = $scope.findIndexOfReportFromReportContainer(id);
                var index = $scope.findIndexOfReportContainer(id);
                vm.Attributes = {};
                $scope.dashboardPrototype.reportWithTab[vm.activeTab].splice(index, 1);
                $scope.dashboardPrototype.reportContainers.splice(index, 1);
                vm.DashboardModel.Dashboard.Report.splice(index1, 1);
            }
            var fetchDataAndColumnsServer = function () {
                var promise = new Promise(
                    function (resolve, reject) {
                        var data = {
                            "matadataObject":JSON.stringify(vm.metadataObject),
                            "type":'dashboard'
                        };
                        /*
                             * Only for cache data to server
                             */
                        var responseColumn="";
                        dataFactory.request($rootScope.DashboardDataCacheToRedis_Url,'post',data).then(function(response){
                            if(response.data.errorCode==1){
                                vm.blnkData = true;
                                vm.blnkFilter = true;
                                vm.blnkChart = true;
                                vm.blnkSource = true;
                                responseColumn=JSON.parse(response.data.result.tableColumn);
                                /*
                                 *  Resolve table column to show
                                 */
                            }else{
                                dataFactory.errorAlert(response.data.errorCode);
                            }
                        }).then(function(){
                            /*if(vm.metadataObject.connObject.column!=undefined)
                                    vm.tableColumns = Object.assign([],vm.metadataObject.connObject.column);
                                else
                                    vm.tableColumns = Object.assign([],vm.metadataObject.column);*/
                            var columns=Object.assign([],vm.metadataObject.connObject.column);
                            var data={
                                metadataId:vm.metadataId,
                                dashboardId:vm.dashboardId,
                                columns:columns
                            };
                            dataFactory.request($rootScope.GetColumn_Url,'post',data).then(function(response){
                                vm.tableColumns = response.data.result;
                            }).then(function() {
                                $rootScope.initializeNodeClient(vm.metadataObject).then(function () {
                                    calculationServer
                                        .oldObject(responseColumn)
                                        .newObject(vm.tableColumns)
                                        .dataGroupCalculation(vm.metadataId, $rootScope.accessToken, vm.categoryGroupObject);
                                    resolve();
                                })
                            });

                        });
                    });
                return promise;
            };
            vm.reportDroppableServerArea=function(){
                $('.draggableArea').draggable({
                    cancel : "a.ui-icon", // clicking an icon
                    revert : true, // bounce back when dropped
                    helper : "clone", // create "copy" with
                    cursor : "move",
                    revertDuration : 0
                    // immediate snap
                });
                $('.droppableArea').droppable({
                    accept : ".draggableArea",
                    activeClass : "ui-state-highlight",
                    drop : function(event, ui) {
                        sketchServer._totalReportCount++;
                        var div =$(ui.draggable)[0];
                        //Chart type check
                        if($(div).find('input').val()=="Text"){
                            var chartObject={};
                            chartObject['attributes']="";
                            chartObject['id']=0;
                            chartObject['image']=0;
                            chartObject['key']="_text";
                            chartObject['name']="Text";
                            chartObject['required']="Text";
                            chartObject['value']="";
                            vm.leftSideController.addNewContainer(chartObject);
                            if(vm.Attributes.axisConfig==undefined){
                                $('#dashboardListView').modal({
                                    backdrop: 'static',
                                    keyboard: false
                                });
                                $(".label_legends"+vm.DashboardModel.Dashboard.activeReport.reportContainer.id).hide();
                                setTimeout(function(){
                                    $('#summernote').summernote({
                                        'disableResizeEditor':false,
                                        'height':'250px'
                                    });
                                    $("#summernote").summernote('reset');
                                    //alert("hello");
                                    //$(".note-codable").text("");
                                },1);
                            }
                        }else{
                            var reportObj=JSON.parse($(div).find('input').val());
                            var axisConfig=Object.assign({},reportObj.axisConfig);
                            var chartObject=Object.assign({},reportObj.chart);
                            var colorObject=Object.assign({},reportObj.colorObject);
                            vm.leftSideController.addNewContainer(chartObject);
                            $scope.$apply();
                            vm.dashboardPrototype.reportContainers[vm.dashboardPrototype.reportContainers.length-1].name=reportObj.reportContainer.name;
                            if(chartObject.key=="_numberWidget" || chartObject.name=="_dataTable"){
                                $(".label_legends"+vm.DashboardModel.Dashboard.activeReport.reportContainer.id).hide();
                            }else{
                                $(".label_legends"+vm.DashboardModel.Dashboard.activeReport.reportContainer.id).show();
                            }
                            //Change chart color
                            if(colorObject){
                                eChartServer.chartRegistry.registerPublicViewColor("chart-"+vm.DashboardModel.Dashboard.activeReport.reportContainer.id,colorObject);
                                var colorObj={};
                                colorObj["chart-"+vm.DashboardModel.Dashboard.activeReport.reportContainer.id]=colorObject;
                            }
                            //Draw chart in container
                            $scope.$apply();
                            //Draw chart
                            if(chartObject.key!='_numberWidget'){
                                if(chartObject.containerSize!=undefined){//for old object save code
                                    //vm.DashboardModel.Dashboard.activeReport.reportContainer.sizeX=chartObject.containerSize.x;
                                    //vm.DashboardModel.Dashboard.activeReport.reportContainer.sizeY=chartObject.containerSize.y;
                                }
                                $("#chart-" + vm.DashboardModel.Dashboard.activeReport.reportContainer.id).hide();
                                var getHeight= ($("#li_"+vm.DashboardModel.Dashboard.activeReport.reportContainer.id).height())-65;
                                var getWidth= $("#li_"+vm.DashboardModel.Dashboard.activeReport.reportContainer.id).width()-vm.chartWidth(vm.DashboardModel.Dashboard.activeReport.chart.key);
                                $("#chart-"+vm.DashboardModel.Dashboard.activeReport.reportContainer.id).height(getHeight);
                                $("#chart-"+vm.DashboardModel.Dashboard.activeReport.reportContainer.id).width(getWidth);
                                $("#chart-" + vm.DashboardModel.Dashboard.activeReport.reportContainer.id).show();
                            }
                            if(chartObject.key=="_maleFemaleChartJs"){
                                sketchServer.axisConfig(axisConfig)
                                    .accessToken($rootScope.accessToken)
                                    .data([])
                                    .container(vm.DashboardModel.Dashboard.activeReport.reportContainer.id)
                                    .chartConfig(chartObject)
                                    ._maleFemaleChartDraw();
                            }
                            else if(chartObject.key=="_PivotCustomized"){
                                sketchServer.axisConfig(axisConfig)
                                    .accessToken($rootScope.accessToken)
                                    .data([])
                                    .container(vm.DashboardModel.Dashboard.activeReport.reportContainer.id)
                                    .chartConfig(chartObject)
                                    .pivotCust_Draw();
                            }
                            else if(chartObject.key=="_mapChartJs"){
                                sketchServer.axisConfig(axisConfig)
                                    .accessToken($rootScope.accessToken)
                                    .data([])
                                    .container(vm.DashboardModel.Dashboard.activeReport.reportContainer.id)
                                    .chartConfig(chartObject)
                                    ._mapChartDraw();
                                setTimeout(function(){
                                    var getHeight= ($("#li_"+vm.DashboardModel.Dashboard.activeReport.reportContainer.id).height())-77;
                                    var getWidth= $("#li_"+vm.DashboardModel.Dashboard.activeReport.reportContainer.id).width()-vm.chartWidth(vm.DashboardModel.Dashboard.activeReport.chart.key);
                                    $("#mapbox-" + vm.DashboardModel.Dashboard.activeReport.reportContainer.id).height(getHeight);
                                    $("#mapbox-" + vm.DashboardModel.Dashboard.activeReport.reportContainer.id).width(getWidth);
                                },500);
                            }
                            else if(chartObject.key=="_bubbleChartJs"){
                                sketchServer.axisConfig(axisConfig)
                                    .accessToken($rootScope.accessToken)
                                    .data([])
                                    .container(vm.DashboardModel.Dashboard.activeReport.reportContainer.id)
                                    .chartConfig(chartObject)
                                    ._bubbleChartJs();
                            }
                            else if(chartObject.key=="_floorPlanJs"){
                                sketch.axisConfig(axisConfig)
                                    .data(vm.tableData)
                                    .container(vm.DashboardModel.Dashboard.activeReport.reportContainer.id)
                                    ._floorPlanDraw();
                            }
                            else{
                                //Check container Size and draw chart
                                setTimeout(function(){
                                    sketchServer
                                        .accessToken($rootScope.accessToken)
                                        .axisConfig(axisConfig)
                                        .data([])
                                        .container(vm.DashboardModel.Dashboard.activeReport.reportContainer.id)
                                        .chartConfig(chartObject)
                                        .render(function (drawn) {
                                            $("#chart-" + vm.DashboardModel.Dashboard.activeReport.reportContainer.id).show();
                                        });

                                },1200);
                                //Check container size make size
                            }
                            var reportIndex;
                            vm.DashboardModel.Dashboard.Report.forEach(function(d,index){
                                if(d.reportContainer.id==vm.DashboardModel.Dashboard.activeReport.reportContainer.id){
                                    reportIndex=index;
                                }
                            });
                            vm.DashboardModel.Dashboard.Report[reportIndex].axisConfig=axisConfig;
                            if(colorObj){
                                vm.DashboardModel.Dashboard.Report[reportIndex]['colorObject']=colorObj;
                            }
                        }
                    }
                });
            }
            vm.selectedFilter=[];
            vm.dashboardSelect = function(dashboardObj){
                vm.checkLoadingBar = true;
                vm.reports=[];
                vm.filerTemp=[];
                vm.conatinerFilter=[];
                vm.tabsInit();
                $scope.dashboardPrototype.reportWithTab=[];
                vm.dashboardSelected=false;
                $(".loadingBar").show();
                var dashboardObj=JSON.parse(dashboardObj);
                var data={
                    "id":dashboardObj.dashboard_id
                };
                dataFactory.request($rootScope.PublicViewIdObj_Url,'post',data).then(function(response) {
                    if(response.data.errorCode){
                        dashboardObj=response.data.result;
                        vm.realtimeExcel=true;
                        if(JSON.parse(dashboardObj.reportObject).connectionObject && JSON.parse(dashboardObj.reportObject).connectionObject.datasourceType=="excel"){
                            vm.realtimeExcel=false;
                        }
                    }
                }).then(function (value) {
                    vm.categoryGroupObject=JSON.parse(dashboardObj.group);
                    vm.metadataObject=JSON.parse(dashboardObj.reportObject).metadataObject;
                    vm.metadataId=vm.metadataObject.metadataId;
                    var data = {
                        "matadataObject":JSON.stringify(vm.metadataObject),
                        "type":'dashboard'
                    };
                    /*
                            Get length for server side check
                         */
                    dataFactory.request($rootScope.MetadataGetLength_Url,'post',data).then(function(response) {
                        if (response.data.errorCode == 1) {
                            vm.dataCount = response.data.result;
                            vm.dashboardId=dashboardObj.dashboard_id;
                            if (vm.dataCount < $rootScope.localDataLimit) {
                                var data={
                                    "id":dashboardObj.dashboard_id
                                };
                                dataFactory.request($rootScope.DashboardIdToObject_Url,'post',data).then(function(response){
                                    if(response.data.errorCode==1){
                                        vm.reports=(JSON.parse(response.data.result.reportObject)).Report;
                                        vm.group=JSON.parse(response.data.result.group);
                                        if((JSON.parse(response.data.result.reportObject)).colorObject){
                                            vm.reports.forEach(function(d){
                                                if((JSON.parse(response.data.result.reportObject)).colorObject["chart-"+d.reportContainer.id]){
                                                    d["colorObject"]=(JSON.parse(response.data.result.reportObject)).colorObject["chart-"+d.reportContainer.id];
                                                }
                                            });
                                        }
                                        vm.checkboxModel = JSON.parse(response.data.result.filterBy);
                                        var reportObject=JSON.parse(response.data.result.reportObject);
                                        if(reportObject.activeReport.axisConfig && reportObject.activeReport.axisConfig.timeline){
                                            var timeLineObj=JSON.parse(response.data.result.reportObject).activeReport.axisConfig.timeline;
                                            var timeLineArray=[];
                                            Object.keys(timeLineObj).forEach(function(key){
                                                timeLineArray.push(timeLineObj[key]);
                                            });
                                            vm.rangeObject = timeLineArray;
                                            var colName = vm.rangeObject[0].key;
                                            var totalCol = vm.rangeObject[0].numberOfColumn;
                                            var dateType = vm.rangeObject[0].dateType;
                                        }
                                        if(timeLineArray!=undefined && timeLineArray.length>0){
                                            var filterColName = timeLineArray[0].key;
                                            var filterTotalCol = timeLineArray[0].numberOfColumn;
                                            var filterDateType = timeLineArray[0].dateType;
                                            vm.filterAdd(filterColName, filterTotalCol, filterDateType );
                                        }
                                        vm.metadataObject=(JSON.parse(response.data.result.reportObject)).metadataObject;
                                        if(vm.checkboxModel){
                                            Object.keys(vm.checkboxModel).forEach(function (d) {
                                                var columnObj = JSON.parse(vm.checkboxModel[d]);
                                                var keyValue = {};
                                                keyValue['key'] = columnObj.columnName;
                                                keyValue['value'] = columnObj.columType;
                                                keyValue['reName'] = columnObj.reName;
                                                vm.filerTemp.push(keyValue);
                                                vm.filtersTo[columnObj.columnName] = [];
                                                Enumerable.From(vm.tableData)
                                                    .Distinct(function (x) { return x[columnObj.columnName]; })
                                                    .Select(function (x) {
                                                        return x[columnObj.columnName]; })
                                                    .ToArray()
                                                    .forEach(function (e) {
                                                        vm.filtersTo[columnObj.columnName].push(e);
                                                    });
                                            });
                                        }
                                        fetchDataAndColumns().then(function(){
                                            $('.draggableArea').draggable({
                                                cancel : "a.ui-icon", // clicking an icon
                                                revert : true, // bounce back when dropped
                                                helper : "clone", // create "copy" with
                                                cursor : "move",
                                                revertDuration : 0
                                                // immediate snap
                                            });
                                            $('.droppableArea').droppable({
                                                accept : ".draggableArea",
                                                activeClass : "ui-state-highlight",
                                                drop : function(event, ui) {
                                                    var div =$(ui.draggable)[0];
                                                    //Chart type check
                                                    if($(div).find('input').val()=="Text"){
                                                        var chartObject={};
                                                        chartObject['attributes']="";
                                                        chartObject['id']=0;
                                                        chartObject['image']=0;
                                                        chartObject['key']="_text";
                                                        chartObject['name']="Text";
                                                        chartObject['required']="Text";
                                                        chartObject['value']="";
                                                        vm.leftSideController.addNewContainer(chartObject);
                                                        if(vm.Attributes.axisConfig==undefined){
                                                            $('#dashboardListView').modal({
                                                                backdrop: 'static',
                                                                keyboard: false
                                                            });
                                                            $(".label_legends"+vm.DashboardModel.Dashboard.activeReport.reportContainer.id).hide();
                                                            setTimeout(function(){
                                                                $('#summernote').summernote({
                                                                    'disableResizeEditor':false,
                                                                    'height':'250px'
                                                                });
                                                            },1);
                                                        }
                                                    }else{
                                                        var reportObj=JSON.parse($(div).find('input').val());
                                                        var axisConfig=Object.assign({},reportObj.axisConfig);
                                                        var chartObject=Object.assign({},reportObj.chart);
                                                        var colorObject=Object.assign({},reportObj.colorObject);
                                                        vm.leftSideController.addNewContainer(chartObject);
                                                        $scope.$apply();
                                                        vm.dashboardPrototype.reportContainers[vm.dashboardPrototype.reportContainers.length-1].name=reportObj.reportContainer.name;
                                                        if(chartObject.key=="_numberWidget" || chartObject.name=="_dataTable"){
                                                            $(".label_legends"+vm.DashboardModel.Dashboard.activeReport.reportContainer.id).hide();
                                                        }else{
                                                            $(".label_legends"+vm.DashboardModel.Dashboard.activeReport.reportContainer.id).show();
                                                        }
                                                        //Change chart color
                                                        if(colorObject){
                                                            eChart.chartRegistry.registerPublicViewColor("chart-"+vm.DashboardModel.Dashboard.activeReport.reportContainer.id,colorObject);
                                                            var colorObj={};
                                                            colorObj["chart-"+vm.DashboardModel.Dashboard.activeReport.reportContainer.id]=colorObject;
                                                        }
                                                        //Draw chart in container
                                                        $scope.$apply();
                                                        //Draw chart
                                                        if(chartObject.key!='_numberWidget'){
                                                            if(chartObject.containerSize!=undefined){//for old object save code
                                                                //vm.DashboardModel.Dashboard.activeReport.reportContainer.sizeX=chartObject.containerSize.x;
                                                                //vm.DashboardModel.Dashboard.activeReport.reportContainer.sizeY=chartObject.containerSize.y;
                                                            }
                                                            $("#chart-" + vm.DashboardModel.Dashboard.activeReport.reportContainer.id).hide();
                                                            var getHeight= ($("#li_"+vm.DashboardModel.Dashboard.activeReport.reportContainer.id).height())-65;
                                                            var getWidth= $("#li_"+vm.DashboardModel.Dashboard.activeReport.reportContainer.id).width()-vm.chartWidth(vm.DashboardModel.Dashboard.activeReport.chart.key);
                                                            $("#chart-"+vm.DashboardModel.Dashboard.activeReport.reportContainer.id).height(getHeight);
                                                            $("#chart-"+vm.DashboardModel.Dashboard.activeReport.reportContainer.id).width(getWidth);
                                                            $("#chart-" + vm.DashboardModel.Dashboard.activeReport.reportContainer.id).show();
                                                        }
                                                        if(chartObject.key=="_maleFemaleChartJs"){
                                                            sketch.axisConfig(axisConfig)
                                                                .data(vm.tableData)
                                                                .container(vm.DashboardModel.Dashboard.activeReport.reportContainer.id)
                                                                .chartConfig(chartObject)
                                                                ._maleFemaleChartDraw();
                                                        }
                                                        else if(chartObject.key=="_PivotCustomized"){
                                                            sketch.axisConfig(axisConfig)
                                                                .data(vm.tableData)
                                                                .container(vm.DashboardModel.Dashboard.activeReport.reportContainer.id)
                                                                .chartConfig(chartObject)
                                                                .pivotCust_Draw();
                                                        }
                                                        else if(chartObject.key=="_mapChartJs"){
                                                            sketch.axisConfig(axisConfig)
                                                                .data(vm.tableData)
                                                                .container(vm.DashboardModel.Dashboard.activeReport.reportContainer.id)
                                                                ._mapChartDraw();
                                                            setTimeout(function(){
                                                                var getHeight= ($("#li_"+vm.DashboardModel.Dashboard.activeReport.reportContainer.id).height())-77;
                                                                var getWidth= $("#li_"+vm.DashboardModel.Dashboard.activeReport.reportContainer.id).width()-vm.chartWidth(vm.DashboardModel.Dashboard.activeReport.chart.key);
                                                                $("#mapbox-" + vm.DashboardModel.Dashboard.activeReport.reportContainer.id).height(getHeight);
                                                                $("#mapbox-" + vm.DashboardModel.Dashboard.activeReport.reportContainer.id).width(getWidth);
                                                            },500);
                                                        }
                                                        else if(chartObject.key=="_bubbleChartJs"){
                                                            sketch.axisConfig(axisConfig)
                                                                .data(vm.tableData)
                                                                .container(vm.DashboardModel.Dashboard.activeReport.reportContainer.id)
                                                                ._bubbleChartJs();
                                                        }
                                                        else if(chartObject.key=="_floorPlanJs"){
                                                            sketch.axisConfig(axisConfig)
                                                                .data(vm.tableData)
                                                                .container(vm.DashboardModel.Dashboard.activeReport.reportContainer.id)
                                                                ._floorPlanDraw();
                                                        }
                                                        else{
                                                            //Check container Size and draw chart
                                                            setTimeout(function(){
                                                                sketch
                                                                    .axisConfig(axisConfig)
                                                                    .data(vm.tableData)
                                                                    .container(vm.DashboardModel.Dashboard.activeReport.reportContainer.id)
                                                                    .chartConfig(chartObject)
                                                                    .render();
                                                                $("#chart-" + vm.DashboardModel.Dashboard.activeReport.reportContainer.id).show();
                                                            },1200);
                                                            //Check container size make size
                                                        }
                                                        var reportIndex;
                                                        vm.DashboardModel.Dashboard.Report.forEach(function(d,index){
                                                            if(d.reportContainer.id==vm.DashboardModel.Dashboard.activeReport.reportContainer.id){
                                                                reportIndex=index;
                                                            }
                                                        });
                                                        vm.DashboardModel.Dashboard.Report[reportIndex].axisConfig=axisConfig;
                                                        if(colorObj){
                                                            vm.DashboardModel.Dashboard.Report[reportIndex]['colorObject']=colorObj;
                                                        }
                                                    }
                                                }
                                            });
                                            // Filter
                                            $('.droppableAddFilter').draggable({
                                                cancel : "a.ui-icon", // clicking an icon
                                                revert : true, // bounce back when dropped
                                                helper : "clone", // create "copy" with
                                                cursor : "move",
                                                revertDuration : 0
                                                // immediate snap
                                            });

                                            $('.droppableFilter').droppable({
                                                accept : ".droppableAddFilter",
                                                activeClass : "ui-state-highlight",
                                                drop : function(event, ui) {
                                                    var div = $(ui.draggable)[0];
                                                    vm.selectedFilter=[];
                                                    var filter = JSON.parse($(div).find('input').val());
                                                    
                                                    vm.selectedFilter.push(filter);
                                                    vm.simpleFilterContainerAdd();
                                                }
                                            });
                                        });
                                    }else{
                                        dataFactory.errorAlert(response.data.message);
                                        $(".loadingBar").hide();
                                    }
                                }).then(function(){
                                    $(".loadingBar").hide();
                                });
                            } else {
                                /*
                                    Server Side
                                 */
                                var data={
                                    "id":dashboardObj.dashboard_id
                                };
                                dataFactory.request($rootScope.DashboardIdToObject_Url,'post',data).then(function(response){
                                    if(response.data.errorCode==1){
                                        vm.reports=(JSON.parse(response.data.result.reportObject)).Report;
                                        // Chart color and filter
                                        vm.group=JSON.parse(response.data.result.group);
                                        if((JSON.parse(response.data.result.reportObject)).colorObject){
                                            vm.reports.forEach(function(d){
                                                if((JSON.parse(response.data.result.reportObject)).colorObject["chart-"+d.reportContainer.id]){
                                                    d["colorObject"]=(JSON.parse(response.data.result.reportObject)).colorObject["chart-"+d.reportContainer.id];
                                                }
                                            });
                                        }
                                        vm.checkboxModel = JSON.parse(response.data.result.filterBy);
                                        var reportObject=JSON.parse(response.data.result.reportObject);
                                        if(reportObject.activeReport.axisConfig && reportObject.activeReport.axisConfig.timeline){
                                            var timeLineObj=JSON.parse(response.data.result.reportObject).activeReport.axisConfig.timeline;
                                            var timeLineArray=[];
                                            Object.keys(timeLineObj).forEach(function(key){
                                                timeLineArray.push(timeLineObj[key]);
                                            });
                                            vm.rangeObject = timeLineArray;
                                            var colName = vm.rangeObject[0].key;
                                            var totalCol = vm.rangeObject[0].numberOfColumn;
                                            var dateType = vm.rangeObject[0].dateType;
                                        }
                                        if(timeLineArray!=undefined && timeLineArray.length>0){
                                            var filterColName = timeLineArray[0].key;
                                            var filterTotalCol = timeLineArray[0].numberOfColumn;
                                            var filterDateType = timeLineArray[0].dateType;
                                            vm.filterAddServer(filterColName, filterTotalCol, filterDateType );
                                        }
                                        if(vm.checkboxModel){
                                            Object.keys(vm.checkboxModel).forEach(function (d) {
                                                try{
                                                    var columnObj = JSON.parse(vm.checkboxModel[d]);
                                                }catch (e){
                                                    var columnObj = vm.checkboxModel[d];
                                                }
                                                var keyValue = {};
                                                keyValue['key'] = columnObj.columnName;
                                                keyValue['value'] = columnObj.columType;
                                                keyValue['reName'] = columnObj.reName;
                                                vm.filerTemp.push(keyValue);
                                                vm.filtersTo[columnObj.columnName] = [];
                                                Enumerable.From(vm.tableData)
                                                    .Distinct(function (x) { return x[columnObj.columnName]; })
                                                    .Select(function (x) {
                                                        return x[columnObj.columnName]; })
                                                    .ToArray()
                                                    .forEach(function (e) {
                                                        vm.filtersTo[columnObj.columnName].push(e);
                                                    });
                                            });
                                        }
                                        setTimeout(function(){
                                            fetchDataAndColumnsServer().then(function () {
                                                vm.preLoad = false;
                                                vm.graphAxis = true;
                                                vm.loadingBarDrawer = false;
                                                vm.reportDroppableServerArea();

                                                // Filter drag and drop define
                                                $('.droppableAddFilter').draggable({
                                                    cancel : "a.ui-icon", // clicking an icon
                                                    revert : true, // bounce back when dropped
                                                    helper : "clone", // create "copy" with
                                                    cursor : "move",
                                                    revertDuration : 0
                                                    // immediate snap
                                                });

                                                $('.droppableFilter').droppable({
                                                    accept : ".droppableAddFilter",
                                                    activeClass : "ui-state-highlight",
                                                    drop : function(event, ui) {
                                                        var div = $(ui.draggable)[0];
                                                        vm.selectedFilter=[];
                                                        var filter = JSON.parse($(div).find('input').val());
                                                        vm.selectedFilter.push(filter);
                                                        vm.simpleFilterServerSide(filter);
                                                    }
                                                });
                                            });
                                        },2000);
                                    }else{
                                        dataFactory.errorAlert(response.data.message);
                                        $(".loadingBar").hide();
                                    }
                                }).then(function(){
                                    setTimeout(function(){
                                        $("#tabBtn_"+vm.tabs[0].key).trigger('click');
                                    },100);
                                    setTimeout(function(){
                                        vm.checkLoadingBar = false;
                                        $scope.$apply();
                                    },2000);
                                    $(".loadingBar").hide();
                                });
                            }
                        }
                    });
                })


                $(".loadingBar").hide();
            }


            //Text to modal recall
            vm.textModel=function(id){
                $('#dashboardListView').modal({
                    backdrop: 'static',
                    keyboard: false
                });
                setTimeout(function(){
                    $('#summernote').summernote({
                        'disableResizeEditor':false,
                        'height':'250px'
                    });
                    $("#summernote").summernote('reset');
                    vm.DashboardModel.Dashboard.Report.forEach(function (d,index) {
                        if(d.reportContainer.id==id){
                            $("#summernote").summernote("code",vm.DashboardModel.Dashboard.Report[index].chart.value);
                        }
                    });
                },1);
            }
            //Image View
            /*vm.imageChange=function(file){

                 }*/

            // End Menu
            $(".full-height").addClass("full-height-background");
            // jQuery.event.props.push('dataTransfer');
            //Left side controller
            // advanced chart settings
            vm.advancedChartSettingModal = {
                dateFormats: sketch._dateFormats.formats,
                groups: sketch._dateFormats.groups,
                Categories: [{}]
            }
            vm.saveText=function(){
                var textareaValue = $('#summernote').summernote('code');
                $('#dashboardListView').modal('hide');
                var reportIndex;
                vm.DashboardModel.Dashboard.Report.forEach(function(d,index){
                    if(d.reportContainer.id==vm.DashboardModel.Dashboard.activeReport.reportContainer.id){
                        reportIndex=index;
                    }
                });
                vm.DashboardModel.Dashboard.Report[reportIndex].chart.value=textareaValue;
                setTimeout(function(){
                    $("#chart-"+vm.DashboardModel.Dashboard.activeReport.reportContainer.id).html(textareaValue);
                },500);
            }
            vm.advancedChartSettingController = {
                init: function () {
                    vm.advancedChartSettingView.init();
                    vm.advancedChartSettingView.render();
                },
                fetchDateFormats: function () {
                    return vm.advancedChartSettingModal.dateFormats;
                },
                fetchGroupFormats: function () {
                    return vm.advancedChartSettingModal.groups;
                },
                getDimensionalData: function () {

                },
                addCategoryContainer: function () {
                    vm.categoryContainers.push({});
                },
                updateTableData: function (categoryData) {
                    var keys = Object.keys(categoryData.selectedItems);
                    var obj = {};
                    var column = categoryData.column;
                    keys.forEach(function (index) {
                        categoryData.selectedItems[index]
                            .forEach(function (d) {
                                obj[d] = categoryData.categoryName[index];
                            })
                    });
                    var newColumnName = categoryData.Name;
                    var updatedData = null;
                    vm.tableData.forEach(function (d) {
                        if (obj[d[column]]) {
                            d[newColumnName] = obj[d[column]];
                        } else {
                            d[newColumnName] = d[column];
                        }
                    });
                },
                saveCategories: function (categoriesData) {
                    var testObject = {
                        "key": "Text",
                        "value": categoriesData.Name,
                        "type": ["DimensionCustom"]
                    };
                    testObject['categoriesData'] = new Object(categoriesData);
                    vm.customGroup.push(testObject);
                    vm.tableColumns.push(testObject);
                    vm.advancedChartSettingController.updateTableData(categoriesData);
                    generate('success', 'Category create successfully');
                },
                removeCategories: function (index) {
                    vm.categoryContainers.splice(index, 1);
                }
            }

            vm.advancedChartSettingView = {
                init: function () {
                    vm.categories = {};
                    vm.categoryContainers = [];
                    vm.addCategoryContainer = vm.advancedChartSettingController.addCategoryContainer;
                    vm.saveCategories = vm.advancedChartSettingController.saveCategories;
                    vm.removeCategory = vm.advancedChartSettingController.removeCategories;
                },
                render: function () {
                    vm.dateFormatList = vm.advancedChartSettingController.fetchDateFormats();
                    vm.dateGroupList = vm.advancedChartSettingController.fetchGroupFormats();
                }
            }
            vm.advancedChartSettingController.init();
            // .............................................
            $scope.edit = function (widget) {
                lastcontainerId = "#chart-" + widget.id;
                if (Object.keys(vm.lastAppliedFilters).length != 0) {
                    if (vm.lastAppliedFilters[lastcontainerId] != undefined) {
                        var config = vm.lastAppliedFilters[lastcontainerId];
                        loadSelectedConfig(config);
                    }
                }
                if (vm.chartType == "Number Widget") {
                    $scope.graphType = true;
                } else {
                    $scope.graphType = false;
                }
            }
            // .................................................................................................
            vm.leftSideModel = {
                chartTypeData: sketch.chartData,
                aggregateData: sketch.AggregatesObject
            };
            vm.leftSideController = {
                init: function () {
                    //Map Comparison
                    vm.Multi_Arr =[0];
                    vm.addRadius = function(){
                        vm.Multi_Arr.push(vm.Multi_Arr.length);
                    }
                    vm.removeRadius = function(index) {
                        vm.Multi_Arr.splice(index, 1);
                    }
                    var mapTooltipSelector=[];
                    vm.mapObject={};
                    vm.mapObject.multipleTooltip=[];
                    vm.mapObject.multipleTooltip_Obj={};
                    vm.insertTooltipTextMap=function(columnName, index){
                        if(vm.mapObject.multipleTooltip_Obj[index] == undefined){
                            vm.mapObject.multipleTooltip_Obj[index] = [];
                        }
                        vm.mapObject.multipleTooltip_Obj[index].push(columnName);
                        sketch.Obj_ToolTip = vm.mapObject.multipleTooltip_Obj[index];
                        var temp = columnName.columnName;
                        if(columnName.dataKey == 'Measure'){
                            var data = temp + ' : <sum(' + temp + ')>\n';
                        }else{
                            var data = temp + ' : <' + temp + '>\n';
                        }
                        if(vm.mapObject.multipleTooltip[index] == undefined){
                            vm.mapObject.multipleTooltip[index] = '';
                        }else{
                            vm.mapObject.multipleTooltip[index]= vm.mapObject.multipleTooltip[index];
                        }
                        vm.mapObject.multipleTooltip[index] += data;
                        mapTooltipSelector.push(columnName);
                    }

                    //Number widget setting
                    vm.numberStyle={};
                    // Comapny Setting
                    vm.companySet={};
                    vm.company.logoWidth="50px";
                    vm.companyHeaderSet = function(){
                        vm.companySet.name=vm.company.name;
                        vm.companySet.description=vm.company.description;
                        if(vm.company.logoWidth!="50px")
                            vm.companySet.logoWidth="50px";
                        $('#companyHeaderSet').modal('show');
                    }
                    vm.companyHeaderSave=function(company){
                        vm.company.name=company.name;
                        vm.company.description=company.description;
                        vm.company.logoWidth=company.logoWidth;
                        //vm.company.log=company.logo;
                        $('#companyHeaderSet').modal('hide');
                    }
                    vm.numberSetting=function(numberSetting){
                        $("#chart-"+vm.DashboardModel.Dashboard.activeReport.reportContainer.id+" > .number-display").attr('style',  'font-size:'+numberSetting.fontSize+'px;color:'+numberSetting.fontColor);
                        vm.Attributes['numberStyle']=numberSetting;
                        $('#chartSetting').modal('hide');
                    }
                    //Check Chart type
                    vm.checkChartType=function(equal,notEqual){
                        var status=false;
                        if(equal.length){
                            equal.forEach(function(d){
                                if(vm.settingChartType==d)
                                    status=true;
                            });
                        }else{
                            status=true;
                            notEqual.forEach(function(d){
                                if(vm.settingChartType==d)
                                    status=false;
                            });
                        }
                        return status;
                    }
                    //Table Static Header
                    $('table').on('scroll', function () {
                        $("table > *").width($("table").width()+$("table").scrollLeft());
                    });
                    //Table Setting
                    vm.tableSetting={};
                    vm.tableSettingFun=function(setting){
                        setting['headerGroup']=vm.selectedGroup;
                        setting['tableColumnOrder']=vm.tableColumnOrder;
                        vm.Attributes['tableSettting']=setting;
                        sketch
                            .axisConfig(vm.Attributes)
                            .data(vm.tableData)
                            .container(vm.DashboardModel.Dashboard.activeReport.reportContainer.id)
                            .chartConfig($scope.selectedChartInView[vm.DashboardModel.Dashboard.activeReport.reportContainer.id])
                            .render(function (drawn) {
                                //$scope.lastDrawnChart[vm.DashboardModel.Dashboard.activeReport.reportContainer.id] = vm.DashboardModel.Dashboard.activeReport.chart.key;
                            });
                        $('#chartSetting').modal('hide');
                    }
                    //Add Backgorund class
                    vm.addBackgroud=function(column){
                        if($( "#backgroudCheck-"+column ).hasClass( "changeBg" )){
                            $("#backgroudCheck-"+column).removeClass('changeBg');
                        }else{
                            $("#backgroudCheck-"+column).addClass('changeBg');
                        }
                    }
                    //Reset Chart
                    vm.resetChart=function(){
                        if(vm.dataCount<$rootScope.localDataLimit){
                            eChart.resetAll();
                        }else{
                            eChartServer.resetAll();
                        }
                        dataFactory.successAlert("Chart reset successfully");
                    }
                    vm.leftSideView.render();
                    //Define Chart setting
                    vm.chartSettingObject = {};

                    vm.chartErrorText = {};
                    vm.showInfo = {};
                    vm.showAggregate = {};
                    // Remove Measure checkbox
                    vm.removeMeasure = function (columnName, checkBoxObject) {
                        delete vm.Attributes.checkboxModelMeasure[columnName];
                        vm.chartDraw(checkBoxObject, 'measure');
                    }
                    // Remove Dimension
                    vm.removeDimension = function (columnName, checkBoxObject) {
                        delete vm.Attributes.checkboxModelDimension[columnName];
                        vm.chartDraw(checkBoxObject, 'dimension');
                    }
                    // watching query selector for change
                    $scope.selectDataSource = function (model) {
                        //$('[data-toggle="tooltip"]').tooltip();
                        vm.dashboardValidation = true;
                        vm.loadingBarDrawer = true;
                        setTimeout(function () {
                            vm.leftSideController.addNewContainer();
                            $scope.$apply();
                        }, 100);
                        vm.tableData = [];
                        vm.tableColumnsMeasure = [];
                        sketch._crossfilter = null;
                        sketch._data = null;
                        vm.filtersToApply = [];
                        lastcount = 0;
                        $scope.clear();
                        // resetAllAxis();
                        if (model != undefined) {
                            vm.ischartTypeShown = true;
                            vm.isQuerySelected = false;
                            vm.graphAxis = false;
                            vm.queryObj = model;
                            vm.dashboardValidation = true;
                            vm.dashboardValidation1 = true;
                            vm.showInfo1 = false;
                            vm.showInfo2 = true;
                            vm.preLoad = true;
                            vm.groupObject = "";
                            vm.metadataObject = model;
                            fetchDataAndColumns().then(
                                function () {
                                    vm.preLoad = false;
                                    vm.graphAxis = true;
                                    vm.loadingBarDrawer = false;
                                    $scope.$apply();
                                });
                        }
                        vm.moveToMdArray = {};
                        vm.moveToDimMeasure = function (moveTo, columnName, isChecked) {
                            if(!isChecked){
                                vm.tableColumns.filter(function (d, is) {
                                    if (d.columnName == columnName) {
                                        vm.tableColumns[is].dataKey = moveTo;
                                        return true;
                                    }
                                    $scope.moveToMdArray[columnName] = moveTo
                                    return false;
                                });
                            }else{
                                alert("Can't Move to "+moveTo+" If it's selected");
                            }
                            // $scope.dimensionMeasureRecall();
                        }
                        $("#filter").multiselect("destroy");
                        $("#filter").multiselect({
                            includeSelectAllOption: false,
                            enableFiltering: true,
                            maxHeight: 200
                        });
                    }
                    vm.periodVal = [];
                    var i = 1;
                    vm.addPeriod = function () {
                        vm.filterPeriod = true;
                        vm.blnkFilter = false;
                        vm.periodVal.push(i);
                        i++;
                    }
                    vm.removePeriod = function (index) {
                        vm.periodVal.splice(index, 1);
                    }
                    //Chart Setting Model
                    vm.selectedGroup = {};
                    var custIndex = 0;
                    vm.subCategory = {};
                    vm.headerGroup = function () {
                        if (vm.tableHeader.length) {
                            $(".ms-selected").hide();
                            if (vm.subCategory[custIndex] == undefined) {
                                vm.subCategory[custIndex] = 'group'+ custIndex;
                            }
                            vm.selectedGroup['group' + custIndex] = vm.tableHeader;
                            vm.tableHeader=[];
                            custIndex++;
                        } else {
                            alert("select any one");
                        }
                    }
                    var tooltipSelector=[];
                    vm.inertTooltipText=function(columnName){
                        if(columnName.dataKey=="Measure"){
                            $("#tooltip").insertAtCaret(columnName.reName+' : <sum('+columnName.reName+')>\n');
                        }else{
                            $("#tooltip").insertAtCaret(columnName.reName+' : <'+columnName.reName+'>\n');
                        }
                        tooltipSelector.push(columnName);
                    }
                    vm.chartSetting = function (chartType) {
                        vm.Attributes.tableColumnOrder=[];
                        $.each(vm.Attributes.checkboxModelMeasure,function(key,value){
                            vm.Attributes.tableColumnOrder.push(value);
                        });
                        $.each(vm.Attributes.checkboxModelDimension,function(key,value){
                            vm.Attributes.tableColumnOrder.push(value);
                        });
                        //Tooltip selector push into chartsetting

                        vm.Attributes['tooltipSelector']=tooltipSelector;
                        /*if((vm.Attributes.setting  && vm.Attributes.setting.tableColumnOrder)){
                             vm.Attributes.setting.tableColumnOrder.forEach(function(d){
                             vm.Attributes.tableColumnOrder
                             });
                             }*/
                        vm.tableSettingCheckbox=angular.copy(vm.Attributes.checkboxModelMeasure);
                        vm.chartSettingObject = {};
                        if(vm.Attributes.dataFormate){
                            vm.chartSettingObject.xAxisDataFormate=vm.Attributes.dataFormate.xAxis;
                            vm.chartSettingObject.yAxisDataFormate=vm.Attributes.dataFormate.yAxis;
                            vm.chartSettingObject.toolTipFormate=vm.Attributes.dataFormate.tooltip;
                            vm.chartSettingObject.groupColor=vm.Attributes.dataFormate.groupColor;
                            vm.chartSettingObject.fontSize=vm.Attributes.dataFormate.fontSize;
                            if(vm.Attributes.dataFormate.chartType){
                                vm.chartTypeLineBar=vm.Attributes.dataFormate.chartType;
                            }else{
                                vm.chartTypeLineBar={};
                            }
                        }else{
                            vm.settingChartType=chartType;
                            vm.chartSettingObject.toolTipFormate=""; //tooltip Formate
                            vm.chartSettingObject.fontSize = 13; //Default font size
                        }
                        vm.numberStyle={};
                        if(vm.Attributes.numberStyle){
                            vm.numberStyle.fontSize=vm.Attributes.numberStyle.fontSize;
                            vm.numberStyle.fontColor=vm.Attributes.numberStyle.fontColor;
                        }else{
                            vm.numberStyle.fontSize=13;
                            vm.numberStyle.fontColor="#757575";
                        }
                        $('#chartSetting').modal({
                            backdrop: 'static',
                            keyboard: false
                        });
                        setTimeout(function(){
                            $(".sortable").sortable({
                                cancel: ".fixed",
                                stop : function(event, ui){
                                    vm.tableColumnOrder=$(this).sortable("toArray");
                                }
                            });
                        },100);
                        $('#chartSetting').modal('show');
                        vm.tableHeader=[];
                        setTimeout(function(){
                            $('#header-group').multiSelect({
                                afterSelect: function (values) {
                                    vm.tableHeader.push(values[0]);
                                },
                                afterDeselect: function (values) {
                                    var index = $scope.tableHeader.indexOf(values[0]);
                                    if (index != -1) {
                                        $scope.tableHeader.splice(index, 1);
                                    }
                                }
                            });
                        },1);
                        $('#chartSetting').modal({
                            backdrop: 'static',
                            keyboard: false
                        });
                        $('#chartSetting').modal('show');
                    }
                    vm.saveDataFormate = function (chartSetting) {
                        var dimensionType;
                        Object.keys(vm.Attributes.checkboxModelDimension).forEach(function (d) {
                            dimensionType = vm.Attributes.checkboxModelDimension[d].columType
                        });
                        var aggregate=['sum','count','avg'];
                        var tooltipFormat=$("#tooltip").val();
                        aggregate.forEach(function(d){
                            if(vm.Attributes.tooltipSelector.length>0){
                                vm.Attributes.tooltipSelector.forEach(function(p){
                                    if(tooltipFormat.includes(d+"("+p.reName+")")){
                                        p.aggregate=d;
                                    }
                                });
                            }
                        });
                        vm.Attributes['dataFormate'] = {};
                        vm.Attributes['dataFormate']['xAxis'] = chartSetting.xAxisDataFormate;
                        vm.Attributes['dataFormate']['yAxis'] = chartSetting.yAxisDataFormate;
                        vm.Attributes['dataFormate']['tooltip'] = $("#tooltip").val();
                        vm.Attributes['dataFormate']['fontSize'] = chartSetting.fontSize;
                        vm.Attributes['dataFormate']['tooltipSelector'] = vm.Attributes.tooltipSelector;
                        vm.Attributes['dataFormate']['chartType'] = vm.chartTypeLineBar;
                        if(chartSetting.canvas){
                            vm.Attributes['dataFormate']['canvas']={};
                            vm.Attributes['dataFormate']['canvas']['height']=chartSetting.canvas.height;
                            /*vm.Attributes['dataFormate']['canvas']['width']=chartSetting.canvas.width;*/
                        }
                        if(chartSetting.groupColor!=""){
                            vm.Attributes['dataFormate']['groupColor'] = chartSetting.groupColor;
                        }else{
                            vm.Attributes['dataFormate']['groupColor'] = null;
                        }

                        vm.Attributes['dataFormate']['fontFamily'] = chartSetting.fontFamily;
                        var activeData = {};
                        if (!$.isEmptyObject(vm.filteredData)) {
                            activeData = vm.filteredData;
                        }else {
                            activeData = vm.tableData;
                        }
                        sketch
                            .axisConfig(vm.Attributes)
                            .data(activeData)
                            .container(vm.DashboardModel.Dashboard.activeReport.reportContainer.id)
                            .chartConfig($scope.selectedChartInView[vm.DashboardModel.Dashboard.activeReport.reportContainer.id])
                            .render(
                                function (drawn) {
                                    $scope.lastDrawnChart[vm.DashboardModel.Dashboard.activeReport.reportContainer.id] = vm.DashboardModel.Dashboard.activeReport.chart.key;

                                });
                        $('#chartSetting').modal('hide');
                    }
                    //Get array list
                    vm.checkRangeObject=false;
                    vm.getArrayList = function (numberColumn) {
                        var numberArray = [];
                        if(numberColumn){
                            vm.checkRangeObject=true;
                        }
                        for (var i = 1; i <= numberColumn; i++) {
                            numberArray.push(i);
                        }
                        return numberArray;
                    }

                    //Remove Range picker
                    vm.timeLineDelete=function(rangePicker){
                        vm.saveRangedatepicker(JSON.stringify(rangePicker.key),rangePicker.period.length-1);
                    }

                    //Range Column and index get
                    $scope.filterRangeColumn = function (index, columnName) {
                        $scope.filterColumnRange = {};
                        $scope.filterColumnRange['index'] = index;
                        $scope.filterColumnRange['columnName'] = columnName;
                    }


                    //Filter column Name
                    $scope.filterColumn = function (columnName, index) {
                        $scope.filterColumnName = columnName;
                        $scope.setRangeFilterIndex = index;
                    }

                    //Range picker add
                    vm.saveRangedatepicker = function (columnName, numberOfColumn, dateType) {
                        var tempObject = {};
                        vm.rangeObject = {};
                        if(numberOfColumn){
                            var ColumnName = JSON.parse(columnName);
                            tempObject['key'] = ColumnName;
                            tempObject['period'] = [];
                            for (var i = 0; i < numberOfColumn; i++) {
                                var d = new Date();
                                var n = d.getFullYear();
                                var year = n - i;
                                var dateObject = {
                                    "start": year+"-01-01",
                                    "end": year+"-12-31"
                                };
                                tempObject['period'].push(dateObject);
                            }
                            setTimeout(function(){
                                for (var i = 0; i < numberOfColumn; i++) {
                                    var d = new Date();
                                    var n = d.getFullYear();
                                    var year = n - i;
                                    var setStartDate = '01-01-'+year;
                                    var setEndDate = '31-12-'+year;
                                    $('.rangeStartFilter' + i).val(setStartDate);
                                    $('.rangeEndFilter' + i).val(setEndDate);
                                }
                                var start = $('.dateTimeLineStart').datepicker({
                                    dateFormat: 'dd-mm-yy',
                                    onSelect: function(e) {
                                        var tempIndex = (start.length) - 1;
                                        var startDate = $(start[tempIndex]).val();
                                        var endDate = $(end[tempIndex]).val();
                                        var tempStart = startDate; //31-12-2017
                                        var tempEnd = endDate;
                                        startDate = tempStart.split("-").reverse().join("-");
                                        endDate = tempEnd.split("-").reverse().join("-");
                                        rangeFilterChange(startDate,endDate);
                                    }
                                });
                                var end = $('.dateTimeLineEnd').datepicker({
                                    dateFormat: 'dd-mm-yy',
                                    onSelect: function(e) {
                                        var tempIndex = (end.length) - 1;
                                        var startDate = $(start[tempIndex]).val();
                                        var endDate = $(end[tempIndex]).val();
                                        var tempStart = startDate; //31-12-2017
                                        var tempEnd = endDate;
                                        startDate = tempStart.split("-").reverse().join("-");
                                        endDate = tempEnd.split("-").reverse().join("-");
                                        rangeFilterChange(startDate,endDate);
                                    }
                                });

                                /*start.on("change", function (e) {
                                        var tempIndex = (start.length) - 1;
                                        var startDate = $(start[tempIndex]).val();
                                        var endDate = $(end[tempIndex]).val();
                                        var tempStart = startDate; //31-12-2017
                                        var tempEnd = endDate;
                                        startDate = tempStart.split("-").reverse().join("-");
                                        endDate = tempEnd.split("-").reverse().join("-");
                                        rangeFilterChange(startDate,endDate);
                                    });*/

                                /*end.on("change", function (e) {
                                        var tempIndex = (end.length) - 1;
                                        var startDate = $(start[tempIndex]).val();
                                        var endDate = $(end[tempIndex]).val();
                                        var tempStart = startDate; //31-12-2017
                                        var tempEnd = endDate;
                                        startDate = tempStart.split("-").reverse().join("-");
                                        endDate = tempEnd.split("-").reverse().join("-");
                                        rangeFilterChange(startDate,endDate);
                                    });*/


                                function rangeFilterChange(startDate,endDate) {
                                    var date = {};
                                    date['start'] = (moment(startDate)).format('YYYY-MM-DD');
                                    date['end'] = (moment(endDate)).format('YYYY-MM-DD');
                                    var columnName = $scope.filterColumnName;
                                    //Date
                                    var columnName = $scope.filterColumnRange.columnName;
                                    var index = $scope.filterColumnRange.index;
                                    vm.rangeObject[columnName].period[index] = date;
                                    //End
                                    //vm.DashboardView.renderChartInActiveContainer();
                                    if (!$.isEmptyObject(vm.rangeObject)) {
                                        vm.Attributes['timeline'] = vm.rangeObject;
                                        var timelineObj = vm.rangeObject;
                                        sketch.getTimeLineFilteredData(vm.tableData, vm.rangeObject, function (data) {
                                            vm.filteredData = data;
                                            vm.DashboardModel.Dashboard.Report.forEach(function (d) {
                                                vm.Attributes = d.axisConfig;
                                                d.axisConfig['timeline'] == vm.rangeObject;
                                                sketch
                                                    .axisConfig(d.axisConfig)
                                                    .data(data)
                                                    .container(d.reportContainer.id)
                                                    .chartConfig(d.chart)
                                                    .render();
                                            });
                                        });
                                    }
                                }
                            },1000);
                            tempObject['numberOfColumn'] = numberOfColumn;
                            tempObject['dateType'] = dateType;
                            vm.rangeObject[ColumnName.reName] = tempObject;
                            var rawData = angular.copy(vm.tableData);
                            if (!$.isEmptyObject(vm.rangeObject)) {
                                vm.Attributes['timeline'] = vm.rangeObject;
                                sketch.getTimeLineFilteredData(rawData, vm.rangeObject, function (data) {
                                    vm.filteredData = data;
                                    vm.Attributes['timeline'] = vm.rangeObject;
                                    vm.DashboardModel.Dashboard.Report.forEach(function (d) {
                                        vm.Attributes = d.axisConfig;
                                        d.axisConfig['timeline'] == vm.rangeObject;
                                        sketch
                                            .axisConfig(d.axisConfig)
                                            .data(data)
                                            .container(d.reportContainer.id)
                                            .chartConfig(d.chart)
                                            .render();
                                    });
                                });
                            }
                            $("#dateModal").modal('hide');
                        }else{
                            sketch._timeline={};
                            vm.DashboardModel.Dashboard.Report.forEach(function (d) {
                                delete d.axisConfig.timeline;
                                sketch
                                    .axisConfig(d.axisConfig)
                                    .data(vm.tableData)
                                    .container(d.reportContainer.id)
                                    .chartConfig(d.chart)
                                    .render();
                            });
                        }
                    }
                    vm.checkboxModelMeasure = [];
                    vm.checkboxModelDimension = [];
                },
                getCharts: function () {
                    return vm.leftSideModel.chartTypeData;
                },
                getAggregates: function () {
                    return vm.leftSideModel.aggregateData;
                },
                senseDefaultChart: function () {
                    if (Object.keys(vm.Attributes.checkboxModelDimension).length == 1 && Object.keys(vm.Attributes.checkboxModelMeasure).length == 1) {
                        $scope.addChartIconBorder(sketch.chartData[0]);
                        return sketch.chartData[0];
                    } else if (Object.keys(vm.Attributes.checkboxModelDimension) == 1 && Object.key(vm.Attributes.checkboxModelMeasure).length == 2) {
                        return sketch.chartData[5];
                    } else {
                        return false;
                    }
                },
                requiredAttributesFound: function () {
                    if (vm.Attributes.checkboxModelDimension && vm.Attributes.checkboxModelMeasure)
                        if (Object.keys(vm.Attributes.checkboxModelDimension).length > 0 && Object.keys(vm.Attributes.checkboxModelMeasure).length > 0) {
                            return true;
                        }
                    return false;
                },
                showChartErrorText: function (chart) {
                    try {
                        if (chart != null) {
                            vm.blnkRep = true;
                            //	vm.showInfovm.DashboardModel.Dashboard.activeReport.reportContainer.id=true;
                            $("#chart-" + vm.DashboardModel.Dashboard.activeReport.reportContainer.id).html("");
                            vm.chartErrorText[vm.DashboardModel.Dashboard.activeReport.reportContainer.id] = "You need to select max "
                                + chart.required.Dimension
                                + " dimension"
                                + " and min "
                                + chart.required.Measure[0]
                                + " measure to draw "
                                + chart.name;
                        } else {
                            vm.blnkRep = true;
                            vm.chartErrorText[vm.DashboardModel.Dashboard.activeReport.reportContainer.id] = "You need to select atleast 1 dimension and 1 measure to draw any chart";
                        }
                    } catch (e) {
                        vm.chartErrorText[vm.DashboardModel.Dashboard.activeReport.reportContainer.id] = "Some Error Occured....";
                    }
                },
                checkChartCanBeDrawn: function (chart) {
                    if (chart != null)
                        if (chart.name == "DataTable")
                            return true;
                    if (vm.Attributes.checkboxModelDimension && vm.Attributes.checkboxModelMeasure) {
                        if ((Object.keys(vm.Attributes.checkboxModelDimension).length == chart.required.Dimension) && ((chart.required.Measure[0] <= Object.keys(vm.Attributes.checkboxModelMeasure).length) && (chart.required.Measure[1] >= Object.keys(vm.Attributes.checkboxModelMeasure).length))) {
                            vm.showInfo[vm.DashboardModel.Dashboard.activeReport.reportContainer.id] = false;
                            return true;
                        }
                    }
                    vm.showInfo1 = true;
                    vm.showInfo2 = true;
                    vm.showInfo[vm.DashboardModel.Dashboard.activeReport.reportContainer.id] = true;
                    return false;
                },
                updateAttributesObject: function (attr, type, chartType) {

                    if (chartType == "_dataTable") {
                        return true;
                    }
                    if (type == "dimension")
                        if ($scope.Attributes.checkboxModelDimension[attr.columnName] == 0) {
                            delete $scope.Attributes.checkboxModelDimension[attr.columnName];
                            return false;
                        }
                    if (type == "measure")
                        if ($scope.Attributes.checkboxModelMeasure[attr.columnName] == 0) {
                            delete $scope.Attributes.checkboxModelMeasure[attr.columnName];
                            return false;
                        }

                    if (type == "chart"
                        && !vm.leftSideController
                            .checkChartCanBeDrawn(attr)) {
                        vm.leftSideController
                            .showChartErrorText(attr);
                        return false;
                    }

                    return true;
                },
                addNewContainer: function (chartObject) {
                    vm.Attributes = {aggregateModel: {}};
                    vm.DashboardController.addReportContainer(chartObject);
                    vm.DashboardModel.Dashboard.activeReport.chart = chartObject;
                    $scope.selectedChartInView[vm.DashboardModel.Dashboard.activeReport.reportContainer.id] = null;
                    $(".custBorderHover").removeClass('activeChart');
                    // $("#" + index).addClass('activeChart');
                },
                setDefaultAggregate:function(measure){
                    //Initializing Aggregate Model In Case Not Initialized
                    if(!vm.Attributes.aggregateModel)
                        vm.Attributes.aggregateModel={};

                    vm.Attributes.aggregateModel[measure.columnName] = {
                        "key": "sumIndex",
                        "value": "Sum",
                        "type": "Aggregate"
                    };

                },
                setChartInView:function(chart){
                    $scope.selectedChartInView[vm.DashboardModel.Dashboard.activeReport.reportContainer.id]=chart;
                },
                getSelectedChartFromView:function(){
                    return $scope.selectedChartInView[vm.DashboardModel.Dashboard.activeReport.reportContainer.id];
                },
                checkAndDrawChart:function(chart){
                    var isChartBeDrawn = sketch.axisConfig(vm.Attributes).chartConfig(chart)._isAllFieldsPassed();

                    if(isChartBeDrawn){
                        vm.showInfo[vm.DashboardModel.Dashboard.activeReport.reportContainer.id] = false;
                        vm.leftSideController.showProgressBar(true);
                        vm.DashboardView.renderChartInActiveContainer();
                        vm.DashboardModel.Dashboard.activeReport.chart = chart;
                    }
                    else {
                        vm.leftSideController.showChartErrorText(vm.DashboardModel.Dashboard.activeReport.chart);
                        vm.showInfo[vm.DashboardModel.Dashboard.activeReport.reportContainer.id] = true;
                        $(".label_legends" + vm.DashboardModel.Dashboard.activeReport.reportContainer.id).hide();
                        $("#left" + vm.DashboardModel.Dashboard.activeReport.reportContainer.id).html("");
                    }

                },
                switchChartContainer:function(attr,callback){
                    var activeReportContainerId = vm.DashboardModel.Dashboard.activeReport.reportContainer.id;
                    var activeContainer = $scope.dashboardPrototype.reportContainers[$scope.findIndexOfReportContainer(activeReportContainerId)];
                    if(attr.key!="_numberWidget") {
                        if ($scope.lastDrawnChart[vm.DashboardModel.Dashboard.activeReport.reportContainer.id] == "_numberWidget") {


                            var obj = vm.DashboardController.getCommonContainerSettings();
                            $.each(obj,function (key,value) {
                                activeContainer[key] = value;

                            });


                        }
                    }
                    else{
                        var obj = vm.DashboardController.getNumberContainerSettings();
                        $.each(obj, function (key, value) {
                            activeContainer[key] = value;

                        });


                    }
                    vm.DashboardModel.Dashboard.activeReport.reportContainer = activeContainer;
                    var indexOfReport = $scope.findIndexOfReportFromReportContainer(activeReportContainerId);
                    vm.DashboardModel.Dashboard.Report[indexOfReport] = activeContainer;
                    setTimeout(function(){

                        callback();
                    },1000);
                },
                showProgressBar:function(state){
                    var variable = "reportLoading_" + vm.DashboardModel.Dashboard.activeReport.reportContainer.id;
                    vm[variable] = state;
                },
                onAttributeSelect: function (attr, type) {

                    //Updating Attribute Object According to selections
                    vm.leftSideController.updateAttributesObject(attr,type);

                    if (type === "measure") {
                        //Sets default aggregate for each measure
                        vm.leftSideController.setDefaultAggregate(attr);

                    }

                    if(type==="dimension" || type==="measure" || type==="aggregate")
                    {
                        var activeChart=vm.leftSideController.getSelectedChartFromView();
                        //check if any chart selected in view
                        if(!activeChart)
                        {
                            vm.leftSideController.setChartInView(vm.DashboardModel.Dashboard.activeReport.chart);

                        }
                        else{


                            vm.leftSideController.checkAndDrawChart(activeChart);

                        }

                    }

                    if(type==="chart")
                    {

                        vm.leftSideController.switchChartContainer(attr,function () {

                            vm.leftSideController.setChartInView(attr);
                            vm.leftSideController.checkAndDrawChart(attr);
                        });

                    }
                }
            }


            vm.leftSideView = {

                render: function () {
                    vm.Attributes = {};
                    vm.chartDraw = vm.leftSideController.onAttributeSelect;
                    // vm.chartSelect=vm.leftSideController.updateAttribute;
                    $scope.charts = vm.leftSideController.getCharts();
                    $scope.aggregates = vm.leftSideController.getAggregates();
                    $scope.addReport = vm.leftSideController.addNewContainer;
                }
            };

            vm.leftSideController.init();

            // ..................Right side
            // .....................................................

            vm.rightSideModel = {
                axisObject: sketch.axisData,
                AggregateObject: sketch.AggregatesObject,
                axisData: sketch.attributesData,
                activeAxisConfig: null
            }

            vm.rightSideView = {
                // this part can be improved
                init: function () {
                    // vm.chartDraw=vm.rightSideController.getAxisRenderObj;
                    vm.refreshChartContainer = vm.rightSideController.onAxisValueChange;
                    // Measure Validation
                    vm.measureAddValidation = function () {
                        /*
                             * var parts =
                             * $scope.measureFormula.split(/[[\]]{1,2}/);
                             * parts.length--; // the last entry is
                             * dummy, need to take it out var
                             * valExp=parts.join('');
                             */
                        var newExpression = $scope.measureFormula
                            .replace(/\b[a-z]\w*/ig,
                                "v['$&']").replace(
                                /[\(|\|\.)]/g, "");
                        var formula = "v['"
                            + vm.lastColumSelected + "']";
                        var testObject = {
                            "key": "INT",
                            "value": $scope.measureNameValidation,
                            "type": ["MeasureCustom"],
                            "validation": newExpression,
                            "formula": formula
                        };
                        vm.tableColumns.push(testObject);

                        $('.advChartSettingsModal').drawer(
                            'close');
                        generate("success",
                            "Measure created successfully");
                    }
                    $scope.addColumnType = function (columnObj) {
                        vm.hiddenAxis = columnObj.Name;

                        $("#myModal").modal();

                        if (columnObj.columnType == "Dimension") {
                            vm.columnTypeObj = vm.rightSideController
                                .getMeasures();
                        } else if (columnObj.columnType == "Measure") {
                            vm.columnTypeObj = vm.rightSideController
                                .getDimensions();
                        }
                    }
                    // End Calculation Part

                },
                render: function (chartTypeObj) {

                    // vm.chartSettings={};

                    this.data = vm.rightSideController
                        .getAxisRenderObj(chartTypeObj);
                    // vm.rightSideView.resetView(this.data);
                    // $(".select2").select2('remove');

                    vm.axisRenderArray = this.data;

                    setTimeout(function () {
                        $('select').select2();

                    }, 100);

                },
                chartSelectView: {
                    update: function (chartInfo) {
                        $scope.chartType = JSON.stringify(chartInfo);
                    }
                }
            }
            vm.DashboardModel = {
                currentActiveGrid: null,
                dashboardPrototypeObject: {
                    id: '1',
                    name: 'Home',
                    reportContainers: []
                },
                reportPrototypeObject: {
                    reportContainer: null,
                    chart: null,
                    axisConfig: null
                },
                gridSettings: {
                    margins: [5, 5],
                    columns: 30,
                    pushing: true,
                    floating: true,
                    maxRows:10000,
                    swapping: true,
                    rowHeight: '20',
                    shift_widgets_up: false,
                    shift_larger_widgets_down: false,
                    collision: {
                        wait_for_mouseup: true,
                        on_overlap_start: function (collider_data) {

                        }
                    },
                    draggable: {
                        enabled: true, // whether dragging
                        // items is supported
                        handle: '.my-class',
                        start: function (event, $element,
                                         widget) {

                        },
                        resize: function (event, $element,
                                          widget) {

                        },
                        stop: function (event, $element, widget) {
                            /*$(".hiddenBorder").removeClass("box-border");*/
                        }
                    },
                    resizable: {
                        enabled: true,
                        handles: ['n', 'e', 's', 'w', 'ne',
                            'se', 'sw', 'nw'],
                        stop: function (event,uiWidget,$element) {
                            if(vm.DashboardModel.Dashboard.activeReport.chart.key=="_maleFemaleChartJs" || vm.DashboardModel.Dashboard.activeReport.chart.key=="_mapChartJs" || vm.DashboardModel.Dashboard.activeReport.chart.key=="_speedoMeterChart"){
                                setTimeout(function () {
                                    var getHeight = ($("#li_" + vm.DashboardModel.Dashboard.activeReport.reportContainer.id).height()) - 65;
                                    var getWidth = $("#li_" + vm.DashboardModel.Dashboard.activeReport.reportContainer.id).width() - vm.chartWidth(vm.DashboardModel.Dashboard.activeReport.chart.key);
                                    //var newHeight = this.resize_coords.data.height;
                                    //chart Loading Bar True
                                    $("#chart-" + vm.DashboardModel.Dashboard.activeReport.reportContainer.id).height(getHeight);
                                    $("#chart-" + vm.DashboardModel.Dashboard.activeReport.reportContainer.id).width(getWidth);
                                    //chart Loading Bar True
                                    var legendHeight = $("#li_" + vm.DashboardModel.Dashboard.activeReport.reportContainer.id).height();
                                    $("#left" + vm.DashboardModel.Dashboard.activeReport.reportContainer.id).height(legendHeight * 60 / 100);
                                    var variable = "reportLoading_" + vm.DashboardModel.Dashboard.activeReport.reportContainer.id;
                                    vm[variable] = false;
                                    //Chart Hide
                                    $("#chart-" + vm.DashboardModel.Dashboard.activeReport.reportContainer.id).show();
                                    var activeReport = vm.DashboardModel.Dashboard.Report[vm.DashboardModel.Dashboard.activeReportIndex];
                                    $("#li_" + uiWidget[0].firstElementChild.id).addClass("li-box-border");
                                    $scope.chartLoading = false;
                                    $("#chart-" + $element.id).show();
                                    $scope.$apply();
                                },1);
                            }else{
                                setTimeout(function () {
                                    var getHeight = ($("#li_" + vm.DashboardModel.Dashboard.activeReport.reportContainer.id).height()) - 70;
                                    var getWidth = $("#li_" + vm.DashboardModel.Dashboard.activeReport.reportContainer.id).width() - vm.chartWidth(vm.DashboardModel.Dashboard.activeReport.chart.key);
                                    //var newHeight = this.resize_coords.data.height;
                                    //chart Loading Bar True
                                    $("#chart-" + vm.DashboardModel.Dashboard.activeReport.reportContainer.id).height(getHeight);
                                    $("#chart-" + vm.DashboardModel.Dashboard.activeReport.reportContainer.id).width(getWidth);
                                    //chart Loading Bar True
                                    var legendHeight = $("#li_" + vm.DashboardModel.Dashboard.activeReport.reportContainer.id).height();
                                    $("#left" + vm.DashboardModel.Dashboard.activeReport.reportContainer.id).height(legendHeight * 60 / 100);
                                    var variable = "reportLoading_" + vm.DashboardModel.Dashboard.activeReport.reportContainer.id;
                                    vm[variable] = false;
                                    //Chart Hide
                                    $("#chart-" + vm.DashboardModel.Dashboard.activeReport.reportContainer.id).show();
                                    var activeReport = vm.DashboardModel.Dashboard.Report[vm.DashboardModel.Dashboard.activeReportIndex];
                                    $("#li_" + uiWidget[0].firstElementChild.id).addClass("li-box-border");
                                    $scope.chartLoading = false;
                                    $("#chart-" + $element.id).show();
                                    $scope.$apply();
                                },500);
                            }

                            setTimeout(function () {
                                var reportContainerId = 'chart-' + vm.DashboardModel.Dashboard.activeReport.reportContainer.id;
                                if (vm.dataCount < $rootScope.localDataLimit) {
                                    eChart.resetChart(reportContainerId);
                                } else {
                                    eChartServer.resetChart(reportContainerId);
                                }
                            }, 300);
                        },
                        start: function (event, uiWidget, $element) {
                            $("#chart-" + $element.id).hide();
                            //chart Loading Bar True
                            var variable = "reportLoading_" + vm.DashboardModel.Dashboard.activeReport.reportContainer.id;
                            vm[variable] = true;
                            //Chart Hide
                            $(".label_legends" + vm.DashboardModel.Dashboard.activeReport.reportContainer.id).hide();
                            $("#chart-" + vm.DashboardModel.Dashboard.activeReport.reportContainer.id).hide();
                            $("#left" + vm.DashboardModel.Dashboard.activeReport.reportContainer.id).hide();
                        }
                    }
                },
                numberWidgetContainerSettings: {
                    id: 0,
                    name: "Number Widget",
                    sizeX: 6,
                    sizeY: 6,
                    minSizeX: 6,
                    minSizeY: 6,
                    maxSizeX: 12,
                    maxSizeY: 12,
                },
                commonContainersSettings: {
                    id: 0,
                    name: "Report",
                    sizeX: 16,
                    sizeY: 20,
                    minSizeX: 8,
                    minSizeY: 12
                },
                Dashboard: {
                    activeReportIndex: null,
                    activeReport: null,
                    lastReportId: 0,
                    Report: []
                }
            };
            var report = [];
            /*$scope.addChartIconBorder = function (chart) {
                    $(".li_box").removeClass('li-box-border');
                    $("#li_" + chart.key).addClass('li-box-border');
                }*/
            vm.DashboardController = {
                init: function () {
                    vm.DashboardView.init();
                    vm.chartLoading = false;
                    vm.checkboxModel = {};
                    //
                    vm.dateRangeArray = [];
                    vm.dateModal = function () {
                        $('#dateModal').modal({
                            backdrop: 'static',
                            keyboard: false
                        });
                        $('#dateModal').modal('show');
                    }

                },
                findIndexofActiveContainer: function (arraytosearch, key, valuetosearch) {

                    for (var i = 0; i < arraytosearch.length; i++) {

                        if (arraytosearch[i].reportContainer[key] == valuetosearch) {

                            return i;
                        }
                    }
                    return null;
                },
                getGridSettings: function () {
                    return vm.DashboardModel.gridSettings;
                },
                getCommonContainerSettings: function () {
                    return {
                        name: "Report",
                        sizeX: 16,
                        sizeY: 20,
                        minSizeX: 8,
                        minSizeY: 12,
                        maxSizeX: 100,
                        maxSizeY: 100
                    };
                },
                getTextContainerSettings:function(){
                    return {
                        name: "Text",
                        sizeX: 8,
                        sizeY: 4,
                        minSizeX: 6,
                        minSizeY: 4,
                        /* maxSizeX: 30,
                             maxSizeY: 15,*/
                        chartType: "Text"
                    };
                },
                getNumberContainerSettings: function () {
                    return {
                        name: "Number Widget",
                        sizeX: 6,
                        sizeY: 6,
                        minSizeX: 6,
                        minSizeY: 6,
                        maxSizeX: 12,
                        maxSizeY: 12,
                        chartType: "Number Widget"
                    };
                },
                onChartDropToContainer: function (event, index, item, external, type, allowedType) {
                    var view = vm.DashboardView;
                    dc.filterAll();
                    vm.isParameterShown = true;
                    $('.drawer').drawer('open');
                    view.removeGridBorder();
                    vm.rightSideView.render(item);
                    vm.DashboardController.addReportContainer(item);
                    vm.rightSideView.chartSelectView.update(item);
                    vm.dimesionColumn = vm.rightSideController.getDimensions();
                },
                onReportContainerClick: function (id) {
                    var index = vm.DashboardController.findIndexofActiveContainer(vm.DashboardModel.Dashboard.Report, "id", id);
                    if(vm.DashboardModel.Dashboard.Report!=undefined){
                        vm.Attributes = vm.DashboardModel.Dashboard.Report[index].axisConfig;
                        vm.DashboardModel.Dashboard.activeReport = vm.DashboardModel.Dashboard.Report[index];
                        vm.selectedChart = vm.DashboardModel.Dashboard.Report[index].chart;
                        $(".li_box").removeClass('li-box-border');
                        $("#li_" + id).addClass('li-box-border');
                        //$scope.addChartIconBorder(vm.selectedChart);
                    }

                },
                getDashboardProto: function () {
                    return vm.DashboardModel.dashboardPrototypeObject;
                },
                getReportProto: function () {

                    return {
                        reportContainer: {},
                        chart: {},
                        axisConfig: {}
                    };

                },
                addReportContainer: function (chartInfo) {
                    var reportObject = vm.DashboardController.getReportProto();
                    reportObject.chart = chartInfo;
                    vm.DashboardModel.Dashboard.lastReportId++;
                    this.lastId = vm.DashboardModel.Dashboard.lastReportId;
                    vm.dashboardPrototype.reportContainers['chartType'] = chartInfo.name;
                    if($scope.selectedChartInView==undefined){$scope.selectedChartInView={};}
                    if(vm.aggregateText==undefined){vm.aggregateText={};}
                    if (chartInfo.name == "Number Widget") {
                        vm.DashboardModel.numberWidgetContainerSettings.id = this.lastId;
                        var obj = vm.DashboardController.getNumberContainerSettings();
                        obj['id'] = this.lastId;
                        obj['row'] = 0;
                        obj['col'] = 0;
                        reportObject.reportContainer = obj;
                        //vm.DashboardModel.Dashboard.activeReport.reportContainer = reportObject.reportContainer;
                        $scope.selectedChartInView[this.lastId] = sketch.chartData[0];
                    }else if (chartInfo.name == "Text") {
                        vm.DashboardModel.commonContainersSettings.id = this.lastId;
                        var obj = vm.DashboardController.getTextContainerSettings();
                        obj['row'] = 0;
                        obj['col'] = 0;
                        obj['id'] = this.lastId;
                        obj['chartType'] = chartInfo.name;
                        reportObject.reportContainer = obj;

                        $scope.selectedChartInView[this.lastId] = sketch.chartData[0];
                        setTimeout(function () {
                            $(".li_box > .handle-se").removeClass("handle-se-hover");
                            //$(".li_box").removeClass("li-box-border");
                            $("#li_" + vm.DashboardModel.commonContainersSettings.id+"> .handle-se").addClass("handle-se-hover");
                            //$("#li_" + vm.DashboardModel.commonContainersSettings.id).addClass("li-box-border");
                        }, 1);
                    }else {
                        vm.DashboardModel.commonContainersSettings.id = this.lastId;
                        var obj = vm.DashboardController.getCommonContainerSettings();
                        obj['row'] = 0;
                        obj['col'] = 0;
                        obj['id'] = this.lastId;
                        obj['chartType'] = chartInfo.name;
                        reportObject.reportContainer = obj;

                        $scope.selectedChartInView[this.lastId] = sketch.chartData[0];
                        setTimeout(function () {
                            $(".li_box > .handle-se").removeClass("handle-se-hover");
                            //$(".li_box").removeClass("li-box-border");
                            $("#li_" + vm.DashboardModel.commonContainersSettings.id+"> .handle-se").addClass("handle-se-hover");
                            //$("#li_" + vm.DashboardModel.commonContainersSettings.id).addClass("li-box-border");
                        }, 1);
                    }

                    setTimeout(function(){
                        var size = { fontSize : vm.CommonSettingObject.fontSize};
                        $("#Header_Text_"+reportObject.reportContainer.id).css(size);
                        if(vm.CommonSettingObject && vm.CommonSettingObject.report_Header=='yes' && vm.CommonSettingObject.headerTextcolor && vm.checkSettingSave==true){
                            var common_Heading = { backgroundColor : vm.CommonSettingObject.headerBgcolor, color : vm.CommonSettingObject.headerTextcolor};
                            $("#Header_"+reportObject.reportContainer.id).css(common_Heading);
                        }else if(vm.CommonSettingObject && vm.CommonSettingObject.report_Header=='no'){
                            $("#Header_"+reportObject.reportContainer.id).css('display', 'none');
                        }
                        if(vm.CommonSettingObject.border=='yes' && vm.checkSettingSave==true){
                            var common_Heading = { border : vm.CommonSettingObject.border_width+" solid "+" "+vm.CommonSettingObject.border_color};
                            $("#li_"+reportObject.reportContainer.id).css(common_Heading);
                        }
                        if(vm.CommonSettingObject.reportBgcolor){
                            var report_bg = { backgroundColor : vm.CommonSettingObject.reportBgcolor +" !important;"};
                            $("#li_"+reportObject.reportContainer.id).css(report_bg);
                        }else{
                            var report_bg = { backgroundColor : "#FFFFFF !important;"};
                            $("#li_"+reportObject.reportContainer.id).css(report_bg);
                        }
                    },1000);

                    reportObject.reportContainer['image'] = dataFactory.baseUrlData() + chartInfo.image;
                    vm.DashboardModel.Dashboard.activeReport = reportObject;
                    vm.DashboardView.addReportContainerView(reportObject.reportContainer);
                    vm.aggregateText[reportObject.reportContainer.id] = "Sum";
                    report.push(reportObject);
                    vm.DashboardModel.Dashboard.Report.push(reportObject);
                    vm.DashboardModel.Dashboard.activeReportIndex = vm.DashboardModel.Dashboard.Report.length - 1;
                },
                updateReport: function () {
                    vm.DashboardModel.Dashboard.activeReport.chart = JSON.parse(vm.chartType);
                    // vm.DashboardModel.Dashboard.activeReportIndex
                },
                getActiveReport: function () {
                    return vm.DashboardModel.Dashboard.activeReport;
                }
            }
            $scope.addBorder = function (id) {
                /*$(".box").removeClass("box-border");*/
                //$(".li_box").removeClass("li-box-border");
                $(".li_box > .handle-se").removeClass("handle-se-hover");
                $("#li_" + id).addClass("li-box-border");
                $("#li_" + id+"> .handle-se").addClass("handle-se-hover");
                /*$("#" + id).addClass("box-border");*/
                vm.DashboardController.onReportContainerClick(id);
            };
            vm.DashboardView = {
                init: function () {
                    // Gridster Settings
                    $scope.dashboardCommonSettings = vm.DashboardController
                        .getGridSettings();
                    $scope.onDrop = vm.DashboardController.onChartDropToContainer;
                    vm.DashboardView.initDashboardProto();
                },
                activeTab:function(tabObj){
                    vm.activeTab=tabObj.key;
                    if($scope.dashboardPrototype.reportWithTab==undefined){
                        $scope.dashboardPrototype.reportWithTab={};
                    }
                    if($scope.dashboardPrototype.reportWithTab[vm.activeTab]==undefined){
                        $scope.dashboardPrototype.reportWithTab[vm.activeTab]=[];
                    }
                    $(window).trigger('resize');
                },
                autoAdjustChartView: function () {
                    sketch.selfAdjustChart(vm.DashboardModel.Dashboard.activeReport);
                },
                render: function () {

                },
                renderChartInActiveContainer: function () {
                    sketch.pivotTableConfig = undefined;
                    try {
                        vm.DashboardView
                            .resetDataAndChart()
                            .then(vm.DashboardView.processChart)
                            .then(vm.DashboardView.updateDashboardObject)
                            .then(function () {
                                vm.chartLoading = false;
                            })
                            .then(function () {

                                var variable = "reportLoading_" + vm.DashboardModel.Dashboard.activeReport.reportContainer.id;
                                vm[variable] = false;
                                $scope.$apply();
                                $(".label_legends" + vm.DashboardModel.Dashboard.activeReport.reportContainer.id).show();
                                return true;
                            });
                    } catch (e) {
                        vm.chartErrorText = "Some Error occured while drawing this chart";
                        vm.chartLoading = false;
                    }
                    return false;

                },

                initDashboardProto: function () {
                    $scope.dashboardPrototype = vm.DashboardController
                        .getDashboardProto();

                },
                // render dashboard view
                addReportContainerView: function (reportContainerObject) {
                    reportContainerObject['tab']=vm.activeTab;
                    if($scope.dashboardPrototype.reportWithTab==undefined) {
                        $scope.dashboardPrototype.reportWithTab = {};
                    }
                    if($scope.dashboardPrototype.reportWithTab[vm.activeTab]==undefined){
                        $scope.dashboardPrototype.reportWithTab[vm.activeTab]=[];
                    }
                    $scope.dashboardPrototype.reportWithTab[vm.activeTab].push(reportContainerObject);
                    $scope.dashboardPrototype.reportContainers.push(reportContainerObject);
                },



                showGridBorder: function () {
                    $("#dash-border").addClass("dash-border");
                },

                removeGridBorder: function () {
                    $("#dash-border").removeClass("dash-border");
                }
            }
            vm.DashboardController.init();
            $("#loadscreen").show();
            $scope.listdata = [];
            $scope.listOf = false;
            $scope.dashboardDesc = "Dashboard Description"
            vm.containerLoaded = false;
            vm.isSmallSidebar = true;
            vm.isParameterShown = false;
            vm.isNoContainerLoaded = true;
            vm.ischartTypeShown = false;
            vm.isQuerySelected = true;
            vm.placeholder = true;
            vm.filtersTo = [];
            vm.lastAppliedFilters = {};
            vm.saveState = -1;
            // charts list
            vm.MetaDataLoader = true;
            // Function for fetching metadata list
            var dashboardView = function () {
                var promise = new Promise(
                    function (resolve, reject) {

                    });
                return promise;
            };
            var fetchDataAndColumns = function () {
                var promise = new Promise(function (resolve, reject) {
                    var data = {
                        "matadataObject":JSON.stringify(vm.metadataObject),
                        "type":'dashboard'
                    };
                    dataFactory.request($rootScope.DashboardData_Url,'post',data).then(function(response){
                        if(response.data.errorCode==1){
                            vm.tableData = JSON.parse(response.data.result.tableData);
                            vm.metadataObject = vm.metadataObject;
                            vm.tableColumns = vm.metadataObject.connObject.column;
                            calculation
                                .oldObject(JSON.parse(response.data.result.tableColumn))
                                .newObject(vm.tableColumns)
                                .tabledata(vm.tableData)
                                .uniqueArray()
                                .columnCal()
                                .groupObject(vm.group)
                                .columnCategory();
                            vm.tableData = calculation._tabledata;
                            setTimeout(function () {
                                $("#filter").selectpicker();
                            }, 100);
                            resolve();
                        }
                    });
                });
                return promise;
            }
            /*
                 * Report group
                 */
            // vm.publicViewType="sharedview";
            vm.publicViewType="public_group";
            vm.grpType = 'selectGrp';
            vm.reportGrp={};
            dataFactory.request($rootScope.UsergroupList_Url,'post',"").then(function(response){
                vm.userGroup=response.data.result;
            });
            // dataFactory.request($rootScope.ReportGroupList_Url,'post',"").then(function(response){
            //     vm.reportGroup=response.data.result;
            // });
            vm.reportGroupFun=function(){
                $("#reportGroupSelect").selectpicker();
                $("#userGroupSelect").selectpicker();
                vm.realTime={};
                vm.realTime.flag=false;
                var flag=0;
                var tabKey="";
                vm.tabs.forEach(function(d){
                    if($scope.dashboardPrototype.reportWithTab==undefined || $scope.dashboardPrototype.reportWithTab[d.key]==undefined || $scope.dashboardPrototype.reportWithTab[d.key].length==0){
                        flag=1;
                        tabKey=d.key;
                    }
                })
                if(flag==0){
                    $('#reportGroup').modal('show');
                    $("#tabBtn_"+vm.tabs[0].key).trigger('click');
                }else{
                    dataFactory.errorAlert(tabKey+" tab can't be empty");
                }
            }

            vm.groupTypeValue = '';
            vm.checkGrp = function(type){
                vm.groupTypeValue = type;
                $(".reportGroupSelect").selectpicker();
                $(".userGroupSelect").selectpicker();
            }

            vm.report_Grp = false;
            vm.userGrpAdd = function(){
                vm.report_Grp = true;
                var data = {
                    'usrGrp' : vm.reportGrp.user_Group,
                }
                dataFactory.request($rootScope.ReportGroupList_Url,'post',data).then(function(response){
                    $scope.reportGroup = response.data.result;
                    $scope.$apply();
                    $(".reportGroupSelect").selectpicker('refresh');
                });
            }

            $scope.groupTypeSave = function(reportGrp){
                if(vm.groupTypeValue.length == 0 || vm.groupTypeValue == ''){
                    vm.groupTypeValue = 'selectGrp';
                }
                reportGrp.grpType = vm.groupTypeValue;
                if(Object.keys(vm.reportGrp).length==0 && vm.publicViewType=='sharedview'){
                    dataFactory.errorAlert("Select group");
                }else{
                    if(reportGrp.name && !reportGrp.userGroup){
                        dataFactory.errorAlert("Select report group");
                        return;
                    }
                    vm.reportGrp = reportGrp;
                    $('#reportGroup').modal('hide');
                    vm.save(reportGrp);
                }
            }


            vm.backBtnGrp = false;
            $scope.selectGroup = function(divClass){
                if(divClass=='back'){
                    vm.publicViewType = "public_group";
                    vm.backBtnGrp = false;
                    $(".mainDiv").slideDown('fast');
                    $(".select").slideUp('fast');
                    $(".create").slideUp('fast');
                    $(".sGrp").hide();
                    $("[name='optionsRadios'][value=public_group]").prop('checked', true);
                    $(".footerDiv").show();
                }else{
                    $("."+divClass).slideDown('fast');
                    $(".footerDiv").slideDown('fast');
                    $(".mainDiv").slideUp('fast');
                }
            }

            /*
             * Check view
             */
            $scope.checkView=function(page){
                $scope.publicViewType=page;
                if(page=='public_group'){
                    vm.backBtnGrp = false;
                    $(".subDiv").hide();
                    $(".footerDiv").show();
                }else{
                    vm.backBtnGrp = true;
                    $(".sGrp").show();
                    $(".footerDiv").show();
                    $(".reportGroupSelect").selectpicker();
                    $(".userGroupSelect").selectpicker();
                }
            }

            //End report Group
            vm.save = function (reportGrp) {
                if(vm.dashName.dashboardName!=undefined && vm.dashName.dashboardName!="" && vm.DashboardModel.Dashboard.Report.length>0){
                    $('#reportGroup').modal('hide');
                    $(".loadingBar").show();
                    var metadataObject = $scope.metadataObject;
                    metadataObject['column'] = vm.tableColumns;
                    vm.DashboardModel.Dashboard['customMeasure'] = vm.customMeasure;
                    vm.DashboardModel.Dashboard['customGroup'] = vm.customGroup;
                    var reportsObject = vm.DashboardModel.Dashboard;
                    reportsObject['tabs']=vm.tabs;
                    var newDate = new Date();
                    var image_name = new Date().getTime();
                    html2canvas($("#tab"+vm.tabs[0].key+'> div'),
                        {
                            onrendered: function (canvas) {
                                var theCanvas = canvas;
                                var img = canvas.toDataURL("image/png");
                                var output = encodeURIComponent(img);
                                var data={
                                    "image":img,
                                    "image_name":image_name,
                                    "logo":vm.logo
                                };
                                dataFactory.request($rootScope.sharedViewImage_Url,'post',data).then(function(response){
                                    if(response.data.errorCode==1){
                                        var dashSaveObj = {};
                                        var dashRepObj = {};
                                        var dObj = "";
                                        reportsObject['metadataObject'] = $scope.metadataObject;
                                        reportsObject['connectionObject'] = $scope.metadataObject.connObject.connectionObject;
                                        reportsObject['colorObject'] = eChart.chartRegistry.listAttributes();
                                        reportsObject['realtime']=vm.realTime;

                                        vm.conatinerFilter.forEach(function (d,i){
                                            d['selectedValue'] = vm.allFilters[d.key].filterValue;
                                        });

                                        var data={
                                            "name":vm.dashName.dashboardName,
                                            "dashboardDesc":$scope.dashboardDesc,
                                            "repotsObject":JSON.stringify(reportsObject),
                                            "filterShow":vm.filterShowHidden,
                                            "filterBy":JSON.stringify(vm.conatinerFilter),
                                            "image":image_name,
                                            "logo":"logo_"+image_name,
                                            "group":JSON.stringify(vm.group),
                                            "dashboardId":vm.dashboardId,
                                            "reportGroup":reportGrp,
                                            "publicViewType":vm.publicViewType
                                        };

                                        dataFactory.request($rootScope.sharedViewSave_Url,'post',data).then(function(response){
                                            if (response.data.errorCode==1) {
                                                $(".loadingBar").hide();
                                                dataFactory.successAlert(response.data.message);
                                                $window.location = "#/sharedView/";
                                            } else {
                                                dataFactory.errorAlert(response.data.message);
                                                $(".loadingBar").hide();
                                            }
                                        });
                                    }else{
                                        dataFactory.errorAlert(response.data.message);
                                    }
                                });
                            }
                        });
                }else{
                    dataFactory.errorAlert("Name and minimum one report required");
                }
            };
            vm.viewCommonSetting = function(){
                $('#Common_View_Setting').modal('show');
            }
            vm.companyHeader = 'no';
            vm.CommonSettingData = function(CommonSettingObject){
                vm.checkSettingSave=true;
                var style = {};
                style = CommonSettingObject;
                style.border = CommonSettingObject.border;
                style.borderColor = CommonSettingObject.border_color;
                style.borderWidth = CommonSettingObject.border_width;
                //Save common setting object
                vm.DashboardModel.Dashboard['commonStyle']=style;
                vm.DashboardModel.Dashboard['companyHeader']=vm.company;

                if(CommonSettingObject && CommonSettingObject.reportBgcolor){
                    var report_Bg = { backgroundColor : CommonSettingObject.reportBgcolor + ' !important'};
                    $(".li_box").css(report_Bg);
                }else{
                    var report_Bg = { backgroundColor : '#FFFFFF !important'};
                    $(".li_box").css(report_Bg);
                }
                if(CommonSettingObject.report_Header == 'yes' || CommonSettingObject.report_Header == 'Yes'){
                    $(".Header_Setting").css('display', 'block');
                    $(".Common_Head").css('display', 'block');
                    var common_Heading = { backgroundColor : CommonSettingObject.headerBgcolor + ' !important', color : CommonSettingObject.headerTextcolor + ' !important' };
                    $(".Common_Head").css(common_Heading);
                    var size = { fontSize : CommonSettingObject.fontSize};
                    $(".Common_Head").css(size);
                }else if(vm.CommonSettingObject && vm.CommonSettingObject.report_Header=='no'){
                    $(".Header_Setting").css('display', 'none');
                    $(".Common_Head").css('display', 'none');
                }
                if(CommonSettingObject.main_Header=="yes"){
                    var common_Heading = { backgroundColor : CommonSettingObject.companyBackground, color : CommonSettingObject.companyColor};
                    $(".companyHeader").css(common_Heading);
                    vm.companyHeader='yes';
                    $(".companyHeader").show();
                }else{
                    vm.companyHeader='no';
                    $(".companyHeader").hide();
                }
                if(CommonSettingObject.border=='yes'){
                    var common_Heading = { border : CommonSettingObject.border_width+" solid "+" "+CommonSettingObject.border_color};
                    $(".li_box").css(common_Heading);
                }

                vm.DashboardModel.Dashboard.Report.forEach(function (d,i) {
                    if(d.axisConfig.CustomSettingObject){
                        if(d.axisConfig.CustomSettingObject.report_Header == 'yes'){
                            var custom_Heading = { backgroundColor : d.axisConfig.CustomSettingObject.headerBgColor + ' !important', color : d.axisConfig.CustomSettingObject.headerTextColor + '!important' };
                            var reportBg = { backgroundColor : d.axisConfig.CustomSettingObject.reportBgColor + ' !important;'};
                            $("#Header_Text_" + d.reportContainer.id).css(custom_Heading);
                            $("#Header_Setting_" + d.reportContainer.id).css(custom_Heading);
                            $("#Header_" + d.reportContainer.id).css(custom_Heading);
                            $("#li_" + d.reportContainer.id).css(reportBg);
                            var size = { fontSize : vm.Attributes.CustomSettingObject.fontSize};
                            $("#Header_Text_" + vm.Custom_Container_Id).css(size);
                        }else if(d.axisConfig.CustomSettingObject.report_Header =='no'){
                            $("#Header_" + d.reportContainer.id).css('display', 'none');
                            $("#Header_Setting_" + d.reportContainer.id).css('display', 'none');
                            if(vm.CommonSettingObject.border=='yes' && vm.checkSettingSave==true){
                                var common_Heading = { border : vm.CommonSettingObject.border_width+" solid "+" "+vm.CommonSettingObject.border_color};
                                $(".li_box").css(common_Heading);
                            }
                        }
                    }
                });

                $('#Common_View_Setting').modal('hide');
                dataFactory.successAlert("Setting Saved Successfully");
            }

            // Custom Setting
            vm.viewCustomSetting = function(index){
                for(var i=0; i<vm.DashboardModel.Dashboard.Report.length; i++){
                    vm.CustomSettingObject = {};
                    if(index == vm.DashboardModel.Dashboard.Report[i].reportContainer.id){
                        if(vm.DashboardModel.Dashboard.Report[i].axisConfig.CustomSettingObject){
                            vm.CustomSettingObject = vm.DashboardModel.Dashboard.Report[i].axisConfig.CustomSettingObject;
                        }else{
                            vm.CustomSettingObject.report_Header = 'yes';
                            vm.CustomSettingObject.headerTextColor = '#000000';
                            vm.CustomSettingObject.fontSize = '14px';
                            vm.CustomSettingObject.headerBgColor = '#FFFFFF';
                            vm.CustomSettingObject.reportBgColor = '#FFFFFF';
                            vm.CustomSettingObject.axisLabelColor = '#000000';
                            vm.CustomSettingObject.axisColor = '#000000';
                        }
                        break;
                    }
                }
                vm.Custom_Container_Id = index;
                $('#Custom_View_Setting').modal('show');
            }

            // Comapny Setting
            vm.companySet={};
            /*vm.companyHeaderSet = function(){
                 $('#companyHeaderSet').modal('show');
                 }*/
            vm.dataimg = 'none';
            vm.logoCheck=false;

            vm.companyHeaderSave=function(company){
                vm.logoCheck=true;
                var f = document.getElementById('logo').files[0];
                var r = new FileReader();
                r.onloadend = function(e) {
                    $scope.data = e.target.result;
                    var img = document.getElementById('img');
                    vm.logo = 'data:image/jpeg;base64,' + btoa(e.target.result);
                    img.src = 'data:image/jpeg;base64,' + btoa(e.target.result);
                    //$("#img").appendChild(img);
                }
                r.readAsBinaryString(f);

                vm.company.name=company.name;
                vm.company.description=company.description;
                //vm.company.log=company.logo;
                $('#companyHeaderSet').modal('hide');
            }

            vm.CustomSettingObject = {};
            vm.CustomSettingObject.report_Header = 'yes';
            vm.CustomSettingData = function(){
                vm.Attributes.CustomSettingObject = {};
                if(vm.Attributes.CustomSettingObject){
                    vm.Attributes.CustomSettingObject = vm.CustomSettingObject;
                }else{
                    vm.Attributes.CustomSettingObject.report_Header = 'no';
                    vm.Attributes.CustomSettingObject.headerTextColor = '#FFFFFF';
                    vm.Attributes.CustomSettingObject.fontSize = '14px';
                    vm.Attributes.CustomSettingObject.headerBgColor = '#58A69F';
                    vm.Attributes.CustomSettingObject.reportBgColor = '#FFFFFF';
                    vm.Attributes.CustomSettingObject.axisLabelColor = '#000000';
                    vm.Attributes.CustomSettingObject.axisColor = '#000000';
                }
                var reportIndex;
                vm.DashboardModel.Dashboard.Report.forEach(function(d,index){
                    if(d.reportContainer.id == vm.DashboardModel.Dashboard.activeReport.reportContainer.id){
                        reportIndex = index;
                    }
                });
                if(vm.Attributes.CustomSettingObject.report_Header == 'yes'){
                    var custom_Heading = { backgroundColor : vm.Attributes.CustomSettingObject.headerBgColor + ' !important', color : vm.Attributes.CustomSettingObject.headerTextColor + ' !important', fontSize : vm.Attributes.CustomSettingObject.fontSize + ' !important' };
                    var reportBg = { backgroundColor : vm.Attributes.CustomSettingObject.reportBgColor + ' !important;'};
                    $("#Header_Text_" + vm.Custom_Container_Id ).css(custom_Heading);
                    $("#Header_Setting_" + vm.Custom_Container_Id ).css(custom_Heading);
                    $("#Header_" + vm.Custom_Container_Id ).css(custom_Heading);
                    $("#li_" + vm.Custom_Container_Id ).css(reportBg);
                    var size = { fontSize : vm.Attributes.CustomSettingObject.fontSize};
                    $("#Header_Text_" + vm.Custom_Container_Id).css(size);
                }else if(vm.Attributes.CustomSettingObject.report_Header =='no'){
                    $("#Header_" + vm.Custom_Container_Id).css('display', 'none');
                    $("#Header_Setting_" + vm.Custom_Container_Id).css('display', 'none');
                    if(vm.CommonSettingObject.border=='yes' && vm.checkSettingSave==true){
                        var common_Heading = { border : vm.CommonSettingObject.border_width+" solid "+" "+vm.CommonSettingObject.border_color};
                        $(".li_box").css(common_Heading);
                    }
                    if(vm.CommonSettingObject.reportBgcolor){
                        var report_bg = { backgroundColor : vm.CommonSettingObject.reportBgcolor +" !important;"};
                        $(".li_box").css(report_bg);
                    }else{
                        var report_bg = { backgroundColor : "#FFFFFF !important;"};
                        $(".li_box").css(report_bg);
                    }
                }
                $('#Custom_View_Setting').modal('hide');

                if(vm.DashboardModel.Dashboard.activeReport.axisConfig.dataFormat == undefined){
                    vm.DashboardModel.Dashboard.activeReport.axisConfig.dataFormat = {};
                }
                if(vm.DashboardModel.Dashboard.activeReport.axisConfig.CustomSettingObject != undefined){
                    vm.DashboardModel.Dashboard.activeReport.axisConfig.dataFormat['axisColor'] = vm.DashboardModel.Dashboard.activeReport.axisConfig.CustomSettingObject.axisColor;
                    vm.DashboardModel.Dashboard.activeReport.axisConfig.dataFormat['axisLabelColor'] = vm.DashboardModel.Dashboard.activeReport.axisConfig.CustomSettingObject.axisLabelColor;
                }

                sketchServer
                    .accessToken($rootScope.accessToken)
                    .axisConfig(vm.DashboardModel.Dashboard.activeReport.axisConfig)
                    .data([])
                    .container(vm.DashboardModel.Dashboard.activeReport.reportContainer.id)
                    .chartConfig(vm.DashboardModel.Dashboard.activeReport.chart)
                    .render(function (drawn) {
                        $("#chart-" + vm.DashboardModel.Dashboard.activeReport.reportContainer.id).show();
                    });
                
            }


            vm.allFilterApply=function(){
                $(".loadingBar").show();
                sketchServer.applyAllFilter(vm.allFilters,vm.metadataId);
                vm.filterModalClose();
            }
            vm.filterModalClose=function () {
                $scope.filterApply=false;
                $(window).trigger('resize');
            }
            vm.cascadeFilter=function(date,columnName,index){
                date=date['filterValue'];
                $(".loadingBar").show();
                var totalLength=Object.keys(vm.allFilters).length;
                sketchServer.applyFilter(date, columnName, vm.metadataId,totalLength,index,function(data,filterObj,status){
                    if (status) {
                        try{

                            // vm.allFilters[filterObj.key]['filterValue']=data;
                            if(filterObj.value=="varchar" || filterObj.value=="char" || filterObj.value=="text"){
                                vm.conatinerFilterTo[filterObj.key] = data;
                                vm.allFilters[filterObj.key].filterValue = data;
                                $scope.$apply();
                                try{
                                    $("[id^=filterSelect_]").each(function () {
                                        if($(this).attr('id')!="filterSelect_"+index) {
                                            $(this).selectpicker('destroy', true);
                                        }
                                    })
                                }catch (e){

                                }

                                setTimeout(function () {
                                    $("[id^=filterSelect_]").each(function () {
                                        if($(this).attr('id')!="filterSelect_"+index){
                                            $(this).selectpicker();
                                        }
                                    });
                                    $(".loadingBar").hide();
                                }, 1000);
                            }else if(filterObj.value=="decimal" || filterObj.value=="int" || filterObj.value=="bigint" || filterObj.value=="double" || filterObj.value=="float"){
                                /*
                                   Range filter
                                */
                                setTimeout(function () {
                                    var min = Math.min.apply(Math,data);
                                    var max = Math.max.apply(Math,data);
                                    if(min && max){
                                        if (min == null) {
                                            min = 0;
                                        }
                                        if(min==max){
                                            max=max+1;
                                        }
                                        var config = {
                                            orientation: "horizontal",
                                            start: [min,max],
                                            range: {
                                                min: min,
                                                max: max,
                                            },
                                            connect: 'lower',
                                            direction: "ltr",
                                            step: 10,
                                        };
                                        var nonLinearSlider = document.getElementById(filterObj.key.replace("(","(").replace(")",")")+ "-Filter");
                                        document.getElementById(filterObj.key.replace("(","(").replace(")",")")+ "-Filter").noUiSlider.destroy();
                                        noUiSlider.create(nonLinearSlider, {
                                            animate: true,
                                            start: [min, max],
                                            connect: true,
                                            range: {
                                                min: parseInt(min),
                                                max: parseInt(max)
                                            },
                                            step: 1,
                                            tooltips: [wNumb({
                                                decimals: 0
                                            }), wNumb({
                                                decimals: 0
                                            })]
                                        });
                                        vm.allFilters[filterObj.key]['filterValue'] = [min, max];
                                        nonLinearSlider.noUiSlider.on('update', function (values, handle) {
                                            vm.allFilters[filterObj.key]['filterValue'] = values;

                                        });
                                        nonLinearSlider.noUiSlider.on('change', function (values, handle) {
                                            vm.filterBy(values,filter);
                                        });
                                    }
                                    $(".loadingBar").hide();
                                },1000);
                                /*
                                  End range filter
                                 */
                            }
                        }catch(e){

                        }
                    }else{
                        setTimeout(function () {
                            $(".loadingBar").hide();
                        }, 1000);
                    }
                });
            }
            vm.chartWidth=function (type) {
                if(type=="_mapChartJs" || type=="_PivotCustomized"){
                    return 30;
                }
                return 40;
            }
        } ]);