'use strict';
/* Controllers */
angular.module('app', ['ngSanitize','gridster'])
    .controller('publicViewListController',['$scope','$sce','dataFactory','$rootScope','$window','$q','$stateParams','$cookieStore','$location',function($scope,$sce,dataFactory,$rootScope,$window,$q,$stateParams,$cookieStore,$location) {
        //--- Top Menu Bar
        var vm=$scope;
        var data={};
        var addClass="";
        var viewClass="";
        if($location.path()=='/dashboard/'){
            viewClass='activeli';
        }else{
            addClass='activeli';
        }
        data['navTitle']="SHARED VIEW";
        data['icon']="fa fa-line-chart";
        data['navigation']=[
            {
                "name":"Add",
                "url":"#/dashboard/add",
                "class":addClass,
            },
            {
                "name":"View",
                "url":"#/dashboard/",
                "class":viewClass,
            },
            {
                "name":"Close",
                "url":"#/dashboard/",
                "class":viewClass,
            }
        ];
        $rootScope.$broadcast('subNavBar', data);
        var userId = $cookieStore.get('userId');
        vm.companyId = $cookieStore.get('companyId');
        vm.userPort = $cookieStore.get('userPort');
        vm.imageUrlBaseUrl = dataFactory.baseUrlData()+"dashboardImage/";
        //Public View List Api
        vm.sharedViewListObj=function () {
            var sharedViewListObj = new Promise(function (resolve, reject){
                dataFactory.request($rootScope.sharedViewFolderList_Url,'post',"").then(function(response){
                    console.log(response.data.result);
                    resolve(response);
                    if(response.data.errorCode==1){
                        vm.tempSharedViewList = response.data.result;
                        vm.sDeptViewList = [];
                        vm.sDeptViewList = Object.keys(response.data.result);
                    }else{
                        dataFactory.errorAlert(response.data.message);
                    }
                });
            });
            sharedViewListObj.then(temp_one);
        }
        vm.sharedViewListObj();
        function temp_one() {
            vm.dataLoaded=true;
            vm.loading=true;
            $scope.$apply();
            if(vm.loading){
                $(".loaderImage").fadeOut("slow");
            }
            setTimeout(function(){
                $('[rel="tooltip"]').tooltip();
            },100);
        }

        vm.permissionCheck=function(permission){
            return dataFactory.checkPermission(permission);
        }

        //Add Public View
        vm.addPublicDashboard = function(){
            $window.location = '#/publicview/add/1';
        };

        vm.copyLink = function (element) {
            var temp = $("<input>");
            $("body").append(temp);
            temp.val($(element).text()).select();
            document.execCommand("copy");
            temp.remove();
            dataFactory.successAlert('Link Copied');
        }
        vm.grpFolder = false;

        vm.selectedGrpFolder = function(){
            setTimeout(function(){
                $('.step1').slideUp('fast');
                $('.stepLoading').slideUp('fast');
                $('.step2').slideDown('fast');
                $('.stepBack').slideDown('fast');
                vm.loadPopOverImage();
            }, 300);
        }

        vm.toggleGrpFolder = function(e,sView,index){
            vm.grpFolder = false;
            vm.sharedViewList = vm.tempSharedViewList[vm.selectedSharedViewName][sView];
            setTimeout(function(){
                $(function(){
                    $('[data-toggle="tooltip"]').tooltip();
                });
            },100);
            vm.loadingBarDrawer = true;
            $('.stepFolder2').slideUp('fast');
            $('.stepBack').slideUp('fast');
            setTimeout(function(){
                vm.loadingBarDrawer = false;
                $('.step1').slideUp('fast');
                $('.stepLoading').slideUp('fast');
                $('.step2').slideDown('fast');
                $('.stepBack').slideDown('fast');
                vm.loadPopOverImage();
            }, 300);
        }

        $scope.toggleImage = function(e,sView,index){
            vm.selectedSharedViewName = sView;
            $cookieStore.put("department", vm.selectedSharedViewName);
            if(sView == 'Public View'){
                vm.grpFolder = false;
                setTimeout(function(){
                    $(function(){
                        $('[data-toggle="tooltip"]').tooltip();
                    });
                },100);
                vm.loadingBarDrawer = true;
                $('.stepFolder1').slideUp('fast');
                $('.stepBack').slideUp('fast');
                vm.sharedViewList = vm.tempSharedViewList[vm.selectedSharedViewName];
                setTimeout(function(){
                    vm.loadingBarDrawer = false;
                    $('.step1').slideUp('fast');
                    $('.stepLoading').slideUp('fast');
                    $('.step2').slideDown('fast');
                    $('.stepBack').slideDown('fast');
                    vm.loadPopOverImage();
                }, 300);
            }else{
                vm.grpFolder = true;
                vm.groupFolderList = Object.keys(vm.tempSharedViewList[sView]);
                setTimeout(function(){
                    $('[rel="tooltip"]').tooltip();
                },100);
                vm.selectedGrpFolder();
            }
        }


        vm.selected_SharedView = function(e,selectedSharedObj,index){
            setTimeout(function(){
                $(function(){
                    $('[data-toggle="tooltip"]').tooltip();
                });
            },100);
            $scope.selectedConfirm=1;
            $('.step3').slideDown('fast');
            $('.imageToggle').removeClass('selectedImage');
            $(e.currentTarget).addClass('selectedImage');
            $scope.auth_key = selectedSharedObj.auth_key;
            $scope.sharedViewID = selectedSharedObj.sharedview_id;
            $scope.selectedIndex = index;
            // copyLink
            vm.publicViewCopyLink = false;
            if($rootScope.roleObj[0].roles[0].display_name == 'Super Admin' || $rootScope.roleObj[0].roles[0].display_name == 'Admin'){
                if(selectedSharedObj.type == "publicview"){
                    vm.publicViewCopyLink = true;
                }else{
                    vm.publicViewCopyLink = false;
                }
            }
            $scope.baseUrl = $rootScope.rootUrl;
        }



// SharedView Delete
        $scope.showSwal = function(type){
            if(type == 'warning-message-and-cancel') {
                swal({
                    title: 'Are you sure?',
                    text: 'You want to delete Shared View !',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: 'Yes',
                    cancelButtonText: 'No',
                    confirmButtonClass: "btn btn-success",
                    cancelButtonClass: "btn btn-danger",
                    buttonsStyling: false,
                }).then(function(value){
                    if(value){
                        $scope.delete();
                    }
                },
                function (dismiss) {

                })
            }
        }

        vm.delete = function(){
            var index = $scope.selectedIndex;
            var id = $scope.sharedViewID;
            vm.sharedViewList.forEach(function (d,i) {
                if(d.sharedview_id==id){
                    index=i;
                    var data={
                        "id":id
                    };
                    dataFactory.request($rootScope.sharedViewDelete_Url,'post',data).then(function(response){
                        if(response.data.errorCode==1){
                            dataFactory.successAlert(response.data.message);
                            vm.sharedViewList.splice(index,1);
                            $('.step1').slideDown('fast');
                            $('.stepFolder1').slideDown('fast');
                            $('.step2').slideUp('fast');
                            $('.step3').slideUp('fast');
                            $('.stepBack').slideUp('fast');
                        }else{
                            dataFactory.errorAlert(response.data.message);
                        }
                    });
                }
            });

            $('.step1').slideDown('fast');
            $('.stepFolder1').slideDown('fast');
            $('.step2').slideUp('fast');
            $('.step3').slideUp('fast');
        }
// SharedView Delete





        vm.backToSharedView = function(){
            $scope.searchDashboard = '';
            $scope.selectedConfirm=0;
            $('.step1').slideDown('fast');
            $('.stepFolder1').slideDown('fast');
            $('.step2').slideUp('fast');
            $('.step3').slideUp('fast');
            $('.stepBack').slideUp('fast');
        }


        vm.loadPopOverImage = function() {
            $('[data-toggle="popover"]').popover({
                container: 'body',
                html: true,
                placement: 'auto',
                trigger: 'hover',
                content: function() {
                    var url = $(this).data('full');
                    return '<img src="' + url + '" onError="this.src = \'assets/img/logo.jpg\';" style="width:200px;height:200px;">';
                }
            });
        }


// sort sharedView list asc-desc Start
        $scope.sharedViewList_Order = 'desc';
        $scope.sharedView_List_order = '-id';
        $scope.orderSharedViewList = function(){
            if($scope.sharedViewList_Order == 'desc'){
                $scope.sharedView_List_order = '-id';
            }
            else if($scope.sharedViewList_Order == 'asc'){
                $scope.sharedView_List_order = 'id';
            }
        }
// sort sharedView list asc-desc End
        $scope.exportImport={
            init:function () {
                $scope.import=0;
                $scope.overwrite={};
                $scope.overwrite.datasource=1;
                $scope.overwrite.metadata=1;
                $scope.overwrite.dashboard=1;
                $scope.overwrite.sharedview=1;
                /*
                    Which folder to save
                 */
                // Call modal

                vm.checkView=function(page){
                    $(".realtime").hide();
                    vm.publicViewType=page;
                    if(page=='publicview'){
                        $(".subDiv").hide();
                        $(".footerDiv").show();
                    }
                    else{
                        $(".subDiv").show();
                        $(".footerDiv").hide();
                    }
                }
                vm.selectGroup=function(divClass){
                    if(divClass=='back'){
                        $(".mainDiv").slideDown('fast');
                        $(".select").slideUp('fast');
                        $(".create").slideUp('fast');
                        $(".footerDiv").slideUp('fast');
                    }else{
                        $("."+divClass).slideDown('fast');
                        $(".footerDiv").slideDown('fast');
                        $(".mainDiv").slideUp('fast');
                    }
                }

                $scope.groupTypeModal=function () {
                    $scope.publicViewType="sharedview";
                    $(".footerDiv").hide(); 
                    $scope.reportGrp={};
                    dataFactory.request($rootScope.UsergroupList_Url,'post',"").then(function(response){
                        $scope.userGroup=response.data.result;
                    });
                    dataFactory.request($rootScope.ReportGroupList_Url,'post',"").then(function(response){
                        $scope.reportGroup=response.data.result;
                    });
                    $('#reportGroup').modal('show');
                }
                /*
                  Folder to save function
                 */
                $scope.save=function () {
                    $(".loaderImage").show();
                    if($scope.metadataImport['file']==undefined){
                        $scope.addClassRemove("File is required");
                        return false;
                    }
                    var file=$scope.metadataImport['file'];
                    var allowedFiles=["think"];
                    var file=$scope.metadataImport.file;
                    var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:])+(" + allowedFiles.join('|') + ")$");
                    if (!regex.test(file.name.toLowerCase())) {
                        $scope.addClassRemove("Please upload files having extensions: <b>" + allowedFiles.join(', ') + "</b> only.");
                        return false;
                    }
                    var data={
                        "overwrite":$scope.overwrite 
                    };
                    var url=$rootScope.sharedViewImportSave_Url;
                    dataFactory.importDataSave(url,file,'',$scope.reportGrp,$scope.publicViewType,JSON.stringify(data)).then(function(response) {
                        if(response.data.errorCode){
                            $(".loaderImage").hide();
                            $scope.exportImport.backToFolder();
                            dataFactory.successAlert(response.data.message);
                            vm.sharedViewListObj();
                        }else{
                            dataFactory.errorAlert(response.data.message);
                        }
                    });
                    $('#reportGroup').modal('hide');
                }
                $scope.selectGroup=function(divClass){
                    if(divClass=='back'){
                        $(".mainDiv").slideDown('fast');
                        $(".select").slideUp('fast');
                        $(".create").slideUp('fast');
                        $(".footerDiv").slideUp('fast');
                    }else{
                        $("."+divClass).slideDown('fast');
                        $(".footerDiv").slideDown('fast');
                        $(".mainDiv").slideUp('fast');
                    }
                    if(divClass=='select' || divClass=='create'){
                        $scope.reportGrp={};
                    }
                }
            },
            importBtn:function () {
                $scope.import=1;
                $('.step1').slideUp('fast');
                $('.step2').slideUp('fast');
                $('.step3').slideDown('fast');
                $scope.testsuccess=false;
                $scope.datasourceStatus=0;
                $scope.metadataStatus=0;
            },
            testImport:function (Import,type) {
                $(".loaderImage").fadeIn("slow");
                if($scope.metadataImport==undefined || $scope.metadataImport['file']==undefined){
                    dataFactory.errorAlert("File is required");
                    return false;
                }
                var file=$scope.metadataImport['file'];
                var allowedFiles=["think"];
                var file=$scope.metadataImport.file;
                var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:])+(" + allowedFiles.join('|') + ")$");
                if (!regex.test(file.name.toLowerCase())) {
                    dataFactory.errorAlert("Please upload files having extensions: <b>" + allowedFiles.join(', ') + "</b> only.");
                    return false;
                }
                $scope.testBtn="Loading...";
                var url=$rootScope.sharedViewImport_Url;
                dataFactory.importData(url,file,type).then(function(response) {
                    if(response.data.errorCode){
                        $(".loadingBar").hide();
                        $scope.Attributes=response.data.result;
                        dataFactory.errorAlert(response.data.message);
                        $scope.importSave=1;
                        $scope.testsuccess=true;
                        $scope.datasourceStatus=response.data.result.datasource;
                        $scope.metadataStatus=response.data.result.metadata;
                        $scope.dashboardStatus=response.data.result.dashboard;
                        $scope.sharedviewStatus=response.data.result.sharedview;
                        $scope.datasourceOverwrite=true;
                        $scope.metadataOverwrite=true;
                    }else{
                        dataFactory.errorAlert(response.data.message);
                    }
                    setTimeout(function(){
                        $scope.loading=false;
                        if(!$scope.loading){
                            $(".loaderImage").fadeOut("slow");
                        }
                    },1000);
                });
            },
            backToFolder:function () {
                $scope.import=0;
                $('.step3').slideUp('fast');
                $('.step2').slideDown('fast');
                $('.step1').slideDown('fast');
            },
            export:function () {
                var data={
                    "sharedview_id":$scope.sharedViewID
                };
                dataFactory.request($rootScope.sharedViewExport_Url,'post',data).then(function(response) {
                    if(response.data.errorCode){
                        var link=document.createElement('a');
                        link.href=response.data.result.url;
                        link.download=response.data.result.fileName;
                        link.click();
                        var data={
                            "url":response.data.result.url
                        };
                        dataFactory.request($rootScope.ExportFileDelete_Url,'post',data);
                    }
                })
            }
        };
        $scope.exportImport.init();

    }]);