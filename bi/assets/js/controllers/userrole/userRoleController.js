angular.module('app', ['ngSanitize', 'gridster'])
    .controller('userRoleController', ['$scope', '$sce', 'dataFactory', '$filter', '$stateParams', '$location', '$window', '$rootScope','$cookieStore',
        function ($scope, $sce, dataFactory, $filter, $stateParams, $location, $window, $rootScope,$cookieStore) {
    	var vm=$scope;
    	var data={};
		data['navTitle']="Role Assign";
		data['icon']="fa fa-user";
		$rootScope.$broadcast('subNavBar', data);
        var userId=$cookieStore.get('userId');
        vm.role={}; 
    	vm.userRole={};   
    	vm.landingPageObj = {};
    	

//    	setTimeout(function(){
//    		$("#landingPageSelect").selectpicker();
//    	},10);
    	
    	
    	
    	
// permission    	
    		vm.dashboardArr = [], vm.dataSourceArr = [], vm.metadataArr = [], vm.sharedViewArr = [], vm.AdministrationArr = [], vm.permissionObj = {};
	    	dataFactory.request($rootScope.Permissionlist_Url,"post","").then(function(response){
	    		if(response.data.errorCode==1){
	        		response.data.result.forEach(function(d){	
	        			if((d.display_name).includes('Dashboard')){
	        				vm.dashboardArr.push(d);
	        			}else if((d.display_name).includes('Datasource')){
	        				vm.dataSourceArr.push(d);
	        			}else if((d.display_name).includes('Metadata')){
	        				vm.metadataArr.push(d);
		    			}else if((d.display_name).includes('Sharedview')){
		    				vm.sharedViewArr.push(d);
						}else if((d.display_name).includes('Administration')){
							vm.AdministrationArr.push(d);
						}
	        		});	       
	        		vm.permissionObj['Dashboard'] = vm.dashboardArr;
	        		vm.permissionObj['DataSource'] = vm.dataSourceArr;
	        		vm.permissionObj['MetaData'] = vm.metadataArr;
	        		vm.permissionObj['SharedView'] = vm.sharedViewArr;
	        		vm.permissionObj['Administration'] = vm.AdministrationArr;

	        		vm.landingPageObj['Dashboard'] = false;
	        		vm.landingPageObj['DataSource'] = false;
	        		vm.landingPageObj['MetaData'] = false;
	        		vm.landingPageObj['SharedView'] = false;
	        		vm.landingPageObj['Administration'] = false;

	    		}else{
	        		dataFactory.errorAlert(response.data.message);
	    		}
	    	});

	    	
	    		    	
// check All CheckBoxes	    	
	    	vm.selectAllChkBox = function(key, value){
	    		var chkBoxName = key + '_ALL';
	    		var chkBoxVal = $('.' + chkBoxName).is(':checked');	
	    		
	    		if(chkBoxVal == true){
	    			$('.' + key).prop('checked', true);
	    			value.forEach(function(d){
			    		vm.selectedPage.push(d.id);
		    		});	    		    		
	    			vm.landingPageObj[key] = true;
	    		}else{	    			
	    			vm.landingPageObj[key] = false;
	    			
	    			$('.' + key).prop('checked', false);
	    			value.forEach(function(d){
	    				var value = d.id;
	    				vm.selectedPage = vm.selectedPage.filter(function(item){
	    					return item !== value;
	    				});	    				
	    			});	    				    				    			    	
	    		}		    		
	    	}
	    	
	    	
	    	
	    	
	    	
// Check selected permission
	    	vm.selectedPage=[];
	    	vm.checkObject = function(id, name, value, key){
	    		
	    		
// check view checkBox on selection of other checkBox	
	    		var listChkBoxName = name.split("-")[0] + '-list';
	    		var listId;
	    		value.forEach(function(d){
	    			if(d.name.includes(listChkBoxName)){
	    				listId = d.id;
	    			}
	    			if(d.name == name){
			    		if(!name.includes('list')){
			    			$('.' + listChkBoxName ).prop('checked', true);
			    			vm.selectedPage.push(d.id);
			    			if(listId != undefined){
			    				vm.selectedPage.push(listId);			    				
			    			}
			    		}   			    				
	    			}
	    		});	  
	    		
	    		
// set values on Un-Checking View Check Box	   	    			    		
	    		var chkBoxAll = key + '_ALL';
	    		if(name.includes('list')){
	    			if($('.' + name).is(':checked') == false){
	    				$('.' + key ).prop('checked', false);
	    				$('.' + chkBoxAll ).prop('checked', false);
	    				value.forEach(function(d){
	    					var value = d.id;
		    				vm.selectedPage = vm.selectedPage.filter(function(item){
		    					return item !== value;
		    				});	    				
		    			});
	    			}
	    		}
	    		
	    		
	    		
// set values on Un-Checking Check Box	    			    		
	    		if($('.' + name).is(':checked') == false){
	    			for(var i=0; i<vm.selectedPage.length; i++){
	    				if(vm.selectedPage[i] == id){
	    					var index = vm.selectedPage.indexOf(id);		    			    			
	    		    		if(index == -1){

	    		    		}else{
	    		    			vm.selectedPage.splice(index,1);
	    		    		}
	    				}
	    			}
	    		}	    		
	    		

// Landing Page Selection	    		
	    	
	    		
// All Checked	    		
	    		if($('.' + name).is(':checked') == true){
	    			vm.landingPageObj[key] = true;
	    		}else{
	    			vm.landingPageObj[key] = false;
	    		}
	    		
// Individual Selection of chkBox	    		
	    		if($('.' + key).is(':checked') == true){	    			
	    			vm.landingPageObj[key] = true;
	    		}else{
	    			vm.landingPageObj[key] = false;
	    		}
	    		
	    		
	    		
	    			    		
	    	}
    	
	    	
	    	
	    	
// save
    	vm.save=function(role){
    		var uniqueNames = [];
    		$.each(vm.selectedPage, function(i, el){
    		    if($.inArray(el, uniqueNames) === -1) 
    		    	uniqueNames.push(el);
    		});

    		if(role.name == undefined){
    			dataFactory.errorAlert("Role Name is required");
    			return;
    		}else if(role.description == undefined){
    			dataFactory.errorAlert("Role Description is required");
    			return;
    		}else if(vm.selectedPage.length == 0){
    			dataFactory.errorAlert("Select min one permission");
    			return;
    		}else if(role.landingPage == undefined || role.landingPage == ""){
    			dataFactory.errorAlert("Select landing page");
    			return;
            }
    		
    		var data={
				name:role.name,
				description:role.description,
				landingPage:role.landingPage,
//				pages:vm.selectedPage,
				pages:uniqueNames,
    		};
    		dataFactory.request($rootScope.RoleSave_Url,'post',data).then(function(response){
    			if(response.data.errorCode==1){
    				dataFactory.successAlert("submited successfully");
    				$window.location = '#/setting/roles';
    			}else{
    				dataFactory.errorAlert(response.data.message);
    			}
    		});
    	}
    	
    	
    	
    	
    	
    }]);

