angular.module('app', ['ngSanitize'])
    .controller('userRoleAssignEditController', ['$scope', '$sce', 'dataFactory', '$filter', '$stateParams', '$location', '$window', '$rootScope','$cookieStore',
        function ($scope, $sce, dataFactory, $filter, $stateParams, $location, $window, $rootScope,$cookieStore) {
    	var vm=$scope;
    	var vm=$scope; 
    	var data={};
    	vm.user={};
		var addClass='';
		var viewClass='';
		data['navTitle']="User Role";
		data['icon']="fa fa-user";
		$rootScope.$broadcast('subNavBar', data);
        $scope.id = $stateParams.id;
    	vm.userRole=$rootScope.roleObj[0].roles[0].display_name; 
    	
        
		dataFactory.request($rootScope.Rolelist_Url,'post',"").then(function(response){
			if(response.data.errorCode==1)
				vm.roleList=response.data.result;
					if(vm.userRole=='Admin'){
						var index;
						vm.roleList.forEach(function(d,ind){
							 if(d.name=='Admin')
								index=ind;
						});
                    vm.roleList.splice(index,1);
                }
		}).then(function(){
			dataFactory.request($rootScope.UsergroupList_Url,'post',"").then(function(response){
				if(response.data.errorCode==1)
					vm.UserGroupList=response.data.result;
			}).then(function(){
				var getRoleAssignData = {
		    		'id': $scope.id
		    	}    	  
		    	dataFactory.request($rootScope.editUserRoleAssign_Url, 'post', getRoleAssignData).then(function(response){
		    		if(response.data.errorCode == 1){
		    			vm.grpDataArr = [];    			
		    			var grpData = response.data.result[0].group;    			
		    			vm.user.name = response.data.result[0].name;
		    			vm.user.email = response.data.result[0].email;
		    			
		    			vm.user.role = String(response.data.result[0].roles[0].id);
		    			grpData.forEach(function(d){
		    				vm.grpDataArr.push(String(d.group_id));
		    			});	 		    			
		    			
		    		}else{
		    			dataFactory.errorAlert(response.data.message);
		    		}		    		
		    	}).then(function(){
		    		vm.user.group = vm.grpDataArr;
		    		setTimeout(function(){
		    			$("#userGroupSelect").selectpicker();
		    		},1000);
		    	});
			});
		});




// Save
		vm.save=function(user){
            if(user.name == undefined){
                dataFactory.errorAlert("User Name is required");
                return;
            }else if(user.role == undefined){
                dataFactory.errorAlert("Role is required");
                return;
            }else if(user.group == undefined){
                dataFactory.errorAlert("User Group is required");
                return;
            }

            var passwordRegex = /(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%&*()]).{6,}/;
            if(user.password != undefined && user.password != ''){
            	if( !passwordRegex.test(user.password) ) {
                    dataFactory.errorAlert("Password must contains 1 Upper Case, 1 Lower Case, 1 Special Character & must be 6 characters long");
                    return;
                }
			}

			var data = {
				id:$scope.id,
				name:user.name,
				role:user.role,
				group:user.group, 
				password: user.password,
			}

			dataFactory.request($rootScope.updateUserRoleAssign_Url, 'post', data).then(function(response){
				if(response.data.errorCode==1){
					dataFactory.successAlert(response.data.message);
					$window.location = '#/setting/role_assign';
				}else{
					dataFactory.errorAlert(response.data.message);
				}
			});
			
		}
		
		
		
    }]);