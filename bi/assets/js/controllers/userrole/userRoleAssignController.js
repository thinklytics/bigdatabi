angular.module('app', ['ngSanitize'])
    .controller('userRoleAssignController', ['$scope', '$sce', 'dataFactory', '$filter', '$stateParams', '$location', '$window', '$rootScope','$cookieStore',
        function ($scope, $sce, dataFactory, $filter, $stateParams, $location, $window, $rootScope,$cookieStore) {
    	var vm=$scope;
    	var vm=$scope; 
    	var data={};
		var addClass='';
		var viewClass='';
		data['navTitle']="User Role";
		data['icon']="fa fa-user";
		$rootScope.$broadcast('subNavBar', data);
        var userId=$cookieStore.get('userId');
	vm.userRole=$rootScope.roleObj[0].roles[0].display_name;
        
		dataFactory.request($rootScope.Rolelist_Url,'post',"").then(function(response){
			if(response.data.errorCode==1){
				vm.roleList=response.data.result;
				if(vm.userRole=='Admin'){
                                        var index;
					vm.roleList.forEach(function(d,ind){
                                             if(d.name=='Admin')
						index=ind; 
                                        });
                                        vm.roleList.splice(index,1); 
                                }
                       	}
		}).then(function(){
			dataFactory.request($rootScope.UsergroupList_Url,'post',"").then(function(response){
				if(response.data.errorCode==1)
					vm.UserGroupList=response.data.result;
			}).then(function(){
	    		setTimeout(function(){
	    			$("#userGroupSelect").selectpicker();
	    		},1000);		    	
			});
		});



// save
		vm.save=function(user){

            var passwordRegex = /(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%&*()]).{6,}/;

            if(user.name == undefined){
                dataFactory.errorAlert("User Name is required");
                return;
            }else if(user.email == undefined){
                dataFactory.errorAlert("Email ID is required");
                return;
            }else if(!(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(user.email))){
                dataFactory.errorAlert("Please Enter Correct Email ID");
                return;
            }else if(user.password == undefined) {
                dataFactory.errorAlert("Password is required");
                return;
            }else if( !passwordRegex.test(user.password) ) {
                dataFactory.errorAlert("Password must contains 1 Upper Case, 1 Lower Case, 1 Special Character & must be 6 characters long");
                return;
            }else if(user.role == undefined){
                dataFactory.errorAlert("Role is required");
                return;
            }else if(user.group == undefined){
                dataFactory.errorAlert("User Group is required");
                return;
            }

			dataFactory.request($rootScope.Roleassign_Url,'post',user).then(function(response){
				if(response.data.errorCode==1){
					dataFactory.successAlert("User created successfully");
					$window.location = '#/setting/role_assign';
				}else{
					dataFactory.errorAlert(response.data.message);
				}
			});
		}
		
		
    }]);

