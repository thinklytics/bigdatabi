'use strict';
/* Controllers */
angular.module('app', ['ngSanitize'])
    .controller('emailGroupAddController', ['$scope', '$sce', 'dataFactory', '$filter', '$stateParams', '$location', '$window', '$rootScope','$cookieStore',
        function ($scope, $sce, dataFactory, $filter, $stateParams, $location, $window, $rootScope,$cookieStore) {
            var vm=$scope;
            var data={};
            var addClass='';
            var viewClass='';
            if($location.path()=='/setting/'){
                viewClass='activeli';
            }else{
                addClass='activeli';
            }
            data['navTitle']="EMAIL";
            data['icon']="fa fa-user";
            data['navigation']=[{
                "name":"Add",
                "url":"#/metadata/add",
                "class":addClass
            },
                {
                    "name":"View",
                    "url":"#/metadata/",
                    "class":viewClass
                }];
            $rootScope.$broadcast('subNavBar', data);
            var userId=$cookieStore.get('userId');
            /*
            * Datetime picker
            */
            // Send Email Start
            vm.mail = {};
            vm.mail.date="";
            vm.send_Mail = function(mail){
                var data={
                    "mailObj":mail
                };
                dataFactory.request($rootScope.EmailGrpAdd_Url,'post',data).then(function(response){
                    dataFactory.successAlert(response.data.message);
                    $window.location = '#/setting/email_report';
                });
            }
// Send Email End
        }]);