angular.module('app', ['ngSanitize'])
    .controller('userController', ['$scope', '$sce', 'dataFactory', '$filter', '$stateParams', '$location', '$window', '$rootScope','$cookieStore',
        function ($scope, $sce, dataFactory, $filter, $stateParams, $location, $window, $rootScope,$cookieStore) {
            var vm=$scope;
            var vm=$scope;
            var data={};
            var addClass='';
            var viewClass='';
            data['navTitle']="USER";
            data['icon']="fa fa-user";
            $rootScope.$broadcast('subNavBar', data);
            var userId=$cookieStore.get('userId');
            /*
              Company list
             */
            vm.userList=function () {
                dataFactory.request($rootScope.UserObjList_Url,'post',"").then(function(response){
                    if(response.data.errorCode==1){
                        vm.userList=response.data.result;
                        setTimeout(function () {
                            $("#company").dataTable();
                        },1000);
                    }
                });
            }
            vm.userList();



            $scope.showSwal = function(type, id){
                if(type == 'warning-message-and-cancel') {
                    swal({
                        title: 'Are you sure?',
                        text: 'You want to Delete !',
                        type: 'warning',
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        cancelButtonText: 'No',
                        confirmButtonClass: "btn btn-success",
                        cancelButtonClass: "btn btn-danger",
                        buttonsStyling: false,
                    }).then(function(value){
                            if(value){
                                vm.delete(id);
                            }
                        },
                        function (dismiss) {

                        })
                }
            }

            vm.delete=function (id) {
                var data={
                    "user_id":id
                };
                dataFactory.request($rootScope.userDelete_Url,'post',data).then(function(response){
                    if(response.data.errorCode==1){
                        dataFactory.successAlert(response.data.message);
                        vm.userList();
                    }else{
                        dataFactory.errorAlert("Check your conection");
                    }
                });
            }
        }]);

