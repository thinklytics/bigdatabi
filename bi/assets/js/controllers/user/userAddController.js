angular.module('app', ['ngSanitize'])
    .controller('userAddController', ['$scope', '$sce', 'dataFactory', '$filter', '$stateParams', '$location', '$window', '$rootScope','$cookieStore',
        function ($scope, $sce, dataFactory, $filter, $stateParams, $location, $window, $rootScope,$cookieStore) {
            var vm=$scope;
            var vm=$scope;
            var data={};
            var addClass='';
            var viewClass='';
            data['navTitle']="USER";
            data['icon']="fa fa-user";
            $rootScope.$broadcast('subNavBar', data);
            var userId=$cookieStore.get('userId');
            /*
              Company list
             */
            dataFactory.request($rootScope.CompanyList_Url,'post',"").then(function(response){
                if(response.data.errorCode==1){
                   vm.CompanyList=response.data.result;
                    setTimeout(function(){
                        $("#CompanyList").selectpicker();
                    },1000);
                }
            });
            // save
            vm.save=function(){
                var user={
                    "company":vm.user.company,
                    "email":vm.user.email,
                    "name":vm.user.name,
                    "password":vm.user.password
                };
                var passwordRegex = /(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%&*()]).{6,}/;
                if(user.name == undefined){
                    dataFactory.errorAlert("User Name is required");
                    return;
                }else if(user.email == undefined){
                    dataFactory.errorAlert("Email ID is required");
                    return;
                }else if(!(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(user.email))){
                    dataFactory.errorAlert("Please Enter Correct Email ID");
                    return;
                }else if(user.password == undefined) {
                    dataFactory.errorAlert("Password is required");
                    return;
                }else if( !passwordRegex.test(user.password) ) {
                    dataFactory.errorAlert("Password must contains 1 Upper Case, 1 Lower Case, 1 Special Character & must be 6 characters long");
                    return;
                }
                dataFactory.request($rootScope.userCreate_Url,'post',user).then(function(response){
                    if(response.data.errorCode==1){
                        $window.location = '#/user/list';
                    }else{
                        dataFactory.errorAlert(response.data.message);
                    }
                });
            }


        }]);

