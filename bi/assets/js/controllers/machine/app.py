'''
@sudhir  01/08/2019
'''
import pymongo
import ast
import requests
import pandas as pd
import numpy as np
import json
import pymssql
import os
import os.path
import MySQLdb 
from os.path import join, dirname
from dotenv import load_dotenv
import sqlalchemy
import psycopg2
import cx_Oracle
import flask
from sqlalchemy import types, create_engine
from sklearn import preprocessing
from sklearn import cluster, datasets
from collections import defaultdict
from pandas.io.json import json_normalize
from flask import Flask,request
from flask_cors import CORS
from werkzeug.contrib.cache import SimpleCache
from sklearn import linear_model
from sklearn.linear_model import LogisticRegression
from sklearn.ensemble import RandomForestRegressor
from sklearn.tree import DecisionTreeRegressor
from sklearn.neighbors import kneighbors_graph
from pandas.io import sql
from sqlalchemy_utils import database_exists, create_database
from openpyxl import Workbook
from flask import Flask,render_template, request
from werkzeug import secure_filename
from sklearn.model_selection import GridSearchCV
from sklearn.metrics import accuracy_score
from sklearn.model_selection import train_test_split
from sklearn.externals import joblib

cache = SimpleCache()
app = Flask(__name__)
CORS(app)
UPLOAD_FOLDER = os.getcwd() + "/files/"
api = os.getcwd().split('bi/', 1)[0] + "api/"
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.config['api_path'] = api
fileName = ''

@app.route('/', methods=['POST'])
def result():		
	#parameters from front end
	params = request.form['datacolumn']
	transformation = request.form['objective']	
	sessionid = request.form['sessionId']
	metadataid = request.form['metadataId']
        apiurl = request.form['connectionString']	
        accuracy = ''	
	#reteriving data from cache
	dc = cache.get('datacolumn')
	dv = cache.get('datavalue')
        
	#reteriving data from api
	if dv is None:
	    data={'sessionId':sessionid ,'metadataId': metadataid};
	    r = requests.post( url = apiurl, data = data)
	    d = r.json()
	    column = d['tableColumn']	
	    datacolumn = json.dumps(column)
	    dc = pd.read_json(datacolumn)
	    value = d['tableData']	
	    datavalue = json.dumps(value)
	    dvs = pd.read_json(datavalue)
	    dvs = dvs.replace("null", np.nan).replace("",np.nan)
	    dj = dvs.to_json(orient='records')
	    dv = pd.read_json(dj)
        else:
            dv = dv.replace("null",np.nan)
            dj = dv.to_json(orient='records')
            dv = pd.read_json(dj)

	#declaring dictonary
	dates_dict = defaultdict(list)
	#appending datatype of each column in parallel
        columnName = []
	for var in dc:
                rep = var.replace("(",'_').replace(")",'')
		columnName.append(rep)
		dates_dict[dc[var]['Field']].append(dc[var]['dataType'])
        

	#Data Cleanisning
	for v in dates_dict: 
		if json.dumps(dates_dict[v]) == '["Measure"]' and v in params:
			if "find_and_replace" in transformation:
				#dv[v] = findreplace(dv[v])
				find = request.form['find']
				replace = request.form['rplace']
                                if find.isdigit():
                                    f = int(find)
                                else:
                                    f = find
                                if replace.isdigit():
                                    r = int(replace)
                                else:
    				    r = replace
				dv[v] = dv[v].replace(f,r) 
			if dv[v].isnull().any() == True:
				if "empty_cell_with_fixed_value" in transformation:
					dv[v] = dv[v].fillna(value = 0)
			if dv[v].isnull().any() == True:
				if "empty_cell_with_given_input" in transformation:
					val = request.form['fillvalue']
					va = int(val)
					dv[v] = dv[v].fillna(value = va)
			if "empty_cell_with_fix_mean" in transformation:
				dv[v] = mean(dv[v])
			if "normalize_measure" in transformation:
				dv[v] = normalize(dv[v])
			if "standardize_measure" in transformation:	
				dv[v] = standardize(dv[v])
					
		if json.dumps(dates_dict[v]) == '["Dimension"]' and v in params:
			if "cleanse_control_characters" in transformation:
				dv[v] = cleansingcontrolcharacters(dv[v])
			if dv[v].isnull().any() == True:
				if "empty_cell_with_fixed_value" in transformation:
					dv[v] = dv[v].fillna(value = 'Unknown')
			if "find_and_replace" in transformation:
				dv[v] = findreplace(dv[v])
			if dv[v].isnull().any() == True:
				if "empty_cell_with_given_input" in transformation:
					val = request.form['fillvalue']
					dv[v] = dv[v].fillna(value = val)
	
	#Data Regressing
	if "linear_regression" in transformation:
		data = dv.copy()
		target = request.form['target']
		for v in dates_dict:
			if json.dumps(dates_dict[v]) == '["Dimension"]' and v in params:
				data[v] = data[v].astype('category')
		cat_columns = data.select_dtypes(['category']).columns
		data[cat_columns] = data[cat_columns].apply(lambda x: x.cat.codes)
		train = data[pd.notnull(data[target])]
		test = data[pd.isnull(data[target])]
		model = linear_model.LinearRegression()
		model.fit(train[ast.literal_eval(params)],train[target])
		result = model.predict(test[ast.literal_eval(params)].values.tolist())
		model_acc = model.predict(train[ast.literal_eval(params)])
                accuracy = accuracy_score(model_acc, train[target])
		for r in result:
			#fill nul with the result limited by 1 on record 
			dv[target].fillna(r, limit = 1, inplace = True)
	
	if "LinReg2" in transformation:
		datacols = request.form['datacolumn']
		target = request.form['target']
		datacol = ast.literal_eval(datacols)
		split_on = request.form['split_on']
		train_split = request.form['train_split']
		test_split = request.form['test_split']
		target = request.form['target']	
		if any(char.isdigit() for char in train_split):
			train_split =int(train_split)
		if any(char.isdigit() for char in test_split):
			test_split = int(test_split)
		test_bef = dv.loc[dv[split_on] == test_split]
		for v in dates_dict: 
			if json.dumps(dates_dict[v]) == '["Dimension"]' and v in datacol:	
				dv[v] = dv[v].astype('category')
		cat_columns = dv.select_dtypes(['category']).columns
		dv[cat_columns] = dv[cat_columns].apply(lambda x: x.cat.codes)
		train = dv.loc[dv[split_on] == train_split]
		test = dv.loc[dv[split_on] == test_split]
		model = linear_model.LinearRegression()
		model.fit(train[datacol],train[target])
		result = model.predict(test[datacol].values.tolist())
		model_acc = model.predict(train[datacol])
                accuracy = accuracy_score(model_acc, train[target])
		columnName.append("predicts")
		test_bef['predicts'] = result
		dv = test_bef
	
	if "LogReg2" in transformation:
		datacols = request.form['datacolumn']
		datacol = ast.literal_eval(datacols)
		target = request.form['target']
		split_on = request.form['split_on']
		train_split = request.form['train_split']
		test_split = request.form['test_split']
		target = request.form['target']
		if any(char.isdigit() for char in train_split):
			train_split =int(train_split)
		if any(char.isdigit() for char in test_split):
			test_split = int(test_split)
		test_bef = dv.loc[dv[split_on] == test_split]
		for v in dates_dict: 
			if json.dumps(dates_dict[v]) == '["Dimension"]' and v in datacol:	
				dv[v] = dv[v].astype('category')
		cat_columns = dv.select_dtypes(['category']).columns
		dv[cat_columns] = dv[cat_columns].apply(lambda x: x.cat.codes)		
		train = dv.loc[dv[split_on] == train_split]
		test = dv.loc[dv[split_on] == test_split]		
		model = LogisticRegression(random_state=0)
		model.fit(train[datacol],train[target])
		result = model.predict(test[datacol].values.tolist())
		model_acc = model.predict(train[datacol])
                accuracy = accuracy_score(model_acc, train[target])
		columnName.append("predicts")
		test_bef['predicts'] = result
		dv = test_bef

	if "logisticRegression" in transformation:		
		data = dv.copy()
		target = request.form['target']
		for v in dates_dict:
			if json.dumps(dates_dict[v]) == '["Dimension"]' and v in params:
				data[v] = data[v].astype('category')
		cat_columns = data.select_dtypes(['category']).columns
		data[cat_columns] = data[cat_columns].apply(lambda x: x.cat.codes)
		train = data[pd.notnull(data[target])]
		test = data[pd.isnull(data[target])]
                model = LogisticRegression(random_state=0) 
                model.fit(train[ast.literal_eval(params)],train[target])
		result = model.predict(test[ast.literal_eval(params)].values.tolist())                
                model_acc = model.predict(train[ast.literal_eval(params)])
                accuracy = accuracy_score(model_acc, train[target])
		for r in result:
			dv[target].fillna(r, limit = 1, inplace = True)
				
	if "DecisionTreeRegressor" in transformation:
		data = dv.copy()
		target = request.form['target']
		for v in dates_dict:
			if json.dumps(dates_dict[v]) == '["Dimension"]' and v in params:
				data[v] = data[v].astype('category')
		cat_columns = data.select_dtypes(['category']).columns
		data[cat_columns] = data[cat_columns].apply(lambda x: x.cat.codes)
		train = data[pd.notnull(data[target])]
		test = data[pd.isnull(data[target])]
		model = DecisionTreeRegressor(random_state = 0)
		model.fit(train[ast.literal_eval(params)],train[target])
		y_pred = model.predict(test[ast.literal_eval(params)].values.tolist())
		model_acc = model.predict(train[ast.literal_eval(params)])
                accuracy = accuracy_score(model_acc, train[target])
		for r in y_pred:
			dv[target].fillna(r, limit = 1, inplace = True)

	if "DecReg2" in transformation:
		datacols = request.form['datacolumn']
		target = request.form['target']
		datacol = ast.literal_eval(datacols)
		split_on = request.form['split_on']
		train_split = request.form['train_split']
		test_split = request.form['test_split']
		target = request.form['target']
		if any(char.isdigit() for char in train_split):
			train_split =int(train_split)
		if any(char.isdigit() for char in test_split):
			test_split = int(test_split)
		test_bef = dv.loc[dv[split_on] == test_split]
		for v in dates_dict: 
			if json.dumps(dates_dict[v]) == '["Dimension"]' and v in datacol:	
				dv[v] = dv[v].astype('category')
		cat_columns = dv.select_dtypes(['category']).columns
		dv[cat_columns] = dv[cat_columns].apply(lambda x: x.cat.codes)		
		train = dv.loc[dv[split_on] == train_split]
		test = dv.loc[dv[split_on] == test_split]
		model = DecisionTreeRegressor(random_state = 0)
		model.fit(train[datacol],train[target])
		result = model.predict(test[datacol].values.tolist())
		model_acc = model.predict(train[datacol])
                accuracy = accuracy_score(model_acc, train[target])
		columnName.append("predicts")
		test_bef['Predicted'] = result
		dv = test_bef
	
	if "RandomForestRegressor" in transformation:
		data = dv.copy()
		target = request.form['target']
		for v in dates_dict:
			if json.dumps(dates_dict[v]) == '["Dimension"]' and v in params:
				data[v] = data[v].astype('category')
		cat_columns = data.select_dtypes(['category']).columns
		data[cat_columns] = data[cat_columns].apply(lambda x: x.cat.codes)
		train = data[pd.notnull(data[target])]
		test = data[pd.isnull(data[target])]
		model = RandomForestRegressor(n_estimators=20, random_state=0)
		model.fit(train[ast.literal_eval(params)],train[target])
		y_pred = model.predict(test[ast.literal_eval(params)].values.tolist())
		model_acc = model.predict(train[ast.literal_eval(params)])
                accuracy = accuracy_score(model_acc, train[target])
		for r in y_pred:
			dv[target].fillna(r, limit = 1, inplace = True)
	
	if "RanForReg2" in transformation:
		datacols = request.form['datacolumn']
		target = request.form['target']
		datacol = ast.literal_eval(datacols)
		split_on = request.form['split_on']
		train_split = request.form['train_split']
		test_split = request.form['test_split']
		target = request.form['target']
		if any(char.isdigit() for char in train_split):
			train_split =int(train_split)
		if any(char.isdigit() for char in test_split):
			test_split = int(test_split)
		test_bef = dv.loc[dv[split_on] == test_split]
		for v in dates_dict: 
			if json.dumps(dates_dict[v]) == '["Dimension"]' and v in datacol:	
				dv[v] = dv[v].astype('category')
		cat_columns = dv.select_dtypes(['category']).columns
		dv[cat_columns] = dv[cat_columns].apply(lambda x: x.cat.codes)		
		train = dv.loc[dv[split_on] == train_split]
		test = dv.loc[dv[split_on] == test_split]
		model = RandomForestRegressor(n_estimators=20, random_state=0)  
		model.fit(train[datacol],train[target])
		result = model.predict(test[datacol].values.tolist())
		model_acc = model.predict(train[datacol])
                accuracy = accuracy_score(model_acc, train[target])
		columnName.append("predicts")
		test_bef['Predicted'] = result
		dv = test_bef
	
	#Data Classification
	if "GaussianNB" in transformation:
		data = dv.copy()
		target = request.form['target']
		for v in dates_dict:
			if json.dumps(dates_dict[v]) == '["Dimension"]' and v in params:
				data[v] = data[v].astype('category')
		cat_columns = data.select_dtypes(['category']).columns
		data[cat_columns] = data[cat_columns].apply(lambda x: x.cat.codes)
		train = data[pd.notnull(data[target])]
		test = data[pd.isnull(data[target])]
		from sklearn.naive_bayes import GaussianNB 
		model = GaussianNB()
		model.fit(train[ast.literal_eval(params)], train[target]) 
		gnb_predictions = model.predict(test[ast.literal_eval(params)].values.tolist())
		model_acc = model.predict(train[ast.literal_eval(params)])
                accuracy = accuracy_score(model_acc, train[target])
		for r in gnb_predictions:
			dv[target].fillna(r, limit = 1, inplace = True)
	
	if "KNeighborsClassifier" in transformation:
		data = dv.copy()
		target = request.form['target']
		for v in dates_dict:
			if json.dumps(dates_dict[v]) == '["Dimension"]' and v in params:
				data[v] = data[v].astype('category')
		cat_columns = data.select_dtypes(['category']).columns
		data[cat_columns] = data[cat_columns].apply(lambda x: x.cat.codes)
		train = data[pd.notnull(data[target])]
		test = data[pd.isnull(data[target])]
		from sklearn.neighbors import KNeighborsClassifier 
		model = KNeighborsClassifier(n_neighbors = 7)
		model.fit(train[ast.literal_eval(params)], train[target]) 
		result = model.predict(test[ast.literal_eval(params)].values.tolist()) 
		model_acc = model.predict(train[ast.literal_eval(params)])
                accuracy = accuracy_score(model_acc, train[target])
		for r in result:
			dv[target].fillna(r, limit = 1, inplace = True)
	
	if "SVC" in transformation:
		data = dv.copy()
		target = request.form['target']
		for v in dates_dict:
			if json.dumps(dates_dict[v]) == '["Dimension"]' and v in params:
				data[v] = data[v].astype('category')
		cat_columns = data.select_dtypes(['category']).columns
		data[cat_columns] = data[cat_columns].apply(lambda x: x.cat.codes)
		train = data[pd.notnull(data[target])]
		test = data[pd.isnull(data[target])]
		from sklearn.svm import SVC 
		model = SVC(kernel = 'linear', C = 1)
		model.fit(train[ast.literal_eval(params)], train[target]) 
		result = model.predict(test[ast.literal_eval(params)].values.tolist()) 
		model_acc = model.predict(train[ast.literal_eval(params)])
                accuracy = accuracy_score(model_acc, train[target])
		for r in result:
			dv[target].fillna(r, limit = 1, inplace = True)
			
	if "DecisionTreeClassifier" in transformation:
		data = dv.copy()
		target = request.form['target']
		for v in dates_dict:
			if json.dumps(dates_dict[v]) == '["Dimension"]' and v in params:
				data[v] = data[v].astype('category')
		cat_columns = data.select_dtypes(['category']).columns
		data[cat_columns] = data[cat_columns].apply(lambda x: x.cat.codes)
		train = data[pd.notnull(data[target])]
		test = data[pd.isnull(data[target])]
		from sklearn.tree import DecisionTreeClassifier 
		model = DecisionTreeClassifier(max_depth = 2)
		model.fit(train[ast.literal_eval(params)], train[target])
		result = model.predict(test[ast.literal_eval(params)].values.tolist())  
		model_acc = model.predict(train[ast.literal_eval(params)])
                accuracy = accuracy_score(model_acc, train[target])
		for r in result:
			dv[target].fillna(r, limit = 1, inplace = True)
	
	#data Clustering
	if "Kmeans" in transformation:
		data = dv.copy()
		for v in dates_dict:
			if json.dumps(dates_dict[v]) == '["Dimension"]' and v in params:
				data[v] = data[v].astype('category')
		cat_columns = data.select_dtypes(['category']).columns
		data[cat_columns] = data[cat_columns].apply(lambda x: x.cat.codes)
		kvalue = request.form['kcluster']
		k = int(kvalue)
		from sklearn.cluster import KMeans
		km = KMeans(n_clusters = k, init = 'k-means++', random_state = 42)
		columnName.append("cluster")
		dv['cluster'] = km.fit_predict(data[ast.literal_eval(params)].values.tolist())
	
	if "Spectral" in transformation:
		data = dv.copy()
		for v in dates_dict:
			if json.dumps(dates_dict[v]) == '["Dimension"]' and v in params:
				data[v] = data[v].astype('category')
		cat_columns = data.select_dtypes(['category']).columns
		data[cat_columns] = data[cat_columns].apply(lambda x: x.cat.codes)
		kvalue = request.form['kcluster']
		k = int(kvalue)
		spectral = cluster.SpectralClustering(n_clusters=k,eigen_solver='arpack',affinity="nearest_neighbors")
		columnName.append("cluster")
		dv['cluster'] = spectral.fit_predict(data[ast.literal_eval(params)].values.tolist())
	
	if "Hierarchical" in transformation:
		data = dv.copy()
		for v in dates_dict:
			if json.dumps(dates_dict[v]) == '["Dimension"]' and v in params:
				data[v] = data[v].astype('category')
		cat_columns = data.select_dtypes(['category']).columns
		data[cat_columns] = data[cat_columns].apply(lambda x: x.cat.codes)
		
		X = data[ast.literal_eval(params)]
		bandwidth = cluster.estimate_bandwidth(X, quantile=0.3)
		connectivity = kneighbors_graph(X, n_neighbors=10, include_self=False)
		connectivity = 0.5 * (connectivity + connectivity.T)
		
		kvalue = request.form['kcluster']
		link = request.form['linkage']
		k = int(kvalue)
		ward = cluster.AgglomerativeClustering(n_clusters=k, linkage=link,connectivity=connectivity)
		columnName.append("cluster")
		dv['cluster'] = ward.fit_predict(data[ast.literal_eval(params)].values.tolist())
	
	if "Brich" in transformation:
		data = dv.copy()
		for v in dates_dict:
			if json.dumps(dates_dict[v]) == '["Dimension"]' and v in params:
				data[v] = data[v].astype('category')
		cat_columns = data.select_dtypes(['category']).columns
		data[cat_columns] = data[cat_columns].apply(lambda x: x.cat.codes)
		kvalue = request.form['kcluster']
		k = int(kvalue)
		birch = cluster.Birch(n_clusters=k)
		columnName.append("cluster")
		dv['cluster'] = birch.fit_predict(data[ast.literal_eval(params)].values.tolist())
		
	#caching the results
        dvs = dv.head(100)
	dj = dvs.to_json(orient='records')        
	isLast = request.form['status']
	cache.set('datacolumn',dc, timeout=5 * 60)
	cache.set('datavalue',dv, timeout=5 * 60)
	dbType = request.form['dbType']
	#pushing results to mongodb
	if isLast == '1':
		#dbType = request.form['dbType']  
		if dbType == 'mongodb':
			host = request.form['mongodb_host']	
			port = request.form['mongodb_port']
			username = request.form['mongodb_username']
			password = request.form['mongodb_password']
			collection = request.form['mongodb_collection']
			database = request.form['mongodb_databasename']
			#mangoDB connect
			p = int(port)
			c = pymongo.MongoClient(host,p)
			c['admin'].authenticate( username, password)
			mydb = c[database]
                        if collection in mydb.collection_names():
                            coll = mydb[collection]
                            coll.drop()
			mycol = mydb[collection]
			mycol.insert_many(dv.to_dict(orient='records'))
			cache.clear()
			
		if dbType == 'mysql':
			host = request.form['mongodb_host']	
			port = request.form['mongodb_port']
			username = request.form['mongodb_username']
			password = request.form['mongodb_password']
			collection = request.form['mongodb_collection']
			database = request.form['mongodb_databasename']
                        engine = create_engine('mysql://{0}:{1}@{2}:{3}/{4}'.format(username,password,host,port,database))
			if not database_exists(engine.url):
				create_database(engine.url)
			con = engine.connect()
			dv.to_sql(con=con, name=collection, if_exists='replace')
			con.close()
                        cache.clear()

		if dbType == 'postgres':
			host = request.form['mongodb_host']	
			port = request.form['mongodb_port']
			username = request.form['mongodb_username']
			password = request.form['mongodb_password']
			collection = request.form['mongodb_collection']
			database = request.form['mongodb_databasename']
			engine = create_engine('postgresql://{0}:{1}@{2}:{3}/{4}'.format(username,password,host,port,database))                        
			if not database_exists(engine.url):
				create_database(engine.url)
			con = engine.connect()
			dv.to_sql(con=con, name=collection, if_exists='replace')
			con.close()
                        cache.clear()

		if dbType == 'oracle':
			host = request.form['mongodb_host']	
			port = request.form['mongodb_port']
			username = request.form['mongodb_username']
			password = request.form['mongodb_password']
			collection = request.form['mongodb_collection']
			database = request.form['mongodb_databasename']
			conn = create_engine('oracle+cx_oracle://{0}:{1}@{2}:{3}/?service_name={4}'.format(username,password,host,port,database))
			dv.to_sql(collection, conn, if_exists='replace')	       
			con.close()
                        cache.clear()
			
		if dbType == 'mssql':
                        inst = pd.DataFrame(data =dv )
			inst.columns = columnName
			host = request.form['mongodb_host']	
			port = request.form['mongodb_port']
			username = request.form['mongodb_username']
			password = request.form['mongodb_password']
			collection = request.form['mongodb_collection']
			database = request.form['mongodb_databasename']	
			engine = create_engine('mssql+pymssql://{0}:{1}@{2}:{3}/{4}'.format(username,password,host,port,database))
			conn = pymssql.connect(host +':'+port,username,password)
			if not database_exists(engine.url):
				conn.autocommit(True)
				cursor = conn.cursor()
				cursor.execute("CREATE DATABASE "+database)
				conn.autocommit(False)
			con = engine.connect()
			dv.to_sql(collection, con, if_exists='replace')
			con.close()
                        cache.clear()
        res = [{"data": dj , "accuracy": accuracy}]
        return json.dumps(res)
        #return dj

#data cleansing methods

@app.route('/upload')
def upload():
	return render_template('datasource-add.html')

@app.route('/uploader', methods = ['GET', 'POST'])
def uploader():
	file = request.files['image']
	name = request.form['fname']
	name = name.replace(" ", "_").replace("-","_")
	json_string = '{"Tables":[]}'
	json_dict = json.loads(json_string)
	f = os.path.join(app.config['UPLOAD_FOLDER'], file.filename)
	fileName = file.filename
	file.save(f)
	extension = os.path.splitext(fileName)[1]
	if extension == '.xlsx' or extension == '.xls':
		xls = pd.ExcelFile(UPLOAD_FOLDER + file.filename)
		for sheets in xls.sheet_names:
			df = pd.read_excel(UPLOAD_FOLDER + file.filename, sheet_name=sheets)
			df = df.applymap(lambda x: np.nan if isinstance(x, basestring) and x.isspace() else x).dropna(how='all').dropna(how='all',axis='columns')
			df = df.apply(lambda x: x.replace('?',np.nan).replace('#',np.nan))
			df.columns = df.columns.str.replace(' ', '_')
			dv = _pushtodf(df,name+'_'+sheets)
			json_dict['Tables'].append(dv)
			json_string = json.dumps(json_dict)
	elif extension == '.csv':
		df = pd.read_csv(UPLOAD_FOLDER + file.filename)
		df = df.applymap(lambda x: np.nan if isinstance(x, basestring) and x.isspace() else x).dropna(how='all').dropna(how='all',axis='columns')
		df = df.apply(lambda x: x.replace('?',np.nan).replace('#',np.nan))
		df.columns = df.columns.str.replace(' ', '_')
		dv = _pushtodf(df,name)
		json_dict['Tables'].append(dv)
		json_string = json.dumps(json_dict)
	else:
		return json.dumps({"result": 'file format exception'})  
	return json_string

@app.route('/append', methods = ['GET', 'POST'])
def append():
	dotenv_path = os.path.join(app.config['api_path'], '.env')
	file = request.files['image']
	keys = request.form['ids']
	tablename = request.form['tablename']
	load_dotenv(dotenv_path)
	host = os.getenv('EXCEL_HOST')
	port = os.getenv('EXCEL_PORT')
	user = os.getenv('EXCEL_USERNAME')
	passwd = os.getenv('EXCEL_PASSWORD')
	db = os.getenv('EXCEL_DB')
	mylist = keys.split(",")
	keys_len = len(mylist)
	f = os.path.join(app.config['UPLOAD_FOLDER'], file.filename)
	fileName = file.filename
	file.save(f)
	extension = os.path.splitext(fileName)[1]
	if extension == '.xlsx' or extension == '.xls':
		df = pd.read_excel(UPLOAD_FOLDER + file.filename)
		df = df.applymap(lambda x: np.nan if isinstance(x, basestring) and x.isspace() else x).dropna(how='all').dropna(how='all',axis='columns')
		df = df.apply(lambda x: x.replace('?',np.nan).replace('#',np.nan))
	elif extension == '.csv':
		df = pd.read_csv(UPLOAD_FOLDER + file.filename)
		df = df.applymap(lambda x: np.nan if isinstance(x, basestring) and x.isspace() else x).dropna(how='all').dropna(how='all',axis='columns')
		df = df.apply(lambda x: x.replace('?',np.nan).replace('#',np.nan))
	con = MySQLdb.connect(host=host,user=user,passwd=passwd,db=db,port=int(port))
	con_str = 'mysql+mysqldb://{0}:{1}@{2}:{3}/{4}'.format(user,passwd,host,port,db)
	engine = sqlalchemy.create_engine(con_str)    
	j = 0   
	while j < len(df):
		i = 0
		delete_str = 'DELETE FROM `'+ tablename +'` WHERE '
		while i < keys_len:
			if i == 0:
				delete_str = delete_str + mylist[i] +' = ' + `df[''+ mylist[i] +''][+j]`
			else:
				delete_str = delete_str + ' and ' + mylist[i] +' = ' + `df[''+ mylist[i] +''][+j]`
			i += 1
		res = _del(delete_str,con)
		j += 1
	if res == 'okay':
		df.to_sql(tablename, if_exists='append', con=engine, index=False)
		return json.dumps({"result": 'Excel Data Appended'})
	return json.dumps({"result": 'Something went wrong..'})

def _del(delete_str,con):
	cur = con.cursor()
	cur.execute(delete_str)
	con.commit()
	return 'okay'
	
def mean(dv):
	dv = dv.fillna(dv.mean())
	return dv

def normalize(dv):
	dv = dv.fillna(0)
	x = dv.values 
	x = x.reshape(-1, 1)					
	min_max_scaler = preprocessing.MinMaxScaler()
	x_scaled = min_max_scaler.fit_transform(x)	
	dv = pd.DataFrame(x_scaled)
	return dv

def standardize(dv):
	dv = dv.fillna(0)
	x = dv.values 
	x = x.reshape(-1, 1)						
	standardized = preprocessing.scale(x)	
	dv = pd.DataFrame(standardized)
	return dv

def cleansingcontrolcharacters(dv):
	dv = dv.apply(lambda x: x.replace('\n',' ').replace('\r','').replace('\t',''))
	dv = dv.str.replace('\s+', ' ', regex=True)
	return dv

def findreplace(dv):
	find = request.form['find']
	replace = request.form['rplace']
	dv = dv.replace(find, replace) 
	return dv

	
#excel methods
def _pushtodf(df,name):
	datatype = df.dtypes
	_objects = datatype[(datatype == 'object')].index.tolist()
	cols = {}
	for col in _objects:
		measurer = np.vectorize(len)
		res = measurer(df[col].values.astype(unicode)).max(axis=0)
		if res < 1000:
			cols[col] = res
	dv = df.to_json(orient = 'records')
	is_inserted = _pushtosql(df,name,cols)
	return is_inserted

def _pushtosql(df,names,cols):
	dotenv_path = os.path.join(app.config['api_path'], '.env')
	load_dotenv(dotenv_path)
	host = os.getenv('EXCEL_HOST')
	port = os.getenv('EXCEL_PORT')
	user = os.getenv('EXCEL_USERNAME')
	passwd = os.getenv('EXCEL_PASSWORD')
	db = os.getenv('EXCEL_DB')
	engine = create_engine('mysql://{0}:{1}@{2}:{3}/{4}'.format(user,passwd,host,port,db))
	if not database_exists(engine.url):
		create_database(engine.url)
	con = engine.connect()
        try:
            df.columns = df.columns.str.replace(".", "_")
	    df.to_sql(con=con, name=names,dtype={k: sqlalchemy.types.NVARCHAR(length= v + 50) for k,v in cols.iteritems()},index=False)
	    con.close()
	    return names
        except:
            return "Datasource already exists"
	
#flask indicator
if __name__ == "__main__":
    app.run(host='0.0.0.0',debug=True)
    #app.run(debug=True)

