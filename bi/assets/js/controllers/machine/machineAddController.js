'use strict';
angular	.module('app', ['ngSanitize']).controller('machineAddController',['$scope','$sce','dataFactory','$rootScope','$filter','$window','$stateParams','$cookieStore','$location',
    function($scope, $sce, dataFactory, $rootScope, $filter, $window,$stateParams,$cookieStore,$location) {
        var data={};
        var addClass='';
        var viewClass='';
        var vm = $scope;
        if($location.path()=='/machine/'){
            viewClass='activeli';
        }else{
            addClass='activeli';
        }
        data['navTitle']="Machine Learning";
        data['icon']="fa fa-book";
        data['navigation']=[
            {
                "name":"Add",
                "url":"#/machine/add",
                "class":addClass
            },
            {
                "name":"View",
                "url":"#/machine/",
                "class":viewClass
            }
        ];

        $rootScope.$broadcast('subNavBar', data);
        var userId = $cookieStore.get('userId');

        vm.showMeasureDimension = false;
        vm.Attributes = {};
        vm.childAttributes = {};
        vm.machineModelName = '';

        vm.operation = [
            {
                "name": "Data Cleansing",
                "id": 1,
                "key": "cleanData",
                "child":[
                    {
                        "name": "Fill Empty Cells With Fixed Value",
                        "id": 1,
                        "key": "empty_cell_with_fixed_value",
                        "value": "fa fa-pencil-square-o",
                        "icon": "\uf044",
                    },
                    {
                        "name": "Fill Empty Cells With Given Input",
                        "id": 2,
                        "key": "empty_cell_with_given_input",
                        "value": "fa fa-sign-in",
                        "icon": "\uf090",
                    },
                    {
                        "name": "Fill Empty Cells With Mean",
                        "id": 3,
                        "key": "empty_cell_with_fix_mean",
                        "value": "fa fa-pencil-square-o",
                        "icon": "\uf044",
                    },
                    {
                        "name": "Normalize Measure",
                        "id": 4,
                        "key": "normalize_measure",
                        "value": "fa fa-pencil-square-o",
                        "icon": "\uf044",
                    },
                    {
                        "name": "Standardize Measure",
                        "id": 5,
                        "key": "standardize_measure",
                        "value": "fa fa-pencil-square-o",
                        "icon": "\uf044",
                    },
                    {
                        "name": "Find and Replace",
                        "id": 6,
                        "key": "find_and_replace",
                        "value": "fa fa-search",
                        "icon": "\uf002",
                    },
                    {
                        "name": "Cleanse Control Characters",
                        "id": 7,
                        "key": "cleanse_control_characters",
                        "value": "fa fa-wrench",
                        "icon": "\uf0ad",
                    }
                ]
            },
            {
                "name": "Data Regression",
                "id": 2,
                "key": "regressionData",
                "child":[
                    {
                        "name": "Linear Regression",
                        "id": 1,
                        "key": "linear_regression",
                        "value": "fa fa-plus",
                        "icon": "\uf067",
                    },
                    {
                        "name": "Logistic Regression",
                        "id": 2,
                        "key": "logisticRegression",
                        "value": "fa fa-plus",
                        "icon": "\uf067",
                    },
                    {
                        "name": "Decision Tree Regressor",
                        "id": 3,
                        "key": "DecisionTreeRegressor",
                        "value": "fa fa-tree",
                        "icon": "\uf1bb",
                    },
                    {
                        "name": "Random Forest Regressor",
                        "id": 4,
                        "key": "RandomForestRegressor",
                        "value": "fa fa-random",
                        "icon": "\uf074",
                    },
                ]
            },
            {
                "name": "Data Classification",
                "id": 3,
                "key": "classificationData",
                "child":[
                    {
                        "name": "Gaussian NB",
                        "id": 1,
                        "key": "GaussianNB",
                        "value": "fa fa-circle-thin",
                        "icon": "\uf1db",
                    },
                    {
                        "name": "K Neighbors Classifier",
                        "id": 2,
                        "key": "KNeighborsClassifier",
                        "value": "fa fa-spinner",
                        "icon": "\uf110",
                    },
                    {
                        "name": "Support Vector Machine",
                        "id": 3,
                        "key": "SVC",
                        "value": "fa fa-life-ring",
                        "icon": "\uf1cd",
                    },
                    {
                        "name": "Decision Tree Classifier",
                        "id": 4,
                        "key": "DecisionTreeClassifier",
                        "value": "fa fa-tree",
                        "icon": "\uf1bb",
                    },
                ]
            },
            {
                "name": "Data Clustering",
                "id": 4,
                "key": "clusterData",
                "child":[
                    {
                        "name": "K Means Clustering",
                        "id": 1,
                        "key": "Kmeans",
                        "value": "fa fa-plus",
                        "icon": "\uf067",
                    },
                    {
                        "name": "Spectral Clustering",
                        "id": 2,
                        "key": "Spectral",
                        "value": "fa fa-spinner",
                        "icon": "\uf110",
                    },
                    {
                        "name": "Hierarchical Clustering",
                        "id": 3,
                        "key": "Hierarchical",
                        "value": "fa fa-anchor",
                        "icon": "\uf13d",
                    },
                    {
                        "name": "Birch Clustering",
                        "id": 4,
                        "key": "Brich",
                        "value": "fa fa-sun-o",
                        "icon": "\uf185",
                    },
                ]
            }
        ];

        var userPort = $cookieStore.get('userPort');



        vm.childOperation = [];
        vm.operation.forEach(function (data,index) {
            data.child.forEach(function(d,i){
                vm.childOperation.push(d);
            });
        });
        $(".commonSelect").selectpicker();
        $(".commonSelect_Cluster").selectpicker();





        vm.op_MetaData = false;
        vm.op_ProcessData = false;
        vm.op_Operation = false;
        vm.op_DBOutput = false;
        vm.op_Delete = true;

        vm.Load_Process_Res = true;
        vm.Load_DB_Res = true;



        vm.MachineParameter = {};
        vm.MachineParameter.Cleansing = {};
        vm.MachineParameter.Regression = {};
        vm.MachineParameter.Classification = {};
        vm.MachineParameter.Clustering = {};
        vm.MachineParameter.DBOutput = {};
        vm.operationType = '';
        vm.dropCounter = 0;

// Draggable MetaData Start
        vm.DroppableZoneOption = true;

        setTimeout(function (){
            $('.draggableArea').draggable({
                cancel : "a.ui-icon",
                revert : true,
                helper : "clone",
                cursor : "move",
                revertDuration : 0
            });

            $('.droppableArea').droppable({
                accept : ".draggableArea",
                activeClass : "ui-state-highlight",
                drop : function(event, ui) {
                    var div = $(ui.draggable)[0];
                    var LI_title = $(div).attr('title');

                    vm.DroppableZoneOption = false;
                    vm.operation_Parent = '';

                    var imgObj = {};
                    if(LI_title == 'MetaData'){
                        imgObj = {
                            "icon" : "\uf1c0",
                            "color" : "yellow",
                            'text' : LI_title,
                            'key' : 'Drop_MetaData',
                        }
                    }else if(LI_title == 'Process Data'){
                        imgObj = {
                            "icon" : "\uf013",
                            "color" : "green",
                            'text' : LI_title,
                            'key' : 'Process_Data',
                        }
                    }else if(LI_title == 'DB Output'){
                        imgObj = {
                            "icon" : "\uf08b",
                            "color" : "red",
                            'text' : LI_title,
                            'key' : 'DB_Output',
                        }
                    }else{
                        vm.childOperation.forEach(function (d) {
                            if(LI_title == d.name){
                                imgObj = {
                                    "icon" : d.icon,
                                    "color" : "#2196F3",
                                    'text' : LI_title,
                                    'key' : d.key,
                                }
                            }
                        });
                    }

                    if(LI_title == 'MetaData'){
                        if(vm.MDCounter == 0){
                            vm.opr_MetaData();
                        }
                    }else if(LI_title == 'DB Output'){
                        vm.opr_DBOutput();
                    }else if(LI_title != 'Process Data' && LI_title != 'DB Output'){
                        vm.operationType = LI_title;
                        vm.operation.forEach(function(data){
                            data.child.forEach(function(d){
                                if(LI_title == d.name){
                                    vm.operation_Parent = data.name;
                                    $scope.$apply();
                                }
                            });
                        });
                    }
                    if(LI_title == 'DB Output'){
                        $( ".droppableArea" ).droppable("disable");
                    }
                    vm.onDropMachineOperation(LI_title, imgObj, vm.dropCounter);
                    vm.dropCounter++;
                }
            });
        }, 1500);

// Draggable MetaData End





//  Drop Machine Operation Start
        vm.stage = new Konva.Stage({
            container : 'canvasContainer',
            width : canvas.canvasWidth,
            height : canvas.canvasHeight,
        });
        vm.layer = new Konva.Layer();
        vm.group;
        vm.iCounter = 0;
        vm.root;
        vm.rect;
        vm.arrow;
        vm.drawComponent = {};
        vm.selected_TitleID_Attr_Name = [];
        vm.selected_MainTitle_Attr_Name = [];
        vm.processCounter = 0;
        vm.metadataCounter = 0;
        vm.MDCounter = 0;
        vm.dbCounter = 0;
        vm.opCounter = 0;

        vm.turnBottom;
        vm.turnLeft;
        vm.turnRight;

        vm.onDropMachineOperation = function(title, ImgObj, dropCounter){
            var titleAttr = title;
            if(title == 'MetaData'){
                vm.metadataCounter++;
                titleAttr = ImgObj.key + '_' + vm.metadataCounter;
            }else if(title == 'Process Data'){
                vm.processCounter++;
                titleAttr = ImgObj.key + '_' + vm.processCounter;
            }else if(title == 'DB Output'){
                vm.dbCounter++;
                titleAttr = ImgObj.key + '_' + vm.dbCounter;
            }else{
                vm.childOperation.forEach(function(d){
                    if(title == d.name){
                        vm.opCounter++;
                        titleAttr = ImgObj.key + '_' + vm.opCounter;
                    }
                });
            }

            vm.group = new Konva.Group({
                "id" : titleAttr,
            });

            if(!vm.drawComponent[titleAttr]){
                vm.drawComponent[titleAttr] = {};
            }

            if(vm.MDCounter == 0 || title != 'MetaData'){
                vm.selected_MainTitle_Attr_Name.push(title);
                vm.selected_TitleID_Attr_Name.push(titleAttr);

                var modLength = vm.selected_TitleID_Attr_Name.length - 1;
                if(vm.iCounter == 0 || vm.stage.children[0] && vm.stage.children[0].children.length == 0){
                    vm.root = canvas.getRootRect(titleAttr, title, dropCounter);
                    vm.rect = vm.root;
                }else{
                    vm.rect = vm.root.attrs.addChild(titleAttr, title, modLength);
                    vm.rootNew = vm.rect;
                    vm.arrow = canvas.drawArrow(vm.root, vm.rect, titleAttr);
                    vm.drawComponent[titleAttr]["arrow"] = vm.arrow;
                    vm.group.add(vm.arrow);
                    if(modLength%6 == 0){
                        vm.turnBottom = canvas.drawTurnLine_Bottom(vm.root, vm.rect, titleAttr);
                        vm.drawComponent[titleAttr]["turnBottom"] = vm.turnBottom;
                        vm.group.add(vm.turnBottom);

                        vm.turnLeft = canvas.drawTurnLine_Left(vm.root, vm.rect, titleAttr);
                        vm.drawComponent[titleAttr]["turnLeft"] = vm.turnLeft;
                        vm.group.add(vm.turnLeft);

                        vm.turnRight = canvas.drawTurnLine_Right(vm.root, vm.rect, titleAttr);
                        vm.drawComponent[titleAttr]["turnRight"] = vm.turnRight;
                        vm.group.add(vm.turnRight);
                    }
                }
                var text = canvas.iconDraw(vm.rect, ImgObj, titleAttr, vm.iCounter, vm.processCounter);
                text.on('click', function(e){
                    vm.commonClickedElement(e, title, text, this);
                });
                vm.drawComponent[titleAttr]["rect"] = vm.rect;
                vm.drawComponent[titleAttr]["text"] = text;
                vm.group.add(vm.rect);
                vm.group.add(text);
                vm.layer.add(vm.group);
                vm.stage.add(vm.layer);
                vm.iCounter++;

                vm.rect.on('mouseenter', function (event) {
                    var mousePos = vm.stage.getPointerPosition();
                    var top = mousePos.y + event.evt.clientY -50;
                    var left = mousePos.x ;
                    vm.stage.container().style.cursor = 'pointer';
                    $('#toolTipShow').text(this.attrs.title);
                    $('#toolTipShow').css('top', top);
                    $('#toolTipShow').css('left', left);
                    $('#toolTipShow').css('opacity', 1);
                });

                vm.rect.on('mouseleave', function (event) {
                    $('#toolTipShow').css('opacity', 0);
                });

            }
            if(title == 'MetaData'){
                vm.MDCounter = 1;
            }
        }
//  Drop Machine Operation End





//  Common Clicked Element Start
        vm.commonClickedElement = function(e, title, text, tempthis){
            vm.clickedOperationDetail = tempthis;
            vm.operationType = title;
            vm.operation_ID_Attr = tempthis.attrs.id;

            if(title == 'MetaData'){
                vm.op_MetaData = true;
                vm.op_ProcessData = false;
                vm.op_Operation = false;
                vm.op_DBOutput = false;
            }else if(title == 'DB Output'){
                vm.op_MetaData = false;
                vm.op_ProcessData = false;
                vm.op_Operation = false;
                vm.op_DBOutput = true;
            }else if(title == 'Process Data'){
                vm.op_MetaData = false;
                vm.op_ProcessData = true;
                vm.op_Operation = false;
                vm.op_DBOutput = false;
            }else{
                vm.op_MetaData = false;
                vm.op_ProcessData = false;
                vm.op_Operation = true;
                vm.op_DBOutput = false;

                vm.operation.forEach(function(data){
                    data.child.forEach(function(d){
                        if(d.name == title){
                            vm.selected_Opr_Parent = data.name;
                            vm.operation_Parent = data.name;
                        }
                    });
                });

            }
            $scope.$apply();

            var bodyOffsets = document.body.getBoundingClientRect();
            var scroll = $(".sideSubBarWidth2").scrollTop();
            var leftSideWidth = $(".sideSubBarWidth1").width() + $(".sidebar").width() + 20;
            var tempX = e.evt.pageX - bodyOffsets.left - leftSideWidth + $(".sideSubBarWidth2").scrollLeft();
            var tempY = e.evt.pageY + scroll;
            $('.opDropDown').css('left', tempX);
            $('.opDropDown').css('top', tempY);
            $('.opDropDown').show();
            setTimeout(function(){
                $('.opDropDown').hide();
            }, 5000);
        }
//  Common Clicked Element End





// Clear All Operations Start
        vm.clearAll = function(){
            vm.dropCounter = 0;
            vm.layer.destroy();
            vm.selected_TitleID_Attr_Name = [];
            vm.selected_MainTitle_Attr_Name = [];
            vm.root;
            vm.rect;
            vm.arrow;
            vm.drawComponent = {};
            vm.selected_TitleID_Attr_Name = [];
            vm.selected_MainTitle_Attr_Name = [];
            vm.processCounter = 0;
            vm.metadataCounter = 0;
            vm.MDCounter = 0;
            vm.dbCounter = 0;
            vm.opCounter = 0;
            vm.iCounter = 0;
            vm.turnBottom;
            vm.turnLeft;
            vm.turnRight;
        }
// Clear All Operations End





// Check Existing DataBase Start
        vm.ExistingDataBaseChkValue = false;
        vm.chkExistingDataBase = function () {
            var chkBoxVal = $("[id='ExistingDataBase']").is(':checked');
            if(chkBoxVal){
                vm.ExistingDataBaseChkValue = true;
                setTimeout(function(){
                    $(".commonSelect_DBName").selectpicker();
                },200);
            }else{
                vm.ExistingDataBaseChkValue = false;
            }
        }
// Check Existing DataBase End





// Check Split Value Start
        vm.SplitChkValue = false;
        vm.chkSplitValue = function () {
            var chkBoxVal = $("[id='SplitBasedOnNonTarget']").is(':checked');
            if(chkBoxVal){
                vm.SplitChkValue = true;
                setTimeout(function(){
                    $(".commonSelect_Split").selectpicker();
                },200);
            }else{
                vm.SplitChkValue = false;
            }
        }
// Check Split Value End





// Operation Select Meta Data Start
        vm.opr_MetaData = function () {
            $('.opDropDown').hide();
            $('#SelectMetadata').modal({
                backdrop: 'static',
                keyboard: false,
            });
        }
// Operation Select Meta Data End





// Operation Process Data Start
        vm.opr_ProcessData = function(){
            $('.opDropDown').hide();
            $('#table_Data').modal('show');

            var ProcessCount = vm.clickedOperationDetail.attrs.processIndex;
            var tempData = vm.Result_DBOutput[ProcessCount-1];
            if(tempData != undefined){
                vm.Load_Process_Res = false;
                $scope.$apply();

                var tempObjData = Object.keys(Object.values(tempData)[0]);
                var tHead = [];
                tempObjData.forEach(function(data){
                    var temp = '<th title="' + data + '">' + data + '</th>';
                    tHead.push(temp);
                });

                var sNo_Head = '<th title="S. No.">S. No.</th>';
                var tableHeaderData = '<tr>' + sNo_Head + tHead + '</tr>';
                tableHeaderData = tableHeaderData.replace(/,/g, '');

                var trArr = [];
                tempData.forEach(function(data, index){
                    Object.values(data).forEach(function (d,i){
                        var temp = '';
                        if(i == 0){
                            var finalIndex = index + 1;
                            var tempSNo = '<td title="' + finalIndex + '">' + finalIndex + '</td>';
                            temp = '<tr>' + tempSNo + '<td title="' + d + '">' + d + '</td>';
                        }else if(i == Object.values(data).length-1){
                            temp = '<td title="' + d + '">' + d + '</td></tr>';
                        }else{
                            temp = '<td title="' + d + '">' + d + '</td>';
                        }
                        trArr.push(temp);
                    });
                });

                var tableHtml = '<table class="table table-bordered bordered table-striped table-condensed tablesorter">' + tableHeaderData + trArr + '</table>';
                tableHtml = tableHtml.replace(/,/g, '');

                $('#processTableDiv').html('');
                $('#processTableDiv').html('').append(tableHtml);
            }
        }
// Operation Process Data End





// Drop Operation Start
        vm.opr_Operation = function(){
            $('.opDropDown').hide();
            $('#SelectMachineOperation').modal({
                backdrop: 'static',
                keyboard: false,
            });

            console.log(vm.MachineOperationList)

            vm.MachineOperationList.forEach(function (d,i) {
                console.log(i,d)
            });

            vm.childAttributes = {};
            vm.childAttributes.Measure = {};
            vm.childAttributes.Dimension = {};

            if(vm.operation_ID_Attr != undefined){
                var dataObj;
                vm.MachineOperationList.forEach(function(data, index){
                    if(data.info == vm.operation_ID_Attr){
                        vm.meaDimChildArr = [];
                        JSON.parse(data.datacolumn).forEach(function (dd) {
                            vm.meaDimChildArr.push(dd);
                        });
                        dataObj = data;

                        vm.meaDimChildArr.forEach(function(data){
                            vm.meadimArr.forEach(function(d){
                                if(data == d.columnName){
                                    if(d.dataKey == 'Measure'){
                                        vm.childAttributes.Measure[d.reName] = d;
                                    }
                                    if(d.dataKey == 'Dimension'){
                                        vm.childAttributes.Dimension[d.reName] = d;
                                    }
                                }
                            });
                        });
                    }
                });

                vm.operation.forEach(function(data){
                    data.child.forEach(function(d){
                        if(vm.operationType == d.name){
                            vm.operation_Parent = data.name;
                        }
                    });
                });

                if(dataObj){
                    if(vm.operation_Parent == 'Data Cleansing'){
                        vm.MachineParameter.Cleansing.input = dataObj.fillvalue;
                        vm.MachineParameter.Cleansing.find = dataObj.find;
                        vm.MachineParameter.Cleansing.replace = dataObj.rplace;
                    }else if(vm.operation_Parent == 'Data Regression'){
                        vm.MachineParameter.Regression.target = dataObj.target;
                        $(".commonSelect_Reg").selectpicker();
                        setTimeout(function(){
                            $(".commonSelect_Reg").selectpicker('refresh');
                        },200);
                        if(dataObj.split_Based_on == 'true' || dataObj.split_Based_on == true){
                            $('#SplitBasedOnNonTarget').prop('checked', true);
                            vm.SplitChkValue = true;

                            vm.MachineParameter.Regression.SplitTrain = dataObj.train_split;
                            vm.MachineParameter.Regression.SplitTest = dataObj.test_split;
                            vm.MachineParameter.Regression.splitOn = dataObj.split_on;
                            $(".commonSelect_Split").selectpicker();
                            setTimeout(function(){
                                $(".commonSelect_Split").selectpicker('refresh');
                            },200);
                        }else{
                            $('#SplitBasedOnNonTarget').prop('checked', false);
                            vm.SplitChkValue = false;
                        }
                    }else if(vm.operation_Parent == 'Data Classification'){
                        vm.MachineParameter.Classification.target = dataObj.target;
                        $(".commonSelect_Class").selectpicker();
                        setTimeout(function(){
                            $('.commonSelect_Class').selectpicker('refresh');
                        },200);
                    }else if(vm.operation_Parent == 'Data Clustering'){
                        vm.MachineParameter.Clustering.numberOfCluster = dataObj.kcluster;
                        vm.MachineParameter.Clustering.linkageType = dataObj.linkage;
                        $(".commonSelect_Cluster").selectpicker();
                        setTimeout(function(){
                            $('.commonSelect_Cluster').selectpicker('refresh');
                        },200);
                    }
                }else{
                    setTimeout(function(){
                        $(".commonSelect_Reg").selectpicker();
                        $(".commonSelect_Cluster").selectpicker();
                        $(".commonSelect_Class").selectpicker();
                    },200);
                }

            }
        }
// Drop Operation End





// Drop DB Output Start
        vm.opr_DBOutput = function(){
            $('.opDropDown').hide();
            $('#DB_Output').modal({
                backdrop: 'static',
                keyboard: false,
            });
            $(".commonSelect_DB").selectpicker();
        }
// Drop DB Output End





// Show DB Result Start
        vm.opr_DBResult = function () {
            $('.opDropDown').hide();
            $('#DB_Result').modal('show');

            var tempData = vm.Result_DBOutput[vm.Result_DBOutput.length-1];
            if(tempData != undefined){
                vm.Load_DB_Res = false;
                $scope.$apply();

                var tempObjData = Object.keys(Object.values(tempData)[0]);
                var tHead = [];
                tempObjData.forEach(function(data){
                    var temp = '<th title="' + data + '">' + data + '</th>';
                    tHead.push(temp);
                });

                var sNo_Head = '<th title="S. No.">S. No.</th>';
                var tableHeaderData = '<tr>' + sNo_Head + tHead + '</tr>';
                tableHeaderData = tableHeaderData.replace(/,/g, '');

                var trArr = [];
                tempData.forEach(function(data, index){
                    Object.values(data).forEach(function (d,i){
                        var temp = '';
                        if(i == 0){
                            var finalIndex = index + 1;
                            var tempSNo = '<td title="' + finalIndex + '">' + finalIndex + '</td>';
                            temp = '<tr>' + tempSNo + '<td title="' + d + '">' + d + '</td>';
                        }else if(i == Object.values(data).length-1){
                            temp = '<td title="' + d + '">' + d + '</td></tr>';
                        }else{
                            temp = '<td title="' + d + '">' + d + '</td>';
                        }
                        trArr.push(temp);
                    });
                });

                var tableHtml = '<table class="table table-bordered bordered table-striped table-condensed tablesorter">' + tableHeaderData + trArr + '</table>';
                tableHtml = tableHtml.replace(/,/g, '');

                $('#DB_Result_Div').html('');
                $('#DB_Result_Div').html('').append(tableHtml);
            }
        }
// Show DB Result End





// Operation Delete Start
        vm.opr_Delete = function(){
            $('.opDropDown').hide();
            if(vm.operationType == 'MetaData'){
                vm.MDCounter = 0;
            }

            var item = vm.clickedOperationDetail.attrs.id;
            if(item == vm.selected_TitleID_Attr_Name[0]){
                vm.dropCounter = 0;
                vm.layer.destroy();
                vm.selected_TitleID_Attr_Name = [];
                vm.selected_MainTitle_Attr_Name = [];
                vm.root;
                vm.rect;
                vm.arrow;
                vm.drawComponent = {};
                vm.selected_TitleID_Attr_Name = [];
                vm.selected_MainTitle_Attr_Name = [];
                vm.processCounter = 0;
                vm.metadataCounter = 0;
                vm.MDCounter = 0;
                vm.dbCounter = 0;
                vm.opCounter = 0;
                vm.iCounter = 0;
                vm.turnBottom;
                vm.turnLeft;
                vm.turnRight;
            }
            var deleteIndex = vm.selected_TitleID_Attr_Name.indexOf(item);
            vm.selected_TitleID_Attr_Name.splice(deleteIndex,1);
            vm.selected_MainTitle_Attr_Name.splice(deleteIndex,1);
            if(vm.operationType == 'DB Output'){
                $(".droppableArea").droppable("enable");
            }
            vm.layer.destroy();
            var tempArr = angular.copy(vm.selected_MainTitle_Attr_Name);
            tempArr.forEach(function(d,index){
                var obj = {};
                if(d == 'MetaData'){
                    obj = {
                        "icon" : "\uf1c0",
                        "color" : "yellow",
                        'text' : d,
                        'key' : 'Drop_MetaData',
                    }
                }else if(d == 'Process Data'){
                    obj = {
                        "icon" : "\uf013",
                        "color" : "green",
                        'text' : d,
                        'key' : 'Process_Data',
                    }
                }else if(d == 'DB Output'){
                    obj = {
                        "icon" : "\uf08b",
                        "color" : "red",
                        'text' : d,
                        'key' : 'DB_Output',
                    }
                }else{
                    vm.childOperation.forEach(function(data){
                        if(d == data.name){
                            obj = {
                                "icon" : data.icon,
                                "color" : "#2196F3",
                                'text' : d,
                                'key' : data.key,
                            }
                        }
                    });
                }
                vm.ElementReDraw(d, obj, index);
            });

            var str = vm.operation_ID_Attr.substring(0, vm.operation_ID_Attr.lastIndexOf('_'));
            var deleteIndex;

            vm.MachineOperationList.forEach(function(d,i){
                var temp = d.objective;
                var tempStr = temp.substring(2, temp.length-2);
                if(tempStr == str){
                    deleteIndex = i;
                }
            });

            if(deleteIndex != undefined){
                vm.MachineOperationList.splice(deleteIndex,1);
            }
        }
// Operation Delete End





//  Element Re-Draw Start
        vm.ElementReDraw = function(title, ImgObj, tempIndex){
            if(tempIndex == 0){
                vm.selected_MainTitle_Attr_Name = [];
                vm.selected_TitleID_Attr_Name = [];
                vm.turnBottom;
                vm.turnLeft;
                vm.turnRight;
                vm.opCounter = 0;
                vm.processCounter = 0;
                vm.metadataCounter = 0;
                vm.MDCounter = 0;
                vm.dbCounter = 0;
            }
            var titleAttr = title;
            if(title == 'MetaData'){
                vm.metadataCounter++;
                titleAttr = ImgObj.key + '_' + vm.metadataCounter;
            }else if(title == 'Process Data'){
                vm.processCounter++;
                titleAttr = ImgObj.key + '_' + vm.processCounter;
            }else if(title == 'DB Output'){
                vm.dbCounter++;
                titleAttr = ImgObj.key + '_' + vm.dbCounter;
            }else{
                vm.childOperation.forEach(function (d){
                    if(title == d.name){
                        vm.opCounter++;
                        titleAttr = ImgObj.key + '_' + vm.opCounter;
                    }
                });
            }

            vm.group = new Konva.Group({
                "id" : titleAttr,
            });

            if(!vm.drawComponent[titleAttr]){
                vm.drawComponent[titleAttr] = {};
            }

            vm.selected_MainTitle_Attr_Name.push(title);
            vm.selected_TitleID_Attr_Name.push(titleAttr);

            var modLength = vm.selected_TitleID_Attr_Name.length - 1;
            if(tempIndex == 0){
                vm.iCounter = 0;
                vm.root = canvas.getRootRect(titleAttr, title, tempIndex);
                vm.rect = vm.root;
            }else{
                vm.rect = vm.root.attrs.addChild(titleAttr, title, modLength);
                vm.arrow = canvas.drawArrow(vm.root, vm.rect, titleAttr);
                vm.drawComponent[titleAttr]["arrow"] = vm.arrow;
                vm.group.add(vm.arrow);
                if(modLength%6 == 0){
                    vm.turnBottom = canvas.drawTurnLine_Bottom(vm.root, vm.rect, titleAttr);
                    vm.drawComponent[titleAttr]["turnBottom"] = vm.turnBottom;
                    vm.group.add(vm.turnBottom);

                    vm.turnLeft = canvas.drawTurnLine_Left(vm.root, vm.rect, titleAttr);
                    vm.drawComponent[titleAttr]["turnLeft"] = vm.turnLeft;
                    vm.group.add(vm.turnLeft);

                    vm.turnRight = canvas.drawTurnLine_Right(vm.root, vm.rect, titleAttr);
                    vm.drawComponent[titleAttr]["turnRight"] = vm.turnRight;
                    vm.group.add(vm.turnRight);
                }
            }

            var text = canvas.iconDraw(vm.rect, ImgObj, titleAttr, vm.iCounter, vm.processCounter);
            text.on('click', function (e) {
                vm.commonClickedElement(e, title, text, this);
            });
            vm.drawComponent[titleAttr]["rect"] = vm.rect;
            vm.drawComponent[titleAttr]["text"] = text;
            vm.group.add(vm.rect);
            vm.group.add(text);
            vm.layer.add(vm.group);
            vm.stage.add(vm.layer);
            vm.iCounter++;

            vm.rect.on('mouseenter', function (event) {
                var mousePos = vm.stage.getPointerPosition();
                var top = mousePos.y + event.evt.clientY -50;
                var left = mousePos.x ;
                vm.stage.container().style.cursor = 'pointer';
                $('#toolTipShow').text(this.attrs.title);
                $('#toolTipShow').css('top', top);
                $('#toolTipShow').css('left', left);
                $('#toolTipShow').css('opacity', 1);
            });

            vm.rect.on('mouseleave', function (event) {
                $('#toolTipShow').css('opacity', 0);
            });

        }
//  Element Re-Draw End





// Operation Update Start
        vm.opr_Update = function(){
            $('.opDropDown').hide();
            $('#update_Operations').modal({
                backdrop: 'static',
                keyboard: false,
            });
        }
// Operation Update End





// Select New Operation Start
        vm.replaceOperation = '';
        vm.selectNewOperation = function(key){
            vm.replaceOperation = key;

            $("div").removeClass("selectedOpr")
            $('#' + key).addClass('selectedOpr');
        }
// Select New Operation End





// Save Update Start
        vm.updateMachineListObj = {};
        vm.updateOperation = function(){
            $('#update_Operations').modal('hide');

            var replaceIndex = vm.clickedOperationDetail.attrs.positionIndex;
            var tempObj = {};
            vm.childOperation.forEach(function(data){
                if(vm.replaceOperation == data.key){
                    tempObj = data;
                }
            });

            vm.updateMachineListObj[vm.clickedOperationDetail.attrs.id] = tempObj.key;
            vm.selected_TitleID_Attr_Name[replaceIndex] = vm.replaceOperation;
            vm.selected_MainTitle_Attr_Name[replaceIndex] = tempObj.name;

            vm.layer.destroy();

            $.each(vm.updateMachineListObj, function (k,v) {
                var replaceStr = v + '_' + k.substring(k.lastIndexOf('_')+1);
                vm.MachineOperationList.forEach(function (d,i) {
                    if(d.info == k){
                        d.info = replaceStr;
                        d.objective = '[' + v + ']';
                    }
                });
            });


            var tempArr = angular.copy(vm.selected_MainTitle_Attr_Name);
            tempArr.forEach(function(d,index){
                var obj = {};
                if(d == 'MetaData'){
                    obj = {
                        "icon" : "\uf1c0",
                        "color" : "yellow",
                        'text' : d,
                        'key' : 'Drop_MetaData',
                    }
                }else if(d == 'Process Data'){
                    obj = {
                        "icon" : "\uf013",
                        "color" : "green",
                        'text' : d,
                        'key' : 'Process_Data',
                    }
                }else if(d == 'DB Output'){
                    obj = {
                        "icon" : "\uf08b",
                        "color" : "red",
                        'text' : d,
                        'key' : 'DB_Output',
                    }
                }else{
                    vm.childOperation.forEach(function(data){
                        if(d == data.name){
                            obj = {
                                "icon" : data.icon,
                                "color" : "#2196F3",
                                'text' : d,
                                'key' : data.key,
                            }
                        }
                    });
                }
                vm.ElementReDraw(d, obj, index);
            });
        }
// Save Update End





// Bind Metadata List Start
        vm.MesaureDimensionPopUp = false;
        dataFactory.request($rootScope.MetadataDashboardList_Url,'post',"").then(function(response){
            if(response.data.errorCode == 1){
                vm.queryList = JSON.parse(response.data.result);
            }else{
                dataFactory.errorAlert("Check Your Connection");
            }
        }).then(function() {
            if ($stateParams.id != undefined) {
                var metadataId = $stateParams.id;
                var i = 0, index;
                vm.queryList.forEach(function (d, indexArray) {
                    if (d.metadataId == metadataId) {
                        index = indexArray;
                    }
                });
                vm.selectDataSource(JSON.stringify(vm.queryList[index]));
            }
            setTimeout(function(){
                $(".commonSelect").selectpicker();
            },200);
        });
// Bind Metadata List End







// Bind Measure-Dimension From Metadata Start
        vm.selectDataSource = function(model){
            var fetchDataAndColumnsServer = function(){
                var promise = new Promise(function (resolve, reject) {
                    if (model != undefined) {
                        vm.metadataId = JSON.parse(model).metadataId;
                        var data = {
                            "metadataId": JSON.parse(model).metadataId,
                        };
                        dataFactory.request($rootScope.MetadataGet_Url, 'post', data).then(function(response){
                            var tempMetaObj={
                                "name" : response.data.result.name,
                                "connObject" : JSON.parse(response.data.result.metadataObject),
                                "metadataId" : response.data.result.metadata_id
                            }
                            vm.meta_Data = JSON.stringify(tempMetaObj);
                        }).then(function(){
                            vm.metadataObject = vm.meta_Data;
                            var data = {
                                "matadataObject" : vm.metadataObject,
                                "type" : 'machine',
                            };
                            dataFactory.request($rootScope.DashboardDataCacheToRedis_Url,'post',data).then(function(response){
                                if(response.data.errorCode == 1){
                                    vm.metadataObject = JSON.parse(vm.metadataObject);
                                    vm.showMeasureDimension = true;
                                    if(vm.metadataObject.connObject.type!=undefined){
                                        var columns = JSON.parse(response.data.result.tableColumn);
                                        var keysArray = [];
                                        var k = 0;
                                        $.each(columns,function(key,d){
                                            keysArray[k] = {
                                                "columType" : d.Type,
                                                "tableName" : d.tableName,
                                                "columnName" : d.Field,
                                                "reName" : d.Field,
                                                "dataKey" : d.dataType,
                                                "type" : "defined"
                                            };
                                            k++;
                                        });
                                        vm.tableColumns = keysArray;
                                        vm.MesaureDimensionPopUp = true;
                                    }else{
                                        vm.tableColumns = vm.metadataObject.connObject.column;
                                        vm.MesaureDimensionPopUp = true;
                                    }
                                    vm.Attributes = {};
                                    vm.childAttributes = {};
                                }else{
                                    dataFactory.errorAlert(response.data.errorCode);
                                }
                                resolve(vm.metadataObject);
                            });
                        });
                    }
                });
                return promise;
            }
            fetchDataAndColumnsServer().then($rootScope.initializeNodeClient).then(function(){
                
            });
        }
// Bind Measure-Dimension From Metadata End





// Check-Uncheck Measure-Dimension Start
        vm.checkMeasureDimension = function(attr, type){
            vm.meadimArr = [];
            vm.meaArr = [];
            vm.dimArr = [];
           if(vm.Attributes.Measure){
               $.each(vm.Attributes.Measure, function(k, v){
                   if(v!=0){
                       vm.meadimArr.push(v);
                       vm.meaArr.push(v);
                   }
               });
           }
           if(vm.Attributes.Dimension){
               $.each(vm.Attributes.Dimension, function(k, v){
                   if(v!=0){
                       vm.meadimArr.push(v);
                       vm.dimArr.push(v);
                   }
               });
           }
        }
// Check-Uncheck Measure-Dimension End





// Check-Uncheck Child Measure-Dimension Start
        vm.checkChildMeasureDimension = function(attr, type){
            vm.meaDimChildArr = [];
            if(vm.childAttributes.Measure){
                $.each(vm.childAttributes.Measure, function(k, v){
                    if(v != 0){
                        vm.meaDimChildArr.push(v.columnName);
                    }
                });
            }
            if(vm.childAttributes.Dimension){
                $.each(vm.childAttributes.Dimension, function(k, v){
                    if(v != 0){
                        vm.meaDimChildArr.push(v.columnName);
                    }
                });
            }
        }
// Check-Uncheck Measure-Dimension End





// Save Parent Measure-Dimension Start
        vm.saveMeaDimPopUp = function(){
            vm.meaArr = [];
            vm.dimArr = [];
            if(vm.Attributes.Measure != undefined){
                vm.meaArr = Object.values(vm.Attributes.Measure);
            }
            if(vm.Attributes.Dimension != undefined){
                vm.dimArr = Object.values(vm.Attributes.Dimension);
            }
            vm.childTableColumns = vm.meaArr.concat(vm.dimArr);

            $('#SelectMetadata').modal('hide');

            vm.child_Measure = true;
            vm.child_Dimension = true;
            if(vm.meaArr.length){
                vm.child_Measure = true;
            }else{
                vm.child_Measure = false;
            }

            if(vm.dimArr.length){
                vm.child_Dimension = true;
            }else{
                vm.child_Dimension = false;
            }
        }
// Save Parent Measure-Dimension End





// Save All Machine Opertaions Start
        vm.MachineOperationList = [];
        vm.saveMachineOperation = function(operationParent){
            $('#SelectMachineOperation').modal('hide');

            var objType;
            vm.childOperation.forEach(function(d){
                if(d.name == vm.operationType){
                    objType = d.key;
                }
            });

            if(operationParent == 'Data Cleansing'){
                if(vm.MachineParameter.Cleansing.input == undefined){
                    var Fill_Value = '';
                }else{
                    var Fill_Value = vm.MachineParameter.Cleansing.input;
                }

                if(vm.MachineParameter.Cleansing.find == undefined){
                    var Find_Value = '';
                }else{
                    var Find_Value = vm.MachineParameter.Cleansing.find;
                }

                if(vm.MachineParameter.Cleansing.replace == undefined){
                    var Replace_Value = '';
                }else{
                    var Replace_Value = vm.MachineParameter.Cleansing.replace;
                }

                var tempData = {
                    datacolumn : JSON.stringify(vm.meaDimChildArr),
                    objective : '["' + objType + '"]',
                    find : Find_Value,
                    rplace : Replace_Value,
                    target : '',
                    fillvalue : Fill_Value,
                    metadataId : vm.metadataObject.metadataId,
                    info : vm.operation_ID_Attr,
                }
            }else if(operationParent == 'Data Regression'){
                if(vm.MachineParameter.Regression.target == undefined){
                    var Reg_Target = '';
                }else{
                    var Reg_Target = vm.MachineParameter.Regression.target;
                }

                if(vm.MachineParameter.Regression.splitOn == undefined){
                    var Reg_SplitOn = '';
                }else{
                    var Reg_SplitOn = vm.MachineParameter.Regression.splitOn;
                }

                if(vm.MachineParameter.Regression.SplitTrain == undefined){
                    var Reg_TrainSplit = '';
                }else{
                    var Reg_TrainSplit = vm.MachineParameter.Regression.SplitTrain;
                }

                if(vm.MachineParameter.Regression.SplitTest == undefined){
                    var Reg_TestSplit = '';
                }else{
                    var Reg_TestSplit = vm.MachineParameter.Regression.SplitTest;
                }

                var chkBoxVal = $("[id='SplitBasedOnNonTarget']").is(':checked');
                if(chkBoxVal){
                    if(vm.operationType == 'Linear Regression'){
                        objType = 'LinReg2';
                    }else if(vm.operationType == 'Logistic Regression'){
                        objType = 'LogReg2';
                    }else if(vm.operationType == 'Decision Tree Regressor'){
                        objType = 'DecReg2';
                    }else if(vm.operationType == 'Random Forest Regressor'){
                        objType = 'RanForReg2';
                    }
                }

                var tempData = {
                    datacolumn : JSON.stringify(vm.meaDimChildArr),
                    objective : '["' + objType + '"]',
                    find : '',
                    rplace : '',
                    target : Reg_Target,
                    split_on : Reg_SplitOn,
                    split_Based_on : chkBoxVal,
                    train_split : Reg_TrainSplit,
                    test_split : Reg_TestSplit,
                    metadataId : vm.metadataObject.metadataId,
                    info : vm.operation_ID_Attr,
                }
            }else if(operationParent == 'Data Classification'){
                if(vm.MachineParameter.Classification.target == undefined){
                    var Class_Target = '';
                }else{
                    var Class_Target = vm.MachineParameter.Classification.target;
                }

                var tempData = {
                    datacolumn : JSON.stringify(vm.meaDimChildArr),
                    objective : '["' + objType + '"]',
                    find : '',
                    rplace : '',
                    target : Class_Target,
                    metadataId : vm.metadataObject.metadataId,
                    info : vm.operation_ID_Attr,
                }
            }else if(operationParent == 'Data Clustering'){
                if(vm.MachineParameter.Clustering.numberOfCluster == undefined){
                    var Cluster_Number = '';
                }else{
                    var Cluster_Number = vm.MachineParameter.Clustering.numberOfCluster;
                }

                var linkType = '';
                if(vm.operationType == 'Hierarchical Clustering'){
                    if(vm.MachineParameter.Clustering.linkageType == undefined){
                        linkType = '';
                    }else{
                        linkType = vm.MachineParameter.Clustering.linkageType;
                    }
                }

                var tempData = {
                    datacolumn : JSON.stringify(vm.meaDimChildArr),
                    objective : '["' + objType + '"]',
                    linkage : linkType,
                    find : '',
                    rplace : '',
                    kcluster : parseInt(Cluster_Number),
                    metadataId : vm.metadataObject.metadataId,
                    info : vm.operation_ID_Attr,
                }
            }
            var status_Val;
            vm.MachineOperationList.forEach(function (d,i){
                if(vm.operation_ID_Attr == d.info){
                    status_Val = d.status;
                }
            });

            tempData.connectionString = $rootScope.rootIP + ':' + userPort + '/getColumnData/';
            tempData.dbType = vm.MachineParameter.DBOutput.datasourceType;
            tempData.existingDataBase = vm.MachineParameter.DBOutput.existingDataBase;
            tempData.mongodb_collection = vm.MachineParameter.DBOutput.tableName;
            tempData.mongodb_databasename = vm.MachineParameter.DBOutput.dbName;
            tempData.mongodb_host = vm.MachineParameter.DBOutput.host;
            tempData.mongodb_password = vm.MachineParameter.DBOutput.password;
            tempData.mongodb_port = vm.MachineParameter.DBOutput.port;
            tempData.mongodb_username = vm.MachineParameter.DBOutput.username;
            tempData.sessionId = $rootScope.accessToken;
            tempData.status = status_Val;

            var rIndex, rData;
            vm.MachineOperationList.forEach(function (d, i) {
                if(tempData.info == d.info){
                    rIndex = i;
                    rData = tempData;
                    vm.MachineOperationList[i] = tempData;
                }
            });

            if(rIndex == undefined){
                vm.MachineOperationList.push(tempData);
            }

            vm.MachineOperationList.forEach(function (d) {
                if(d.info == vm.clickedOperationDetail.attrs.id){
                    d = tempData;
                }
            });
        }

// Save All Machine Opertaions End





// Test DB Output Start
        vm.saveDB_Btn = true;
        vm.output_DB = false;
        vm.TestDBOutput = function(){
            if(vm.MachineParameter.DBOutput.host == undefined || vm.MachineParameter.DBOutput.host == ''){
                dataFactory.errorAlert('Host is required !');
                return;
            }else if(vm.MachineParameter.DBOutput.port == undefined || vm.MachineParameter.DBOutput.port == ''){
                dataFactory.errorAlert('Port is required !');
                return;
            }else if(vm.MachineParameter.DBOutput.username == undefined || vm.MachineParameter.DBOutput.username == ''){
                dataFactory.errorAlert('User Name is required !');
                return;
            }else if(vm.MachineParameter.DBOutput.password == undefined || vm.MachineParameter.DBOutput.password == ''){
                dataFactory.errorAlert('Password is required !');
                return;
            }else if(vm.MachineParameter.DBOutput.databaseType == undefined || vm.MachineParameter.DBOutput.databaseType == ''){
                dataFactory.errorAlert('Database Type is required !');
                return;
            }
            var dataForm = {
                host : vm.MachineParameter.DBOutput.host,
                port : parseInt(vm.MachineParameter.DBOutput.port),
                username : vm.MachineParameter.DBOutput.username,
                password : vm.MachineParameter.DBOutput.password,
                datasourceType : vm.MachineParameter.DBOutput.databaseType,
            }

            var data = {
                dataForm : JSON.stringify(dataForm),
            }

            dataFactory.request($rootScope.dataSourceConnectionCheck_Url,'post',data).then(function(response){
                if(response.data.errorCode == 1){
                    vm.databaselist = [];
                    response.data.result.forEach(function(d){
                        vm.databaselist.push(d);
                    });
                    setTimeout(function(){
                        $(".commonSelect_DBName").selectpicker();
                    },200);
                    vm.saveDB_Btn = false;
                    vm.output_DB = true;
                    dataFactory.successAlert(response.data.message);
                }else{
                    dataFactory.errorAlert(response.data.message);
                }
            });
        }
// Test DB Output End





// Send DB Output Start
        vm.Result_DBOutput = [];

        vm.SendDBOutput = function(){
            $('#DB_Output').modal('hide');

            if(vm.MachineParameter.DBOutput.databaseType == undefined || vm.MachineParameter.DBOutput.databaseType == ''){
                dataFactory.errorAlert('DataBase Type is required !');
                return;
            }
            if(vm.MachineParameter.DBOutput.ExistingDataBase){
                if(vm.MachineParameter.DBOutput.EDataBaseName == undefined || vm.MachineParameter.DBOutput.EDataBaseName == ''){
                    dataFactory.errorAlert('DataBase Name is required !');
                    return;
                }else{
                    var db_name = vm.MachineParameter.DBOutput.EDataBaseName;
                }
            }else{
                if(vm.MachineParameter.DBOutput.DataBaseName == undefined || vm.MachineParameter.DBOutput.DataBaseName == ''){
                    dataFactory.errorAlert('DataBase Name is required !');
                    return;
                }else{
                    var db_name = vm.MachineParameter.DBOutput.DataBaseName;
                }
            }
            if(vm.MachineParameter.DBOutput.TableName == undefined || vm.MachineParameter.DBOutput.TableName == ''){
                dataFactory.errorAlert('Table Name is required !');
                return;
            }

            if(vm.MachineParameter.DBOutput.ExistingDataBase == undefined || vm.MachineParameter.DBOutput.ExistingDataBase == ''){
                var exist_DB = false;
            }else{
                var exist_DB = vm.MachineParameter.DBOutput.ExistingDataBase;
            }

            var dataForm = {
                dbName : db_name,
                tableName : vm.MachineParameter.DBOutput.TableName,
                host : vm.MachineParameter.DBOutput.host,
                port : vm.MachineParameter.DBOutput.port,
                username : vm.MachineParameter.DBOutput.username,
                password : vm.MachineParameter.DBOutput.password,
                datasourceType : vm.MachineParameter.DBOutput.databaseType,
                existingDataBase : exist_DB,
            }
            vm.DataBaseDetail = dataForm;
            vm.getDBResult(0);
        }
// Send DB Output End





// Get DB Output From API Start
        vm.getDBResult = function(index){
            var apiUrl = $rootScope.rootIP;
            var statusVal = 0;
            if(index == 0){
                vm.Result_DBOutput = [];
            }
            if(index+1 == vm.MachineOperationList.length){
                statusVal = 1;
            }

            var data = vm.MachineOperationList[index];
            var da = data;

            if(vm.MachineParameter.DBOutput.ExistingDataBase){
                var db_name = vm.MachineParameter.DBOutput.EDataBaseName;
                var exist_DB = vm.MachineParameter.DBOutput.ExistingDataBase;
            }else{
                var db_name = vm.MachineParameter.DBOutput.DataBaseName;
                var exist_DB = false;
            }

            if(da){
                da.mongodb_host = vm.MachineParameter.DBOutput.host;
                da.mongodb_port = parseInt(vm.MachineParameter.DBOutput.port);
                da.mongodb_username = vm.MachineParameter.DBOutput.username;
                da.mongodb_password = vm.MachineParameter.DBOutput.password;
                da.mongodb_collection = vm.MachineParameter.DBOutput.TableName;
                da.mongodb_databasename = db_name;
                da.dbType = vm.MachineParameter.DBOutput.databaseType;
                da.status = statusVal;
                da.sessionId = $rootScope.accessToken;
                da.existingDataBase = exist_DB;
                da.connectionString = $rootScope.rootIP + ':' + userPort + '/getColumnData/';

                $.post(apiUrl + ":5000/", da).done(function(data){
                    var temp = JSON.parse(data);
		    var t = JSON.parse(temp[0]["data"]);
                    vm.Result_DBOutput.push(t);
                    if(index < vm.MachineOperationList.length){
                        vm.getDBResult(++index, statusVal);
                    }
                    if(index == vm.MachineOperationList.length){
                        dataFactory.successAlert('ML Model created successfully');
                    }
                });
            }
        }
// Get DB Output From API End































//Save Main Machine Model Start
        vm.publicViewType = "specific_group";
        vm.reportGrp = {};
        dataFactory.request($rootScope.UsergroupList_Url,'post',"").then(function(response){
            vm.userGroup = response.data.result;
        });
        dataFactory.request($rootScope.ReportGroupList_Url,'post',"").then(function(response){
            vm.reportGroup = response.data.result;
        });

        vm.selectGroup=function(divClass){
            if(divClass=='back'){
                $(".mainDiv").slideDown('fast');
                $(".select").slideUp('fast');
                $(".create").slideUp('fast');
                $(".footerDiv").slideUp('fast');
            }else{
                $("."+divClass).slideDown('fast');
                $(".footerDiv").slideDown('fast');
                $(".mainDiv").slideUp('fast');
            }
            if(divClass=='select' || divClass=='create'){
                vm.reportGrp = {};
            }
        }

        vm.checkView = function(page){
            vm.publicViewType=page;
            if(page == 'public_group'){
                $(".subDiv").hide();
                $(".footerDiv").show();
            }
            else{
                $(".subDiv").show();
                $(".footerDiv").hide();
            }
        }

        vm.groupTypeModal = function(){
            $('#reportGroup').modal('show');
        }

        vm.groupTypeSave = function(reportGrp){
            if(Object.keys(vm.reportGrp).length == 0 && vm.publicViewType=='sharedview'){
                dataFactory.errorAlert("Select group");
            }else{
                vm.reportGrp = reportGrp;
                $('#reportGroup').modal('hide');
                vm.saveMachineModel();
            }
        }


        vm.saveMachineModel = function(){
            if(vm.machineModelName == undefined || vm.machineModelName == ''){
                dataFactory.errorAlert('Machine Model Name is required !');
                return;
            }
            var oprArr = [];
            for(var i=0; i<vm.selected_TitleID_Attr_Name.length; i++){
                var obj = {};
                obj.key = vm.selected_TitleID_Attr_Name[i];
                obj.title = vm.selected_MainTitle_Attr_Name[i];
                oprArr.push(obj);
            }
            oprArr.forEach(function(d){
                if(d.title == 'MetaData'){
                    d.color = 'yellow';
                    d.icon = '\uf1c0';
                }else if(d.title == 'Process Data'){
                    d.color = 'green';
                    d.icon = '\uf013';
                }else if(d.title == 'DB Output') {
                    d.color = 'red';
                    d.icon = '\uf08b';
                }else{
                    vm.childOperation.forEach(function(data){
                        if(d.title == data.name){
                            d.color = '#2196F3';
                            d.icon = data.icon;
                        }
                    });
                }
            });
            var tempOprList = angular.copy(vm.MachineOperationList);
            tempOprList.forEach(function(d){
                delete d.sessionId;
            });

            var mdobj = {};
            mdobj.name = vm.metadataObject.name;
            mdobj.metadataId = vm.metadataObject.metadataId;

            var selected_Mea = [], selected_Dim = [];
            vm.meaArr.forEach(function (d) {
                selected_Mea.push(d.columnName);
            });
            vm.dimArr.forEach(function (d) {
                selected_Dim.push(d.columnName);
            });

            var tempObject = {};
            tempObject.allOperation = oprArr;
            tempObject.machineOperation = tempOprList;
            tempObject.dbDetail = vm.DataBaseDetail;
            tempObject.metaDataDetail = mdobj;
            tempObject.selectedMeasure = selected_Mea;
            tempObject.selectedDimension = selected_Dim;
            // tempObject.selectedMeasure = vm.meaArr;
            // tempObject.selectedDimension = vm.dimArr;

            console.log(tempObject.machineOperation)
            console.log(tempObject)
            console.log(vm.clickedOperationDetail.attrs)


            tempOprList.forEach(function (d,i){
                console.log(i, d.info);
            });

            


            var data = {
                "name" : vm.machineModelName,
                "machineDesc" : "Machine Model Description",
                "dbtype" : vm.DataBaseDetail.datasourceType,
                "machineObj" : tempObject,
                "reportGroup" : vm.reportGrp,
                "publicViewType" : vm.publicViewType,
            };

            console.log(data)

            dataFactory.request($rootScope.MachineSave_Url,'post',data).then(function(response){
                if(response.data.errorCode==1){
                    dataFactory.successAlert(response.data.message);
                    var id = response.data.result;
                    $window.location = '#/machine/view/' + id + '/1';
                }else{
                    dataFactory.errorAlert(response.data.message);
                }
            });
        }
//Save Main Machine Model End











    } ]);


