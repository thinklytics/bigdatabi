'use strict';
angular.module('app', [ 'ngSanitize','gridster' ])
    .controller('machineViewController',['$scope','$sce','dataFactory','$rootScope','$window','$q','$stateParams','$cookieStore','$location',function($scope,$sce,dataFactory,$rootScope,$window,$q,$stateParams,$cookieStore,$location) {
        //--- Top Menu Bar
        var data={};
        var addClass='';
        var viewClass='';
        var vm = $scope;
        if($location.path()=='/machine/'){
            viewClass='activeli';
        }else{
            addClass='activeli';
        }
        data['navTitle']="Machine Learning";
        data['icon']="fa fa-book";
        data['navigation']=[
            {
                "name":"Add",
                "url":"#/machine/add",
                "class":addClass
            },
            {
                "name":"View",
                "url":"#/machine/",
                "class":viewClass
            },
            {
                "name":"Close",
                "url":"#/machine/",
                "class":viewClass
            }
        ];
        $rootScope.$broadcast('subNavBar', data);
        //End Menu
        vm.permissionCheck=function(permission){
            return dataFactory.checkPermission(permission);
        }

        // vm.imageUrlBaseUrl = dataFactory.baseUrlData() + "machineImage/";
        var machineId = $stateParams.data;

        var userId = $cookieStore.get('userId');
        if(userId == undefined){
            userId = 1;
        }

        $scope.dataBaseIcon = [
            {
                "name" : "mysql",
                "image" : "theme/assets/img/MySQL-Logo.png",
            },
            {
                "name" : "mongodb",
                "image" : "theme/assets/img/mongodb.png",
            },
            {
                "name" : "mssql",
                "image" : "theme/assets/img/mssql-server.png",
            },
            {
                "name" : "cassandra",
                "image" : "theme/assets/img/cassandra-logo.png",
            },
            {
                "name" : "excel",
                "image" : "theme/assets/img/excel-logo.png",
            },
            {
                "name" : "awsrds",
                "image" : "theme/assets/img/aws-rds-logo.png",
            },
            {
                "name" : "oracle",
                "image" : "theme/assets/img/oracle-logo.png",
            },
            {
                "name" : "sybase",
                "image" : "theme/assets/img/sybase-logo.png",
            }
        ];

        vm.getDatasourceIconPath = function(obj){
            try{
                var datasourceType = obj.dbtype;
                var path = null;
                vm.dataBaseIcon.forEach(function(d){
                    if(datasourceType.toLowerCase() == d.name.toLowerCase()){
                        path = d.image;
                    }
                });
                return path;
            }catch (e){
                return "";
            }
        }



        var machineListObjLoad = new Promise(function (resolve, reject){
            dataFactory.request($rootScope.MachineList_Url,'post',"").then(function(response){
                resolve(response);
                if(response.data.errorCode==1){
                    vm.groupFolder = Object.keys(response.data.result);
                    vm.machineList = response.data.result;
                }else{
                    dataFactory.errorAlert(response.data.message);
                }
            });
        });

        function temp_one() {
            vm.loading=true;
            $scope.$apply();
            if(vm.loading){
                $(".loaderImage").fadeOut("slow");
            }
            setTimeout(function(){
                $('[rel="tooltip"]').tooltip();
            },100);
        }
        machineListObjLoad.then(temp_one);



// ML Delete
        vm.showSwal = function(type){
            if(type == 'warning-message-and-cancel') {
                swal({
                    title: 'Are you sure?',
                    text: 'You want to delete ML Model !',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: 'Yes',
                    cancelButtonText: 'No',
                    confirmButtonClass: "btn btn-success",
                    cancelButtonClass: "btn btn-danger",
                    buttonsStyling: false,
                }).then(function(value){
                    if(value){
                        $scope.delete();
                    }
                },
                function (dismiss) {

                })
            }
        }
        vm.delete = function(){
            var index;
            var id = vm.machineId;
            vm.machineSelectedFromList.forEach(function (d,i) {
                if(d.id==id){
                    index=i;
                }
            });
            var data={
                "id":id
            };
            dataFactory.request($rootScope.MachineDelete_Url,'post',data).then(function(response){
                if(response.data.errorCode==1){
                    dataFactory.successAlert("Delete Successfully");
                    vm.machineSelectedFromList.splice(index,1);
                    $('.stepFolder1').slideDown('fast');
                    $('.stepFolder').slideDown('fast');
                    $('.stepFolder2').slideUp('fast');
                    vm.selectedConfirm = 0;
                    vm.backBtn = 0;
                }else{
                    dataFactory.errorAlert(response.data.message);
                }
            });
            vm.selectedConfirm=0;
        }
// ML Delete




// sort machine list asc-desc Start
        vm.machineList_Order = 'desc';
        vm.machine_List_order = '-id';
        vm.orderMachineList = function(){
            if(vm.machineList_Order == 'desc'){
                vm.machine_List_order = '-id';
            }else if(vm.machineList_Order == 'asc'){
                vm.machine_List_order = 'id';
            }
        }
// sort machine list asc-desc End




        vm.toggle = function(e, machineObject, index){
            setTimeout(function(){
                $(function () {
                    $('[data-toggle="tooltip"]').tooltip();
                })
            },100);
            $('.imageToggle').removeClass('selectedImage');
            vm.selectedIndex = index;
            vm.machineId = machineObject.machine_id;
            $(e.currentTarget).addClass('selectedImage');
            vm.selectedConfirm = 1;
        }

        vm.selectedConfirm=0;
        vm.Folder = {
            toggleImage: function (e, mView, index) {
                setTimeout(function(){
                    $(function () {
                        $('[data-toggle="tooltip"]').tooltip();
                    })
                },100);
                $('.imageToggle').removeClass('selectedImage');
                vm.loadingBarDrawer = true;
                vm.selectedMachineName = mView;
                $('.stepFolder').slideUp('fast');
                $('.stepBack').slideUp('fast');
                vm.machineSelectedFromList = vm.machineList[vm.selectedMachineName];
                setTimeout(function () {
                    vm.loadingBarDrawer = false;
                    $('.stepFolder1').slideUp('fast');
                    $('.stepLoading').slideUp('fast');
                    $('.stepFolder2').slideDown('fast');
                    $('.stepBack').slideDown('fast');
                }, 300);
                vm.backBtn = 1;
            },
            backToFolderView: function () {
                vm.search = '';
                $('.stepFolder1').slideDown('fast');
                $('.stepFolder').slideDown('fast');
                $('.stepFolder2').slideUp('fast');
                vm.selectedConfirm = 0;
                vm.backBtn = 0;
            }

        };





    }]);