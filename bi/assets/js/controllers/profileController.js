angular.module('app', ['ngSanitize'])
    .controller('profileController', ['$scope', '$sce', 'dataFactory', '$filter', '$stateParams', '$location', '$window', '$rootScope','$cookieStore',
        function ($scope, $sce, dataFactory, $filter, $stateParams, $location, $window, $rootScope,$cookieStore) {
            var vm = $scope;
            var data={};
            var addClass='';
            var viewClass='';
            data['navTitle']="Profile";
            data['icon']="fa fa-user";
            $rootScope.$broadcast('subNavBar', data);
            var userId=$cookieStore.get('userId');
            vm.role={};
            vm.userRole={};
            vm.profile={};
            vm.userProfile={};
            vm.userProfile['name'] = '';

            dataFactory.request($rootScope.Loginuserrole_Url,'post',"").then(function(response){
                vm.loggedUserName = response.data.result[0].roles[0].display_name;
                if(response.data.errorCode==1){
                    vm.roles=response.data.result;
                    $rootScope.roleObj=vm.roles;
                    if(vm.roles[0]["roles"] && vm.roles[0]["roles"][0] && vm.roles[0]["roles"][0]["name"]!=undefined){
                        $rootScope.role=vm.roles[0]["roles"][0]["name"];
                    }
                }
            }).then(function(){
                if(vm.loggedUserName=="Super Admin"){
                    dataFactory.request($rootScope.CompanyProfileget_Url,'post',"").then(function(response){
                        if(response.data.errorCode==1){
                            vm.profile=response.data.result;
                        }else{
                            dataFactory.errorAlert("Check your conection");
                        }
                    });
                }else{
                    dataFactory.request($rootScope.UserProfileget_Url,'post',"").then(function(response){
                        if(response.data.errorCode==1){
                            vm.userProfile=response.data.result;
                        }else{
                            dataFactory.errorAlert("Check your conection");
                        }
                    });
                }

            });


            vm.saveCompanyProfile=function(profile){
                if(profile.company_name==undefined){
                    dataFactory.errorAlert("Company Name is required");
                    return;
                }else if(profile.email==undefined){
                    dataFactory.errorAlert("Email ID is required");
                    return;
                }else if(profile.contact_no==undefined){
                    dataFactory.errorAlert("Conatct Number is required");
                    return;
                }else if(profile.address==undefined){
                    dataFactory.errorAlert("Address is required");
                    return;
                }else if(profile.city==undefined){
                    dataFactory.errorAlert("City is required");
                    return;
                }else if(profile.country==undefined){
                    dataFactory.errorAlert("Country is required");
                    return;
                }

                dataFactory.request($rootScope.CompanyProfileAdd_Url,'post',profile).then(function(response){
                    if(response.data.errorCode==1){
                        dataFactory.successAlert("Profile Saved Sucessfully");
                    }else{
                        dataFactory.errorAlert("Check your coneection");
                    }
                });

            }


            vm.saveUserProfile=function(userProfile){
                if(userProfile.name==undefined){
                    dataFactory.errorAlert("Name is required");
                    return;
                }else if(userProfile.email==undefined){
                    dataFactory.errorAlert("Email ID is required");
                    return;
                }else if(userProfile.contact_no==undefined){
                    dataFactory.errorAlert("Conatct Number is required");
                    return;
                }else if(userProfile.address==undefined){
                    dataFactory.errorAlert("Address is required");
                    return;
                }else if(userProfile.city==undefined){
                    dataFactory.errorAlert("City is required");
                    return;
                }else if(userProfile.country==undefined){
                    dataFactory.errorAlert("Country is required");
                    return;
                }

                dataFactory.request($rootScope.UserProfileAdd_Url,'post',userProfile).then(function(response){
                    if(response.data.errorCode==1){
                        dataFactory.successAlert("Profile Saved Sucessfully");
                    }else{
                        dataFactory.errorAlert("Check your coneection");
                    }
                });

            }

        }]);
