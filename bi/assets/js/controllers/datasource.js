
'use strict';
/* Controllers */
angular.module('app', [ 'ngSanitize' ])
    .controller('datasourceCtrl',['$scope','$sce','dataFactory','$rootScope','$state','$cookieStore','$location', '$window',
        function($scope,$sce, dataFactory,$rootScope,$state,$cookieStore,$location, $window) {
            //--- Top Menu Bar
            //*********************************************Datasource Add************************************************************
            //Current page path
            var data={};
            var addClass='';
            var viewClass='';
            if($location.path()=='/datasource/'){
                viewClass='activeli';
            }else{
                addClass='activeli';
            }
            data['navTitle']="DATASOURCE";
            data['icon']="fa fa-connectdevelop";
            data['navigation']=[
                {
                    "name":"Add",
                    "url":"#/datasource/add",
                    "class":addClass,
                },
                {
                    "name":"View",
                    "url":"#/datasource/",
                    "class":viewClass,
                }
            ];
            $rootScope.$broadcast('subNavBar', data);
            $scope.subNavBarMenu=true;
            //End Menu

            var vm = $scope;
            $scope.dataSourceForm = {};
            $scope.dataSourceFormEdit = {};
            $scope.testButton = 1;
            $scope.checkError = 3;
            $scope.testBtn="Test";
            $scope.loading=false;

            $scope.saveLoading = false;
            $scope.tableName={};
            $scope.databaselist=[];

            //TEst
            //End
            // Model Box start
            $scope.modal = {};
            $scope.modal.slideUp = "default";
            $scope.modal.stickUp = "default";
            // Model Box End
            // Select Box start
            $scope.permissionCheck=function(permission){
                return dataFactory.checkPermission(permission);
            }

            $scope.trustAsHtml = function(value) {
                return $sce.trustAsHtml(value);
            };

            $scope.confirmDatasource=function(){
                $('.step1').slideUp('fast');
                $('.step2').slideDown('fast');
                $('.step3').slideDown('fast');
            };
            $scope.import=function () {
                $scope.datasourceImport={};
                // $scope.selectedConfirm=0;
                // $('.step4').slideUp('fast');
                $('.step4').slideDown('fast');
                $('.step1').slideUp('fast');
                //$('.step3').slideDown('fast');
            }
            // Select box end
            // Datasource Name,image and port
            $scope.datasource = [ {
                "name" : "MySQL",
                "image" : "theme/assets/img/MySQL-Logo.png",
                "port" : 3306
            },{
                "name" : "Mongodb",
                "image" : "theme/assets/img/mongodb.png",
                "port" : 3306
            },{
                "name" : "MsSQL",
                "image" : "theme/assets/img/mssql-server.png",
                "port" : 3306
            },{
                "name" : "Postgres",
                "image" : "theme/assets/img/postgresql.png",
                "port" : 5432
            },{
                "name" : "Excel",
                "image" : "theme/assets/img/excel-logo.png",
                "port" : 3306
            },{
                "name" : "Oracle",
                "image" : "theme/assets/img/oracle-logo.png",
                "port" : 3306
            }/*,{
                "name" : "AWS RDS",
                "image" : "theme/assets/img/aws-rds-logo.png",
                "port" : 3306
            },{
                "name" : "Oracle",
                "image" : "theme/assets/img/oracle-logo.png",
                "port" : 3306
            },{
                "name" : "Sybase",
                "image" : "theme/assets/img/sybase-logo.png",
                "port" : 3306
            }*/];
            //get datasource
            $scope.loadingBarList=true;
            var userId=$cookieStore.get('userId');
            $scope.selectedConfirm=0;
            $scope.toggle=function(e,source){
                setTimeout(function(){
                    $(function () {
                        $('[data-toggle="tooltip"]').tooltip();
                    })
                },100);
                $('.imageToggle').removeClass('selectedImage');
                $scope.selectedDatasource=source;
                $(e.currentTarget).addClass('selectedImage');
                $scope.selectedConfirm=1;

                if(source.name == 'Excel'){
                    $scope.saveButton = 1;
                }else{
                    $scope.saveButton = 0;
                }

            }

            // Datasource Test connect or not
            $scope.testsource = function(dataSourceForm,type) {
                $scope.loading=true;
                if(type!="Excel"){
                    /*
                     * Check validation
                     */
                    if(type=="Oracle"){
                        if(dataSourceForm.serviceType==undefined || dataSourceForm.serviceType==""){
                            $scope.addClassRemove("Select sid or service name");
                            $scope.loading=false;
                        }else if(dataSourceForm.sid==undefined || dataSourceForm.sid==""){
                            $scope.addClassRemove("Enter sid or service name");
                            $scope.loading=false;
                        }
                        dataSourceForm.dbname=dataSourceForm.serviceType+"//@"+dataSourceForm.sid;
                    }

                    if(dataSourceForm.connectionName==undefined || dataSourceForm.connectionName==""){
                        $scope.addClassRemove("Enter datasource name");
                        $scope.loading=false;
                    }else if(dataSourceForm.host==undefined || dataSourceForm.host==""){
                        $scope.addClassRemove("Enter host name");
                        $scope.loading=false;
                    }else if(dataSourceForm.port==undefined || dataSourceForm.port==""){
                        $scope.addClassRemove("Enter port");
                        $scope.loading=false;
                    }else if(dataSourceForm.username==undefined || dataSourceForm.username==""){
                        $scope.addClassRemove("Enter username");
                        $scope.loading=false;
                    }else if(dataSourceForm.password==undefined || dataSourceForm.password==""){
                        $scope.addClassRemove("Enter password");
                        $scope.loading=false;
                    }else{
                        $scope.testBtn="Connecting...";
                        if(!type){
                            type="mysql";
                        }
                        dataSourceForm['datasourceType']=type.toLowerCase();
                        var data={
                            "dataForm":JSON.stringify(dataSourceForm)
                        }
                        /*
                         * Connection check
                         */
                        dataFactory.request($rootScope.dataSourceConnectionCheck_Url,'post',data).then(
                            function(response) {
                                if(response.data.result=="oracle" && response.data.errorCode==1) {
                                    $scope.saveButton = 1;
                                    dataFactory.successAlert(response.data.message);
                                }else if(response.data.errorCode==1){
                                    $scope.databaselist=[];
                                    response.data.result.forEach(function(d) {
                                        $scope.databaselist.push(d);
                                    });
                                    dataFactory.successAlert(response.data.message);
                                    $scope.dbListSelect = 1;
                                    //Save Button
                                    $scope.saveButton=1;
                                }else{
                                    dataFactory.errorAlert(response.data.message);
                                }
                                $scope.testBtn="Test";
                                setTimeout(function(){
                                    $scope.loading=false;
                                    if(!$scope.loading){
                                        $(".loaderImage").fadeOut("slow");
                                    }
                                },1000);
                            });
                    }
                }else{
                    if(dataSourceForm.connectionName==undefined || dataSourceForm.connectionName==""){
                        $scope.addClassRemove("Enter datasource name");
                        $scope.loading=false;
                    }else{
                        $(".loadingBar").show();
                        $scope.testBtn="Connecting...";
                        dataSourceForm['datasourceType']=type.toLowerCase();
                        var allowedFiles=["xls","xlsx","tsv"];
                        var file=dataSourceForm['excelFile'];
                        var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:])+(" + allowedFiles.join('|') + ")$");
                        if (!regex.test(file.name.toLowerCase())) {
                            $scope.addClassRemove("Please upload files having extensions: <b>" + allowedFiles.join(', ') + "</b> only.");
                            return false;
                        }
                        // SEND THE FILES.
                        var url=$rootScope.ExcelSet_Url;
                        dataFactory.dataSourceSetExcel(url,file,dataSourceForm.connectionName).then(
                            function(response) {
                                if(!response.data.errorCode){
                                    $(".loadingBar").hide();
                                    $scope.Attributes=response.data.result;
                                    dataFactory.successAlert(response.data.message);
                                    $scope.saveButton=1;
                                    $scope.testBtn="Test";
                                }else{
                                    $(".loadingBar").hide();
                                    $scope.testBtn="Test";
                                    dataFactory.errorAlert(response.data.message);
                                    //$scope.addClassRemove("Check your connection");
                                }
                                setTimeout(function(){
                                    $scope.loading=false;
                                    if(!$scope.loading){
                                        $(".loaderImage").fadeOut("slow");
                                    }
                                },1000);
                            });

                        // SEND THE FILES.

                        // dataFactory.request($rootScope.excel_url,"post",JSON.stringify(dataSourceForm)).then(
                        // 	function(response) {
                        //
                        //
                        // });
                    }

                }
                $scope.loading=false;
            }

            // Datasource Save details
            $scope.sourceDbName={};
            $scope.save = function() {
                var dataSourceForm=$scope.dataSourceForm;
                var sourceDbName=$scope.sourceDbName;
                // if(dataSourceForm.datasourceType!="excel"){
                if(vm.selectedDatasource.name != "Excel"){
                    if(dataSourceForm.datasourceType=="oracle"){
                        sourceDbName.name=dataSourceForm.dbname;
                    }
                    if (sourceDbName.name == undefined || sourceDbName.name == '') {
                        dataFactory.errorAlert("DataBase Name is required");
                        return;
                    }

                    $scope.saveLoading = true;
                    if (sourceDbName.name) {
                        $scope.dataSourceForm.dbName = sourceDbName.name;
                        var data = {
                            'dataSourceForm': JSON.stringify(dataSourceForm),
                            "reportGroup":$scope.reportGrp,
                            "publicViewType":$scope.publicViewType
                        };

                        dataFactory.request($rootScope.dataSourceSave_Url, 'post', data).then(function (response) {
                            if (response.data.errorCode == 1) {
                                window.location.href = "#/datasource/";
                                dataFactory.successAlert(response.data.message);
                            } else {
                                dataFactory.errorAlert(response.data.message);
                            }
                            setTimeout(function () {
                                $scope.saveLoading = false;
                                if (!$scope.saveLoading) {
                                    $(".loaderImage").hide();
                                    $(".loaderImage").fadeOut("slow");
                                }
                            }, 1000);
                        });
                    } else {
                        dataFactory.errorAlert("select database name");
                    }
                }
                else{
                    var form_data = new FormData($('#LoginValidation')[0]);
                    jQuery.support.cors = true;
                    $.ajax({
                        url: $rootScope.rootIP + ':5000/uploader',
                        type: 'POST',
                        enctype: 'multipart/form-data',
                        processData: false,
                        contentType: false,
                        cache: false,
                        crossOrigin: true,
                        dataType: 'json',
                        data: form_data,
                        success:function(json){
                            var tempObj={};
                            tempObj['Tables']=json.Tables;
                            dataSourceForm['userId']=userId;
                            dataSourceForm['Attributes']=JSON.stringify(tempObj);
                            delete dataSourceForm['excelFile'];
                            var data = {
                                'dataSourceForm': JSON.stringify(dataSourceForm),
                                "reportGroup":$scope.reportGrp,
                                "publicViewType":$scope.publicViewType,
                            };
                            dataFactory.request($rootScope.dataSourceSave_Url, 'post', data).then(function(response) {
                                if (response.data.errorCode){
                                    //$scope.sourcedata.push(response.data.data);
                                    dataFactory.successAlert(response.data.message);
                                } else {
                                    dataFactory.errorAlert(response.data.message);
                                }
                                window.location.href = "#/datasource/";
                                setTimeout(function(){
                                    $scope.saveLoading=false;
                                    if(!$scope.saveLoading){
                                        $(".loaderImage").hide();
                                        $(".loaderImage").fadeOut("slow");
                                    }
                                },1000);
                            });
                        },
                        error: function(error){

                        }
                    });


                }



            }
            //Common function
            $scope.addClassRemove=function(message){
                dataFactory.errorAlert(message);
            }
            //*********************************************Datasource Edit************************************************************
            /*
            * Datasource List
            */
            dataFactory.request($rootScope.datasourceList_Url,'post',data).then(function(response){
                console.log(response.data.result)
                $scope.loadingBarList=false;
                if(response.data.errorCode==1){
                    $scope.sourcedata = response.data.result;
                    var sources = $scope.sourcedata;
                    for (var i = 0; i < sources.length; i++) {
                        var imageUrl = "images/custom/" + sources[i].datasourceType + '.png';
                        sources[i]['imageUrl'] = imageUrl;
                    }
                }else{
                    dataFactory.errorAlert(response.data.message);
                }
            }).then(function(){
                $scope.loadingView = true;
                if($scope.loadingView){
                    $(".loaderImage").fadeOut("slow");
                }
                setTimeout(function(){
                    $('[rel="tooltip"]').tooltip();
                },100);
            });
            //ENd datasource list
            /*$scope.toggleImage=function(e,getSourcedata,index){
                $scope.allDataSourceData = true;
                $('.imageToggle').removeClass('selectedImage');
                getSourcedata['index']=index;
                $scope.selectedDatasourceObject=getSourcedata;
                if($scope.selectedDatasourceObject.datasourceType == 'excel'){
                    $scope.allDataSourceData = false;
                }else{
                    $scope.allDataSourceData = true;
                }
                $(e.currentTarget).addClass('selectedImage');
                $scope.selectedConfirm=1;
            }*/
            $scope.getDatasourceIconPath=function(datasourceType){
                var path=null;
                $scope.datasource.forEach(function(d){
                    if(datasourceType.toLowerCase() == d.name.toLowerCase()){
                        path=d.image;
                    }
                });
                return path;
            }

            //Edit Datasource
            $scope.EditDatasource=function(){
                //$scope.step=2;
                // $('.step1').transition('slide up');
                // $('.step3').transition('slide down');
                $('.step1').slideUp('fast');
                $('.step3').slideDown('fast');
                //	$('.step2').slideDown('fast');
                $scope.sourceDbNameUpdate={};
                $scope.selectedDatasource={};
                $scope.selectedDatasource.name=$scope.selectedDatasourceObject.datasourceType;
                $scope.selectedPath=$scope.getDatasourceIconPath($scope.selectedDatasource.name);
                $scope.dataSourceFormEdit.connectionName=$scope.selectedDatasourceObject.name;
                $scope.dataSourceFormEdit.dbName=$scope.selectedDatasourceObject.dbname;
                $scope.sourceDbNameUpdate.name=$scope.selectedDatasourceObject.dbname;
                if($scope.selectedDatasourceObject.datasourceType=='oracle'){
                    $scope.dataSourceFormEdit.dbname=$scope.selectedDatasourceObject.dbname;
                    var oracleServiceType=$scope.selectedDatasourceObject.dbname.split('//@');
                    $scope.dataSourceFormEdit.serviceType=oracleServiceType[0];
                    $scope.dataSourceFormEdit.sid=oracleServiceType[1];
                }
                $scope.dataSourceFormEdit.host=$scope.selectedDatasourceObject.host;
                $scope.dataSourceFormEdit.username=$scope.selectedDatasourceObject.username;
                $scope.dataSourceFormEdit.password=$scope.selectedDatasourceObject.password;
                $scope.dataSourceFormEdit.port=$scope.selectedDatasourceObject.port;
                $scope.dataSourceFormEdit.datasourceType=$scope.selectedDatasourceObject.datasourceType;
                $scope.dbListSelectEdit=1;
                var data={
                    "dataForm":JSON.stringify($scope.dataSourceFormEdit)
                };
                dataFactory.request($rootScope.dataSourceConnectionCheck_Url,'post',data).then(
                    function(response) {
                        if(response.data.result=="oracle" && response.data.errorCode==1) {
                            //Oracle
                        }else{
                            response.data.result.forEach(function(d) {
                                $scope.databaselist.push(d);
                            });
                        }
                    }).then(function(){
                    $scope.sourceDbName.name=$scope.selectedDatasourceObject.dbname;
                });
            }
            //View Datasource
            $scope.tableNameCheck={};
            $scope.viewDatasource=function(){


//						$('.step1').transition('slide up');
//						$('.step2').transition('slide down');

                $('.step1').slideUp('fast');
                $('.step2').slideDown('fast');



                var datasource=$scope.selectedDatasourceObject;
                $scope.view={};
                $scope.selectedDatasource={};
                $scope.selectedDatasource.name=datasource.datasourceType;
                $scope.selectedPath=$scope.getDatasourceIconPath($scope.selectedDatasource.name);
                $scope.datasourceBackup=datasource;
                $scope.view.connectionname=datasource.name;
                $scope.view.image=$scope.getDatasourceIconPath($scope.selectedDatasource.name);
                $scope.view.host=datasource.host;
                $scope.view.port=datasource.port;
                $scope.view.username=datasource.username;
                $scope.view.password=datasource.password;
                $scope.view.dbname=datasource.dbname;
                $scope.view.id=datasource.id;
                $scope.view.datasourceName=datasource.datasourceType;
            }
            $scope.closeDatasource=function(){

//						$('.step1').transition('slide down');
//						$('.step2').transition('slide up');
                $scope.dataSourceForm={};
                $('.step4').slideUp('fast');
                $('.step2').slideUp('fast');
                $('.step3').slideUp('fast');
                $('.step1').slideDown('fast');

            }
            $scope.closeDatasourceEdit=function(){

//						$('.step1').transition('slide down');
//						$('.step3').transition('slide up');

                $('.step3').slideUp('fast');
                $('.step1').slideDown('fast');

            }
            //End watch
            // Datasource Update details
            $scope.update = function(dataSourceForm,sourceDbName) {
                dataSourceForm['datasource_id']=$scope.selectedDatasourceObject.datasource_id;
                dataSourceForm['userId']=userId;
                if(sourceDbName){
                    dataSourceForm.dbName=sourceDbName.name;
                    var data={
                        "dataForm":JSON.stringify(dataSourceForm)
                    };
                    /*
                     * Update datasource object
                     */
                    dataFactory.request($rootScope.dataSourceEdit_Url,'post',data).then(function(response) {
                        var selectedIndex;

                        if (response.data.errorCode==1){
                            //Successfully
                            $scope.dataSourceSelected.forEach(function(d,index){
                                if(d.id==$scope.selectedDatasourceObject.id){
                                    selectedIndex=index;
                                }
                            });
                            dataFactory.successAlert("Datasource update successfully");
                            $scope.dataSourceSelected[selectedIndex]=response.data.result;


//										$('.step1').transition('slide down');
//										$('.step3').transition('slide up');

                            $('.step3').slideUp('fast');
                            $('.step1').slideDown('fast');
                            $scope.backToSharedView();
                        } else {
                            dataFactory.errorAlert("Check your connection");
                        }
                    });
                }else{
                    dataFactory.errorAlert("select database name");
                }
            }
            //model view and edit end





            //Delete datasource
            $scope.showSwal = function(type){
                if(type == 'warning-message-and-cancel') {
                    swal({
                        title: 'Are you sure?',
                        text: 'You want to delete DataSource !',
                        type: 'warning',
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        cancelButtonText: 'No',
                        confirmButtonClass: "btn btn-success",
                        cancelButtonClass: "btn btn-danger",
                        buttonsStyling: false,
                    }).then(function(value){
                        if(value){
                            $scope.datasourceDelete();
                        }
                    },
                    function (dismiss) {

                    })
                }
            }


            $scope.datasourceDelete=function(){
                var dataSourceForm=$scope.selectedDatasourceObject;
                //$rootScope.dataSourceDelete_Url
                var data={
                    "id":dataSourceForm.datasource_id
                };
                dataFactory.request($rootScope.dataSourceDelete_Url,'post',data).then(function(response) {
                    if(response.data.errorCode==1){
                        var selectedIndex="";
                        $scope.dataSourceSelected.forEach(function(d,index){
                            if(d.datasource_id==dataSourceForm.datasource_id){
                                $scope.dataSourceSelected.splice(index,1);
                                dataFactory.successAlert(response.data.message);
                            }
                        });
                        $scope.selectedConfirm=0;
                        $('.stepFolder2').slideDown('fast');
                        $('.stepFolder1').slideDown('fast');
                        $('.stepFolder3').slideUp('fast');
                    }else{
                        dataFactory.errorAlert(response.data.message);
                    }
                });
            }


            $(".loadingBar").hide();
            $scope.errorMsg = '';
            $scope.tablePrimaryObj={};
            $scope.excelPrimary = function(){
                setTimeout(function () {
                    $scope.tablePrimaryObj={};
                    $scope.tablePrimaryObj.tablePrimaryName='';
                    $scope.$apply();
                },1000);
                var data={
                    "sourceObject":JSON.stringify($scope.selectedDatasourceObject)
                };
                dataFactory.request($rootScope.TableList_Url,'post',data).then(function(response) {
                    if(response.data.errorCode==1){
                        var resp = response.data.result;
                        $scope.tableList = [];
                        resp.forEach(function(d) {
                            $scope.tableList.push(d);
                        });
                    }else{
                        dataFactory.errorAlert(response.data.message);
                    }
                    $scope.metadataInfo1 = true;
                })
                $scope.tablePrimaryObj=[];
                $('#primaryKeyModal').modal({
                    backdrop : 'static',
                    keyboard : false
                });
                $scope.primaryKeyDiv=false;
            }

            $scope.primaryKeyCheck=false;
            $scope.getPrimary=function(tableName){
                if(tableName==0){
                    $scope.primaryKeyDiv=false;
                    return;
                }
                var data={
                    "tableName":tableName,
                    "connObject":JSON.stringify($scope.selectedDatasourceObject)
                };
                dataFactory.request($rootScope.TableColumnsInfo_Url,'post',data).then(function(response) {
                    if(response.data.errorCode==1){
                        $scope.tablePrimaryColumn = response.data.result;
                    }
                }).then(function(){
                    dataFactory.request($rootScope.TablePrimary_Url,'post',data).then(function(response) {
                        if(response.data.errorCode==1){
                            $scope.primaryKeyDiv=true;
                            $scope.primaryKeyCheck=true;

                            var selectedPrimary = response.data.result;
                            var selectedPrimaryArr = [];
                            selectedPrimary.forEach(function(d){
                                selectedPrimaryArr.push(d.Column_name);
                            });
                            var primaryData = $scope.tablePrimaryColumn;
                            var primaryDataArr = [];
                            primaryData.forEach(function(d){
                                if(selectedPrimaryArr.indexOf(d.Field) != -1){
                                    primaryDataArr.push(d.Field);
                                }
                            });
                            $scope.tablePrimaryObj.column = primaryDataArr;
                            if(primaryDataArr.length){
                                $scope.primaryKeyCheck=true;
                                $scope.primaryKeySaveBtn= false;
                            }else{
                                $scope.primaryKeyCheck=false;
                                $scope.primaryKeySaveBtn= true;
                            }
                        }
                        setTimeout(function() {
                            $(".columnPrimarySelect").selectpicker();
                        },300);
                    })
                });
            }



            $scope.tablePrimaryCreate = function(tablePrimaryObj){
                if(tablePrimaryObj.column.length==0){
                    $scope.errorMsg = "Please Select Primary Key";
                    // dataFactory.errorAlert("Please select Primary Key");
                    return;
                }
                $scope.errorMsg = "";
                var data={
                    "columns":(tablePrimaryObj.column).join(','),
                    "connObject":JSON.stringify($scope.selectedDatasourceObject),
                    "tableName":tablePrimaryObj.tablePrimaryName
                };
                dataFactory.request($rootScope.TablePrimaryCreate_Url,'post',data).then(function(response) {
                    if(response.data.errorCode==1){
                        $scope.primaryKeyCheck = true;
                        $scope.primaryKeySaveBtn = false;
                        dataFactory.successAlert("Primary Key Created Successfully");
                        //  $('#primaryKeyModal').modal('hide');
                    }
                });
            }


            $scope.deletePrimary = function(tablePrimaryObj){
                if(tablePrimaryObj.tablePrimaryName==0){
                    $scope.primaryKeyDiv=false;
                    return;
                }
                var data={
                    "connObject":JSON.stringify($scope.selectedDatasourceObject),
                    "tableName":tablePrimaryObj.tablePrimaryName
                };
                dataFactory.request($rootScope.TablePrimaryDelete_Url,'post',data).then(function(response) {
                    if(response.data.errorCode==1){
                        $scope.errorMsg = '';
                        $scope.primaryKeyCheck = false;
                        $scope.primaryKeySaveBtn = true;

                        $scope.tablePrimaryObj.column = [];
                        $("#SelectColumn_Primary").selectpicker('destroy');
                        dataFactory.successAlert("Primary Key deleted successfully");
                        setTimeout(function() {
                            $(".columnPrimarySelect").selectpicker();
                        },300);
                    }
                });
            }


            $scope.test_Save_Excel = function(){
                var tableName = $scope.tablePrimaryObj.tablePrimaryName;
                var primaryKey = vm.tablePrimaryObj.column;
                var form_data = new FormData($('#upload-file')[0]);
                form_data.append('ids', primaryKey);
                form_data.append('tablename',tableName);
                $.ajax({
                    url: $rootScope.rootIP + ':5000/append',
                    type: 'POST',
                    enctype: 'multipart/form-data',
                    processData: false,
                    contentType: false,
                    cache: false,
                    dataType: 'json',
                    data: form_data,
                    success:function(json){
                        $('#primaryKeyModal').modal('hide');
                        dataFactory.successAlert('Excel Appended Successfully');
                        var data = {
                            'datasourceId' : $scope.selectedDatasourceObject.datasource_id
                        }
                        dataFactory.request($rootScope.AppendExcelLoad_Url,'post',data).then(function(response) {

                        });
                    },
                    error: function(error){

                    }
                });

                // $scope.errorMsg = '';
                // var allowedFiles=["xls","xlsx","tsv"];
                // var file=$scope.tablePrimaryObj['file'];
                // if(file == undefined){
                //     $scope.errorMsg = "Please Upload File";
                //     return;
                // }
                // var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:])+(" + allowedFiles.join('|') + ")$");
                // if (!regex.test(file.name.toLowerCase())) {
                //     $scope.addClassRemove("Please upload files having extensions: <b>" + allowedFiles.join(', ') + "</b> only.");
                //     return false;
                // }
                // var url = $rootScope.AppendExcelLoad_Url;
                // var connectionName = '';
                // var tableName=$scope.tablePrimaryObj.tablePrimaryName;
                // dataFactory.dataSourceSetExcel(url,file,connectionName,tableName,$scope.selectedDatasourceObject.datasource_id).then(function(response) {
                //     if(response.data.errorCode==1){
                //         $('#primaryKeyModal').modal('hide');
                //         dataFactory.successAlert(response.data.message);
                //     }else{
                //         $scope.errorMsg = response.data.message;
                //     }
                // });
            }
            /*
            Report grp
             */
            /*
             * Datasource list
             */

            $scope.init=function(){
                dataFactory.request($rootScope.datasourceList_Url,'post',data).then(function(response) {
                    $scope.loadingBarList=false;
                    if(response.data.errorCode==1){
                        $scope.groupFolder = Object.keys(response.data.result);
                        $scope.dataSourceList=response.data.result;
                        /*
                         $scope.sourcedata=response.data.result;
                         var sources = $scope.sourcedata;
                         for (var i = 0; i < sources.length; i++) {
                         var imageUrl = "images/custom/" + sources[i].datasourceType + '.png';
                         sources[i]['imageUrl'] = imageUrl;
                         }*/
                    }else{
                        dataFactory.errorAlert(response.data.message);
                    }

                }).then(function(){
                    $scope.loadingView = true;
                    if($scope.loadingView){
                        $(".loaderImage").fadeOut("slow");
                    }
                    setTimeout(function(){
                        $('[rel="tooltip"]').tooltip();
                    },100);
                });
            }

            $scope.init();
            $scope.backBtn=0;
            vm.grpFolder = false;
            vm.selectedGrpFolder = function(){
                vm.groupFolderList = Object.keys(vm.sourcedata[vm.sDepartment]);
                $scope.backBtn = 1;
                setTimeout(function () {
                    $('[rel="tooltip"]').tooltip();
                    $scope.loadingBarDrawer = false;
                    $('.stepFolder1').slideUp('fast');
                    $('.stepFolder2').slideUp('fast');
                    $('.stepLoading').slideUp('fast');
                    $('.stepFolder4').slideDown('fast');
                }, 300);
            }

            vm.loadDataSourceList = function(){
                $('.stepFolder1').slideUp('fast');
                $('.stepFolder4').slideUp('fast');
                $('.stepBack').slideUp('fast');
                $('.imageToggle').removeClass('selectedImage');
                setTimeout(function(){
                    $(function () {
                        $('[data-toggle="tooltip"]').tooltip();
                    })
                },100);
                setTimeout(function(){
                    $scope.loadingBarDrawer = false;
                    $('.stepFolder2').slideUp('fast');
                    $('.stepLoading').slideUp('fast');
                    $('.stepFolder3').slideDown('fast');
                    $('.stepBack').slideDown('fast');
                }, 300);
            }

            vm.toggleGrpFolder = function(e, sView, index){
                vm.selectedSharedViewName = sView;
                vm.dataSourceSelected = vm.sourcedata[vm.sDepartment][sView];
                vm.loadDataSourceList();
            }

            $scope.toggleImage=function(e,sView,index){
                if(sView == 'Public View'){
                    $scope.backBtn=1;
                    $scope.selectedConfirm=0;
                    $scope.loadingBarDrawer = true;
                    $scope.selectedSharedViewName = sView;
                    $scope.dataSourceSelected = $scope.dataSourceList[$scope.selectedSharedViewName];
                    vm.loadDataSourceList();
                }else{
                    vm.grpFolder = true;
                    vm.sDepartment = sView;
                    vm.selectedGrpFolder();
                }
            }

            $scope.backToSharedView = function(){
                $scope.search='';
                $scope.backBtn=0;
                $('.stepFolder2').slideDown('fast');
                $('.stepFolder1').slideDown('fast');
                $('.stepFolder3').slideUp('fast');
                $scope.selectedConfirm=0;
            }
            //ENd datasource list
            $scope.toggleImageSource=function(e,getSourcedata,index){
                setTimeout(function(){
                    $(function () {
                        $('[data-toggle="tooltip"]').tooltip();
                    })
                },100);
                $scope.allDataSourceData = true;

                $('.imageToggle').removeClass('selectedImage');
                getSourcedata['index']=index;
                $scope.selectedDatasourceObject=getSourcedata;

                if($scope.selectedDatasourceObject.datasourceType == 'excel'){
                    $scope.allDataSourceData = false;
                }else{
                    $scope.allDataSourceData = true;
                }
                $(e.currentTarget).addClass('selectedImage');
                $scope.selectedConfirm=1;
            }
            $scope.checkView=function(page){
                $scope.publicViewType=page;
                if(page=='public_group'){
                    vm.backBtnGrp = false;
                    $(".subDiv").hide();
                    $(".footerDiv").show();
                }else{
                    vm.backBtnGrp = true;
                    $(".sGrp").show();
                    $(".footerDiv").show();
                    $(".reportGroupSelect").selectpicker();
                    $(".userGroupSelect").selectpicker();
                }
            }

            vm.backBtnGrp = false;
            vm.grpType = 'selectGrp';
            vm.groupTypeValue = '';
            vm.checkGrp = function(type){
                vm.groupTypeValue = type;
                $scope.reportGrp={};
                $(".reportGroupSelect").selectpicker();
                $(".userGroupSelect").selectpicker();
            }
            /*
            Grp select model
            */
            // $scope.publicViewType="specific_group";
            vm.publicViewType="public_group";
            $scope.reportGrp={};
            dataFactory.request($rootScope.UsergroupList_Url,'post',"").then(function(response){
                $scope.userGroup=response.data.result;
            });
            // dataFactory.request($rootScope.ReportGroupList_Url,'post',"").then(function(response){
            //     $scope.reportGroup=response.data.result;
            // });
            $scope.selectGroup = function(divClass){
                if(divClass=='back'){
                    vm.publicViewType = "public_group";
                    vm.backBtnGrp = false;
                    $(".mainDiv").slideDown('fast');
                    $(".select").slideUp('fast');
                    $(".create").slideUp('fast');
                    $(".sGrp").hide();
                    $("[name='optionsRadios'][value=public_group]").prop('checked', true);
                    $(".footerDiv").show();
                }else{
                    $("."+divClass).slideDown('fast');
                    $(".footerDiv").slideDown('fast');
                    $(".mainDiv").slideUp('fast');
                }
                if(divClass=='select' || divClass=='create'){
                    $scope.reportGrp={};
                }
            }

            vm.report_Grp = false;
            vm.userGrpAdd = function(){
                vm.report_Grp = true;
                var data = {
                    'usrGrp' : vm.reportGrp.user_Group,
                }
                dataFactory.request($rootScope.ReportGroupList_Url,'post',data).then(function(response){
                    $scope.reportGroup = response.data.result;
                    $scope.$apply();
                    $(".reportGroupSelect").selectpicker('refresh');
                });
            }

            /*
            Save datasource
             */
            $scope.groupTypeModal=function(dataSourceForm,sourceDbName){
                $scope.dataSourceForm=dataSourceForm;
                $scope.sourceDbName=sourceDbName;
                $(".userGroupSelect").selectpicker();
                $('#reportGroup').modal('show');
            }

            $(".modal-backdrop").hide();
            $scope.publicViewType=='public_group';
            $scope.groupTypeSave=function(reportGrp){
                if(vm.groupTypeValue.length == 0 || vm.groupTypeValue == ''){
                    vm.groupTypeValue = 'selectGrp';
                }
                reportGrp.grpType = vm.groupTypeValue;
                console.log(reportGrp)
                if(Object.keys($scope.reportGrp).length==0 && $scope.publicViewType!='public_group'){
                    dataFactory.errorAlert("Select group");
                }else{
                    if(reportGrp.name && !reportGrp.userGroup){
                        dataFactory.errorAlert("Select report group");
                        return;
                    }
                    $scope.reportGrp = reportGrp;
                    $('#reportGroup').modal('hide');
                    $scope.save();
                }
            }
            // sort datasource list asc-desc Start
            $scope.dataSourceList_Order = 'desc';
            $scope.dataSource_List_order = '-id';
            $scope.orderDataSourceList = function(){
                if($scope.dataSourceList_Order == 'desc'){
                    $scope.dataSource_List_order = '-id';
                }
                else if($scope.dataSourceList_Order == 'asc'){
                    $scope.dataSource_List_order = 'id';
                }
            }
// sort datasource list asc-desc End



// sort datasource add asc-desc Start
            $scope.dataSourceAdd_Order = 'asc';
            $scope.dataSource_Add_order = 'name';
            $scope.orderDataSourceAdd = function(){
                if($scope.dataSourceAdd_Order == 'desc'){
                    $scope.dataSource_Add_order = '-name';
                }
                else if($scope.dataSourceAdd_Order == 'asc'){
                    $scope.dataSource_Add_order = 'name';
                }
            }
// sort datasource add asc-desc End
            /*
             * Export and import
             */
            $scope.export=function () {
                var dataSourceForm=$scope.selectedDatasourceObject;
                var data={
                    "datasourceId":dataSourceForm.datasource_id
                };
                dataFactory.request($rootScope.datasourceExport_Url,'post',data).then(function(response) {
                    if(response.data.errorCode){
                        var link=document.createElement('a');
                        link.href=response.data.result.url;
                        link.download=response.data.result.fileName;
                        link.click();
                        var data={
                            "url":response.data.result.url
                        };
                        dataFactory.request($rootScope.ExportFileDelete_Url,'post',data);
                    }
                })
            }
            $scope.testImport=function (datasourceImport,type) {
                $(".loaderImage").fadeIn("slow");
                var file=$scope.datasourceImport['file'];
                var allowedFiles=["think"];
                var file=$scope.datasourceImport.file;
                var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:])+(" + allowedFiles.join('|') + ")$");
                if (!regex.test(file.name.toLowerCase())) {
                    $scope.addClassRemove("Please upload files having extensions: <b>" + allowedFiles.join(', ') + "</b> only.");
                    return false;
                }
                $scope.testBtn="Loading...";
                var url=$rootScope.datasourceImport_Url;
                dataFactory.importData(url,file,type).then(function(response) {
                    if(response.data.errorCode){
                        $(".loadingBar").hide();
                        $scope.Attributes=response.data.result;
                        dataFactory.errorAlert(response.data.message);
                        $scope.saveButton=1;
                        $scope.testBtn="Test";
                    }else{
                        dataFactory.errorAlert(response.data.message);
                    }
                    setTimeout(function(){
                        $scope.loading=false;
                        if(!$scope.loading){
                            $(".loaderImage").fadeOut("slow");
                        }
                    },1000);
                });
            }
            $scope.saveImport=function () {
                $(".loaderImage").show();
                var file=$scope.datasourceImport['file'];
                var allowedFiles=["think"];
                var file=$scope.datasourceImport.file;
                var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:])+(" + allowedFiles.join('|') + ")$");
                if (!regex.test(file.name.toLowerCase())) {
                    $scope.addClassRemove("Please upload files having extensions: <b>" + allowedFiles.join(', ') + "</b> only.");
                    return false;
                }
                var url=$rootScope.datasourceImportSave_Url;
                dataFactory.importDataSave(url,file,$scope.type,$scope.reportGrp,$scope.publicViewType).then(function(response) {
                    if(response.data.errorCode){
                        $('.imageToggle').removeClass('selectedImage');
                        $('.step4').slideUp('fast');
                        $('.step1').slideDown('fast');
                        $scope.backToSharedView();
                        dataFactory.successAlert(response.data.message);
                    }else{
                        dataFactory.errorAlert(response.data.message);
                    }
                });
                $('#reportGroupImport').modal('hide');
                $window.location = '#/datasource/';
            }
            $scope.saveImportGroup=function (datasourceImport,type) {
                $('#reportGroupImport').modal('show');
                $scope.type=type;
            }
        } ]);




window.onerror = function(e, url, line) {

}

 