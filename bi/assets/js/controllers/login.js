'use strict';
/* Controllers */
angular.module('app').controller('loginController', ['$scope','$interval','$location','$cookieStore','AuthenticationService','dataFactory','$rootScope','$state','$http', function($scope,$interval,$location,$cookieStore,AuthenticationService,dataFactory,$rootScope,$state,$http) {
    //AuthenticationService.ClearCredentials();
    //$cookieStore.remove('accessToken');
    //$cookieStore.remove('userId');
    //$cookieStore.remove('accessPage');
    //$cookieStore.remove('companyId');
    //$cookieStore.remove('userPort');
    $scope.user={};
    $scope.loginCheck = function (form) {
        $(".loader-overlay").show();
        $scope.signUpRedirect=function(){
            $location.path('/singUp');
        }
        var data={
            "email":form.username,
            "password":form.password
        };
        dataFactory.request($rootScope.Login_Url,"post",data).then(function(response){
            if(response.errors!=undefined){
                dataFactory.errorAlert(response.data.result.message);
            }else if(response.data.errorCode){
                AuthenticationService.SetCredentials(response);
                //Check url condition
                $cookieStore.put("userPort", response.data.result.port);
                $cookieStore.put("companyId", response.data.result.company_id);
                if($cookieStore.get('accessPage')){
                    var locationUrl=$cookieStore.get('accessPage').split("/");
                    var urlAllow="";
                    if(locationUrl[1]=="sharedView" && (locationUrl[2]=="show" || locationUrl[2]=="mobileView"))
                        urlAllow="/"+locationUrl[1]+"/"+locationUrl[2];
                    if(urlAllow!=""){
                        var data={
                            "auth_key":locationUrl[3]
                        };
                        dataFactory.request($rootScope.CheckPermission_Url,'post',data).then(function(response){
                            if(response.data.errorCode){
                                $location.path($cookieStore.get('accessPage'));
                                setTimeout(function(){
                                    window.location.reload();
                                },100);
                            }else{
                                $scope.roleCheck();
                            }
                        });
                    }else{
                        $scope.roleCheck();
                    }
                }else{
                    $scope.roleCheck();
                }
            }else{
                dataFactory.errorAlert(response.data.result.email);
            }
            $(".loader-overlay").hide();
        });
    };
    $scope.roleCheck=function(){
        var url;
        var config = {
            headers:{
                'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;',
            }
        };
        var permission="";
        dataFactory.request($rootScope.RoleGet_Url,'post',"").then(function(response) {
            if(response.data.errorCode==2){
                url = response.data.result;
            }else{
                $state.get().forEach(function (d) {
                    if (d.permission == response.data.result[0].landing_page) {
                        permission = d.permission.replace(/\s/g, "");
                        url = d.name;
                    }
                });
            }
            $state.go(url, {}, {reload: true}).then(function() {
                setTimeout(function () {
                    window.location.reload();
                },200);
            });
        });
    }
}]);
