'use strict';
/* Controllers */
angular.module('app', ['ngSanitize'])
    .controller('emailController', ['$scope', '$sce', 'dataFactory', '$filter', '$stateParams', '$location', '$window', '$rootScope','$cookieStore',
        function ($scope, $sce, dataFactory, $filter, $stateParams, $location, $window, $rootScope,$cookieStore) {
            var vm=$scope;
            var data={};
            var addClass='';
            var viewClass='';
            if($location.path()=='/setting/'){
                viewClass='activeli';
            }else{
                addClass='activeli';
            }
            data['navTitle']="EMAIL";
            data['icon']="fa fa-user";
            data['navigation']=[{
                "name":"Add",
                "url":"#/metadata/add",
                "class":addClass
            },
                {
                    "name":"View",
                    "url":"#/metadata/",
                    "class":viewClass
                }];
            $rootScope.$broadcast('subNavBar', data);
            var userId=$cookieStore.get('userId');
            /*
             * Datetime picker
             */


            // Send Email Start
            dataFactory.request($rootScope.sharedViewList_Url,'post',"").then(function(response){
                vm.sharedviewList = response.data.result;
            }).then(function(){
                setTimeout(function(){
                    $("#sharedViewSelect").selectpicker();
                },100);
            });
            $('.datetime').datetimepicker({
                defaultDate: new Date(),
                format : "DD-MM-YYYY HH:mm:ss"
            }).on('dp.change', function (e) {
                var formatedValue = e.date.format(e.date._f);
                vm.mail.date=formatedValue;
            });
            vm.mail = {};
            vm.mail.date="";
            vm.send_Mail = function(mail){
                if(Array.isArray(mail.emailId)){
                    var emailStr="";
                    mail.emailId.forEach(function (d) {
                        emailStr +=d+",";
                    })
                    emailStr=emailStr.replace(/,\s*$/, "");
                    mail.emailId=emailStr;
                }
                var data={
                    "mailObj":mail
                };
                dataFactory.request($rootScope.SendEmail_Url,'post',data).then(function(response){
                    dataFactory.successAlert(response.data.message);
                    $window.location = '#/setting/email_report';
                });
            }
            // Send Email End
            /*
             * Group type select
             */
            vm.emailTypeSelect=true;
            vm.emailSelectType=function(emailType){
                vm.mail.emailId="";
                if(emailType=="specific"){
                    // All Users
                    vm.emailTypeGrp=false;
                    vm.emailTypeSelect=true;

                }else if(emailType=="group"){
                    //Group
                    vm.emailTypeSelect=false;
                    vm.emailTypeGrp=true;
                    dataFactory.request($rootScope.EmailGrpList_Url,'post',"").then(function(response){
                        if(response.data.errorCode==1){
                            vm.emailGrpList=response.data.result;
                            setTimeout(function(){
                                $("#emailIdGrp").selectpicker('destroy');
                                $("#emailIdGrp").selectpicker();
                                vm.emailTypeGrp=true;
                                $scope.$apply();
                            },1000);
                        }
                    });
                }
            }
        }]);