angular.module('app', ['ngSanitize'])
    .controller('changePasswordController', ['$scope', '$sce', 'dataFactory', '$filter', '$stateParams', '$location', '$window', '$rootScope','$cookieStore',
        function ($scope, $sce, dataFactory, $filter, $stateParams, $location, $window, $rootScope,$cookieStore) {
    	var vm=$scope;
    	var vm=$scope; 
    	var data={};
		var addClass='';
		var viewClass='';
		data['navTitle']="Change Password";
		data['icon']="fa fa-user";  
		$rootScope.$broadcast('subNavBar', data);
        var userId=$cookieStore.get('userId');
        
    	vm.userPassword = {};
    	
//      $scope.id = $stateParams.id;        
      	
    	
    	
    	
    	
    	
// save    	
  	vm.save = function(userPassword){    		
  		var passwordRegex = /(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%&*()]).{6,}/;
  		
  		if(userPassword.password == undefined){
  			dataFactory.errorAlert("Password is required");
  			return;
  		}else if(userPassword.cPassword == undefined){
  			dataFactory.errorAlert("Confirm Password is required");
  			return;
  		}else if(userPassword.password != userPassword.cPassword ){
  			dataFactory.errorAlert("Password & Confirm Password should be same ");    
  			return;
  		}else if( !passwordRegex.test(userPassword.cPassword) ) {
  			dataFactory.errorAlert("Password must contains 1 Upper Case, 1 Lower Case, 1 Special Character & must be 6 characters long ");    
  			return;  			
        }  		
  		var data={
  			password:userPassword.cPassword
  		};
  		
  		dataFactory.request($rootScope.changePassword_Url,'post',data).then(function(response){
  			if(response.data.errorCode==1){
  				dataFactory.successAlert(response.data.message);
  				$window.location = '#/Login';
  			}else{
  				dataFactory.errorAlert(response.data.message);
  			}
  		});
  		
  		
  		
  	}
  	

    	
    	
    	
    }]);