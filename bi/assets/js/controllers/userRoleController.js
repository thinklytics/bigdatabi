angular.module('app', ['ngSanitize', 'gridster'])
    .controller('userRoleController', ['$scope', '$sce', 'dataFactory', '$filter', '$stateParams', '$location', '$window', '$rootScope','$cookieStore',
        function ($scope, $sce, dataFactory, $filter, $stateParams, $location, $window, $rootScope,$cookieStore) {
    	var vm=$scope;
    	var vm=$scope; 
    	var data={};
		var addClass='';
		var viewClass='';
		data['navTitle']="User Role";
		data['icon']="fa fa-database";  
		$rootScope.$broadcast('subNavBar', data);
        var userId=$cookieStore.get('userId');
        vm.role={}; 
    	vm.userRole={};
    	/*
    	 * Get Role List
    	 */
		dataFactory.request($rootScope.Rolelist_Url,'post',"").then(function(response){
			if(response.data.errorCode==1)
				vm.roleList=response.data.result;
			else
				dataFactory.errorAlert("Check your connection");
		});
    	/*
    	 * Save role and Permission 
    	 */
    	vm.save=function(role){
    		if(role.name==undefined || role.description==undefined){
    			dataFactory.errorAlert("Role name and role description required");
    			return;
    		}else if(vm.selectedPage.length==0){
    			dataFactory.errorAlert("Select min one permission");
    			return;
    		}
    		var data={
				name:role.name,
				description:role.description,
				pages:vm.selectedPage
    		};
    		dataFactory.request($rootScope.Role_Url,'post',data).then(function(response){
    			if(response.data.errorCode==1){
    				dataFactory.successAlert("submited successfully");
    			}else{
    				dataFactory.errorAlert(response.data.message);
    			}
    		});
    	}
    	/*
    	 * Permission api
    	 */
    	var list = dataFactory.request($rootScope.Permissionlist_Url,"post","").then(function(response){
    		if(response.data.errorCode==1)
    			vm.permissionList=response.data.result;
    		else
    			dataFactory.errorAlert(response.data.message);
    	});
    	/*
    	 * Check selected permission
    	 */
    	vm.selectedPage=[];
    	vm.checkObject=function(id){
    		var index = vm.selectedPage.indexOf(id);
    		if(index==-1){
    			vm.selectedPage.push(id);
    		}else{
    			vm.selectedPage.splice(index,1);
    		}
    	}
    }]);