'use strict'; 
/* Controllers */  
angular.module('app').controller('signupController', ['$scope','$interval','$location','$cookieStore','AuthenticationService','dataFactory','$rootScope', function($scope,$interval,$location,$cookieStore,AuthenticationService,dataFactory,$rootScope) {
		AuthenticationService.ClearCredentials();
		$scope.user={};
		
        $(".navbar").css('display', 'none');
//        $(".navbar navbar-default navbar-absolute").css('display', 'none');
		$( document ).ready(function() {
		   $('body').addClass('page-login-v2 layout-full page-dark');
		});
		$scope.signInRedirect=function(){
			$location.path('/Login');
		}
		
       $scope.signUp = function (user) {
    	    $(".loader-overlay").show();
	       	if(user && user.cpassword!=user.password){
	       		dataFactory.errorAlert("Pasword mismatch");
	       		$(".loader-overlay").hide();
	       		return;
	       	}else if(user && user.password.length<6){
	       		dataFactory.errorAlert("Create a password at least 6 characters long.");
	       		$(".loader-overlay").hide();
	       		return;
	       	}
	       	var data={
	       			"name":user.firstname,
	       			"email":user.email,
	       			"password":user.password
	       	};
	       	dataFactory.request($rootScope.Signup_Url,"post",data).then(function(response){
	     		if(response.errors!=undefined){
	     			dataFactory.errorAlert(response.data.result.message);
	     		}else if(response.data.errorCode){
	     			$location.path('/login');
		            window.location.reload();
	     		}else{
	     			dataFactory.errorAlert(JSON.stringify(response.data.result));
	     		}
	     		$(".loader-overlay").hide();
	     	});
       };
}]); 
