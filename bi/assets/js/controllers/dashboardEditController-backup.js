'use strict';

/* Controllers */
angular.module('app',[ 'ngSanitize', 'ui.select', 'gridster', 'daterangepicker' ])
.controller('dashboardEditController',['$scope','$sce','dataFactory','$filter','$location','$window','$q','$stateParams','$rootScope',
        function($scope, $sce, dataFactory, $filter,$location,$window,$q,
                 $stateParams,$rootScope){
    	//--- Top Menu Bar
        var data={}; 
        data['navTitle']="Dashboard";
        data['navIcon']="fa fa-bar-chart";
        $rootScope.$broadcast('navBar', data);
        //End Menu
        //Default Tab 
		$scope.tab=false;
		//End
            $scope.dashboardId=$stateParams.data;
            // Select Box start
            $scope.trustAsHtml = function(value) {
                return $sce.trustAsHtml(value);
            };
            var vm = $scope;
           
            vm.selectedChart = {};
            vm.customGroup = [];
            vm.activeXLabel = {};
            vm.activeYLabel = {};
            vm.aggregateText = {};
            vm.showInfo2 = true;
            vm.showInfo1 = true; 
            $scope.columnSearch = "";
            $(".full-height").addClass("full-height-background");
            // vm.AggregateFunctions=[];
            vm.tableColumns = [];
            $scope.dashName = {};
            $scope.lineBar = {};
            $scope.lastDrawnChart = {};
            $scope.selectedChartInView = {};

            $scope.$watch("columnSearch", function(model) {



                }

            );
            vm.colorChange=function(color){
            	eChart.addColor(color);
             }
            $scope.toolTipFormate="{xLabel}:{yLabel}"; //tooltip Formate
            vm.colorPallete=["#810040" ,"#DE3B00" ,"#FF8700" ,"#FFB500" ,"#B9E52F" ,"#2CAD00" ,"#30CB71" ,"#33AEB1","#A8577B" ,"#EE94C9" ,"#F6AA54" ,"#F7E65A" ,"#B6F5AC" ,"#25F1AA" ,"#C6F0D7" ,"#A7DBE2","#219FD7" ,"#0251D3" ,"#863CFF" ,"#E436BB" ,"#EA5081" ,"#8B5011" ,"#000000" ,"#AAAAAA","#9FDEF1" ,"#ED90C7" ,"#6197E2" ,"#C2C4E3" ,"#DFC0ED" ,"#DDCFA4","#2A5D78" ,"#EEEEEE"];
            vm.rowsLength=function(){
            	var length=vm.colorPallete.length;
            	var times=length/8;
            	
            	if(length%8==0)
            		 times=length/8;
            	else
            		times=(length/8)+1;
            	
            	var arr=[];
            	for(var i=0;i<times;i++){
            		arr.push(i);
            		
            	}
            	return arr;
            }
            vm.getColorPallete=function(val){
            	return vm.colorPallete.slice(val*8,val*8+8);
            }
           
            vm.applyFilter=function(){
            	
            	eChart.applyFilter();
            	$(".colorPalette").hide();
            }
            
            //Sort By
            vm.sortBy=function(type){
            
            	eChart.sortData(type);
            }
            //Reset Filter
            vm.resetFilter=function(){
            	eChart.resetAll(); 
            }
            vm.openSettingsWindow = function(type) {
                vm.curPlace = "";
                if (type == "Measure") {
                    $scope.measureLi = true;
                    $scope.dimensionLi = false;
                    $scope.typeSelectMD = "measure";
                } else {
                    $scope.measureLi = false;
                    $scope.dimensionLi = true;
                    $scope.typeSelectMD = "category";
                }
                vm.addCategoryContainer = function() {
                    $scope.categoryContainers.push({});
                    $('.multielect1').multiselect({
                        includeSelectAllOption : true,
                        enableFiltering : true,
                        maxHeight : 200
                    });
                }
                vm.removeCategories = function(index) {
                    vm.categoryContainers.splice(index, 1);
                }

                vm.columnTypekeysArray = vm.tableColumns
                    .filter(function(d) {

                        if (d.type.indexOf(type) != -1) {

                            return true;
                        }
                        return false;
                    });
                
                vm.applyFilter=function(){
              
                	eChart.applyFilter();
                	$(".colorPalette").hide();
                }
                
                
               
                vm.calMeasureShow = [];
                vm.calMeasureDb = [];
                vm.measureAdd = function() {
                    // Formula in array
                    var expression = $scope.measureFormula;
                    var objectNewKeys = {
                        columType : "int",
                        columnName : $scope.measureName,
                        dataKey : "Measure",
                        tableName : "",
                        formula : $scope.measureFormula
                    }
                    $scope.updatebtn = true;
                    $scope.calMeasureFormula
                        .push(objectNewKeys);
                    $scope.tableColumns.push(objectNewKeys);
                    $scope.measureName = "";
                    $scope.measureFormula = "";
                    generate('success',
                        'Measure create successfully');
                    $scope.lastColumnShow = true;
                }
                $scope.measureFormula = "";

                $scope.insertText = function(elemID, text,
                                             tableName) {
                    if (tableName) {
                        $scope.measureFormula += tableName
                            + "." + text;
                    } else {
                        $scope.measureFormula += text;
                    }
                }
                
                $scope.insertText1 = function(elemID, text,
                                              tableName) {
                    if (tableName) {
                        $scope.measureFormula += tableName
                            + "." + text;
                    } else {
                        $scope.measureFormula += text;
                    }
                }

                // vm.categories=[];
            };
            // Next Tab Select
            vm.nextTab = function(columnType) {

                vm.selectDimension = columnType;
                var dataKey = columnType;
                var tempData = [];
                Enumerable.From(vm.tableData).Distinct("$." + dataKey).Select("$." + dataKey).ToArray().forEach(function(e) {
                    tempData.push(e);
                });
                vm.dataItems = tempData;
            }
            
            //Pivot table
            $scope.pivotTable=function(){
            	$('#pivot-table').modal({
                    backdrop : 'static',
                    keyboard : false
                });
                $('#pivot-table').modal('show');
            }
            // Next Tab Select
            vm.nextTabValidation = function(columnType) {

                vm.selectDimension = columnType;
                vm.measureNameValidation = columnType+ "WithValidation";
                vm.lastColumSelected = columnType;
                var dataKey = columnType;
                var tempData = [];
                Enumerable.From(vm.tableData).Distinct("$." + dataKey).Select("$." + dataKey)
                    .ToArray().forEach(function(e) {
                    tempData.push(e);
                });
                vm.dataItems = tempData;
            }
            // back Tab
            vm.backTab = function() {

            }
            vm.dimensionCategoryChange = function() {
                var dataKey = vm.categories.column;

                var tempData = [];
                Enumerable.From(sketch.getData()).Distinct(
                    "$." + dataKey).Select("$." + dataKey)
                    .ToArray().forEach(function(e) {
                    tempData.push(e);
                });
                vm.dataItems = tempData;
            }

            // advanced chart settings
            vm.advancedChartSettingModal = {
                dateFormats : sketch._dateFormats.formats,
                groups : sketch._dateFormats.groups,
                Categories : [ {} ]
            }

            vm.advancedChartSettingController = {
                init : function() {
                    vm.advancedChartSettingView.init();
                    vm.advancedChartSettingView.render();
                },
                fetchDateFormats : function() {

                    return vm.advancedChartSettingModal.dateFormats;

                },
                fetchGroupFormats : function() {

                    return vm.advancedChartSettingModal.groups;

                },
                getDimensionalData : function() {
                    // sketch._
                },
                addCategoryContainer : function() {
                    vm.categoryContainers.push({});

                },
                updateTableData : function(categoryData) {
                    var keys = Object
                        .keys(categoryData.selectedItems);
                    var obj = {};
                    var column = categoryData.column;
                    keys.forEach(function(index) {
                        categoryData.selectedItems[index]
                            .forEach(function(d) {
                                obj[d] = categoryData.categoryName[index];
                            })
                    });
                    var newColumnName = categoryData.Name;
                    var updatedData = null;

                    vm.tableData.forEach(function(d) {

                        if (obj[d[column]]) {
                            d[newColumnName] = obj[d[column]];
                        } else {
                            d[newColumnName] = d[column];

                        }
                    });
                },
                saveCategories : function(categoriesData) {
                    var testObject = {
                        "key" : "Text",
                        "value" : categoriesData.Name,
                        "type" : [ "DimensionCustom" ]
                    };
                    testObject['categoriesData'] = new Object(
                        categoriesData);
                    vm.customGroup.push(testObject);
                    vm.tableColumns.push(testObject);
                    vm.advancedChartSettingController
                        .updateTableData(categoriesData);
                    generate('success',
                        'Category create successfully');

                },
                removeCategories : function(index) {
                    vm.categoryContainers.splice(index, 1);
                }
            }

            vm.advancedChartSettingView = {
                init : function() {

                    vm.categories = {};
                    vm.categoryContainers = [];
                    vm.addCategoryContainer = vm.advancedChartSettingController.addCategoryContainer;
                    vm.saveCategories = vm.advancedChartSettingController.saveCategories;
                    vm.removeCategory = vm.advancedChartSettingController.removeCategories;

                },
                render : function() {

                    vm.dateFormatList = vm.advancedChartSettingController
                        .fetchDateFormats();
                    vm.dateGroupList = vm.advancedChartSettingController
                        .fetchGroupFormats();

                }

            }
            vm.advancedChartSettingController.init();
            // .............................................
            $scope.edit = function(widget) {
                lastcontainerId = "#chart-" + widget.id;
                if (Object.keys(vm.lastAppliedFilters).length != 0) {
                    if (vm.lastAppliedFilters[lastcontainerId] != undefined) {
                        var config = vm.lastAppliedFilters[lastcontainerId];
                        loadSelectedConfig(config);
                    }
                }
                if (vm.chartType == "Number Widget") {
                    $scope.graphType = true;
                } else {
                    $scope.graphType = false;
                }
            }
            // .................................................................................................
            vm.leftSideModel = {
                chartTypeData : sketch.chartData,
                aggregateData : sketch.AggregatesObject
            };
            vm.leftSideController = {
                init : function() {
                	//Reset Chart
                	vm.resetChart=function(){
                		eChart.resetAll();
                		dataFactory.successAlert("Chart reset successfully");
                	} 
                	//Type Cast
                	vm.dataType=function(type){
                		if(type=='measure')
                			return sketch.dataTypeMeasure;
                		else 
                			return sketch.dataTypeDimension;
                	}
                	vm.checkType=function(type){
                		var i=0;
                		sketch.dataTypeMeasure.forEach(function(d,index){
                			i++;
                			if(d.type==type){
                				var indexNumber=i;
                			}
                		});
                		if(i){
                			return true;
                		}
                		return false;
                	}
                	vm.typeCast=function(object,dataType,type){
                		var index;
                		vm.tableColumns.forEach(function(d,arrayIndex){
                			if(d == object) { 
                		        index=arrayIndex;
                		    }
                		});
                		vm.tableColumns[index].columType=dataType.type;
                		if(type == 'dimension' && vm.checkType(dataType.type)){
                			vm.tableColumns[index].dataKey='measure';
                		}
                	}
                	//Add Report COntainer Function
                	vm.addReport = vm.leftSideController.addNewContainer; 
                    vm.leftSideView.render();
                    vm.chartErrorText = {};
                    vm.showInfo = {};
                    vm.showAggregate = {};
                    // Remove Measure checkbox
                    vm.resetC=function(){
                    	eChart.resetAll();
                    	dataFactory.successAlert("Chart reset successfully");
                    }; 
                    vm.removeMeasure = function(columnName,checkBoxObject) {
                        delete vm.Attributes.checkboxModelMeasure[columnName];
                        vm.chartDraw(checkBoxObject, 'measure');
                    }
                    // Remove Dimension
                    vm.removeDimension = function(columnName,
                                                  checkBoxObject) {
                        delete vm.Attributes.checkboxModelDimension[columnName];
                        vm.chartDraw(checkBoxObject,'dimension');
                    }
                    //creating pivot table
                    $scope.pivotTableSave=function(){
                    	sketch._pivotTableDraw();
                    }
                    
                    // watching query selector for change
                    
                    $scope.selectDataSource = function(model) {
                    	//$('[data-toggle="tooltip"]').tooltip(); 
                        vm.dashboardValidation=true;
                        vm.loadingBarDrawer=true;
                        setTimeout(function() {
                            //vm.leftSideController.addNewContainer();
                            $scope.$apply();
                        }, 100);
                        vm.tableData = [];
                        vm.tableColumnsMeasure = [];
                        sketch._crossfilter = null;
                        sketch._data = null;
                        vm.filtersToApply = [];
                        lastcount = 0;
                        $scope.clear();
                        // resetAllAxis();
                        if(model != undefined) {
                            vm.ischartTypeShown = true;
                            vm.isQuerySelected = false;
                            vm.graphAxis = false;
                            vm.queryObj = model;
                            vm.dashboardValidation = true;
                            vm.dashboardValidation1 = true;
                            vm.showInfo1 = false;
                            vm.showInfo2 = true;
                            vm.preLoad = true;
                            vm.groupObject = "";
                            vm.metadataObject = model;
                            
                            fetchDataAndColumns().then(
                                function() {
                                    vm.preLoad = false;
                                    vm.graphAxis = true;
                                    vm.loadingBarDrawer=false;
                                    $scope.$apply();
                                });
                        }
                        vm.moveToMdArray = {};
                        vm.moveToDimMeasure = function(moveTo,columnName) {
                            vm.tableColumns.filter(function(d,is) {
                                    if (d.columnName == columnName) {
                                        vm.tableColumns[is].dataKey = moveTo;
                                        return true;
                                    }
                                    $scope.moveToMdArray[columnName] = moveTo

                                    return false;
                                });
                            // $scope.dimensionMeasureRecall();
                        }
                        vm.periodVal=[];
                        var i=1;
                        vm.addPeriod=function(){
                            vm.filterPeriod=true;
                            vm.blnkFilter=false;
                            vm.periodVal.push(i);
                            i++;
                        }
                        vm.removePeriod=function(index){
                            vm.periodVal.splice(index,1);
                        }
                        //Get Chart List
                        $scope.charts = vm.leftSideController.getCharts();
                        //Chart Setting Model
                        vm.chartSetting=function(){
                        	vm.chartSettingObject={};
                        	vm.chartSettingObject.toolTipFormate="{xLabel}:{yLabel}"; //tooltip Formate
                        	vm.chartSettingObject.fontSize=13; //Default font size 
                        	$('#chartSetting').modal({
                                backdrop : 'static',
                                keyboard : false
                            });
                            $('#chartSetting').modal('show');
                        }
                        vm.saveDataFormate=function(chartSetting){
                        	var dimensionType;
                        	Object.keys(vm.Attributes.checkboxModelDimension).forEach(function(d){
                        		dimensionType=vm.Attributes.checkboxModelDimension[d].columType
                        	});
                        	vm.Attributes['dataFormate']={};
                        	vm.Attributes['dataFormate']['xAxis']=chartSetting.xAxisDataFormate;
                        	vm.Attributes['dataFormate']['yAxis']=chartSetting.yAxisDataFormate;
                        	vm.Attributes['dataFormate']['xAxisType']=chartSetting.toolTipFormate;
                        	vm.Attributes['dataFormate']['fontSize']=chartSetting.fontSize;
                        	vm.Attributes['dataFormate']['fontFamily']=chartSetting.fontFamily;
                        	var activeData={};
                        	if(!$.isEmptyObject(vm.filteredData))
                            {
                            activeData=vm.filteredData;
                            }
                            else{
                        	    activeData=vm.tableData;
                        	}
                        	var drawn = sketch
                            .axisConfig(vm.Attributes)
                            .data(activeData)
                            .container(vm.DashboardModel.Dashboard.activeReport.reportContainer.id)
                            .chartConfig($scope.selectedChartInView[vm.DashboardModel.Dashboard.activeReport.reportContainer.id])
                                .render(
                                function(drawn) {
                                    $scope.lastDrawnChart[vm.DashboardModel.Dashboard.activeReport.reportContainer.id] = vm.DashboardModel.Dashboard.activeReport.chart.key;
                                    //resolve(drawn);
                               });
                        	$('#chartSetting').modal('hide');
                           }
                           
                    }
                    // Create Column Calculation
                    vm.columnCalculation = function() {
                        $('#calculationMeasure').modal({
                            backdrop : 'static',
                            keyboard : false
                        });
                        $('#calculationMeasure').modal('show');
                        $(".reportDashboard").addClass("background-container");
                    }
                    // Operators define
                    vm.operators = [ {
                        "key" : "Addition",
                        "value" : "+"
                    }, {
                        "key" : "Subtraction",
                        "value" : "-"
                    }, {
                        "key" : "Multiplication",
                        "value" : "*"
                    }, {
                        "key" : "Division",
                        "value" : "/"
                    } ];
                    // Filter
                    vm.filterAdd = function() {
                    	 vm.filerTemp = [];
                         vm.filtersToApply = [];
                         vm.filterHSApply = true;
                         var columnObj = "";
                         Object.keys(vm.checkboxModel).forEach(function(d) {
                                         columnObj = JSON.parse(vm.checkboxModel[d]);
                                         var keyValue = {};
                                         keyValue['key'] = columnObj.columnName;
                                         keyValue['value'] = columnObj.columType;
                                         vm.filerTemp.push(keyValue);
                                         vm.filtersTo[columnObj.columnName] = [];
                                         Enumerable.From(vm.tableData)
                                             .Distinct("$."+ columnObj.columnName)
                                             .Select("$."+ columnObj.columnName)
                                             .ToArray()
                                             .forEach(function(e) {
                                                 vm.filtersTo[columnObj.columnName].push(e);
                                             });
                                         
                        });
                         setTimeout(function(){
                             $('.filterSelect').multiselect({
                                 includeSelectAllOption : true,
                                 enableFiltering : true,
                                 maxHeight : 200 
                             });
                             
                             $('.daterange').daterangepicker(
                     		{ 
                     		    locale: {
                     		      format: 'DD-MM-YYYY'
                     		    }
                     		}, 
                     		function(start, end, label) {
                     			var date={};
                     			date['start']=start.format('YYYY-MM-DD');
                     			date['end']=end.format('YYYY-MM-DD');
                     			var columnName=$scope.filterColumnName;
                     			sketch.applyFilter(date,columnName);
                     		});
                             Object.keys(vm.checkboxModel).forEach(function(d) {
                                 columnObj = JSON.parse(vm.checkboxModel[d]);
     	                        if(columnObj.columType=="int" || columnObj.columType=="float" || columnObj.columType=="decimal"){
     	                        	var minObject = _.min(vm.tableData, function(o){return o[columnObj.columnName];});
     	                            var maxObject = _.max(vm.tableData, function(o){return o[columnObj.columnName];});
     	                            var min = minObject[columnObj.columnName];
     	                            var max= maxObject[columnObj.columnName];
     	                            var columnObject={};
     	                            columnObject['key']=columnObj.columnName;
     	                            columnObject['value']=columnObj.columType;
     	                            if(min==null){min=0;}
     	                            	  $("#"+columnObj.columnName+"-Filter").ionRangeSlider({
     	    	                			    type: "double",
     	    	                			    grid: true, 
     	    	                			    min: min,
     	    	                			    max: max,
     	    	                			    from: min,
     	    	                			    step: 1,
     	    	                			    id : columnObject,
     	    	                			    onFinish: function (data) {
     	    	                			    	sketch.applyFilter(data,this.id);
     	    	                			    }
     	    	                            });
     	                        }
                            });
                        },1000); 
                    }
                    //Filter column Name
                    $scope.filterColumn=function(columnName){
                    	$scope.filterColumnName=columnName;
                    } 
                    $scope.functionList = [
                        {
                            "key" : "ceil",
                            "operator" : "Math.ceil()",
                            "description" : "Ceil rounds a decimal value up to the next highest integer.Syntax: ceil(decimal)"
                        },
                        {
                            "key" : "concat",
                            "operator" : "concat()",
                            "description" : "concat concatenates multiple strings.Syntax: concat(expression, expression, expression... )"
                        },
                        {
                            "key" : "dateDiff",
                            "operator" : "dateDiff('','')",
                            "description" : "dateDiff evaluates the difference between two date strings in days.Syntax: dateDiff(date, date)"
                        },
                        {
                            "key" : "decimalToInt",
                            "operator" : "decimalToInt()",
                            "description" : "decimalToInt converts a decimal value to the integer data type by stripping off the decimal point and any numbers after it. Syntax: decimalToInt(decimal)"
                        },
                        {
                            "key" : "floor",
                            "operator" : "floor()",
                            "description" : "floor rounds a decimal value down to the closest, lowest integer. Syntax: floor(decimal)"
                        },
                        {
                            "key" : "formatDate",
                            "operator" : "formatDate()",
                            "description" : "formatDate formats a date using a specified pattern.Syntax: formatDate(date, ['format'], ['time_zone'])"
                        },
                        {
                            "key" : "intToDecimal",
                            "operator" : "parseFloat().toFixed(2)",
                            "description" : "intToDecimal converts an integer value to the decimal data type.Syntax: parseFloat(expression).toFixed(number)"
                        },
                        {
                            "key" : "isNotNull",
                            "operator" : "isNotNull()",
                            "description" : "isNotNull evaluates an expression and returns true if the expression is not null.Syntax: isNotNull(expression)"
                        },
                        {
                            "key" : "isNull",
                            "operator" : "isNull()",
                            "description" : "isNull evaluates an expression and returns true if the expression is null.Syntax: isNull(expression)"
                        }, /*
                         * { "key" : "left", "operator" :
                         * "left(,)", "description" :
                         * "left returns a specific
                         * number of characters starting
                         * with the leftmost
                         * character.Syntax:
                         * left(expression, limit)" },
                         */
                        {
                            "key" : "ltrim",
                            "operator" : "ltrim()",
                            "description" : "ltrim removes preceding whitespace from a string.Syntax: ltrim(expression)"
                        }, /*
                         * { "key" : "now", "operator" :
                         * "now()", "description" : "now
                         * returns the UTC date and
                         * time, in the format
                         * yyyy-MM-ddTkk:mm:ss:SSSZ.Syntax:
                         * now()" },
                         */
                        {
                            "key" : "nullIf",
                            "operator" : "nullIf()",
                            "description" : "nullIf compares expression 1 and expression 2. If they are equal, then returns null, else returns expression 1.Syntax: nullIf(expression1, expression2)"
                        },
                        {
                            "key" : "parseDate",
                            "operator" : "parseDate()",
                            "description" : "parseDate parseDate parses a string to determine if it contains a date value. This function returns all rows that contain a date in a valid format and skips any rows that do not, including rows that contain null values.Syntax: parseDate(date, ['format'], ['time_zone'])"
                        }, /*
                         * { "key" : "parseDecimal",
                         * "operator" :
                         * "parseDecimal()",
                         * "description" : "parseDecimal
                         * parses a string to determine
                         * if it contains a decimal
                         * value.Syntax:
                         * parseDecimal(expression)" },
                         */
                        {
                            "key" : "replace",
                            "operator" : "replace()",
                            "description" : "replace replaces part of a string with another specified string.Syntax: replace(expression, substring, replacement)"
                        },
                        {
                            "key" : "round",
                            "operator" : "round()",
                            "description" : "round rounds a decimal value to the closest integer, with decimal values of .5 or larger being rounded up. Syntax: round(decimal)"
                        },
                        {
                            "key" : "rtrim",
                            "operator" : "rtrim()",
                            "description" : "rtrim removes following whitespace from a string.Syntax: rtrim(expression)"
                        },
                        {
                            "key" : "strlen",
                            "operator" : "strlen()",
                            "description" : "strlen returns the number of characters in a string.Syntax: strlen(expression)"
                        },
                        {
                            "key" : "toLower",
                            "operator" : "toLower()",
                            "description" : "toLower formats string in all lowercase; skips rows containing null values.Syntax: toLower(expression)"
                        },
                        {
                            "key" : "toString",
                            "operator" : "toString()",
                            "description" : "toString formats the input expression as a string; skips rows containing null values.Syntax: toString(expression)"
                        },
                        {
                            "key" : "trim",
                            "operator" : "trim()",
                            "description" : "trim removes both preceding and following whitespace from a string.Syntax: trim(expression)"
                        } ];
                    // Save
                    vm.categoryError = "";// For Error
                    vm.categoryGroupObject = {}; 
                    vm.createCateogrySave = function(categoryData, categoryName,subCategory) {
                        var j = 0;
                        if (categoryName) {
                            Object.keys(categoryData)
                                .forEach(function(r) {
                                    if (categoryData[$scope.subCategory[j]] != categoryData[r]) {
                                        categoryData[$scope.subCategory[j]] = categoryData[r];
                                        delete categoryData[r];
                                    }
                                    j++;
                                });
                            var lengthCheck = 0;
                            vm.tableData.forEach(function(d) {
                                var keysVal = "";
                                Object
                                    .keys(categoryData)
                                    .forEach(function(r) {

                                        var index = categoryData[r]
                                            .indexOf(d[vm.lastCategoryTableColumn]);
                                        if (index != -1) {
                                            keysVal = r;
                                        }
                                    });
                                if (keysVal == "") {
                                    keysVal = d[vm.lastCategoryTableColumn];
                                }
                                d[categoryName] = keysVal;
                                lengthCheck++
                                if (lengthCheck == vm.tableData.length) {
                                    vm.lastColumnShow = true;
                                    vm.tableColumns
                                        .push({
                                            "columType" : "Long",
                                            "columnName" : categoryName,
                                            "dataKey" : "Dimension",
                                            "reName" : categoryName,
                                            "tableName" : "",
                                            "type" : "custom",
                                            "id" : j
                                        });
                                    vm.categoryGroupObject[j] = {};
                                    vm.categoryGroupObject[j]["columnName"] = $scope.lastCategoryTableColumn,
                                        vm.categoryGroupObject[j]["categoryName"] = categoryName,
                                        vm.categoryGroupObject[j]["groupData"] = categoryData
                                    $("#category").modal('hide');
                                }
                            });
                        }else if (categoryData.length) {
                            $scope.checkError = 0;
                            $scope.categoryError = "Make sub category";
                        }else{
                            $scope.checkError = 0;
                            $scope.categoryError = "Enter Category Name";
                        }
                        $scope.selectedGroup = {};
                        $(".reportDashboard").removeClass("background-container");
                    }
                 // Sub category
                    vm.selectedGroup = {};
                    var custIndex = 0;
                    vm.subCategory = {};
                    //Toggle category group
                    vm.clicked=function($event){
                    	var a = jQuery($event.target);
                        var ul = a.next();
                        ul.slideToggle();
                    }
                    vm.deleteGroupValue=function(key,index,value){
                    	if((vm.selectedGroup[key].length)>1){
                    		var indexCategory=vm.categorySelectColumnData.indexOf(value);
                        	vm.selectedGroup[key].splice(index,1);
                        	
                        	$($(".ms-list > .ms-elem-selectable")[indexCategory]).show()
                    	}else{
                    		alert("You can't delete all sub category");
                    	}
                    }
                    vm.deleteGroup=function(key){
                    	vm.selectedGroup[key].forEach(function(d){
                    		var indexCategory=vm.categorySelectColumnData.indexOf(d);
                    		$($(".ms-list > .ms-elem-selectable")[indexCategory]).show();
                    	});
                    	
                    	delete vm.selectedGroup[key];
                    }
                    vm.addSubCategory = function () {
                        if (vm.selectedValue.length) {
                            $(".ms-selected").hide();
                            if (vm.subCategory[custIndex] == undefined) {
                                vm.subCategory[custIndex] = 'group'
                                    + custIndex;
                            }
                            
                            vm.selectedGroup['group' + custIndex] = vm.selectedValue;
                            vm.selectedValue = [];
                            custIndex++;
                        } else {
                            vm.checkError = 0;
                            vm.categoryError = "Select a value";
                        }
                    }
                    // Text add value formula field
                    $scope.measureFormula = "";
                    vm.calObject={};
                    $scope.insertTextToFormula = function (name, type, functionListObj) {
                        if (type && functionListObj==undefined) {
                            name = '[' + name + ']';
                            
                        }else if(functionListObj){
                        	var description=functionListObj.description;
                        } 
                        if(vm.calObject.measureFormula==undefined){vm.calObject.measureFormula="";}
                        if (name) {
                            vm.calObject.measureFormula += name;
                        }
                        vm.descriptionText = description;
                    }
                    //Update Group Category
                    vm.categoryError = "";// For Error
                    vm.categoryGroupObject = {};
                    var groupIndex=0;
                    vm.cateogryUpdate = function (categoryData, categoryName,subCategory) {
                        	 var j = 0;
                             var columnType;
                             vm.tableColumns.forEach(function (d) {
                                 if (d.reName == vm.lastCategoryTableColumn) {
                                     columnType = d.columType;
                                 }
                             });
                             if (categoryName) {
                                 Object.keys(categoryData)
                                     .forEach(function (r) {
                                         if (categoryData[$scope.subCategory[j]] != categoryData[r]) {
                                             categoryData[$scope.subCategory[j]] = categoryData[r];
                                             delete categoryData[r];
                                         }
                                         j++;
                                     });
                                 var lengthCheck = 0;
                                 vm.tableData.forEach(function (d) {
                                     var keysVal = "";
                                     Object.keys(categoryData).forEach(function (r) {
                                         var index = categoryData[r].indexOf(d[vm.lastCategoryTableColumn]);
                                         if (index != -1) {
                                             keysVal = r;
                                         }
                                     });
                                     if (keysVal == "") {
                                         keysVal = d[vm.lastCategoryTableColumn];
                                     }
                                     d[categoryName] = keysVal;
                                     lengthCheck++
                                     if (lengthCheck == vm.tableData.length) {
                                         vm.lastColumnShow = true;
                                         vm.tableColumns.push({
                                             "columType": columnType,
                                             "columnName": categoryName,
                                             "dataKey": "Dimension",
                                             "reName": categoryName,
                                             "tableName": "",
                                             "type": "custom",
                                             "id": j,
                                             "createdby":vm.columnSelectByGroup
                                         });
                                         setTimeout(function () {
                                             $("#filter").multiselect("destroy");
                                             $("#filter").multiselect({
                                                 includeSelectAllOption: false,
                                                 enableFiltering: true,
                                                 maxHeight: 200
                                             });
                                         }, 100);
                                         vm.categoryGroupObject[groupIndex] = {};
                                         vm.categoryGroupObject[groupIndex]["columnName"] = $scope.lastCategoryTableColumn;
                                         vm.categoryGroupObject[groupIndex]["categoryName"] = categoryName;
                                         vm.categoryGroupObject[groupIndex]["groupData"] = categoryData;
                                         groupIndex++;
                                         $("#categoryEdit").modal('hide');
                                     }
                                     $scope.selectedGroup = {};
                                     $(".reportDashboard").removeClass("background-container");
                                     
                                     
                                 });
                                 var lastObj=vm.categoryGroupObject[vm.editGroupIndex];
                                 vm.tableData.forEach(function(d,index){
                                	 if(lastObj.columnName==d.columnName){
                                		 delete d[index];
                                	 }
                                 });
                                 var columnIndex="";
                                 vm.tableColumns.forEach(function(d,index){
                                	 if(d.reName==lastObj.categoryName){
                                		 columnIndex=index;
                                	 }
                                 });
                                 vm.tableColumns.splice(columnIndex,1);
                                 delete vm.categoryGroupObject[vm.editGroupIndex];
                             } else if (categoryData.length) {
                                 $scope.checkError = 0;
                                 $scope.categoryError = "Make sub category";
                             } else {
                                 $scope.checkError = 0;
                                 $scope.categoryError = "Enter Category Name";
                             }
                    }
                    //Edit category
                    vm.editGroupCategory=function(columnObject){
                    	vm.categoryEdit={};
                    	vm.categoryEdit.categoryName=columnObject.reName;
                    	 var modalElem = $('#categoryEdit');
                         $('#categoryEdit').modal({
                             backdrop: 'static',
                             keyboard: false
                         });
                         var editIndex=0;
                         $.each(vm.categoryGroupObject,function(key,val){
                        	 if(val.categoryName==columnObject.reName){
                        		 vm.editGroupIndex=editIndex;
                        	 }
                        	 editIndex++;
                         });
                         vm.columnSelectByGroup=columnObject;
                         vm.selectedGroup={};
                         $(".reportDashboard").addClass("background-container");
                         var connectionObject = vm.metadataObject.connObject.connectionObject;
                         vm.lastCategoryTableColumn = columnObject.createdby.columnName;
                         if (vm.categorySelectColumnData != undefined) {
                             $("#my-select").multiSelect("destroy");
                         }
                         vm.categorySelectColumnData = [];
                         var categoryIndex;
                         var objIndex=0;
                         $.each(vm.categoryGroupObject,function (key,value) {
                             if(value.columnName==columnObject.createdby.reName){
                            	 categoryIndex=objIndex;
                             }
                             objIndex++;
                         });
                         vm.selectedGroup=vm.categoryGroupObject[categoryIndex].groupData;
                         dataFactory.categoryColumnData(
                             JSON.stringify(connectionObject),
                             JSON.stringify(columnObject.createdby)).then(function (response) {
                             var itemsProcessed = 0;
                             response.data.result.forEach(function (d) {
                                 if (d[vm.lastCategoryTableColumn]) {
                                     $scope.categorySelectColumnData.push(d[vm.lastCategoryTableColumn]);
                                 }
                                 itemsProcessed++;
                                 if (itemsProcessed === response.data.result.length) {
                                	 setTimeout(function () {
                                                 $scope.selectedValue = [];
                                                 $('#groupEditCategory').multiSelect({
                                                     afterSelect: function (values) {
                                                         $scope.selectedValue.push(values[0]);
                                                         $scope.checkError = 1;
                                                     },
                                                     afterDeselect: function (values) {
                                                         var index = $scope.selectedValue.indexOf(values[0]);
                                                         if (index != -1) {
                                                             $scope.selectedValue.splice(index, 1);
                                                         }
                                                         $scope.checkError = 1;
                                                     }
                                                 });
                                      },1);
                                 }
                             });
                         });
                    }         
                    // Create Category
                    vm.categoryGroupObj={};
                    vm.createCategory = function (columnObject) {
                        var modalElem = $('#category');
                        $('#category').modal({
                            backdrop: 'static',
                            keyboard: false
                        });
                        vm.categoryGroupObj={};
                        vm.columnSelectByGroup=columnObject;
                        vm.selectedGroup={};
                        $(".reportDashboard").addClass("background-container");
                        var connectionObject = vm.metadataObject.connObject.connectionObject;
                        vm.lastCategoryTableColumn = columnObject.columnName;
                        if (vm.categorySelectColumnData != undefined) {
                            $("#my-select").multiSelect("destroy");
                        }
                        vm.categorySelectColumnData = [];
                        dataFactory.categoryColumnData(
                            JSON.stringify(connectionObject),
                            JSON.stringify(columnObject)).then(function (response) {
                            var itemsProcessed = 0;
                            response.data.result.forEach(function (d) {
                                if (d[vm.lastCategoryTableColumn]) {
                                    $scope.categorySelectColumnData.push(d[vm.lastCategoryTableColumn]);
                                }
                                itemsProcessed++;
                                if (itemsProcessed === response.data.result.length) {
                                    callBack();
                                }
                            });
                        });
                        // Close Error
                        vm.notificationClose = function () {
                            vm.checkError = 1;
                        }
                        var callBack = function () {
                            setTimeout(
                                function () {
                                    $scope.selectedValue = [];
                                    $('#my-select').multiSelect({
                                        afterSelect: function (values) {
                                            $scope.selectedValue.push(values[0]);
                                            $scope.checkError = 1;
                                        },
                                        afterDeselect: function (values) {
                                            var index = $scope.selectedValue.indexOf(values[0]);
                                            if (index != -1) {
                                                $scope.selectedValue.splice(index, 1);
                                            }
                                            $scope.checkError = 1;
                                        }
                                    });

                                }, 1);
                        }

                    }
                    // Delete dimension and measures
                    vm.deleteColumn = function (columnName) {
                        // filter the array
                        var foundItem = $filter('filter')(
                            vm.tableColumns, {
                                reName: columnName
                            }, true)[0];
                        // get the index
                        var index = vm.tableColumns.indexOf(foundItem);
                        vm.tableColumns.splice(index, 1);
                    }
                    // save Calculation field
                    $scope.fieldName = "";
                    $scope.saveCalculationField = function (calObj) {
                    	var name=calObj.fieldName;
                    	var formula =calObj.measureFormula;
                        if (formula != "" && name != "" && name!=undefined) {
                            var expression;
                            var errorMessage;
                            // Check express for bracketFp
                            function update() {
                                expression = true;
                                try {
                                    balanced.matches({
                                            source: formula,
                                            open: ['{',
                                                '(',
                                                '['],
                                            close: ['}',
                                                ')',
                                                ']'],
                                            balance: true,
                                            exceptions: true
                                        });
                                } catch (error) {
                                    expression = false;
                                    errorMessage = error.message;
                                }
                            }
                            update();
                            expression=true;
                            if (expression) {
                                var errorMessage = false;
                                if (vm.filteredData.length != 0) {
                                    errorMessage = calculate.processExpression(formula, vm.filteredData, name);
                                }
                                else{
                                    errorMessage = calculate.processExpression(formula, vm.tableData, name);
                                }
                                if (errorMessage == false) {
                                	
                                    $scope.tableColumns.push({
                                        "columType": "varchar",
                                        "columnName": name,
                                        "dataKey": "Measure",
                                        "reName": name,
                                        "tableName": "",
                                        "formula": formula,
                                        "type": "custom",
                                    });
                                    //For Edit calculation 
                                    if(vm.flagCheck){
                                    	var index=$scope.tableColumns.indexOf(vm.calCurrentEditObj);
                                    	$scope.tableColumns.splice(index,1);
                                    }
                                    setTimeout(function () {
                                        $("#filter").multiselect("destroy");
                                        $("#filter").multiselect({
                                            includeSelectAllOption: false,
                                            enableFiltering: true,
                                            maxHeight: 200
                                        });
                                    }, 100);
                                    $scope.lastColumnShow = true;
                                    $("#calculationMeasure")
                                        .modal('hide');
                                } else {
                                    $scope.errorCalculation = errorMessage;

                                }
                            } else {
                                $scope.errorCalculation = errorMessage;

                            }
                        } else if (formula === "") {
                            $scope.errorCalculation = "Please select expressions";

                        } else if (name === "" ||  name === undefined) {
                            $scope.errorCalculation = "Please Enter field name";
                        }

                        $(".reportDashboard").removeClass("background-container");


                    }

                    $scope.modal = {};
                    $scope.modal.slideUp = "default";
                    $scope.modal.stickUp = "default";
                    $scope.addChartIconBorder = function (chart) {

                        $(".custBorderHover").removeClass('activeChart');
                        $("#" + chart.key).addClass('activeChart');
                    }
                    vm.onAggregateSelect = function (measure, aggregate) {
                        vm.Attributes.aggregateModel[measure.columnName] = aggregate;
                    }
                    vm.chartSelect = function (index, chart) {
                        vm.dashboardPrototype.reportContainers[vm.DashboardModel.Dashboard.activeReportIndex]['chartType'] = chart.name;
                        if (chart.key === "_compositeLineBarChart") {
                            var modalElem1 = $('#barLineModal');
                            $('#barLineModal').modal({
                                backdrop: 'static',
                                keyboard: false
                            });
                        }
                        $scope.addChartIconBorder(chart);

                    }
                    vm.aggeregateSelect = function (aggregate, containerId, measure) {
                        vm.aggregateText[containerId] = aggregate.value;
                        if (vm.Attributes.aggregateModel)
                            vm.Attributes.aggregateModel[measure.columnName] = aggregate;
                    }
                    vm.checkboxModelMeasure = [];
                    vm.checkboxModelDimension = [];

                },

                getCharts : function() {
                    return vm.leftSideModel.chartTypeData;

                },
                getAggregates : function() {

                    return vm.leftSideModel.aggregateData;

                },

                senseDefaultChart : function() {
                    if (Object
                            .keys(vm.Attributes.checkboxModelDimension).length == 1
                        && Object
                            .keys(vm.Attributes.checkboxModelMeasure).length == 1) {
                        $scope.addChartIconBorder(sketch.chartData[0]);
                        return sketch.chartData[0];

                    } else if (Object
                            .keys(vm.Attributes.checkboxModelDimension) == 1
                        && Object
                            .key(vm.Attributes.checkboxModelMeasure).length == 2) {
                        return sketch.chartData[5];
                    } else {
                        return false;

                    }

                },

                requiredAttributesFound : function() {

                    if (vm.Attributes.checkboxModelDimension
                        && vm.Attributes.checkboxModelMeasure)
                        if (Object.keys(vm.Attributes.checkboxModelDimension).length > 0
                            && Object.keys(vm.Attributes.checkboxModelMeasure).length > 0) {
                            return true;
                        }
                    return false;

                },
                showChartErrorText : function(chart) {
                    try {
                        if (chart != null) {
                            vm.blnkRep = true;
                            vm.chartErrorText[vm.DashboardModel.Dashboard.activeReport.reportContainer.id] = "You need to select "
                                + chart.required.Dimension
                                + " dimension"
                                + " and "
                                + chart.required.Measure[0]
                                + " measure to draw "
                                + chart.name;
                        }else{
                            vm.blnkRep = true;
                            vm.chartErrorText[vm.DashboardModel.Dashboard.activeReport.reportContainer.id] = "You need to select atleast 1 dimension and 1 measure to draw any chart";
                        }
                    }catch(e){
                        vm.chartErrorText[vm.DashboardModel.Dashboard.activeReport.reportContainer.id] = "Some Error Occured....";
                    }
                },
                checkChartCanBeDrawn : function(chart) {
                    if (chart != null)
                        if (vm.Attributes.checkboxModelDimension && vm.Attributes.checkboxModelMeasure) {
                            if ((Object.keys(vm.Attributes.checkboxModelDimension).length == chart.required.Dimension) && ((chart.required.Measure[0] <= Object.keys(vm.Attributes.checkboxModelMeasure).length) && (chart.required.Measure[1] >= Object.keys(vm.Attributes.checkboxModelMeasure).length))) {
                                return true;

                            }
                        }


                    return false;
                },
                updateAttributesObject : function(attr, type) {
                    if (type == "dimension")
                        if ($scope.Attributes.checkboxModelDimension[attr.columnName] == 0) {
                            delete $scope.Attributes.checkboxModelDimension[attr.columnName];
                            return false;
                    }
                    if (type == "measure")
                        if ($scope.Attributes.checkboxModelMeasure[attr.columnName] == 0) {
                            delete $scope.Attributes.checkboxModelMeasure[attr.columnName];
                            return false;
                    }
                    if (type == "chart" && !vm.leftSideController.checkChartCanBeDrawn(attr)) {
                        vm.leftSideController.showChartErrorText(attr);
                        return false;
                    }
                    return true;
                },
                addNewContainer : function() { 
                    vm.Attributes = {aggregateModel:{}};
                    vm.DashboardController.addReportContainer(sketch.chartData[0]);
                    vm.DashboardModel.Dashboard.activeReport.chart = sketch.chartData[0];
                    $scope.selectedChartInView[vm.DashboardModel.Dashboard.activeReport.reportContainer.id] = null;
                    $(".custBorderHover").removeClass('activeChart');
                    // $("#" + index).addClass('activeChart');

                },
                onAttributeSelect : function(attr, type) {
                

                    if(type=="measure")
                    {

                        vm.Attributes.aggregateModel[attr.columnName]={
                            "key" : "sumIndex",
                            "value" : "Sum",
                            "type" : "Aggregate"
                        };


                    }
                    vm.chartLoading = true;
                    if (type == "chart") {
                        $scope.selectedChartInView[vm.DashboardModel.Dashboard.activeReport.reportContainer.id] = attr;
                        vm.DashboardModel.Dashboard.activeReport.chart = null;
                    }
                    //pivot table
                    if(type=="chart" && attr.key=="_pivotTable"){
                    	  $scope.showInfo1=true;
                    	  
                    	  sketch.pivotTableConfig=undefined; 
                    	  sketch.data(vm.tableData).container(vm.DashboardModel.Dashboard.activeReport.reportContainer.id)._pivotTable();
                    	  $(".label_legends"+vm.DashboardModel.Dashboard.activeReport.reportContainer.id).hide();
                    	  $("#left"+vm.DashboardModel.Dashboard.activeReport.reportContainer.id).html("");
                          return;
                    }
                    if (vm.leftSideController.updateAttributesObject(attr, type) || vm.leftSideController.checkChartCanBeDrawn(vm.DashboardModel.Dashboard.activeReport.chart)) {

                        if (vm.DashboardModel.Dashboard.activeReport.reportContainer != undefined) {
                            vm.showInfo[vm.DashboardModel.Dashboard.activeReport.reportContainer.id] = false;

                        }

                        if (vm.leftSideController.requiredAttributesFound()) {

                            var chart = null;
                            if ($scope.selectedChartInView[vm.DashboardModel.Dashboard.activeReport.reportContainer.id] == null) {
                                $scope.selectedChartInView[vm.DashboardModel.Dashboard.activeReport.reportContainer.id] = vm.leftSideController
                                    .senseDefaultChart();
                                chart = vm.leftSideController
                                    .senseDefaultChart();

                            }
                            if (vm.DashboardModel.Dashboard.activeReportIndex == null) {

                                vm.DashboardController
                                    .addReportContainer(chart);
                                vm.DashboardModel.Dashboard.activeReport.chart = chart;
                            }
                        } else {

                            vm.chartLoading = false;
                        }
                        if (type == "chart") {
                            vm.DashboardModel.Dashboard.activeReport.chart = attr;

                        }

                        if (vm.DashboardModel.Dashboard.activeReport.chart != null
                            && vm.leftSideController.checkChartCanBeDrawn(vm.DashboardModel.Dashboard.activeReport.chart)) {
                            setTimeout(
                                function() {

                                    if ($scope.selectedChartInView[vm.DashboardModel.Dashboard.activeReport.reportContainer.id].key == "_numberWidget") {
                                        // $scope.dashboardPrototype.reportContainers.splice(vm.DashboardModel.Dashboard.activeReport.reportContainer.id);
                                        // $scope.deleteReportContainer(vm.DashboardModel.Dashboard.activeReport.reportContainer.id);
                                        // vm.DashboardController.addReportContainer(attr);
                                        // vm.showAggregate[.DashboardModel.Dashboard.activeReport.reportContainer.id]
                                        if (!$scope.lastDrawnChart[vm.DashboardModel.Dashboard.activeReport.reportContainer.id]
                                            || $scope.lastDrawnChart[vm.DashboardModel.Dashboard.activeReport.reportContainer.id] != "_numberWidget") {
                                            var activeReportContainerId = vm.DashboardModel.Dashboard.activeReport.reportContainer.id;

                                            var activeContainer = $scope.dashboardPrototype.reportContainers[$scope
                                                .findIndexOfReportContainer(activeReportContainerId)];

                                            var obj = vm.DashboardController
                                                .getNumberContainerSettings();
                                            $.each(obj,function(key,value) {
                                                activeContainer[key] = value;

                                            });

                                            vm.DashboardModel.Dashboard.activeReport.reportContainer = activeContainer;
                                            var indexOfReport = $scope.findIndexOfReportFromReportContainer(activeReportContainerId);
                                            vm.DashboardModel.Dashboard.Report[indexOfReport] = activeContainer;
                                            vm.activeXLabel[vm.DashboardModel.Dashboard.activeReport.reportContainer.id] = $scope.Attributes.checkboxModelDimension[Object
                                                .keys($scope.Attributes.checkboxModelDimension)[0]];
                                            vm.activeYLabel[vm.DashboardModel.Dashboard.activeReport.reportContainer.id] = $scope.Attributes.checkboxModelMeasure[Object
                                                .keys($scope.Attributes.checkboxModelMeasure)[0]];

                                        }
                                        setTimeout(
                                            function() {

                                                vm.DashboardView.renderChartInActiveContainer();

                                            }, 1000);


                                    } else {
                                        if ($scope.lastDrawnChart[vm.DashboardModel.Dashboard.activeReport.reportContainer.id] == "_numberWidget") {
                                            var activeReportContainerId = vm.DashboardModel.Dashboard.activeReport.reportContainer.id;

                                            var activeContainer = $scope.dashboardPrototype.reportContainers[$scope.findIndexOfReportContainer(activeReportContainerId)];

                                            var obj = vm.DashboardController
                                                .getCommonContainerSettings();
                                            $.each(obj,
                                                    function(
                                                        key,
                                                        value) {
                                                        activeContainer[key] = value;

                                                    });

                                            vm.DashboardModel.Dashboard.activeReport.reportContainer = activeContainer;
                                            var indexOfReport = $scope
                                                .findIndexOfReportFromReportContainer(activeReportContainerId);
                                            vm.DashboardModel.Dashboard.Report[indexOfReport] = activeContainer;

                                        }

                                        vm.activeXLabel[vm.DashboardModel.Dashboard.activeReport.reportContainer.id] = $scope.Attributes.checkboxModelDimension[Object
                                            .keys($scope.Attributes.checkboxModelDimension)[0]];
                                        vm.activeYLabel[vm.DashboardModel.Dashboard.activeReport.reportContainer.id] = $scope.Attributes.checkboxModelMeasure[Object
                                            .keys($scope.Attributes.checkboxModelMeasure)[0]];

                                        setTimeout(function() {

                                            vm.DashboardView.renderChartInActiveContainer();

                                        }, 1000);
                                    }
                                }, 1);
                        } else {
                            vm.chartLoading = false;
                            vm.leftSideController.showChartErrorText($scope.selectedChartInView[vm.DashboardModel.Dashboard.activeReport.reportContainer.id]);
                            vm.showInfo[vm.DashboardModel.Dashboard.activeReport.reportContainer.id] = true;
                        }



                    } else {
                        vm.chartLoading = false;
                        vm.leftSideController.showChartErrorText(vm.DashboardModel.Dashboard.activeReport.chart);
                        vm.showInfo[vm.DashboardModel.Dashboard.activeReport.reportContainer.id] = true;

                    }
                    // For Aggegate show or hide
                    if (vm.showInfo[vm.DashboardModel.Dashboard.activeReport.reportContainer.id] == true) {
                        vm.showAggregate[vm.DashboardModel.Dashboard.activeReport.reportContainer.id] = false;
                    } else {
                        vm.showAggregate[vm.DashboardModel.Dashboard.activeReport.reportContainer.id] = true;
                    }

                }
            };

            vm.leftSideView = {

                render : function() {
                    vm.Attributes = {};
                    vm.chartDraw = vm.leftSideController.onAttributeSelect;
                    // vm.chartSelect=vm.leftSideController.updateAttribute;
                    $scope.charts = vm.leftSideController
                        .getCharts();
                    $scope.aggregates = vm.leftSideController.getAggregates();
                    
                }
            };

            vm.leftSideController.init();

            // ..................Right side
            // .....................................................

            vm.rightSideModel = {
                axisObject : sketch.axisData,
                AggregateObject : sketch.AggregatesObject,
                axisData : sketch.attributesData,
                activeAxisConfig : null
            }
            vm.rightSideController = {
                init : function() {
                	
                    vm.rightSideView.init();
                    vm.checkboxModel = {};
                    vm.checkboxMD = {};
                    vm.filtersToApply = [];
                    vm.filtersTo = [];
                    var newDim = [];
                    vm.styleSettings = {};
                    vm.columnAddValue = function(type, value) {
                        var tableColumnIndex = vm.rightSideController
                            .findIndexofActiveContainer(
                                vm.tableColumns,
                                "value", value);
                        vm.tableColumns[tableColumnIndex].type = [];
                        vm.tableColumns[tableColumnIndex].type
                            .push(type);
                        // $('.advChartSettingsModal').drawer('close');
                        generate("success",
                            "Move To Column Successfully");
                    }
                    vm.chartStyle = function(reportContainer) {
                        vm.styleSettings = {};
                        vm.DashboardController.onReportContainerClick(reportContainer.id);
                        if (vm.DashboardModel.Dashboard.Report[vm.DashboardModel.Dashboard.activeReportIndex].chart.styleSettings) {
                            vm.styleSettings = vm.DashboardModel.Dashboard.Report[vm.DashboardModel.Dashboard.activeReportIndex].chart.styleSettings;
                            $('.chartStyle').drawer('open');
                        } else if (vm.DashboardModel.Dashboard.Report[vm.DashboardModel.Dashboard.activeReportIndex].chartObject != undefined) {
                            vm.cWidthshow = false;
                            vm.styleSettings.rotateLabel = 0;
                            vm.styleSettings.rotateChart = 0;
                            vm.styleSettings.resizable = "false";
                            vm.styleSettings.translateX = 0;
                            vm.styleSettings.translateY = 0;
                            var index = reportContainer.id - 1;
                            var chart = vm.DashboardModel.Dashboard.Report[vm.DashboardModel.Dashboard.activeReportIndex].chart;
                            var chartObj = vm.DashboardModel.Dashboard.Report[vm.DashboardModel.Dashboard.activeReportIndex].chartObject;
                            // Push Value to input fields
                            vm.styleSettings.chartColor = "#1f77b4";
                            if (typeof chartObj !== 'undefined'
                                && chartObj.margins) {
                                vm.styleSettings.xAxisMargin = chartObj
                                    .margins().bottom;
                                vm.styleSettings.yAxisMargin = chartObj
                                    .margins().left;
                            }
                            vm.styleSettings.cHeight = $(
                                "#chart-"
                                + reportContainer.id)
                                .height();
                            vm.styleSettings.cWidth = $(
                                "#chart-"
                                + reportContainer.id)
                                .width();
                            vm.styleSettings.xAxisLabelName = $(
                                "#chart-"
                                + reportContainer.id)
                                .find(".x-axis-label")
                                .text();
                            vm.styleSettings.yAxisLabelName = $(
                                "#chart-"
                                + reportContainer.id)
                                .find(".y-axis-label")
                                .text();
                            vm.DashboardModel.Dashboard.Report[vm.DashboardModel.Dashboard.activeReportIndex].chart.styleSettings = vm.styleSettings;

                            $('.chartStyle').drawer('open');
                            var processedData = [];
                            vm.rightSideModel.axisData
                                .forEach(function(obj) {
                                    chart.attributes
                                        .forEach(function(
                                            chartType) {
                                            if (chartType
                                                    .indexOf(obj.Name) != -1) {
                                                // chartSettingsObj[obj.modelName]='{"key":"","value":"","type":[""]}';
                                                obj.visible = true;
                                                processedData
                                                    .push(obj);
                                            }
                                        });
                                });
                            vm.processStyle = processedData;
                        }
                    }
                },
                getMeasures : function() {
                    var tempArray = [];

                    vm.tableColumns.forEach(function(obj) {

                        if (obj.type == "Measure")
                            tempArray.push(obj);
                        if (obj.type == "MeasureCustom")
                            tempArray.push(obj);

                    });
                    return tempArray;
                },

                setActiveAxisConfig : function(config) {
                    vm.rightSideModel.activeAxisConfig = config;
                },
                getActiveAxisConfig : function() {
                    return vm.rightSideModel.activeAxisConfig;
                },
                getDimensions : function() {
                    var tempArray = [];
                    var pushSkip = 0;
                    vm.tableColumns.forEach(function(obj) {

                        if (obj.type == "Dimension")
                            tempArray.push(obj);
                        if (obj.type == "DimensionCustom")
                            tempArray.push(obj);

                    });
                    return tempArray;
                },
                columnDataPush : function(obj) {
                    if (obj.columnType == "Measure") {
                        obj.columnData = vm.rightSideController
                            .getMeasures();
                        return obj;
                    } else if (obj.columnType == "Dimension") {
                        obj.columnData = vm.rightSideController
                            .getDimensions();
                        return obj;

                    } else if (obj.columnType == "Aggregate") {
                        obj.columnData = vm.rightSideModel.AggregateObject;
                        return obj;
                    }
                },

                getAxisRenderObj : function(selected, type) {
                    var chartSettingsObj = {};
                    var dimensionArray = [];
                    chartSettingsObj['dimension'] = vm.checkboxModelDimension;
                    chartSettingsObj['measure'] = vm.checkboxModelMeasure;
                    chartSettingsObj['aggregate'] = [ {
                        "key" : "sumIndex",
                        "value" : "Sum",
                        "type" : "Aggregate"
                    } ];
                    var processedData = [];
                    vm.chartSettings = chartSettingsObj;
                    vm.DashboardModel.Dashboard.activeReport.chart = selected;
                    vm.rightSideController.onAxisValueChange();
                    // return finalData;

                    

                },

                findIndexofActiveContainer : function(
                    arraytosearch, key, valuetosearch) {
                    for (var i = 0; i < arraytosearch.length; i++) {

                        if (arraytosearch[i][key] == valuetosearch) {
                            return i;
                        }
                    }
                    return null;

                },

                onAxisValueChange : function() {

                    /*
                     * if
                     * (JSON.parse(vm.chartSettings["Xaxis"]).key ==
                     * "DATETIME")
                     * $("#dateFormatChangeModal").modal();
                     */
                    var drawn = sketch
                        .axisConfig(vm.chartSettings)
                        .data(vm.tableData)
                        .container(vm.DashboardModel.Dashboard.activeReport.reportContainer.id)
                        .chartConfig(vm.DashboardModel.Dashboard.activeReport.chart)
                        .render();

                    var index = vm.rightSideController
                        .findIndexofActiveContainer(
                            $scope.dashboardPrototype.reportContainers,
                            "id",
                            vm.DashboardModel.Dashboard.activeReport.reportContainer.id);

                    if (drawn) {
                        $scope.filterSave = true;
                        $scope.saveState = 1;

                        $scope.dashboardPrototype.reportContainers[index]['image'] = null;

                    }

                    vm.rightSideController
                        .setActiveAxisConfig(vm.chartSettings);
                    var indexOfReport = vm.DashboardController
                        .findIndexofActiveContainer(
                            vm.DashboardModel.Dashboard.Report,
                            "id",
                            vm.DashboardModel.Dashboard.activeReport.reportContainer.id);
                    vm.DashboardModel.Dashboard.Report[indexOfReport].axisConfig = new Object(
                        vm.chartSettings);
                    if (drawn) {
                        drawn["chartTypeAttribute"] = vm.DashboardModel.Dashboard.activeReport.chart.name;
                        drawn["resize"] = "ON";
                        vm.DashboardModel.Dashboard.Report[indexOfReport]["chartObject"] = drawn;

                    }
                    vm.DashboardModel.Dashboard.activeReport = vm.DashboardModel.Dashboard.Report[indexOfReport];

                    vm.DashboardModel.Dashboard.activeReportIndex = indexOfReport;

                }
            }

            vm.rightSideView = {
                // this part can be improved
                init : function() {
                    // vm.chartDraw=vm.rightSideController.getAxisRenderObj;
                    vm.refreshChartContainer = vm.rightSideController.onAxisValueChange;
                    vm.columnTypeObj = {};
                    $scope.addColumnTypeRightSide = function(
                        columnObj) {

                        vm.hiddenAxis = columnObj.Name;
                        $("#myModal").modal();

                        if (columnObj.columnType
                                .indexOf("Dimension") != -1) {
                            vm.columnTypeObj = vm.rightSideController
                                .getMeasures();

                        } else if (columnObj.columnType
                                .indexOf("Measure") != -1) {
                            vm.columnTypeObj = vm.rightSideController
                                .getDimensions();
                        }

                    }
                    // Calculation part
                    $scope.measureSetting = function(columnObj) {
                        $("#measureModel").modal();
                        columnObj.columnType = "Dimension";
                        if (columnObj.columnType == "Dimension") {
                            vm.columnTypeObj = vm.rightSideController
                                .getMeasures();
                        } else if (columnObj.columnType == "Measure") {
                            vm.columnTypeObj = vm.rightSideController
                                .getDimensions();
                        }
                    }
                    $scope.calMeasureShow = [];
                    $scope.calMeasureDb = [];
                    $scope.measureAdd = function() {
                        // Formula in array

                        $scope.measureName = "";
                        $scope.measureFormula = "";
                    }
                    $scope.measureFormula = "";
                    $scope.insertText = function(elemID, text) {
                        $scope.measureFormula += text;

                        // $scope.measureVal.measureFormula
                        // +=text
                    }
                    // Close Style Type Drawer
                    $scope.styleTypeDrawerClose = function() {
                        $('.chartStyle').drawer('close');
                    }

                    vm.calMeasureArrayForCompare = [];
                    // Measure Add Calculation
                    vm.customMeasure = [];
                    vm.measureAddCal = function() {
                        // sketch.setGroupExpression($scope.expression);
                        var expression = $scope.measureFormula;
                        vm.FormulaArray = expression
                            .split(/([\+\-\*\/])/);
                        vm.expression = expression.replace(
                            /\b[a-z]\w*/ig, "v['$&']")
                            .replace(/[\(|\|\.)]/g, "");
                        vm.calMeasureShow
                            .push({
                                'measureName' : $scope.measureName,
                                'measureFormula' : $scope.measureFormula
                            });
                        vm.calMeasureDb
                            .push({
                                'measureName' : $scope.measureName,
                                'measureFormula' : $scope.FormulaArray
                            });
                        vm.calMeasureShow
                            .forEach(function(d) {
                                if (vm.calMeasureArrayForCompare
                                        .indexOf(d.measureName) == -1) {
                                    var newExpression = d.measureFormula
                                        .replace(
                                            /\b[a-z]\w*/ig,
                                            "v['$&']")
                                        .replace(
                                            /[\(|\|\.)]/g,
                                            "");

                                    var testObject = {
                                        "key" : "INT",
                                        "value" : d.measureName,
                                        "type" : [ "MeasureCustom" ],
                                        "formula" : newExpression
                                    };
                                    vm.customMeasure
                                        .push(testObject);
                                    vm.tableColumns
                                        .push(testObject);
                                    $(
                                        '.advChartSettingsModal')
                                        .drawer('close');
                                    generate("success",
                                        "Measure created successfully");
                                } else {
                                    generate("error",
                                        "You Calculation column match to other column");
                                }
                            });
                    }
                    // Measure Validation
                    vm.measureAddValidation = function() {
                        /*
                         * var parts =
                         * $scope.measureFormula.split(/[[\]]{1,2}/);
                         * parts.length--; // the last entry is
                         * dummy, need to take it out var
                         * valExp=parts.join('');
                         */
                        var newExpression = $scope.measureFormula
                            .replace(/\b[a-z]\w*/ig,
                                "v['$&']").replace(
                                /[\(|\|\.)]/g, "");
                        var formula = "v['"
                            + vm.lastColumSelected + "']";
                        var testObject = {
                            "key" : "INT",
                            "value" : $scope.measureNameValidation,
                            "type" : [ "MeasureCustom" ],
                            "validation" : newExpression,
                            "formula" : formula
                        };
                        vm.tableColumns.push(testObject);

                        $('.advChartSettingsModal').drawer(
                            'close');
                        generate("success",
                            "Measure created successfully");
                    }
                    $scope.addColumnType = function(columnObj) {
                        vm.hiddenAxis = columnObj.Name;

                        $("#myModal").modal();

                        if (columnObj.columnType == "Dimension") {
                            vm.columnTypeObj = vm.rightSideController
                                .getMeasures();
                        } else if (columnObj.columnType == "Measure") {
                            vm.columnTypeObj = vm.rightSideController
                                .getDimensions();
                        }
                    }
                    // End Calculation Part

                },
                render : function(chartTypeObj) {
                    // vm.chartSettings={};

                    this.data = vm.rightSideController.getAxisRenderObj(chartTypeObj);
                    // vm.rightSideView.resetView(this.data);
                    // $(".select2").select2('remove');

                    vm.axisRenderArray = this.data;

                    setTimeout(function() {
                        $('select').select2();

                    }, 100);

                },
                chartSelectView : {
                    update : function(chartInfo) {
                        $scope.chartType = JSON
                            .stringify(chartInfo);
                    }

                }

            }

            // vm.rightSideController.init();

            // .................................................End
            // Right Side
            // View...........................................................................

            // ...........................Dashboard
            // View...............................................................................
            var maxSizeY;
            if($stateParams.type==1){
            	maxSizeY=2;
            }else{
            	maxSizeY=18; 
            }
            vm.DashboardModel = { 
                currentActiveGrid : null,
                dashboardPrototypeObject : {
                    id : '1',
                    name : 'Home',
                    reportContainers : []
                },
                reportPrototypeObject : {
                    reportContainer : null,
                    chart : null,
                    axisConfig : null
                },
                gridSettings : {
                    margins : [20,20],
                    columns : 30,
                    pushing : true,
                    floating : true,
                    swapping : true,
                    maxSizeY: maxSizeY,
                    collision : {
                        on_overlap_start : function(collider_data) {

                        }
                    },
                    draggable : {
                        enabled : true, // whether dragging
                        // items is supported
                        handle : '.my-class',
                        start : function(event, $element,
                                         widget) {

                        },
                        resize : function(event, $element,
                                          widget) {

                        },
                        stop : function(event, $element, widget) {
							/*$(".hiddenBorder").removeClass(
							 "box-border");*/
                        }
                    },
                    resizable : {
                        enabled : true,
                        handles : [ 'n', 'e', 's', 'w', 'ne',
                            'se', 'sw', 'nw' ],
                        stop : function(event, uiWidget,
                                        $element) {

                            var width=($element.sizeX)*40;
                            var height=($element.sizeY)*32;
                            //chart Loading Bar True
                            var variable="reportLoading_"+vm.DashboardModel.Dashboard.activeReport.reportContainer.id;
                            vm[variable]=false;
                            //Chart Hide
                            $("#chart-"+vm.DashboardModel.Dashboard.activeReport.reportContainer.id).show();
                            //$("#chart-"+$element.id).width(width).height(height);
                            
                            if(width>560 && height>250){ 
                            	$(".label_legends"+vm.DashboardModel.Dashboard.activeReport.reportContainer.id).show();
                            	$("#left"+vm.DashboardModel.Dashboard.activeReport.reportContainer.id).show();
                            }
                            var activeReport = vm.DashboardModel.Dashboard.Report[vm.DashboardModel.Dashboard.activeReportIndex];
                            $("#li_"+ uiWidget[0].firstElementChild.id).addClass("li-box-border");
                            $scope.chartLoading=false;
                            $("#chart-"+$element.id).show();
                        },
                        start : function(event, uiWidget,$element) {
                            $("#chart-"+$element.id).hide();
                            //chart Loading Bar True
                            var variable="reportLoading_"+vm.DashboardModel.Dashboard.activeReport.reportContainer.id;
                            vm[variable]=true;
                            //Chart Hide
                            $(".label_legends"+vm.DashboardModel.Dashboard.activeReport.reportContainer.id).hide();
                            $("#chart-"+vm.DashboardModel.Dashboard.activeReport.reportContainer.id).hide();
                            $("#left"+vm.DashboardModel.Dashboard.activeReport.reportContainer.id).hide();
                        }
                    }
                },
                numberWidgetContainerSettings : {
                    id : 0,
                    name : "Number Widget",
                    sizeX : 6,
                    sizeY : 5,
                    minSizeX : 6,
                    minSizeY : 5,
                    maxSizeX : 6,
                    maxSizeY : 5
                },
                commonContainersSettings : {
                    id : 0,
                    name : "Report",
                    sizeX : 6,
                    sizeY : 8,
                    minSizeX : 6,
                    minSizeY : 8
                },
                Dashboard : {
                    activeReportIndex : null,
                    activeReport : null,
                    lastReportId : 0,
                    Report : []
                }
            };

            var report = [];
            vm.DashboardController = {

                init : function() {
                    vm.DashboardView.init();
                    vm.chartLoading = false;
                    // vm.showInfo[vm.DashboardModel.Dashboard.activeReport.reportContainer.id]=false;
                    // vm.chartErrorText={};
                    // Filter Apply
                    vm.checkboxModel = {};
                    vm.filterApplyAdd = function(keys) {
                        vm.blnkFilter = false;
                        var filerTemp = [];
                        vm.filtersToApply = [];
                        vm.filterHSApply = true;
                        var columnObj = "";
                        vm.checkboxModel[keys.columnName] = keys;
                        Object
                            .keys(vm.checkboxModel)
                            .forEach(
                                function(d) {
                                    if (vm.checkboxModel[d] != "0") {
                                        vm.filtersTo[d] = [];
                                        columnObj = vm.checkboxModel[d];
                                        var keyValue = {};
                                        keyValue['key'] = columnObj.columnName;
                                        keyValue['value'] = columnObj.columType;
                                        filerTemp.push(keyValue);
                                        Enumerable
                                            .From(vm.tableData)
                                            .Distinct("$."+ d)
                                            .Select("$."+ d)
                                            .ToArray()
                                            .forEach(
                                                function(
                                                    e) {
                                                    vm.filtersTo[d]
                                                        .push(e);
                                                });
                                    }
                                });
                        if (filerTemp.length) {
                            $scope.onApplyFilter = true;
                            $("#dash-border").css("margin-top",
                                "110px");
                        } else {
                            $scope.onApplyFilter = false;
                            $("#dash-border").css("margin-top",
                                "60px");
                        }
                        vm.filtersToApply = filerTemp;
                        setTimeout(
                            function() {
                                $('.multielect')
                                    .multiselect(
                                        {
                                            includeSelectAllOption : true,
                                            enableFiltering : true,
                                            maxHeight : 200
                                        });
                                $('.datepicker')
                                    .daterangepicker();
                                $('.datepicker')
                                    .on(
                                        'apply.daterangepicker',
                                        function(
                                            ev,
                                            picker) {
                                            setTimeout(
                                                function() {
                                                    alert($(
                                                        ".datepicker")
                                                        .val());
                                                    $(
                                                        '.datepicker')
                                                        .trigger(
                                                            "change");
                                                },
                                                1000);
                                        });
                            }, 1000);

                    }
                    vm.filter_add = function() {
                        $('.drawer1').drawer('open');
                    }
                    // Filter Hide Show
                    $scope.filterHideShow = function() {
                        $scope.filterHide = !$scope.filterHide;
                        $scope.filterShow = !$scope.filterShow;
                        $scope.filterHSApply = !$scope.filterHSApply;

                        if ($scope.filterShowHidden) {
                            $scope.filterShowHidden = 0
                        } else {
                            $scope.filterShowHidden = 1;
                        }
                    }

                },
                findIndexofActiveContainer : function(
                    arraytosearch, key, valuetosearch) {

                    for (var i = 0; i < arraytosearch.length; i++) {

                        if (arraytosearch[i].reportContainer[key] == valuetosearch) {

                            return i;
                        }
                    }
                    return null;

                },
                getGridSettings : function() {
                    return vm.DashboardModel.gridSettings;
                },
                getCommonContainerSettings: function () {
                    return {
                        name: "Report",
                        sizeX: 30,
                        sizeY: 15,
                        minSizeX: 9,
                        minSizeY: 9,
                        maxSizeX: 30,
                        maxSizeY: 15
                    };
                },
                getNumberContainerSettings: function () {
                    return {
                        name: "Number Widget",
                        sizeX: 8,
                        sizeY: 4,
                        minSizeX: 6,
                        minSizeY: 4,
                        maxSizeX: 16,
                        maxSizeY: 8,
                        chartType: "Number Widget"
                    };
                },
                onChartDropToContainer : function(event, index,item, external, type, allowedType) {
                    var view = vm.DashboardView;
                    dc.filterAll();
                    vm.isParameterShown = true;
                    $('.drawer').drawer('open');
                    view.removeGridBorder();
                    vm.rightSideView.render(item);
                    vm.DashboardController.addReportContainer(item);
                    vm.rightSideView.chartSelectView.update(item);
                    vm.dimesionColumn = vm.rightSideController.getDimensions();
                },

                onReportContainerClick : function(id) {
                	var index = vm.DashboardController.findIndexofActiveContainer(vm.DashboardModel.Dashboard.Report,"id", id);
                	
                    $scope.addChartIconBorder($scope.selectedChartInView[id]);
                    $.each(vm.DashboardModel.Dashboard.Report[index].axisConfig.checkboxModelDimension,function(key,value){
                    	if(value.key){
                    		delete value.key;
                        	delete value.value;
                    	}
                    	
                    });
                    $.each(vm.DashboardModel.Dashboard.Report[index].axisConfig.checkboxModelMeasure,function(key,value){
                    	if(value.key){
                    		delete value.key;
                        	delete value.value;
                    	}
                    	
                    });
                    vm.Attributes = vm.DashboardModel.Dashboard.Report[index].axisConfig;
                     
                    
                    vm.DashboardModel.Dashboard.activeReport = vm.DashboardModel.Dashboard.Report[index];
                    vm.selectedChart = vm.DashboardModel.Dashboard.Report[index].chartObject;
                },
                getDashboardProto : function() {
                    return vm.DashboardModel.dashboardPrototypeObject;
                },
                getReportProto : function() {

                    return {
                        reportContainer : {},
                        chart : {},
                        axisConfig : {}
                    };

                },
                addReportContainer : function(chartInfo) {
                	
                	var flag = true;
                    if ($scope.dashboardPrototype.reportContainers.length != 0)
                      vm.DashboardModel.Dashboard.Report.forEach(function(d,index){
                        if(!d.chartObject){
                            flag=false;
                        }
                    });
					/*if(flag)
					 {*/

                    var reportObject = vm.DashboardController.getReportProto();
              
                    reportObject.chart = chartInfo;

                    vm.DashboardModel.Dashboard.lastReportId++; 
                    this.lastId = vm.DashboardModel.Dashboard.lastReportId;
                    vm.dashboardPrototype.reportContainers['chartType']=chartInfo.name;
                    if (chartInfo.name == "Number Widget") {
                        vm.DashboardModel.numberWidgetContainerSettings.id = this.lastId;
                        var obj = vm.DashboardController
                            .getNumberContainerSettings();
                        obj['id'] = this.lastId;
                        //obj['chartType'] = chartInfo.name;
                        obj['row'] = 0;
                        obj['col'] = 0;
                        reportObject.reportContainer = obj;
                        vm.DashboardModel.Dashboard.activeReport.reportContainer = reportObject.reportContainer;
                        $scope.selectedChartInView[this.lastId] = sketch.chartData[0];
                    } else {
                        vm.DashboardModel.commonContainersSettings.id = this.lastId;
                        var obj = vm.DashboardController.getCommonContainerSettings();
                        obj['row'] = 0;
                        obj['col'] = 0;
                        obj['id'] = this.lastId;
                        obj['chartType'] = chartInfo.name;
                        reportObject.reportContainer = obj;

                        $scope.selectedChartInView[this.lastId] = sketch.chartData[0];
                        setTimeout(function(){
                            $(".li_box").removeClass("li-box-border");
                            $("#li_"+vm.DashboardModel.commonContainersSettings.id).addClass("li-box-border");
                        },1);

                    }
					/*
					 * var $target = $('html,body');
					 * $target.animate({scrollTop:
					 * $target.height()}, 1000);
					 */
                       if(flag)
                    	   {
                    reportObject.reportContainer['image'] = dataFactory
                            .baseUrlData()
                        + chartInfo.image;
                    vm.DashboardModel.Dashboard.activeReport = reportObject;
                    vm.DashboardView
                        .addReportContainerView(reportObject.reportContainer);
                    vm.aggregateText[reportObject.reportContainer.id] = "Sum";
                    report.push(reportObject);
                    vm.DashboardModel.Dashboard.Report
                        .push(reportObject);
                    vm.DashboardModel.Dashboard.activeReportIndex = vm.DashboardModel.Dashboard.Report.length - 1;
                    	   }
                       else
	  				   {
	                    	   dataFactory.errorAlert("can't add more reports till current is empty");
	  				   }
                },

                updateReport : function() {

                    vm.DashboardModel.Dashboard.activeReport.chart = JSON
                        .parse(vm.chartType);
                    // vm.DashboardModel.Dashboard.activeReportIndex

                },

                getActiveReport : function() {
                    return vm.DashboardModel.Dashboard.activeReport;
                }

            }

            vm.DashboardView = {

                init : function() {
                    // Gridster Settings
                    $scope.dashboardCommonSettings = vm.DashboardController
                        .getGridSettings();
                    $scope.onDrop = vm.DashboardController.onChartDropToContainer;
                    vm.DashboardView.initDashboardProto();
                    $scope.addBorder = function(id) {
                        /*$(".box").removeClass("box-border");*/
                        $(".li_box").removeClass("li-box-border");
                        $("#li_" + id).addClass("li-box-border");
                        /*$("#" + id).addClass("box-border");*/
                        vm.DashboardController.onReportContainerClick(id);
                    };
                    vm.numberFormate=function(style){
                        var styleSettings={};
                        styleSettings["NumberFormate"]=style;
                        styleSettings["type"]="Number Widget";
                        var activeDashboardIndex = vm.DashboardModel.Dashboard.activeReportIndex;
                        vm.DashboardModel.Dashboard.Report[activeDashboardIndex]['attributes'] = "Number Widget";
                        sketch.renderAttributes(vm.DashboardModel.Dashboard.Report[activeDashboardIndex].chartObject,styleSettings);
                    }
                    vm.saveChartStyle = function(styleSettings) {
                        var activeDashboardIndex = vm.DashboardModel.Dashboard.activeReportIndex;
                        vm.DashboardModel.Dashboard.Report[activeDashboardIndex]['attributes'] = styleSettings;
                        sketch.renderAttributes(
                                vm.DashboardModel.Dashboard.Report[activeDashboardIndex].chartObject,
                                styleSettings);
                        generate('success',
                            'Chart style apply successfully');
                    }
                },
                autoAdjustChartView : function() {
                    sketch.selfAdjustChart(vm.DashboardModel.Dashboard.activeReport);
                },
                render : function() {
                    vm.DashboardModel.Dashboard.Report.forEach(function(d) {
                        vm.DashboardView.addReportContainerView(d.reportContainer);
                        $scope.selectedChartInView[d.reportContainer.id]=d.chart;
                        if(d.chart.key=="_pivotTable"){
                        	sketch.pivotTableConfig=d.chartObject; 
                        }
                    });
                    setTimeout(function(){
                        vm.DashboardModel.Dashboard.Report.forEach(function(d,index) {
                    			sketch
                                .axisConfig(d.axisConfig)
                                .data(vm.tableData)
                                .container(d.reportContainer.id)
                                .chartConfig(d.chart)
                                .render();
                                if(index==0)
                                {
                                	vm.Attributes=d.axisConfig;
                            	}
                                d.reportContainer.chartType=d.chart.name;
                                if($stateParams.type==1){
                                	var width=$("#dashboardContainer_"+d.reportContainer.id).width();
                                    var height=$("#dashboardContainer_"+d.reportContainer.id).height();
                                    if(width<650){
                                    	$("#left"+d.reportContainer.id).hide();
                                    	$(".label_legends"+d.reportContainer.id).hide();
                                    }
                                }else{
                                	var width=$("#li_"+d.reportContainer.id).width();
                                    var height=$("#li_"+d.reportContainer.id).height();
                                    if(width<450){
                                    	$("#left"+d.reportContainer.id).hide();
                                    	$(".label_legends"+d.reportContainer.id).hide();
                                    }
                                }
                        });
                    },1000);
                
                },
                updateDashboardObject : function(drawn) {
                	  var index = vm.rightSideController
                        .findIndexofActiveContainer(
                            $scope.dashboardPrototype.reportContainers,
                            "id",
                            vm.DashboardModel.Dashboard.activeReport.reportContainer.id);
                    if (drawn) {
                        $scope.filterSave = true;
                        $scope.saveState = 1;
                        $scope.dashboardPrototype.reportContainers[index]['image'] = null;
                    }
                    // vm.rightSideController.setActiveAxisConfig(vm.chartSettings);
                    var indexOfReport = vm.DashboardController
                        .findIndexofActiveContainer(
                            vm.DashboardModel.Dashboard.Report,
                            "id",
                            vm.DashboardModel.Dashboard.activeReport.reportContainer.id);

                    vm.DashboardModel.Dashboard.Report[indexOfReport].axisConfig = new Object(
                        vm.Attributes);

                    vm.DashboardModel.Dashboard.Report[indexOfReport].axisConfig["xLabel"] = angular
                        .copy(vm.activeXLabel[vm.DashboardModel.Dashboard.activeReport.reportContainer.id]);
                    vm.DashboardModel.Dashboard.Report[indexOfReport].axisConfig["yLabel"] = angular
                        .copy(vm.activeYLabel[vm.DashboardModel.Dashboard.activeReport.reportContainer.id]);
                    if (drawn) {
                        // drawn["chartTypeAttribute"]=vm.DashboardModel.Dashboard.activeReport.chart.name;
                        // drawn["resize"]="ON";
                        vm.DashboardModel.Dashboard.Report[indexOfReport]["chartObject"] = drawn;
                    }
                    vm.DashboardModel.Dashboard.activeReport = vm.DashboardModel.Dashboard.Report[indexOfReport];
                    vm.DashboardModel.Dashboard.activeReportIndex = indexOfReport;

                },

                processChart : function() {
                    dc.filterAll();
                    vm.chartLoading =false;
                    return new Promise(
                        function(resolve, reject) {
                            try {
                         
                                var drawn = sketch
                                    .axisConfig(vm.Attributes)
                                    .data(vm.tableData)
                                    .container(vm.DashboardModel.Dashboard.activeReport.reportContainer.id)
                                    .chartConfig($scope.selectedChartInView[vm.DashboardModel.Dashboard.activeReport.reportContainer.id])
                                    .render(
                                        function(drawn) {

                                            $scope.lastDrawnChart[vm.DashboardModel.Dashboard.activeReport.reportContainer.id] = vm.DashboardModel.Dashboard.activeReport.chart.key;
                                            resolve(drawn);
                                        });
                            } catch (e) {

                            }
                        }
                    );
                },
                renderChartInActiveContainer : function() {
                	
                	try {
                        vm.DashboardView
                            .processChart()
                            .then(vm.DashboardView.updateDashboardObject)
                            .then(function() {
                                vm.chartLoading = false;
                                
                            })
                            .then(function() {
                            	
                            	var variable="reportLoading_"+vm.DashboardModel.Dashboard.activeReport.reportContainer.id;
                            	vm[variable]=false; 
                            	$scope.$apply();
                            	$(".label_legends"+vm.DashboardModel.Dashboard.activeReport.reportContainer.id).show();
                            	
                            });
                    } catch (e) {
                        vm.chartErrorText = "Some Error occured while drawing this chart";
                        vm.chartLoading = false;
                    }
                },
                initDashboardProto : function() {
                    $scope.dashboardPrototype = vm.DashboardController.getDashboardProto();
                },
                // render dashboard view
                addReportContainerView : function(reportContainerObject) {
                    $scope.dashboardPrototype.reportContainers.push(reportContainerObject);
                },
                selectReportContainerView : function(
                    reportContainerObject) {

                },
                showGridBorder : function() {
                    $("#dash-border").addClass("dash-border");
                },
                removeGridBorder : function() {
                    $("#dash-border")
                        .removeClass("dash-border");

                }
            }
            vm.DashboardController.init();
            $("#loadscreen").show();
            $scope.listdata = [];
            $scope.listOf = false;
            $scope.dashName.dashboardName = "";
            $scope.dashboardDesc = "Dashboard Description";
            vm.containerLoaded = false;
            vm.isSmallSidebar = true;
            vm.isParameterShown = false;
            vm.isNoContainerLoaded = true;
            vm.ischartTypeShown = false;
            vm.isQuerySelected = true;
            vm.placeholder = true;
            vm.filtersTo = [];
            vm.lastAppliedFilters = {};
            vm.saveState = -1;
            // charts list
            vm.MetaDataLoader = true;
            // Function for fetching metadata list
            var dashboardView = function() {
                var promise = new Promise(
                    function(resolve, reject) {

                      
                    });
                return promise;
            };
            dashboardView().then(function() {
                vm.MetaDataLoader = false;
            });
            $scope.fullNavShow = function() {
                $scope.fullNav = true;
                $scope.smallNav = true;
                $(".be-content").removeClass("toggleNav");
                onresize();
            }
            $scope.fullNavHide = function() {
                $scope.fullNav = false;
                $scope.smallNav = false;
                $(".be-content").addClass("toggleNav");
                onresize();
            }
            $scope.publicDashboard = function(d) {
                $location.path('/public-view').search({
                    param : JSON.stringify(d)
                });
            }
            $scope.viewDashboard = function(d) {
                $state.go("app.view-dashboard", {
                    param : JSON.stringify(d)
                });
            }
            $scope.EditDashboard = function(d) {
                $state.go("app.dashboard-edit", {
                    param : JSON.stringify(d)
                });
            }
            // Api For Getting Connection
            dataFactory.metaDataList("1").then(function(response) {
                vm.queryList = JSON.parse(response.data.result);
            }).then(function() {
                        if ($stateParams.data != undefined) {
                            var metadataName = $stateParams.data;
                            $(".modal-backdrop").hide();
                        }
                    }); 
            var fetchDataAndColumns = function() {
                var promise = new Promise(
                    function(resolve, reject) {
                    	
                        dataFactory.getQueryDataDashboard(JSON.stringify(vm.metadataObject),"dashboard")
                            .then(function(response) {
                            	if(response.data.error==false){
                            		vm.blnkData = true;
                                    vm.blnkFilter = true;
                                    vm.blnkChart = true;
                                    vm.blnkSource = true;
                                    vm.tableData = JSON.parse(response.data.result.tableData);
                                    vm.tableColumns = vm.metadataObject.connObject.column;
                                    // $scope.groupObject
                                    calculation
                                        .oldObject(vm.tableColumns)
                                        .newObject(JSON.parse(response.data.result.tableColumn))
                                        .groupObject(vm.group)
                                        .tabledata(vm.tableData)
                                        .uniqueArray()
                                        .columnCal()
                                        .columnCategory();
                                    vm.tableData = calculation._tabledata;
                                    setTimeout(function(){
                                        $("#filter").multiselect({
                                            includeSelectAllOption : false,
                                            enableFiltering : true,
                                            maxHeight : 200
                                        });
                                    },1000);
                                    resolve();
                            	}else{
                            		dataFactory.errorAlert("Check your connection");
                            	}
                                
                            });
                    });
                return promise;
            };
            
          
            $(document)
                .mouseup(
                    function(e) {
                        var container = $(".dc-chart");
                        var handle_s = $(".handle-s");
                        var handle_e = $(".handle-e");
                        var handle_n = $(".handle-n");
                        var handle_se = $(".handle-se");
                        var handle_w = $(".handle-w");
                        if (!(handle_s.is(e.target)
                            || handle_e
                                .is(e.target)
                            || handle_n
                                .is(e.target)
                            || handle_se
                                .is(e.target) || handle_w
                                .is(e.target))) {
                            if (!container.is(e.target) // if
                                // the
                                // target
                                // of
                                // the
                                // click
                                // isn't
                                // the
                                // container...
                                && container
                                    .has(e.target).length === 0) // ...
                            // nor
                            // a
                            // descendant
                            // of
                            // the
                            // container
                            {
                                /*$(".box").removeClass(
                                 "box-border");*/
                                /*$(".li_box")
                                 .removeClass(
                                 "li-box-border");*/
                            }
                        }
                    });

            /*
             * $('html').click(function() {
             * $(".box").removeClass("box-border"); });
             */

            $scope.chartClicked = function(id) {

            };

            $scope.clear = function() {
                if (!($scope.dashboardPrototype == undefined)) {
                    $scope.dashboardPrototype.reportContainers = [];
                    vm.DashboardModel.Dashboard.activeReport = {};
                    vm.DashboardModel.Dashboard.Report = [];
                }
            };

            // creating widgets

            var lastcontainerId;
            var lastcount = 0;
            var newDim = [];
            // Filter Drawer

            vm.filterBy = function(item, filter) {
                sketch.applyFilter(item, filter);
                // dc.renderAll();
            }

            var allAxisSelected = function() {
                if (vm.Xaxis == undefined
                    || vm.Yaxis == undefined
                    || vm.Maxis == undefined) {

                    return false;
                }
                return true;
            };

            $scope.imageTest = function() {

                html2canvas(
                    $("#dashboard-image"),
                    {
                        background : '#FFFFFF',
                        onrendered : function(canvas) {
                            // restore the old offscreen
                            // position

                            var img = canvas
                                .toDataURL("image/png");
                            var output = encodeURIComponent(img);
                            var newDate = new Date();
                            var image_name = $scope.dashboardName
                                + new Date().getTime();
                            var a = document
                                .createElement('a');
                            // toDataURL defaults to png, so
                            // we need to request a jpeg,
                            // then convert for file
                            // download.
                            a.href = canvas.toDataURL(
                                "image/png").replace(
                                "image/png",
                                "image/octet-stream");
                            a.download = 'somefilename.jpg';
                            a.click();
                            var Parameters = "image="
                                + output
                                + "&image_name="
                                + image_name;
                            $
                                .ajax(
                                    {
                                        type : "POST",
                                        url : "http://bi.thinklayer.com/api/newbi/data/dashboard/save-image.php",
                                        data : Parameters,
                                        success : function(
                                            data) {
                                            alert(data);
                                        }
                                    }).done(
                                function() {
                                    // $('body').html(data);
                                });
                            // Convert and download as image
                            // Canvas2Image.saveAsPNG(canvas);
                            // $("#img-out").append(canvas);
                            // Clean up
                            // document.body.removeChild(canvas);
                        }
                    }); 
            }
            // Rename keys
            vm.rename = {};
            $scope.renameKeys = function(name, type) {
            	$(".colorPalette").hide();
                var i = 0;
                $scope.typeRename = type;
                $.each(vm.tableColumns, function(index, value) {
                    if (value.reName == name) {
                        $scope.lastRenameIndex = i;
                        $scope.typeRename = type;
                        $scope.typeAxis = value.dataKey;
                    }
                    i++;
                });
                var modalElem = $('#myModal');
                $('#myModal').modal({
                    backdrop : 'static',
                    keyboard : false
                });
                $('#myModal').modal('show');
                vm.rename.reNameInput = name;
                $(".page").addClass(
                    "background-container");

            }

            // delete bhavesh
            $scope.deleteGraphKeys = function(id) {
                $scope.deleteReportContainer(id);
                alert("Delete Successfully");
            }

            $scope.findIndexOfReportContainer = function(id) {
                var index = -1;
                var i = 0;
                $scope.dashboardPrototype.reportContainers
                    .forEach(function(d) {
                        if (d.id == id)
                            index = i;
                        i++;
                    });

                return index;
            }
            $scope.findIndexOfReportFromReportContainer = function(
                id) {
                var index = -1;
                var i = 0;
                vm.DashboardModel.Dashboard.Report
                    .forEach(function(d) {
                        if (d.id == id)
                            index = i;
                        i++;
                    });

                return index;
            }

            $scope.deleteReportContainer = function(id) {

                var index1 = $scope
                    .findIndexOfReportFromReportContainer(id);
                var index = $scope
                    .findIndexOfReportContainer(id);
                vm.Attributes = {};

                $scope.dashboardPrototype.reportContainers
                    .splice(index, 1);
                vm.DashboardModel.Dashboard.Report.splice(
                    index1, 1);
            }
            vm.saveRename = function(rename) {
                if ($scope.typeRename == "Axis") {
                    var tempVar = {};
                    if ($scope.typeAxis == "Measure") {
                        tempVar = angular
                            .copy(vm.activeYLabel[vm.DashboardModel.Dashboard.activeReport.reportContainer.id]);
                        tempVar.reName = rename;
                        vm.activeYLabel[vm.DashboardModel.Dashboard.activeReport.reportContainer.id] = tempVar;
                    } else if ($scope.typeAxis == "Dimension") {
                        tempVar = angular
                            .copy(vm.activeXLabel[vm.DashboardModel.Dashboard.activeReport.reportContainer.id]);
                        tempVar.reName = rename;
                        vm.activeXLabel[vm.DashboardModel.Dashboard.activeReport.reportContainer.id] = tempVar;
                    }
                } else if ($scope.typeRename == "mainContainer") {
                    vm.DashboardModel.Dashboard.activeReport.reportContainer.name = rename;
                } else {
                    vm.tableColumns[$scope.lastRenameIndex]['reName'] = rename;
                }

                $('#myModal').modal('hide');
                $(".reportDashboard").removeClass('background-container');
            }

            vm.updateDashboard = function() {
            	
            	if ($scope.dashName.dashboardName == ""  || $scope.dashName.dashboardName == undefined) {
                    dataFactory.errorAlert("Please Enter Dashboard Name");
                    return;
                }
                $(".loader-overlay").show();
                var metadataObject = $scope.metadataObject;
                metadataObject['column'] = vm.tableColumns;
                vm.DashboardModel.Dashboard['customMeasure'] = vm.customMeasure;
                vm.DashboardModel.Dashboard['customGroup'] = vm.customGroup;
                var reportsObject = vm.DashboardModel.Dashboard;
                return;
                var newDate = new Date();
                var image_name = $scope.dashName.dashboardName+ new Date().getTime();
                html2canvas($("#dashboard-image"),
                    {
                        onrendered : function(canvas) {
                            var theCanvas = canvas;
                            // document.getElementById("dash-border").appendChild(canvas);
                            var img = canvas.toDataURL("image/png");
                            var output = encodeURIComponent(img);
                            dataFactory.dashboardImageSave(img,image_name).then(function(response) { 
                                	var dashSaveObj={};
                                    var dashRepObj={};
                                    var dObj="";
                                    reportsObject['metadataObject']=$scope.metadataObject;
                                    reportsObject['connectionObject']=$scope.metadataObject.connObject.connectionObject;
                                    reportsObject['colorObject']=eChart.chartRegistry.listAttributes();
                                    dataFactory.updateDashboard($scope.dashName.dashboardName,$scope.dashboardDesc,
                                            reportsObject,$scope.filterShowHidden,JSON.stringify(vm.checkboxModel),
                                            image_name,JSON.stringify(vm.categoryGroupObject),$scope.dashboardId).then(function(response) { 
                                            	if(response.data.error){
                                            		dataFactory.successAlert(response.data.message);
                                            	}else{
                                            		var id = response.data.result;
    		                                        $window.location = '#/dashboard/view/'+id+'/1';
    		                                        dataFactory.errorAlert("Dashboard Update Successfully");
    		                                        $(".loader-overlayLoding").hide();
                                            	}
		                                        
                                    });
                            });
                        }
                    });
            };
            $scope.closedashboard=function(){
                $window.location = '#/dashboard/'
            }
            vm.measureDimensionDropdown = function(id, index){
                $("#" + id).addClass("overflowRemove");
                if (id == "dimensionDiv"){
                    dropDownFixPosition($('.buttonDropdownDimension'+ index), $('.custDropdownDimension'));
                } else {
                    dropDownFixPosition($('.buttonDropdownMeasure' + index), $('.custDropdownMeasure'));
                }
            }
            function dropDownFixPosition(button, dropdown) {
                var dropDownTop = button.offset().top + button.outerHeight();
                dropdown.css('top', dropDownTop + "px");
                dropdown.css('left', (button.offset().left) - 160 + "px");
            }
            $(window).click(
                function() {
                    $("#measureDiv").removeClass(
                        "overflowRemove");
                    $("#dimensionDiv").removeClass(
                        "overflowRemove");

                });
            function onscrollDrawer() {

                $("#measureDiv").removeClass("overflowRemove");
                $("#dimensionDiv")
                    .removeClass("overflowRemove");
                $(".dropdown-menu").hide();
            }
            vm.removeFilter = function(index) {
                vm.filtersToApply.splice(index, 1);
            }

            vm.applyLineBar = function(lineBar) {
                sketch._expandLineBarAxisConfig(lineBar);
            }
            //Model box close
            $scope.modelClose = function() {
                $(".reportDashboard").removeClass(
                    "background-container");
            }
            /* $scope.closeDashboard=function(){
                $window.location = '/#/dashboard/'
            }*/
            //Get array for rage picker
            vm.getArrayList=function(numberColumn){
            	var numberArray=[];
            	for(var i=1;i<=numberColumn;i++){
            		numberArray.push(i);
            	} 
            	return numberArray;
            }
            //Range Column and index get
            $scope.filterRangeColumn=function(index,columnName){
            	$scope.filterColumnRange={};
            	$scope.filterColumnRange['index']=index;
            	$scope.filterColumnRange['columnName']=columnName;
            }
           //Get all data and chart draw
            vm.isTimeObjectFound=function(reportArray){
            	var flag=-1;
            	reportArray.forEach(function(d,index){
            		if(d.axisConfig.timeline!=undefined){
            			flag=index;
            		}
            	});
            	return flag; 
            }
            dataFactory.dashboardIdGetObject($scope.dashboardId).then(function(response) {
            	try{
	               $scope.dashName.dashboardName=response.data.result.name;
	               
	               var reportObject=JSON.parse(response.data.result.reportObject);
	               var index =vm.isTimeObjectFound(reportObject.Report);
	               if(index!=-1){
	            	   vm.rangeObject=reportObject.Report[index].axisConfig.timeline;
	               }
	               vm.colorObject=reportObject.colorObject;
	               eChart.chartRegistry.registerAllAttributes(reportObject.colorObject);
               }
               catch(e){
            	   
               }
               //delete reportObject.metadataObject.column;
               vm.querySelector=reportObject.metadataObject;
               vm.metadataSelect={};
               vm.metadataSelect.name=reportObject.metadataObject.metadataId;
               vm.selectDataSource(vm.querySelector);
               vm.DashboardModel.Dashboard=reportObject;
               vm.DashboardModel.Dashboard.Report.forEach(function(d){
            	   if(d.axisConfig.xLabel){
            		   delete d.axisConfig.xLabel.key;
                	   delete d.axisConfig.xLabel.value;
                	   delete d.axisConfig.yLabel.key;
                	   delete d.axisConfig.yLabel.value;
            	   }
               });
               
               vm.metadataObject=reportObject.metadataObject;
               vm.group=JSON.parse(response.data.result.group); 
              
               fetchDataAndColumns().then(function(){
                   vm.DashboardView.render();
                   vm.checkboxModel=JSON.parse(response.data.result.filterBy);
                   
                    vm.filterAdd();
                  
                   //$scope.filterColumnName=
                   $scope.$apply();
               }).then(function(){
            		$scope.loadingBar=true; 
    	 	 	 	$scope.loadingBarView=true;
    	 	 	 	var rawData=vm.tableData;
    	 	 
    	 	 	 	
    	 	 	 	setTimeout(function(){
    	 	 	 		
    	           		$('.dateTimeLine').daterangepicker(
    	           		{ 
    	           		    locale: {
    	           		      format: 'DD-MM-YYYY'
    	           		    }
    	           		},
    	           		function(start, end, label) {
    	           			var date={};
    	           			date['start']=start.format('YYYY-MM-DD');
    	           			date['end']=end.format('YYYY-MM-DD');
    	           			var columnName=$scope.filterColumnName;
    	           			//Date
    	           			
    	           			var columnName=$scope.filterColumnRange.columnName; 
    	           			var index=$scope.filterColumnRange.index;
    	           			vm.rangeObject[columnName].period[index]=date;
    	           		
    	                   	//End
    	           			//vm.DashboardView.renderChartInActiveContainer();
    	           			var rawData=vm.tableData;
                        	if(!$.isEmptyObject(vm.rangeObject)){
                        		  sketch.getTimeLineFilteredData(vm.tableData,vm.rangeObject,function(data){
                        			    vm.DashboardModel.Dashboard.Report.forEach(function(obj,i) {
                                        var d=vm.DashboardModel.Dashboard.Report[i];
                                       
                                  		vm.Attributes=d.axisConfig;
                                  	
                                      	d.axisConfig['timeline']=vm.rangeObject;
                                              sketch
                                              .axisConfig(d.axisConfig)
                                              .data(data)
                                              .container(d.reportContainer.id)
                                              .chartConfig(d.chart)
                                              .render();
                                      });
                        		  });
                        	}
                        	
    	           			
    	           			//sketch.applyFilter(date,columnName);
    	           		});
    	           		for(var i=0;i<2;i++){
    	           			var d = new Date();
    	               		var n = d.getFullYear();
    	               		var year =n-i;
    	               		var start = "01-01-"+year;
    	                   	var end = "31-12-"+year;
    	                   	$(".dateTimeLine"+i).val(start+" - "+end);
    	           		}
    	           		//vm.DashboardView.renderChartInActiveContainer();
    	           		
    	           		if(!$.isEmptyObject(vm.rangeObject)){
                  		  sketch.getTimeLineFilteredData(vm.tableData,vm.rangeObject,function(data){
                  		
                  			  vm.DashboardModel.Dashboard.Report.forEach(function(d,i) {
                                        d.axisConfig['timeline']=vm.rangeObject;
                                        sketch
                                        .axisConfig(d.axisConfig)
                                        .data(data)
                                        .container(d.reportContainer.id)
                                        .chartConfig(d.chart)
                                        .render();
                                });
                  		  });
                  	}
    	           		if(vm.rangeObject)
    	           		vm.Attributes['timeline']=vm.rangeObject;
    	           		
    	           	},500);
               });
                //vm.Attributes=;
              
            });
          
        } ]);