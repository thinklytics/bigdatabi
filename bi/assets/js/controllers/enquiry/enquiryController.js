
'use strict';
/* Controllers */
angular.module('app', [ 'ngSanitize' ])
    .controller('enquiryController',['$scope','$sce','dataFactory','$rootScope','$state','$cookieStore','$location', '$window',
        function($scope,$sce, dataFactory,$rootScope,$state,$cookieStore,$location, $window) {
            //--- Top Menu Bar

            //Current page path
            var data={};
            var addClass='';
            var viewClass='';
            if($location.path()=='/datasource/'){
                viewClass='activeli';
            }else{
                addClass='activeli';
            }
            data['navTitle']="Enquiry Details";
            data['icon']="fa fa-pencil-square-o ";
            data['navigation']=[{
                "name":"Add",
                "url":"#/datasource/add",
                "class":addClass
            },
                {
                    "name":"View",
                    "url":"#/datasource/",
                    "class":viewClass
                }];
            $rootScope.$broadcast('subNavBar', data);
            $scope.subNavBarMenu=true;
            //End Menu
            var vm = $scope;
            //$(".loadingBar").show();
            vm.enquiryListUrl=function () {
                dataFactory.request($rootScope.EnquiryList_Url,'post',"").then(function(response){
                    if(response.data.errorCode==1){
                        $(".loadingBar").hide();
                        vm.enquiryList=response.data.result;
                    }else{
                        dataFactory.errorAlert("Check your conection");
                    }
                }).then(function () {
                    setTimeout(function () {
                        $("#enquiry").dataTable();
                    },1000);
                });
            }
            vm.enquiryListUrl();
            /*
                Delete 
             */
            vm.delete=function (id) {
                var data={
                    "enquiry_id":id
                };
                $(".loadingBar").show();
                dataFactory.request($rootScope.EnquiryDelete_Url,'post',data).then(function(response){
                    if(response.data.errorCode==1){
                        $(".loadingBar").hide();
                        dataFactory.successAlert(response.data.message);
                    }else{
                        dataFactory.errorAlert("Check your conection");
                    }
                }).then(function () {
                    vm.enquiryListUrl();
                    setTimeout(function () {
                        $("#enquiry").dataTable();
                    },1000);
                });
            }
        } ]);

