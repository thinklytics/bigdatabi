'use strict';
/* Controllers */
angular.module('app', [ 'ngSanitize' ])
    .controller('enquiryEditController',['$scope','$sce','dataFactory','$rootScope','$state','$cookieStore','$location', '$window','$stateParams',
        function($scope,$sce, dataFactory,$rootScope,$state,$cookieStore,$location, $window,$stateParams) {
            //--- Top Menu Bar

            //Current page path
            var data={};
            var addClass='';
            var viewClass='';
            if($location.path()=='/datasource/'){
                viewClass='activeli';
            }else{
                addClass='activeli';
            }
            data['navTitle']="Enquiry Details";
            data['icon']="fa fa-pencil-square-o ";
            data['navigation']=[{
                "name":"Add",
                "url":"#/datasource/add",
                "class":addClass
            },
            {
                "name":"View",
                "url":"#/datasource/",
                "class":viewClass
            }];
            $rootScope.$broadcast('subNavBar', data);
            $scope.subNavBarMenu=true;
            $scope.id=$stateParams.id;
            //End Menu
            var vm = $scope;
            $('.date').datepicker({dateFormat: 'dd-mm-yy'});

            vm.EnquiryDetails={
                update:function (enquiry) {
                    $(".loadingBar").show();
                    var enquiry={
                        "id":$scope.id,
                        "name":vm.enquiry.name,
                        "email":vm.enquiry.email_id,
                        "phone":vm.enquiry.phone,
                        "description":vm.enquiry.description,
                        "address":vm.enquiry.address,
                        "pincode":vm.enquiry.pincode,
                        "city":vm.enquiry.city,
                        "state":vm.enquiry.state,
                        "country":vm.enquiry.country,
                        "enquirydate":vm.enquiry.enquiry_date,
                    };
                    var data={
                        "enquiry":enquiry
                    }
                    dataFactory.request($rootScope.EnquiryDetailsUpdate_Url,'post',data).then(function(response){
                        if(response.data.errorCode==1){
                            dataFactory.successAlert(response.data.message);
                            $window.location = '#/enquiry/details';
                        }else{

                        }
                    });
                },
                enquiryObj:function () {
                    var data={
                        "enquiry_id":$scope.id
                    }
                    dataFactory.request($rootScope.EnquiryObj_Url,'post',data).then(function(response){
                        if(response.data.errorCode==1){
                            vm.enquiry=response.data.result;
                            vm.enquiry.phone=parseInt(vm.enquiry.phone);
                            vm.enquiry.pincode=parseInt(vm.enquiry.pincode);
                            $(".loadingBar").hide();
                        }else{
                            dataFactory.errorAlert("Check your conection");
                        }
                    });
                }
            }
            vm.EnquiryDetails.enquiryObj();
        } ]);

