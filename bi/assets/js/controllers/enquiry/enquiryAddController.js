
'use strict';
/* Controllers */
angular.module('app', [ 'ngSanitize' ])
    .controller('enquiryAddController',['$scope','$sce','dataFactory','$rootScope','$state','$cookieStore','$location', '$window',
        function($scope,$sce, dataFactory,$rootScope,$state,$cookieStore,$location, $window) {
            //--- Top Menu Bar
//*********************************************Datasource add************************************************************


            //Current page path
            var data={};
            var addClass='';
            var viewClass='';
            if($location.path()=='/datasource/'){
                viewClass='activeli';
            }else{
                addClass='activeli';
            }
            data['navTitle']="Enquiry Details";
            data['icon']="fa fa-pencil-square-o";
            data['navigation']=[{
                "name":"Add",
                "url":"#/datasource/add",
                "class":addClass
            },
                {
                    "name":"View",
                    "url":"#/datasource/",
                    "class":viewClass
                }];
            $rootScope.$broadcast('subNavBar', data);
            $scope.subNavBarMenu=true;
            //End Menu
            var vm = $scope;

            $('.date').datepicker({dateFormat: 'dd-mm-yy'});
            vm.EnquiryDetails={
                add:function () {
                    $(".loadingBar").show();
                    var enquiry={
                        "name":vm.enquiry.name,
                        "email":vm.enquiry.email_id,
                        "phone":vm.enquiry.phone,
                        "description":vm.enquiry.description,
                        "address":vm.enquiry.address,
                        "pincode":vm.enquiry.pincode,
                        "city":vm.enquiry.city,
                        "state":vm.enquiry.state,
                        "country":vm.enquiry.country,
                        "enquirydate":vm.enquiry.enquiry_date,
                    };
                    var data={
                        "enquiry":enquiry
                    };
                    dataFactory.request($rootScope.EnquiryDetailsSave_Url,'post',data).then(function(response){
                        $(".loadingBar").hide();
                        if(response.data.errorCode==1){
                            vm.enquiry={};
                            dataFactory.successAlert(response.data.message);
                            $window.location = '#/enquiry/details';
                        }else{
                            dataFactory.errorAlert(response.data.message);
                        }
                    });
                }
            }
        } ]);

