'use strict';
angular.module('app')
.controller('NavbarCtrl', ['$scope', '$location','AuthenticationService', function($scope, $location,AuthenticationService) {
	$scope.navBarMenu=false;
	$scope.$on('navBar', function (event, data) {
		$scope.navTitle=data.navTitle; 
		$scope.navIcon=data.navIcon; 
		$scope.navBarMenu=true;
	});
	 $scope.logout =function(){
		 AuthenticationService.ClearCredentials();
		 $location.path('/Login');
		 $scope.navBarMenu=false;
	 }
	 $( document ).ready(function() {
		 $('.listhover').removeClass('bold');
		  var attrId = '2';
		  $('#'+attrId).addClass('bold');
	  });
	 $('.listhover').on('click',function(){
		 $('.listhover').removeClass('bold');
		  var attrId =  $( this ).attr("id");
		  $('#'+attrId).addClass('bold');
	  });
}]);