
'use strict';
/* Controllers */
angular.module('app', [ 'ngSanitize' ])
    .controller('companyAddController',['$scope','$sce','dataFactory','$rootScope','$state','$cookieStore','$location', '$window',
        function($scope,$sce, dataFactory,$rootScope,$state,$cookieStore,$location, $window) {
            //--- Top Menu Bar
//*********************************************Datasource add************************************************************


            //Current page path
            var data={};
            var addClass='';
            var viewClass='';
            if($location.path()=='/datasource/'){
                viewClass='activeli';
            }else{
                addClass='activeli';
            }
            data['navTitle']="COMPANY";
            data['icon']="fa fa-building";
            data['navigation']=[{
                "name":"Add",
                "url":"#/datasource/add",
                "class":addClass
            },
                {
                    "name":"View",
                    "url":"#/datasource/",
                    "class":viewClass
                }];
            $rootScope.$broadcast('subNavBar', data);
            $scope.subNavBarMenu=true;
            //End Menu
            var vm = $scope;
            vm.profileImg={};
            $('.date').datepicker({dateFormat: 'dd-mm-yy'});
            vm.CompanyDetails={
                add:function () {
                    $(".loadingBar").show();
                    var profile={
                        "address":vm.profile.address,
                        "city":vm.profile.city,
                        "country":vm.profile.country,
                        "dbName":vm.profile.dbName,
                        "description":vm.profile.description,
                        "email":vm.profile.email,
                        "name":vm.profile.name,
                        "phone":vm.profile.phone,
                        "pincode":vm.profile.pincode,
                        "plan_id":vm.profile.plan_id,
                        "startdate":vm.profile.startdate,
                        "state":vm.profile.state
                    };
                    var data={
                        "profile":profile
                        //"logo":profileImg
                    };
                    dataFactory.request($rootScope.CompanyDetailsSave_Url,'post',data).then(function(response){
                        $(".loadingBar").hide();
                        if(response.data.errorCode==1){
                            vm.profile={};
                            dataFactory.successAlert(response.data.message);
                            $window.location = '#/company/details';
                        }else{
                            dataFactory.errorAlert(response.data.message);
                        }
                    });
                }
            }
        } ]);

