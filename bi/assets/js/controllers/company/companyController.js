
'use strict';
/* Controllers */
angular.module('app', [ 'ngSanitize' ])
    .controller('companyController',['$scope','$sce','dataFactory','$rootScope','$state','$cookieStore','$location', '$window',
        function($scope,$sce, dataFactory,$rootScope,$state,$cookieStore,$location, $window) {
            //--- Top Menu Bar
//*********************************************Datasource add************************************************************


            //Current page path
            var data={};
            var addClass='';
            var viewClass='';
            if($location.path()=='/datasource/'){
                viewClass='activeli';
            }else{
                addClass='activeli';
            }
            data['navTitle']="COMPANY";
            data['icon']="fa fa-building";
            data['navigation']=[{
                "name":"Add",
                "url":"#/datasource/add",
                "class":addClass
            },
                {
                    "name":"View",
                    "url":"#/datasource/",
                    "class":viewClass
                }];
            $rootScope.$broadcast('subNavBar', data);
            $scope.subNavBarMenu=true;
            //End Menu
            var vm = $scope;
            //$(".loadingBar").show();
            vm.companyListUrl=function () {
                dataFactory.request($rootScope.CompanyList_Url,'post',"").then(function(response){
                    if(response.data.errorCode==1){
                        $(".loadingBar").hide();
                        vm.companyList=response.data.result;
                    }else{
                        dataFactory.errorAlert("Check your conection");
                    }
                }).then(function () {
                    setTimeout(function () {
                        $("#company").dataTable();
                    },1000);
                });
            }
            vm.companyListUrl();


            $scope.showSwal = function(type, id){
                if(type == 'warning-message-and-cancel') {
                    swal({
                        title: 'Are you sure?',
                        text: 'You want to Delete !',
                        type: 'warning',
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        cancelButtonText: 'No',
                        confirmButtonClass: "btn btn-success",
                        cancelButtonClass: "btn btn-danger",
                        buttonsStyling: false,
                    }).then(function(value){
                            if(value){
                                vm.delete(id);
                            }
                        },
                        function (dismiss) {

                        })
                }
            }

            /*
                Delete 
             */
            vm.delete=function (id) {
                var data={
                    "company_id":id
                };
                $(".loadingBar").show();
                dataFactory.request($rootScope.CompanyDelete_Url,'post',data).then(function(response){
                    if(response.data.errorCode==1){
                        $(".loadingBar").hide();
                        dataFactory.successAlert(response.data.message);
                    }else{
                        dataFactory.errorAlert("Check your conection");
                    }
                }).then(function () {
                    vm.companyListUrl();
                    setTimeout(function () {
                        $("#company").dataTable();
                    },1000);
                });
            }
        } ]);

