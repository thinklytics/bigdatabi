'use strict';
/* Controllers */
angular.module('app', [ 'ngSanitize' ])
    .controller('companyEditController',['$scope','$sce','dataFactory','$rootScope','$state','$cookieStore','$location', '$window','$stateParams',
        function($scope,$sce, dataFactory,$rootScope,$state,$cookieStore,$location, $window,$stateParams) {
            //--- Top Menu Bar
//*********************************************Datasource add************************************************************
            //Current page path
            var data={};
            var addClass='';
            var viewClass='';
            if($location.path()=='/datasource/'){
                viewClass='activeli';
            }else{
                addClass='activeli';
            }
            data['navTitle']="COMPANY";
            data['icon']="fa fa-building";
            data['navigation']=[{
                "name":"Add",
                "url":"#/datasource/add",
                "class":addClass
            },
            {
                "name":"View",
                "url":"#/datasource/",
                "class":viewClass
            }];
            $rootScope.$broadcast('subNavBar', data);
            $scope.subNavBarMenu=true;
            $scope.id=$stateParams.id;
            //End Menu
            var vm = $scope;
            $('.date').datepicker({dateFormat: 'dd-mm-yy'});
            vm.profileImg={};
            vm.CompanyDetails={
                update:function (profile,profileImg) {
                    $(".loadingBar").show();
                    var profile={
                        "id":$scope.id,
                        "address":vm.profile.address,
                        "city":vm.profile.city,
                        "country":vm.profile.country,
                        "description":vm.profile.description,
                        "email":vm.profile.email_id,
                        "name":vm.profile.name,
                        "phone":vm.profile.phone,
                        "pincode":vm.profile.pincode,
                        "startdate":vm.profile.start_date,
                        "state":vm.profile.state,
                        "status_id":vm.profile.status_id
                    };
                    var flag=false;
                    Object.keys(profile).forEach(function (d) {
                        if(profile[d]==undefined || profile[d]==""){
                            flag=true;
                            dataFactory.errorAlert(d + " is required");
                        }
                    });
                    if(flag){
                        $(".loadingBar").hide();
                        return false;
                    }
                    var data={
                        "profile":profile
                        //"logo":profileImg
                    }
                    dataFactory.request($rootScope.CompanyDetailsUpdate_Url,'post',data).then(function(response){
                        if(response.data.errorCode==1){
                            dataFactory.successAlert(response.data.message);
                            $window.location = '#/company/details';
                        }else{

                        }
                    });
                },
                compObj:function () {
                    var data={
                        "company_id":$scope.id
                    }
                    dataFactory.request($rootScope.CompanyObj_Url,'post',data).then(function(response){
                        if(response.data.errorCode==1){
                            vm.profile=response.data.result;
                            vm.profile.port=parseInt(vm.profile.port);
                            vm.profile.phone=parseInt(vm.profile.phone);
                            vm.profile.pincode=parseInt(vm.profile.pincode);
                            $(".loadingBar").hide();
                            //dataFactory.successAlert(response.data.message);
                        }else{
                            dataFactory.errorAlert("Check your conection");
                        }
                    });
                }
            }
            vm.CompanyDetails.compObj();
        } ]);

