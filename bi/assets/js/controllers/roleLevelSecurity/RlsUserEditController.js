angular.module('app', ['ngSanitize'])
    .controller('RlsUserEditController', ['$scope', '$sce', 'dataFactory', '$filter', '$stateParams', '$location', '$window', '$rootScope','$cookieStore',
        function ($scope, $sce, dataFactory, $filter, $stateParams, $location, $window, $rootScope,$cookieStore) {
            var vm=$scope;
            var data={};
            data['navTitle']="ROLE LEVEL USER";
            data['icon']="fa fa-user";
            $rootScope.$broadcast('subNavBar', data);
            vm.id=$stateParams.id;
            var data={
                "id":vm.id
            };
            vm.rlsObj={};
            dataFactory.request($rootScope.rlsList_Url,'post',"").then(function(response){
                vm.rlsList=response.data.result.rlsList;
                vm.userList=response.data.result.userList;
            }).then(function () {
                dataFactory.request($rootScope.rlsUserById_Url,'post',data).then(function(response){
                    vm.rlsObj.userid=[];
                    Object.keys(response.data.result).forEach(function(d){
                        vm.rlsObj.rlsId=d;
                        response.data.result[d]['user'].forEach(function(e){
                            vm.rlsObj.userid.push(JSON.stringify(e));
                        });
                    })
                }).then(function () {
                    setTimeout(function(){
                        $(".commonSelect").selectpicker();
                    },100);
                });
            });
            vm.update=function(rlsObj){
                var data={
                    "rlsObj":rlsObj
                };
                dataFactory.request($rootScope.rlsUserUpdate_Url,'post',data).then(function(response){
                    if(response.data.errorCode==1){
                        dataFactory.successAlert(response.data.message);
                        $window.location = '#/setting/role_security';
                    }else{
                        dataFactory.errorAlert(response.data.message);
                    }
                });
            }
        }]);

