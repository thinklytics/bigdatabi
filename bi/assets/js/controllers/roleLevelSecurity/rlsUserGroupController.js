angular.module('app', ['ngSanitize'])
    .controller('RlsUserGroupController', ['$scope', '$sce', 'dataFactory', '$filter', '$stateParams', '$location', '$window', '$rootScope','$cookieStore',
        function ($scope, $sce, dataFactory, $filter, $stateParams, $location, $window, $rootScope,$cookieStore) {
            var vm=$scope;
            var data={};
            data['navTitle']="RLS USER GROUP";
            data['icon']="fa fa-user";
            $rootScope.$broadcast('subNavBar', data);
            vm.rlsObj={};
            dataFactory.request($rootScope.rlsListWithGroup_Url,'post',"").then(function(response){
                vm.rlsList=response.data.result.rlsList;
                vm.userGroup=response.data.result.userGroup;
            }).then(function () {
                setTimeout(function(){
                    $(".commonSelect").selectpicker();
                },100);
            });
            vm.save=function(rlsObj){
                var data={
                    "rlsObj":rlsObj
                };
                dataFactory.request($rootScope.rlsUserGroupSave_Url,'post',data).then(function(response){
                    if(response.data.errorCode==1){
                        $window.location = '#/setting/role_security';
                        dataFactory.successAlert(response.data.message);
                    }else{
                        dataFactory.errorAlert(response.data.message);
                    }
                });
            }
        }]);

