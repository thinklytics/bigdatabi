angular.module('app', ['ngSanitize'])
    .controller('RlsUserGroupEditController', ['$scope', '$sce', 'dataFactory', '$filter', '$stateParams', '$location', '$window', '$rootScope','$cookieStore',
        function ($scope, $sce, dataFactory, $filter, $stateParams, $location, $window, $rootScope,$cookieStore) {
            var vm=$scope;
            var data={};
            data['navTitle']="RLS USER GROUP";
            data['icon']="fa fa-user";
            $rootScope.$broadcast('subNavBar', data);
            vm.rlsObj={};
            dataFactory.request($rootScope.rlsListWithGroup_Url,'post',"").then(function(response){
                vm.rlsList=response.data.result.rlsList;
                vm.userGroup=response.data.result.userGroup;
            }).then(function () {
                var data={
                    "id":$scope.id = $stateParams.id
                };
                dataFactory.request($rootScope.rlsUserGroupById_Url,'post',data).then(function(response){
                    Object.keys(response.data.result).forEach(function(d){
                        vm.rlsObj.rlsId=d;
                        var userGroupArray=[];
                        response.data.result[d].userGroup.forEach(function(d){
                            userGroupArray.push(JSON.stringify(d));
                        });
                        vm.rlsObj.userid=userGroupArray;
                    })
                }).then(function () {
                    setTimeout(function(){
                        $(".commonSelect").selectpicker();
                    },100);
                });
            });
            vm.update=function(rlsObj){
                var data={
                    "rlsObj":rlsObj
                };
                dataFactory.request($rootScope.rlsUserGroupUpdate_Url,'post',data).then(function(response){
                    if(response.data.errorCode==1){
                        $window.location = '#/setting/role_security';
                        dataFactory.successAlert(response.data.message);
                    }else{
                        dataFactory.errorAlert(response.data.message);
                    }
                });
            }
        }]);

