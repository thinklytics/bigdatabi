angular.module('app', ['ngSanitize'])
    .controller('RlsUserController', ['$scope', '$sce', 'dataFactory', '$filter', '$stateParams', '$location', '$window', '$rootScope','$cookieStore',
        function ($scope, $sce, dataFactory, $filter, $stateParams, $location, $window, $rootScope,$cookieStore) {
            var vm=$scope;
            var data={};
            data['navTitle']="ROLE LEVEL USER";
            data['icon']="fa fa-user";
            $rootScope.$broadcast('subNavBar', data);
            vm.rlsObj={};
            dataFactory.request($rootScope.rlsList_Url,'post',"").then(function(response){
                vm.rlsList=response.data.result.rlsList;
                vm.userList=response.data.result.userList;
            }).then(function () {
                setTimeout(function(){
                    $(".commonSelect").selectpicker();
                },100);
            });
            vm.save=function(rlsObj){
                var data={
                    "rlsObj":rlsObj
                };
                dataFactory.request($rootScope.rlsUserSave_Url,'post',data).then(function(response){
                    if(response.data.errorCode==1){
                        dataFactory.successAlert(response.data.message);
                        $window.location = '#/setting/role_security';
                    }else{
                        dataFactory.errorAlert(response.data.message);
                    }
                });
            }
}]);

