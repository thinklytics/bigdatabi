angular.module('app', ['ngSanitize'])
    .controller('roleLevelSecurityController', ['$scope', '$sce', 'dataFactory', '$filter', '$stateParams', '$location', '$window', '$rootScope','$cookieStore',
        function ($scope, $sce, dataFactory, $filter, $stateParams, $location, $window, $rootScope,$cookieStore) {
            var vm=$scope;
            var data={};
            data['navTitle']="ROLE LEVEL SECURITY";
            data['icon']="fa fa-user";
            $rootScope.$broadcast('subNavBar', data);
            vm.tableFilterCondition=0;
            /*
             * User group
             */
            vm.rlsObj={};
            dataFactory.request($rootScope.rlsListWithGroup_Url,'post',"").then(function(response){
                vm.userGroup=response.data.result.userGroup;
            }).then(function () {
                setTimeout(function(){
                    $(".userGroup").selectpicker();
                },100);
            });
            vm.setUser = function (userGroupId) {
                var data={
                    'userGroupId':userGroupId
                };

                dataFactory.request($rootScope.roleLevelList_Url,'post',data).then(function(response){
                    vm.userList=response.data.result.userList;
                    vm.sharedviewList=response.data.result.sharedViewList;

                }).then(function () {
                    setTimeout(function(){
                        vm.rlsObj.usersObj=[];
                        vm.userList.forEach(function(d){
                            vm.rlsObj.usersObj.push(d.id.toString());
                        });
                        $scope.$apply();
                        //console.log(vm.rlsObj.users);
                        $(".userList").selectpicker('refresh');
                        $(".sharedviewList").selectpicker('refresh');
                    },100);
                });
            }
            vm.publicviewSelect=function(obj){
                var reportMetadata=JSON.parse(obj);
                if(reportMetadata.metadataObject && reportMetadata.metadataObject.connObject && reportMetadata.metadataObject.connObject.type){
                    vm.MetadataConnType=reportMetadata.metadataObject.connObject.type;
                }
                $(".loadingBar").show();
                var data={
                    "id":JSON.parse(obj).id
                };
                vm.filterObj={};
                vm.formula="";
                vm.tableFilterCondition=1;
                dataFactory.request($rootScope.getTableColumn_Url,'post',data).then(function(response){
                    console.log(response)
                    var tempObj = JSON.parse((response.data.result));
                    vm.tableColObj={};
                    $.each(tempObj, function(key,val){
                        if(vm.tableColObj[val.tableName]==undefined){
                            vm.tableColObj[val.tableName]=[];
                            vm.tableColObj[val.tableName].push(val);
                        }else{
                            vm.tableColObj[val.tableName].push(val);
                        }
                    });
                    vm.sharedviewList=response.data.result;
                }).then(function(){
                    $(".loadingBar").hide();
                });
            }
            vm.tableFilter=function(table,column){
                if(table!=""){
                    if(vm.MetadataConnType!=undefined){
                        if(vm.lastTable==undefined){
                            vm.lastTable=table;
                            vm.formula="";
                        }else if(vm.lastTable!=table){
                            vm.formula="";
                        }
                        re = /\((.*)\)/;
                        var test1=column.match(re);
                        if(test1!=null && test1.length && table==test1[1]){
                            var tempVar="("+test1[1]+")";
                            column=column.replace(tempVar,"");
                        }
                        vm.lastTable=table;
                        vm.formula +=column;
                    }else{
                        if(vm.lastTable==undefined){
                            vm.lastTable=table;
                            vm.formula="";
                        }else if(vm.lastTable!=table){
                            vm.formula="";
                        }
                        re = /\((.*)\)/;
                        var test1=column.match(re);
                        if(test1!=null && test1.length && table==test1[1]){
                            var tempVar="("+test1[1]+")";
                            column=column.replace(tempVar,"");
                        }
                        vm.lastTable=table;
                        vm.formula +=table+"."+column;
                    }
                }else{
                    vm.formula +=" "+column+" ";
                }
            }
            vm.updateFormula=function(formula){
                vm.formula=formula;
            }
            vm.filterSave=function(){
                vm.filterBtn=0;
                if(vm.formula!=""){
                    vm.filterObj[vm.lastTable]=vm.formula;
                }else{
                    if(vm.filterObj[vm.lastTable]!=undefined){
                        delete vm.filterObj[vm.lastTable];
                    }
                }
                vm.formula="";
                $("#"+vm.lastSelectedTable).css("background-color", "#FFF");
                $("#"+vm.lastSelectedTable).css("color", "#000");
                vm.lastTable="";
            }
            vm.filterBtn=0;
            vm.tableSelect=function(table){
                vm.filterBtn=1;
                $(".filterCls").css("background-color", "#FFF");
                $(".filterCls").css("color", "#000");
                if(vm.lastTable!=table){
                    vm.formula="";
                }
                if(vm.filterObj[table]!=undefined && vm.lastTable!=table){
                    vm.formula=vm.filterObj[table];
                }
                $("#"+table).css("background-color", "#2196f3");
                $("#"+table).css("color", "#FFF");
                vm.lastTable=table;
            }
            vm.filterClose=function(){
                vm.filterBtn=0;
                vm.formula="";
                $("#"+vm.lastSelectedTable).css("background-color", "#FFF");
                $("#"+vm.lastSelectedTable).css("color", "#000");
            }
            vm.saveRoleLevel=function () {
                if(Object.keys(vm.filterObj).length==0){
                    dataFactory.errorAlert("You have to select minimum one filter");
                    return;
                }
                if(vm.rlsObj.usersObj==undefined || vm.rlsObj.usersObj==""){
                    dataFactory.errorAlert("Select user first");
                    return;
                }
                if(vm.rlsObj.usersObj.length==vm.userList.length){
                    vm.rlsObj.usersObj="ALL";
                }
                var sharedviewObj=JSON.parse(vm.rlsObj.publicview);
                vm.rlsObj.publicviewId=sharedviewObj.id;
                vm.rlsObj.metadataId=sharedviewObj.metadataId;
                //delete vm.rlsObj.publicview;
                var data={
                    "filterObj":JSON.stringify(vm.filterObj),
                    "rlsObj":vm.rlsObj
                };
                dataFactory.request($rootScope.roleLevelSecurity_Url,'post',data).then(function(response){
                    if(response.data.errorCode==1){
                        dataFactory.successAlert(response.data.message);
                        $window.location = '#/setting/role_security';
                    }else{
                        dataFactory.errorAlert(response.data.message);
                    }
                });
            }
        }]);

