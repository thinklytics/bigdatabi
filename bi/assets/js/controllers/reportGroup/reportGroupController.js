angular.module('app', ['ngSanitize'])
    .controller('reportGroupController', ['$scope', '$sce', 'dataFactory', '$filter', '$stateParams', '$location', '$window', '$rootScope','$cookieStore',
        function ($scope, $sce, dataFactory, $filter, $stateParams, $location, $window, $rootScope,$cookieStore) {
            var vm=$scope;
            var data={};
            data['navTitle']="REPORT GROUP";
            data['icon']="fa fa-user";
            $rootScope.$broadcast('subNavBar', data);
            $scope.type=$stateParams.type;
            /*var userId=$cookieStore.get('userId');
            $scope.id = $stateParams.id;*/
            dataFactory.request($rootScope.UsergroupList_Url,'post',"").then(function(response){
                vm.userGroup=response.data.result;
            }).then(function(){
                if($scope.type=="sharedview"){
                    dataFactory.request($rootScope.sharedViewList_Url,'post',"").then(function(response){
                        vm.sharedviewList=response.data.result;
                    }).then(function(){
                        setTimeout(function(){
                            $("#userGroupSelect").selectpicker();
                            $("#sharedViewSelect").selectpicker();
                        },100);
                    });
                }else if($scope.type=="datasource"){
                    dataFactory.request($rootScope.datasourceList_Url,'post',"").then(function(response){
                        var result=response.data.result;
                        vm.sharedviewList=[];
                        Object.keys(result).forEach(function(d){
                            result[d].forEach(function(k){
                                vm.sharedviewList.push(k);
                            })
                        });
                    }).then(function(){
                        setTimeout(function(){
                            $("#userGroupSelect").selectpicker();
                            $("#sharedViewSelect").selectpicker();
                        },100);
                    });
                }else if($scope.type=="metadata"){
                    dataFactory.request($rootScope.MetadataList_Url,'post',"").then(function(response){
                        var result=response.data.result;
                        vm.sharedviewList=[];
                        Object.keys(result).forEach(function(d){
                            result[d].forEach(function(k){
                                vm.sharedviewList.push(k);
                            })
                        });
                    }).then(function(){
                        setTimeout(function(){
                            $("#userGroupSelect").selectpicker();
                            $("#sharedViewSelect").selectpicker();
                        },100);
                    });
                }else if($scope.type=="dashboard"){
                    dataFactory.request($rootScope.DashboardList_Url,'post',"").then(function(response){
                        var result=response.data.result;
                        vm.sharedviewList=[];
                        Object.keys(result).forEach(function(d){
                            result[d].forEach(function(k){
                                vm.sharedviewList.push(k);
                            })
                        });
                    }).then(function(){
                        setTimeout(function(){
                            $("#userGroupSelect").selectpicker();
                            $("#sharedViewSelect").selectpicker();
                        },100);
                    });
                }
            });
            // Update Report Group
            vm.group={};
            vm.save=function(group){
                if(group.name==undefined){
                    dataFactory.errorAlert("Report Group Name is required");
                    return;
                }else if(group.description==undefined){
                    dataFactory.errorAlert("Description is required");
                    return; 
                }else if(group.selectedlist.length==0){
                    dataFactory.errorAlert("Select min one public shared view");
                    return;
                }else if(group.userGroup.length==0){
                    dataFactory.errorAlert("Select min one user group");
                    return;
                }
                var data={
                    group:group,
                    type:$scope.type
                };
                dataFactory.request($rootScope.ReportGroupSave_Url,'post',data).then(function(response){
                    if(response.data.errorCode==1){
                        dataFactory.successAlert("save successfully");
                        $window.location = '#/setting/report_group';
                    }else{
                        dataFactory.errorAlert(response.data.message);
                    }
                });
            }
        }]);

