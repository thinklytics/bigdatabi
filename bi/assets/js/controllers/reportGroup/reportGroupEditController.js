angular.module('app', ['ngSanitize'])
    .controller('reportGroupEditController', ['$scope', '$sce', 'dataFactory', '$filter', '$stateParams', '$location', '$window', '$rootScope','$cookieStore',
        function ($scope, $sce, dataFactory, $filter, $stateParams, $location, $window, $rootScope,$cookieStore) {
    	var vm=$scope;
    	var data={};
		data['navTitle']="REPORT GROUP";
		data['icon']="fa fa-user";
		$rootScope.$broadcast('subNavBar', data);
        var userId=$cookieStore.get('userId');       
        $scope.id = $stateParams.id;
        $scope.type=$stateParams.type;
		dataFactory.request($rootScope.UsergroupList_Url,'post',"").then(function(response){
			vm.userGroup=response.data.result;
		}).then(function(){
            vm.sharedviewList=[];
			if($scope.type=="sharedview"){
				dataFactory.request($rootScope.sharedViewList_Url,'post',"").then(function(response){
					vm.sharedviewList=response.data.result;
				});
			}else if($scope.type=="datasource"){
				dataFactory.request($rootScope.datasourceList_Url,'post',"").then(function(response){
					var result=response.data.result;
					vm.sharedviewList=[];
					Object.keys(result).forEach(function(d){
						result[d].forEach(function(k){
							vm.sharedviewList.push(k);
						})
					});
				});
			}else if($scope.type=="metadata"){
				dataFactory.request($rootScope.MetadataList_Url,'post',"").then(function(response){
					var result=response.data.result;
					vm.sharedviewList=[];
					Object.keys(result).forEach(function(d){
						result[d].forEach(function(k){
							vm.sharedviewList.push(k);
						})
					});
				});
			}else if($scope.type=="dashboard"){
				dataFactory.request($rootScope.DashboardList_Url,'post',"").then(function(response){
					var result=response.data.result;
					vm.sharedviewList=[];
					Object.keys(result).forEach(function(d){
						result[d].forEach(function(k){
							vm.sharedviewList.push(k);
						})
					});
				});
			}
		}).then(function(){
            var data = {
                'id': $scope.id,
                'type':$scope.type
            };
            dataFactory.request($rootScope.ReportGroupById_Url, 'post', data).then(function(response){
                $scope.group.name = response.data.result[0].name;
                $scope.group.description = response.data.result[0].description;
                var grpDataArr = [];
                var grpData = response.data.result[0].group;
                grpData.forEach(function(d){
                    grpDataArr.push(String(d.user_group_id));
                });
                $scope.group.groupData = grpDataArr;
                var sharedViewArr = [];
                if($scope.type=="datasource"){
                    var sharedViewData = response.data.result[0].datasource_group;
                    sharedViewData.forEach(function(d){
                        sharedViewArr.push(String(d.datasource_id));
                    });
				}else if($scope.type=="metadata"){
                    var sharedViewData = response.data.result[0].metadata_group;
                    sharedViewData.forEach(function(d){
                        sharedViewArr.push(String(d.metadata_id));
                    });
				}else if($scope.type=="dashboard"){
                    var sharedViewData = response.data.result[0].dashboard_group;
                    sharedViewData.forEach(function(d){
                        sharedViewArr.push(String(d.dashboard_id));
                    });
				}else if($scope.type=="sharedview" || $scope.type=="shardview"){
                	var sharedViewData = response.data.result[0].shared_group;
                    sharedViewData.forEach(function(d){
                        sharedViewArr.push(String(d.sharedview_id));
                    });
				}
                $scope.group.selectedList = sharedViewArr;
            }).then(function(){
                setTimeout(function(){
                    $("#userGroupSelect").selectpicker();
                    $("#sharedViewSelect").selectpicker();
                },100);
            });
		});

        	
    	
// Update Report Group 
    	vm.group={};
    	vm.save=function(group){
    		if(group.name==undefined){
    			dataFactory.errorAlert("Report Group Name is required");
    			return;
    		}else if(group.description==undefined){
    			dataFactory.errorAlert("Description is required");
    			return;
    		}else if(group.selectedList.length==0){
    			dataFactory.errorAlert("Select min one public shared view");
    			return;
    		}else if(group.groupData.length==0){
    			dataFactory.errorAlert("Select min one user group");
    			return;
    		}
    		var data={
    			id:$scope.id,
    			name:group.name,
    			description:group.description,
    			group:group.groupData,
    			sharedView:group.selectedList,
				type:$scope.type
    		};
    		dataFactory.request($rootScope.ReportGroupUpdate_Url,'post',data).then(function(response){
    			if(response.data.errorCode==1){
    				dataFactory.successAlert("submited successfully");
    				$window.location = '#/setting/report_group';
    			}else{
    				dataFactory.errorAlert(response.data.message);
    			}
    		});
    	}
    	
    	
	    	
    }]);

