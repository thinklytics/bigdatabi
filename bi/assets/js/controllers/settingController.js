'use strict';
/* Controllers */
angular.module('app', ['ngSanitize'])
    .controller('settingController', ['$scope', '$compile', '$sce', 'dataFactory', '$filter', '$stateParams', '$location', '$window', '$rootScope', '$cookieStore',
        function ($scope, $compile, $sce, dataFactory, $filter, $stateParams, $location, $window, $rootScope, $cookieStore) {
            var vm = $scope;
            var data = {};
            var addClass = '';
            var viewClass = '';
            if ($location.path() == '/setting/') {
                viewClass = 'activeli';
            } else {
                addClass = 'activeli';
            }
            data['navTitle'] = "ADMINISTRATION";
            data['icon'] = "fa fa-user";
            data['navigation'] = [
                {
                    "name": "Add",
                    "url": "#/setting/add",
                    "class": addClass
                },
                {
                    "name": "View",
                    "url": "#/setting/",
                    "class": viewClass
                }
            ];
            $rootScope.$broadcast('subNavBar', data);
            var userId = $cookieStore.get('userId');
            var tabId = $stateParams.tabId;

                var id="javascript:void(0);#"+tabId;
                $('a[href="'+id+'"]').click();


            /*
              tab click
             */

            $("#tabs ul li a").click(function(ky, vl) {
                //location.hash = $(this).attr('href');
                var hash=$(this).attr('href');
                var hashArray = hash.split(";");
                var tab=hashArray[1].split("#")[1];
                location.hash="setting/"+tab;
                //roles
            });







// Delete
            $scope.showSwal = function(type, callType, id, reportType){
                if(type == 'warning-message-and-cancel') {
                    swal({
                        title: 'Are you sure?',
                        text: 'You want to Delete !',
                        type: 'warning',
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        cancelButtonText: 'No',
                        confirmButtonClass: "btn btn-success",
                        cancelButtonClass: "btn btn-danger",
                        buttonsStyling: false,
                    }).then(function(value){
                        if(value){
                            if(callType == 'user-role'){
                                vm.deleteRoleAssign(id);
                            }
                            else if(callType == 'role'){
                                vm.deleteRole(id);
                            }
                            else if(callType == 'user-group'){
                                vm.deleteUsergroup(id);
                            }
                            else if(callType == 'report-group'){
                                vm.deleteReportGroup(id, reportType);
                            }
                            else if(callType == 'report-security'){
                                vm.rlsDelete(id);
                            }
                            else if(callType == 'role-level-user'){
                                vm.rlsUserDelete(id);
                            }
                            else if(callType == 'role-user-group'){
                                vm.rlsUserGroupDelete(id);
                            }
                            else if(callType == 'email-list'){
                                vm.emailListDelete(id, reportType);
                            }
                            else if(callType == 'email-group'){
                                vm.emailGrpListDelete(id, reportType);
                            }
                            else if(callType == 'email-notification'){
                                vm.emailNotificationDelete(id, reportType);
                            }
                        }
                    },
                    function (dismiss) {

                    })
                }
            }
// Delete





            /**********************************************Role******************************************/
            /*
             * Get Role List
             */
            if(tabId=="roles"){
                vm.userRoleList = function () {
                    dataFactory.request($rootScope.Rolelist_Url, 'post', "").then(function (response) {
                        if (response.data.errorCode == 1)
                            vm.roleList = response.data.result;
                        else
                            dataFactory.errorAlert("Check your connection");
                    }).then(function () {
                        /*
                         * Call datatable
                         */
                        setTimeout(function () {
                            dataCreate("roleList");
                        }, 2000);
                    });
                }
                vm.userRoleList();
                /*
                 * Delete ROle
                 */
                vm.deleteRole = function (id) {
                    dataDestroy("roleList");
                    var data={
                        "id":id
                    };
                    dataFactory.request($rootScope.Roledelete_Url, 'post', data).then(function (response) {
                        if (response.data.errorCode == 1) {
                            dataFactory.successAlert("Delete successfully");
                            vm.userRoleList();
                        }
                        else
                            dataFactory.errorAlert("Check your connection");
                    });
                }
            }


            /**************************************User group***************************************************/
            function dataCreate(id) {
                setTimeout(function () {
                    $("#" + id).dataTable();
                }, 2000);
            }

            function dataDestroy(id) {
                if ($.fn.DataTable.isDataTable("#" + id)) {
                    $("#" + id).DataTable().destroy();
                }
                //$("#"+id).dataTable().fnDestroy(); 
            }

            vm.userGroupModel = function () {
                $('#groupname').val('');
                $('#usergroup').modal({
                    backdrop: 'static',
                    keyboard: false
                });
                $('#usergroup').modal('show');
            }
            vm.userGroupModelEdit = function (groupObj) {
                $('#usergroupEdit').modal({
                    backdrop: 'static',
                    keyboard: false
                });
                $('#usergroupEdit').modal('show');
                vm.groupnameEdit = groupObj.name;
                vm.groupnameId = groupObj.id;
            }
            /*
             * User group list
             */
            if(tabId=="user_group"){
                vm.userGroupListInit=function () {
                    dataFactory.request($rootScope.UsergroupList_Url, 'post', "").then(function (response) {
                        response.data.result.forEach(function (d) {
                            var reportGroup = "";
                            d.report_group.forEach(function (p) {
                                if (p.report_group != null) {
                                    reportGroup += p.report_group.name + ",";
                                }
                            });
                            if (reportGroup) {
                                reportGroup = reportGroup.replace(/,\s*$/, "");
                            }
                            d.reportGroup = reportGroup;
                        })
                        if (response.data.errorCode == 1)
                            vm.UserGroupList = response.data.result;

                    }).then(function () {
                        /*
                         * Call datatable
                         */
                        dataCreate("userGroupListid");
                    });
                }
                vm.userGroupListInit();
                /*
                 * Delete user group
                 */
                vm.deleteUsergroup = function (id) {
                    dataDestroy("userGroupListid");
                    var data = {
                        "id": id
                    };
                    dataFactory.request($rootScope.userGroupDelete_Url, 'post', data).then(function (response) {
                        if (response.data.errorCode == 1) {
                            dataFactory.successAlert("User Group Deleted Successfully");
                            vm.userGroupListInit();
                        }
                        else
                            dataFactory.errorAlert("Check your connection");
                    });
                }
                vm.saveUserGroup = function (name) {
                    dataDestroy("userGroupList");
                    if (name == undefined || name == "") {
                        dataFactory.errorAlert("Name is required");
                        return;
                    }
                    var data = {
                        "name": name
                    };
                    dataFactory.request($rootScope.Groupcreate_Url, 'post', data).then(function (response) {
                        if (response.data.errorCode == 1) {
                            dataFactory.successAlert("User group create successfully");
                            vm.UserGroupList.push(response.data.result);

                        }
                        else
                            dataFactory.errorAlert("Check your connection");
                    }).then(function () {
                        dataCreate("userGroupList");
                    });
                    $('#usergroup').modal('hide');
                }
                vm.updateUserGroup = function (name, id) {
                    dataDestroy("userGroupList");
                    if (name == undefined || name == "") {
                        dataFactory.errorAlert("Name is required");
                        return;
                    }
                    var data = {
                        "name": name,
                        "id": id
                    };
                    dataFactory.request($rootScope.Groupupdate_Url, 'post', data).then(function (response) {
                        if (response.data.errorCode == 1) {
                            dataFactory.successAlert("User group update successfully");
                            //dataDestroy("userGroupList");
                            var updateIndex;
                            vm.UserGroupList.forEach(function (d, index) {
                                if (d.id == id)
                                    updateIndex = index
                            });
                            vm.UserGroupList[updateIndex].name = name;
                        }
                        else
                            dataFactory.errorAlert("Check your connection");
                    }).then(function () {
                        dataCreate("userGroupList");
                    });
                    $('#usergroupEdit').modal('hide');
                }
            }



            /***********************************End User Group************************************************/
            /***********************************User List****************************************************/
            if(tabId=="role_assign"){
                vm.userListInit=function () {
                    dataFactory.request($rootScope.userList_Url, 'post', "").then(function (response) {
                        try {
                            vm.userRole = $rootScope.roleObj[0].roles[0].display_name;
                        } catch (e) {

                        }

                        if (response.data.errorCode == 1) {
                            response.data.result.forEach(function (d) {
                                var userGroup = "";
                                d.group.forEach(function (p) {
                                    if (p != undefined && p.user_group != undefined) {
                                        userGroup += p.user_group.name + ",";
                                    }
                                });
                                var temp = userGroup.substring(0, userGroup.length - 1);
                                d.group = temp;
                            });
                            vm.userList = response.data.result;
                        }
                        else
                            dataFactory.errorAlert("Check your connection");
                    }).then(function () {
                        dataCreate("userRoleAssign");
                    });
                }
                vm.userListInit();
                /*
                 * Report group list datasource
                 */

            }

            /*
             * Report group list Metadata
             */
            if(tabId=="report_group"){

                vm.reportGroupListInt=function () {
                    vm.reportData_sharedview = [];
                    dataFactory.request($rootScope.ReportGroupListPublicview_Url, 'post', "").then(function (response) {
                        if (response.data.errorCode == 1) {
                            response.data.result.forEach(function (d) {
                                var reportObj = {};
                                reportObj['id'] = d.id;
                                reportObj['name'] = d.name;
                                var userGroup = "";
                                d.group.forEach(function (g) {
                                    if (g['user_group_details'][0] && g['user_group_details'][0]['name'] != undefined)
                                        userGroup += g['user_group_details'][0]['name'] + ",";
                                });
                                userGroup = userGroup.substring(0, (userGroup.length - 1));
                                var shared_group = "";
                                d.shared_group.forEach(function (s) {
                                    if (s['shared_view_details'][0]) {
                                        shared_group += s['shared_view_details'][0]['name'] + ",";
                                    }
                                });
                                shared_group = shared_group.substring(0, (shared_group.length - 1));
                                if (shared_group) {
                                    reportObj['userGroup'] = userGroup;
                                    reportObj['sharedGroup'] = shared_group;
                                    vm.reportData_sharedview.push(reportObj);
                                }
                            });
                        }
                        else
                            dataFactory.errorAlert("Check your connection");
                    }).then(function () {
                        dataCreate("reportGroup_sharedview");
                    });
                    vm.reportData_datasource = [];
                    dataFactory.request($rootScope.ReportGroupListDatasource_Url, 'post', "").then(function (response) {
                        if (response.data.errorCode == 1) {
                            response.data.result.forEach(function (d) {
                                var reportObj = {};
                                reportObj['id'] = d.id;
                                reportObj['name'] = d.name;
                                var userGroup = "";
                                d.group.forEach(function (g) {
                                    if (g['user_group_details'][0] && g['user_group_details'][0]['name'] != undefined)
                                        userGroup += g['user_group_details'][0]['name'] + ",";
                                });
                                userGroup = userGroup.substring(0, (userGroup.length - 1));
                                var datasource_group = "";
                                d.datasource_group.forEach(function (s) {
                                    if (s['datasource_details'][0]) {
                                        datasource_group += s['datasource_details'][0]['name'] + ",";
                                    }
                                });
                                datasource_group = datasource_group.substring(0, (datasource_group.length - 1));
                                if (datasource_group) {
                                    reportObj['userGroup'] = userGroup;
                                    reportObj['datasourceGroup'] = datasource_group;
                                    vm.reportData_datasource.push(reportObj);
                                }
                            });
                        }
                        else
                            dataFactory.errorAlert("Check your connection");
                    }).then(function () {
                        dataCreate("reportGroup_datasource");
                    });
                    vm.reportData_metadata = [];
                    dataFactory.request($rootScope.ReportGroupListMetadata_Url, 'post', "").then(function (response) {
                        if (response.data.errorCode == 1) {
                            response.data.result.forEach(function (d) {
                                var reportObj = {};
                                reportObj['id'] = d.id;
                                reportObj['name'] = d.name;
                                var userGroup = "";
                                d.group.forEach(function (g) {
                                    if (g['user_group_details'][0] && g['user_group_details'][0]['name'])
                                        userGroup += g['user_group_details'][0]['name'] + ",";
                                });
                                userGroup = userGroup.substring(0, (userGroup.length - 1));
                                var metadata_group = "";
                                d.metadata_group.forEach(function (s) {
                                    if (s['metadata_details'][0]) {
                                        metadata_group += s['metadata_details'][0]['name'] + ",";
                                    }
                                });
                                metadata_group = metadata_group.substring(0, (metadata_group.length - 1));
                                if (metadata_group) {
                                    reportObj['userGroup'] = userGroup;
                                    reportObj['metadataGroup'] = metadata_group;
                                    vm.reportData_metadata.push(reportObj);
                                }
                            });
                        }
                        else
                            dataFactory.errorAlert("Check your connection");
                    }).then(function () {
                        dataCreate("reportGroup_metadata");
                    });
                    /*
                     * Report group list dashbord
                     */
                    vm.reportData_dashboard = [];
                    dataFactory.request($rootScope.ReportGroupListDashboard_Url, 'post', "").then(function (response) {
                        if (response.data.errorCode == 1) {
                            response.data.result.forEach(function (d) {
                                var reportObj = {};
                                reportObj['id'] = d.id;
                                reportObj['name'] = d.name;
                                var userGroup = "";
                                d.group.forEach(function (g) {
                                    if (g['user_group_details'][0] && g['user_group_details'][0]['name'] != undefined)
                                        userGroup += g['user_group_details'][0]['name'] + ",";
                                });
                                userGroup = userGroup.substring(0, (userGroup.length - 1));
                                var dashboard_group = "";
                                d.dashboard_group.forEach(function (s) {
                                    if (s['dashboard_details'][0]) {
                                        dashboard_group += s['dashboard_details'][0]['name'] + ",";
                                    }
                                });
                                dashboard_group = dashboard_group.substring(0, (dashboard_group.length - 1));
                                if (dashboard_group) {
                                    reportObj['userGroup'] = userGroup;
                                    reportObj['dashboardGroup'] = dashboard_group;
                                    vm.reportData_dashboard.push(reportObj);
                                }
                            });
                        }
                        else
                            dataFactory.errorAlert("Check your connection");
                    }).then(function () {
                        dataCreate("reportGroup_dashboard");
                    });
                }
                vm.reportGroupListInt();
                /*
                 * Delete report group
                 */
                vm.deleteReportGroup = function (id, type) {
                    /*var tableClass;
                    if (type == 'datasource') {
                        tableClass = "reportGroup_datasource";
                        dataDestroy("reportGroup_datasource");
                        var deleteIndex;
                        vm.reportData_datasource.forEach(function (d, index) {
                            if (d.id == id)
                                deleteIndex = index
                        });
                        vm.reportData_datasource.splice(deleteIndex, 1);
                    } else if (type == 'metadata') {
                        tableClass = "reportGroup_metadata";
                        dataDestroy("reportGroup_metadata");
                        var deleteIndex;
                        vm.reportData_metadata.forEach(function (d, index) {
                            if (d.id == id)
                                deleteIndex = index
                        });
                        vm.reportData_metadata.splice(deleteIndex, 1);
                    } else if (type == 'dashboard') {
                        tableClass = "reportGroup_dashboard";
                        dataDestroy("reportGroup_dashboard");
                        var deleteIndex;
                        vm.reportData_dashboard.forEach(function (d, index) {
                            if (d.id == id)
                                deleteIndex = index
                        });
                        vm.reportData_dashboard.splice(deleteIndex, 1);
                    } else if (type == 'sharedview') {
                        tableClass = "reportGroup_sharedview";
                        dataDestroy("reportGroup_sharedview");
                        var deleteIndex;
                        vm.reportData_sharedview.forEach(function (d, index) {
                            if (d.id == id)
                                deleteIndex = index
                        });
                        vm.reportData_sharedview.splice(deleteIndex, 1);
                    }*/
                    var data = {
                        "id": id,
                        "type": type
                    };
                    dataFactory.request($rootScope.reportGroupDelete_Url, 'post', data).then(function (response) {
                        if (response.data.errorCode == 1) {
                            dataFactory.successAlert("Delete successfully");
                            vm.reportGroupListInt();
                        }
                        else
                            dataFactory.errorAlert("Check your connection");
                    });
                }
            }

            /*
             * Report group list shared view
             */


            /* Delete Role assign */
            vm.deleteRoleAssign = function (id) {
                dataDestroy("userRoleAssign");
                var data = {
                    "id": id
                };
                dataFactory.request($rootScope.Roleassigndelete_Url, 'post', data).then(function (response) {
                    if (response.data.errorCode == 1) {
                        dataFactory.successAlert("Delete successfully");
                        vm.userListInit();
                    }else{
                        dataFactory.errorAlert("Check your connection");
                    }
                });
            }
            /*******************************Report Level Security*****************************************************/
            if(tabId=="role_security"){
                vm.rlsListInit=function () {
                    dataFactory.request($rootScope.rlsList_Url, 'post', "").then(function (response) {
                        vm.rlsList = response.data.result.rlsList;
                        vm.rlsList.forEach(function (d) {
                            var condition = JSON.parse(d.condition);
                            var conditionStr = "";
                            Object.keys(condition).forEach(function (key) {
                                conditionStr += key + "(" + condition[key] + ")";
                            });
                            d['conditionStr'] = conditionStr;
                        })
                    }).then(function () {
                        dataCreate('roleLevel');
                    });
                }
                vm.rlsListInit();
                /*
                 Delete rls
                 */
                vm.rlsDelete = function (id) {
                    dataDestroy("roleLevel");
                    var data = {
                        "id": id
                    };
                    dataFactory.request($rootScope.rlsDelete_Url, 'post', data).then(function (response) {
                        if (response.data.errorCode == 1) {
                            dataFactory.successAlert(response.data.message);
                            vm.rlsListInit();
                        }
                        else
                            dataFactory.errorAlert("Check your connection");
                    });
                }

            }

            /*******************************Report Level Security User*****************************************************/



// Get MetaData List
            if(tabId=="cache_manager"){
                vm.nodeRestart = function () {
                    $(".loadingBar").show();
                    dataFactory.request($rootScope.nodeRestart_Url, 'post', "").then(function (response) {
                        if (response.data.errorCode == 1) {
                            dataFactory.successAlert(response.data.message);
                        }else {
                            dataFactory.errorAlert("Check your connection");
                        }
                        $(".loadingBar").hide();
                    });
                }

                vm.redisClear = function () {
                    $(".loadingBar").show();
                    dataFactory.request($rootScope.redisClear_Url, 'post', "").then(function (response) {
                        if (response.data.errorCode == 1) {
                            dataFactory.successAlert(response.data.message);
                        }else {
                            dataFactory.errorAlert("Check your connection");
                        }
                        $(".loadingBar").hide();
                    });
                }

                vm.cacheManger = {};
                vm.cacheManger['metadata'] = '';
                vm.metadataDashboardListInit=function () {
                    dataFactory.request($rootScope.MetadataDashboardList_Url, 'post', "").then(function (response) {
                        if (response.data.errorCode == 1) {
                            vm.metadatList = JSON.parse(response.data.result);
                        } else {
                            dataFactory.errorAlert("Check your connection");
                        }
                    });
                }
                vm.metadataDashboardListInit();

                vm.ClearCache = function (cacheMangerData) {
                    if (cacheMangerData.metadata.length == 0) {
                        dataFactory.errorAlert("Please Select MetaData");
                        return;
                    }
                    var data = {
                        "id": cacheMangerData.metadata
                    };
                    $(".loadingBar").show();
                    dataFactory.request($rootScope.clearCache_Url, 'post', data).then(function (response) {
                        if (response.data.errorCode == 1) {
                            dataFactory.successAlert(response.data.message);
                        } else {
                            dataFactory.errorAlert(response.data.message);
                        }
                        $(".loadingBar").hide();
                    });
                }
                // Send Email Start

            }
            if(tabId=="email_report"){
                dataFactory.request($rootScope.sharedViewList_Url, 'post', "").then(function (response) {
                    vm.sharedviewList = response.data.result;
                }).then(function () {
                    setTimeout(function () {
                        $("#sharedViewSelect").selectpicker();
                    }, 100);
                });
                vm.mail = {};
                vm.send_Mail = function (mail) {
                    var data = {
                        "mailObj": mail
                    };
                    dataFactory.request($rootScope.SendEmail_Url, 'post', data).then(function (response) {

                    });
                }
                // Send Email End
                //Email list
                vm.emailScheduleListInit=function () {
                    dataFactory.request($rootScope.emailScheduleList_Url, 'post', "").then(function (response) {
                        vm.emailScheduleList = response.data.result;
                    }).then(function () {
                        dataCreate('emailSchedule');
                    });
                }
                vm.emailScheduleListInit();
                vm.emailListDelete = function (id, index) {
                    dataDestroy("emailSchedule");
                    var data = {
                        "id": id
                    };
                    dataFactory.request($rootScope.emailScheduleListDelete_Url, 'post', data).then(function (response) {
                        if (response.data.errorCode == 1) {
                            dataFactory.successAlert(response.data.message);
                            vm.emailScheduleListInit();
                        }
                        else
                            dataFactory.errorAlert("Check your connection");
                    });
                }
                /*
                  * Email grp controller list
                 */
                //list
                vm.emailGrpListInit=function () {
                    dataFactory.request($rootScope.EmailGrpList_Url, 'post', "").then(function (response) {
                        vm.emailGrpList = response.data.result;
                    }).then(function () {
                        dataCreate('emailGroupTable');
                    });
                }
                vm.emailGrpListInit();
                vm.emailGrpListDelete = function (id, index) {
                    dataDestroy("emailGroupTable");
                    var data = {
                        "id": id
                    };
                    dataFactory.request($rootScope.emailGrpListDelete_Url, 'post', data).then(function (response) {
                        if (response.data.errorCode == 1) {
                            dataFactory.successAlert(response.data.message);
                            vm.emailGrpListInit();
                        }
                        else
                            dataFactory.errorAlert("Check your connection");
                    });
                }
                /*
                 * Email notification list
                 */
                vm.emailNotificationListInit=function () {
                    dataFactory.request($rootScope.emailNotificationList_Url, 'post', "").then(function (response) {
                        vm.emailNotificationList = response.data.result;
                    }).then(function () {
                        dataCreate('emailNotification');
                    });
                }
                vm.emailNotificationListInit();
                vm.emailNotificationEdit = function (list) {
                    vm.emailNotificataion = {};
                    vm.emailNotificataion.id = list.id;
                    vm.emailNotificataion.title = list.notificationObj.title;
                    vm.emailNotificataion.threshold = list.notificationObj.threshold;
                    vm.emailNotificataion.status = list.status;
                    vm.emailNotificataion.condition = list.notificationObj.condition;
                    vm.activeEmailNotification = list;
                    $('#emailNotificationEdit').modal({
                        backdrop: 'static',
                        keyboard: false
                    });
                    $('#emailNotificationEdit').modal('show');
                }
                vm.emailNotificationUpdate = function (notificationObj) {
                    var data = {
                        'notificationObj': notificationObj
                    };
                    dataFactory.request($rootScope.emailNotificationUpdate_Url, 'post', data).then(function (response) {
                        if (response.data.errorCode) {
                            vm.activeEmailNotification.notificationObj.title = notificationObj.title;
                            vm.activeEmailNotification.notificationObj.threshold = notificationObj.threshold;
                            vm.activeEmailNotification.notificationObj.condition = notificationObj.condition;
                            vm.activeEmailNotification.status = notificationObj.status;
                            dataFactory.successAlert(response.data.message);

                        }
                        $('#emailNotificationEdit').modal('hide');
                    });
                }
                vm.emailNotificationDelete = function (id, index) {
                    dataDestroy("emailNotification");
                    vm.emailNotificationList.splice(index, 1);
                    var data = {
                        "id": id
                    };
                    dataFactory.request($rootScope.emailNotificationDelete_Url, 'post', data).then(function (response) {
                        if (response.data.errorCode == 1) {
                            dataFactory.successAlert(response.data.message);
                            vm.emailNotificationListInit();
                        }
                        else
                            dataFactory.errorAlert("Check your connection");
                    });
                }
            }
        }]);