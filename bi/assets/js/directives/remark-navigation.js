/* ============================================================
 * Directive: pgNavigate
 * Pre-made view ports to be used for HTML5 mobile hybrid apps
 * ============================================================ */

angular.module('app')
    .directive('rmNavigation', function() {
    	
    	
        return {
            restrict: 'A',
            templateUrl: 'html/common/top-navigation.html',
            scope: true,
            controller: 'NavbarCtrl'
        }
    });