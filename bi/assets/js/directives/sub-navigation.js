/* ============================================================
 * Directive: pgNavigate
 * Pre-made view ports to be used for HTML5 mobile hybrid apps
 * ============================================================ */

angular.module('app')
    .directive('subNavigation', function() {
        return {
            restrict: 'A', 
            templateUrl: 'html/common/sub-navigation.html',
            scope: true, 
            controller: 'NavbarsubCtrl'
        }
    });