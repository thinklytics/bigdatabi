/* ============================================================
 * File: config.lazyload.js
 * Configure modules for ocLazyLoader. These are grouped by 
 * vendor libraries. 
 * ============================================================ */

angular.module('app')
    .config(['$ocLazyLoadProvider', function($ocLazyLoadProvider) {
        $ocLazyLoadProvider.config({
            debug: true,
            events: true,
            modules: [{
					name: 'js',
				    files: [
				     /*"theme/assets/vendors/bootstrap.min.js",
				     "theme/assets/vendors/material.min.js",
				     "theme/assets/vendors/perfect-scrollbar.jquery.min.js",
				     "theme/assets/vendors/jquery.validate.min.js",
				     "https://cdn.jsdelivr.net/momentjs/latest/moment.min.js",				     
				     "theme/assets/vendors/jquery.bootstrap-wizard.js",
				     "theme/assets/vendors/bootstrap-notify.js",
				     "theme/assets/vendors/bootstrap-datetimepicker.js", 
				     "theme/assets/vendors/jquery-jvectormap.js",
				     "theme/assets/vendors/nouislider.min.js", 
				     "theme/assets/vendors/jquery.select-bootstrap.js",
				     "theme/assets/vendors/jquery.datatables.js",
				     "theme/assets/vendors/sweetalert2.js",
				     "theme/assets/vendors/jasny-bootstrap.min.js",
				     "theme/assets/vendors/fullcalendar.min.js",
				     "theme/assets/vendors/jquery.tagsinput.js",
				     "theme/assets/js/turbo.js",
				     "theme/assets/js/demo.js", 
				     'assets/plugins/utility/textinsert.js',
				     //Under socre js
				     'custom-js/underscore.js',
				     'custom-js/svg.js',
				     // loading bar progress-io
				     'assets/custom-js/loading-bar.js',
				     'assets/custom-js/loading-bar.min.js',*/
				     'assets/css/loading-bar.css', 
				     'assets/custom-js/jquery.tablesorter.js',
                     // '//cdnjs.cloudflare.com/ajax/libs/jspdf/0.9.0rc1/jspdf.min.js',
                     //'assets/custom-js/newJSPDF.js',
                      'https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.4.1/jspdf.min.js',
                     // 'https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.4.1/jspdf.debug.js',
                      'assets/custom-js/jspdf.js',


				    ],
				    serie: true // load in the exact order 
				},
				{
					name: 'animation',
                    files: [
                       'custom-js/semantic.min.js'
                    ], 
                    serie: true // load in the exact order
				},
				
                { 
                	 name: 'pivot-table',
                     files: [
                        /*'https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js',   */  
                        /*'https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js',  */  
                        /*'https://cdnjs.cloudflare.com/ajax/libs/jqueryui-touch-punch/0.2.3/jquery.ui.touch-punch.min.js',*/
                        'assets/plugins/pivot-table/pivot.css',
                        'assets/plugins/pivot-table/pivot.js'
                     ], 
                     serie: true // load in the exact order
                },  
                {
                	name:'konvaJs',
                	files:[
	               	       	'assets/plugins/belay/belay.js',
	            	       	'assets/custom-js/balanced-min.js',
	            	       	'assets/custom-js/konva.min.js',
							'assets/plugins/konvajs/konvaSetting.js',
                	 ],
                	 serie: true
                },
                {
                	name:'konvaJsMachine',
                	files:[
                            'assets/plugins/konvajs/konvaMachineSetting.js'
	               	      //  	'assets/plugins/belay/belay.js',
	            	       	// 'assets/custom-js/balanced-min.js',
	            	       	// 'assets/custom-js/konva.min.js',
							// 'assets/plugins/konvajs/konvaSetting.js',
                	 ],
                	 serie: true 
                },
                {
                    name: 'gridster',
                    files:[
                        'assets/plugins/angular-gridster/dist/angular-gridster.min.css',
                        'assets/plugins/angular-gridster/dist/angular-gridster.min.js',
                        'custom-js/balanced-min.js',
                        //'assets/plugins/dc/html2canvas.js',
                        'https://cdnjs.cloudflare.com/ajax/libs/html2canvas/0.5.0-beta4/html2canvas.min.js',
                    ]
                },
                {
               	 name: 'pivot-table',
                    files: [
                       /*'https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js',*/    
                       'assets/plugins/pivot-table/pivot.css',
                       'assets/plugins/pivot-table/pivot.js'
                    ], 
                    serie: true // load in the exact order
               },
               {
                   name: 'dc',
                   files: [
						'assets/custom-js/leaflet.js',
			            'assets/custom-js/leaflet.tooltip.js',
		                /*'https://api.tiles.mapbox.com/mapbox.js/v2.1.9/mapbox.js',*/
		                "https://api.mapbox.com/mapbox.js/v2.2.2/mapbox.js",
		                'https://api.tiles.mapbox.com/mapbox-gl-js/v0.40.1/mapbox-gl.js',
                        'assets/plugins/dc/d3.min.js',
                        'assets/plugins/dc/crossfilter.js',
                        'assets/custom-js/linq.js',
                        'assets/plugins/dc/calculate.js',
                        'assets/plugins/dc/calculateServer.js',
                        'assets/plugins/utility/calculation.js',
                        'assets/plugins/utility/calculationServer.js', 
                       'assets/plugins/dc/customizedTable.js',
                       // 'assets/plugins/dc/Chart.bundle.js',
                       'https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.6.0/Chart.bundle.js',
                       //'https://unpkg.com/chart.js@2.8.0/dist/Chart.bundle.js',
                       //'https://unpkg.com/chartjs-chart-box-and-violin-plot@2/build/Chart.BoxPlot.js',
                       //'https://cdn.jsdelivr.net/npm/chartjs-funnel@1.0.5/dist/chart.funnel.min.js',
                       'assets/custom-js/chart.funnel.bundled.min.js',
                       'assets/plugins/dc/dc.js',
                       'assets/custom-js/Chart.Zoom.min.js',
                       'assets/plugins/dc/draw.js',
                       'assets/plugins/dc/drawServer.js',
                       'assets/plugins/dc/extendedServer.js',
                       'assets/plugins/dc/extended.chart.js',
                       "assets/custom-js/konva.min.js",                                              
                       "custom-js/d3.v3.min.js",  
                       'https://cdn.rawgit.com/jasondavies/d3-cloud/v1.2.1/build/d3.layout.cloud.js',
                       '//bernii.github.io/gauge.js/dist/gauge.min.js',
                       'assets/plugins/dc/customizedTable.js',
                       //'assets/custom-js/chartjs-plugin-datalabels.js',
                       /*"http://cdn.leafletjs.com/leaflet-0.7/leaflet.js",
           			   'https://api.tiles.mapbox.com/mapbox.js/v2.1.9/mapbox.js',
           			   'https://api.tiles.mapbox.com/mapbox.js/v2.1.9/mapbox.css',
	           		   "https://api.mapbox.com/mapbox.js/v2.2.2/mapbox.css",
	           		   "https://api.mapbox.com/mapbox.js/v2.2.2/mapbox.js",
           			   'assets/custom-js/leaflet.tooltip.js',
                       'assets/custom-js/leaflet.tooltip.css',
                       'https://api.tiles.mapbox.com/mapbox-gl-js/v0.40.1/mapbox-gl.js',
                       'https://api.tiles.mapbox.com/mapbox-gl-js/v0.40.1/mapbox-gl.css', */
                       /*"https://cdnjs.cloudflare.com/ajax/libs/Chart.js/1.0.2/Chart.js",
                       "assets/plugins/heatMap/dst/Chart.HeatMap.js"*/

                       // 'https://unpkg.com/leaflet@1.0.3/dist/leaflet.js',
                       // 'assets/custom-js/bundle-leaflet-map.js',// with old code
                       'assets/custom-js/bundle-leafletmap.js',
                       'assets/custom-js/chartjs-plugin-annotation.js',
                       'assets/custom-js/jQAllRangeSliders-min.js',

                   ],
                   serie: true
               },{
            	   name:"multiselect",
            	   files:[
						'assets/plugins/multi-select/cust-multiselect.js',
	                    'assets/plugins/multi-select/js/jquery.multi-select.js',
                        'assets/plugins/multi-select/css/multi-select.css',
                        'assets/custom-js/bootstrap-multiselect.js',                          
            	    ],
            	    serie:true
               }
            ]
        });
    }]);