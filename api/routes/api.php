<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});*/
Route::group(["middleware" => ["auth:api","check.permission"]], function(){
    //All routes goes here
    //Role api
    Route::post("/role/add","RoleController@save");

    //Permission list
    Route::post("/role/permissionList","RoleController@permissionList");

    //Role list
    Route::post("/role/list","RoleController@listRoles");
    Route::post("/role/getDataById","RoleController@listRoleId");
    Route::post("/role/updateRole","RoleController@updateRole");

    //Role delete
    Route::post("/role/delete","RoleController@roleDelete");

    //User role
    Route::post("login/user/role","RoleController@loginUserRole");

    //Role assign
    Route::post("/role/assign","RoleController@assgin");
    Route::post("/role/userlist","RoleController@userList");
    Route::post("/role/getRole","RoleController@getRole");
    Route::post("/role/getRoleAssignDataById","RoleController@getRoleAssign");
    Route::post("/role/updateRoleAssignById","RoleController@updateRoleAssign");
    Route::post("/role/assign/delete","RoleController@roleAssignDelete");



    //User Group Create
    Route::post("/usergroup/add","UserGroupController@save");
    Route::post("/usergroup/edit","UserGroupController@update");
    Route::post("/usergroup/list","UserGroupController@UsergroupList");
    Route::post("/usergroup/delete","UserGroupController@UsergroupDelete");

    // DataSource
    Route::group(['prefix' => 'datasource'], function() {
        Route::post('/list', 'DatasourceController@datasourceList');
        Route::post('/connectionCheck', 'DatasourceController@connectionCheck');
        Route::post('/save', 'DatasourceController@save');
        Route::post('/edit', 'DatasourceController@update');
        Route::post('/delete', 'DatasourceController@datasourceDelete');
        Route::post('/export', 'DatasourceController@datasourceExport');
        Route::post('/import', 'DatasourceController@checkValidation');
        Route::post('/importSave', 'DatasourceController@importSave');
    });
    /*
     * user controller
     */
    Route::group(['prefix' => 'user'], function() {
        Route::post('/list', 'MetadataController@export');
        Route::post('/add', 'UserController@userCreate');
        Route::post('/edit', 'UserController@userEdit');
        Route::post('/obj', 'UserController@userObj');
        Route::post('/updateObj', 'UserController@userUpdate');
        Route::post('/userList', 'UserController@userList');
        Route::post('/delete', 'UserController@userDelete');
    });
    /*
     * Metadata api
     */
    Route::group(['prefix' => 'metadata'], function() {
        Route::post('/export', 'MetadataController@export');
        Route::post('/importValidation', 'MetadataController@checkValidation');
        Route::post('/importSave', 'MetadataController@importSave');
        Route::post('/list', 'MetadataController@metadataList');
        Route::post('/dashboardList', 'MetadataController@dashboardMetadataList');
        Route::post('/tableList', 'MetadataController@getTableList');
        Route::post('/getData', 'MetadataController@getQueryData');
        Route::post('/condition', 'MetadataController@RoleLevelcondition');
        Route::post('/categoryColumn', 'MetadataController@getColumn');
        Route::post('/tableColumnInfo', 'MetadataController@getColumn');
        Route::post('/tableMultiColumnInfo', 'MetadataController@getMultiColumn');
        Route::post('/save', 'MetadataController@save');
        Route::post('/edit', 'MetadataController@update');
        Route::post('/delete', 'MetadataController@metadataDelete');
        Route::post('/getMetadataObject', 'MetadataController@getMetadataObject');
        Route::post('/tableindexs', 'MetadataController@tableIndexs');
        Route::post('/tableindexs/delete', 'MetadataController@tableIndexsDelete');
        Route::post('/tableindexs/create', 'MetadataController@tableIndexsCreate');
        Route::post('/getPrimarykey', 'MetadataController@getPrimarykey');
        Route::post('/createPrimarykey', 'MetadataController@createPrimarykey');
        Route::post('/deletePrimarykey', 'MetadataController@deletePrimarykey');
        Route::post('datatype/update', 'MetadataController@updateDataType');
        Route::post('getLength', 'MetadataController@getDataLength');
        Route::post('/cacheClear', 'MetadataController@redisCacheClear');
        Route::post('/columnCustomGet', 'MetadataController@columnCustomGet'); 
        Route::post('/incrementObjSave', 'MetadataController@incrementDataObjSave');

        Route::group(['prefix' => 'blending'], function() {
            Route::post('/tableMultiColumnInfo', 'MetadataController@getBlendingMultiColumn');
            Route::post('/getData', 'MetadataController@getBlendingQueryData');
            Route::post('/saveData', 'MetadataController@blendingSaveData');
            Route::post('/save', 'MetadataController@blendingSave');
            Route::post('/update', 'MetadataController@blendingUpdate');
            Route::post('/list', 'MetadataController@blendingList');
            Route::post('/syncData', 'MetadataController@syncBlendingData');
            Route::post('/rename', 'MetadataController@renameData');
        });
    });

    /*
    * Machine api
    */
    Route::group(['prefix' => 'machine'], function() {
        Route::post('/save', 'MachineController@save');
        Route::post('/list', 'MachineController@machineList');
        Route::post('/delete', 'MachineController@machineDelete');
        Route::post('/getMachineObject', 'MachineController@getMachineObject');
        Route::post('/update', 'MachineController@update');
    });

    
    /*
     * Dashboard api
     */
    Route::group(['prefix' => 'dashboard'], function() {
        Route::post('/export', 'DashboardController@export');
        Route::post('/importValidation', 'DashboardController@checkValidation');
        Route::post('/importSave', 'DashboardController@importSave');
        Route::post('/list', 'DashboardController@dashboardList');
        Route::post('/list/sharedview', 'DashboardController@dashboardSharedViewList');
        Route::post('/delete', 'DashboardController@dashboardDelete');
        Route::post('/data', 'MetadataController@getQueryDataDashboard');
        Route::post('/image', 'DashboardController@imageSave');
        Route::post('/save', 'DashboardController@save');
        Route::post('/ObjectById', 'DashboardController@dashboardIdGetObject');
        Route::post('/update', 'DashboardController@update');
        Route::post('metadata/update', 'DashboardController@updateMetadata');
        Route::post('metadata/blending/update', 'DashboardController@updateMetadataBlending');
        Route::group(['prefix' => 'server'], function() {
            Route::post('/dataCache', 'MetadataController@dataCacheToRedis');
        });
    });
    /*
     * Shared view
     */
    Route::group(['prefix' => 'sharedview'], function() {
        Route::post('/export', 'PublicViewController@export');
        Route::post('/importValidation', 'PublicViewController@checkValidation');
        Route::post('/importSave', 'PublicViewController@importSave');
        Route::post('/list', 'PublicViewController@getSharedviewList');
        Route::post('/listObj', 'PublicViewController@getSharedviewListObj');
        Route::post('/folderList', 'PublicViewController@getSharedviewFolderList');
        Route::post('/delete', 'PublicViewController@delete');
        Route::post('/data', 'MetadataController@getQueryDataDashboard');
        Route::post('/image', 'PublicViewController@publicViewImageSave');

        Route::post('/dataobject', 'PublicViewController@getPublicViewObject');
        Route::post('/dataobjectEdit', 'PublicViewController@getPublicViewObjectEdit');
        Route::post('/save', 'PublicViewController@save');
        Route::post('/databyId', 'DashboardController@dashboardIdGetObject');
        Route::post('/update', 'PublicViewController@update');
        Route::post('/checkPermission', 'PublicViewController@checkPermission');
        //unused
    });
    //Company profile
    Route::group(['prefix' => 'company/profile'], function() {
        Route::post('/add', 'CompanyProfileController@save');
        Route::post('/get', 'CompanyProfileController@getProfile');
    });
    Route::group(['prefix' => 'company/profile'], function() {
        Route::post('/add', 'CompanyProfileController@save');
        Route::post('/get', 'CompanyProfileController@getProfile');
    });
    //User profile
    Route::group(['prefix' => 'companyDetails'], function() {
        Route::post('/add', 'CompanyDetailsController@save');
        Route::post('/update', 'CompanyDetailsController@update');
        Route::post('/list', 'CompanyDetailsController@companyList');
        Route::post('/get', 'CompanyDetailsController@getProfile');
        Route::post('/delete', 'CompanyDetailsController@delete');
        Route::post('/obj', 'CompanyDetailsController@comObj');
    });

    //Enquiry
    Route::group(['prefix' => 'enquiryDetails'], function() {
        Route::post('/add', 'EnquiryDetailsController@save');
        Route::post('/list', 'EnquiryDetailsController@enquiryList');
        Route::post('/delete', 'EnquiryDetailsController@delete');
        Route::post('/update', 'EnquiryDetailsController@update');
        Route::post('/obj', 'EnquiryDetailsController@enquiryObj');
    });




    Route::post('/changePassword/update', 'UserProfileController@changePassword');
    /*
     * Report group
     */
    // DataSource
    Route::group(['prefix' => 'reportgroup'], function() {
        Route::post('/add', 'ReportGroupController@add');
        Route::post('/list', 'ReportGroupController@ReportGroupList');
        Route::post('datasource/list', 'ReportGroupController@ReportGroupDatasourceList');
        Route::post('metadata/list', 'ReportGroupController@ReportGroupMetadataList');
        Route::post('dashboard/list', 'ReportGroupController@ReportGroupDashboardList');
        Route::post('publicview/list', 'ReportGroupController@ReportGroupPublicviewList');
        Route::post('/dataById', 'ReportGroupController@reportGroupDataById');
        Route::post('/update', 'ReportGroupController@reportGroupUpdate');
        Route::post('/delete', 'ReportGroupController@deleteReportGroup');
    });
    Route::group(['prefix' => 'excel'], function() {
        Route::post('/setExcel', 'DatasourceController@setExcelDsParams');
        Route::get('/loadExcel', 'DatasourceController@loadExcel');
        Route::group(['prefix' => 'append_excel'], function() {
            Route::post('/setExcel', 'DatasourceController@appendExcelData');
        });
    });
    /*
     * ROle level security
     */
    Route::group(['prefix' => 'rolelevelsecurity'], function() {
        Route::post('/getTableColumn', 'PublicViewController@getTableColumn');
        Route::post('/saveRoleLevel','RLSController@save');
        Route::post('/updateRoleLevel','RLSController@update');
        Route::post('/rlsList','RLSController@rlsList');
        Route::post('/list','RLSController@roleLevelList');
        Route::post('/listGroup','RLSController@rlsListWithGroup');
        Route::post('/delete','RLSController@rlsDelete');
        Route::post('/getDataById','RLSController@rlsGetDataById');
        Route::group(['prefix' => 'user'], function() {
            Route::post('/save','RLSController@userRLSSave');
            Route::post('/update','RLSController@userRLSUpdate');
            Route::post('/groupSave','RLSController@userGroupRLSSave');
            Route::post('/groupUpdate','RLSController@userGroupRLSUpdate');
            Route::post('/groupList','RLSController@userGroupRLSList');
            Route::post('/list','RLSController@userRLSList');
            Route::post('/getDataById','RLSController@rlsUserGetDataById');
            Route::post('/getGroupDataById','RLSController@rlsUserGroupGetDataById');
            Route::post('/delete','RLSController@userRLSDelete');
            Route::post('/groupDelete','RLSController@userGroupRLSDelete');
        });
    });
    Route::group(['prefix' => 'email'], function() {
        Route::post('/save','EmailController@emailSave');
        Route::post('users/list','EmailController@userEmail');
        Route::post('schedule/list','EmailController@emailList');
        Route::post('schedule/delete','EmailController@emailListDelete');

        Route::group(['prefix' => 'grp'], function() {
            Route::post('/add','EmailController@emailGrpAdd');
            Route::post('/list','EmailController@emailGrpList');
            Route::post('/delete','EmailController@emailGrpDelete');
            Route::post('/editObjGet','EmailController@emailEditObj');
            Route::post('/update','EmailController@emailGrpUpdate');
        });
        Route::group(['prefix' => 'notification'], function() {
            Route::post('/save','EmailController@notificationSave');
            Route::post('/update','EmailController@notificationUpdate');
            Route::post('/list','EmailController@notificationList');
            Route::post('/delete','EmailController@notificationDelete');
            Route::post('/getData','EmailController@notificationGetData');
            Route::post('/deleteByReport','EmailController@notificationDeleteByReport');
        });
    });
    Route::post('/deleteFile', 'Controller@deleteExportFile');
    Route::post('nodeRestart','Controller@nodeRestart');
    Route::post('redisClear','Controller@redisClear');
    /*
     * Universal api
     */
    Route::group(['prefix' => 'universal'], function() {
        Route::post('machineAdd','UniversalController@machineAdd');
        Route::post('machineList','UniversalController@machineList');
        Route::post('machineAddedList','UniversalController@machineAddedList');
        Route::post('getMachine','UniversalController@getMachine');
        Route::post('machineEdit','UniversalController@machineEdit');
        Route::post('machineDelete','UniversalController@machineDelete');
        Route::post('role','UniversalController@roleCheck');


        Route::post('machineSharedviewSave','UniversalController@machineSharedviewsave');
        Route::post('machineSharedviewList','UniversalController@machineSharedviewList');
    });
});
Route::group(['prefix' => 'publicview'], function() {
    Route::post('/data', 'PublicViewController@getPublicData');
    Route::post('dashboard/data', 'MetadataController@getQueryDataDashboard');
});
Route::post('/errorLog', 'UserProfileController@LogSave');
Route::post("/register",    "Auth\\RegisterController@register");
Route::post("/login",    "Auth\\LoginController@login");
Route::post("/jsList",    "BucketController@listJs");
Route::post('getDataLength', 'MetadataController@getDataLengthPublicView');
Route::post('/cacheToServer', 'MetadataController@dataCacheToRedisPublic');
Route::get('email/send','EmailController@email');
Route::get('email/conditioncheck','EmailController@conditionCheck');
Route::post('sharedview/tempImageSave', 'PublicViewController@tempImageSave');
Route::get('redisUpdate','DepartmentController@redisAddData');
Route::post('redisUpdateNode','MetadataController@redisAddDataNode');


//Enquiry Auth
Route::group(['prefix' => 'enquiryAuth'], function() {
    Route::post('/add', 'EnquiryAuthController@add');
    Route::post('/auth', 'EnquiryAuthController@auth');
});


