<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
    return view('welcome');
});
/*
 * Login and Signup Api
 */
Route::post('/loginCheck', 'LoginController@loginCheck');
Route::post('/signUp', 'LoginController@signUp');
Route::get('/Logout', 'LoginController@getLogout');
/*
 * Datasource Save
 */

Route::post('/datasourceUpdate', 'DatasourceController@update');
Route::get('/idToGetTablelist', 'DatasourceController@idToGetTablelist');
Route::get('/idToGetTableData', 'DatasourceController@idToGetTableData');
Route::get('/datasourceDelete', 'DatasourceController@datasourceDelete');
Route::post('/setExcel', 'DatasourceController@setExcelDsParams');
Route::get('/loadExcel', 'DatasourceController@loadExcel');
/*
 * Metadata Api
 */
Route::post('/getTableList', 'MetadataController@getTableList');
Route::post('/getQueryData', 'MetadataController@getQueryData');
Route::post('/getQueryDataDashboard', 'MetadataController@getQueryDataDashboard');
Route::get('/getColumn', 'MetadataController@getColumn');
Route::post('/metadataSave', 'MetadataController@save');
Route::post('/metadataUpdate', 'MetadataController@update');
Route::get('/getCategoryColumnData', 'MetadataController@getCategoryColumnData');
Route::get('/metadataDelete', 'MetadataController@metadataDelete');
Route::get('/getMetadataObject', 'MetadataController@getMetadataObject');

/*
 * Dashboard Api
 */
Route::get('/metadataList', 'MetadataController@metadataList');
Route::post('/dashboardSave', 'DashboardController@save');


Route::get('/dashboardList', 'DashboardController@dashboardList');
Route::get('/dashboardIdGetObject','DashboardController@dashboardIdGetObject');
Route::get('/dashboardDelete','DashboardController@dashboardDelete');
Route::post('/dashboardUpdate', 'DashboardController@update');
Route::post('/dashboardImageSave', 'DashboardController@imageSave');
/*
 * Public View Api
 */
Route::post('/publicDashboardSave', 'PublicViewController@save');
Route::post('/publicDashboardUpdate', 'PublicViewController@update');
Route::get('/publicViewShow', 'PublicViewController@getPublicViewObject');
Route::post('/publicViewListDashboard', 'PublicViewController@getDashboards');
Route::post('/publicViewDashboardDelete', 'PublicViewController@delete');
Route::post('/publicViewImageSave', 'PublicViewController@publicViewImageSave');

/*
 * Department
 */
Route::post('/departmentSave', 'DepartmentController@save');
Route::get('/departmentList', 'DepartmentController@getList');
Route::post('/departmentDelete', 'DepartmentController@delete');
/*
 * Roles
 */
Route::post('/rolesSave', 'RoleController@save');
Route::get('/rolesList', 'RoleController@list');
Route::get('/rolesDelete', 'RoleController@delete');


// MongoDB Controller

Route::get('/mongoConnection','MongoDBController@checkConnection');


// MSSql Controller

Route::get('/mssqlConnection','MssqlController@checkConnection');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
