<?php

namespace App\Jobs;

use App\Http\Core\ExcelDs\ExcelDs;
use App\Http\Core\MsSQL\MsSQL;
use App\Model\Metadata;
use App\Http\Core\MongoDB\MongoDB; 
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Http\Core\MySQL\MySQL;
use Illuminate\Support\Facades\Log;

class BlendingData implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    protected $metadataId;
    public function __construct($metadataId,$connection)
    {
        //
        $this->metadataId = $metadataId;
        $this->connection=$connection;
    }
    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
       try{
           $metadataResult=Metadata::find($this->metadataId);
           $metadataGetObj=json_decode($metadataResult->metadataObject);
           $metadataId=$metadataGetObj->selectedMetadata;
           $metadataArray=[];
           $tempMetadataArray=[];
           foreach ($metadataId as $id) {
               $metadata = "";
               $metadataObjTemp = Metadata::select("metadataObject", "name")->find($id);
               array_push($tempMetadataArray,$metadataObjTemp);
           }
           foreach ($tempMetadataArray as $metadataObj){
               $metadataObject=json_decode($metadataObj->metadataObject);
               $connection = $metadataObject->connectionObject;
               switch ($connection->datasourceType) {
                   case "mysql":
                       $metadata = (new MySQL())->connect($connection);
                       break;
                   case "mongodb":
                       $metadata = (new MongoDB())->connect($connection);
                       break;
                   case "mssql":
                       $metadata = (new MsSQL())->connect($connection);
                       break;
                   case "excel":
                       $metadata = (new ExcelDs())->connect($connection);
                       break;
               }
               $metadataArray[$metadataObj->name]=($metadata->getDataDashboard($metadataObject))->result;
           }
           foreach($metadataArray as $key=>$value){
               $connection=[];
               $connection['datasourceType']=env('MDB_CONNECTION');
               $connection['dbname']=env('MDB_DATABASE');
               $connection['host']=env('MDB_HOST');
               $connection['port']=env('MDB_PORT');
               $connection['username']=env('MDB_USERNAME');
               $connection['password']=env('MDB_PASSWORD');
               $metadata = (new MongoDB())->connect((Object)$connection);
               $metadata->saveData($key,(Object)$value,$this->metadataId,$this->connection);
           }
           return "done";
       }catch(Exception $e){
           Log::debug("Error Occured");
        }
    }
}
