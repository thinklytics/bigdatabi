<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;

class VerifyCsrfToken extends BaseVerifier
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        //
        '/connectionCheck',
        '/datasourceSave',
        '/getTableList',
        '/dashboardSave',
        '/getQueryData',
    	'/getQueryDataDashboard', 
        '/metadataSave',
    	'/metadataUpdate',
    	'/signUp',
        '/dashboardImageSave'
    ];
}
