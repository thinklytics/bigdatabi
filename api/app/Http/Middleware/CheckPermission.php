<?php

namespace App\Http\Middleware;

use App\Http\Controllers\Controller;
use App\User;
use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;

class CheckPermission
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!Auth::user() || Auth::user()->role=="SA"){
            return $next($request);
        }
        if($request->get('pageName')!='login' && $request->get('pageName')!='change-password' && $request->get('pageName')!='profile' && $request->get('pageName')!='undefined' && $request->get('pageName')!='null' && $request->get('pageName')!=null){
            $user=User::on(Controller::switchDB())->find(Auth::user()->user_id);
            $userCheck=$user->can($request->get('pageName'));
            if(!$userCheck){
                return Response::json([
                    "errorCode"=>401,
                    "message"=>"Unauthorized login again",
                    "result"=>""
                ]);
            }
        }
        return $next($request);
    }
}
