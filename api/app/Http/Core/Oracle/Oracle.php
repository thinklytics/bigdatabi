<?php

namespace App\Http\Core\Oracle;

use App\Http\Contract\DatasourceInterface;
use App\Http\Controllers\Controller;
use App\Http\Controllers\DepartmentController;
use App\Http\Core\Data\QueryResult;
use App\Http\Utils\DBHelper;
use App\Http\Utils\HelperFunctions;
use App\Model\Metadata;
use Exception;
use PDO;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redis;
use Illuminate\Database\Capsule\Manager as DB;
use Illuminate\Support\Str;


class Oracle extends DatasourceInterface
{

    static $con = null;
    static $conObject = null;

    public function connect($connectQuery)
    {
        self::$conObject = $connectQuery;
        self::$con = DBHelper::switchDataBase($connectQuery);
        //$this->con = DB::switchDataBase($connectQuery);
        return $this;
    }
    /*
     * List of data
     */
    public function listDatabase()
    {
        try {
            $query = DB::query("SELECT owner, table_name FROM dba_tables");
            $this->result = $query;
            $this->errorCode = 1;
            $this->message = "Connection Successfully";
        } catch (Exception $ex) {
            $this->errorCode = 0;
            $this->message = $ex->getMessage();
        }
        return $this;
    }

    /*
     * Table Indexs
     */
    public function tableIndexsList($tableName)
    {
        try {
            $indexList = DB::select("SHOW INDEXES FROM $tableName");
            $this->result = $indexList;
            $this->errorCode = 1;
            $this->message = 'Successfully';
        } catch (Exception $ex) {
            $this->errorCode = 0;
            $this->message = $ex->getMessage();
        }
        return $this;

    }

    /*
     * Table index delete
     */
    public function tableIndexsListDelete($indexColumn)
    {
        try {
            $indexColumn = (Object)$indexColumn;
            DB::select("DROP INDEX $indexColumn->Key_name ON $indexColumn->Table");
            $this->result = "";
            $this->errorCode = 1;
            $this->message = 'Successfully';
        } catch (Exception $ex) {
            $this->errorCode = 0;
            $this->message = $ex->getMessage();
        }
        return $this;
    }

    /*
     * Table index create
     */
    public function tableIndexsListCreate($tableIndex, $tableName)
    {
        try {

            foreach ($tableIndex as $table) {
                $table = (Object)$table;
                DB::select("CREATE INDEX $table->keyName ON $tableName ($table->column)");
            }
            $this->result = "";
            $this->errorCode = 1;
            $this->message = 'Successfully';
        } catch (Exception $ex) {
            $this->errorCode = 0;
            $this->message = $ex->getMessage();
        }
        return $this;
    }

    /*
     * Table List
     */
    public function listTables()
    {

        try {
            $tables = DB::select("SELECT table_name FROM user_tables");
            $tableList = [];
            foreach ($tables as $table) {
                array_push($tableList, $table->table_name);
            }
            $this->result = $tableList;
            $this->errorCode = 1;
            $this->message = 'Successfully';
        } catch (Exception $ex) {
            $this->errorCode = 0;
            $this->message = $ex->getMessage();
        }
        return $this;
    }

    /*
     * Query to get data
     */
    function changeObject($ro){
        $ro->Type=$ro->type;
        $ro->Field=$ro->field;
        $ro->tableName=$ro->tablename;
        unset($ro->type);
        unset($ro->field);
        unset($ro->tablename);
        if($ro->Type=="NUMBER"){
            $ro->Type="int";
        }
        if($ro->Type=="FLOAT"){
            $ro->Type="float";
        }
        if($ro->Type=="VARCHAR2"){
            $ro->Type="varchar";
        }
    }
    function getDataLatestVersion($metadataObject,$type){
        $rootTable = $metadataObject->rootTable;
        $tempArrry = [];
        $connectionObject = $metadataObject->connectionObject;
        $dbName = $connectionObject->dbname;
        $tempArrry = [];
        $tableColumnQuery = "SELECT TABLE_NAME as tableName,COLUMN_NAME as Field,DATA_TYPE as Type,data_length FROM USER_TAB_COLUMNS WHERE table_name = '$rootTable'";
        //$tableColumnQuery = "SELECT TABLE_NAME as tableName,COLUMN_NAME as Field,DATA_TYPE as Type FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = '$rootTable' and TABLE_SCHEMA='$dbName'";
        $columnRoot = DB::select($tableColumnQuery);
        foreach ($columnRoot as $ro) {
            //longblob
            $this->changeObject($ro);

            if ($ro->Type == "int" || $ro->Type == "bigint" || $ro->Type == "decimal" || $ro->Type == "double" || $ro->Type == "float") {

                $ro->dataType = "Measure";
            } else {
                $ro->dataType = "Dimension";
            }
            array_push($tempArrry, $ro);
        }
        $flag = false;
        if (isset($metadataObject->joinsDetails)) {
            foreach ($metadataObject->joinsDetails as $ros) {
                $tableColumnQuery = "SELECT TABLE_NAME as tableName,COLUMN_NAME as Field,DATA_TYPE as Type FROM USER_TAB_COLUMNS WHERE table_name = '$ros->rightColumnTable'";
                $tables = DB::select($tableColumnQuery);
                foreach ($tables as $ro) {
                    $this->changeObject($ro);
                    if ($ro->Type == "int" || $ro->Type == "bigint" || $ro->Type == "decimal" || $ro->Type == "double" || $ro->Type == "float") {
                        $ro->dataType = "Measure";
                    } else {
                        $ro->dataType = "Dimension";
                    }
                    $ro->tableName = $ros->rightColumnTable;
                    array_push($tempArrry, $ro);
                }
                $flag = true;
            }
        }
        $tableCol = $tempArrry;//All Column
        //Table column array with key
        $tableColumnArray = array();
        $tableColumnComma = "";
        $duplicateColumn = [];
        $i = 0;
        $tableIndex = 0;
        $blobColumn = [];
        foreach ($tableCol as $col) {
            if ($col->Type == "longblob" || $col->Type == "blob") {
                array_push($blobColumn, $tableIndex);
            } else if (isset($p[$col->Field])) {
                array_push($duplicateColumn, $col->Field);
            } else {
                $p[$col->Field] = true;
            }
            $tableIndex++;
        }
        foreach ($blobColumn as $key => $value) {
            unset($tableCol[$value]);
        }
        foreach ($tableCol as $ro) {
            if (isset($metadataObject->blending)) {
                $tableColumnComma .= $ro->tableName . '.' . $ro->Field . ' as "' . $ro->Field . '_' . $ro->tableName . '",';
                $ro->Field = $ro->Field . "_" . $ro->tableName;
            } else {
                $tableColumnComma .= $ro->tableName . '.' . $ro->Field . ' as "' . $ro->Field . '(' . $ro->tableName . ')",';
                $ro->Field = $ro->Field . "(" . $ro->tableName . ")";
            }
            $tableColumnArray[$ro->Field] = $ro;
        }
        $tableColumnObject = json_encode($tableColumnArray);
        //**********************Table Data***********************************
        $tableColumnComma = rtrim($tableColumnComma, ",");
        $query = "select $tableColumnComma from $rootTable";
        if (isset($metadataObject->joinsDetails)) {
            foreach ($metadataObject->joinsDetails as $ros) {
                $i = 0;
                $query .= " $ros->join join $ros->rightColumnTable";
                foreach ($ros->node as $row) {

                    if (is_object(json_decode($row->leftColumn))) {
                        $leftColumn = json_decode($row->leftColumn);
                        $rightColumn = json_decode($row->rightColumn);
                        $joinLeftColumn = $leftColumn->tableName . '.' . $leftColumn->Field;
                        $joinRightColumn = $rightColumn->tableName . "." . $rightColumn->Field;
                    } else {
                        $joinLeftColumn =  $ros->leftColumnTable . "." . $row->leftColumn;
                        $joinRightColumn = $ros->leftColumnTable . "." . $row->leftColumn;
                    }
                    //$leftColumnTable = trim(preg_replace('/\s\s+/', ' ', $ros->leftColumnTable));
                    //$rightColumnTable = trim(preg_replace('/\s\s+/', ' ', $ros->rightColumnTable));
                    $leftColumn = trim(preg_replace('/\s\s+/', ' ', $joinLeftColumn));
                    $rightColumn = trim(preg_replace('/\s\s+/', ' ', $joinRightColumn));
                    if ($i == 0) {
                        $p = " on  ";
                    } else {
                        $p = " and ";
                    }
                    $query .= " $p  $leftColumn=$rightColumn";
                    $i++;
                }
            }
        }
        $start = microtime(true);
        $query = trim(preg_replace('/\s\s+/', ' ', $query));
        if (isset($metadataObject->condition) && $metadataObject->condition) {
            //$return = preg_match("/^[a-zA-Z ]+$/", $string);
            while (preg_match_all('/(\w*)(\(((\w+))\))+/', $metadataObject->condition, $out)) {
                $str = $out[3][0] . "." . $out[1][0];
                $metadataObject->condition = str_replace($out[0][0], $str, $metadataObject->condition);
            }
            $query .= " where " . $metadataObject->condition;
            if (isset($metadataObject->limit) && $metadataObject->limit) {
                if ($metadataObject->limit < 100)
                    $query .= " and ROWNUM <= " . $metadataObject->limit;
                else
                    $query .= " and ROWNUM <= 100";
            }

        }else{
            if (isset($metadataObject->limit) && $metadataObject->limit) {
                if ($metadataObject->limit < 100)
                    $query .= " where ROWNUM <= " . $metadataObject->limit;
                else
                    $query .= " where ROWNUM <= 100";
            } else {
                $query .= " where ROWNUM <= 100";
            }
        }


        $tablesData = DB::select($query);//Table Data
        return ["tablesData"=>$tablesData,"tableColumn"=>$tableColumnObject];
    }
    function getQueryColumnOldV($metadataObject,$type,$incrementObj){
        $rootTable = $metadataObject->rootTable;
        $flagWhere=false;
        $tempArrry = [];
        $connectionObject = $metadataObject->connectionObject;
        $dbName = $connectionObject->dbname;
        $tempArrry = [];
        $tableColumnQuery = "SELECT TABLE_NAME as tableName,COLUMN_NAME as Field,DATA_TYPE as Type,data_length FROM USER_TAB_COLUMNS WHERE table_name = '$rootTable'";
        //$tableColumnQuery = "SELECT TABLE_NAME as tableName,COLUMN_NAME as Field,DATA_TYPE as Type FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = '$rootTable' and TABLE_SCHEMA='$dbName'";
        $columnRoot = DB::select($tableColumnQuery);
        foreach ($columnRoot as $ro) {
            //longblob
            $this->changeObject($ro);
            if ($ro->Type == "int" || $ro->Type == "bigint" || $ro->Type == "decimal" || $ro->Type == "double" || $ro->Type == "float") {
                $ro->dataType = "Measure";
            } else {
                $ro->dataType = "Dimension";
            }
            array_push($tempArrry, $ro);
        }
        $flag = false;
        if (isset($metadataObject->joinsDetails)) {
            foreach ($metadataObject->joinsDetails as $ros) {
                $tableColumnQuery = "SELECT TABLE_NAME as tableName,COLUMN_NAME as Field,DATA_TYPE as Type FROM USER_TAB_COLUMNS WHERE table_name = '$ros->rightColumnTable'";
                $tables = DB::select($tableColumnQuery);
                foreach ($tables as $ro) {
                    $this->changeObject($ro);
                    if ($ro->Type == "int" || $ro->Type == "bigint" || $ro->Type == "decimal" || $ro->Type == "double" || $ro->Type == "float") {
                        $ro->dataType = "Measure";
                    } else {
                        $ro->dataType = "Dimension";
                    }
                    $ro->tableName = $ros->rightColumnTable;
                    array_push($tempArrry, $ro);
                }
                $flag = true;
            }
        }
        $tableCol = $tempArrry;//All Column
        //Table column array with key
        $tableColumnArray = array();
        $tableColumnComma = "";
        $duplicateColumn = [];
        $i = 0;
        $tableIndex = 0;
        $blobColumn = [];
        foreach ($tableCol as $col) {
            if ($col->Type == "longblob" || $col->Type == "blob") {
                array_push($blobColumn, $tableIndex);
            } else if (isset($p[$col->Field])) {
                array_push($duplicateColumn, $col->Field);
            } else {
                $p[$col->Field] = true;
            }
            $tableIndex++;
        }
        foreach ($blobColumn as $key => $value) {
            unset($tableCol[$value]);
        }
        $randomColumnArray=[];
        foreach ($tableCol as $ro) {
            if (isset($metadataObject->blending)) {
                $randomVal=strtolower(Str::random(20));
                $randomColumnArray[$randomVal]=$ro->Field . '_' . $ro->tableName;
                $tableColumnComma .= $ro->tableName . '.' . $ro->Field . ' as "'.$randomVal.'",';
                $ro->Field = $ro->Field . "_" . $ro->tableName;
            } else {
                $randomVal=strtolower(Str::random(20));
                $randomColumnArray[$randomVal]=$ro->Field . '(' . $ro->tableName . ')';
                $tableColumnComma .= $ro->tableName . '.' . $ro->Field . ' as "'.$randomVal.'",';
                $ro->Field = $ro->Field . "(" . $ro->tableName . ")";
            }
            $tableColumnArray[$ro->Field] = $ro;
        }
        $tableColumnObject = json_encode($tableColumnArray);
        //**********************Table Data***********************************
        $tableColumnComma = rtrim($tableColumnComma, ",");
        $querylength="select count(*) as dataLength from $rootTable";
        $query = "select $tableColumnComma from $rootTable";
        if (isset($metadataObject->joinsDetails)) {
            foreach ($metadataObject->joinsDetails as $ros) {
                $i = 0;
                $query .= " $ros->join join $ros->rightColumnTable";
                $querylength .= " $ros->join join $ros->rightColumnTable";
                foreach ($ros->node as $row) {
                    if (is_object(json_decode($row->leftColumn))) {
                        $leftColumn = json_decode($row->leftColumn);
                        $rightColumn = json_decode($row->rightColumn);
                        $joinLeftColumn = $leftColumn->tableName . '.' . $leftColumn->Field;
                        $joinRightColumn = $rightColumn->tableName . "." . $rightColumn->Field;
                    } else {
                        $joinLeftColumn =  $ros->leftColumnTable . "." . $row->leftColumn;
                        $joinRightColumn = $ros->leftColumnTable . "." . $row->leftColumn;
                    }
                    $leftColumn = trim(preg_replace('/\s\s+/', ' ', $joinLeftColumn));
                    $rightColumn = trim(preg_replace('/\s\s+/', ' ', $joinRightColumn));
                    if ($i == 0) {
                        $p = " on  ";
                    } else {
                        $p = " and ";
                    }
                    $query .= " $p  $leftColumn=$rightColumn";
                    $querylength .= " $p  $leftColumn=$rightColumn";
                    $i++;
                }
            }
        }
        $start = microtime(true);
        $query = trim(preg_replace('/\s\s+/', ' ', $query));
        $column="";
        $maxDateQuery="";
        $roleLevellengthQ="";
        $roleLevel=false;
        $roleLevellength=0;
        $dataLength=0;
        $limit=env('Cache_Limit');
        if($type=="dashboard"){
            if (isset($metadataObject->condition) && $metadataObject->condition) {
                $flagWhere=true;
                //$return = preg_match("/^[a-zA-Z ]+$/", $string);
                while (preg_match_all('/(\w*)(\(((\w+))\))+/', $metadataObject->condition, $out)) {
                    $str = "`" . $out[3][0] . "`.`" . $out[1][0] . "`";
                    $metadataObject->condition = str_replace($out[0][0], $str, $metadataObject->condition);
                }
                $query .= " where " . $metadataObject->condition;
                $querylength .= " where " . $metadataObject->condition;
                if (isset($metadataObject->exCondition)) {
                    foreach ($metadataObject->exCondition as $condition) {
                        foreach (json_decode($condition) as $key => $value) {
                            $roleLevellengthQ .= $querylength." and " . $value;
                        }
                    }
                }
            } else if (isset($metadataObject->exCondition)) {
                $flagWhere=true;
                $roleLevel=true;
                foreach ($metadataObject->exCondition as $condition) {
                    foreach (json_decode($condition) as $key => $value) {
                        if ($i == 0) {
                            $roleLevellengthQ .= $querylength." where " . $value;
                        } else {
                            $roleLevellengthQ .= $querylength." and " . $value;
                        }
                        $i++;
                    }
                }
            }
            if($incrementObj && isset($metadataObject->realtime)){
                $realTimeObj=$metadataObject->realtime;
                if(isset($realTimeObj->column)){
                    $column=json_decode($realTimeObj->column);
                    $colName=$column->columnName;
                    //$columnName=explode("(",$column->columnName)[0];
                    if($flagWhere){
                        $maxDateQuery = " order by '" . $colName."' DESC fetch first 1 rows only";
                    }else{
                        $maxDateQuery = " order by '" . $colName."' DESC fetch first 1 rows only";
                    }
                }
            }else{
                if (isset($metadataObject->limit) && $metadataObject->limit) {
                    if($flagWhere){
                        $maxDateQuery .= " and ROWNUM <= " . $metadataObject->limit;
                    }else{
                        $maxDateQuery .= " where ROWNUM <= " . $metadataObject->limit;
                    }
                }
            }

            $tablesLengthData = DB::select($querylength);
            if($roleLevel){
                $roleLevellength=DB::select($roleLevellengthQ)[0]->datalength;
            }
            $dataLength=$tablesLengthData[0]->datalength;
            $lastLimit=0;

            /*
             * Check set limit and get data limit
             */
            if($limit>$dataLength){
                $limit=$dataLength;
            }
            if(isset($metadataObject->limit) && $metadataObject->limit && $limit>$metadataObject->limit){
                $limit=$metadataObject->limit;
            }
            if(isset($metadataObject->limit) && $dataLength > $metadataObject->limit && $metadataObject->limit){
                $dataLength=$metadataObject->limit;
            }
        }else{
            if (isset($metadataObject->condition) && $metadataObject->condition) {
                //$return = preg_match("/^[a-zA-Z ]+$/", $string);
                while (preg_match_all('/(\w*)(\(((\w+))\))+/', $metadataObject->condition, $out)) {
                    $str = $out[3][0] . "." . $out[1][0];
                    $metadataObject->condition = str_replace($out[0][0], $str, $metadataObject->condition);
                }
                $query .= " where " . $metadataObject->condition;
                if (isset($metadataObject->limit) && $metadataObject->limit) {
                    if ($metadataObject->limit < 100)
                        $query .= " and ROWNUM <= " . $metadataObject->limit;
                    else
                        $query .= " and ROWNUM <= 100";
                }

            }else{
                if (isset($metadataObject->limit) && $metadataObject->limit) {
                    if ($metadataObject->limit < 100)
                        $query .= " where ROWNUM <= " . $metadataObject->limit;
                    else
                        $query .= " where ROWNUM <= 100";
                } else {
                    $query .= " where ROWNUM <= 100";
                }
            }
        }
        return ["query"=>$query,"roleLevel"=>$roleLevel,"roleLevellength"=>$roleLevellength,"dataLength"=>$dataLength,"limit"=>$limit,'randomColumn'=>$randomColumnArray,'tablesColumn'=>$tableColumnObject,'maxDateQuery'=>$maxDateQuery,'flagWhere'=>$flagWhere,'column'=>$column];
    }
    function getDataOldVersion($metadataObject,$type,$incrementObj){
        $queryColumnArr=$this->getQueryColumnOldV($metadataObject,$type,$incrementObj);
        if($type=="dashboard"){
            return $queryColumnArr;
        }
        $tablesData = DB::select($queryColumnArr['query']);//Table Data
        $tablesData=$this->replaceColumnName($tablesData,$queryColumnArr['randomColumn']);
        return ["tablesData"=>$tablesData,"tableColumn"=>$queryColumnArr['tablesColumn']];
    }
    function replaceColumnName($tablesData,$replaceColumnArray){
        $str=json_encode($tablesData);
        $key = array_keys($replaceColumnArray);
        $value   = array_values($replaceColumnArray);
        $result = str_replace($key,$value, $str);
        return json_decode($result);
    }
    public function getData($metadataObject)
    {
        try {
            //***********************Table column Name object*******************

            //For Fetch Column Details
            $oracleVersion = DB::select('SELECT version FROM v$instance');
            if($oracleVersion[0]->version>=12.2){
                $table=$this->getDataLatestVersion($metadataObject,"metadata");
            }else{
                $table=$this->getDataOldVersion($metadataObject,"metadata","");
            }
            //Execution time
            $tableDataArray = array();
            $tableData = json_encode($table['tablesData']);
            $this->result = array("tableColumn" => $table['tableColumn'], "tableData" => $tableData);
            $this->errorCode = 1;
            $this->message = 'Successfully';
        } catch (Exception $ex) {
            $this->errorCode = 0;
            $this->message = $ex->getMessage();
            $this->result = "";
        }
        DB::disconnect();
        return $this;
    }


    /*
     * ************************************** Server side code ******************************************************
     */
    //  Cache data to redis
    public function queryDataToredis($metadataObject, $incrementObj,$token,$department)
    {
        /*ini_set('memory_limit', '-1');
        set_time_limit(0);*/
        /*
         * Limited Obj
         */
        ini_set('memory_limit', '-1');
        set_time_limit(0);
        $data = array();
        $company_id=Auth::user()->company_id;
        $client_id=Auth::user()->id;
        if($incrementObj && isset($metadataObject->realtime)){
            $client_id=$client_id."-realtime";
            $token=$token."-realtime";
        }
        /*
         * Role level conditions
         */
        $roleLevelCondition="";
        $userType=false;
        if(isset($metadataObject->exCondition)){
            $roleLevelCondition=$metadataObject->exCondition;
            $userType=$metadataObject->userType;
        }
        $realTimeFlag=false;
        if(isset($metadataObject->realtime)){
            $realTimeFlag=true;
        }
        if (!HelperFunctions::isCachedInRedis($client_id, $metadataObject->metadataId,$company_id,$realTimeFlag)) {
            try {
                /*
                 * Check data
                 */
                $oracleVersion = DB::select('SELECT version FROM v$instance');
                if($oracleVersion[0]->version>=12.2){
                    $queryColumnArr=$this->getDataLatestVersion($metadataObject,"dashboard");
                }else{
                    $queryColumnArr=$this->getDataOldVersion($metadataObject,"dashboard",$incrementObj);
                }

                $dataLength=$queryColumnArr['dataLength'];
                $limit=$queryColumnArr['limit'];
                $query=$queryColumnArr['query'];
                $maxDateQuery=$queryColumnArr['maxDateQuery'];
                $rs=0;
                $tableColumnObject=$queryColumnArr['tablesColumn'];
                $roleLevel=$queryColumnArr['roleLevel'];
                $roleLevellength=$queryColumnArr['roleLevellength'];
                for($i=0;$i<$dataLength;$i=$i+$limit){
                    if($realTimeFlag==false){
                        $queryNew =$query." OFFSET ".$i." ROWS FETCH NEXT ".$limit." ROWS ONLY";
                        $lastLimit=$i+1;
                        $tablesData = DB::select($queryNew);//Table Data
                    }else{
                        $tablesData = DB::select($query." ".$maxDateQuery);//Table Data
                    }
                    $tablesData=$this->replaceColumnName($tablesData,$queryColumnArr['randomColumn']);
                    if($incrementObj && isset($metadataObject->realtime) && $maxDateQuery!=""){
                        $realTimeObj="";
                        if(isset($metadataObject->realtime)){
                            $realTimeObj=$metadataObject->realtime;
                        }
                        if(count($tablesData)){
                            $column=$queryColumnArr['column'];
                            $colName=$column->columnName;
                            $maxDate=$tablesData[0]->$colName;
                            $maxDate=date('Y-m-d 23:59:59',strtotime($maxDate));
                            //dd($realTimeObj->dataShow);
                            $start_date=date('Y-m-d 00:00:00', strtotime($maxDate. " - $realTimeObj->dataShow days"));
                            $end_date=$maxDate;
                            $columnName=explode("(",$column->columnName)[0];
                            if(!$queryColumnArr['flagWhere']){
                                $queryColumnArr['query'] .= " where ".$column->tableName.".".$columnName." between '".$start_date."' and '".$end_date."'";
                            }else{
                                $queryColumnArr['query'] .= " and ".$column->tableName.".".$columnName." between '".$start_date."' and '".$end_date."'";
                            }
                            $tablesData = DB::select($queryColumnArr['query']);
                        }else{
                            $tablesData = DB::select($queryColumnArr['query']);
                        }

                    }
                    $tablesData=$this->replaceColumnName($tablesData,$queryColumnArr['randomColumn']);
                    //Execution time
                    $tableDataArray = array();
                    $tableDataArray = array();
                    $tableData  = json_encode($tablesData);
                    $tableColumnObject = json_decode($tableColumnObject);
                    if(isset($metadataObject->column))
                        foreach ($metadataObject->column as $column) {
                            if(isset($column->columnName))
                                $columnName = $column->columnName;
                            if (isset($tableColumnObject->$columnName) && isset($column->dataKey)) {
                                $tableColumnObject->$columnName->dataType = $column->dataKey;
                            }
                        }
                    $tableColumnObject = json_encode($tableColumnObject);
                    $this->result = array("tableColumn" => $tableColumnObject, "tableData" => $tableData);
                    $this->errorCode = 1;
                    $this->message = 'Successfully';
                    $tempArr=[];
                    $tempArr['flag']=1;
                    $tempArr['ukey']=$rs;
                    $tempArr['dataLength']=$dataLength;
                    if($roleLevel){
                        $tempArr['roleLevelLength']=$roleLevellength;
                    }
                    $data = HelperFunctions::writeToRedis($client_id, $metadataObject->metadataId, $this->result, $incrementObj,$company_id,$token,$tempArr,$roleLevelCondition,$department,$realTimeFlag,$userType);
                    $rs++;
                }

            } catch (Exception $ex) {
                dd($ex->getMessage());
                $this->errorCode = 0;
                $this->message = $ex->getMessage();
            }
        } else {
            $data = HelperFunctions::writeToRedis($client_id, $metadataObject->metadataId, $this, $incrementObj,$company_id,$token,0,$roleLevelCondition,$department,$realTimeFlag,$userType);
            $this->redisAddData($metadataObject->metadataId.":".$company_id, $metadataObject, $incrementObj,$token,$client_id,$realTimeFlag);
        }
        DB::disconnect();
        return $data;

    }
    /*
         * Get column name
         */
    public function getColumnName($tableName)
    {
        try {
            $tableColumnQuery = "SELECT TABLE_NAME as tableName,COLUMN_NAME as Field,DATA_TYPE as Type,data_length FROM USER_TAB_COLUMNS WHERE table_name = '$tableName' ";
            $tables = DB::select($tableColumnQuery);
            foreach ($tables as $ro) {
                $this->changeObject($ro);
                if ($ro->Type == "int") {
                    $ro->dataType = "Measure";
                } else {
                    $ro->dataType = "Dimension";
                }
                $ro->tableName = $tableName;
            }
            $this->result = $tables;
            $this->errorCode = 1;
            $this->message = 'Successfully';
        } catch (Exception $ex) {
            $this->errorCode = 0;
            $this->message = $ex->getMessage();
        }
        return $this;
    }

    public function getColumnMultiTable($tablesObj)
    {
        try {
            $tableArr = [];
            foreach ($tablesObj as $table) {
                $tableColumnQuery = "SELECT TABLE_NAME as tableName,COLUMN_NAME as Field,DATA_TYPE as Type,data_length FROM USER_TAB_COLUMNS WHERE table_name = '$table' ";
                $tables = DB::select($tableColumnQuery);
                foreach ($tables as $ro) {
                    $this->changeObject($ro);
                    if ($ro->Type == "int") {
                        $ro->dataType = "Measure";
                    } else {
                        $ro->dataType = "Dimension";
                    }
                    $ro->tableName = $table;
                }
                $tableArr[$table] = $tables;
            }
            $this->result = $tableArr;
            $this->errorCode = 1;
            $this->message = 'Successfully';
        } catch (Exception $ex) {
            $this->errorCode = 0;
            $this->message = $ex->getMessage();
        }
        return $this;
    }
    public function getDataLength($metadataObject)
    {
        try {
            $rootTable = $metadataObject->rootTable;
            //***********************Table column Name object*******************
            $tempArrry = [];
            $connectionObject = $metadataObject->connectionObject;
            $dbName = $connectionObject->dbname;
            $flag = false;
            //Table column array with key
            //**********************Table Data***********************************
            $query = "select count(*) as length from $rootTable";
            if (isset($metadataObject->joinsDetails)) {
                foreach ($metadataObject->joinsDetails as $ros) {
                    $i = 0;
                    $query .= " $ros->join join $ros->rightColumnTable";
                    foreach ($ros->node as $row) {

                        if (is_object(json_decode($row->leftColumn))) {
                            $leftColumn = json_decode($row->leftColumn);
                            $rightColumn = json_decode($row->rightColumn);
                            $joinLeftColumn = $leftColumn->tableName . "." . $leftColumn->Field;
                            $joinRightColumn = $rightColumn->tableName . "." . $rightColumn->Field;
                        } else {
                            $joinLeftColumn = $ros->leftColumnTable . "." . $row->leftColumn;
                            $joinRightColumn =  $ros->leftColumnTable . "." . $row->leftColumn;
                        }
                        //$leftColumnTable = trim(preg_replace('/\s\s+/', ' ', $ros->leftColumnTable));
                        //$rightColumnTable = trim(preg_replace('/\s\s+/', ' ', $ros->rightColumnTable));
                        $leftColumn = trim(preg_replace('/\s\s+/', ' ', $joinLeftColumn));
                        $rightColumn = trim(preg_replace('/\s\s+/', ' ', $joinRightColumn));
                        if ($i == 0) {
                            $p = " on  ";
                        } else {
                            $p = " and ";
                        }
                        $query .= " $p  $leftColumn=$rightColumn";
                        $i++;
                    }
                }
            }
            $start = microtime(true);
            $query = trim(preg_replace('/\s\s+/', ' ', $query));
            if (isset($metadataObject->condition) && $metadataObject->condition) {
                //$return = preg_match("/^[a-zA-Z ]+$/", $string);
                while (preg_match_all('/(\w*)(\(((\w+))\))+/', $metadataObject->condition, $out)) {
                    $str = $out[3][0] . "." . $out[1][0];
                    $metadataObject->condition = str_replace($out[0][0], $str, $metadataObject->condition);
                }
                $query .= " where " . $metadataObject->condition;
            }
            //$query .= " limit 1000";
            if (isset($metadataObject->limit) && $metadataObject->limit) {
                $query .= " limit " . $metadataObject->limit;
            }
            $tablesData = DB::select($query);//Table Data
            $tablesCount = $tablesData[0]->length;
            $this->result = $tablesCount;
            $this->errorCode = 1;
            $this->message = 'Successfully';
        } catch (Exception $ex) {

            $this->errorCode = 0;
            $this->message = $ex->getMessage();
        }
        return $this;
    }

    //  Cache data to redis
    public function queryDataToredisPublic($metadataObject,$incrementObj, $uid,$token)
    {
        ini_set('memory_limit', '-1');
        set_time_limit(0);
        $data = array();
        if (!HelperFunctions::isCachedInRedis(0, $metadataObject->metadataId, $uid,false)) {
            /*
             * Get data form query
             */
            try {
                $oracleVersion = DB::select('SELECT version FROM v$instance');
                if($oracleVersion[0]->version>=12.2){
                    $table=$this->getDataLatestVersion($metadataObject,"metadata");
                }else{
                    $table=$this->getDataOldVersion($metadataObject,"metadata","");
                }
                $tableDataArray = array();
                //$tables = DB::select($query);
                $tableData  = json_encode($table['tablesData']);
                $tableColumnObject = $table['tableColumn'];
                //Execution time
                $tableDataArray = array();
                $this->result = array("tableColumn" => $tableColumnObject, "tableData" => $tableData);
                $this->errorCode = 1;
                $this->message = 'Successfully';
                $data = HelperFunctions::writeToRedis(0, $metadataObject->metadataId, $this->result, $incrementObj, $uid,$token,1,"","",false,false);
            } catch (Exception $ex) {
                $this->errorCode = 0;
                $this->message = $ex->getMessage();
            }
        } else {
            $data = HelperFunctions::writeToRedis(0, $metadataObject->metadataId, $this, $incrementObj, $uid,$token,0,"","",false,false);
            $this->redisAddData($metadataObject->metadataId.":".$uid, $metadataObject, $incrementObj,$token,0,false);
        }
        return $data;
    }

    public static function getLimitData($metadataObject, $metadataObj, $currentLength, $incrementObj)
    {
        /*
         * Connection
         */
        $lastLength = $metadataObj->length;
        $lastUpdatedTime = $metadataObj->updated_at;
        (new Oracle())->connect($metadataObject->connectionObject);
        /*
         * Query to get data
         */
        $rootTable = $metadataObject->rootTable;
        //***********************Table column Name object*******************
        $tempArrry = [];
        $connectionObject = $metadataObject->connectionObject;
        $dbName = $connectionObject->dbname;
        $tableColumnQuery = "SELECT TABLE_NAME as tableName,COLUMN_NAME as Field,DATA_TYPE as Type,data_length FROM USER_TAB_COLUMNS WHERE table_name = '$rootTable'";
        //$tableColumnQuery = "SELECT TABLE_NAME as tableName,COLUMN_NAME as Field,DATA_TYPE as Type FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = '$rootTable' and TABLE_SCHEMA='$dbName'";
        $tables = DB::select($tableColumnQuery);
        $colName=$tables[0]->field;
        foreach ($tables as $ro) {
            (new Oracle())->changeObject($ro);
            //longblob
            if ($ro->Type == "int" || $ro->Type == "bigint" || $ro->Type == "decimal" || $ro->Type == "double" || $ro->Type == "float") {
                $ro->dataType = "Measure";
            } else {
                $ro->dataType = "Dimension";
            }
            array_push($tempArrry, $ro);
        }
        $flag = false;
        if (isset($metadataObject->joinsDetails)) {
            foreach ($metadataObject->joinsDetails as $ros) {
                $tableColumnQuery = "SELECT TABLE_NAME as tableName,COLUMN_NAME as Field,DATA_TYPE as Type,data_length FROM USER_TAB_COLUMNS WHERE table_name = '$ros->rightColumnTable'";
                //$tableColumnQuery = "SELECT TABLE_NAME as tableName,COLUMN_NAME as Field,DATA_TYPE as Type FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = '$ros->rightColumnTable' and TABLE_SCHEMA='$dbName'";
                $tables = DB::select($tableColumnQuery);
                foreach ($tables as $ro) {
                    changeObject($ro);
                    if ($ro->Type == "int" || $ro->Type == "bigint" || $ro->Type == "decimal" || $ro->Type == "double" || $ro->Type == "float") {
                        $ro->dataType = "Measure";
                    } else {
                        $ro->dataType = "Dimension";
                    }
                    $ro->tableName = $ros->rightColumnTable;
                    array_push($tempArrry, $ro);
                }
                $flag = true;
            }
        }
        $tableCol = $tempArrry;//All Column
        //Table column array with key
        $tableColumnArray = array();
        $tableColumnComma = "";
        $duplicateColumn = [];
        $i = 0;
        $tableIndex = 0;
        $blobColumn = [];
        foreach ($tableCol as $col) {
            if ($col->Type == "longblob" || $col->Type == "blob") {
                array_push($blobColumn, $tableIndex);
            } else if (isset($p[$col->Field])) {
                array_push($duplicateColumn, $col->Field);
            } else {
                $p[$col->Field] = true;
            }
            $tableIndex++;
        }
        foreach ($blobColumn as $key => $value) {
            unset($tableCol[$value]);
        }

        $randomColumnArray=[];
        foreach ($tableCol as $ro) {
            if (isset($metadataObject->blending)) {
                $randomVal=strtolower(Str::random(20));
                $randomColumnArray[$randomVal]=$ro->Field . '_' . $ro->tableName;
                $tableColumnComma .= $ro->tableName . '.' . $ro->Field . ' as "'.$randomVal.'",';
                $ro->Field = $ro->Field . "_" . $ro->tableName;
            } else {
                $randomVal=strtolower(Str::random(20));
                $randomColumnArray[$randomVal]=$ro->Field . '(' . $ro->tableName . ')';
                $tableColumnComma .= $ro->tableName . '.' . $ro->Field . ' as "'.$randomVal.'",';
                $ro->Field = $ro->Field . "(" . $ro->tableName . ")";
            }
            $tableColumnArray[$ro->Field] = $ro;
        }
        $tableColumnObject = json_encode($tableColumnArray);
        //**********************Table Data***********************************
        $tableColumnComma = rtrim($tableColumnComma, ",");
        $tableColumnObject = json_encode($tableColumnArray);
        //**********************Table Data***********************************
        $query = "select $tableColumnComma from $rootTable";
        if (isset($metadataObject->joinsDetails)) {
            foreach ($metadataObject->joinsDetails as $ros) {
                $i = 0;
                $query .= " $ros->join join $ros->rightColumnTable";
                foreach ($ros->node as $row) {
                    if (is_object(json_decode($row->leftColumn))) {
                        $leftColumn = json_decode($row->leftColumn);
                        $rightColumn = json_decode($row->rightColumn);
                        $joinLeftColumn = $leftColumn->tableName . '.' . $leftColumn->Field;
                        $joinRightColumn = $rightColumn->tableName . "." . $rightColumn->Field;
                    } else {
                        $joinLeftColumn =  $ros->leftColumnTable . "." . $row->leftColumn;
                        $joinRightColumn = $ros->leftColumnTable . "." . $row->leftColumn;
                    }
                    $leftColumn = trim(preg_replace('/\s\s+/', ' ', $joinLeftColumn));
                    $rightColumn = trim(preg_replace('/\s\s+/', ' ', $joinRightColumn));
                    if ($i == 0) {
                        $p = " on  ";
                    } else {
                        $p = " and ";
                    }
                    $query .= " $p  $leftColumn=$rightColumn";
                    $i++;
                }
            }
        }
        $start = microtime(true);
        $query = trim(preg_replace('/\s\s+/', ' ', $query));
        $column="";
        $maxDateQuery="";
        $roleLevellengthQ="";
        $roleLevel=false;
        $roleLevellength=0;
        $dataLength=0;
        $flagWhere=false;
        if (isset($metadataObject->condition) && $metadataObject->condition) {
            $flagWhere=true;
            //$return = preg_match("/^[a-zA-Z ]+$/", $string);
            while (preg_match_all('/(\w*)(\(((\w+))\))+/', $metadataObject->condition, $out)) {
                $str = "`" . $out[3][0] . "`.`" . $out[1][0] . "`";
                $metadataObject->condition = str_replace($out[0][0], $str, $metadataObject->condition);
            }
            $query .= " where " . $metadataObject->condition;
            if (isset($metadataObject->exCondition)) {
                foreach ($metadataObject->exCondition as $condition) {
                    foreach (json_decode($condition) as $key => $value) {
                        $query .= " and " . $value;
                    }
                }
            }
        } else if (isset($metadataObject->exCondition)) {
            $flagWhere=true;
            $roleLevel=true;
            foreach ($metadataObject->exCondition as $condition) {
                foreach (json_decode($condition) as $key => $value) {
                    if ($i == 0) {
                        $query .= " where " . $value;
                    } else {
                        $query .= " and " . $value;
                    }
                    $i++;
                }
            }
        }
        if($incrementObj){
            $incrementObj = json_decode($incrementObj);
            foreach ($incrementObj->table as $table) {
                $tableName = $table->name;
                if (isset($metadataObj->updated_at->$tableName)) {
                    //$dateTime = date('d-M-y h:i:s a',strtotime($metadataObj->updated_at->$tableName));
                    $dateTime = $metadataObj->updated_at->$tableName;
                    if($flagWhere){
                        $query .= " and " . $table->name . "." . $table->key . " > '" . $dateTime . "'";
                    }else{
                        $query .= " where " . $table->name . "." . $table->key . " > '" . $dateTime . "'";
                    }
                }
            }
        }else{
            if (isset($metadataObject->limit) && $metadataObject->limit) {
                if($flagWhere){
                    $query .= " and ROWNUM <= $metadataObject->limit";
                }else{
                    $query .= " where ROWNUM <= $metadataObject->limit";
                }

            } else {
                $query .= " OFFSET ".$lastLength." ROWS FETCH NEXT ".$currentLength." ROWS ONLY";
            }
        }
        $tablesData = DB::select($query);//Table Data
        $tablesData=(new Oracle())->replaceColumnName($tablesData,$randomColumnArray);
        return $tablesData;
    }

    public static function checkLength($metadataObject, $metadataDetailsObj, $incrementObj)
    {
        /*
         * Check length
         */
        $limit = $metadataDetailsObj->length;
        if (isset($metadataObject->type) && $metadataObject->type == "blending") {
            $connection = [];
            $connection['datasourceType'] = env('MDB_CONNECTION', 'mongodb');
            $connection['dbname'] = env('MDB_DATABASE', 'commondb');
            $connection['host'] = env('MDB_HOST', '101.53.130.66');
            $connection['port'] = env('MDB_PORT', '27017');
            $connection['username'] = env('MDB_USERNAME', 'admin');
            $connection['password'] = env('MDB_PASSWORD', 'hellothinklayer1');
            $metadataObject->connectionObject = (Object)$connection;
        }
        $db = (new Oracle())->connect($metadataObject->connectionObject);
        /*
         * Query to get data
         */
        $rootTable = $metadataObject->rootTable;
        $query = 'select count(*) as "length" from '.$rootTable;
        if (isset($metadataObject->joinsDetails)) {
            foreach ($metadataObject->joinsDetails as $ros) {
                $i = 0;
                $query .= " $ros->join join $ros->rightColumnTable";
                foreach ($ros->node as $row) {
                    if (is_object(json_decode($row->leftColumn))) {
                        $leftColumn = json_decode($row->leftColumn);
                        $rightColumn = json_decode($row->rightColumn);
                        $joinLeftColumn =  $leftColumn->tableName . "." . $leftColumn->Field;
                        $joinRightColumn = $rightColumn->tableName . "." . $rightColumn->Field;
                    } else {
                        $joinLeftColumn =  $ros->leftColumnTable . "." . $row->leftColumn;
                        $joinRightColumn =  $ros->leftColumnTable . "." . $row->leftColumn;
                    }
                    //$leftColumnTable = trim(preg_replace('/\s\s+/', ' ', $ros->leftColumnTable));
                    //$rightColumnTable = trim(preg_replace('/\s\s+/', ' ', $ros->rightColumnTable));
                    $leftColumn = trim(preg_replace('/\s\s+/', ' ', $joinLeftColumn));
                    $rightColumn = trim(preg_replace('/\s\s+/', ' ', $joinRightColumn));
                    if ($i == 0) {
                        $p = " on  ";
                    } else {
                        $p = " and ";
                    }
                    $query .= " $p  $leftColumn=$rightColumn";
                    $i++;
                }
            }
        }
        $start = microtime(true);
        $query = trim(preg_replace('/\s\s+/', ' ', $query));
        /*if (isset($metadataObject->condition) && $metadataObject->condition) {
            //$return = preg_match("/^[a-zA-Z ]+$/", $string);
            while (preg_match_all('/(\w*)(\(((\w+))\))+/', $metadataObject->condition, $out)) {
                $str = $out[3][0] . "." . $out[1][0];
                $metadataObject->condition = str_replace($out[0][0], $str, $metadataObject->condition);
            }
            $query .= " where " . $metadataObject->condition;
            if (isset($metadataObject->exCondition)) {
                $i = 0;
                foreach ($metadataObject->exCondition as $condition) {
                    foreach (json_decode($condition) as $key => $value) {
                        if ($i == 0) {
                            $query .= " where " . $value;
                        } else {
                            $query .= " and " . $value;
                        }
                        $i++;
                    }

                }
            }
        }
        if (isset($metadataObject->exCondition)) {
            $i = 0;
            foreach ($metadataObject->exCondition as $condition) {
                foreach (json_decode($condition) as $key => $value) {
                    if ($i == 0 && !(isset($metadataObject->condition) && $metadataObject->condition)) {
                        $query .= " where " . $value;
                    } else {
                        $query .= " and " . $value;
                    }
                    $i++;
                }

            }
        }*/
        if ((isset($metadataObject->exCondition) && $metadataObject->exCondition) || (isset($metadataObject->condition) && $metadataObject->condition)) {
            $conditionWhere = "and";
        } else {
            $conditionWhere = "or";
        }
        if (!(isset($metadataObject->condition) && $metadataObject->condition) && !(isset($metadataObject->exCondition) && $metadataObject->exCondition) && $incrementObj) {
            $incrementObj = json_decode($incrementObj);
            $i = 0;
            foreach ($incrementObj->table as $table) {
                $tableName = $table->name;
                if (isset($metadataDetailsObj->updated_at->$tableName)) {
                    $dateTime = $metadataDetailsObj->updated_at->$tableName;
                    if ($i == 0 && isset($table->key)) {
                        $query .= " where " . $table->name . "." . $table->key . " > '" . $dateTime . "'";
                        $i++;
                    } else if (isset($table->key)) {
                        $query .= " $conditionWhere " . $table->name . "`.`" . $table->key . " > '" . $dateTime . "'";
                    }
                }
            }
            if (isset($metadataObject->limit) && $metadataObject->limit) {
                $query .= " and  ROWNUM <=  " . $metadataObject->limit;
            }
        } elseif ($incrementObj) {
            $incrementObj = json_decode($incrementObj);
            $i=0;
            foreach ($incrementObj->table as $table) {
                $tableName = $table->name;
                if (isset($metadataDetailsObj->updated_at->$tableName)) {
                    $dateTime = $metadataDetailsObj->updated_at->$tableName;
                    if($i==0){
                        $query .= " where " . $table->name . "." . $table->key . " > '" . $dateTime . "'";
                        $i++;
                    }else{
                        $query .= " $conditionWhere " . $table->name . "." . $table->key . " > '" . $dateTime . "'";
                    }

                }
            }
            if (isset($metadataObject->limit) && $metadataObject->limit) {
                $query .= " and  ROWNUM <=  " . $metadataObject->limit;
            }
        }else{
            if (isset($metadataObject->limit) && $metadataObject->limit) {
                $query .= " where  ROWNUM <=  " . $metadataObject->limit;
            }
        }
        $tablesData = DB::select($query);
        return $tablesData[0]->length;
    }

    public static function nodeToRedis($metadataObj, $redis, $incrementObj,$token,$metadataId,$companyId)
    {
        foreach ($metadataObj as $key => $metadataVal) {
            $key_meta=$key.":".$token;
            $lengthObj = $redis->hgetAll("metadataDetails");
            if(isset($lengthObj[$key_meta])){
                $metadataDetailsObj = json_decode($lengthObj[$key_meta]);
                $length = json_decode($lengthObj[$key_meta])->length;
                $dataLength = self::checkLength($metadataVal, $metadataDetailsObj, $incrementObj);
                if ($incrementObj && $dataLength) {
                    $data = self::getLimitData($metadataVal, $metadataDetailsObj, $dataLength, $incrementObj);
                    $updatedKey = [];
                    $tempArr = [];
                    $incrementObj = json_decode($incrementObj);
                    foreach ($data as $dt) {
                        $selectedKeys = [];
                        foreach ($incrementObj->selected_Key as $keyObj) {
                            $keyObj = json_decode($keyObj);
                            array_push($selectedKeys, $keyObj->Field . "(" . $keyObj->tableName . ")");
                        }
                        $setKey = "";
                        foreach ($selectedKeys as $selectedKey) {
                            $setKey .= $dt->$selectedKey;
                        }
                        array_push($updatedKey, $setKey);
                        $lastData = (array)$dt;
                        foreach ($incrementObj->table as $table) {
                            if (isset($table->key)) {
                                if (!isset($tempArr[$table->name])) {
                                    $tempArr[$table->name] = $lastData[$table->key . "(" . $table->name . ")"];
                                } else if (strtotime($lastData[$table->key . "(" . $table->name . ")"]) > strtotime($tempArr[$table->name])) {
                                    $tempArr[$table->name] = $lastData[$table->key . "(" . $table->name . ")"];
                                }
                            }
                        }
                        $redis->hset("$metadataId:$companyId-realtime", $setKey, json_encode($dt));
                    }
                    $metadataDetailsObj->length = $dataLength;
                    $metadataDetailsObj->updated_at = $tempArr;
                    $metadataDetailsObj->updated_key = $updatedKey;
                    $redis->hset("metadataDetails", $key_meta, json_encode($metadataDetailsObj));
                    $incrementObj = $metadataDetailsObj;
                    $responseArr = array(
                        "incrementObjFlag" => true,
                        "flag" => true,
                        "incrementObj" => $incrementObj
                    );
                    return $responseArr;
                } else if ($dataLength > $length) {
                    $data = self::getLimitData($metadataVal, $metadataDetailsObj, $dataLength, "");
                    $i = 0;
                    foreach ($data as $dt) {
                        $redis->hset($key, $length + $i, json_encode($dt));
                        $i++;
                    }
                    $metadataDetailsObj->length = $dataLength;
                    $redis->hset("metadataDetails", $key_meta, json_encode($metadataDetailsObj));
                    $responseArr = array(
                        "incrementObj" => false,
                        "flag" => true
                    );
                    return $responseArr;
                } else {
                    $metadataDetailsObj->updated_key = [];
                    $redis->hset("metadataDetails", $key_meta, json_encode($metadataDetailsObj));
                }
                $responseArr = array(
                    "incrementObj" => false,
                    "flag" => false
                );
                return $responseArr;
            }
            $responseArr = array(
                "incrementObj" => false,
                "flag" => false
            );
            return $responseArr;
        }
    }

    public function redisAddData($client, $metadata, $incrementObj,$token,$clientId,$realTimeFlag)
    {
        $redis = Redis::connection();
        /*
         * Get All Data
         */
        //$allKeys=$redis->keys("*");
        $metadataObj = [];
        /*
         * Get all metadata
         */
        $metadataObj[$client] = $metadata;
        /*
         * Check length and get data
         */
        foreach ($metadataObj as $key => $metadataVal) {
            $key_meta=$clientId.":".$key.":".$token;
            $lengthObj = $redis->hgetAll("metadataDetails");
            $totalLength=$lengthObj[$key];
            if($realTimeFlag){
                $key=$key."-realtime";
            }
            if (isset($lengthObj[$key_meta])) {
                $metadataDetailsObj = json_decode($lengthObj[$key_meta]);
                $length = json_decode($lengthObj[$key_meta])->length;
                $dataLength = self::checkLength($metadataVal, $metadataDetailsObj, $incrementObj);
                if ($incrementObj && $dataLength) {
                    $data = self::getLimitData($metadataVal, $metadataDetailsObj, $dataLength, $incrementObj);
                    $updatedKey = [];
                    $incrementObj = json_decode($incrementObj);
                    $tempArr = [];
                    $lastData = "";
                    foreach ($data as $dt) {
                        $selectedKeys = [];
                        foreach ($incrementObj->selected_Key as $keyObj) {
                            $keyObj = json_decode($keyObj);
                            array_push($selectedKeys, $keyObj->Field . "(" . $keyObj->tableName . ")");
                        }
                        $setKey = "";
                        foreach ($selectedKeys as $selectedKey) {
                            $setKey .= $dt->$selectedKey;
                        }
                        array_push($updatedKey, $setKey);
                        $lastData = (array)$dt;
                        foreach ($incrementObj->table as $table) {
                            if (isset($table->key)) {
                                if (!isset($tempArr[$table->name])) {
                                    $tempArr[$table->name] = $lastData[$table->key . "(" . $table->name . ")"];
                                } else if (strtotime($lastData[$table->key . "(" . $table->name . ")"]) > strtotime($tempArr[$table->name])) {
                                    $tempArr[$table->name] = $lastData[$table->key . "(" . $table->name . ")"];
                                }
                            }

                        }
                        $redis->hset($key, $setKey, json_encode($dt));
                    }
                    $metadataDetailsObj->length = $dataLength;
                    $metadataDetailsObj->updated_at = $tempArr;
                    $metadataDetailsObj->updated_key = $updatedKey;
                    $redis->hset("metadataDetails", $key_meta, json_encode($metadataDetailsObj));
                } else if ($dataLength > $length) {
                    $data = self::getLimitData($metadataVal, $metadataDetailsObj, $dataLength, "");
                    $i=1;
                    $newKeyCreated=[];
                    foreach ($data as $dt) {
                        $newKey=$totalLength + $i;
                        array_push($newKeyCreated,$newKey);
                        $redis->hset($key,$newKey , json_encode($dt));
                        $i++;
                    }
                    $metadataDetailsObj->length = $dataLength;
                    $metadataDetailsObj->newCreatedKey = $newKeyCreated;
                    $redis->hset("metadataDetails", $key_meta, json_encode($metadataDetailsObj));
                }
            }
        }
    }
    public function getDataDashboard($metadataObject){
        try {
            //***********************Table column Name object*******************
            //For Fetch Column Details
            $oracleVersion = DB::select('SELECT version FROM v$instance');
            if($oracleVersion[0]->version>=12.2){
                $table=$this->getDataLatestVersion($metadataObject,"dashboard");
            }else{
                $table=$this->getDataOldVersion($metadataObject,"dashboard","");
            }
            //Execution time
            $tableDataArray = array();
            $tableData = json_encode($table['tablesData']);
            $this->result = array("tableColumn" => $table['tableColumn'], "tableData" => $tableData);
            $this->errorCode = 1;
            $this->message = 'Successfully';
        } catch (Exception $ex) {
            $this->errorCode = 0;
            $this->message = $ex->getMessage();
            $this->result = "";
        }
        DB::disconnect();
        return $this;

    }
}

?>