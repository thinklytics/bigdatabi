<?php

namespace App\Http\Core\PostGres;

use App\Http\Contract\DatasourceInterface;
use App\Http\Controllers\Controller;
use App\Http\Controllers\DepartmentController;
use App\Http\Core\Data\QueryResult;
use App\Http\Utils\DBHelper;
use App\Http\Utils\HelperFunctions;
use App\Model\Metadata;
use Exception;
use PDO;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redis;
use Illuminate\Database\Capsule\Manager as DB;

class PostGres extends DatasourceInterface
{

    static $con = null;
    static $conObject = null;

    public function connect($connectQuery)
    {
        self::$conObject = $connectQuery;
        self::$con = DBHelper::switchDataBase($connectQuery);
        //$this->con = DB::switchDataBase($connectQuery);
        return $this;
    }

    /*
     * List of data
     */
    public function listDatabase()
    {
        try {
            $query = DB::select("SELECT datname FROM pg_database");
            /*
             *  Object convert to array
             */
            $dataBaseArr=[];
            foreach ($query as $data){
                $tempObj['schema_name']=$data->datname;
                array_push($dataBaseArr,$tempObj);
            }
            $this->result = $dataBaseArr;
            $this->errorCode = 1;
            $this->message = "Connection Successfully";
        } catch (Exception $ex) {
            $this->errorCode = 0;
            $this->message = $ex->getMessage();
        }
        return $this;
    }

    /*
     * Table Indexs
     */
    public function tableIndexsList($tableName)
    {
        try {
            $indexList = DB::select("SHOW INDEXES FROM $tableName");
            $this->result = $indexList;
            $this->errorCode = 1;
            $this->message = 'Successfully';
        } catch (Exception $ex) {
            $this->errorCode = 0;
            $this->message = $ex->getMessage();
        }
        return $this;

    }

    /*
     * Table index delete
     */
    public function tableIndexsListDelete($indexColumn)
    {
        try {
            $indexColumn = (Object)$indexColumn;
            DB::select("DROP INDEX $indexColumn->Key_name ON $indexColumn->Table");
            $this->result = "";
            $this->errorCode = 1;
            $this->message = 'Successfully';
        } catch (Exception $ex) {
            $this->errorCode = 0;
            $this->message = $ex->getMessage();
        }
        return $this;
    }

    /*
     * Table index create
     */
    public function tableIndexsListCreate($tableIndex, $tableName)
    {
        try {

            foreach ($tableIndex as $table) {
                $table = (Object)$table;
                DB::select("CREATE INDEX $table->keyName ON $tableName ($table->column)");
            }
            $this->result = "";
            $this->errorCode = 1;
            $this->message = 'Successfully';
        } catch (Exception $ex) {
            $this->errorCode = 0;
            $this->message = $ex->getMessage();
        }
        return $this;
    }

    /*
     * Table List
     */
    public function listTables()
    {
        try {
            $tables = DB::select("SELECT table_schema,table_name FROM information_schema.tables where table_schema='public'");
            $tableList = [];
            foreach ($tables as $table) {
                array_push($tableList, $table->table_name);
            }
            $this->result = $tableList;
            $this->errorCode = 1;
            $this->message = 'Successfully';
        } catch (Exception $ex) {
            $this->errorCode = 0;
            $this->message = $ex->getMessage();
        }
        return $this;
    }

    /*
     * Query to get data
     */
    public function getData($metadataObject)
    {
        try {
            $rootTable = $metadataObject->rootTable;
            //***********************Table column Name object*******************
            $tempArrry = [];
            $connectionObject = $metadataObject->connectionObject;
            $dbName = $connectionObject->dbname;
            $tempArrry = [];
            //For Fetch Column Details
            $tableColumnQuery = "SELECT table_name as tableName,column_name as Field,data_type as Type FROM information_schema.columns WHERE table_schema = 'public' AND table_name   = '$rootTable'";
            $columnRoot = DB::select($tableColumnQuery);
            foreach ($columnRoot as $ro) {
                //longblob
                /*
                 *
                 * Match with old format
                 */
                $ro->tableName=$ro->tablename;
                $ro->Field=$ro->field;
                $ro->Type=$ro->type;
                unset($ro->tablename);
                unset($ro->field);
                unset($ro->type);
                if ($ro->Type == "integer" || $ro->Type == "biginteger" || $ro->Type == "real" || $ro->Type == "decimal" || $ro->Type == "double" || $ro->Type == "float") {
                    if($ro->Type=="integer"  || $ro->Type == "real"){
                        $ro->Type="int";
                    }
                    $ro->dataType = "Measure";
                } else {
                    if($ro->Type=="character varying"){
                        $ro->Type="varchar";
                    }
                    $ro->dataType = "Dimension";
                }
                array_push($tempArrry, $ro);
            }
            $flag = false;
            if (isset($metadataObject->joinsDetails)) {
                foreach ($metadataObject->joinsDetails as $ros) {
                    $tableColumnQuery = "SELECT table_name as tableName,column_name as Field,data_type as Type FROM information_schema.columns WHERE table_schema = 'public' AND table_name   = '$ros->rightColumnTable'";
                    $tables = DB::select($tableColumnQuery);
                    foreach ($tables as $ro) {
                        /*
                         *
                         * Match with old format
                         */
                        $ro->tableName=$ro->tablename;
                        $ro->Field=$ro->field;
                        $ro->Type=$ro->type;
                        unset($ro->tablename);
                        unset($ro->field);
                        unset($ro->type);
                        if ($ro->Type == "integer" || $ro->Type == "biginteger" || $ro->Type == "real" || $ro->Type == "decimal" || $ro->Type == "double" || $ro->Type == "float") {
                            if($ro->Type=="integer"  || $ro->Type == "real"){
                                $ro->Type="int";
                            }
                            $ro->dataType = "Measure";
                        } else {
                            if($ro->Type=="character varying"){
                                $ro->Type="varchar";
                            }
                            $ro->dataType = "Dimension";
                        }
                        $ro->tableName = $ros->rightColumnTable;
                        array_push($tempArrry, $ro);
                    }
                    $flag = true;
                }
            }

            $tableCol = $tempArrry;//All Column
            //Table column array with key
            $tableColumnArray = array();
            $tableColumnComma = "";
            $duplicateColumn = [];
            $i = 0;
            $tableIndex = 0;
            $blobColumn = [];
            foreach ($tableCol as $col) {
                if ($col->Type == "longblob" || $col->Type == "blob") {
                    array_push($blobColumn, $tableIndex);
                } else if (isset($p[$col->Field])) {
                    array_push($duplicateColumn, $col->Field);
                } else {
                    $p[$col->Field] = true;
                }
                $tableIndex++;
            }
            foreach ($blobColumn as $key => $value) {
                unset($tableCol[$value]);
            }
            foreach ($tableCol as $ro) {
                if (isset($metadataObject->blending)) {
                    $tableColumnComma .= '"' . $ro->tableName . '"."' . $ro->Field . '" as "' . $ro->Field .'_' . $ro->tableName . '",';
                    $ro->Field = $ro->Field . "_" . $ro->tableName;
                } else {
                    $tableColumnComma .= '"' . $ro->tableName . '"."' . $ro->Field . '" as "' . $ro->Field . '(' . $ro->tableName . ')",';
                    $ro->Field = $ro->Field . "(" . $ro->tableName . ")";
                }
                $tableColumnArray[$ro->Field] = $ro;
            }
            $tableColumnObject = json_encode($tableColumnArray);
            //**********************Table Data***********************************
            $tableColumnComma = rtrim($tableColumnComma, ",");
            $query = "select $tableColumnComma from $rootTable";
            if (isset($metadataObject->joinsDetails)) {
                foreach ($metadataObject->joinsDetails as $ros) {
                    $i = 0;
                    $query .= " $ros->join join $ros->rightColumnTable";
                    foreach ($ros->node as $row) {

                        if (is_object(json_decode($row->leftColumn))) {
                            $leftColumn = json_decode($row->leftColumn);
                            $rightColumn = json_decode($row->rightColumn);
                            $joinLeftColumn =  $leftColumn->tableName . "." . $leftColumn->Field;
                            $joinRightColumn = $rightColumn->tableName . "." . $rightColumn->Field;
                        } else {
                            $joinLeftColumn = $ros->leftColumnTable . "." . $row->leftColumn;
                            $joinRightColumn = $ros->leftColumnTable . "." . $row->leftColumn;
                        }
                        //$leftColumnTable = trim(preg_replace('/\s\s+/', ' ', $ros->leftColumnTable));
                        //$rightColumnTable = trim(preg_replace('/\s\s+/', ' ', $ros->rightColumnTable));
                        $leftColumn = trim(preg_replace('/\s\s+/', ' ', $joinLeftColumn));
                        $rightColumn = trim(preg_replace('/\s\s+/', ' ', $joinRightColumn));
                        if ($i == 0) {
                            $p = " on  ";
                        } else {
                            $p = " and ";
                        }
                        $query .= " $p  $leftColumn=$rightColumn";
                        $i++;
                    }
                }
            }
            $start = microtime(true);
            $query = trim(preg_replace('/\s\s+/', ' ', $query));
            if (isset($metadataObject->condition) && $metadataObject->condition) {
                //$return = preg_match("/^[a-zA-Z ]+$/", $string);
                while (preg_match_all('/(\w*)(\(((\w+))\))+/', $metadataObject->condition, $out)) {
                    $str =  $out[3][0] . "." . $out[1][0];
                    $metadataObject->condition = str_replace($out[0][0], $str, $metadataObject->condition);
                }
                $query .= " where " . $metadataObject->condition;
            }
            if (isset($metadataObject->limit) && $metadataObject->limit) {
                if ($metadataObject->limit < 100)
                    $query .= " limit " . $metadataObject->limit;
                else
                    $query .= " limit 100";
            } else {
                $query .= " limit 100";
            }
            $tablesData = DB::select($query);//Table Data
            //Execution time
            $tableDataArray = array();
            $tableData = json_encode($tablesData);
            $this->result = array("tableColumn" => $tableColumnObject, "tableData" => $tableData);
            $this->errorCode = 1;
            $this->message = 'Successfully';
        } catch (Exception $ex) {
            $this->errorCode = 0;
            $this->message = $ex->getMessage();
            $this->result = "";
        }
        DB::disconnect();
        return $this;
    }

    /*
     *
     * Dashboard Data
     */
    public function getDataDashboard($metadataObject)
    {
        try {
            $rootTable = $metadataObject->rootTable;
            //***********************Table column Name object*******************
            $tempArrry = [];
            $connectionObject = $metadataObject->connectionObject;
            $dbName = $connectionObject->dbname;
            $tableColumnQuery = "SELECT TABLE_NAME as ".'"'.'tableName'.'"'.",COLUMN_NAME as ".'"'.'Field'.'"'.",DATA_TYPE as ".'"'.'Type'.'"'." FROM information_schema.columns WHERE table_schema = 'public' AND table_name   = '$rootTable'";
            //$tableColumnQuery = "SELECT TABLE_NAME as tableName,COLUMN_NAME as Field,DATA_TYPE as Type FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = '$rootTable' and TABLE_SCHEMA='$dbName'";
            $tables = DB::select($tableColumnQuery);
            foreach ($tables as $ro) {
                //longblob
                if ($ro->Type == "int" || $ro->Type == "bigint"  || $ro->Type == "decimal" || $ro->Type == "double" || $ro->Type == "float") {
                    $ro->dataType = "Measure";
                } else {
                    $ro->dataType = "Dimension";
                }
                array_push($tempArrry, $ro);
            }
            $flag = false;
            if (isset($metadataObject->joinsDetails)) {
                foreach ($metadataObject->joinsDetails as $ros) {
                    $tableColumnQuery = "SELECT TABLE_NAME as ".'"'.'tableName'.'"'.",COLUMN_NAME as ".'"'.'Field'.'"'.",DATA_TYPE as ".'"'.'Type'.'"'." FROM information_schema.columns WHERE table_schema = 'public' AND table_name   = '$ros->rightColumnTable'";
                    $tables = DB::select($tableColumnQuery);
                    foreach ($tables as $ro) {
                        if ($ro->Type == "int" || $ro->Type == "decimal" || $ro->Type == "double" || $ro->Type == "float") {
                            $ro->dataType = "Measure";
                        } else {
                            $ro->dataType = "Dimension";
                        }
                        $ro->tableName = $ros->rightColumnTable;
                        array_push($tempArrry, $ro);
                    }
                    $flag = true;
                }
            }
            $tableCol = $tempArrry;//All Column
            //Table column array with key
            $tableColumnArray = array();
            $tableColumnComma = "";
            $duplicateColumn = [];
            $i = 0;
            $tableIndex = 0;
            $blobColumn = [];
            foreach ($tableCol as $col) {
                if ($col->Type == "longblob" || $col->Type == "blob") {
                    array_push($blobColumn, $tableIndex);
                } else if (isset($p[$col->Field])) {
                    array_push($duplicateColumn, $col->Field);
                } else {
                    $p[$col->Field] = true;
                }
                $tableIndex++;
            }
            foreach ($blobColumn as $key => $value) {
                unset($tableCol[$value]);
            }
            if ($flag) {
                foreach ($tableCol as $ro) {
                    if (in_array($ro->Field, $duplicateColumn)) {
                        $tableColumnComma .= $ro->tableName . '.' . $ro->Field . ' as "' . $ro->Field . '(' . $ro->tableName . ')",';
                        $ro->Field = $ro->Field . "(" . $ro->tableName . ")";
                    } else {
                        $tableColumnComma .= $ro->tableName . "." . $ro->Field . ",";
                    }
                    $tableColumnArray[$ro->Field] = $ro;

                }
                //Table column with comma for query

                $tableColumnComma = rtrim($tableColumnComma, ",");
            } else {
                $tableColumnComma = "*";
                foreach ($tableCol as $ro) {
                    $tableColumnArray[$ro->Field] = $ro;
                }
            }
            $tableColumnObject = json_encode($tableColumnArray);
            //**********************Table Data***********************************
            $query = "select $tableColumnComma from $rootTable";
            if (isset($metadataObject->joinsDetails)) {
                foreach ($metadataObject->joinsDetails as $ros) {
                    $i = 0;
                    $query .= " $ros->join join `$ros->rightColumnTable`";
                    foreach ($ros->node as $row) {

                        if (is_object(json_decode($row->leftColumn))) {
                            $leftColumn = json_decode($row->leftColumn);
                            $rightColumn = json_decode($row->rightColumn);
                            $joinLeftColumn = $leftColumn->tableName . "." . $leftColumn->Field;
                            $joinRightColumn =  $rightColumn->tableName . "." . $rightColumn->Field;
                        } else {
                            $joinLeftColumn = $ros->leftColumnTable . "." . $row->leftColumn;
                            $joinRightColumn = $ros->leftColumnTable . "." . $row->leftColumn;
                        }
                        //$leftColumnTable = trim(preg_replace('/\s\s+/', ' ', $ros->leftColumnTable));
                        //$rightColumnTable = trim(preg_replace('/\s\s+/', ' ', $ros->rightColumnTable));
                        $leftColumn = trim(preg_replace('/\s\s+/', ' ', $joinLeftColumn));
                        $rightColumn = trim(preg_replace('/\s\s+/', ' ', $joinRightColumn));
                        if ($i == 0) {
                            $p = " on  ";
                        } else {
                            $p = " and ";
                        }
                        $query .= " $p  $leftColumn=$rightColumn";
                        $i++;
                    }
                }
            }
            $start = microtime(true);
            $query = trim(preg_replace('/\s\s+/', ' ', $query));
            if (isset($metadataObject->condition) && $metadataObject->condition) {
                //$return = preg_match("/^[a-zA-Z ]+$/", $string);
                while (preg_match_all('/(\w*)(\(((\w+))\))+/', $metadataObject->condition, $out)) {
                    $str = $out[3][0] . "." . $out[1][0];
                    $metadataObject->condition = str_replace($out[0][0], $str, $metadataObject->condition);
                }
                $query .= " where " . $metadataObject->condition;
                if (isset($metadataObject->exCondition)) {
                    $i = 0;
                    foreach ($metadataObject->exCondition as $condition) {
                        foreach (json_decode($condition) as $key => $value) {
                            if ($i == 0) {
                                $query .= " where " . $value;
                            } else {
                                $query .= " and " . $value;
                            }
                            $i++;
                        }

                    }
                }
            } else {

                if (isset($metadataObject->exCondition)) {
                    $i = 0;
                    foreach ($metadataObject->exCondition as $condition) {
                        foreach (json_decode($condition) as $key => $value) {
                            if ($i == 0) {
                                $query .= " where " . $value;
                            } else {
                                $query .= " and " . $value;
                            }
                            $i++;
                        }

                    }
                }
            }
            //$query .= " limit 1000";
            if (isset($metadataObject->limit) && $metadataObject->limit) {
                $query .= " limit " . $metadataObject->limit;
            }
            $tablesData = DB::select($query);//Table Data
            //Execution time
            $tableDataArray = array();

            $time = microtime(true) - $start;
            //$tables = DB::select($query);
            $tableData = json_encode($tablesData);

            //Execution time
            $tableDataArray = array();
            $tableData = json_encode($tablesData);
            $this->result = array("tableColumn" => $tableColumnObject, "tableData" => $tableData);
            $this->errorCode = 1;
            $this->message = 'Successfully';
        } catch (Exception $ex) {
            $this->errorCode = 0;
            $this->message = $ex->getMessage();
        }
        DB::disconnect();
        return $this;
    }

    /*
     * Get column name
     */
    public function getColumnName($tableName)
    {
        try {
            $tableColumnQuery = "SELECT TABLE_NAME as ".'"'.'tableName'.'"'.",COLUMN_NAME as ".'"'.'Field'.'"'.",DATA_TYPE as ".'"'.'Type'.'"'." FROM information_schema.columns WHERE table_schema = 'public' AND table_name   = '$tableName'";
            $tables = DB::select($tableColumnQuery);
            foreach ($tables as $ro) {
                if ($ro->Type == "integer" || $ro->Type == "real") {
                    if($ro->Type=="integer"  || $ro->Type == "real"){
                        $ro->Type="int";
                    }
                    $ro->dataType = "Measure";
                } else {
                    if($ro->Type=="character varying"){
                        $ro->Type="varchar";
                    }
                    $ro->dataType = "Dimension";
                }
                $ro->tableName = $tableName;
            }
            $this->result = $tables;
            $this->errorCode = 1;
            $this->message = 'Successfully';
        } catch (Exception $ex) {
            $this->errorCode = 0;
            $this->message = $ex->getMessage();
        }
        return $this;
    }

    public function getColumnMultiTable($tablesObj)
    {
        try {
            $tableArr = [];
            foreach ($tablesObj as $table) {
                $tableColumnQuery = "SELECT TABLE_NAME as ".'"'.'tableName'.'"'.",COLUMN_NAME as ".'"'.'Field'.'"'.",DATA_TYPE as ".'"'.'Type'.'"'." FROM information_schema.columns WHERE table_schema = 'public' AND table_name   = '$table'";
                $tables = DB::select($tableColumnQuery);
                foreach ($tables as $ro) {
                    if ($ro->Type == "integer" || $ro->Type == "real" ) {
                        if($ro->Type=="integer"  || $ro->Type == "real"){
                            $ro->Type="int";
                        }
                        $ro->dataType = "Measure";
                    } else {
                        if($ro->Type=="character varying"){
                            $ro->Type="varchar";
                        }
                        $ro->dataType = "Dimension";
                    }
                    $ro->tableName = $table;
                }
                $tableArr[$table] = $tables;
            }
            $this->result = $tableArr;
            $this->errorCode = 1;
            $this->message = 'Successfully';
        } catch (Exception $ex) {
            $this->errorCode = 0;
            $this->message = $ex->getMessage();
        }
        return $this;
    }
    /*
     * ************************************** Server side code ******************************************************
     */
    //  Cache data to redis
    public function queryDataToredis($metadataObject, $incrementObj,$token,$department)
    {
        /*ini_set('memory_limit', '-1');
        set_time_limit(0);*/
        /*
         * Limited Obj
         */
        ini_set('memory_limit', '-1');
        set_time_limit(0);
        $data = array();
        $company_id=Auth::user()->company_id;
        $client_id=Auth::user()->id;
        if($incrementObj && isset($metadataObject->realtime)){
            $client_id=$client_id."-realtime";
            $token=$token."-realtime";
        }
        /*
         * Role level conditions
         */
        $roleLevelCondition="";
        $userType=false;
        if(isset($metadataObject->exCondition)){
            $roleLevelCondition=$metadataObject->exCondition;
            $userType=$metadataObject->userType;
        }
        $realTimeFlag=false;
        if(isset($metadataObject->realtime)){
            $realTimeFlag=true;
        }
        if (!HelperFunctions::isCachedInRedis($client_id, $metadataObject->metadataId,$company_id,$realTimeFlag)) {
            try {
                /*
                 * Check data
                 */

                $rootTable = $metadataObject->rootTable;
                //***********************Table column Name object*******************
                $tempArrry = [];
                $connectionObject = $metadataObject->connectionObject;
                $dbName = $connectionObject->dbname;
                $tableColumnQuery = "SELECT TABLE_NAME as ".'"'.'tableName'.'"'.",COLUMN_NAME as ".'"'.'Field'.'"'.",DATA_TYPE as ".'"'.'Type'.'"'." FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = '$rootTable' and TABLE_SCHEMA='public'";
                $tables = DB::select($tableColumnQuery);
                foreach ($tables as $ro) {
                    //longblob
                    if ($ro->Type == "integer" || $ro->Type == "biginteger" || $ro->Type == "real" || $ro->Type == "decimal" || $ro->Type == "double" || $ro->Type == "float") {
                        if($ro->Type=="integer"  || $ro->Type == "real"){
                            $ro->Type="int";
                        }
                        $ro->dataType = "Measure";
                    } else {
                        if($ro->Type=="character varying"){
                            $ro->Type="varchar";
                        }
                        $ro->dataType = "Dimension";
                    }
                    array_push($tempArrry, $ro);
                }
                $flag = false;
                if (isset($metadataObject->joinsDetails)) {
                    foreach ($metadataObject->joinsDetails as $ros) {
                        $tableColumnQuery = "SELECT TABLE_NAME as ".'"'.'tableName'.'"'.",COLUMN_NAME as ".'"'.'Field'.'"'.",DATA_TYPE as ".'"'.'Type'.'"'." FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = '$ros->rightColumnTable' and TABLE_SCHEMA='public'";
                        $tables = DB::select($tableColumnQuery);
                        foreach ($tables as $ro) {
                            if ($ro->Type == "integer" || $ro->Type == "biginteger" || $ro->Type == "real" || $ro->Type == "decimal" || $ro->Type == "double" || $ro->Type == "float") {
                                if($ro->Type=="integer"  || $ro->Type == "real"){
                                    $ro->Type="int";
                                }
                                $ro->dataType = "Measure";
                            } else {
                                if($ro->Type=="character varying"){
                                    $ro->Type="varchar";
                                }
                                $ro->dataType = "Dimension";
                            }
                            $ro->tableName = $ros->rightColumnTable;
                            array_push($tempArrry, $ro);
                        }
                        $flag = true;
                    }
                }
                $tableCol = $tempArrry;//All Column
                //Table column array with key
                $tableColumnArray = array();
                $tableColumnComma = "";
                $duplicateColumn = [];
                $i = 0;
                $tableIndex = 0;
                $blobColumn = [];
                foreach ($tableCol as $col) {
                    if ($col->Type == "longblob" || $col->Type == "blob") {
                        array_push($blobColumn, $tableIndex);
                    } else if (isset($p[$col->Field])) {
                        array_push($duplicateColumn, $col->Field);
                    } else {
                        $p[$col->Field] = true;
                    }
                    $tableIndex++;
                }
                foreach ($blobColumn as $key => $value) {
                    unset($tableCol[$value]);
                }
                foreach ($tableCol as $ro) {
                    $tableColumnComma .=  '"'.$ro->tableName . '"."' . $ro->Field . '" as "' . $ro->Field . '(' . $ro->tableName . ')",';
                    $ro->Field = $ro->Field . "(" . $ro->tableName . ")";
                    $tableColumnArray[$ro->Field] = $ro;
                }
                $tableColumnComma = rtrim($tableColumnComma, ",");
                $tableColumnObject = json_encode($tableColumnArray);
                //**********************Table Data***********************************
                $querylength="select count(*) as dataLength from $rootTable";
                $query = "select $tableColumnComma from $rootTable";
                if (isset($metadataObject->joinsDetails)) {
                    foreach ($metadataObject->joinsDetails as $ros) {
                        $i = 0;
                        $query .= " $ros->join join $ros->rightColumnTable";
                        $querylength .= " $ros->join join $ros->rightColumnTable";
                        foreach ($ros->node as $row) {

                            if (is_object(json_decode($row->leftColumn))) {
                                $leftColumn = json_decode($row->leftColumn);
                                $rightColumn = json_decode($row->rightColumn);
                                $joinLeftColumn = $leftColumn->tableName . "." . $leftColumn->Field;
                                $joinRightColumn =  $rightColumn->tableName . "." . $rightColumn->Field;
                            } else {
                                $joinLeftColumn = $ros->leftColumnTable . "." . $row->leftColumn;
                                $joinRightColumn = $ros->leftColumnTable . "." . $row->leftColumn;
                            }
                            //$leftColumnTable = trim(preg_replace('/\s\s+/', ' ', $ros->leftColumnTable));
                            //$rightColumnTable = trim(preg_replace('/\s\s+/', ' ', $ros->rightColumnTable));
                            $leftColumn = trim(preg_replace('/\s\s+/', ' ', $joinLeftColumn));
                            $rightColumn = trim(preg_replace('/\s\s+/', ' ', $joinRightColumn));
                            if ($i == 0) {
                                $p = " on  ";
                            } else {
                                $p = " and ";
                            }
                            $query .= " $p  $leftColumn=$rightColumn";
                            $querylength .= " $p  $leftColumn=$rightColumn";
                            $i++;
                        }
                    }
                }
                $start = microtime(true);
                $query = trim(preg_replace('/\s\s+/', ' ', $query));
                $querylength = trim(preg_replace('/\s\s+/', ' ', $querylength));
                $flagWhere=false;
                $roleLevel=false;
                $roleLevellengthQ="";
                if (isset($metadataObject->condition) && $metadataObject->condition) {
                    $flagWhere=true;
                    //$return = preg_match("/^[a-zA-Z ]+$/", $string);
                    while (preg_match_all('/(\w*)(\(((\w+))\))+/', $metadataObject->condition, $out)) {
                        $str =  $out[3][0] . "." . $out[1][0];
                        $metadataObject->condition = str_replace($out[0][0], $str, $metadataObject->condition);
                    }
                    $query .= " where " . $metadataObject->condition;
                    $querylength .= " where " . $metadataObject->condition;
                    if (isset($metadataObject->exCondition)) {
                        foreach ($metadataObject->exCondition as $condition) {
                            foreach (json_decode($condition) as $key => $value) {
                                $value=str_replace(".",'"."',$value);
                                $value=str_replace("=",'"=',$value);
                                $value='"'.$value;
                                $roleLevellengthQ .= $querylength." and " . $value;
                            }
                        }
                    }
                } else if (isset($metadataObject->exCondition)) {
                    $flagWhere=true;
                    foreach ($metadataObject->exCondition as $condition) {
                        foreach (json_decode($condition) as $key => $value) {
                            $value=str_replace(".",'"."',$value);
                            $value=str_replace("=",'"=',$value);
                            $value='"'.$value;
                            if ($i == 0) {
                                $roleLevellengthQ .= $querylength." where " . $value;
                            } else {
                                $roleLevellengthQ .= $querylength." and " . $value;
                            }
                            $i++;
                        }
                    }
                }
                $maxDateQuery="";
                if($incrementObj && isset($metadataObject->realtime)){
                    $realTimeObj=$metadataObject->realtime;
                    if(isset($realTimeObj->column)){
                        $column=json_decode($realTimeObj->column);
                        $colName=$column->columnName;
                        //$columnName=explode("(",$column->columnName)[0];
                        $maxDateQuery = " order by " . $colName." DESC limit 1";
                    }
                }else{
                    if (isset($metadataObject->limit) && $metadataObject->limit) {
                        $querylength .= " limit " . $metadataObject->limit;
                    }
                }
                /*
                 *  Check total length
                 */
                $tablesLengthData = DB::select($querylength);
                if($roleLevel){
                    $roleLevellength=DB::select($roleLevellengthQ)[0]->datalength;
                }
                $dataLength=$tablesLengthData[0]->datalength;
                $lastLimit=0;
                $limit=env('Cache_Limit');
                /*
                 * Check set limit and get data limit
                 */
                if($limit>$dataLength){
                    $limit=$dataLength;
                }
                if(isset($metadataObject->limit) && $metadataObject->limit && $limit>$metadataObject->limit){
                    $limit=$metadataObject->limit;
                }
                $rs=1;
                if(isset($metadataObject->limit) && $dataLength > $metadataObject->limit && $metadataObject->limit){
                    $dataLength=$metadataObject->limit;
                }
                for($i=0;$i<$dataLength;$i=$i+$limit){
                    if($realTimeFlag==false){
                        $queryNew =$query." limit ".$limit." offset ".$i;
                        $lastLimit=$i+1;
                        $tablesData = DB::select($queryNew);//Table Data
                    }else{
                        $tablesData = DB::select($query." ".$maxDateQuery);//Table Data
                    }
                    if($incrementObj && isset($metadataObject->realtime) && $maxDateQuery!=""){
                        if(count($tablesData)){
                            $maxDate=$tablesData[0]->$colName;
                            $maxDate=date('Y-m-d 23:59:59',strtotime($maxDate));
                            //dd($realTimeObj->dataShow);
                            $start_date=date('Y-m-d 00:00:00', strtotime($maxDate. " - $realTimeObj->dataShow days"));
                            $end_date=$maxDate;
                            $columnName=explode("(",$column->columnName)[0];
                            if(!$flagWhere){
                                $query .= " where ".$column->tableName.".".$columnName." between '".$start_date."' and '".$end_date."'";
                            }else{
                                $query .= " and ".$column->tableName.".".$columnName." between '".$start_date."' and '".$end_date."'";
                            }
                            $tablesData = DB::select($query);
                        }else{
                            $tablesData = DB::select($query);
                        }

                    }

                    //Execution time
                    $tableDataArray = array();
                    $time = microtime(true) - $start;
                    $tableDataArray = array();
                    $tableData  = json_encode($tablesData);
                    $tableColumnObject = json_decode($tableColumnObject);
                    if(isset($metadataObject->column))
                        foreach ($metadataObject->column as $column) {
                            if(isset($column->columnName))
                                $columnName = $column->columnName;
                            if (isset($tableColumnObject->$columnName) && isset($column->dataKey)) {
                                $tableColumnObject->$columnName->dataType = $column->dataKey;
                            }
                        }
                    $tableColumnObject = json_encode($tableColumnObject);
                    $this->result = array("tableColumn" => $tableColumnObject, "tableData" => $tableData);
                    $this->errorCode = 1;
                    $this->message = 'Successfully';
                    $tempArr=[];
                    $tempArr['flag']=1;
                    $tempArr['ukey']=$rs;

                    $tempArr['dataLength']=$dataLength;
                    if($roleLevel){
                        $tempArr['roleLevelLength']=$roleLevellength;
                    }
                    $data = HelperFunctions::writeToRedis($client_id, $metadataObject->metadataId, $this->result, $incrementObj,$company_id,$token,$tempArr,$roleLevelCondition,$department,$realTimeFlag,$userType);
                    $rs++;
                }
            } catch (Exception $ex) {
                dd($ex->getMessage());
                $this->errorCode = 0;
                $this->message = $ex->getMessage();
            }
        } else {
            $data = HelperFunctions::writeToRedis($client_id, $metadataObject->metadataId, $this, $incrementObj,$company_id,$token,0,$roleLevelCondition,$department,$realTimeFlag,$userType);
            $this->redisAddData($metadataObject->metadataId.":".$company_id, $metadataObject, $incrementObj,$token,$client_id,$realTimeFlag);
        }
        DB::disconnect();
        return $data;

    }

    public function getDataLength($metadataObject)
    {
        try {
            $rootTable = $metadataObject->rootTable;
            //***********************Table column Name object*******************
            $tempArrry = [];
            $connectionObject = $metadataObject->connectionObject;
            $dbName = $connectionObject->dbname;
            $flag = false;
            //Table column array with key
            //**********************Table Data***********************************
            $query = "select count(*) as length from $rootTable";
            if (isset($metadataObject->joinsDetails)) {
                foreach ($metadataObject->joinsDetails as $ros) {
                    $i = 0;
                    $query .= " $ros->join join $ros->rightColumnTable";
                    foreach ($ros->node as $row) {

                        if (is_object(json_decode($row->leftColumn))) {
                            $leftColumn = json_decode($row->leftColumn);
                            $rightColumn = json_decode($row->rightColumn);
                            $joinLeftColumn =  $leftColumn->tableName . "." . $leftColumn->Field;
                            $joinRightColumn =$rightColumn->tableName . "." . $rightColumn->Field;
                        } else {
                            $joinLeftColumn =  $ros->leftColumnTable . "." . $row->leftColumn;
                            $joinRightColumn =  $ros->leftColumnTable . "." . $row->leftColumn;
                        }
                        //$leftColumnTable = trim(preg_replace('/\s\s+/', ' ', $ros->leftColumnTable));
                        //$rightColumnTable = trim(preg_replace('/\s\s+/', ' ', $ros->rightColumnTable));
                        $leftColumn = trim(preg_replace('/\s\s+/', ' ', $joinLeftColumn));
                        $rightColumn = trim(preg_replace('/\s\s+/', ' ', $joinRightColumn));
                        if ($i == 0) {
                            $p = " on  ";
                        } else {
                            $p = " and ";
                        }
                        $query .= " $p  $leftColumn=$rightColumn";
                        $i++;
                    }
                }
            }
            $start = microtime(true);
            $query = trim(preg_replace('/\s\s+/', ' ', $query));
            if (isset($metadataObject->condition) && $metadataObject->condition) {
                //$return = preg_match("/^[a-zA-Z ]+$/", $string);
                while (preg_match_all('/(\w*)(\(((\w+))\))+/', $metadataObject->condition, $out)) {
                    $str =  $out[3][0] . "." . $out[1][0] ;
                    $metadataObject->condition = str_replace($out[0][0], $str, $metadataObject->condition);
                }
                $query .= " where " . $metadataObject->condition;
                if (isset($metadataObject->exCondition)) {
                    $i = 0;
                    foreach ($metadataObject->exCondition as $condition) {
                        foreach (json_decode($condition) as $key => $value) {
                            $value=str_replace(".",'"."',$value);
                            $value=str_replace("=",'"=',$value);
                            $value='"'.$value;
                            if ($i == 0) {
                                $query .= " where " . $value;
                            } else {
                                $query .= " and " . $value;
                            }
                            $i++;
                        }

                    }
                }
            } else {

                if (isset($metadataObject->exCondition)) {
                    $i = 0;
                    foreach ($metadataObject->exCondition as $condition) {
                        foreach (json_decode($condition) as $key => $value) {
                            $value=str_replace(".",'"."',$value);
                            $value=str_replace("=",'"=',$value);
                            $value='"'.$value;
                            if ($i == 0) {
                                $query .= " where " . $value;
                            } else {
                                $query .= " and " . $value;
                            }
                            $i++;
                        }

                    }
                }
            }
            //$query .= " limit 1000";
            if (isset($metadataObject->limit) && $metadataObject->limit) {
                $query .= " limit " . $metadataObject->limit;
            }
            $tablesData = DB::select($query);//Table Data
            $tablesCount = $tablesData[0]->length;
            $this->result = $tablesCount;
            $this->errorCode = 1;
            $this->message = 'Successfully';
        } catch (Exception $ex) {

            $this->errorCode = 0;
            $this->message = $ex->getMessage();
        }
        return $this;
    }

    //  Cache data to redis
    public function queryDataToredisPublic($metadataObject,$incrementObj, $uid,$token)
    {
        ini_set('memory_limit', '-1');
        set_time_limit(0);
        $data = array();
        if (!HelperFunctions::isCachedInRedis(0, $metadataObject->metadataId, $uid,false)) {
            /*
             * Get data form query
             */
            try {
                $rootTable = $metadataObject->rootTable;
                //***********************Table column Name object*******************
                $tempArrry = [];
                $connectionObject = $metadataObject->connectionObject;
                $dbName = $connectionObject->dbname;
                $tableColumnQuery = "SELECT TABLE_NAME as ".'"'.'tableName'.'"'.",COLUMN_NAME as ".'"'.'Field'.'"'.",DATA_TYPE as ".'"'.'Type'.'"'." FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = '$rootTable' and TABLE_SCHEMA='public'";
                $tables = DB::select($tableColumnQuery);
                foreach ($tables as $ro) {
                    //longblob
                    if ($ro->Type == "integer" || $ro->Type == "biginteger" || $ro->Type == "decimal" || $ro->Type == "double" || $ro->Type == "float") {
                        if($ro->Type=="integer"  || $ro->Type == "real"){
                            $ro->Type="int";
                        }
                        $ro->dataType = "Measure";
                    } else {
                        if($ro->Type=="character varying"){
                            $ro->Type="varchar";
                        }
                        $ro->dataType = "Dimension";
                    }
                    array_push($tempArrry, $ro);
                }
                $flag = false;
                if (isset($metadataObject->joinsDetails)) {
                    foreach ($metadataObject->joinsDetails as $ros) {
                        $tableColumnQuery = "SELECT TABLE_NAME as ".'"'.'tableName'.'"'.",COLUMN_NAME as ".'"'.'Field'.'"'.",DATA_TYPE as ".'"'.'Type'.'"'." FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = '$ros->rightColumnTable' and TABLE_SCHEMA='public'";
                        $tables = DB::select($tableColumnQuery);
                        foreach ($tables as $ro) {
                            if ($ro->Type == "integer" || $ro->Type == "biginteger" || $ro->Type == "real" || $ro->Type == "decimal" || $ro->Type == "double" || $ro->Type == "float") {
                                if($ro->Type=="integer"  || $ro->Type == "real"){
                                    $ro->Type="int";
                                }
                                $ro->dataType = "Measure";
                            } else {
                                if($ro->Type=="character varying"){
                                    $ro->Type="varchar";
                                }
                                $ro->dataType = "Dimension";
                            }
                            $ro->tableName = $ros->rightColumnTable;
                            array_push($tempArrry, $ro);
                        }
                        $flag = true;
                    }
                }
                $tableCol = $tempArrry;//All Column
                //Table column array with key
                $tableColumnArray = array();
                $tableColumnComma = "";
                $duplicateColumn = [];
                $i = 0;
                $tableIndex = 0;
                $blobColumn = [];
                foreach ($tableCol as $col) {
                    if ($col->Type == "longblob" || $col->Type == "blob") {
                        array_push($blobColumn, $tableIndex);
                    } else if (isset($p[$col->Field])) {
                        array_push($duplicateColumn, $col->Field);
                    } else {
                        $p[$col->Field] = true;
                    }
                    $tableIndex++;
                }
                foreach ($blobColumn as $key => $value) {
                    unset($tableCol[$value]);
                }
                foreach ($tableCol as $ro) {
                    $tableColumnComma .= $ro->tableName . '.' . $ro->Field . ' as "' . $ro->Field . '('. $ro->tableName . ')",';
                    $ro->Field = $ro->Field . "(" . $ro->tableName . ")";
                    $tableColumnArray[$ro->Field] = $ro;
                }
                $tableColumnComma = rtrim($tableColumnComma, ",");
                $tableColumnObject = json_encode($tableColumnArray);
                //**********************Table Data***********************************
                $query = "select $tableColumnComma from $rootTable";
                if (isset($metadataObject->joinsDetails)) {
                    foreach ($metadataObject->joinsDetails as $ros) {
                        $i = 0;
                        $query .= " $ros->join join $ros->rightColumnTable";
                        foreach ($ros->node as $row) {

                            if (is_object(json_decode($row->leftColumn))) {
                                $leftColumn = json_decode($row->leftColumn);
                                $rightColumn = json_decode($row->rightColumn);
                                $joinLeftColumn = $leftColumn->tableName . "." . $leftColumn->Field ;
                                $joinRightColumn =  $rightColumn->tableName . "." . $rightColumn->Field ;
                            } else {
                                $joinLeftColumn =  $ros->leftColumnTable . "." . $row->leftColumn ;
                                $joinRightColumn =  $ros->leftColumnTable . "." . $row->leftColumn ;
                            }
                            //$leftColumnTable = trim(preg_replace('/\s\s+/', ' ', $ros->leftColumnTable));
                            //$rightColumnTable = trim(preg_replace('/\s\s+/', ' ', $ros->rightColumnTable));
                            $leftColumn = trim(preg_replace('/\s\s+/', ' ', $joinLeftColumn));
                            $rightColumn = trim(preg_replace('/\s\s+/', ' ', $joinRightColumn));
                            if ($i == 0) {
                                $p = " on  ";
                            } else {
                                $p = " and ";
                            }
                            $query .= " $p  $leftColumn=$rightColumn";
                            $i++;
                        }
                    }
                }
                $start = microtime(true);
                $query = trim(preg_replace('/\s\s+/', ' ', $query));
                if (isset($metadataObject->condition) && $metadataObject->condition) {
                    //$return = preg_match("/^[a-zA-Z ]+$/", $string);
                    while (preg_match_all('/(\w*)(\(((\w+))\))+/', $metadataObject->condition, $out)) {
                        $str =  $out[3][0] . "." . $out[1][0];
                        $metadataObject->condition = str_replace($out[0][0], $str, $metadataObject->condition);
                    }
                    $query .= " where " . $metadataObject->condition;
                    if (isset($metadataObject->exCondition)) {
                        $i = 0;
                        foreach ($metadataObject->exCondition as $condition) {
                            foreach (json_decode($condition) as $key => $value) {
                                if ($i == 0) {
                                    $query .= " where " . $value;
                                } else {
                                    $query .= " and " . $value;
                                }
                                $i++;
                            }

                        }
                    }
                } else if (isset($metadataObject->exCondition)) {
                    $i = 0;
                    foreach ($metadataObject->exCondition as $condition) {
                        foreach (json_decode($condition) as $key => $value) {
                            if ($i == 0) {
                                $query .= " where " . $value;
                            } else {
                                $query .= " and " . $value;
                            }
                            $i++;
                        }
                    }
                }
                if (isset($metadataObject->limit) && $metadataObject->limit) {
                    $query .= " limit " . $metadataObject->limit;
                }
                $tablesData = DB::select($query);//Table Data
                //Execution time
                $tableDataArray = array();

                $time = microtime(true) - $start;
                //$tables = DB::select($query);
                $tableData = json_encode($tablesData);

                //Execution time
                $tableDataArray = array();
                $tableData = json_encode($tablesData);
                $this->result = array("tableColumn" => $tableColumnObject, "tableData" => $tableData);
                $this->errorCode = 1;
                $this->message = 'Successfully';
                $data = HelperFunctions::writeToRedis(0, $metadataObject->metadataId, $this->result, $incrementObj, $uid,$token,1,"","",false,false);
            } catch (Exception $ex) {
                $this->errorCode = 0;
                $this->message = $ex->getMessage();
            }
        } else {
            $data = HelperFunctions::writeToRedis(0, $metadataObject->metadataId, $this, $incrementObj, $uid,$token,0,"","",false,false);
            $this->redisAddData($metadataObject->metadataId.":".$uid, $metadataObject, $incrementObj,$token,0,false);
        }
        return $data;
    }

    public static function getLimitData($metadataObject, $metadataObj, $currentLength, $incrementObj)
    {
        /*
         * Connection
         */
        $lastLength = $metadataObj->length;
        $lastUpdatedTime = $metadataObj->updated_at;
        (new Postgres())->connect($metadataObject->connectionObject);
        /*
         * Query to get data
         */
        $rootTable = $metadataObject->rootTable;
        //***********************Table column Name object*******************
        $tempArrry = [];
        $connectionObject = $metadataObject->connectionObject;
        $dbName = $connectionObject->dbname;
        $tableColumnQuery = "SELECT TABLE_NAME as ".'"'.'tableName'.'"'.",COLUMN_NAME as ".'"'.'Field'.'"'.",DATA_TYPE as ".'"'.'Type'.'"'." FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = '$rootTable' and TABLE_SCHEMA='public'";
        $tables = DB::select($tableColumnQuery);
        foreach ($tables as $ro) {
            //longblob
            if ($ro->Type == "integer" || $ro->Type == "biginteger" || $ro->Type == "decimal" || $ro->Type == "double" || $ro->Type == "float") {
                if($ro->Type=="integer"  || $ro->Type == "real"){
                    $ro->Type="int";
                }
                $ro->dataType = "Measure";
            } else {
                if($ro->Type=="character varying"){
                    $ro->Type="varchar";
                }
                $ro->dataType = "Dimension";
            }
            array_push($tempArrry, $ro);
        }
        $flag = false;
        if (isset($metadataObject->joinsDetails)) {
            foreach ($metadataObject->joinsDetails as $ros) {
                $tableColumnQuery = "SELECT TABLE_NAME as ".'"'.'tableName'.'"'.",COLUMN_NAME as ".'"'.'Field'.'"'.",DATA_TYPE as ".'"'.'Type'.'"'." FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = '$ros->rightColumnTable' and TABLE_SCHEMA='public'";
                $tables = DB::select($tableColumnQuery);
                foreach ($tables as $ro) {
                    if ($ro->Type == "integer" || $ro->Type == "biginteger" || $ro->Type == "real" || $ro->Type == "decimal" || $ro->Type == "double" || $ro->Type == "float") {
                        if($ro->Type=="integer"  || $ro->Type == "real"){
                            $ro->Type="int";
                        }
                        $ro->dataType = "Measure";
                    } else {
                        if($ro->Type=="character varying"){
                            $ro->Type="varchar";
                        }
                        $ro->dataType = "Dimension";
                    }
                    $ro->tableName = $ros->rightColumnTable;
                    array_push($tempArrry, $ro);
                }
                $flag = true;
            }
        }
        $tableCol = $tempArrry;//All Column
        //Table column array with key
        $tableColumnArray = array();
        $tableColumnComma = "";
        $duplicateColumn = [];
        $i = 0;
        $tableIndex = 0;
        $blobColumn = [];
        foreach ($tableCol as $col) {
            if ($col->Type == "longblob" || $col->Type == "blob") {
                array_push($blobColumn, $tableIndex);
            } else if (isset($p[$col->Field])) {
                array_push($duplicateColumn, $col->Field);
            } else {
                $p[$col->Field] = true;
            }
            $tableIndex++;
        }
        foreach ($blobColumn as $key => $value) {
            unset($tableCol[$value]);
        }

        foreach ($tableCol as $ro) {
            $tableColumnComma .=  '"'.$ro->tableName . '"."' . $ro->Field . '" as "' . $ro->Field . '(' . $ro->tableName . ')",';
            $ro->Field = $ro->Field . "(" . $ro->tableName . ")";
            $tableColumnArray[$ro->Field] = $ro;
        }
        $tableColumnComma = rtrim($tableColumnComma, ",");
        $tableColumnObject = json_encode($tableColumnArray);
        //**********************Table Data***********************************
        $query = "select $tableColumnComma from $rootTable";
        if (isset($metadataObject->joinsDetails)) {
            foreach ($metadataObject->joinsDetails as $ros) {
                $i = 0;
                $query .= " $ros->join join $ros->rightColumnTable";
                foreach ($ros->node as $row) {
                    if (is_object(json_decode($row->leftColumn))) {
                        $leftColumn = json_decode($row->leftColumn);
                        $rightColumn = json_decode($row->rightColumn);
                        $joinLeftColumn =  $leftColumn->tableName . "." . $leftColumn->Field;
                        $joinRightColumn = $rightColumn->tableName . "." . $rightColumn->Field;
                    } else {
                        $joinLeftColumn =  $ros->leftColumnTable . "." . $row->leftColumn;
                        $joinRightColumn =  $ros->leftColumnTable . "." . $row->leftColumn;
                    }
                    //$leftColumnTable = trim(preg_replace('/\s\s+/', ' ', $ros->leftColumnTable));
                    //$rightColumnTable = trim(preg_replace('/\s\s+/', ' ', $ros->rightColumnTable));
                    $leftColumn = trim(preg_replace('/\s\s+/', ' ', $joinLeftColumn));
                    $rightColumn = trim(preg_replace('/\s\s+/', ' ', $joinRightColumn));
                    if ($i == 0) {
                        $p = " on  ";
                    } else {
                        $p = " and ";
                    }
                    $query .= " $p  $leftColumn=$rightColumn";
                    $i++;
                }
            }
        }
        $start = microtime(true);
        $query = trim(preg_replace('/\s\s+/', ' ', $query));
        if (isset($metadataObject->condition) && $metadataObject->condition) {
            //$return = preg_match("/^[a-zA-Z ]+$/", $string);
            while (preg_match_all('/(\w*)(\(((\w+))\))+/', $metadataObject->condition, $out)) {
                $str = "`" . $out[3][0] . "`.`" . $out[1][0] . "`";
                $metadataObject->condition = str_replace($out[0][0], $str, $metadataObject->condition);
            }
            $query .= " where " . $metadataObject->condition;
            if (isset($metadataObject->exCondition)) {
                $i = 0;
                foreach ($metadataObject->exCondition as $condition) {
                    foreach (json_decode($condition) as $key => $value) {
                        $value=str_replace(".",'"."',$value);
                        $value=str_replace("=",'"=',$value);
                        $value='"'.$value;
                        if ($i == 0) {
                            $query .= " where " . $value;
                        } else {
                            $query .= " and " . $value;
                        }
                        $i++;
                    }

                }
            }
        }
        if (isset($metadataObject->exCondition)) {
            $i = 0;
            foreach ($metadataObject->exCondition as $condition) {
                foreach (json_decode($condition) as $key => $value) {
                    $value=str_replace(".",'"."',$value);
                    $value=str_replace("=",'"=',$value);
                    $value='"'.$value;
                    if ($i == 0 && !(isset($metadataObject->condition) && $metadataObject->condition)) {
                        $query .= " where " . $value;
                    } else {
                        $query .= " and " . $value;
                    }
                    $i++;
                }
            }
        }
        if ((isset($metadataObject->exCondition) && $metadataObject->exCondition) || (isset($metadataObject->condition) && $metadataObject->condition)) {
            $conditionWhere = "and";
        } else {
            $conditionWhere = "or";
        }
        /*
         * If metadata limit apply then incremental and realtime not work
         */
        if (isset($metadataObject->limit) && $metadataObject->limit) {
            return [];
        }
        if (!(isset($metadataObject->condition) && $metadataObject->condition) && !(isset($metadataObject->exCondition) && $metadataObject->exCondition) && $incrementObj) {
            $incrementObj = json_decode($incrementObj);
            $i = 0;
            foreach ($incrementObj->table as $table) {
                $tableName = $table->name;
                if (isset($metadataObj->updated_at->$tableName)) {
                    $dateTime = $metadataObj->updated_at->$tableName;
                    if ($i == 0 && isset($table->key)) {
                        $query .= " where " . $table->name . "." . $table->key . " > '" . $dateTime . "'";
                        $i++;
                    } else if (isset($table->key)) {
                        $query .= " $conditionWhere " . $table->name . "." . $table->key . " > '" . $dateTime . "'";
                    }
                }
            }
        } elseif ($incrementObj) {
            $incrementObj = json_decode($incrementObj);
            foreach ($incrementObj->table as $table) {
                $tableName = $table->name;
                if (isset($metadataObj->updated_at->$tableName)) {
                    $dateTime = $metadataObj->updated_at->$tableName;
                    $query .= " $conditionWhere " . $table->name . "." . $table->key . " > '" . $dateTime . "'";
                }
            }
        } else {
            if (isset($metadataObject->limit) && $metadataObject->limit) {
                $query .= " limit " . $metadataObject->limit;
            } else {
                $query .= " limit " . $lastLength . " OFFSET " . $currentLength;
            }
        }
        $tablesData = DB::select($query);//Table Data
        return $tablesData;
    }

    public static function checkLength($metadataObject, $metadataDetailsObj, $incrementObj)
    {
        /*
         * Check length
         */
        $limit = $metadataDetailsObj->length;
        if (isset($metadataObject->type) && $metadataObject->type == "blending") {
            $connection = [];
            $connection['datasourceType'] = env('MDB_CONNECTION', 'mongodb');
            $connection['dbname'] = env('MDB_DATABASE', 'commondb');
            $connection['host'] = env('MDB_HOST', '101.53.130.66');
            $connection['port'] = env('MDB_PORT', '27017');
            $connection['username'] = env('MDB_USERNAME', 'admin');
            $connection['password'] = env('MDB_PASSWORD', 'hellothinklayer1');
            $metadataObject->connectionObject = (Object)$connection;
        }
        $db = (new PostGres())->connect($metadataObject->connectionObject);
        /*
         * Query to get data
         */
        $rootTable = $metadataObject->rootTable;
        $query = "select count(*) as length from $rootTable";
        if (isset($metadataObject->joinsDetails)) {
            foreach ($metadataObject->joinsDetails as $ros) {
                $i = 0;
                $query .= " $ros->join join $ros->rightColumnTable";
                foreach ($ros->node as $row) {
                    if (is_object(json_decode($row->leftColumn))) {
                        $leftColumn = json_decode($row->leftColumn);
                        $rightColumn = json_decode($row->rightColumn);
                        $joinLeftColumn = $leftColumn->tableName . "." . $leftColumn->Field;
                        $joinRightColumn =  $rightColumn->tableName . "." . $rightColumn->Field;
                    } else {
                        $joinLeftColumn =  $ros->leftColumnTable . "." . $row->leftColumn ;
                        $joinRightColumn = $ros->leftColumnTable . "." . $row->leftColumn ;
                    }
                    //$leftColumnTable = trim(preg_replace('/\s\s+/', ' ', $ros->leftColumnTable));
                    //$rightColumnTable = trim(preg_replace('/\s\s+/', ' ', $ros->rightColumnTable));
                    $leftColumn = trim(preg_replace('/\s\s+/', ' ', $joinLeftColumn));
                    $rightColumn = trim(preg_replace('/\s\s+/', ' ', $joinRightColumn));
                    if ($i == 0) {
                        $p = " on  ";
                    } else {
                        $p = " and ";
                    }
                    $query .= " $p  $leftColumn=$rightColumn";
                    $i++;
                }
            }
        }
        $start = microtime(true);
        $query = trim(preg_replace('/\s\s+/', ' ', $query));
        if (isset($metadataObject->condition) && $metadataObject->condition) {
            //$return = preg_match("/^[a-zA-Z ]+$/", $string);
            while (preg_match_all('/(\w*)(\(((\w+))\))+/', $metadataObject->condition, $out)) {
                $str =  $out[3][0] . "." . $out[1][0] ;
                $metadataObject->condition = str_replace($out[0][0], $str, $metadataObject->condition);
            }
            $query .= " where " . $metadataObject->condition;
            if (isset($metadataObject->exCondition)) {
                $i = 0;
                foreach ($metadataObject->exCondition as $condition) {
                    foreach (json_decode($condition) as $key => $value) {
                        $value=str_replace(".",'"."',$value);
                        $value=str_replace("=",'"=',$value);
                        $value='"'.$value;
                        if ($i == 0) {
                            $query .= " where " . $value;
                        } else {
                            $query .= " and " . $value;
                        }
                        $i++;
                    }

                }
            }
        }
        if (isset($metadataObject->exCondition)) {
            $i = 0;
            foreach ($metadataObject->exCondition as $condition) {
                foreach (json_decode($condition) as $key => $value) {
                    $value=str_replace(".",'"."',$value);
                    $value=str_replace("=",'"=',$value);
                    $value='"'.$value;
                    if ($i == 0 && !(isset($metadataObject->condition) && $metadataObject->condition)) {
                        $query .= " where " . $value;
                    } else {
                        $query .= " and " . $value;
                    }
                    $i++;
                }

            }
        }
        if ((isset($metadataObject->exCondition) && $metadataObject->exCondition) || (isset($metadataObject->condition) && $metadataObject->condition)) {
            $conditionWhere = "and";
        } else {
            $conditionWhere = "or";
        }
        if (!(isset($metadataObject->condition) && $metadataObject->condition) && !(isset($metadataObject->exCondition) && $metadataObject->exCondition) && $incrementObj) {
            $incrementObj = json_decode($incrementObj);
            $i = 0;
            foreach ($incrementObj->table as $table) {
                $tableName = $table->name;
                if (isset($metadataDetailsObj->updated_at->$tableName)) {
                    $dateTime = $metadataDetailsObj->updated_at->$tableName;
                    if ($i == 0 && isset($table->key)) {
                        $query .= " where " . $table->name . "." . $table->key . " > '" . $dateTime . "'";
                        $i++;
                    } else if (isset($table->key)) {
                        $query .= " $conditionWhere " . $table->name . "." . $table->key . ">" . $dateTime . "'";
                    }
                }
            }
        } elseif ($incrementObj) {
            $incrementObj = json_decode($incrementObj);
            foreach ($incrementObj->table as $table) {
                $tableName = $table->name;
                if (isset($metadataDetailsObj->updated_at->$tableName)) {
                    $dateTime = $metadataDetailsObj->updated_at->$tableName;
                    $query .= " $conditionWhere " . $table->name . "." . $table->key . " > '" . $dateTime . "'";
                }
            }
        } else {
            if (isset($metadataObject->limit) && $metadataObject->limit) {
                $query .= " limit " . $metadataObject->limit;
            }
        }
        $tablesData = DB::select($query);
        return $tablesData[0]->length;
    }

    public static function nodeToRedis($metadataObj, $redis, $incrementObj,$token,$metadataId,$companyId)
    {
        foreach ($metadataObj as $key => $metadataVal) {
            $key_meta=$key.":".$token;
            $lengthObj = $redis->hgetAll("metadataDetails");
            if(isset($lengthObj[$key_meta])){
                $metadataDetailsObj = json_decode($lengthObj[$key_meta]);
                $length = json_decode($lengthObj[$key_meta])->length;
                $dataLength = self::checkLength($metadataVal, $metadataDetailsObj, $incrementObj);
                if ($incrementObj && $dataLength) {
                    $data = self::getLimitData($metadataVal, $metadataDetailsObj, $dataLength, $incrementObj);
                    $updatedKey = [];
                    $tempArr = [];
                    $incrementObj = json_decode($incrementObj);
                    foreach ($data as $dt) {
                        $selectedKeys = [];
                        foreach ($incrementObj->selected_Key as $keyObj) {
                            $keyObj = json_decode($keyObj);
                            array_push($selectedKeys, $keyObj->Field . "(" . $keyObj->tableName . ")");
                        }
                        $setKey = "";
                        foreach ($selectedKeys as $selectedKey) {
                            $setKey .= $dt->$selectedKey;
                        }
                        array_push($updatedKey, $setKey);
                        $lastData = (array)$dt;
                        foreach ($incrementObj->table as $table) {
                            if (isset($table->key)) {
                                if (!isset($tempArr[$table->name])) {
                                    $tempArr[$table->name] = $lastData[$table->key . "(" . $table->name . ")"];
                                } else if (strtotime($lastData[$table->key . "(" . $table->name . ")"]) > strtotime($tempArr[$table->name])) {
                                    $tempArr[$table->name] = $lastData[$table->key . "(" . $table->name . ")"];
                                }
                            }
                        }
                        $redis->hset("$metadataId:$companyId-realtime", $setKey, json_encode($dt));
                    }
                    $metadataDetailsObj->length = $dataLength;
                    $metadataDetailsObj->updated_at = $tempArr;
                    $metadataDetailsObj->updated_key = $updatedKey;
                    $redis->hset("metadataDetails", $key_meta, json_encode($metadataDetailsObj));
                    $incrementObj = $metadataDetailsObj;
                    $responseArr = array(
                        "incrementObjFlag" => true,
                        "flag" => true,
                        "incrementObj" => $incrementObj
                    );
                    return $responseArr;
                } else if ($dataLength > $length) {
                    $data = self::getLimitData($metadataVal, $metadataDetailsObj, $dataLength, "");
                    $i = 0;
                    foreach ($data as $dt) {
                        $redis->hset($key, $length + $i, json_encode($dt));
                        $i++;
                    }
                    $metadataDetailsObj->length = $dataLength;
                    $redis->hset("metadataDetails", $key_meta, json_encode($metadataDetailsObj));
                    $responseArr = array(
                        "incrementObj" => false,
                        "flag" => true
                    );
                    return $responseArr;
                } else {
                    $metadataDetailsObj->updated_key = [];
                    $redis->hset("metadataDetails", $key_meta, json_encode($metadataDetailsObj));
                }
                $responseArr = array(
                    "incrementObj" => false,
                    "flag" => false
                );
                return $responseArr;
            }
            $responseArr = array(
                "incrementObj" => false,
                "flag" => false
            );
            return $responseArr;
        }
    }

    public function redisAddData($client, $metadata, $incrementObj,$token,$clientId,$realTimeFlag)
    {
        $redis = Redis::connection();
        /*
         * Get All Data
         */
        //$allKeys=$redis->keys("*");
        $metadataObj = [];
        /*
         * Get all metadata
         */

        $metadataObj[$client] = $metadata;
        /*
         * Check length and get data
         */
        foreach ($metadataObj as $key => $metadataVal) {
            $key_meta=$clientId.":".$key.":".$token;
            $lengthObj = $redis->hgetAll("metadataDetails");
            $totalLength=$lengthObj[$key];
            if($realTimeFlag){
                $key=$key."-realtime";
            }
            if (isset($lengthObj[$key_meta])) {
                $metadataDetailsObj = json_decode($lengthObj[$key_meta]);
                $length = json_decode($lengthObj[$key_meta])->length;
                $dataLength = self::checkLength($metadataVal, $metadataDetailsObj, $incrementObj);
                if ($incrementObj && $dataLength) {
                    $data = self::getLimitData($metadataVal, $metadataDetailsObj, $dataLength, $incrementObj);
                    $updatedKey = [];
                    $incrementObj = json_decode($incrementObj);
                    $tempArr = [];
                    $lastData = "";
                    foreach ($data as $dt) {
                        $selectedKeys = [];
                        foreach ($incrementObj->selected_Key as $keyObj) {
                            $keyObj = json_decode($keyObj);
                            array_push($selectedKeys, $keyObj->Field . "(" . $keyObj->tableName . ")");
                        }
                        $setKey = "";
                        foreach ($selectedKeys as $selectedKey) {
                            $setKey .= $dt->$selectedKey;
                        }
                        array_push($updatedKey, $setKey);
                        $lastData = (array)$dt;
                        foreach ($incrementObj->table as $table) {
                            if (isset($table->key)) {
                                if (!isset($tempArr[$table->name])) {
                                    $tempArr[$table->name] = $lastData[$table->key . "(" . $table->name . ")"];
                                } else if (strtotime($lastData[$table->key . "(" . $table->name . ")"]) > strtotime($tempArr[$table->name])) {
                                    $tempArr[$table->name] = $lastData[$table->key . "(" . $table->name . ")"];
                                }
                            }

                        }
                        $redis->hset($key, $setKey, json_encode($dt));
                    }
                    $metadataDetailsObj->length = $dataLength;
                    $metadataDetailsObj->updated_at = $tempArr;
                    $metadataDetailsObj->updated_key = $updatedKey;
                    $redis->hset("metadataDetails", $key_meta, json_encode($metadataDetailsObj));
                } else if ($dataLength > $length) {
                    $totalLength=$lengthObj[$key];
                    $data = self::getLimitData($metadataVal, $metadataDetailsObj, $dataLength, "");
                    $i=1;
                    $newKeyCreated=[];
                    foreach ($data as $dt) {
                        $newKey=$totalLength + $i;
                        array_push($newKeyCreated,$newKey);
                        $redis->hset($key,$newKey , json_encode($dt));
                        $i++;
                    }
                    $metadataDetailsObj->length = $dataLength;
                    $metadataDetailsObj->newCreatedKey = $newKeyCreated;
                    $redis->hset("metadataDetails", $key_meta, json_encode($metadataDetailsObj));
                }
            }
        }
    }
}

?>