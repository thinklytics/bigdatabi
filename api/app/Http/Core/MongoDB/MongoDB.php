<?php
namespace App\Http\Core\MongoDB;
use App\Http\Contract\DatasourceInterface;
use App\Http\Core\Data\QueryResult;
use App\Http\Utils\DBHelper;
use App\Http\Utils\HelperFunctions;
use App\Model\RoleLevelSecurity;
use Exception;
use Mockery\Undefined;
use PDO;
use Illuminate\Database\Capsule\Manager as DB;
use Illuminate\Support\Facades\Redis;
use PhpParser\Node\Expr\Cast\Object_;
use Illuminate\Support\Facades\Auth;
use App\Model\Metadata;

class MongoDB extends DatasourceInterface {

    static $con = null;
    static $conObject = null;
    public function connect($connectQuery)
    {
        self::$conObject=$connectQuery;
        self::$con=DBHelper::switchDataBase($connectQuery);
        return $this;
    }
    /*
     * List of data
     */
    public function listDatabase()
    {
        try{
            $collections = self::$con->listDatabases();
            $dbArray=array();
            foreach ($collections as $collection) {
                $dbname = $collection->getName();
                $tempArr=[];
                $tempArr['schema_name']=$dbname;
                array_push($dbArray,$tempArr);
            }
            $this->result =  $dbArray;
            $this->errorCode = 1;
            $this->message = "Connection Successfully";
        }catch(Exception $ex){
            $this->errorCode = 0;
            $this->message = $ex->getMessage();
        }
        return $this;
    }
    public function listTables()
    {
        try{
            $tableName=self::$conObject->dbname;
            $db=self::$con->$tableName;
            $listTable = $db->listCollections();
            $tableArray=[];
            foreach ($listTable as $collection) {
                array_push($tableArray,$collection->getName());
            }
            $this->result =  $tableArray;
            $this->errorCode = 1;
            $this->message = "Connection Successfully";
        }catch(Exception $ex){
            $this->errorCode = 0;
            $this->message = $ex->getMessage();
        }
        return $this;
    }
    public function getData($metadataObject){
        try{
            $dbName=self::$conObject->dbname;
            $db=self::$con->$dbName;
            $data=$db->selectCollection($metadataObject->rootTable);
            $pipeline=[];
            if (isset($metadataObject->joinsDetails)) {
                $joinTables=[];
                foreach ($metadataObject->joinsDetails as $ros) {
                    foreach ($ros->node as $row) {
                        if (is_object(json_decode($row->leftColumn))) {
                            $leftColumn = json_decode($row->leftColumn);
                            $rightColumn = json_decode($row->rightColumn);
                            $joinLeftColumn = "`" . $leftColumn->tableName . "`.`" . $leftColumn->Field . "`";
                            $joinRightColumn = "`" . $rightColumn->tableName . "`.`" . $rightColumn->Field . "`";
                        } else {
                            $joinLeftColumn = "`" . $ros->leftColumnTable . "`.`" . $row->leftColumn . "`";
                            $joinRightColumn = "`" . $ros->leftColumnTable . "`.`" . $row->leftColumn . "`";
                        }
                        $leftColumn = trim(preg_replace('/\s\s+/', ' ', $joinLeftColumn));
                        $rightColumn = trim(preg_replace('/\s\s+/', ' ', $joinRightColumn));
                        $lookup['$lookup']=[];
                        $lookup['$lookup']['from']=$ros->rightColumnTable;
                        if(json_decode($row->leftColumn)->tableName==$metadataObject->rootTable || isset($metadataObject->blending))
                            $lookup['$lookup']['localField']=json_decode($row->leftColumn)->Field;
                        else
                            $lookup['$lookup']['localField']=json_decode($row->leftColumn)->tableName.".".json_decode($row->leftColumn)->Field;
                        $lookup['$lookup']['foreignField']=json_decode($row->rightColumn)->Field;
                        $lookup['$lookup']['as']=$ros->rightColumnTable;
                        $unwind=[];
                        $unwind['$unwind']="$".$ros->rightColumnTable;
                        array_push($pipeline,$lookup);
                        array_push($pipeline,$unwind);
                        array_push($joinTables,$ros->rightColumnTable);
                    }
                }
            }

            if(isset($metadataObject->condition) && $metadataObject->condition){
                //array( '$and' => array(array('customers_id'=> array('$gt'=>3) )))) greater condition
                //Less then $lt
                // for and $and
                // for $or
                // for join table table append
                while(preg_match_all('/(\w*)(\(((\w+))\))+/',$metadataObject->condition,$out)){
                    //$str=$out[3][0].".".$out[1][0];
                    /* $out[3][0].".".$out[1][0]
                     * Filter not working so change this to simple $out[1][0]
                     */
                    $str=$out[1][0];
                    $metadataObject->condition=str_replace($out[0][0],$str,$metadataObject->condition);
                }
                //$conditions = preg_split( "/ (and|AND|or|OR) /", $metadataObject->condition );
                $whereArray=[];
                preg_match_all('/((?:\w+.\w+|\w+)\s*(?:=|<|>|>=|<=)\s*(?:\'\w*\s*\w*\'|\w+))(\s*and\s*|\s*or\s*|)/',$metadataObject->condition, $outArray);
                for($i=0;$i<count($outArray[1]);$i++){
                    if($outArray[1][$i])
                    array_push($whereArray,$outArray[1][$i]);
                    if($outArray[2][$i])
                    array_push($whereArray,$outArray[2][$i]);
                }
                /*preg_match_all('/((?:\w+.\w+|\w+)\s*=\s*\w+) (\s*and\s*|\s*or\s*) ((?:\w+.\w+|\w+)\s*=\s*\w+)/',$metadataObject->condition, $conditionExpArray);
                $whereArray=[];
                if(isset($conditionExpArray[0])!=0){
                    $i=0;
                    foreach ($conditionExpArray as $conditionArray){
                        if($i>0){
                            array_push($whereArray,$conditionArray[0]);
                        }
                        $i++;
                    }
                }else{
                    array_push($whereArray,$metadataObject->condition);
                }*/
                $match=[];
                $match['$match']=[];
                $match['$match']['$or']=[];
                $lastCondition='$or';

                foreach ($whereArray as $condition){
                    $conditionExp=[];
                    $condition=ltrim($condition," ");
                    $condition=rtrim($condition," ");
                    if($condition=="and" || $condition=="or"){
                        $lastCondition='$'.$condition;
                    }else{
                        while(preg_match_all('/(\w+.\w+|\w+)(\s*=\s*|\s*<\s*|\s*>\s*|\s*<=\s*|\s*>=\s*)(\'\w*\s*\w*\'|\w+)/',$condition, $conditionOpr)){
                            $conditionOpr[3][0]=trim($conditionOpr[3][0],"'");
                            $conditionOpr[3][0]=ltrim($conditionOpr[3][0]," ");
                            $conditionOpr[3][0]=rtrim($conditionOpr[3][0]," ");
                            if(is_numeric($conditionOpr[3][0])){
                                $conditionValue=intval($conditionOpr[3][0]);
                            }else{
                                $conditionValue=$conditionOpr[3][0];
                            }
                            $conditionKey=trim($conditionOpr[1][0],"`");
                            if($conditionOpr[2][0]==">"){
                                $conditionExp[$conditionKey]=array('$gt'=>$conditionValue);
                            }else if($conditionOpr[2][0]=="<"){
                                $conditionExp[$conditionKey]=array('$lt'=>$conditionValue);
                            }else if($conditionOpr[2][0]==">="){
                                $conditionExp[$conditionKey]=array('$gte'=>$conditionValue);
                            }else if($conditionOpr[2][0]=="<="){
                                $conditionExp[$conditionKey]=array('$lte'=>$conditionValue);
                            }else{
                                $conditionExp[$conditionKey]=$conditionValue;
                            }
                            $condition=str_replace($conditionOpr[0][0],"",$condition);
                        }
                        //if(count($conditionExp))
                        if(!isset($match['$match'][$lastCondition])){
                            $match['$match'][$lastCondition]=[];
                        }
                        array_push($match['$match'][$lastCondition],$conditionExp);
                    }
                }
                //array('$match'=>array('$and'=>array(array('customers_id'=> 3))))
                array_push($pipeline,$match);

            }
            if(isset($metadataObject->limit) && $metadataObject->limit){
                if($metadataObject->limit<100)
                    array_push($pipeline, array('$limit' => intval($metadataObject->limit)));
                else
                    array_push($pipeline, array('$limit' => 100));
            }else{
                array_push($pipeline, array('$limit' => 100));
            }
            //dd($pipeline);
            $list= $data->aggregate($pipeline);
            $tableColumnObject=[];
            $tableData=[];
            foreach ($list as $lists)
            {
                $singleData=[];
                foreach($lists as $key => $value)
                {
                    if($key!='_id') {
                        if (in_array($key, $joinTables)) {
                            foreach ($value as $keyJoin => $valueJoin) {
                                if($keyJoin!="_id") {
                                    $val = gettype($valueJoin);
                                    if ($val == "string") {
                                        $val = "text";
                                    } else if ($val == "Null") {
                                        $val = "text";
                                    }
                                    if ($val == "integer" || $val == "decimal" || $val == "double" || $val == "float") {
                                        if ($val == "integer") {
                                            $val = "int";
                                        }
                                        /*if (!isset($tableColumnObject[$keyJoin])) {
                                            $tableColumnObject[$keyJoin] = [];
                                            $tableColumnObject[$keyJoin]['tableName'] = $key;
                                            $tableColumnObject[$keyJoin]['Field'] = $keyJoin;
                                            $tableColumnObject[$keyJoin]['Type'] = $val;
                                            $tableColumnObject[$keyJoin]['dataType'] = "Measure";
                                        } else {
                                            if ($tableColumnObject[$keyJoin]['tableName'] != $key) {
                                                if(isset($metadataObject->blending)){
                                                    $tableColumnObject[$keyJoin . "_" . $key] = [];
                                                    $tableColumnObject[$keyJoin . "_" . $key]['tableName'] = $key;
                                                    $tableColumnObject[$keyJoin . "_" . $key]['Field'] = $keyJoin . "_" . $key;
                                                    $tableColumnObject[$keyJoin . "_" . $key]['Type'] = $val;
                                                    $tableColumnObject[$keyJoin . "_" . $key]['dataType'] = "Measure";
                                                }else{
                                                    $tableColumnObject[$keyJoin . "(" . $key . ")"] = [];
                                                    $tableColumnObject[$keyJoin . "(" . $key . ")"]['tableName'] = $key;
                                                    $tableColumnObject[$keyJoin . "(" . $key . ")"]['Field'] = $keyJoin . "(" . $key . ")";
                                                    $tableColumnObject[$keyJoin . "(" . $key . ")"]['Type'] = $val;
                                                    $tableColumnObject[$keyJoin . "(" . $key . ")"]['dataType'] = "Measure";
                                                }
                                            }
                                        }*/
                                        if(isset($metadataObject->blending) && $metadataObject->blending){
                                            $tableColumnObject[$keyJoin . "_" . $key] = [];
                                            $tableColumnObject[$keyJoin . "_" . $key]['tableName'] = $key;
                                            $tableColumnObject[$keyJoin . "_" . $key]['Field'] = $keyJoin . "_" . $key;
                                            $tableColumnObject[$keyJoin . "_" . $key]['Type'] = $val;
                                            $tableColumnObject[$keyJoin . "_" . $key]['dataType'] = "Measure";
                                        }else{
                                            $tableColumnObject[$keyJoin . "(" . $key . ")"] = [];
                                            $tableColumnObject[$keyJoin . "(" . $key . ")"]['tableName'] = $key;
                                            $tableColumnObject[$keyJoin . "(" . $key . ")"]['Field'] = $keyJoin . "(" . $key . ")";
                                            $tableColumnObject[$keyJoin . "(" . $key . ")"]['Type'] = $val;
                                            $tableColumnObject[$keyJoin . "(" . $key . ")"]['dataType'] = "Measure";
                                        }
                                    } else {
                                        /*if (!isset($tableColumnObject[$keyJoin])) {
                                            $tableColumnObject[$keyJoin] = [];
                                            $tableColumnObject[$keyJoin]['tableName'] = $key;
                                            $tableColumnObject[$keyJoin]['Field'] = $keyJoin;
                                            $tableColumnObject[$keyJoin]['Type'] = $val;
                                            $tableColumnObject[$keyJoin]['dataType'] = "Dimension";
                                        } else {
                                            if ($tableColumnObject[$keyJoin]['tableName'] != $key) {
                                                if(isset($metadataObject->blending)){
                                                    $tableColumnObject[$keyJoin . "_" . $key] = [];
                                                    $tableColumnObject[$keyJoin . "_" . $key]['tableName'] = $key;
                                                    $tableColumnObject[$keyJoin . "_" . $key]['Field'] = $keyJoin . "|" . $key;
                                                    $tableColumnObject[$keyJoin . "_" . $key]['Type'] = $val;
                                                    $tableColumnObject[$keyJoin . "_" . $key]['dataType'] = "Measure";
                                                }else{
                                                    $tableColumnObject[$keyJoin . "(" . $key . ")"] = [];
                                                    $tableColumnObject[$keyJoin . "(" . $key . ")"]['tableName'] = $key;
                                                    $tableColumnObject[$keyJoin . "(" . $key . ")"]['Field'] = $keyJoin . "(" . $key . ")";
                                                    $tableColumnObject[$keyJoin . "(" . $key . ")"]['Type'] = $val;
                                                    $tableColumnObject[$keyJoin . "(" . $key . ")"]['dataType'] = "Dimension";
                                                }
                                            }
                                        }*/
                                        if(isset($metadataObject->blending) && $metadataObject->blending){
                                            $tableColumnObject[$keyJoin . "_" . $key] = [];
                                            $tableColumnObject[$keyJoin . "_" . $key]['tableName'] = $key;
                                            $tableColumnObject[$keyJoin . "_" . $key]['Field'] = $keyJoin . "_" . $key;
                                            $tableColumnObject[$keyJoin . "_" . $key]['Type'] = $val;
                                            $tableColumnObject[$keyJoin . "_" . $key]['dataType'] = "Measure";
                                        }else{
                                            $tableColumnObject[$keyJoin . "(" . $key . ")"] = [];
                                            $tableColumnObject[$keyJoin . "(" . $key . ")"]['tableName'] = $key;
                                            $tableColumnObject[$keyJoin . "(" . $key . ")"]['Field'] = $keyJoin . "(" . $key . ")";
                                            $tableColumnObject[$keyJoin . "(" . $key . ")"]['Type'] = $val;
                                            $tableColumnObject[$keyJoin . "(" . $key . ")"]['dataType'] = "Dimension";
                                        }
                                    }
                                    if (isset($tableColumnObject[$keyJoin . "(" . $key . ")"])) {
                                        $singleData[$keyJoin . "(" . $key . ")"] = $valueJoin;
                                    } else if(isset($tableColumnObject[$keyJoin . "_" . $key])){
                                        $singleData[$keyJoin . "_" . $key] = $valueJoin;
                                    } else {
                                        $singleData[$keyJoin . "(" . $key . ")"] = $valueJoin;
                                    }
                                }
                            }
                        } else {
                            $val = gettype($value);
                            if ($val == "string") {
                                $val = "text";
                            } else if ($val == "Null") {
                                $val = "text";
                            }
                            if ($val == "integer" || $val == "decimal" || $val == "double" || $val == "float") {
                                if ($val == "integer") {
                                    $val = "int";
                                }
                                if(isset($metadataObject->blending) && $metadataObject->blending){
                                    $tableColumnObject[$key . "_" . $metadataObject->rootTable] = [];
                                    $tableColumnObject[$key . "_" . $metadataObject->rootTable]['tableName'] = $metadataObject->rootTable;
                                    $tableColumnObject[$key . "_" . $metadataObject->rootTable]['Field'] = $key . "_" . $metadataObject->rootTable;
                                    $tableColumnObject[$key . "_" . $metadataObject->rootTable]['Type'] = $val;
                                    $tableColumnObject[$key . "_" . $metadataObject->rootTable]['dataType'] = "Measure";
                                }else if (!isset($tableColumnObject[$key . "(" . $metadataObject->rootTable . ")"])) {
                                    $tableColumnObject[$key . "(" . $metadataObject->rootTable . ")"] = [];
                                    $tableColumnObject[$key . "(" . $metadataObject->rootTable . ")"]['tableName'] = $metadataObject->rootTable;
                                    $tableColumnObject[$key . "(" . $metadataObject->rootTable . ")"]['Field'] = $key . "(" . $metadataObject->rootTable . ")";
                                    $tableColumnObject[$key . "(" . $metadataObject->rootTable . ")"]['Type'] = $val;
                                    $tableColumnObject[$key . "(" . $metadataObject->rootTable . ")"]['dataType'] = "Measure";
                                }

                            } else {
                                if(isset($metadataObject->blending) && $metadataObject->blending){
                                    $tableColumnObject[$key . "_" . $metadataObject->rootTable] = [];
                                    $tableColumnObject[$key . "_" . $metadataObject->rootTable]['tableName'] = $metadataObject->rootTable;
                                    $tableColumnObject[$key . "_" . $metadataObject->rootTable]['Field'] = $key . "_" . $metadataObject->rootTable;
                                    $tableColumnObject[$key . "_" . $metadataObject->rootTable]['Type'] = $val;
                                    $tableColumnObject[$key . "_" . $metadataObject->rootTable]['dataType'] = "Dimension";
                                }
                                else if (!isset($tableColumnObject[$key . "(" . $metadataObject->rootTable . ")"])) {
                                    $tableColumnObject[$key . "(" . $metadataObject->rootTable . ")"] = [];
                                    $tableColumnObject[$key . "(" . $metadataObject->rootTable . ")"]['tableName'] = $metadataObject->rootTable;
                                    $tableColumnObject[$key . "(" . $metadataObject->rootTable . ")"]['Field'] = $key . "(" . $metadataObject->rootTable . ")";
                                    $tableColumnObject[$key . "(" . $metadataObject->rootTable . ")"]['Type'] = $val;
                                    $tableColumnObject[$key . "(" . $metadataObject->rootTable . ")"]['dataType'] = "Dimension";
                                }
                            }
                            if(isset($metadataObject->blending) && $metadataObject->blending){
                                $singleData[$key . "_" . $metadataObject->rootTable] = $value;
                            }else{
                                $singleData[$key . "(" . $metadataObject->rootTable . ")"] = $value;
                            }

                        }
                    }
                }
                array_push($tableData,$singleData);
            }
            $this->result =  array("tableColumn" => json_encode($tableColumnObject),"tableData" => json_encode($tableData));
            $this->errorCode = 1;
            $this->message = 'Successfully';
        }catch (Exception $ex){
            $this->errorCode = 0;
            $this->message = $ex->getMessage();
            $this->result = "";
        }
        return $this;
    }
    /*
     *
     * Dashboard Data
     */
    public function getDataDashboard($metadataObject){
            try{
                $dbName=self::$conObject->dbname;
                $db=self::$con->$dbName;
                $data=$db->selectCollection($metadataObject->rootTable);
                $pipeline=[];
                if (isset($metadataObject->joinsDetails)) {
                    $joinTables=[];
                    foreach ($metadataObject->joinsDetails as $ros) {
                        foreach ($ros->node as $row) {
                            if (is_object(json_decode($row->leftColumn))) {
                                $leftColumn = json_decode($row->leftColumn);
                                $rightColumn = json_decode($row->rightColumn);
                                $joinLeftColumn = "`" . $leftColumn->tableName . "`.`" . $leftColumn->Field . "`";
                                $joinRightColumn = "`" . $rightColumn->tableName . "`.`" . $rightColumn->Field . "`";
                            } else {
                                $joinLeftColumn = "`" . $ros->leftColumnTable . "`.`" . $row->leftColumn . "`";
                                $joinRightColumn = "`" . $ros->leftColumnTable . "`.`" . $row->leftColumn . "`";
                            }
                            $leftColumn = trim(preg_replace('/\s\s+/', ' ', $joinLeftColumn));
                            $rightColumn = trim(preg_replace('/\s\s+/', ' ', $joinRightColumn));
                            $lookup['$lookup']=[];
                            $lookup['$lookup']['from']=$ros->rightColumnTable;
                            //json_decode($row->leftColumn)->tableName==$metadataObject->rootTable
                            //|| isset($metadataObject->type)
                            if($ros->leftColumnTable==$metadataObject->rootTable ){
                                $lookup['$lookup']['localField']=json_decode($row->leftColumn)->Field;
                            }
                            else{
                                $lookup['$lookup']['localField']=$ros->leftColumnTable  .".".json_decode($row->leftColumn)->Field;
                            }
                            $lookup['$lookup']['foreignField']=json_decode($row->rightColumn)->Field;
                            $lookup['$lookup']['as']=$ros->rightColumnTable;
                            $unwind=[];
                            $unwind['$unwind']="$".$ros->rightColumnTable;
                            array_push($pipeline,$lookup);
                            array_push($pipeline,$unwind);
                            array_push($joinTables,$ros->rightColumnTable);
                        }
                    }
                }

                if(isset($metadataObject->condition) && $metadataObject->condition){
                    //array( '$and' => array(array('customers_id'=> array('$gt'=>3) )))) greater condition
                    //Less then $lt
                    // for and $and
                    // for $or
                    // for join table table append
                    while(preg_match_all('/(\w*)(\(((\w+))\))+/',$metadataObject->condition,$out)){
                        $str=$out[3][0].".".$out[1][0];
                        $metadataObject->condition=str_replace($out[0][0],$str,$metadataObject->condition);
                    }
                    //$conditions = preg_split( "/ (and|AND|or|OR) /", $metadataObject->condition );
                    $whereArray=[];
                    preg_match_all('/((?:\w+.\w+|\w+)\s*(?:=|<|>|>=|<=)\s*(?:\'\w*\s*\w*\'|\w+))(\s*and\s*|\s*or\s*|)/',$metadataObject->condition, $outArray);

                    for($i=0;$i<count($outArray[1]);$i++){
                        if($outArray[1][$i])
                            array_push($whereArray,$outArray[1][$i]);
                        if($outArray[2][$i])
                            array_push($whereArray,$outArray[2][$i]);
                    }

                    /*preg_match_all('/((?:\w+.\w+|\w+)\s*=\s*\w+) (\s*and\s*|\s*or\s*) ((?:\w+.\w+|\w+)\s*=\s*\w+)/',$metadataObject->condition, $conditionExpArray);
                    $whereArray=[];
                    if(isset($conditionExpArray[0])!=0){
                        $i=0;
                        foreach ($conditionExpArray as $conditionArray){
                            if($i>0){
                                array_push($whereArray,$conditionArray[0]);
                            }
                            $i++;
                        }
                    }else{
                        array_push($whereArray,$metadataObject->condition);
                    }*/
                    $match=[];
                    $match['$match']=[];
                    $match['$match']['$or']=[];
                    $lastCondition='$or';
                    foreach ($whereArray as $condition){

                        $conditionExp=[];
                        $condition=ltrim($condition," ");
                        $condition=rtrim($condition," ");
                        if($condition=="and" || $condition=="or"){
                            $lastCondition='$'.$condition;
                        }else{
                            while(preg_match_all('/(\w+.\w+|\w+)(\s*=\s*|\s*<\s*|\s*>\s*|\s*<=\s*|\s*>=\s*)(\'\w*\s*\w*\'|\w+)/',$condition, $conditionOpr)){

                                $conditionOpr[3][0]=trim($conditionOpr[3][0],"'");
                                $conditionOpr[3][0]=ltrim($conditionOpr[3][0]," ");
                                $conditionOpr[3][0]=rtrim($conditionOpr[3][0]," ");
                                if(is_numeric($conditionOpr[3][0])){
                                    $conditionValue=intval($conditionOpr[3][0]);
                                }else{
                                    $conditionValue=$conditionOpr[3][0];
                                }
                                $conditionKey=trim($conditionOpr[1][0],"`");
                                if($conditionOpr[2][0]==">"){
                                    $conditionExp[$conditionKey]=array('$gt'=>$conditionValue);
                                }else if($conditionOpr[2][0]=="<"){
                                    $conditionExp[$conditionKey]=array('$lt'=>$conditionValue);
                                }else if($conditionOpr[2][0]==">="){
                                    $conditionExp[$conditionKey]=array('$gte'=>$conditionValue);
                                }else if($conditionOpr[2][0]=="<="){
                                    $conditionExp[$conditionKey]=array('$lte'=>$conditionValue);
                                }else{
                                    $conditionExp[$conditionKey]=$conditionValue;
                                }
                                $condition=str_replace($conditionOpr[0][0],"",$condition);
                            }
                            //if(count($conditionExp))
                            if(!isset($match['$match'][$lastCondition])){
                                $match['$match'][$lastCondition]=[];
                            }
                            if(isset($metadataObject->type)){
                                $p=[];
                                foreach($conditionExp as $key=>$value){
                                    $temp=explode(".",$key);
                                    $p[$temp[1]."(".$temp[0].")"]=$value;
                                }
                                array_push($match['$match'][$lastCondition],$p);
                            }else{
                                array_push($match['$match'][$lastCondition],$conditionExp);
                            }
                        }
                    }
                    //array('$match'=>array('$and'=>array(array('customers_id'=> 3))))
                    array_push($pipeline,$match);
                }
                //Check one more condition
                if(isset($metadataObject->exCondition)){
                    if(isset($metadataObject->type)) {
                        $match = [];
                        $match['$match'] = [];
                        $match['$match']['$or'] = [];
                        $lastCondition = '$or';
                        $conditionExp = [];
                        foreach ($metadataObject->exCondition as $conditions) {
                            foreach (json_decode($conditions) as $key => $condition) {
                                $whereArray = [];
                                preg_match_all('/((?:(\w+\s)*\w+.\w+|\w+)\s*(?:=|<|>|>=|<=)\s*(?:\'\w*\s*\w*\'|\w+))(\s*and\s*|\s*or\s*|)/', $condition, $outArray);
                                for ($i = 0; $i < count($outArray[1]); $i++) {
                                    if ($outArray[1][$i])
                                        array_push($whereArray, $outArray[1][$i]);
                                    if ($outArray[3][$i])
                                        array_push($whereArray, $outArray[3][$i]);
                                }
                                foreach ($whereArray as $condition) {
                                    $condition = ltrim($condition, " ");
                                    $condition = rtrim($condition, " ");
                                    if (preg_match("/^and/", $condition) != null || preg_match("/^or/", $condition) != null) {
                                        $lastCondition = '$' . $condition;
                                    } else {
                                        while (preg_match_all('/(?:((?:\w*\s)*\w+.\w+|\w+)\s*(=)\s*|\s*<\s*|\s*>\s*|\s*<=\s*|\s*>=\s*)(\'\w*\s*\w*\'|\w+)/', $condition, $conditionOpr)) {
                                            $conditionOpr[3][0] = trim($conditionOpr[3][0], "'");
                                            $conditionOpr[3][0] = ltrim($conditionOpr[3][0], " ");
                                            $conditionOpr[3][0] = rtrim($conditionOpr[3][0], " ");
                                            if (is_numeric($conditionOpr[3][0])) {
                                                $conditionValue = intval($conditionOpr[3][0]);
                                            } else {
                                                $conditionValue = $conditionOpr[3][0];
                                            }
                                            $conditionKey = trim($conditionOpr[1][0], "`");
                                            if ($conditionOpr[2][0] == ">") {
                                                $conditionExp[$conditionKey] = array('$gt' => $conditionValue);
                                            } else if ($conditionOpr[2][0] == "<") {
                                                $conditionExp[$conditionKey] = array('$lt' => $conditionValue);
                                            } else if ($conditionOpr[2][0] == ">=") {
                                                $conditionExp[$conditionKey] = array('$gte' => $conditionValue);
                                            } else if ($conditionOpr[2][0] == "<=") {
                                                $conditionExp[$conditionKey] = array('$lte' => $conditionValue);
                                            } else {
                                                $conditionExp[$conditionKey] = $conditionValue;
                                            }
                                            $condition = str_replace($conditionOpr[0][0], "", $condition);
                                        }
                                        //if(count($conditionExp))
                                        if (!isset($match['$match'][$lastCondition])) {
                                            $match['$match'][$lastCondition] = [];
                                        }
                                        array_push($match['$match'][$lastCondition], $conditionExp);
                                    }
                                }
                            }
                        }
                        array_push($pipeline, $match);
                    }else{
                        $match=[];
                        $match['$match']=[];
                        $match['$match']['$or']=[];
                        $lastCondition='$or';
                        $conditionExp=[];
                        foreach($metadataObject->exCondition as $conditions){
                            foreach (json_decode($conditions) as $key=>$condition){
                                while(preg_match_all('/(\w*)(\(((\w+))\))+/',$condition,$out)){
                                    $str=$out[3][0].".".$out[1][0];
                                    $condition=str_replace($out[0][0],$str,$condition);
                                }
                            }
                            $whereArray=[];
                            //preg_match_all('/((?:\w+.\w+|\w+)\s*(?:=|<|>|>=|<=)\s*(?:\'\w*\s*\w*\'|\w+))(\s*and\s*|\s*or\s*|)/',$condition, $outArray);
                            preg_match_all('/((?:(\w*\s*\w*)+.\w+|\w+)\s*(?:=|<|>|>=|<=)\s*(?:\'\w*\s*\w*\'|\w+))(\s*and\s*|\s*or\s*|)/',$condition, $outArray);
                            for($i=0;$i<count($outArray[1]);$i++){
                                if($outArray[1][$i])
                                    array_push($whereArray,$outArray[1][$i]);
                                if($outArray[2][$i])
                                    array_push($whereArray,$outArray[2][$i]);
                            }
                            $condition=ltrim($condition," ");
                            $condition=rtrim($condition," ");
                            if($condition=="and" || $condition=="or"){
                                $lastCondition='$'.$condition;
                            }else{
                                while(preg_match_all('/(\w+.\w+|\w+)(\s*=\s*|\s*<\s*|\s*>\s*|\s*<=\s*|\s*>=\s*)(\'\w*\s*\w*\'|\w+)/',$condition, $conditionOpr)){
                                    $conditionOpr[3][0]=trim($conditionOpr[3][0],"'");
                                    $conditionOpr[3][0]=ltrim($conditionOpr[3][0]," ");
                                    $conditionOpr[3][0]=rtrim($conditionOpr[3][0]," ");
                                    if(is_numeric($conditionOpr[3][0])){
                                        $conditionValue=intval($conditionOpr[3][0]);
                                    }else{
                                        $conditionValue=$conditionOpr[3][0];
                                    }
                                    $conditionKey=trim($conditionOpr[1][0],"`");
                                    if($conditionOpr[2][0]==">"){
                                        $conditionExp[$conditionKey]=array('$gt'=>$conditionValue);
                                    }else if($conditionOpr[2][0]=="<"){
                                        $conditionExp[$conditionKey]=array('$lt'=>$conditionValue);
                                    }else if($conditionOpr[2][0]==">="){
                                        $conditionExp[$conditionKey]=array('$gte'=>$conditionValue);
                                    }else if($conditionOpr[2][0]=="<="){
                                        $conditionExp[$conditionKey]=array('$lte'=>$conditionValue);
                                    }else{
                                        $conditionExp[$conditionKey]=$conditionValue;
                                    }
                                    $condition=str_replace($conditionOpr[0][0],"",$condition);
                                }
                                //if(count($conditionExp))
                                if(!isset($match['$match'][$lastCondition])){
                                    $match['$match'][$lastCondition]=[];
                                }
                                array_push($match['$match'][$lastCondition],$conditionExp);
                            }
                        }
                        array_push($pipeline,$match);
                    }
                }
                if(isset($metadataObject->limit) && $metadataObject->limit){
                    if($metadataObject->limit<100)
                        array_push($pipeline, array('$limit' => intval($metadataObject->limit)));
                    else
                        array_push($pipeline, array('$limit' => 100));
                }/*else{
                    array_push($pipeline, array('$limit' => 100));
                }*/
                $list= $data->aggregate($pipeline);
                $tableColumnObject=[];
                $tableData=[];
                foreach ($list as $lists)
                {
                    $singleData=[];
                    foreach($lists as $key => $value)
                    {
                        if($key!='_id') {
                            if (in_array($key, $joinTables)) {
                                foreach ($value as $keyJoin => $valueJoin) {
                                    if($keyJoin!="_id") {
                                        $val = gettype($valueJoin);
                                        if ($val == "string") {
                                            $val = "text";
                                        } else if ($val == "Null") {
                                            $val = "text";
                                        }
                                        if ($val == "integer" || $val == "decimal" || $val == "double" || $val == "float") {
                                            if ($val == "integer") {
                                                $val = "int";
                                            }
                                            /*if (!isset($tableColumnObject[$keyJoin])) {
                                                $tableColumnObject[$keyJoin] = [];
                                                $tableColumnObject[$keyJoin]['tableName'] = $key;
                                                $tableColumnObject[$keyJoin]['Field'] = $keyJoin;
                                                $tableColumnObject[$keyJoin]['Type'] = $val;
                                                $tableColumnObject[$keyJoin]['dataType'] = "Measure";
                                            } else {*/
                                                if ($tableColumnObject[$keyJoin]['tableName'] != $key) {
                                                    $tableColumnObject[$keyJoin . "(" . $key . ")"] = [];
                                                    $tableColumnObject[$keyJoin . "(" . $key . ")"]['tableName'] = $key;
                                                    $tableColumnObject[$keyJoin . "(" . $key . ")"]['Field'] = $keyJoin . "(" . $key . ")";
                                                    $tableColumnObject[$keyJoin . "(" . $key . ")"]['Type'] = $val;
                                                    $tableColumnObject[$keyJoin . "(" . $key . ")"]['dataType'] = "Measure";
                                                }
                                            /*}*/
                                        } else {
                                            /*if (!isset($tableColumnObject[$keyJoin])) {
                                                $tableColumnObject[$keyJoin] = [];
                                                $tableColumnObject[$keyJoin]['tableName'] = $key;
                                                $tableColumnObject[$keyJoin]['Field'] = $keyJoin;
                                                $tableColumnObject[$keyJoin]['Type'] = $val;
                                                $tableColumnObject[$keyJoin]['dataType'] = "Dimension";
                                            } else {*/
                                                if ($tableColumnObject[$keyJoin]['tableName'] != $key) {
                                                    $tableColumnObject[$keyJoin . "(" . $key . ")"] = [];
                                                    $tableColumnObject[$keyJoin . "(" . $key . ")"]['tableName'] = $key;
                                                    $tableColumnObject[$keyJoin . "(" . $key . ")"]['Field'] = $keyJoin . "(" . $key . ")";
                                                    $tableColumnObject[$keyJoin . "(" . $key . ")"]['Type'] = $val;
                                                    $tableColumnObject[$keyJoin . "(" . $key . ")"]['dataType'] = "Dimension";
                                                }
                                            /*}*/
                                        }
                                        if (isset($tableColumnObject[$keyJoin . "(" . $key . ")"])) {
                                            $singleData[$keyJoin . "(" . $key . ")"] = $valueJoin;
                                        } else {
                                            $singleData[$keyJoin] = $valueJoin;
                                        }
                                    }
                                }
                            } else {
                                $val = gettype($value);
                                if ($val == "string") {
                                    $val = "text";
                                } else if ($val == "Null") {
                                    $val = "text";
                                }
                                if ($val == "integer" || $val == "decimal" || $val == "double" || $val == "float") {
                                    if ($val == "integer") {
                                        $val = "int";
                                    }
                                    if (!isset($tableColumnObject[$key])) {
                                        $tableColumnObject[$key] = [];
                                        $tableColumnObject[$key]['tableName'] = $metadataObject->rootTable;
                                        $tableColumnObject[$key]['Field'] = $key;
                                        $tableColumnObject[$key]['Type'] = $val;
                                        $tableColumnObject[$key]['dataType'] = "Measure";
                                    }
                                } else {
                                    if (!isset($tableColumnObject[$key])) {
                                        $tableColumnObject[$key] = [];
                                        $tableColumnObject[$key]['tableName'] = $metadataObject->rootTable;
                                        $tableColumnObject[$key]['Field'] = $key;
                                        $tableColumnObject[$key]['Type'] = $val;
                                        $tableColumnObject[$key]['dataType'] = "Dimension";
                                    }
                                }
                                $singleData[$key] = $value;
                            }
                        }
                    }
                    array_push($tableData,$singleData);
                }
                $this->result =  array("tableColumn" => json_encode($tableColumnObject),"tableData" => json_encode($tableData));
                $this->errorCode = 1;
                $this->message = 'Successfully';
            }catch (Exception $ex){
                $this->errorCode = 0;
                $this->message = $ex->getMessage();
                $this->result = "";
            }
            return $this;
    }
    /*
     * Get column name
     */
    public function getColumnName($tableName){
        try {
            $dbName = self::$conObject->dbname;
            $db = self::$con->$dbName;
            $data = $db->selectCollection($tableName);
            $list = $data->find();
            $tableColumnObject = [];
            foreach ($list as $lists) {
                foreach ($lists as $key => $value) {
                    $val = gettype($value);
                    if ($val == "integer" || $val == "decimal" || $val == "double" || $val == "float") {
                        $tableColumnObject[$key] = [];
                        $tableColumnObject[$key]['tableName'] = $tableName;
                        $tableColumnObject[$key]['Field'] = $key;
                        $tableColumnObject[$key]['Type'] = $val;
                        $tableColumnObject[$key]['dataType'] = "Measure";
                    } else {
                        $tableColumnObject[$key] = [];
                        $tableColumnObject[$key]['tableName'] = $tableName;
                        $tableColumnObject[$key]['Field'] = $key;
                        $tableColumnObject[$key]['Type'] = $val;
                        $tableColumnObject[$key]['dataType'] = "Dimension";
                    }
                }
            }
            $tableCol=[];
            foreach($tableColumnObject as $key=>$value){
                array_push($tableCol,$value);
            }
            $this->result = $tableCol;
            $this->errorCode = 1;
            $this->message = 'Successfully';
        }catch (Exception $ex){
            $this->errorCode = 0;
            $this->message = $ex->getMessage();
        }
        return $this;
    }
    public function getColumnMultiTable($tablesObj){
        try{
            $tableArr=[];
            $tableCol=[];
            $dbName = self::$conObject->dbname;
            $db = self::$con->$dbName;
            foreach ($tablesObj as $table) {
                $data = $db->selectCollection($table);
                $list = $data->find();
                $tableColumnObject = [];
                foreach ($list as $lists) {
                    foreach ($lists as $key => $value) {
                        $val = gettype($value);
                        if ($val == "integer" || $val == "decimal" || $val == "double" || $val == "float") {
                            $tableColumnObject[$key] = [];
                            $tableColumnObject[$key]['tableName'] = $table;
                            $tableColumnObject[$key]['Field'] = $key;
                            $tableColumnObject[$key]['Type'] = $val;
                            $tableColumnObject[$key]['dataType'] = "Measure";
                        } else {
                            $tableColumnObject[$key] = [];
                            $tableColumnObject[$key]['tableName'] = $table;
                            $tableColumnObject[$key]['Field'] = $key;
                            $tableColumnObject[$key]['Type'] = $val;
                            $tableColumnObject[$key]['dataType'] = "Dimension";
                        }
                    }
                }
                $tableCol[$table]=[];
                foreach($tableColumnObject as $key=>$value){
                    array_push($tableCol[$table],$value);
                }
            }
            $this->result =  $tableCol;
            $this->errorCode = 1;
            $this->message = 'Successfully';
        }catch (Exception $ex){
            $this->errorCode = 0;
            $this->message = $ex->getMessage();
        }
        return $this;
    }
    public function saveData($key,$value,$metadataId,$dataTypeCheck){
        $lastKey=$key;
        $dbName = self::$conObject->dbname;
        $db=self::$con->$dbName;
        $listCollection=$db->listCollections();
        $tableArray=[];
        foreach ($listCollection as $collection) {
            array_push($tableArray,$collection->getName());
        }
        //Check collection exist or not

        if(!in_array($key, $tableArray)){
            $this->mongoCollectionDataCreate($db,$key,$value,$metadataId,$lastKey);
        }else{
            if($dataTypeCheck==true) {
                $collection=$db->$key;
                $collection->drop();
                $this->mongoCollectionDataCreate($db, $key, $value, $metadataId, $lastKey);
            }
            /*$Blendingdata=json_decode($value->tableData)[0];
            $data=$db->selectCollection($key);
            $reuslt=$data->findOne();
            if($reuslt){
                $mongoData=[];
                foreach ($reuslt as $key1 => $value1) {
                    if($key1!="_id"){
                        $mongoData[$key1]=$value1;
                    }
                }
                if(isset($Blendingdata)) {
                    $i=1;
                    $key=$this->myRecursiveFunction($key,$tableArray,$i);
                    $this->mongoCollectionDataCreate($db, $key, $value,$metadataId,$lastKey);
                }
            }*/
        }
    }
    public function myRecursiveFunction($key,$tableArray,$i)
    {
        if (in_array($key, $tableArray)){
            $key = $key . "_".$i;
            $i++;
            $this->myRecursiveFunction($key,$tableArray,$i);
        }
        return $key;
    }
    public function mongoCollectionDataCreate($db,$key,$value,$metadataId,$lastKey){
        $db->createCollection(
            $key,
            array(
                'capped' => true,
                'size' => 10000 * 1024
            )
        ); 
        $col = $db->selectCollection($key);
        /*if($key!=$lastKey){
            $metadata=Metadata::find($metadataId);
            $metadataObject=json_decode($metadata->metadataObject);
            if($metadataObject->rootTable==$lastKey){
                $metadataObject->rootTable=$key;
            }
            foreach($metadataObject->joinsDetails as $key_new=>$values_new){
                if($key_new==$lastKey){
                    $key_new=$key;
                }
                if($values_new->leftColumnTable==$lastKey){
                    $values_new->leftColumnTable=$key;
                }
                if($values_new->rightColumnTable==$lastKey){
                    $values_new->rightColumnTable=$key;
                }
                if($values_new->root==$lastKey){
                    $values_new->root=$key;
                }
            }
            $metadata->metadataObject=json_encode($metadataObject);
            $metadata->save();
        }*/
        //dd(json_decode($value->tableData));
        $tableData=json_encode( json_decode($value->tableData), JSON_NUMERIC_CHECK );
        $value->tableData=str_replace("$","",$tableData);
        $tableDataEncoded=json_decode($tableData);
        $col->insertMany($tableDataEncoded);
    }
    public function syncData($key,$value,$metadataId,$dataTypeCheck){
        $lastKey=$key;
        $dbName = self::$conObject->dbname;
        $db=self::$con->$dbName;
        //$listCollection=$db->listCollections();
        $collection=$db->$key;
        $collection->drop();
        //$this->mongoCollectionDataCreate($db, $key, $value, $metadataId, $lastKey);
        $db->createCollection(
            $key,
            array(
                'capped' => true,
                'size' => 10000 * 1024
            )
        );
        $col = $db->selectCollection($key);
        $tableData=json_encode( json_decode($value->tableData), JSON_NUMERIC_CHECK );
        $tableDataEncoded=json_decode($tableData);
        $col->insertMany($tableDataEncoded);
    }
    /*public function getDataLength(){
	    $this->result =  10;
        $this->errorCode = 1;
        $this->message = 'Successfully';
        return $this;
    }*/
    /*
    * ************************************** Server side code ******************************************************
    */
    //  Cache data to redis
    public function queryDataToredis($metadataObject, $incrementObj,$token,$department){
        ini_set('memory_limit', '-1');
        set_time_limit(0);
        $data = array();
        $company_id=Auth::user()->company_id;
        $client_id=Auth::user()->id;
        if($incrementObj && isset($metadataObject->realtime)){
            $client_id=$client_id."-realtime";
            $token=$token."-realtime";
        }
        /*
         * Role level conditions
         */
        $roleLevelCondition="";
        $userType=false;
        if(isset($metadataObject->exCondition)){
            $roleLevelCondition=$metadataObject->exCondition;
            $userType=$metadataObject->userType;
        }
        $realTimeFlag=false;
        if(isset($metadataObject->realtime)){
            $realTimeFlag=true;
        }
        if(!HelperFunctions::isCachedInRedis($client_id, $metadataObject->metadataId,$company_id,$realTimeFlag)){
            try{
                $dbName=self::$conObject->dbname;
                $db=self::$con->$dbName;
                $rootTable=$metadataObject->rootTable;
                $data=$db->selectCollection($metadataObject->rootTable);
                $pipeline=[];
                if (isset($metadataObject->joinsDetails)) {
                    $joinTables=[];
                    foreach ($metadataObject->joinsDetails as $ros) {
                        foreach ($ros->node as $row) {
                            if (is_object(json_decode($row->leftColumn))) {
                                $leftColumn = json_decode($row->leftColumn);
                                $rightColumn = json_decode($row->rightColumn);
                                $joinLeftColumn = "`" . $leftColumn->tableName . "`.`" . $leftColumn->Field . "`";
                                $joinRightColumn = "`" . $rightColumn->tableName . "`.`" . $rightColumn->Field . "`";
                            } else {
                                $joinLeftColumn = "`" . $ros->leftColumnTable . "`.`" . $row->leftColumn . "`";
                                $joinRightColumn = "`" . $ros->leftColumnTable . "`.`" . $row->leftColumn . "`";
                            }
                            $leftColumn = trim(preg_replace('/\s\s+/', ' ', $joinLeftColumn));
                            $rightColumn = trim(preg_replace('/\s\s+/', ' ', $joinRightColumn));
                            $lookup['$lookup']=[];
                            $lookup['$lookup']['from']=$ros->rightColumnTable;
                            //json_decode($row->leftColumn)->tableName==$metadataObject->rootTable
                            //|| isset($metadataObject->type)
                            if($ros->leftColumnTable==$metadataObject->rootTable ){
                                $lookup['$lookup']['localField']=json_decode($row->leftColumn)->Field;
                            }
                            else{
                                $lookup['$lookup']['localField']=$ros->leftColumnTable  .".".json_decode($row->leftColumn)->Field;
                            }
                            $lookup['$lookup']['foreignField']=json_decode($row->rightColumn)->Field;
                            $lookup['$lookup']['as']=$ros->rightColumnTable;
                            $unwind=[];
                            $unwind['$unwind']="$".$ros->rightColumnTable;
                            array_push($pipeline,$lookup);
                            array_push($pipeline,$unwind);
                            array_push($joinTables,$ros->rightColumnTable);
                        }
                    }
                }

                if(isset($metadataObject->condition) && $metadataObject->condition){
                    //array( '$and' => array(array('customers_id'=> array('$gt'=>3) )))) greater condition
                    //Less then $lt
                    // for and $and
                    // for $or
                    // for join table table append
                    while(preg_match_all('/(\w*)(\(((\w+))\))+/',$metadataObject->condition,$out)){
                        $str=$out[3][0].".".$out[1][0];
                        $metadataObject->condition=str_replace($out[0][0],$str,$metadataObject->condition);
                    }
                    //$conditions = preg_split( "/ (and|AND|or|OR) /", $metadataObject->condition );
                    $whereArray=[];
                    preg_match_all('/((?:\w+.\w+|\w+)\s*(?:=|<|>|>=|<=)\s*(?:\'\w*\s*\w*\'|\w+))(\s*and\s*|\s*or\s*|)/',$metadataObject->condition, $outArray);

                    for($i=0;$i<count($outArray[1]);$i++){
                        if($outArray[1][$i])
                            array_push($whereArray,$outArray[1][$i]);
                        if($outArray[2][$i])
                            array_push($whereArray,$outArray[2][$i]);
                    }
                    $match=[];
                    $match['$match']=[];
                    $match['$match']['$or']=[];
                    $lastCondition='$or';
                    foreach ($whereArray as $condition){

                        $conditionExp=[];
                        $condition=ltrim($condition," ");
                        $condition=rtrim($condition," ");
                        if($condition=="and" || $condition=="or"){
                            $lastCondition='$'.$condition;
                        }else{
                            while(preg_match_all('/(\w+.\w+|\w+)(\s*=\s*|\s*<\s*|\s*>\s*|\s*<=\s*|\s*>=\s*)(\'\w*\s*\w*\'|\w+)/',$condition, $conditionOpr)){

                                $conditionOpr[3][0]=trim($conditionOpr[3][0],"'");
                                $conditionOpr[3][0]=ltrim($conditionOpr[3][0]," ");
                                $conditionOpr[3][0]=rtrim($conditionOpr[3][0]," ");
                                if(is_numeric($conditionOpr[3][0])){
                                    $conditionValue=intval($conditionOpr[3][0]);
                                }else{
                                    $conditionValue=$conditionOpr[3][0];
                                }
                                $conditionKey=trim($conditionOpr[1][0],"`");
                                $whereTemp=explode(".",$conditionOpr[1][0]);
                                if($whereTemp[0]==$rootTable){
                                    $conditionKey=$whereTemp[1];
                                }
                                if($conditionOpr[2][0]==">"){
                                    $conditionExp[$conditionKey]=array('$gt'=>$conditionValue);
                                }else if($conditionOpr[2][0]=="<"){
                                    $conditionExp[$conditionKey]=array('$lt'=>$conditionValue);
                                }else if($conditionOpr[2][0]==">="){
                                    $conditionExp[$conditionKey]=array('$gte'=>$conditionValue);
                                }else if($conditionOpr[2][0]=="<="){
                                    $conditionExp[$conditionKey]=array('$lte'=>$conditionValue);
                                }else{
                                    $conditionExp[$conditionKey]=$conditionValue;
                                }
                                $condition=str_replace($conditionOpr[0][0],"",$condition);
                            }
                            //if(count($conditionExp))
                            if(!isset($match['$match'][$lastCondition])){
                                $match['$match'][$lastCondition]=[];
                            }
                            if(isset($metadataObject->type)){
                                $p=[];
                                foreach($conditionExp as $key=>$value){
                                    $temp=explode(".",$key);
                                    $p[$temp[1]."(".$temp[0].")"]=$value;
                                }
                                array_push($match['$match'][$lastCondition],$p);
                            }else{
                                array_push($match['$match'][$lastCondition],$conditionExp);
                            }
                        }
                    }
                    //array('$match'=>array('$and'=>array(array('customers_id'=> 3))))
                    array_push($pipeline,$match);
                }
                //Check one more condition
                $roleLevellengthQ="";
                $roleLevel=false;
                if(isset($metadataObject->exCondition) && $metadataObject->exCondition){
                    $roleLevellengthQ=$pipeline;
                    $roleLevel=true;
                    if(isset($metadataObject->type)) {
                        $match = [];
                        $match['$match'] = [];
                        $match['$match']['$or'] = [];
                        $lastCondition = '$or';
                        $conditionExp = [];
                        foreach ($metadataObject->exCondition as $conditions) {
                            foreach (json_decode($conditions) as $key => $condition) {
                                $whereArray = [];
                                preg_match_all('/((?:(\w+\s)*\w+.\w+|\w+)\s*(?:=|<|>|>=|<=)\s*(?:\'\w*\s*\w*\'|\w+))(\s*and\s*|\s*or\s*|)/', $condition, $outArray);
                                for ($i = 0; $i < count($outArray[1]); $i++) {
                                    if ($outArray[1][$i])
                                        array_push($whereArray, $outArray[1][$i]);
                                    if ($outArray[3][$i])
                                        array_push($whereArray, $outArray[3][$i]);
                                }
                                foreach ($whereArray as $condition) {
                                    $condition = ltrim($condition, " ");
                                    $condition = rtrim($condition, " ");
                                    if (preg_match("/^and/", $condition) != null || preg_match("/^or/", $condition) != null) {
                                        $lastCondition = '$' . $condition;
                                    } else {
                                        while (preg_match_all('/(?:((?:\w*\s)*\w+.\w+|\w+)\s*(=)\s*|\s*<\s*|\s*>\s*|\s*<=\s*|\s*>=\s*)(\'\w*\s*\w*\'|\w+)/', $condition, $conditionOpr)) {
                                            $conditionOpr[3][0] = trim($conditionOpr[3][0], "'");
                                            $conditionOpr[3][0] = ltrim($conditionOpr[3][0], " ");
                                            $conditionOpr[3][0] = rtrim($conditionOpr[3][0], " ");
                                            if (is_numeric($conditionOpr[3][0])) {
                                                $conditionValue = intval($conditionOpr[3][0]);
                                            } else {
                                                $conditionValue = $conditionOpr[3][0];
                                            }
                                            $conditionKey = trim($conditionOpr[1][0], "`");
                                            $whereTemp=explode(".",$conditionOpr[1][0]);
                                            if($whereTemp[0]==$rootTable){
                                                $conditionKey=$whereTemp[1];
                                            }
                                            if ($conditionOpr[2][0] == ">") {
                                                $conditionExp[$conditionKey] = array('$gt' => $conditionValue);
                                            } else if ($conditionOpr[2][0] == "<") {
                                                $conditionExp[$conditionKey] = array('$lt' => $conditionValue);
                                            } else if ($conditionOpr[2][0] == ">=") {
                                                $conditionExp[$conditionKey] = array('$gte' => $conditionValue);
                                            } else if ($conditionOpr[2][0] == "<=") {
                                                $conditionExp[$conditionKey] = array('$lte' => $conditionValue);
                                            } else {
                                                $conditionExp[$conditionKey] = $conditionValue;
                                            }
                                            $condition = str_replace($conditionOpr[0][0], "", $condition);
                                        }
                                        //if(count($conditionExp))
                                        if (!isset($match['$match'][$lastCondition])) {
                                            $match['$match'][$lastCondition] = [];
                                        }
                                        array_push($match['$match'][$lastCondition], $conditionExp);
                                    }
                                }
                            }
                        }
                        array_push($roleLevellengthQ, $match);
                    }else{
                        $match=[];
                        $match['$match']=[];
                        $match['$match']['$or']=[];
                        $lastCondition='$or';
                        $conditionExp=[];
                        foreach($metadataObject->exCondition as $conditions){
                            foreach (json_decode($conditions) as $key=>$condition){
                                while(preg_match_all('/(\w*)(\(((\w+))\))+/',$condition,$out)){
                                    $str=$out[3][0].".".$out[1][0];
                                    $condition=str_replace($out[0][0],$str,$condition);
                                }
                            }
                            $whereArray=[];
                            //preg_match_all('/((?:\w+.\w+|\w+)\s*(?:=|<|>|>=|<=)\s*(?:\'\w*\s*\w*\'|\w+))(\s*and\s*|\s*or\s*|)/',$condition, $outArray);
                            preg_match_all('/((?:(\w*\s*\w*)+.\w+|\w+)\s*(?:=|<|>|>=|<=)\s*(?:\'\w*\s*\w*\'|\w+))(\s*and\s*|\s*or\s*|)/',$condition, $outArray);
                            for($i=0;$i<count($outArray[1]);$i++){
                                if($outArray[1][$i]){
                                    array_push($whereArray,$outArray[1][$i]);
                                }
                                if($outArray[2][$i]){
                                    array_push($whereArray,$outArray[2][$i]);
                                }
                            }
                            $condition=ltrim($condition," ");
                            $condition=rtrim($condition," ");
                            if($condition=="and" || $condition=="or"){
                                $lastCondition='$'.$condition;
                            }else{
                                while(preg_match_all('/(\w+.\w+|\w+)(\s*=\s*|\s*<\s*|\s*>\s*|\s*<=\s*|\s*>=\s*)(\'\w*\s*\w*\'|\w+)/',$condition, $conditionOpr)){
                                    $conditionOpr[3][0]=trim($conditionOpr[3][0],"'");
                                    $conditionOpr[3][0]=ltrim($conditionOpr[3][0]," ");
                                    $conditionOpr[3][0]=rtrim($conditionOpr[3][0]," ");
                                    if(is_numeric($conditionOpr[3][0])){
                                        $conditionValue=intval($conditionOpr[3][0]);
                                    }else{
                                        $conditionValue=$conditionOpr[3][0];
                                    }
                                    $conditionKey=trim($conditionOpr[1][0],"`");
                                    $whereTemp=explode(".",$conditionOpr[1][0]);
                                    if($whereTemp[0]==$rootTable){
                                        $conditionKey=$whereTemp[1];
                                    }
                                    if($conditionOpr[2][0]==">"){
                                        $conditionExp[$conditionKey]=array('$gt'=>$conditionValue);
                                    }else if($conditionOpr[2][0]=="<"){
                                        $conditionExp[$conditionKey]=array('$lt'=>$conditionValue);
                                    }else if($conditionOpr[2][0]==">="){
                                        $conditionExp[$conditionKey]=array('$gte'=>$conditionValue);
                                    }else if($conditionOpr[2][0]=="<="){
                                        $conditionExp[$conditionKey]=array('$lte'=>$conditionValue);
                                    }else{
                                        $conditionExp[$conditionKey]=$conditionValue;
                                    }
                                    $condition=str_replace($conditionOpr[0][0],"",$condition);
                                }
                                //if(count($conditionExp))
                                if(!isset($match['$match'][$lastCondition])){
                                    $match['$match'][$lastCondition]=[];
                                }
                                array_push($match['$match'][$lastCondition],$conditionExp);
                            }
                        }
                        array_push($roleLevellengthQ,$match);
                    }
                }
                $realTimeFlag==false;

                if($incrementObj && isset($metadataObject->realtime)){
                    $realTimeObj=$metadataObject->realtime;
                    if(isset($realTimeObj->column)){
                        $column=json_decode($realTimeObj->column);
                        $columnName=explode("(",$column->columnName)[0];
                        $colName=$column->columnName;
                        //$columnName=explode("(",$column->columnName)[0];
                        $tempPipeline=$pipeline;
                        if($column->tableName==$rootTable){
                            array_push($tempPipeline, array('$sort' => array($columnName=>intval(-1))));
                        }else{
                            array_push($tempPipeline, array('$sort' => array($column->tableName.".".$columnName=>intval(-1))));
                        }
                        array_push($tempPipeline, array('$limit' => intval(1)));
                    }
                    $tablesLengthData = $data->aggregate($tempPipeline);
                    $realTimeFlag==true;
                }else{
                    if(isset($metadataObject->limit) && $metadataObject->limit){
                        if($metadataObject->limit<100)
                            array_push($pipeline, array('$limit' => intval($metadataObject->limit)));
                        else
                            array_push($pipeline, array('$limit' => 100));
                    }
                    $tablesLengthData = $data->aggregate($pipeline);
                }
                /*
                 *  Check total length
                 */

                /*if($roleLevel){
                    $roleLevellength=DB::select($roleLevellengthQ)[0]->dataLength;
                }*/
                $dataLength=count(iterator_to_array($tablesLengthData));
                if($roleLevel){
                    $roleLevellengthQData = $data->aggregate($roleLevellengthQ);
                    $roleLevellength=count(iterator_to_array($roleLevellengthQData));
                }
                $lastLimit=0;
                $limit=env('Cache_Limit');
                /*
                 * Check set limit and get data limit
                 */
                if($limit>$dataLength){
                    $limit=$dataLength;
                }
                if(isset($metadataObject->limit) && $metadataObject->limit && $limit>$metadataObject->limit){
                    $limit=$metadataObject->limit;
                }
                $rs=1;
                if(isset($metadataObject->limit) && $dataLength > $metadataObject->limit && $metadataObject->limit) {
                    $dataLength = $metadataObject->limit;
                }
                for($i=0;$i<$dataLength;$i=$i+$limit){
                    /*
                     * Realtime condition check
                     * @realtime
                     */
                    $tempPipleForLimit=$pipeline;
                    if($realTimeFlag==false){
                        array_push($tempPipleForLimit, array('$skip' => intval($i)));
                        array_push($tempPipleForLimit, array('$limit' => intval($limit)));
                        $list= $data->aggregate($tempPipleForLimit);
                    }else{
                        $list= $data->aggregate($tempPipeline);
                    }
                    $tableColumnObject=[];
                    $tableData=[];
                    foreach ($list as $lists)
                    {
                        $singleData=[];
                        foreach($lists as $key => $value)
                        {
                            if($key!='_id') {
                                if (in_array($key, $joinTables)) {
                                    foreach ($value as $keyJoin => $valueJoin) {
                                        if($keyJoin!="_id") {
                                            $val = gettype($valueJoin);
                                            if ($val == "string") {
                                                $val = "text";
                                            } else if ($val == "Null") {
                                                $val = "text";
                                            }
                                            if ($val == "integer" || $val == "decimal" || $val == "double" || $val == "float") {
                                                if ($val == "integer") {
                                                    $val = "int";
                                                }
                                                if (!isset($tableColumnObject[$keyJoin . "(" . $key . ")"])) {
                                                    $tableColumnObject[$keyJoin . "(" . $key . ")"] = [];
                                                    $tableColumnObject[$keyJoin . "(" . $key . ")"]['tableName'] = $key;
                                                    $tableColumnObject[$keyJoin . "(" . $key . ")"]['Field'] = $keyJoin . "(" . $key . ")";
                                                    $tableColumnObject[$keyJoin . "(" . $key . ")"]['Type'] = $val;
                                                    $tableColumnObject[$keyJoin . "(" . $key . ")"]['dataType'] = "Measure";
                                                } else {
                                                    if ($tableColumnObject[$keyJoin . "(" . $key . ")"]['tableName'] != $key) {
                                                        $tableColumnObject[$keyJoin . "(" . $key . ")"] = [];
                                                        $tableColumnObject[$keyJoin . "(" . $key . ")"]['tableName'] = $key;
                                                        $tableColumnObject[$keyJoin . "(" . $key . ")"]['Field'] = $keyJoin . "(" . $key . ")";
                                                        $tableColumnObject[$keyJoin . "(" . $key . ")"]['Type'] = $val;
                                                        $tableColumnObject[$keyJoin . "(" . $key . ")"]['dataType'] = "Measure";
                                                    }
                                                }
                                            } else {
                                                if (!isset($tableColumnObject[$keyJoin . "(" . $key . ")"])) {
                                                    $tableColumnObject[$keyJoin . "(" . $key . ")"] = [];
                                                    $tableColumnObject[$keyJoin . "(" . $key . ")"]['tableName'] = $key;
                                                    $tableColumnObject[$keyJoin . "(" . $key . ")"]['Field'] =  $keyJoin . "(" . $key . ")";
                                                    $tableColumnObject[$keyJoin . "(" . $key . ")"]['Type'] = $val;
                                                    $tableColumnObject[$keyJoin . "(" . $key . ")"]['dataType'] = "Dimension";
                                                } else {
                                                    if ($tableColumnObject[$keyJoin . "(" . $key . ")"]['tableName'] != $key) {
                                                        $tableColumnObject[$keyJoin . "(" . $key . ")"] = [];
                                                        $tableColumnObject[$keyJoin . "(" . $key . ")"]['tableName'] = $key;
                                                        $tableColumnObject[$keyJoin . "(" . $key . ")"]['Field'] = $keyJoin . "(" . $key . ")";
                                                        $tableColumnObject[$keyJoin . "(" . $key . ")"]['Type'] = $val;
                                                        $tableColumnObject[$keyJoin . "(" . $key . ")"]['dataType'] = "Dimension";
                                                    }
                                                }
                                            }
                                            if (isset($tableColumnObject[$keyJoin . "(" . $key . ")"])) {
                                                $singleData[$keyJoin . "(" . $key . ")"] = $valueJoin;
                                            } else {
                                                $singleData[$keyJoin . "(" . $key . ")"] = $valueJoin;
                                            }
                                        }
                                    }
                                } else {
                                    $val = gettype($value);
                                    if ($val == "string") {
                                        $val = "text";
                                    } else if ($val == "Null") {
                                        $val = "text";
                                    }
                                    if ($val == "integer" || $val == "decimal" || $val == "double" || $val == "float") {
                                        if ($val == "integer") {
                                            $val = "int";
                                        }
                                        if (!isset($tableColumnObject[ $key . "(" . $metadataObject->rootTable . ")"])) {
                                            $tableColumnObject[$key . "(" . $metadataObject->rootTable . ")"] = [];
                                            $tableColumnObject[$key . "(" . $metadataObject->rootTable . ")"]['tableName'] = $metadataObject->rootTable;
                                            $tableColumnObject[$key . "(" . $metadataObject->rootTable . ")"]['Field'] = $key . "(" . $metadataObject->rootTable . ")";
                                            $tableColumnObject[$key . "(" . $metadataObject->rootTable . ")"]['Type'] = $val;
                                            $tableColumnObject[$key . "(" . $metadataObject->rootTable . ")"]['dataType'] = "Measure";
                                        }
                                    } else {
                                        if (!isset($tableColumnObject[$key . "(" . $metadataObject->rootTable . ")"])) {
                                            $tableColumnObject[$key . "(" . $metadataObject->rootTable . ")"] = [];
                                            $tableColumnObject[$key . "(" . $metadataObject->rootTable . ")"]['tableName'] = $metadataObject->rootTable;
                                            $tableColumnObject[$key . "(" . $metadataObject->rootTable . ")"]['Field'] = $key . "(" . $metadataObject->rootTable . ")";
                                            $tableColumnObject[$key . "(" . $metadataObject->rootTable . ")"]['Type'] = $val;
                                            $tableColumnObject[$key . "(" . $metadataObject->rootTable . ")"]['dataType'] = "Dimension";
                                        }
                                    }
                                    $singleData[$key . "(" . $metadataObject->rootTable . ")"] = $value;
                                }
                            }
                        }
                        array_push($tableData,$singleData);
                    }


                    if($incrementObj && isset($metadataObject->realtime)){
                        $maxDate=$tableData[0][$colName];
                        $maxDate=date('Y-m-d 23:59:59',strtotime($maxDate));
                        //dd($realTimeObj->dataShow);
                        $start_date=date('Y-m-d 00:00:00', strtotime($maxDate. " - $realTimeObj->dataShow days"));
                        $end_date=$maxDate;
                        /*
                         * Check if base table then table name concat skip
                         */
                        $temp=[];
                        $temp['$match']=[];
                        if($column->tableName==$rootTable){
                            $temp['$match'][$columnName]=[];
                            $temp['$match'][$columnName]['$gte']=$start_date;
                            $temp['$match'][$columnName]['$lt']=$end_date;
                        }else{
                            $temp['$match'][$column->tableName.".".$columnName]=[];
                            $temp['$match'][$column->tableName.".".$columnName]['$gte']=$start_date;
                            $temp['$match'][$column->tableName.".".$columnName]['$lt']=$end_date;
                        }
                        array_push($pipeline,$temp);
                        $list= $data->aggregate($pipeline);
                        $tableColumnObject=[];
                        $tableData=[];
                        foreach ($list as $lists)
                        {
                            $singleData=[];
                            foreach($lists as $key => $value)
                            {
                                if($key!='_id') {
                                    if (in_array($key, $joinTables)) {
                                        foreach ($value as $keyJoin => $valueJoin) {
                                            if($keyJoin!="_id") {
                                                $val = gettype($valueJoin);
                                                if ($val == "string") {
                                                    $val = "text";
                                                } else if ($val == "Null") {
                                                    $val = "text";
                                                }
                                                if ($val == "integer" || $val == "decimal" || $val == "double" || $val == "float") {
                                                    if ($val == "integer") {
                                                        $val = "int";
                                                    }
                                                    if (!isset($tableColumnObject[$keyJoin . "(" . $key . ")"])) {
                                                        $tableColumnObject[$keyJoin . "(" . $key . ")"] = [];
                                                        $tableColumnObject[$keyJoin . "(" . $key . ")"]['tableName'] = $key;
                                                        $tableColumnObject[$keyJoin . "(" . $key . ")"]['Field'] = $keyJoin . "(" . $key . ")";
                                                        $tableColumnObject[$keyJoin . "(" . $key . ")"]['Type'] = $val;
                                                        $tableColumnObject[$keyJoin . "(" . $key . ")"]['dataType'] = "Measure";
                                                    } else {
                                                        if ($tableColumnObject[$keyJoin . "(" . $key . ")"]['tableName'] != $key) {
                                                            $tableColumnObject[$keyJoin . "(" . $key . ")"] = [];
                                                            $tableColumnObject[$keyJoin . "(" . $key . ")"]['tableName'] = $key;
                                                            $tableColumnObject[$keyJoin . "(" . $key . ")"]['Field'] = $keyJoin . "(" . $key . ")";
                                                            $tableColumnObject[$keyJoin . "(" . $key . ")"]['Type'] = $val;
                                                            $tableColumnObject[$keyJoin . "(" . $key . ")"]['dataType'] = "Measure";
                                                        }
                                                    }
                                                } else {
                                                    if (!isset($tableColumnObject[$keyJoin . "(" . $key . ")"])) {
                                                        $tableColumnObject[$keyJoin . "(" . $key . ")"] = [];
                                                        $tableColumnObject[$keyJoin . "(" . $key . ")"]['tableName'] = $key;
                                                        $tableColumnObject[$keyJoin . "(" . $key . ")"]['Field'] =  $keyJoin . "(" . $key . ")";
                                                        $tableColumnObject[$keyJoin . "(" . $key . ")"]['Type'] = $val;
                                                        $tableColumnObject[$keyJoin . "(" . $key . ")"]['dataType'] = "Dimension";
                                                    } else {
                                                        if ($tableColumnObject[$keyJoin . "(" . $key . ")"]['tableName'] != $key) {
                                                            $tableColumnObject[$keyJoin . "(" . $key . ")"] = [];
                                                            $tableColumnObject[$keyJoin . "(" . $key . ")"]['tableName'] = $key;
                                                            $tableColumnObject[$keyJoin . "(" . $key . ")"]['Field'] = $keyJoin . "(" . $key . ")";
                                                            $tableColumnObject[$keyJoin . "(" . $key . ")"]['Type'] = $val;
                                                            $tableColumnObject[$keyJoin . "(" . $key . ")"]['dataType'] = "Dimension";
                                                        }
                                                    }
                                                }
                                                if (isset($tableColumnObject[$keyJoin . "(" . $key . ")"])) {
                                                    $singleData[$keyJoin . "(" . $key . ")"] = $valueJoin;
                                                } else {
                                                    $singleData[$keyJoin . "(" . $key . ")"] = $valueJoin;
                                                }
                                            }
                                        }
                                    } else {
                                        $val = gettype($value);
                                        if ($val == "string") {
                                            $val = "text";
                                        } else if ($val == "Null") {
                                            $val = "text";
                                        }
                                        if ($val == "integer" || $val == "decimal" || $val == "double" || $val == "float") {
                                            if ($val == "integer") {
                                                $val = "int";
                                            }
                                            if (!isset($tableColumnObject[ $key . "(" . $metadataObject->rootTable . ")"])) {
                                                $tableColumnObject[$key . "(" . $metadataObject->rootTable . ")"] = [];
                                                $tableColumnObject[$key . "(" . $metadataObject->rootTable . ")"]['tableName'] = $metadataObject->rootTable;
                                                $tableColumnObject[$key . "(" . $metadataObject->rootTable . ")"]['Field'] = $key . "(" . $metadataObject->rootTable . ")";
                                                $tableColumnObject[$key . "(" . $metadataObject->rootTable . ")"]['Type'] = $val;
                                                $tableColumnObject[$key . "(" . $metadataObject->rootTable . ")"]['dataType'] = "Measure";
                                            }
                                        } else {
                                            if (!isset($tableColumnObject[$key . "(" . $metadataObject->rootTable . ")"])) {
                                                $tableColumnObject[$key . "(" . $metadataObject->rootTable . ")"] = [];
                                                $tableColumnObject[$key . "(" . $metadataObject->rootTable . ")"]['tableName'] = $metadataObject->rootTable;
                                                $tableColumnObject[$key . "(" . $metadataObject->rootTable . ")"]['Field'] = $key . "(" . $metadataObject->rootTable . ")";
                                                $tableColumnObject[$key . "(" . $metadataObject->rootTable . ")"]['Type'] = $val;
                                                $tableColumnObject[$key . "(" . $metadataObject->rootTable . ")"]['dataType'] = "Dimension";
                                            }
                                        }
                                        $singleData[$key . "(" . $metadataObject->rootTable . ")"] = $value;
                                    }
                                }
                            }
                            array_push($tableData,$singleData);
                        }
                    }
                    $this->result =  array("tableColumn" => json_encode($tableColumnObject),"tableData" => json_encode($tableData));
                    $this->errorCode = 1;
                    $this->message = 'Successfully';
                    $tempArr=[];
                    $tempArr['flag']=1;
                    $tempArr['ukey']=$rs;
                    $tempArr['dataLength']=$dataLength;
                    if($roleLevel){
                        $tempArr['roleLevelLength']=$roleLevellength;
                    }
                    $data=HelperFunctions::writeToRedis($client_id, $metadataObject->metadataId, $this->result, $incrementObj,$company_id,$token,$tempArr,$roleLevelCondition,$department,$realTimeFlag,$userType);
                    $rs++;
                }
            }catch (Exception $ex){
                dd($ex->getMessage());
                $this->errorCode = 0;
                $this->message = $ex->getMessage();
                $this->result = "";
            }
        }else{
            $data=HelperFunctions::writeToRedis($client_id,$metadataObject->metadataId,$this,$incrementObj,$company_id,$token,0,$roleLevelCondition,$department,$realTimeFlag,$userType);
            $this->redisAddData($client_id . ":" . $metadataObject->metadataId.":".$company_id, $metadataObject, $incrementObj,$token,$client_id,$realTimeFlag);
        }
        try{
            $data['result']['tableColumn']=json_encode($data['result']['tableColumn']);
        }catch (Exception $e){

        }

        return $data;
    }
    public function getDataLength($metadataObject){
        $dbName=self::$conObject->dbname;
        $db=self::$con->$dbName;
        $rootTable=$metadataObject->rootTable;
        $data=$db->selectCollection($metadataObject->rootTable);
        $pipeline=[];
        if (isset($metadataObject->joinsDetails)) {
            $joinTables=[];
            foreach ($metadataObject->joinsDetails as $ros) {
                foreach ($ros->node as $row) {
                    if (is_object(json_decode($row->leftColumn))) {
                        $leftColumn = json_decode($row->leftColumn);
                        $rightColumn = json_decode($row->rightColumn);
                        $joinLeftColumn = "`" . $leftColumn->tableName . "`.`" . $leftColumn->Field . "`";
                        $joinRightColumn = "`" . $rightColumn->tableName . "`.`" . $rightColumn->Field . "`";
                    } else {
                        $joinLeftColumn = "`" . $ros->leftColumnTable . "`.`" . $row->leftColumn . "`";
                        $joinRightColumn = "`" . $ros->leftColumnTable . "`.`" . $row->leftColumn . "`";
                    }
                    $leftColumn = trim(preg_replace('/\s\s+/', ' ', $joinLeftColumn));
                    $rightColumn = trim(preg_replace('/\s\s+/', ' ', $joinRightColumn));
                    $lookup['$lookup']=[];
                    $lookup['$lookup']['from']=$ros->rightColumnTable;
                    //json_decode($row->leftColumn)->tableName==$metadataObject->rootTable
                    //|| isset($metadataObject->type)
                    if($ros->leftColumnTable==$metadataObject->rootTable ){
                        $lookup['$lookup']['localField']=json_decode($row->leftColumn)->Field;
                    }
                    else{
                        $lookup['$lookup']['localField']=$ros->leftColumnTable  .".".json_decode($row->leftColumn)->Field;
                    }
                    $lookup['$lookup']['foreignField']=json_decode($row->rightColumn)->Field;
                    $lookup['$lookup']['as']=$ros->rightColumnTable;
                    $unwind=[];
                    $unwind['$unwind']="$".$ros->rightColumnTable;
                    array_push($pipeline,$lookup);
                    array_push($pipeline,$unwind);
                    array_push($joinTables,$ros->rightColumnTable);
                }
            }
        }

        if(isset($metadataObject->condition) && $metadataObject->condition){
            //array( '$and' => array(array('customers_id'=> array('$gt'=>3) )))) greater condition
            //Less then $lt
            // for and $and
            // for $or
            // for join table table append
            while(preg_match_all('/(\w*)(\(((\w+))\))+/',$metadataObject->condition,$out)){
                $str=$out[3][0].".".$out[1][0];
                $metadataObject->condition=str_replace($out[0][0],$str,$metadataObject->condition);
            }
            //$conditions = preg_split( "/ (and|AND|or|OR) /", $metadataObject->condition );
            $whereArray=[];
            preg_match_all('/((?:\w+.\w+|\w+)\s*(?:=|<|>|>=|<=)\s*(?:\'\w*\s*\w*\'|\w+))(\s*and\s*|\s*or\s*|)/',$metadataObject->condition, $outArray);

            for($i=0;$i<count($outArray[1]);$i++){
                if($outArray[1][$i])
                    array_push($whereArray,$outArray[1][$i]);
                if($outArray[2][$i])
                    array_push($whereArray,$outArray[2][$i]);
            }
            $match=[];
            $match['$match']=[];
            $match['$match']['$or']=[];
            $lastCondition='$or';
            foreach ($whereArray as $condition){

                $conditionExp=[];
                $condition=ltrim($condition," ");
                $condition=rtrim($condition," ");
                if($condition=="and" || $condition=="or"){
                    $lastCondition='$'.$condition;
                }else{
                    while(preg_match_all('/(\w+.\w+|\w+)(\s*=\s*|\s*<\s*|\s*>\s*|\s*<=\s*|\s*>=\s*)(\'\w*\s*\w*\'|\w+)/',$condition, $conditionOpr)){

                        $conditionOpr[3][0]=trim($conditionOpr[3][0],"'");
                        $conditionOpr[3][0]=ltrim($conditionOpr[3][0]," ");
                        $conditionOpr[3][0]=rtrim($conditionOpr[3][0]," ");
                        if(is_numeric($conditionOpr[3][0])){
                            $conditionValue=intval($conditionOpr[3][0]);
                        }else{
                            $conditionValue=$conditionOpr[3][0];
                        }
                        $conditionKey=trim($conditionOpr[1][0],"`");
                        $whereTemp=explode(".",$conditionOpr[1][0]);
                        if($whereTemp[0]==$rootTable){
                            $conditionKey=$whereTemp[1];
                        }
                        if($conditionOpr[2][0]==">"){
                            $conditionExp[$conditionKey]=array('$gt'=>$conditionValue);
                        }else if($conditionOpr[2][0]=="<"){
                            $conditionExp[$conditionKey]=array('$lt'=>$conditionValue);
                        }else if($conditionOpr[2][0]==">="){
                            $conditionExp[$conditionKey]=array('$gte'=>$conditionValue);
                        }else if($conditionOpr[2][0]=="<="){
                            $conditionExp[$conditionKey]=array('$lte'=>$conditionValue);
                        }else{
                            $conditionExp[$conditionKey]=$conditionValue;
                        }
                        $condition=str_replace($conditionOpr[0][0],"",$condition);
                    }
                    //if(count($conditionExp))
                    if(!isset($match['$match'][$lastCondition])){
                        $match['$match'][$lastCondition]=[];
                    }
                    if(isset($metadataObject->type)){
                        $p=[];
                        foreach($conditionExp as $key=>$value){
                            $temp=explode(".",$key);
                            $p[$temp[1]."(".$temp[0].")"]=$value;
                        }
                        array_push($match['$match'][$lastCondition],$p);
                    }else{
                        array_push($match['$match'][$lastCondition],$conditionExp);
                    }
                }
            }
            //array('$match'=>array('$and'=>array(array('customers_id'=> 3))))
            array_push($pipeline,$match);
        }
        //Check one more condition
        if(isset($metadataObject->exCondition) && $metadataObject->exCondition){
            if(isset($metadataObject->type)) {
                $match = [];
                $match['$match'] = [];
                $match['$match']['$or'] = [];
                $lastCondition = '$or';
                $conditionExp = [];
                foreach ($metadataObject->exCondition as $conditions) {
                    foreach (json_decode($conditions) as $key => $condition) {
                        $whereArray = [];
                        preg_match_all('/((?:(\w+\s)*\w+.\w+|\w+)\s*(?:=|<|>|>=|<=)\s*(?:\'\w*\s*\w*\'|\w+))(\s*and\s*|\s*or\s*|)/', $condition, $outArray);
                        for ($i = 0; $i < count($outArray[1]); $i++) {
                            if ($outArray[1][$i])
                                array_push($whereArray, $outArray[1][$i]);
                            if ($outArray[3][$i])
                                array_push($whereArray, $outArray[3][$i]);
                        }
                        foreach ($whereArray as $condition) {
                            $condition = ltrim($condition, " ");
                            $condition = rtrim($condition, " ");
                            if (preg_match("/^and/", $condition) != null || preg_match("/^or/", $condition) != null) {
                                $lastCondition = '$' . $condition;
                            } else {
                                while (preg_match_all('/(?:((?:\w*\s)*\w+.\w+|\w+)\s*(=)\s*|\s*<\s*|\s*>\s*|\s*<=\s*|\s*>=\s*)(\'\w*\s*\w*\'|\w+)/', $condition, $conditionOpr)) {
                                    $conditionOpr[3][0] = trim($conditionOpr[3][0], "'");
                                    $conditionOpr[3][0] = ltrim($conditionOpr[3][0], " ");
                                    $conditionOpr[3][0] = rtrim($conditionOpr[3][0], " ");
                                    if (is_numeric($conditionOpr[3][0])) {
                                        $conditionValue = intval($conditionOpr[3][0]);
                                    } else {
                                        $conditionValue = $conditionOpr[3][0];
                                    }
                                    $conditionKey = trim($conditionOpr[1][0], "`");
                                    $whereTemp=explode(".",$conditionOpr[1][0]);
                                    if($whereTemp[0]==$rootTable){
                                        $conditionKey=$whereTemp[1];
                                    }
                                    if ($conditionOpr[2][0] == ">") {
                                        $conditionExp[$conditionKey] = array('$gt' => $conditionValue);
                                    } else if ($conditionOpr[2][0] == "<") {
                                        $conditionExp[$conditionKey] = array('$lt' => $conditionValue);
                                    } else if ($conditionOpr[2][0] == ">=") {
                                        $conditionExp[$conditionKey] = array('$gte' => $conditionValue);
                                    } else if ($conditionOpr[2][0] == "<=") {
                                        $conditionExp[$conditionKey] = array('$lte' => $conditionValue);
                                    } else {
                                        $conditionExp[$conditionKey] = $conditionValue;
                                    }
                                    $condition = str_replace($conditionOpr[0][0], "", $condition);
                                }
                                //if(count($conditionExp))
                                if (!isset($match['$match'][$lastCondition])) {
                                    $match['$match'][$lastCondition] = [];
                                }
                                array_push($match['$match'][$lastCondition], $conditionExp);
                            }
                        }
                    }
                }
                array_push($pipeline, $match);
            }else{
                $match=[];
                $match['$match']=[];
                $match['$match']['$or']=[];
                $lastCondition='$or';
                $conditionExp=[];
                foreach($metadataObject->exCondition as $conditions){
                    foreach (json_decode($conditions) as $key=>$condition){
                        while(preg_match_all('/(\w*)(\(((\w+))\))+/',$condition,$out)){
                            $str=$out[3][0].".".$out[1][0];
                            $condition=str_replace($out[0][0],$str,$condition);
                        }
                    }
                    $whereArray=[];
                    //preg_match_all('/((?:\w+.\w+|\w+)\s*(?:=|<|>|>=|<=)\s*(?:\'\w*\s*\w*\'|\w+))(\s*and\s*|\s*or\s*|)/',$condition, $outArray);
                    preg_match_all('/((?:(\w*\s*\w*)+.\w+|\w+)\s*(?:=|<|>|>=|<=)\s*(?:\'\w*\s*\w*\'|\w+))(\s*and\s*|\s*or\s*|)/',$condition, $outArray);
                    for($i=0;$i<count($outArray[1]);$i++){
                        if($outArray[1][$i]){
                            array_push($whereArray,$outArray[1][$i]);
                        }
                        if($outArray[2][$i]){
                            array_push($whereArray,$outArray[2][$i]);
                        }
                    }
                    $condition=ltrim($condition," ");
                    $condition=rtrim($condition," ");
                    if($condition=="and" || $condition=="or"){
                        $lastCondition='$'.$condition;
                    }else{
                        while(preg_match_all('/(\w+.\w+|\w+)(\s*=\s*|\s*<\s*|\s*>\s*|\s*<=\s*|\s*>=\s*)(\'\w*\s*\w*\'|\w+)/',$condition, $conditionOpr)){
                            $conditionOpr[3][0]=trim($conditionOpr[3][0],"'");
                            $conditionOpr[3][0]=ltrim($conditionOpr[3][0]," ");
                            $conditionOpr[3][0]=rtrim($conditionOpr[3][0]," ");
                            if(is_numeric($conditionOpr[3][0])){
                                $conditionValue=intval($conditionOpr[3][0]);
                            }else{
                                $conditionValue=$conditionOpr[3][0];
                            }
                            $conditionKey=trim($conditionOpr[1][0],"`");
                            $whereTemp=explode(".",$conditionOpr[1][0]);
                            if($whereTemp[0]==$rootTable){
                                $conditionKey=$whereTemp[1];
                            }
                            if($conditionOpr[2][0]==">"){
                                $conditionExp[$conditionKey]=array('$gt'=>$conditionValue);
                            }else if($conditionOpr[2][0]=="<"){
                                $conditionExp[$conditionKey]=array('$lt'=>$conditionValue);
                            }else if($conditionOpr[2][0]==">="){
                                $conditionExp[$conditionKey]=array('$gte'=>$conditionValue);
                            }else if($conditionOpr[2][0]=="<="){
                                $conditionExp[$conditionKey]=array('$lte'=>$conditionValue);
                            }else{
                                $conditionExp[$conditionKey]=$conditionValue;
                            }
                            $condition=str_replace($conditionOpr[0][0],"",$condition);
                        }
                        //if(count($conditionExp))
                        if(!isset($match['$match'][$lastCondition])){
                            $match['$match'][$lastCondition]=[];
                        }
                        array_push($match['$match'][$lastCondition],$conditionExp);
                    }
                }
                array_push($pipeline,$match);
            }
        }
        if(isset($metadataObject->limit) && $metadataObject->limit){
            if($metadataObject->limit<100)
                array_push($pipeline, array('$limit' => intval($metadataObject->limit)));
            else
                array_push($pipeline, array('$limit' => 100));
        }
        /*
         * We  can't fetch all data so i limit 100 data only
         */
        array_push($pipeline, array('$limit' => 100));
        if(isset($tempPipeline)){
            $list= $data->aggregate($tempPipeline);
        }else{
            $list= $data->aggregate($pipeline);
        }

        $tableColumnObject=[];
        $tableData=[];
        foreach ($list as $lists)
        {
            $singleData=[];
            foreach($lists as $key => $value)
            {
                if($key!='_id') {
                    if (in_array($key, $joinTables)) {
                        foreach ($value as $keyJoin => $valueJoin) {
                            if($keyJoin!="_id") {
                                $val = gettype($valueJoin);
                                if ($val == "string") {
                                    $val = "text";
                                } else if ($val == "Null") {
                                    $val = "text";
                                }
                                if ($val == "integer" || $val == "decimal" || $val == "double" || $val == "float") {
                                    if ($val == "integer") {
                                        $val = "int";
                                    }
                                    if (!isset($tableColumnObject[$keyJoin . "(" . $key . ")"])) {
                                        $tableColumnObject[$keyJoin . "(" . $key . ")"] = [];
                                        $tableColumnObject[$keyJoin . "(" . $key . ")"]['tableName'] = $key;
                                        $tableColumnObject[$keyJoin . "(" . $key . ")"]['Field'] = $keyJoin . "(" . $key . ")";
                                        $tableColumnObject[$keyJoin . "(" . $key . ")"]['Type'] = $val;
                                        $tableColumnObject[$keyJoin . "(" . $key . ")"]['dataType'] = "Measure";
                                    } else {
                                        if ($tableColumnObject[$keyJoin . "(" . $key . ")"]['tableName'] != $key) {
                                            $tableColumnObject[$keyJoin . "(" . $key . ")"] = [];
                                            $tableColumnObject[$keyJoin . "(" . $key . ")"]['tableName'] = $key;
                                            $tableColumnObject[$keyJoin . "(" . $key . ")"]['Field'] = $keyJoin . "(" . $key . ")";
                                            $tableColumnObject[$keyJoin . "(" . $key . ")"]['Type'] = $val;
                                            $tableColumnObject[$keyJoin . "(" . $key . ")"]['dataType'] = "Measure";
                                        }
                                    }
                                } else {
                                    if (!isset($tableColumnObject[$keyJoin . "(" . $key . ")"])) {
                                        $tableColumnObject[$keyJoin . "(" . $key . ")"] = [];
                                        $tableColumnObject[$keyJoin . "(" . $key . ")"]['tableName'] = $key;
                                        $tableColumnObject[$keyJoin . "(" . $key . ")"]['Field'] =  $keyJoin . "(" . $key . ")";
                                        $tableColumnObject[$keyJoin . "(" . $key . ")"]['Type'] = $val;
                                        $tableColumnObject[$keyJoin . "(" . $key . ")"]['dataType'] = "Dimension";
                                    } else {
                                        if ($tableColumnObject[$keyJoin . "(" . $key . ")"]['tableName'] != $key) {
                                            $tableColumnObject[$keyJoin . "(" . $key . ")"] = [];
                                            $tableColumnObject[$keyJoin . "(" . $key . ")"]['tableName'] = $key;
                                            $tableColumnObject[$keyJoin . "(" . $key . ")"]['Field'] = $keyJoin . "(" . $key . ")";
                                            $tableColumnObject[$keyJoin . "(" . $key . ")"]['Type'] = $val;
                                            $tableColumnObject[$keyJoin . "(" . $key . ")"]['dataType'] = "Dimension";
                                        }
                                    }
                                }
                                if (isset($tableColumnObject[$keyJoin . "(" . $key . ")"])) {
                                    $singleData[$keyJoin . "(" . $key . ")"] = $valueJoin;
                                } else {
                                    $singleData[$keyJoin . "(" . $key . ")"] = $valueJoin;
                                }
                            }
                        }
                    } else {
                        $val = gettype($value);
                        if ($val == "string") {
                            $val = "text";
                        } else if ($val == "Null") {
                            $val = "text";
                        }
                        if ($val == "integer" || $val == "decimal" || $val == "double" || $val == "float") {
                            if ($val == "integer") {
                                $val = "int";
                            }
                            if (!isset($tableColumnObject[ $key . "(" . $metadataObject->rootTable . ")"])) {
                                $tableColumnObject[$key . "(" . $metadataObject->rootTable . ")"] = [];
                                $tableColumnObject[$key . "(" . $metadataObject->rootTable . ")"]['tableName'] = $metadataObject->rootTable;
                                $tableColumnObject[$key . "(" . $metadataObject->rootTable . ")"]['Field'] = $key . "(" . $metadataObject->rootTable . ")";
                                $tableColumnObject[$key . "(" . $metadataObject->rootTable . ")"]['Type'] = $val;
                                $tableColumnObject[$key . "(" . $metadataObject->rootTable . ")"]['dataType'] = "Measure";
                            }
                        } else {
                            if (!isset($tableColumnObject[$key . "(" . $metadataObject->rootTable . ")"])) {
                                $tableColumnObject[$key . "(" . $metadataObject->rootTable . ")"] = [];
                                $tableColumnObject[$key . "(" . $metadataObject->rootTable . ")"]['tableName'] = $metadataObject->rootTable;
                                $tableColumnObject[$key . "(" . $metadataObject->rootTable . ")"]['Field'] = $key . "(" . $metadataObject->rootTable . ")";
                                $tableColumnObject[$key . "(" . $metadataObject->rootTable . ")"]['Type'] = $val;
                                $tableColumnObject[$key . "(" . $metadataObject->rootTable . ")"]['dataType'] = "Dimension";
                            }
                        }
                        $singleData[$key . "(" . $metadataObject->rootTable . ")"] = $value;
                    }
                }
            }
            array_push($tableData,$singleData);
        }
        $this->result =  count($tableData);
        $this->errorCode = 1;
        $this->message = 'Successfully';
        return $this;
    }
    //  Cache data to redis
    public function queryDataToredisPublic($metadataObject,$incrementObj, $uid,$token){
        ini_set('memory_limit', '-1');
        set_time_limit(0);
        $data=array();
        $company_id=$uid;
        $incrementObj=[];
        if(!HelperFunctions::isCachedInRedis(0,$metadataObject->metadataId,$company_id,false)){
            try{
                $dbName=self::$conObject->dbname;
                $db=self::$con->$dbName;
                $rootTable=$metadataObject->rootTable;
                $data=$db->selectCollection($metadataObject->rootTable);
                $pipeline=[];
                if (isset($metadataObject->joinsDetails)) {
                    $joinTables=[];
                    foreach ($metadataObject->joinsDetails as $ros) {
                        foreach ($ros->node as $row) {
                            if (is_object(json_decode($row->leftColumn))) {
                                $leftColumn = json_decode($row->leftColumn);
                                $rightColumn = json_decode($row->rightColumn);
                                $joinLeftColumn = "`" . $leftColumn->tableName . "`.`" . $leftColumn->Field . "`";
                                $joinRightColumn = "`" . $rightColumn->tableName . "`.`" . $rightColumn->Field . "`";
                            } else {
                                $joinLeftColumn = "`" . $ros->leftColumnTable . "`.`" . $row->leftColumn . "`";
                                $joinRightColumn = "`" . $ros->leftColumnTable . "`.`" . $row->leftColumn . "`";
                            }
                            $leftColumn = trim(preg_replace('/\s\s+/', ' ', $joinLeftColumn));
                            $rightColumn = trim(preg_replace('/\s\s+/', ' ', $joinRightColumn));
                            $lookup['$lookup']=[];
                            $lookup['$lookup']['from']=$ros->rightColumnTable;
                            //json_decode($row->leftColumn)->tableName==$metadataObject->rootTable
                            //|| isset($metadataObject->type)
                            if($ros->leftColumnTable==$metadataObject->rootTable ){
                                $lookup['$lookup']['localField']=json_decode($row->leftColumn)->Field;
                            }
                            else{
                                $lookup['$lookup']['localField']=$ros->leftColumnTable  .".".json_decode($row->leftColumn)->Field;
                            }
                            $lookup['$lookup']['foreignField']=json_decode($row->rightColumn)->Field;
                            $lookup['$lookup']['as']=$ros->rightColumnTable;
                            $unwind=[];
                            $unwind['$unwind']="$".$ros->rightColumnTable;
                            array_push($pipeline,$lookup);
                            array_push($pipeline,$unwind);
                            array_push($joinTables,$ros->rightColumnTable);
                        }
                    }
                }

                if(isset($metadataObject->condition) && $metadataObject->condition){
                    //array( '$and' => array(array('customers_id'=> array('$gt'=>3) )))) greater condition
                    //Less then $lt
                    // for and $and
                    // for $or
                    // for join table table append
                    while(preg_match_all('/(\w*)(\(((\w+))\))+/',$metadataObject->condition,$out)){
                        $str=$out[3][0].".".$out[1][0];
                        $metadataObject->condition=str_replace($out[0][0],$str,$metadataObject->condition);
                    }
                    //$conditions = preg_split( "/ (and|AND|or|OR) /", $metadataObject->condition );
                    $whereArray=[];
                    preg_match_all('/((?:\w+.\w+|\w+)\s*(?:=|<|>|>=|<=)\s*(?:\'\w*\s*\w*\'|\w+))(\s*and\s*|\s*or\s*|)/',$metadataObject->condition, $outArray);

                    for($i=0;$i<count($outArray[1]);$i++){
                        if($outArray[1][$i])
                            array_push($whereArray,$outArray[1][$i]);
                        if($outArray[2][$i])
                            array_push($whereArray,$outArray[2][$i]);
                    }
                    $match=[];
                    $match['$match']=[];
                    $match['$match']['$or']=[];
                    $lastCondition='$or';
                    foreach ($whereArray as $condition){

                        $conditionExp=[];
                        $condition=ltrim($condition," ");
                        $condition=rtrim($condition," ");
                        if($condition=="and" || $condition=="or"){
                            $lastCondition='$'.$condition;
                        }else{
                            while(preg_match_all('/(\w+.\w+|\w+)(\s*=\s*|\s*<\s*|\s*>\s*|\s*<=\s*|\s*>=\s*)(\'\w*\s*\w*\'|\w+)/',$condition, $conditionOpr)){

                                $conditionOpr[3][0]=trim($conditionOpr[3][0],"'");
                                $conditionOpr[3][0]=ltrim($conditionOpr[3][0]," ");
                                $conditionOpr[3][0]=rtrim($conditionOpr[3][0]," ");
                                if(is_numeric($conditionOpr[3][0])){
                                    $conditionValue=intval($conditionOpr[3][0]);
                                }else{
                                    $conditionValue=$conditionOpr[3][0];
                                }
                                $conditionKey=trim($conditionOpr[1][0],"`");
                                $whereTemp=explode(".",$conditionOpr[1][0]);
                                if($whereTemp[0]==$rootTable){
                                    $conditionKey=$whereTemp[1];
                                }
                                if($conditionOpr[2][0]==">"){
                                    $conditionExp[$conditionKey]=array('$gt'=>$conditionValue);
                                }else if($conditionOpr[2][0]=="<"){
                                    $conditionExp[$conditionKey]=array('$lt'=>$conditionValue);
                                }else if($conditionOpr[2][0]==">="){
                                    $conditionExp[$conditionKey]=array('$gte'=>$conditionValue);
                                }else if($conditionOpr[2][0]=="<="){
                                    $conditionExp[$conditionKey]=array('$lte'=>$conditionValue);
                                }else{
                                    $conditionExp[$conditionKey]=$conditionValue;
                                }
                                $condition=str_replace($conditionOpr[0][0],"",$condition);
                            }
                            //if(count($conditionExp))
                            if(!isset($match['$match'][$lastCondition])){
                                $match['$match'][$lastCondition]=[];
                            }
                            if(isset($metadataObject->type)){
                                $p=[];
                                foreach($conditionExp as $key=>$value){
                                    $temp=explode(".",$key);
                                    $p[$temp[1]."(".$temp[0].")"]=$value;
                                }
                                array_push($match['$match'][$lastCondition],$p);
                            }else{
                                array_push($match['$match'][$lastCondition],$conditionExp);
                            }
                        }
                    }
                    //array('$match'=>array('$and'=>array(array('customers_id'=> 3))))
                    array_push($pipeline,$match);
                }
                //Check one more condition
                if(isset($metadataObject->exCondition) && $metadataObject->exCondition){
                    if(isset($metadataObject->type)) {
                        $match = [];
                        $match['$match'] = [];
                        $match['$match']['$or'] = [];
                        $lastCondition = '$or';
                        $conditionExp = [];
                        foreach ($metadataObject->exCondition as $conditions) {
                            foreach (json_decode($conditions) as $key => $condition) {
                                $whereArray = [];
                                preg_match_all('/((?:(\w+\s)*\w+.\w+|\w+)\s*(?:=|<|>|>=|<=)\s*(?:\'\w*\s*\w*\'|\w+))(\s*and\s*|\s*or\s*|)/', $condition, $outArray);
                                for ($i = 0; $i < count($outArray[1]); $i++) {
                                    if ($outArray[1][$i])
                                        array_push($whereArray, $outArray[1][$i]);
                                    if ($outArray[3][$i])
                                        array_push($whereArray, $outArray[3][$i]);
                                }
                                foreach ($whereArray as $condition) {
                                    $condition = ltrim($condition, " ");
                                    $condition = rtrim($condition, " ");
                                    if (preg_match("/^and/", $condition) != null || preg_match("/^or/", $condition) != null) {
                                        $lastCondition = '$' . $condition;
                                    } else {
                                        while (preg_match_all('/(?:((?:\w*\s)*\w+.\w+|\w+)\s*(=)\s*|\s*<\s*|\s*>\s*|\s*<=\s*|\s*>=\s*)(\'\w*\s*\w*\'|\w+)/', $condition, $conditionOpr)) {
                                            $conditionOpr[3][0] = trim($conditionOpr[3][0], "'");
                                            $conditionOpr[3][0] = ltrim($conditionOpr[3][0], " ");
                                            $conditionOpr[3][0] = rtrim($conditionOpr[3][0], " ");
                                            if (is_numeric($conditionOpr[3][0])) {
                                                $conditionValue = intval($conditionOpr[3][0]);
                                            } else {
                                                $conditionValue = $conditionOpr[3][0];
                                            }
                                            $conditionKey = trim($conditionOpr[1][0], "`");
                                            $whereTemp=explode(".",$conditionOpr[1][0]);
                                            if($whereTemp[0]==$rootTable){
                                                $conditionKey=$whereTemp[1];
                                            }
                                            if ($conditionOpr[2][0] == ">") {
                                                $conditionExp[$conditionKey] = array('$gt' => $conditionValue);
                                            } else if ($conditionOpr[2][0] == "<") {
                                                $conditionExp[$conditionKey] = array('$lt' => $conditionValue);
                                            } else if ($conditionOpr[2][0] == ">=") {
                                                $conditionExp[$conditionKey] = array('$gte' => $conditionValue);
                                            } else if ($conditionOpr[2][0] == "<=") {
                                                $conditionExp[$conditionKey] = array('$lte' => $conditionValue);
                                            } else {
                                                $conditionExp[$conditionKey] = $conditionValue;
                                            }
                                            $condition = str_replace($conditionOpr[0][0], "", $condition);
                                        }
                                        //if(count($conditionExp))
                                        if (!isset($match['$match'][$lastCondition])) {
                                            $match['$match'][$lastCondition] = [];
                                        }
                                        array_push($match['$match'][$lastCondition], $conditionExp);
                                    }
                                }
                            }
                        }
                        array_push($pipeline, $match);
                    }else{
                        $match=[];
                        $match['$match']=[];
                        $match['$match']['$or']=[];
                        $lastCondition='$or';
                        $conditionExp=[];
                        foreach($metadataObject->exCondition as $conditions){
                            foreach (json_decode($conditions) as $key=>$condition){
                                while(preg_match_all('/(\w*)(\(((\w+))\))+/',$condition,$out)){
                                    $str=$out[3][0].".".$out[1][0];
                                    $condition=str_replace($out[0][0],$str,$condition);
                                }
                            }
                            $whereArray=[];
                            //preg_match_all('/((?:\w+.\w+|\w+)\s*(?:=|<|>|>=|<=)\s*(?:\'\w*\s*\w*\'|\w+))(\s*and\s*|\s*or\s*|)/',$condition, $outArray);
                            preg_match_all('/((?:(\w*\s*\w*)+.\w+|\w+)\s*(?:=|<|>|>=|<=)\s*(?:\'\w*\s*\w*\'|\w+))(\s*and\s*|\s*or\s*|)/',$condition, $outArray);
                            for($i=0;$i<count($outArray[1]);$i++){
                                if($outArray[1][$i]){
                                    array_push($whereArray,$outArray[1][$i]);
                                }
                                if($outArray[2][$i]){
                                    array_push($whereArray,$outArray[2][$i]);
                                }
                            }
                            $condition=ltrim($condition," ");
                            $condition=rtrim($condition," ");
                            if($condition=="and" || $condition=="or"){
                                $lastCondition='$'.$condition;
                            }else{
                                while(preg_match_all('/(\w+.\w+|\w+)(\s*=\s*|\s*<\s*|\s*>\s*|\s*<=\s*|\s*>=\s*)(\'\w*\s*\w*\'|\w+)/',$condition, $conditionOpr)){
                                    $conditionOpr[3][0]=trim($conditionOpr[3][0],"'");
                                    $conditionOpr[3][0]=ltrim($conditionOpr[3][0]," ");
                                    $conditionOpr[3][0]=rtrim($conditionOpr[3][0]," ");
                                    if(is_numeric($conditionOpr[3][0])){
                                        $conditionValue=intval($conditionOpr[3][0]);
                                    }else{
                                        $conditionValue=$conditionOpr[3][0];
                                    }
                                    $conditionKey=trim($conditionOpr[1][0],"`");
                                    $whereTemp=explode(".",$conditionOpr[1][0]);
                                    if($whereTemp[0]==$rootTable){
                                        $conditionKey=$whereTemp[1];
                                    }
                                    if($conditionOpr[2][0]==">"){
                                        $conditionExp[$conditionKey]=array('$gt'=>$conditionValue);
                                    }else if($conditionOpr[2][0]=="<"){
                                        $conditionExp[$conditionKey]=array('$lt'=>$conditionValue);
                                    }else if($conditionOpr[2][0]==">="){
                                        $conditionExp[$conditionKey]=array('$gte'=>$conditionValue);
                                    }else if($conditionOpr[2][0]=="<="){
                                        $conditionExp[$conditionKey]=array('$lte'=>$conditionValue);
                                    }else{
                                        $conditionExp[$conditionKey]=$conditionValue;
                                    }
                                    $condition=str_replace($conditionOpr[0][0],"",$condition);
                                }
                                //if(count($conditionExp))
                                if(!isset($match['$match'][$lastCondition])){
                                    $match['$match'][$lastCondition]=[];
                                }
                                array_push($match['$match'][$lastCondition],$conditionExp);
                            }
                        }
                        array_push($pipeline,$match);
                    }
                }
                if(isset($metadataObject->limit) && $metadataObject->limit){
                    if($metadataObject->limit<100)
                        array_push($pipeline, array('$limit' => intval($metadataObject->limit)));
                    else
                        array_push($pipeline, array('$limit' => 100));
                }
                $list= $data->aggregate($pipeline);
                $tableColumnObject=[];
                $tableData=[];
                foreach ($list as $lists)
                {
                    $singleData=[];
                    foreach($lists as $key => $value)
                    {
                        if($key!='_id') {
                            if (in_array($key, $joinTables)) {
                                foreach ($value as $keyJoin => $valueJoin) {
                                    if($keyJoin!="_id") {
                                        $val = gettype($valueJoin);
                                        if ($val == "string") {
                                            $val = "text";
                                        } else if ($val == "Null") {
                                            $val = "text";
                                        }
                                        if ($val == "integer" || $val == "decimal" || $val == "double" || $val == "float") {
                                            if ($val == "integer") {
                                                $val = "int";
                                            }
                                            if (!isset($tableColumnObject[$keyJoin . "(" . $key . ")"])) {
                                                $tableColumnObject[$keyJoin . "(" . $key . ")"] = [];
                                                $tableColumnObject[$keyJoin . "(" . $key . ")"]['tableName'] = $key;
                                                $tableColumnObject[$keyJoin . "(" . $key . ")"]['Field'] = $keyJoin . "(" . $key . ")";
                                                $tableColumnObject[$keyJoin . "(" . $key . ")"]['Type'] = $val;
                                                $tableColumnObject[$keyJoin . "(" . $key . ")"]['dataType'] = "Measure";
                                            } else {
                                                if ($tableColumnObject[$keyJoin . "(" . $key . ")"]['tableName'] != $key) {
                                                    $tableColumnObject[$keyJoin . "(" . $key . ")"] = [];
                                                    $tableColumnObject[$keyJoin . "(" . $key . ")"]['tableName'] = $key;
                                                    $tableColumnObject[$keyJoin . "(" . $key . ")"]['Field'] = $keyJoin . "(" . $key . ")";
                                                    $tableColumnObject[$keyJoin . "(" . $key . ")"]['Type'] = $val;
                                                    $tableColumnObject[$keyJoin . "(" . $key . ")"]['dataType'] = "Measure";
                                                }
                                            }
                                        } else {
                                            if (!isset($tableColumnObject[$keyJoin . "(" . $key . ")"])) {
                                                $tableColumnObject[$keyJoin . "(" . $key . ")"] = [];
                                                $tableColumnObject[$keyJoin . "(" . $key . ")"]['tableName'] = $key;
                                                $tableColumnObject[$keyJoin . "(" . $key . ")"]['Field'] =  $keyJoin . "(" . $key . ")";
                                                $tableColumnObject[$keyJoin . "(" . $key . ")"]['Type'] = $val;
                                                $tableColumnObject[$keyJoin . "(" . $key . ")"]['dataType'] = "Dimension";
                                            } else {
                                                if ($tableColumnObject[$keyJoin . "(" . $key . ")"]['tableName'] != $key) {
                                                    $tableColumnObject[$keyJoin . "(" . $key . ")"] = [];
                                                    $tableColumnObject[$keyJoin . "(" . $key . ")"]['tableName'] = $key;
                                                    $tableColumnObject[$keyJoin . "(" . $key . ")"]['Field'] = $keyJoin . "(" . $key . ")";
                                                    $tableColumnObject[$keyJoin . "(" . $key . ")"]['Type'] = $val;
                                                    $tableColumnObject[$keyJoin . "(" . $key . ")"]['dataType'] = "Dimension";
                                                }
                                            }
                                        }
                                        if (isset($tableColumnObject[$keyJoin . "(" . $key . ")"])) {
                                            $singleData[$keyJoin . "(" . $key . ")"] = $valueJoin;
                                        } else {
                                            $singleData[$keyJoin . "(" . $key . ")"] = $valueJoin;
                                        }
                                    }
                                }
                            } else {
                                $val = gettype($value);
                                if ($val == "string") {
                                    $val = "text";
                                } else if ($val == "Null") {
                                    $val = "text";
                                }
                                if ($val == "integer" || $val == "decimal" || $val == "double" || $val == "float") {
                                    if ($val == "integer") {
                                        $val = "int";
                                    }
                                    if (!isset($tableColumnObject[ $key . "(" . $metadataObject->rootTable . ")"])) {
                                        $tableColumnObject[$key . "(" . $metadataObject->rootTable . ")"] = [];
                                        $tableColumnObject[$key . "(" . $metadataObject->rootTable . ")"]['tableName'] = $metadataObject->rootTable;
                                        $tableColumnObject[$key . "(" . $metadataObject->rootTable . ")"]['Field'] = $key . "(" . $metadataObject->rootTable . ")";
                                        $tableColumnObject[$key . "(" . $metadataObject->rootTable . ")"]['Type'] = $val;
                                        $tableColumnObject[$key . "(" . $metadataObject->rootTable . ")"]['dataType'] = "Measure";
                                    }
                                } else {
                                    if (!isset($tableColumnObject[$key . "(" . $metadataObject->rootTable . ")"])) {
                                        $tableColumnObject[$key . "(" . $metadataObject->rootTable . ")"] = [];
                                        $tableColumnObject[$key . "(" . $metadataObject->rootTable . ")"]['tableName'] = $metadataObject->rootTable;
                                        $tableColumnObject[$key . "(" . $metadataObject->rootTable . ")"]['Field'] = $key . "(" . $metadataObject->rootTable . ")";
                                        $tableColumnObject[$key . "(" . $metadataObject->rootTable . ")"]['Type'] = $val;
                                        $tableColumnObject[$key . "(" . $metadataObject->rootTable . ")"]['dataType'] = "Dimension";
                                    }
                                }
                                $singleData[$key . "(" . $metadataObject->rootTable . ")"] = $value;
                            }
                        }
                    }
                    array_push($tableData,$singleData);
                }
                $this->result =  array("tableColumn" => json_encode($tableColumnObject),"tableData" => json_encode($tableData));
                $this->errorCode = 1;
                $this->message = 'Successfully';
                $data=HelperFunctions::writeToRedis(0, $metadataObject->metadataId, $this->result, $incrementObj, $company_id,$token,1,"","",false,false);
            }catch (Exception $ex){

                $this->errorCode = 0;
                $this->message = $ex->getMessage();
                $this->result = "";
            }
        }else{
            $data=HelperFunctions::writeToRedis(0, $metadataObject->metadataId, $this, $incrementObj, $company_id,$token,0,"","",false,false);
            $this->redisAddData("0:".$metadataObject->metadataId.":".$company_id, $metadataObject, $incrementObj,$token,0,false);
        }
        try{
            $data['result']['tableColumn']=json_encode($data['result']['tableColumn']);
        }catch (Exception $e){
//dd($e->getMessage());
        }

        return $data;
    }
    public function redisAddData($client, $metadata, $incrementObj,$token,$clientId,$realTimeFlag){
        $redis = Redis::connection();
        /*
         * Get All Data
         */
        //$allKeys=$redis->keys("*");
        $metadataObj=[];
        /*
         * Get all metadata
         */
        $metadataObj[$client]=$metadata;
        /*
         * Check length and get data
         */
        foreach ($metadataObj as $key=>$metadataVal){
            $key_meta=$key.":".$token;
            $lengthObj=$redis->hgetAll("metadataDetails");
            $totalLength=$lengthObj[$key_meta];
            if($realTimeFlag){
                $key=$key."-realtime";
            }
            if(isset($lengthObj[$key_meta])) {
                $metadataDetailsObj = json_decode($lengthObj[$key_meta]);
                $length = json_decode($lengthObj[$key_meta])->length;
                $dataLength = self::checkLength($metadataVal, $metadataDetailsObj, $incrementObj);
                if ($incrementObj && $dataLength) {
                    $data = self::getLimitData($metadataVal, $metadataDetailsObj, $dataLength, $incrementObj);
                    $updatedKey = [];
                    $incrementObj = json_decode($incrementObj);
                    $tempArr = [];
                    $lastData = "";
                    foreach ($data as $dt) {
                        $dt = (object)$dt;
                        $selectedKeys = [];
                        foreach ($incrementObj->selected_Key as $keyObj) {
                            $keyObj = json_decode($keyObj);
                            array_push($selectedKeys, $keyObj->Field . "(" . $keyObj->tableName . ")");
                        }
                        $setKey = "";
                        foreach ($selectedKeys as $selectedKey) {
                            $setKey .= $dt->$selectedKey;
                        }
                        array_push($updatedKey, $setKey);
                        $lastData = (array)$dt;
                        foreach ($incrementObj->table as $table) {
                            if (isset($table->key)) {
                                if (!isset($tempArr[$table->name])) {
                                    $tempArr[$table->name] = $lastData[$table->key . "(" . $table->name . ")"];
                                } else if (strtotime($lastData[$table->key . "(" . $table->name . ")"]) > strtotime($tempArr[$table->name])) {
                                    $tempArr[$table->name] = $lastData[$table->key . "(" . $table->name . ")"];
                                }
                            }

                        }
                        $redis->hset($key, $setKey, json_encode($dt));
                    }
                    $metadataDetailsObj->length = $dataLength;
                    $metadataDetailsObj->updated_at = $tempArr;
                    $metadataDetailsObj->updated_key = $updatedKey;
                    $redis->hset("metadataDetails", $key_meta, json_encode($metadataDetailsObj));
                } else if ($dataLength > $length) {
                    $totalLength=$lengthObj[$key];
                    $data = self::getLimitData($metadataVal, $metadataDetailsObj, $dataLength, "");
                    $i=1;
                    $newKeyCreated=[];
                    foreach ($data as $dt) {
                        $newKey=$totalLength + $i;
                        array_push($newKeyCreated,$newKey);
                        $redis->hset($key,$newKey , json_encode($dt));
                        $i++;
                    }
                    $metadataDetailsObj->length = $dataLength;
                    $metadataDetailsObj->newCreatedKey = $newKeyCreated;
                    $redis->hset("metadataDetails", $key_meta, json_encode($metadataDetailsObj));
                }
            }
        }
    }
    /*
     * Check length
     */
    public static function checkLength($metadataObject,$metadataDetailsObj,$incrementObj){
        /*
         * Check length
         */
        $limit=$metadataDetailsObj->length;
        if(isset($metadataObject->type) && $metadataObject->type=="blending"){
            $connection=[];
            $connection['datasourceType']=env('MDB_CONNECTION','mongodb');
            $connection['dbname']=env('MDB_DATABASE','commondb');
            $connection['host']=env('MDB_HOST','101.53.130.66');
            $connection['port']=env('MDB_PORT','27017');
            $connection['username']=env('MDB_USERNAME','admin');
            $connection['password']=env('MDB_PASSWORD','hellothinklayer1');
            $metadataObject->connectionObject=(Object)$connection;
        }
        $db=(new MongoDB())->connect($metadataObject->connectionObject);


        /*
         * Query to get data
         */
        $dbName=self::$conObject->dbname;
        $db=self::$con->$dbName;
        $rootTable=$metadataObject->rootTable;
        $data=$db->selectCollection($metadataObject->rootTable);
        $pipeline=[];
//dd($metadataObject->joinsDetails);
        if (isset($metadataObject->joinsDetails)) {
            $joinTables=[];
            foreach ($metadataObject->joinsDetails as $ros) {
                foreach ($ros->node as $row) {
                    if (is_object(json_decode($row->leftColumn))) {
                        $leftColumn = json_decode($row->leftColumn);
                        $rightColumn = json_decode($row->rightColumn);
                        $joinLeftColumn = "`" . $leftColumn->tableName . "`.`" . $leftColumn->Field . "`";
                        $joinRightColumn = "`" . $rightColumn->tableName . "`.`" . $rightColumn->Field . "`";
                    } else {
                        $joinLeftColumn = "`" . $ros->leftColumnTable . "`.`" . $row->leftColumn . "`";
                        $joinRightColumn = "`" . $ros->leftColumnTable . "`.`" . $row->leftColumn . "`";
                    }
                    $leftColumn = trim(preg_replace('/\s\s+/', ' ', $joinLeftColumn));
                    $rightColumn = trim(preg_replace('/\s\s+/', ' ', $joinRightColumn));
                    $lookup['$lookup']=[];
                    $lookup['$lookup']['from']=$ros->rightColumnTable;
                    //json_decode($row->leftColumn)->tableName==$metadataObject->rootTable
                    //|| isset($metadataObject->type)
                    if($ros->leftColumnTable==$metadataObject->rootTable ){
                        $lookup['$lookup']['localField']=json_decode($row->leftColumn)->Field;
                    }
                    else{
                        $lookup['$lookup']['localField']=$ros->leftColumnTable  .".".json_decode($row->leftColumn)->Field;
                    }
                    $lookup['$lookup']['foreignField']=json_decode($row->rightColumn)->Field;
                    $lookup['$lookup']['as']=$ros->rightColumnTable;
                    $unwind=[];
                    $unwind['$unwind']="$".$ros->rightColumnTable;
                    array_push($pipeline,$lookup);
                    array_push($pipeline,$unwind);
                    array_push($joinTables,$ros->rightColumnTable);
                }
            }
        }

        if(isset($metadataObject->condition) && $metadataObject->condition){

            //array( '$and' => array(array('customers_id'=> array('$gt'=>3) )))) greater condition
            //Less then $lt
            // for and $and
            // for $or
            // for join table table append
            while(preg_match_all('/(\w*)(\(((\w+))\))+/',$metadataObject->condition,$out)){
                $str=$out[3][0].".".$out[1][0];
                $metadataObject->condition=str_replace($out[0][0],$str,$metadataObject->condition);
            }
            //$conditions = preg_split( "/ (and|AND|or|OR) /", $metadataObject->condition );
            $whereArray=[];
            preg_match_all('/((?:\w+.\w+|\w+)\s*(?:=|<|>|>=|<=)\s*(?:\'\w*\s*\w*\'|\w+))(\s*and\s*|\s*or\s*|)/',$metadataObject->condition, $outArray);

            for($i=0;$i<count($outArray[1]);$i++){
                if($outArray[1][$i])
                    array_push($whereArray,$outArray[1][$i]);
                if($outArray[2][$i])
                    array_push($whereArray,$outArray[2][$i]);
            }
            $match=[];
            $match['$match']=[];
            $match['$match']['$or']=[];
            $lastCondition='$or';
            foreach ($whereArray as $condition){

                $conditionExp=[];
                $condition=ltrim($condition," ");
                $condition=rtrim($condition," ");
                if($condition=="and" || $condition=="or"){
                    $lastCondition='$'.$condition;
                }else{
                    while(preg_match_all('/(\w+.\w+|\w+)(\s*=\s*|\s*<\s*|\s*>\s*|\s*<=\s*|\s*>=\s*)(\'\w*\s*\w*\'|\w+)/',$condition, $conditionOpr)){

                        $conditionOpr[3][0]=trim($conditionOpr[3][0],"'");
                        $conditionOpr[3][0]=ltrim($conditionOpr[3][0]," ");
                        $conditionOpr[3][0]=rtrim($conditionOpr[3][0]," ");
                        if(is_numeric($conditionOpr[3][0])){
                            $conditionValue=intval($conditionOpr[3][0]);
                        }else{
                            $conditionValue=$conditionOpr[3][0];
                        }
                        $conditionKey=trim($conditionOpr[1][0],"`");
                        if($conditionOpr[2][0]==">"){
                            $conditionExp[$conditionKey]=array('$gt'=>$conditionValue);
                        }else if($conditionOpr[2][0]=="<"){
                            $conditionExp[$conditionKey]=array('$lt'=>$conditionValue);
                        }else if($conditionOpr[2][0]==">="){
                            $conditionExp[$conditionKey]=array('$gte'=>$conditionValue);
                        }else if($conditionOpr[2][0]=="<="){
                            $conditionExp[$conditionKey]=array('$lte'=>$conditionValue);
                        }else{
                            $conditionExp[$conditionKey]=$conditionValue;
                        }
                        $condition=str_replace($conditionOpr[0][0],"",$condition);
                    }
                    //if(count($conditionExp))
                    if(!isset($match['$match'][$lastCondition])){
                        $match['$match'][$lastCondition]=[];
                    }
                    if(isset($metadataObject->type)){
                        $p=[];
                        foreach($conditionExp as $key=>$value){
                            $temp=explode(".",$key);
                            $p[$temp[1]."(".$temp[0].")"]=$value;
                        }
                        array_push($match['$match'][$lastCondition],$p);
                    }else{
                        array_push($match['$match'][$lastCondition],$conditionExp);
                    }
                }
            }
            //array('$match'=>array('$and'=>array(array('customers_id'=> 3))))
            array_push($pipeline,$match);
        }
        //Check one more condition

        if(isset($metadataObject->exCondition) && $metadataObject->exCondition){
            if(isset($metadataObject->type)) {
                $match = [];
                $match['$match'] = [];
                $match['$match']['$or'] = [];
                $lastCondition = '$or';
                $conditionExp = [];
                foreach ($metadataObject->exCondition as $conditions) {
                    foreach (json_decode($conditions) as $key => $condition) {
                        $whereArray = [];
                        preg_match_all('/((?:(\w+\s)*\w+.\w+|\w+)\s*(?:=|<|>|>=|<=)\s*(?:\'\w*\s*\w*\'|\w+))(\s*and\s*|\s*or\s*|)/', $condition, $outArray);
                        for ($i = 0; $i < count($outArray[1]); $i++) {
                            if ($outArray[1][$i])
                                array_push($whereArray, $outArray[1][$i]);
                            if ($outArray[3][$i])
                                array_push($whereArray, $outArray[3][$i]);
                        }
                        foreach ($whereArray as $condition) {
                            $condition = ltrim($condition, " ");
                            $condition = rtrim($condition, " ");
                            if (preg_match("/^and/", $condition) != null || preg_match("/^or/", $condition) != null) {
                                $lastCondition = '$' . $condition;
                            } else {
                                while (preg_match_all('/(?:((?:\w*\s)*\w+.\w+|\w+)\s*(=)\s*|\s*<\s*|\s*>\s*|\s*<=\s*|\s*>=\s*)(\'\w*\s*\w*\'|\w+)/', $condition, $conditionOpr)) {
                                    $conditionOpr[3][0] = trim($conditionOpr[3][0], "'");
                                    $conditionOpr[3][0] = ltrim($conditionOpr[3][0], " ");
                                    $conditionOpr[3][0] = rtrim($conditionOpr[3][0], " ");
                                    if (is_numeric($conditionOpr[3][0])) {
                                        $conditionValue = intval($conditionOpr[3][0]);
                                    } else {
                                        $conditionValue = $conditionOpr[3][0];
                                    }
                                    $conditionKey = trim($conditionOpr[1][0], "`");
                                    $whereTemp=explode(".",$conditionOpr[1][0]);
                                    if($whereTemp[0]==$rootTable){
                                        $conditionKey=$whereTemp[1];
                                    }
                                    if ($conditionOpr[2][0] == ">") {
                                        $conditionExp[$conditionKey] = array('$gt' => $conditionValue);
                                    } else if ($conditionOpr[2][0] == "<") {
                                        $conditionExp[$conditionKey] = array('$lt' => $conditionValue);
                                    } else if ($conditionOpr[2][0] == ">=") {
                                        $conditionExp[$conditionKey] = array('$gte' => $conditionValue);
                                    } else if ($conditionOpr[2][0] == "<=") {
                                        $conditionExp[$conditionKey] = array('$lte' => $conditionValue);
                                    } else {
                                        $conditionExp[$conditionKey] = $conditionValue;
                                    }
                                    $condition = str_replace($conditionOpr[0][0], "", $condition);
                                }
                                //if(count($conditionExp))
                                if (!isset($match['$match'][$lastCondition])) {
                                    $match['$match'][$lastCondition] = [];
                                }
                                array_push($match['$match'][$lastCondition], $conditionExp);
                            }
                        }
                    }
                }
                array_push($pipeline, $match);
            }else{
                $match=[];
                $match['$match']=[];
                $match['$match']['$or']=[];
                $lastCondition='$or';
                $conditionExp=[];
                foreach($metadataObject->exCondition as $conditions){
                    foreach (json_decode($conditions) as $key=>$condition){
                        while(preg_match_all('/(\w*)(\(((\w+))\))+/',$condition,$out)){
                            $str=$out[3][0].".".$out[1][0];
                            $condition=str_replace($out[0][0],$str,$condition);
                        }
                    }

                    $whereArray=[];
                    //preg_match_all('/((?:\w+.\w+|\w+)\s*(?:=|<|>|>=|<=)\s*(?:\'\w*\s*\w*\'|\w+))(\s*and\s*|\s*or\s*|)/',$condition, $outArray);
                    preg_match_all('/((?:(\w*\s*\w*)+.\w+|\w+)\s*(?:=|<|>|>=|<=)\s*(?:\'\w*\s*\w*\'|\w+))(\s*and\s*|\s*or\s*|)/',$condition, $outArray);
                    for($i=0;$i<count($outArray[1]);$i++){
                        if($outArray[1][$i])
                            array_push($whereArray,$outArray[1][$i]);
                        if($outArray[2][$i])
                            array_push($whereArray,$outArray[2][$i]);
                    }
                    $condition=ltrim($condition," ");
                    $condition=rtrim($condition," ");
                    if($condition=="and" || $condition=="or"){
                        $lastCondition='$'.$condition;
                    }else{
                        while(preg_match_all('/(\w+.\w+|\w+)(\s*=\s*|\s*<\s*|\s*>\s*|\s*<=\s*|\s*>=\s*)(\'\w*\s*\w*\'|\w+)/',$condition, $conditionOpr)){
                            $conditionOpr[3][0]=trim($conditionOpr[3][0],"'");
                            $conditionOpr[3][0]=ltrim($conditionOpr[3][0]," ");
                            $conditionOpr[3][0]=rtrim($conditionOpr[3][0]," ");
                            if(is_numeric($conditionOpr[3][0])){
                                $conditionValue=intval($conditionOpr[3][0]);
                            }else{
                                $conditionValue=$conditionOpr[3][0];
                            }
                            $conditionKey=trim($conditionOpr[1][0],"`");
                            $whereTemp=explode(".",$conditionOpr[1][0]);
                            if($whereTemp[0]==$rootTable){
                                $conditionKey=$whereTemp[1];
                            }
                            if($conditionOpr[2][0]==">"){
                                $conditionExp[$conditionKey]=array('$gt'=>$conditionValue);
                            }else if($conditionOpr[2][0]=="<"){
                                $conditionExp[$conditionKey]=array('$lt'=>$conditionValue);
                            }else if($conditionOpr[2][0]==">="){
                                $conditionExp[$conditionKey]=array('$gte'=>$conditionValue);
                            }else if($conditionOpr[2][0]=="<="){
                                $conditionExp[$conditionKey]=array('$lte'=>$conditionValue);
                            }else{
                                $conditionExp[$conditionKey]=$conditionValue;
                            }
                            $condition=str_replace($conditionOpr[0][0],"",$condition);
                        }
                        //if(count($conditionExp))
                        if(!isset($match['$match'][$lastCondition])){
                            $match['$match'][$lastCondition]=[];
                        }
                        array_push($match['$match'][$lastCondition],$conditionExp);
                    }
                }
                array_push($pipeline,$match);
            }
        }
        if($incrementObj){
            $incrementObj=json_decode($incrementObj);
            $i=0;
            $conditionExp=[];
            $match=[];
            $match['$match']=[];
            $match['$match']['$and']=[];
            foreach ($incrementObj->table as $table){
                $tableName=$table->name;
                if(isset($metadataDetailsObj->updated_at->$tableName)){
                    $dateTime=$metadataDetailsObj->updated_at->$tableName;
                    $conditionExp[$table->key]=array('$gt'=>$dateTime);
                }
                if($conditionExp && count($conditionExp)){
                    if(!isset($match['$match']['$and'])){
                        $match['$match']['$and']=[];
                    }
                    array_push($match['$match']['$and'],$conditionExp);
                }
            }
            if($conditionExp && count($conditionExp)){
		        array_push($pipeline,$match);
            }
        }
        if(isset($metadataObject->limit) && $metadataObject->limit && !$incrementObj){
            if($metadataObject->limit<100)
                array_push($pipeline, array('$limit' => intval($metadataObject->limit)));
            else
                array_push($pipeline, array('$limit' => 100));
        }
        //dd($pipeline);
        $list= $data->aggregate($pipeline);
        $tableColumnObject=[];
        $tableData=[];
        foreach ($list as $lists)
        {
            $singleData=[];
            foreach($lists as $key => $value)
            {
                if($key!='_id') {
                    if (in_array($key, $joinTables)) {
                        foreach ($value as $keyJoin => $valueJoin) {
                            if($keyJoin!="_id") {
                                $val = gettype($valueJoin);
                                if ($val == "string") {
                                    $val = "text";
                                } else if ($val == "Null") {
                                    $val = "text";
                                }
                                if ($val == "integer" || $val == "decimal" || $val == "double" || $val == "float") {
                                    if ($val == "integer") {
                                        $val = "int";
                                    }
                                    if (!isset($tableColumnObject[$keyJoin . "(" . $key . ")"])) {
                                        $tableColumnObject[$keyJoin . "(" . $key . ")"] = [];
                                        $tableColumnObject[$keyJoin . "(" . $key . ")"]['tableName'] = $key;
                                        $tableColumnObject[$keyJoin . "(" . $key . ")"]['Field'] = $keyJoin . "(" . $key . ")";
                                        $tableColumnObject[$keyJoin . "(" . $key . ")"]['Type'] = $val;
                                        $tableColumnObject[$keyJoin . "(" . $key . ")"]['dataType'] = "Measure";
                                    } else {
                                        if ($tableColumnObject[$keyJoin . "(" . $key . ")"]['tableName'] != $key) {
                                            $tableColumnObject[$keyJoin . "(" . $key . ")"] = [];
                                            $tableColumnObject[$keyJoin . "(" . $key . ")"]['tableName'] = $key;
                                            $tableColumnObject[$keyJoin . "(" . $key . ")"]['Field'] = $keyJoin . "(" . $key . ")";
                                            $tableColumnObject[$keyJoin . "(" . $key . ")"]['Type'] = $val;
                                            $tableColumnObject[$keyJoin . "(" . $key . ")"]['dataType'] = "Measure";
                                        }
                                    }
                                } else {
                                    if (!isset($tableColumnObject[$keyJoin . "(" . $key . ")"])) {
                                        $tableColumnObject[$keyJoin . "(" . $key . ")"] = [];
                                        $tableColumnObject[$keyJoin . "(" . $key . ")"]['tableName'] = $key;
                                        $tableColumnObject[$keyJoin . "(" . $key . ")"]['Field'] =  $keyJoin . "(" . $key . ")";
                                        $tableColumnObject[$keyJoin . "(" . $key . ")"]['Type'] = $val;
                                        $tableColumnObject[$keyJoin . "(" . $key . ")"]['dataType'] = "Dimension";
                                    } else {
                                        if ($tableColumnObject[$keyJoin . "(" . $key . ")"]['tableName'] != $key) {
                                            $tableColumnObject[$keyJoin . "(" . $key . ")"] = [];
                                            $tableColumnObject[$keyJoin . "(" . $key . ")"]['tableName'] = $key;
                                            $tableColumnObject[$keyJoin . "(" . $key . ")"]['Field'] = $keyJoin . "(" . $key . ")";
                                            $tableColumnObject[$keyJoin . "(" . $key . ")"]['Type'] = $val;
                                            $tableColumnObject[$keyJoin . "(" . $key . ")"]['dataType'] = "Dimension";
                                        }
                                    }
                                }
                                if (isset($tableColumnObject[$keyJoin . "(" . $key . ")"])) {
                                    $singleData[$keyJoin . "(" . $key . ")"] = $valueJoin;
                                } else {
                                    $singleData[$keyJoin . "(" . $key . ")"] = $valueJoin;
                                }
                            }
                        }
                    } else {
                        $val = gettype($value);
                        if ($val == "string") {
                            $val = "text";
                        } else if ($val == "Null") {
                            $val = "text";
                        }
                        if ($val == "integer" || $val == "decimal" || $val == "double" || $val == "float") {
                            if ($val == "integer") {
                                $val = "int";
                            }
                            if (!isset($tableColumnObject[ $key . "(" . $metadataObject->rootTable . ")"])) {
                                $tableColumnObject[$key . "(" . $metadataObject->rootTable . ")"] = [];
                                $tableColumnObject[$key . "(" . $metadataObject->rootTable . ")"]['tableName'] = $metadataObject->rootTable;
                                $tableColumnObject[$key . "(" . $metadataObject->rootTable . ")"]['Field'] = $key . "(" . $metadataObject->rootTable . ")";
                                $tableColumnObject[$key . "(" . $metadataObject->rootTable . ")"]['Type'] = $val;
                                $tableColumnObject[$key . "(" . $metadataObject->rootTable . ")"]['dataType'] = "Measure";
                            }
                        } else {
                            if (!isset($tableColumnObject[$key . "(" . $metadataObject->rootTable . ")"])) {
                                $tableColumnObject[$key . "(" . $metadataObject->rootTable . ")"] = [];
                                $tableColumnObject[$key . "(" . $metadataObject->rootTable . ")"]['tableName'] = $metadataObject->rootTable;
                                $tableColumnObject[$key . "(" . $metadataObject->rootTable . ")"]['Field'] = $key . "(" . $metadataObject->rootTable . ")";
                                $tableColumnObject[$key . "(" . $metadataObject->rootTable . ")"]['Type'] = $val;
                                $tableColumnObject[$key . "(" . $metadataObject->rootTable . ")"]['dataType'] = "Dimension";
                            }
                        }
                        $singleData[$key . "(" . $metadataObject->rootTable . ")"] = $value;
                    }
                }
            }
            array_push($tableData,$singleData);
        }
        return count($tableData);
    }
    public static function getLimitData($metadataObject,$metadataObj,$currentLength,$incrementObj){
        $lastLength=$metadataObj->length;
        $dbName=self::$conObject->dbname;
        $db=self::$con->$dbName;
        $rootTable=$metadataObject->rootTable;
        $data=$db->selectCollection($metadataObject->rootTable);
        $pipeline=[];
        if (isset($metadataObject->joinsDetails)) {
            $joinTables=[];
            foreach ($metadataObject->joinsDetails as $ros) {
                foreach ($ros->node as $row) {
                    if (is_object(json_decode($row->leftColumn))) {
                        $leftColumn = json_decode($row->leftColumn);
                        $rightColumn = json_decode($row->rightColumn);
                        $joinLeftColumn = "`" . $leftColumn->tableName . "`.`" . $leftColumn->Field . "`";
                        $joinRightColumn = "`" . $rightColumn->tableName . "`.`" . $rightColumn->Field . "`";
                    } else {
                        $joinLeftColumn = "`" . $ros->leftColumnTable . "`.`" . $row->leftColumn . "`";
                        $joinRightColumn = "`" . $ros->leftColumnTable . "`.`" . $row->leftColumn . "`";
                    }
                    $leftColumn = trim(preg_replace('/\s\s+/', ' ', $joinLeftColumn));
                    $rightColumn = trim(preg_replace('/\s\s+/', ' ', $joinRightColumn));
                    $lookup['$lookup']=[];
                    $lookup['$lookup']['from']=$ros->rightColumnTable;
                    //json_decode($row->leftColumn)->tableName==$metadataObject->rootTable
                    //|| isset($metadataObject->type)
                    if($ros->leftColumnTable==$metadataObject->rootTable ){
                        $lookup['$lookup']['localField']=json_decode($row->leftColumn)->Field;
                    }
                    else{
                        $lookup['$lookup']['localField']=$ros->leftColumnTable  .".".json_decode($row->leftColumn)->Field;
                    }
                    $lookup['$lookup']['foreignField']=json_decode($row->rightColumn)->Field;
                    $lookup['$lookup']['as']=$ros->rightColumnTable;
                    $unwind=[];
                    $unwind['$unwind']="$".$ros->rightColumnTable;
                    array_push($pipeline,$lookup);
                    array_push($pipeline,$unwind);
                    array_push($joinTables,$ros->rightColumnTable);
                }
            }
        }

        if(isset($metadataObject->condition) && $metadataObject->condition){
            //array( '$and' => array(array('customers_id'=> array('$gt'=>3) )))) greater condition
            //Less then $lt
            // for and $and
            // for $or
            // for join table table append
            while(preg_match_all('/(\w*)(\(((\w+))\))+/',$metadataObject->condition,$out)){
                $str=$out[3][0].".".$out[1][0];
                $metadataObject->condition=str_replace($out[0][0],$str,$metadataObject->condition);
            }
            //$conditions = preg_split( "/ (and|AND|or|OR) /", $metadataObject->condition );
            $whereArray=[];
            preg_match_all('/((?:\w+.\w+|\w+)\s*(?:=|<|>|>=|<=)\s*(?:\'\w*\s*\w*\'|\w+))(\s*and\s*|\s*or\s*|)/',$metadataObject->condition, $outArray);

            for($i=0;$i<count($outArray[1]);$i++){
                if($outArray[1][$i])
                    array_push($whereArray,$outArray[1][$i]);
                if($outArray[2][$i])
                    array_push($whereArray,$outArray[2][$i]);
            }
            $match=[];
            $match['$match']=[];
            $match['$match']['$or']=[];
            $lastCondition='$or';
            foreach ($whereArray as $condition){

                $conditionExp=[];
                $condition=ltrim($condition," ");
                $condition=rtrim($condition," ");
                if($condition=="and" || $condition=="or"){
                    $lastCondition='$'.$condition;
                }else{
                    while(preg_match_all('/(\w+.\w+|\w+)(\s*=\s*|\s*<\s*|\s*>\s*|\s*<=\s*|\s*>=\s*)(\'\w*\s*\w*\'|\w+)/',$condition, $conditionOpr)){

                        $conditionOpr[3][0]=trim($conditionOpr[3][0],"'");
                        $conditionOpr[3][0]=ltrim($conditionOpr[3][0]," ");
                        $conditionOpr[3][0]=rtrim($conditionOpr[3][0]," ");
                        if(is_numeric($conditionOpr[3][0])){
                            $conditionValue=intval($conditionOpr[3][0]);
                        }else{
                            $conditionValue=$conditionOpr[3][0];
                        }
                        $conditionKey=trim($conditionOpr[1][0],"`");
                        if($conditionOpr[2][0]==">"){
                            $conditionExp[$conditionKey]=array('$gt'=>$conditionValue);
                        }else if($conditionOpr[2][0]=="<"){
                            $conditionExp[$conditionKey]=array('$lt'=>$conditionValue);
                        }else if($conditionOpr[2][0]==">="){
                            $conditionExp[$conditionKey]=array('$gte'=>$conditionValue);
                        }else if($conditionOpr[2][0]=="<="){
                            $conditionExp[$conditionKey]=array('$lte'=>$conditionValue);
                        }else{
                            $conditionExp[$conditionKey]=$conditionValue;
                        }
                        $condition=str_replace($conditionOpr[0][0],"",$condition);
                    }
                    //if(count($conditionExp))
                    if(!isset($match['$match'][$lastCondition])){
                        $match['$match'][$lastCondition]=[];
                    }
                    if(isset($metadataObject->type)){
                        $p=[];
                        foreach($conditionExp as $key=>$value){
                            $temp=explode(".",$key);
                            $p[$temp[1]."(".$temp[0].")"]=$value;
                        }
                        array_push($match['$match'][$lastCondition],$p);
                    }else{
                        array_push($match['$match'][$lastCondition],$conditionExp);
                    }
                }
            }
            //array('$match'=>array('$and'=>array(array('customers_id'=> 3))))
            array_push($pipeline,$match);
        }
        //Check one more condition
        if(isset($metadataObject->exCondition) && count($metadataObject->exCondition)){
            if(isset($metadataObject->type)) {
                $match = [];
                $match['$match'] = [];
                $match['$match']['$or'] = [];
                $lastCondition = '$or';
                $conditionExp = [];
                foreach ($metadataObject->exCondition as $conditions) {
                    foreach (json_decode($conditions) as $key => $condition) {
                        $whereArray = [];
                        preg_match_all('/((?:(\w+\s)*\w+.\w+|\w+)\s*(?:=|<|>|>=|<=)\s*(?:\'\w*\s*\w*\'|\w+))(\s*and\s*|\s*or\s*|)/', $condition, $outArray);
                        for ($i = 0; $i < count($outArray[1]); $i++) {
                            if ($outArray[1][$i])
                                array_push($whereArray, $outArray[1][$i]);
                            if ($outArray[3][$i])
                                array_push($whereArray, $outArray[3][$i]);
                        }
                        foreach ($whereArray as $condition) {
                            $condition = ltrim($condition, " ");
                            $condition = rtrim($condition, " ");
                            if (preg_match("/^and/", $condition) != null || preg_match("/^or/", $condition) != null) {
                                $lastCondition = '$' . $condition;
                            } else {
                                while (preg_match_all('/(?:((?:\w*\s)*\w+.\w+|\w+)\s*(=)\s*|\s*<\s*|\s*>\s*|\s*<=\s*|\s*>=\s*)(\'\w*\s*\w*\'|\w+)/', $condition, $conditionOpr)) {
                                    $conditionOpr[3][0] = trim($conditionOpr[3][0], "'");
                                    $conditionOpr[3][0] = ltrim($conditionOpr[3][0], " ");
                                    $conditionOpr[3][0] = rtrim($conditionOpr[3][0], " ");
                                    if (is_numeric($conditionOpr[3][0])) {
                                        $conditionValue = intval($conditionOpr[3][0]);
                                    } else {
                                        $conditionValue = $conditionOpr[3][0];
                                    }
                                    $conditionKey = trim($conditionOpr[1][0], "`");
                                    if ($conditionOpr[2][0] == ">") {
                                        $conditionExp[$conditionKey] = array('$gt' => $conditionValue);
                                    } else if ($conditionOpr[2][0] == "<") {
                                        $conditionExp[$conditionKey] = array('$lt' => $conditionValue);
                                    } else if ($conditionOpr[2][0] == ">=") {
                                        $conditionExp[$conditionKey] = array('$gte' => $conditionValue);
                                    } else if ($conditionOpr[2][0] == "<=") {
                                        $conditionExp[$conditionKey] = array('$lte' => $conditionValue);
                                    } else {
                                        $conditionExp[$conditionKey] = $conditionValue;
                                    }
                                    $condition = str_replace($conditionOpr[0][0], "", $condition);
                                }
                                //if(count($conditionExp))
                                if (!isset($match['$match'][$lastCondition])) {
                                    $match['$match'][$lastCondition] = [];
                                }
                                array_push($match['$match'][$lastCondition], $conditionExp);
                            }
                        }
                    }
                }
                array_push($pipeline, $match);
            }else{
                $match=[];
                $match['$match']=[];
                $match['$match']['$or']=[];
                $lastCondition='$or';
                $conditionExp=[];
                foreach($metadataObject->exCondition as $conditions){
                    foreach (json_decode($conditions) as $key=>$condition){
                        while(preg_match_all('/(\w*)(\(((\w+))\))+/',$condition,$out)){
                            $str=$out[3][0].".".$out[1][0];
                            $condition=str_replace($out[0][0],$str,$condition);
                        }
                    }

                    $whereArray=[];
                    //preg_match_all('/((?:\w+.\w+|\w+)\s*(?:=|<|>|>=|<=)\s*(?:\'\w*\s*\w*\'|\w+))(\s*and\s*|\s*or\s*|)/',$condition, $outArray);
                    preg_match_all('/((?:(\w*\s*\w*)+.\w+|\w+)\s*(?:=|<|>|>=|<=)\s*(?:\'\w*\s*\w*\'|\w+))(\s*and\s*|\s*or\s*|)/',$condition, $outArray);
                    for($i=0;$i<count($outArray[1]);$i++){
                        if($outArray[1][$i])
                            array_push($whereArray,$outArray[1][$i]);
                        if($outArray[2][$i])
                            array_push($whereArray,$outArray[2][$i]);
                    }
                    $condition=ltrim($condition," ");
                    $condition=rtrim($condition," ");
                    if($condition=="and" || $condition=="or"){
                        $lastCondition='$'.$condition;
                    }else{
                        while(preg_match_all('/(\w+.\w+|\w+)(\s*=\s*|\s*<\s*|\s*>\s*|\s*<=\s*|\s*>=\s*)(\'\w*\s*\w*\'|\w+)/',$condition, $conditionOpr)){
                            $conditionOpr[3][0]=trim($conditionOpr[3][0],"'");
                            $conditionOpr[3][0]=ltrim($conditionOpr[3][0]," ");
                            $conditionOpr[3][0]=rtrim($conditionOpr[3][0]," ");
                            if(is_numeric($conditionOpr[3][0])){
                                $conditionValue=intval($conditionOpr[3][0]);
                            }else{
                                $conditionValue=$conditionOpr[3][0];
                            }
                            $conditionKey=trim($conditionOpr[1][0],"`");
                            $whereTemp=explode(".",$conditionOpr[1][0]);
                            if($whereTemp[0]==$rootTable){
                                $conditionKey=$whereTemp[1];
                            }
                            if($conditionOpr[2][0]==">"){
                                $conditionExp[$conditionKey]=array('$gt'=>$conditionValue);
                            }else if($conditionOpr[2][0]=="<"){
                                $conditionExp[$conditionKey]=array('$lt'=>$conditionValue);
                            }else if($conditionOpr[2][0]==">="){
                                $conditionExp[$conditionKey]=array('$gte'=>$conditionValue);
                            }else if($conditionOpr[2][0]=="<="){
                                $conditionExp[$conditionKey]=array('$lte'=>$conditionValue);
                            }else{
                                $conditionExp[$conditionKey]=$conditionValue;
                            }
                            $condition=str_replace($conditionOpr[0][0],"",$condition);
                        }
                        //if(count($conditionExp))
                        if(!isset($match['$match'][$lastCondition])){
                            $match['$match'][$lastCondition]=[];
                        }
                        array_push($match['$match'][$lastCondition],$conditionExp);
                    }
                }
                array_push($pipeline,$match);
            }
        }
        /*if(isset($metadataObject->limit) && $metadataObject->limit){
            if($metadataObject->limit<100)
                array_push($pipeline, array('$limit' => intval($metadataObject->limit)));
            else
                array_push($pipeline, array('$limit' => 100));
        }*/
        //$lastLength.",".$currentLength
        /*
         * If metadata limit apply then incremental and realtime not work
         */
        if (isset($metadataObject->limit) && $metadataObject->limit) {
            return [];
        }
        if($incrementObj){
            $incrementObj=json_decode($incrementObj);
            $i=0;
            $conditionExp=[];
            $match=[];
            $match['$match']=[];
            $match['$match']['$and']=[];
            foreach ($incrementObj->table as $table){
                $tableName=$table->name;
                if(isset($metadataObj->updated_at->$tableName)){
                    $dateTime=$metadataObj->updated_at->$tableName;
                    $conditionExp[$table->key]=array('$gt'=>$dateTime);
                }
                if($conditionExp && count($conditionExp)){
                    if(!isset($match['$match']['$and'])){
                        $match['$match']['$and']=[];
                    }
                    array_push($match['$match']['$and'],$conditionExp);
                }
            }
            if($conditionExp && count($conditionExp)){
		        array_push($pipeline,$match);
            }
        }else{ 
            array_push($pipeline, array('$skip' => $lastLength));
        }
        $list= $data->aggregate($pipeline);

        $tableColumnObject=[];
        $tableData=[];
        foreach ($list as $lists)
        {
            $singleData=[];
            foreach($lists as $key => $value)
            {
                if($key!='_id') {
                    if (in_array($key, $joinTables)) {
                        foreach ($value as $keyJoin => $valueJoin) {
                            if($keyJoin!="_id") {
                                $val = gettype($valueJoin);
                                if ($val == "string") {
                                    $val = "text";
                                } else if ($val == "Null") {
                                    $val = "text";
                                }
                                if ($val == "integer" || $val == "decimal" || $val == "double" || $val == "float") {
                                    if ($val == "integer") {
                                        $val = "int";
                                    }
                                    if (!isset($tableColumnObject[$keyJoin . "(" . $key . ")"])) {
                                        $tableColumnObject[$keyJoin . "(" . $key . ")"] = [];
                                        $tableColumnObject[$keyJoin . "(" . $key . ")"]['tableName'] = $key;
                                        $tableColumnObject[$keyJoin . "(" . $key . ")"]['Field'] = $keyJoin . "(" . $key . ")";
                                        $tableColumnObject[$keyJoin . "(" . $key . ")"]['Type'] = $val;
                                        $tableColumnObject[$keyJoin . "(" . $key . ")"]['dataType'] = "Measure";
                                    } else {
                                        if ($tableColumnObject[$keyJoin . "(" . $key . ")"]['tableName'] != $key) {
                                            $tableColumnObject[$keyJoin . "(" . $key . ")"] = [];
                                            $tableColumnObject[$keyJoin . "(" . $key . ")"]['tableName'] = $key;
                                            $tableColumnObject[$keyJoin . "(" . $key . ")"]['Field'] = $keyJoin . "(" . $key . ")";
                                            $tableColumnObject[$keyJoin . "(" . $key . ")"]['Type'] = $val;
                                            $tableColumnObject[$keyJoin . "(" . $key . ")"]['dataType'] = "Measure";
                                        }
                                    }
                                } else {
                                    if (!isset($tableColumnObject[$keyJoin . "(" . $key . ")"])) {
                                        $tableColumnObject[$keyJoin . "(" . $key . ")"] = [];
                                        $tableColumnObject[$keyJoin . "(" . $key . ")"]['tableName'] = $key;
                                        $tableColumnObject[$keyJoin . "(" . $key . ")"]['Field'] =  $keyJoin . "(" . $key . ")";
                                        $tableColumnObject[$keyJoin . "(" . $key . ")"]['Type'] = $val;
                                        $tableColumnObject[$keyJoin . "(" . $key . ")"]['dataType'] = "Dimension";
                                    } else {
                                        if ($tableColumnObject[$keyJoin . "(" . $key . ")"]['tableName'] != $key) {
                                            $tableColumnObject[$keyJoin . "(" . $key . ")"] = [];
                                            $tableColumnObject[$keyJoin . "(" . $key . ")"]['tableName'] = $key;
                                            $tableColumnObject[$keyJoin . "(" . $key . ")"]['Field'] = $keyJoin . "(" . $key . ")";
                                            $tableColumnObject[$keyJoin . "(" . $key . ")"]['Type'] = $val;
                                            $tableColumnObject[$keyJoin . "(" . $key . ")"]['dataType'] = "Dimension";
                                        }
                                    }
                                }
                                if (isset($tableColumnObject[$keyJoin . "(" . $key . ")"])) {
                                    $singleData[$keyJoin . "(" . $key . ")"] = $valueJoin;
                                } else {
                                    $singleData[$keyJoin . "(" . $key . ")"] = $valueJoin;
                                }
                            }
                        }
                    } else {
                        $val = gettype($value);
                        if ($val == "string") {
                            $val = "text";
                        } else if ($val == "Null") {
                            $val = "text";
                        }
                        if ($val == "integer" || $val == "decimal" || $val == "double" || $val == "float") {
                            if ($val == "integer") {
                                $val = "int";
                            }
                            if (!isset($tableColumnObject[ $key . "(" . $metadataObject->rootTable . ")"])) {
                                $tableColumnObject[$key . "(" . $metadataObject->rootTable . ")"] = [];
                                $tableColumnObject[$key . "(" . $metadataObject->rootTable . ")"]['tableName'] = $metadataObject->rootTable;
                                $tableColumnObject[$key . "(" . $metadataObject->rootTable . ")"]['Field'] = $key . "(" . $metadataObject->rootTable . ")";
                                $tableColumnObject[$key . "(" . $metadataObject->rootTable . ")"]['Type'] = $val;
                                $tableColumnObject[$key . "(" . $metadataObject->rootTable . ")"]['dataType'] = "Measure";
                            }
                        } else {
                            if (!isset($tableColumnObject[$key . "(" . $metadataObject->rootTable . ")"])) {
                                $tableColumnObject[$key . "(" . $metadataObject->rootTable . ")"] = [];
                                $tableColumnObject[$key . "(" . $metadataObject->rootTable . ")"]['tableName'] = $metadataObject->rootTable;
                                $tableColumnObject[$key . "(" . $metadataObject->rootTable . ")"]['Field'] = $key . "(" . $metadataObject->rootTable . ")";
                                $tableColumnObject[$key . "(" . $metadataObject->rootTable . ")"]['Type'] = $val;
                                $tableColumnObject[$key . "(" . $metadataObject->rootTable . ")"]['dataType'] = "Dimension";
                            }
                        }
                        $singleData[$key . "(" . $metadataObject->rootTable . ")"] = $value;
                    }
                }
            }
            array_push($tableData,$singleData);
        }
        return $tableData;
    }
    public static function nodeToRedis($metadataObj, $redis, $incrementObj,$token,$metadataId,$companyId){
        foreach ($metadataObj as $key=>$metadataVal) {
            $key_meta=$key.":".$token;
            $lengthObj = $redis->hgetAll("metadataDetails");
            if (isset($lengthObj[$key_meta])) {
                $metadataDetailsObj = json_decode($lengthObj[$key_meta]);
                $length = json_decode($lengthObj[$key_meta])->length;
                $dataLength = self::checkLength($metadataVal, $metadataDetailsObj, $incrementObj);
                if ($incrementObj && $dataLength) {
                    $data = self::getLimitData($metadataVal, $metadataDetailsObj, $dataLength, $incrementObj);
                    $updatedKey = [];
                    $tempArr = [];
                    $incrementObj = json_decode($incrementObj);
                    foreach ($data as $dt) {
                        $dt = (object)$dt;
                        $selectedKeys = [];
                        foreach ($incrementObj->selected_Key as $keyObj) {
                            $keyObj = json_decode($keyObj);
                            array_push($selectedKeys, $keyObj->Field . "(" . $keyObj->tableName . ")");
                        }
                        $setKey = "";
                        foreach ($selectedKeys as $selectedKey) {
                            $setKey .= $dt->$selectedKey;
                        }
                        array_push($updatedKey, $setKey);
                        $lastData = (array)$dt;
                        foreach ($incrementObj->table as $table) {
                            if (isset($table->key)) {
                                if (!isset($tempArr[$table->name])) {
                                    $tempArr[$table->name] = $lastData[$table->key . "(" . $table->name . ")"];
                                } else if (strtotime($lastData[$table->key . "(" . $table->name . ")"]) > strtotime($tempArr[$table->name])) {
                                    $tempArr[$table->name] = $lastData[$table->key . "(" . $table->name . ")"];
                                }
                            }
                        }
                        $redis->hset("$metadataId:$companyId-realtime", $setKey, json_encode($dt));
                    }
                    $metadataDetailsObj->length = $dataLength;
                    $metadataDetailsObj->updated_at = $tempArr;
                    $metadataDetailsObj->updated_key = $updatedKey;
                    $redis->hset("metadataDetails", $key_meta, json_encode($metadataDetailsObj));
                    $incrementObj = $metadataDetailsObj;
                    $responseArr = array(
                        "incrementObjFlag" => true,
                        "flag" => true,
                        "incrementObj" => $incrementObj
                    );
                    return $responseArr;
                } else if ($dataLength > $length) {
                    $data = self::getLimitData($metadataVal, $metadataDetailsObj, $dataLength, "");
                    $i = 0;
                    foreach ($data as $dt) {
                        $dt = (object)$dt;
                        $redis->hset($key, $length + $i, json_encode($dt));
                        $i++;
                    }
                    $metadataDetailsObj->length = $dataLength;
                    $redis->hset("metadataDetails", $key_meta, json_encode($metadataDetailsObj));
                    $responseArr = array(
                        "incrementObj" => false,
                        "flag" => true
                    );
                    return $responseArr;
                } else {
                    $metadataDetailsObj->updated_key = [];
                    $redis->hset("metadataDetails", $key_meta, json_encode($metadataDetailsObj));
                }
                $responseArr = array(
                    "incrementObj" => false,
                    "flag" => false
                );
                return $responseArr;
            }
            $responseArr = array(
                "incrementObj" => false,
                "flag" => false
            );
            return $responseArr;
        }
    }
    /*
     * Blending data column and data
     */
    public function blendingDataColumn($metadataObject){
        $data=array();
        $dbName=self::$conObject->dbname;
        $db=self::$con->$dbName;
        $rootTable=$metadataObject->rootTable;
        $data=$db->selectCollection($metadataObject->rootTable);
        $pipeline=[];
        if (isset($metadataObject->joinsDetails)) {
            $joinTables=[];
            foreach ($metadataObject->joinsDetails as $ros) {
                foreach ($ros->node as $row) {
                    if (is_object(json_decode($row->leftColumn))) {
                        $leftColumn = json_decode($row->leftColumn);
                        $rightColumn = json_decode($row->rightColumn);
                        $joinLeftColumn = "`" . $leftColumn->tableName . "`.`" . $leftColumn->Field . "`";
                        $joinRightColumn = "`" . $rightColumn->tableName . "`.`" . $rightColumn->Field . "`";
                    } else {
                        $joinLeftColumn = "`" . $ros->leftColumnTable . "`.`" . $row->leftColumn . "`";
                        $joinRightColumn = "`" . $ros->leftColumnTable . "`.`" . $row->leftColumn . "`";
                    }
                    $leftColumn = trim(preg_replace('/\s\s+/', ' ', $joinLeftColumn));
                    $rightColumn = trim(preg_replace('/\s\s+/', ' ', $joinRightColumn));
                    $lookup['$lookup']=[];
                    $lookup['$lookup']['from']=$ros->rightColumnTable;
                    //json_decode($row->leftColumn)->tableName==$metadataObject->rootTable
                    //|| isset($metadataObject->type)
                    if($ros->leftColumnTable==$metadataObject->rootTable ){
                        $lookup['$lookup']['localField']=json_decode($row->leftColumn)->Field;
                    }
                    else{
                        $lookup['$lookup']['localField']=$ros->leftColumnTable  .".".json_decode($row->leftColumn)->Field;
                    }
                    $lookup['$lookup']['foreignField']=json_decode($row->rightColumn)->Field;
                    $lookup['$lookup']['as']=$ros->rightColumnTable;
                    $unwind=[];
                    $unwind['$unwind']="$".$ros->rightColumnTable;
                    array_push($pipeline,$lookup);
                    array_push($pipeline,$unwind);
                    array_push($joinTables,$ros->rightColumnTable);
                }
            }
        }

        if(isset($metadataObject->condition) && $metadataObject->condition){
            //array( '$and' => array(array('customers_id'=> array('$gt'=>3) )))) greater condition
            //Less then $lt
            // for and $and
            // for $or
            // for join table table append
            while(preg_match_all('/(\w*)(\(((\w+))\))+/',$metadataObject->condition,$out)){
                $str=$out[3][0].".".$out[1][0];
                $metadataObject->condition=str_replace($out[0][0],$str,$metadataObject->condition);
            }
            //$conditions = preg_split( "/ (and|AND|or|OR) /", $metadataObject->condition );
            $whereArray=[];
            preg_match_all('/((?:\w+.\w+|\w+)\s*(?:=|<|>|>=|<=)\s*(?:\'\w*\s*\w*\'|\w+))(\s*and\s*|\s*or\s*|)/',$metadataObject->condition, $outArray);

            for($i=0;$i<count($outArray[1]);$i++){
                if($outArray[1][$i])
                    array_push($whereArray,$outArray[1][$i]);
                if($outArray[2][$i])
                    array_push($whereArray,$outArray[2][$i]);
            }
            $match=[];
            $match['$match']=[];
            $match['$match']['$or']=[];
            $lastCondition='$or';
            foreach ($whereArray as $condition){

                $conditionExp=[];
                $condition=ltrim($condition," ");
                $condition=rtrim($condition," ");
                if($condition=="and" || $condition=="or"){
                    $lastCondition='$'.$condition;
                }else{
                    while(preg_match_all('/(\w+.\w+|\w+)(\s*=\s*|\s*<\s*|\s*>\s*|\s*<=\s*|\s*>=\s*)(\'\w*\s*\w*\'|\w+)/',$condition, $conditionOpr)){

                        $conditionOpr[3][0]=trim($conditionOpr[3][0],"'");
                        $conditionOpr[3][0]=ltrim($conditionOpr[3][0]," ");
                        $conditionOpr[3][0]=rtrim($conditionOpr[3][0]," ");
                        if(is_numeric($conditionOpr[3][0])){
                            $conditionValue=intval($conditionOpr[3][0]);
                        }else{
                            $conditionValue=$conditionOpr[3][0];
                        }
                        $conditionKey=trim($conditionOpr[1][0],"`");
                        $whereTemp=explode(".",$conditionOpr[1][0]);
                        if($whereTemp[0]==$rootTable){
                            $conditionKey=$whereTemp[1];
                        }
                        if($conditionOpr[2][0]==">"){
                            $conditionExp[$conditionKey]=array('$gt'=>$conditionValue);
                        }else if($conditionOpr[2][0]=="<"){
                            $conditionExp[$conditionKey]=array('$lt'=>$conditionValue);
                        }else if($conditionOpr[2][0]==">="){
                            $conditionExp[$conditionKey]=array('$gte'=>$conditionValue);
                        }else if($conditionOpr[2][0]=="<="){
                            $conditionExp[$conditionKey]=array('$lte'=>$conditionValue);
                        }else{
                            $conditionExp[$conditionKey]=$conditionValue;
                        }
                        $condition=str_replace($conditionOpr[0][0],"",$condition);
                    }
                    //if(count($conditionExp))
                    if(!isset($match['$match'][$lastCondition])){
                        $match['$match'][$lastCondition]=[];
                    }
                    if(isset($metadataObject->type)){
                        $p=[];
                        foreach($conditionExp as $key=>$value){
                            $temp=explode(".",$key);
                            $p[$temp[1]."(".$temp[0].")"]=$value;
                        }
                        array_push($match['$match'][$lastCondition],$p);
                    }else{
                        array_push($match['$match'][$lastCondition],$conditionExp);
                    }
                }
            }
            //array('$match'=>array('$and'=>array(array('customers_id'=> 3))))
            array_push($pipeline,$match);
        }
        //Check one more condition
        if(isset($metadataObject->exCondition) && $metadataObject->exCondition){
            if(isset($metadataObject->type)) {
                $match = [];
                $match['$match'] = [];
                $match['$match']['$or'] = [];
                $lastCondition = '$or';
                $conditionExp = [];
                foreach ($metadataObject->exCondition as $conditions) {
                    foreach (json_decode($conditions) as $key => $condition) {
                        $whereArray = [];
                        preg_match_all('/((?:(\w+\s)*\w+.\w+|\w+)\s*(?:=|<|>|>=|<=)\s*(?:\'\w*\s*\w*\'|\w+))(\s*and\s*|\s*or\s*|)/', $condition, $outArray);
                        for ($i = 0; $i < count($outArray[1]); $i++) {
                            if ($outArray[1][$i])
                                array_push($whereArray, $outArray[1][$i]);
                            if ($outArray[3][$i])
                                array_push($whereArray, $outArray[3][$i]);
                        }
                        foreach ($whereArray as $condition) {
                            $condition = ltrim($condition, " ");
                            $condition = rtrim($condition, " ");
                            if (preg_match("/^and/", $condition) != null || preg_match("/^or/", $condition) != null) {
                                $lastCondition = '$' . $condition;
                            } else {
                                while (preg_match_all('/(?:((?:\w*\s)*\w+.\w+|\w+)\s*(=)\s*|\s*<\s*|\s*>\s*|\s*<=\s*|\s*>=\s*)(\'\w*\s*\w*\'|\w+)/', $condition, $conditionOpr)) {
                                    $conditionOpr[3][0] = trim($conditionOpr[3][0], "'");
                                    $conditionOpr[3][0] = ltrim($conditionOpr[3][0], " ");
                                    $conditionOpr[3][0] = rtrim($conditionOpr[3][0], " ");
                                    if (is_numeric($conditionOpr[3][0])) {
                                        $conditionValue = intval($conditionOpr[3][0]);
                                    } else {
                                        $conditionValue = $conditionOpr[3][0];
                                    }
                                    $conditionKey = trim($conditionOpr[1][0], "`");
                                    $whereTemp=explode(".",$conditionOpr[1][0]);
                                    if($whereTemp[0]==$rootTable){
                                        $conditionKey=$whereTemp[1];
                                    }
                                    if ($conditionOpr[2][0] == ">") {
                                        $conditionExp[$conditionKey] = array('$gt' => $conditionValue);
                                    } else if ($conditionOpr[2][0] == "<") {
                                        $conditionExp[$conditionKey] = array('$lt' => $conditionValue);
                                    } else if ($conditionOpr[2][0] == ">=") {
                                        $conditionExp[$conditionKey] = array('$gte' => $conditionValue);
                                    } else if ($conditionOpr[2][0] == "<=") {
                                        $conditionExp[$conditionKey] = array('$lte' => $conditionValue);
                                    } else {
                                        $conditionExp[$conditionKey] = $conditionValue;
                                    }
                                    $condition = str_replace($conditionOpr[0][0], "", $condition);
                                }
                                //if(count($conditionExp))
                                if (!isset($match['$match'][$lastCondition])) {
                                    $match['$match'][$lastCondition] = [];
                                }
                                array_push($match['$match'][$lastCondition], $conditionExp);
                            }
                        }
                    }
                }
                array_push($pipeline, $match);
            }else{
                $match=[];
                $match['$match']=[];
                $match['$match']['$or']=[];
                $lastCondition='$or';
                $conditionExp=[];
                foreach($metadataObject->exCondition as $conditions){
                    foreach (json_decode($conditions) as $key=>$condition){
                        while(preg_match_all('/(\w*)(\(((\w+))\))+/',$condition,$out)){
                            $str=$out[3][0].".".$out[1][0];
                            $condition=str_replace($out[0][0],$str,$condition);
                        }
                    }
                    $whereArray=[];
                    //preg_match_all('/((?:\w+.\w+|\w+)\s*(?:=|<|>|>=|<=)\s*(?:\'\w*\s*\w*\'|\w+))(\s*and\s*|\s*or\s*|)/',$condition, $outArray);
                    preg_match_all('/((?:(\w*\s*\w*)+.\w+|\w+)\s*(?:=|<|>|>=|<=)\s*(?:\'\w*\s*\w*\'|\w+))(\s*and\s*|\s*or\s*|)/',$condition, $outArray);
                    for($i=0;$i<count($outArray[1]);$i++){
                        if($outArray[1][$i]){
                            array_push($whereArray,$outArray[1][$i]);
                        }
                        if($outArray[2][$i]){
                            array_push($whereArray,$outArray[2][$i]);
                        }
                    }
                    $condition=ltrim($condition," ");
                    $condition=rtrim($condition," ");
                    if($condition=="and" || $condition=="or"){
                        $lastCondition='$'.$condition;
                    }else{
                        while(preg_match_all('/(\w+.\w+|\w+)(\s*=\s*|\s*<\s*|\s*>\s*|\s*<=\s*|\s*>=\s*)(\'\w*\s*\w*\'|\w+)/',$condition, $conditionOpr)){
                            $conditionOpr[3][0]=trim($conditionOpr[3][0],"'");
                            $conditionOpr[3][0]=ltrim($conditionOpr[3][0]," ");
                            $conditionOpr[3][0]=rtrim($conditionOpr[3][0]," ");
                            if(is_numeric($conditionOpr[3][0])){
                                $conditionValue=intval($conditionOpr[3][0]);
                            }else{
                                $conditionValue=$conditionOpr[3][0];
                            }
                            $conditionKey=trim($conditionOpr[1][0],"`");
                            $whereTemp=explode(".",$conditionOpr[1][0]);
                            if($whereTemp[0]==$rootTable){
                                $conditionKey=$whereTemp[1];
                            }
                            if($conditionOpr[2][0]==">"){
                                $conditionExp[$conditionKey]=array('$gt'=>$conditionValue);
                            }else if($conditionOpr[2][0]=="<"){
                                $conditionExp[$conditionKey]=array('$lt'=>$conditionValue);
                            }else if($conditionOpr[2][0]==">="){
                                $conditionExp[$conditionKey]=array('$gte'=>$conditionValue);
                            }else if($conditionOpr[2][0]=="<="){
                                $conditionExp[$conditionKey]=array('$lte'=>$conditionValue);
                            }else{
                                $conditionExp[$conditionKey]=$conditionValue;
                            }
                            $condition=str_replace($conditionOpr[0][0],"",$condition);
                        }
                        //if(count($conditionExp))
                        if(!isset($match['$match'][$lastCondition])){
                            $match['$match'][$lastCondition]=[];
                        }
                        array_push($match['$match'][$lastCondition],$conditionExp);
                    }
                }
                array_push($pipeline,$match);
            }
        } 
        if(isset($metadataObject->limit) && $metadataObject->limit){
            if($metadataObject->limit<100)
                array_push($pipeline, array('$limit' => intval($metadataObject->limit)));
            else
                array_push($pipeline, array('$limit' => 100));
        }
        $list= $data->aggregate($pipeline);
        $tableColumnObject=[];
        $tableData=[];
        foreach ($list as $lists)
        {
            $singleData=[];
            foreach($lists as $key => $value)
            {
                if($key!='_id') {
                    if (in_array($key, $joinTables)) {
                        foreach ($value as $keyJoin => $valueJoin) {
                            if($keyJoin!="_id") {
                                $val = gettype($valueJoin);
                                if ($val == "string") {
                                    $val = "text";
                                } else if ($val == "Null") {
                                    $val = "text";
                                }
                                if ($val == "integer" || $val == "decimal" || $val == "double" || $val == "float") {
                                    if ($val == "integer") {
                                        $val = "int";
                                    }
                                    if (!isset($tableColumnObject[$keyJoin . "(" . $key . ")"])) {
                                        $tableColumnObject[$keyJoin . "(" . $key . ")"] = [];
                                        $tableColumnObject[$keyJoin . "(" . $key . ")"]['tableName'] = $key;
                                        $tableColumnObject[$keyJoin . "(" . $key . ")"]['Field'] = $keyJoin . "(" . $key . ")";
                                        $tableColumnObject[$keyJoin . "(" . $key . ")"]['Type'] = $val;
                                        $tableColumnObject[$keyJoin . "(" . $key . ")"]['dataType'] = "Measure";
                                    } else {
                                        if ($tableColumnObject[$keyJoin . "(" . $key . ")"]['tableName'] != $key) {
                                            $tableColumnObject[$keyJoin . "(" . $key . ")"] = [];
                                            $tableColumnObject[$keyJoin . "(" . $key . ")"]['tableName'] = $key;
                                            $tableColumnObject[$keyJoin . "(" . $key . ")"]['Field'] = $keyJoin . "(" . $key . ")";
                                            $tableColumnObject[$keyJoin . "(" . $key . ")"]['Type'] = $val;
                                            $tableColumnObject[$keyJoin . "(" . $key . ")"]['dataType'] = "Measure";
                                        }
                                    }
                                } else {
                                    if (!isset($tableColumnObject[$keyJoin . "(" . $key . ")"])) {
                                        $tableColumnObject[$keyJoin . "(" . $key . ")"] = [];
                                        $tableColumnObject[$keyJoin . "(" . $key . ")"]['tableName'] = $key;
                                        $tableColumnObject[$keyJoin . "(" . $key . ")"]['Field'] =  $keyJoin . "(" . $key . ")";
                                        $tableColumnObject[$keyJoin . "(" . $key . ")"]['Type'] = $val;
                                        $tableColumnObject[$keyJoin . "(" . $key . ")"]['dataType'] = "Dimension";
                                    } else {
                                        if ($tableColumnObject[$keyJoin . "(" . $key . ")"]['tableName'] != $key) {
                                            $tableColumnObject[$keyJoin . "(" . $key . ")"] = [];
                                            $tableColumnObject[$keyJoin . "(" . $key . ")"]['tableName'] = $key;
                                            $tableColumnObject[$keyJoin . "(" . $key . ")"]['Field'] = $keyJoin . "(" . $key . ")";
                                            $tableColumnObject[$keyJoin . "(" . $key . ")"]['Type'] = $val;
                                            $tableColumnObject[$keyJoin . "(" . $key . ")"]['dataType'] = "Dimension";
                                        }
                                    }
                                }
                                if (isset($tableColumnObject[$keyJoin . "(" . $key . ")"])) {
                                    $singleData[$keyJoin . "(" . $key . ")"] = $valueJoin;
                                } else {
                                    $singleData[$keyJoin . "(" . $key . ")"] = $valueJoin;
                                }
                            }
                        }
                    } else {
                        $val = gettype($value);
                        if ($val == "string") {
                            $val = "text";
                        } else if ($val == "Null") {
                            $val = "text";
                        }
                        if ($val == "integer" || $val == "decimal" || $val == "double" || $val == "float") {
                            if ($val == "integer") {
                                $val = "int";
                            }
                            if (!isset($tableColumnObject[ $key . "(" . $metadataObject->rootTable . ")"])) {
                                $tableColumnObject[$key . "(" . $metadataObject->rootTable . ")"] = [];
                                $tableColumnObject[$key . "(" . $metadataObject->rootTable . ")"]['tableName'] = $metadataObject->rootTable;
                                $tableColumnObject[$key . "(" . $metadataObject->rootTable . ")"]['Field'] = $key . "(" . $metadataObject->rootTable . ")";
                                $tableColumnObject[$key . "(" . $metadataObject->rootTable . ")"]['Type'] = $val;
                                $tableColumnObject[$key . "(" . $metadataObject->rootTable . ")"]['dataType'] = "Measure";
                            }
                        } else {
                            if (!isset($tableColumnObject[$key . "(" . $metadataObject->rootTable . ")"])) {
                                $tableColumnObject[$key . "(" . $metadataObject->rootTable . ")"] = [];
                                $tableColumnObject[$key . "(" . $metadataObject->rootTable . ")"]['tableName'] = $metadataObject->rootTable;
                                $tableColumnObject[$key . "(" . $metadataObject->rootTable . ")"]['Field'] = $key . "(" . $metadataObject->rootTable . ")";
                                $tableColumnObject[$key . "(" . $metadataObject->rootTable . ")"]['Type'] = $val;
                                $tableColumnObject[$key . "(" . $metadataObject->rootTable . ")"]['dataType'] = "Dimension";
                            }
                        }
                        $singleData[$key . "(" . $metadataObject->rootTable . ")"] = $value;
                    }
                }
            }
            array_push($tableData,$singleData);
        }
        $this->result =  array("tableColumn" => json_encode($tableColumnObject),"tableData" => json_encode($tableData));
        $this->errorCode = 1;
        $this->message = 'Successfully';
        return $this;
    }
}
?>