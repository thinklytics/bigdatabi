<?php
namespace App\Http\Utils;
use Illuminate\Support\Facades\Redis;

class HelperFunctions
{
    /*
     * Data write to redis
     */
    public static function writeToRedis($customerId,$identifier,$data,$incrementObj,$company_id,$token,$statusObj,$roleLevelCondition,$department,$realTimeFlag,$userType){
        $redis = Redis::connection();
        $processedData=array();
        if(is_array($statusObj)){
            $flag=$statusObj['flag'];
            $dataLength=$statusObj['dataLength'];
            $uKey=$statusObj['ukey'];
        }else{
            $flag=0;
        }
        try{
            if($realTimeFlag){
                $keyRedis="$identifier:$company_id-realtime";
            }else{
                $keyRedis="$identifier:$company_id";
            }
            if(!$redis->exists($keyRedis)){
               // dd(count(json_decode($data['tableData'])));
                if(!isset($dataLength)){
                    $dataLength=count(json_decode($data['tableData']));
                }
                $tempArr=[];
                $tempArr['updated_at']=[];
                if($incrementObj && $dataLength<100000){
                    $incrementObj=json_decode($incrementObj);
                    $lastData="";
                    foreach (json_decode($data['tableData']) as $value){
                        $selectedKeys=[];
                        foreach ($incrementObj->selected_Key as $keyObj){
                            $keyObj=json_decode($keyObj);
                            array_push($selectedKeys,$keyObj->Field."(".$keyObj->tableName.")");
                        }
                        /*
                         * Current Date time
                         */
                        /*
                         * make key
                         */
                        $key="";
                        foreach ($selectedKeys as $selectedKey){
                            $key .= $value->$selectedKey;
                        }
                        $lastData=(array)$value;
                        foreach($incrementObj->table as $table){
                            if(isset($table->key)){
                                if(!isset($tempArr['updated_at'][$table->name])){
                                    $tempArr['updated_at'][$table->name]=$lastData[$table->key."(".$table->name.")"];
                                }else if(strtotime($lastData[$table->key."(".$table->name.")"])>strtotime($tempArr['updated_at'][$table->name])){
                                    $tempArr['updated_at'][$table->name]=$lastData[$table->key."(".$table->name.")"];
                                }
                            }
                        }
                        if($realTimeFlag){
                            $redis->hset("$identifier:$company_id-realtime",$key,json_encode($value,JSON_UNESCAPED_UNICODE));
                        }else{
                            $redis->hset("$identifier:$company_id",$key,json_encode($value,JSON_UNESCAPED_UNICODE));
                        }
                    }
                }else{
                    if($dataLength>=100000){
                        $dataVal = base64_encode(gzencode(
                            json_encode($data['tableData'], JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_UNESCAPED_UNICODE),
                            8,
                            FORCE_DEFLATE
                        ));
                        $redis->hset("$identifier:$company_id",1,$dataVal);
                    }else{
                        foreach (json_decode($data['tableData']) as $key=>$value){
                            if($realTimeFlag){
                                $redis->hset("$identifier:$company_id-realtime",$key,json_encode($value));
                            }else{
                                $redis->hset("$identifier:$company_id",$key,json_encode($value));
                            }

                        }
                    }
                }
                if($realTimeFlag){
                    $redis->hset("$identifier:$company_id-realtime","tableColumn",json_encode($data['tableColumn']));
                }else{
                    $redis->hset("$identifier:$company_id","tableColumn",json_encode($data['tableColumn']));
                }
                $currentData=date('Y-m-d H:i:s');
                if(isset($statusObj['roleLevelLength'])){
                    $tempArr['length']=$statusObj['roleLevelLength'];
                }else{
                    $tempArr['length']=$dataLength;


                }
                if($incrementObj){
                    $tempArr['incrementObj']=true;
                }else{
                    $tempArr['incrementObj']=false;
                }
                if($userType){
                    $tempArr['userType']=$userType;
                }
                $tempArr['roleLevelCondition']=$roleLevelCondition;
                $tempArr['department']=$department;
                $redis->hset("metadataDetails","$customerId:{$identifier}:$company_id:$token",json_encode($tempArr));
                $redis->hset("metadataDetails","{$identifier}:$company_id",$dataLength);
            }else if($flag==1){
                $dataVal = base64_encode(gzencode(
                    json_encode($data['tableData'], JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_UNESCAPED_UNICODE),
                    8,
                    FORCE_DEFLATE
                ));
                $redis->hset("$identifier:$company_id",$uKey,$dataVal);
            }else{

                /*
                 * Metadata key for new user
                 */
                if($realTimeFlag){
                    $data=$redis->hgetAll("$identifier:$company_id-realtime");
                }else{
                    $data=$redis->hgetAll("$identifier:$company_id");
                }
                $lengthObj = $redis->hgetAll("metadataDetails");
                $meta_key="$customerId:{$identifier}:$company_id:$token";
                $lengthByMetaObj=count($data);
                if(!$realTimeFlag){
                    $lengthByMetaObj=$lengthObj["{$identifier}:$company_id"];
                }
                if (!isset($lengthObj[$meta_key])) {
                    $tempArr=[];
                    $tempArr['updated_at']=[];
                    if($incrementObj){
                        $incrementObj=json_decode($incrementObj);
                        $lastData="";
                        foreach ($data as $key_redis=>$value){
                            $value=json_decode($value);
                            $selectedKeys=[];
                            if($key_redis!="tableColumn"){
                                foreach ($incrementObj->selected_Key as $keyObj){
                                    $keyObj=json_decode($keyObj);
                                    array_push($selectedKeys,$keyObj->Field."(".$keyObj->tableName.")");
                                }
                            }
                            /*
                             * Current Date time
                             */
                            /*
                             * make key
                             */
                            $key="";
                            foreach ($selectedKeys as $selectedKey){
                                $key .= $value->$selectedKey;
                            }
                            $lastData=(array)$value;
                            //dd($lastData);
                            foreach($incrementObj->table as $table){
                                if(isset($table->key)){
                                    if(!isset($tempArr['updated_at'][$table->name])){
                                        $tempArr['updated_at'][$table->name]=$lastData[$table->key."(".$table->name.")"];
                                    }else if(isset($lastData[$table->key."(".$table->name.")"]) && strtotime($lastData[$table->key."(".$table->name.")"])>strtotime($tempArr['updated_at'][$table->name])){
                                        $tempArr['updated_at'][$table->name]=$lastData[$table->key."(".$table->name.")"];
                                    }
                                }
                            }
                        }
                    }
                    $currentData=date('Y-m-d H:i:s');
                    $tempArr['length']=$lengthByMetaObj;
                    if($incrementObj){
                        $tempArr['incrementObj']=true;
                    }else{
                        $tempArr['incrementObj']=false;
                    }
                    $tempArr['roleLevelCondition']=$roleLevelCondition;
                    $tempArr['department']=$department;
                    if($userType){
                        $tempArr['userType']=$userType;
                    }
                    $redis->hset("metadataDetails","$customerId:{$identifier}:$company_id:$token",json_encode($tempArr));
                }
            }
            //$data=$redis->hgetall("$identifier:$company_id");
            $processedData["result"]=array();
            $processedData["result"]["tableColumn"]= $data['tableColumn'];
            $processedData["errorCode"]=1;
            $processedData["message"]="Successful";
        }catch (Exception $e){
            $processedData["errorCode"]=0;
            $processedData["message"]=$e;
        }
        return $processedData;
    }
    public static function isCachedInRedis($customerId,$identifier,$company_id,$realTimeFlag){
        $redis = Redis::connection();
        $redisFlag=false;
        if($realTimeFlag){
            if($redis->exists("$identifier:$company_id-realtime")){
                $redisFlag=true;
            }
        }else{
            if($redis->exists("$identifier:$company_id")){
                $redisFlag=true;
            }
        }

        return $redisFlag;
    }
}
?>