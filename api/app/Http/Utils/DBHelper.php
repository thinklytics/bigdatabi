<?php
namespace App\Http\Utils;
use App\Http\Core\MongoDB\MongoDB;
use App\Model\Permission;
use App\Model\PermissionRole;
use App\Model\Role;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Capsule\Manager as DBNew;
use Illuminate\Support\Facades\File;
use Jenssegers\Mongodb\Connection as Connection;
use Monolog\Handler\Mongo;
use Jenssegers\Mongodb\Queue\MongoConnector;
use MongoDB\Client;
use App\Model\Datasource;
use Illuminate\Support\Facades\Schema;
use PDO;

/**
 * Created by PhpStorm.
 * User: rahul.sharma
 * Date: 1/4/2018
 * Time: 1:05 PM
 */
class DBHelper
{
    public static function switchDataBase($sourceObject){
        //Datasource fetch from database
        //$sourceObject=Datasource::find($sourceObject->id)->first();
        $sourceObject->datasourceType=strtolower($sourceObject->datasourceType);
        $db = new DBNew();
        if($sourceObject->datasourceType=="mysql"){
            $db->addConnection([
                'driver' => 'mysql',
                'host' => $sourceObject->host,
                'port' => $sourceObject->port,
                'database' => $sourceObject->dbname,
                'username' => $sourceObject->username,
                'password' => $sourceObject->password,
                'charset' => 'utf8mb4',
                'collation' => 'utf8mb4_unicode_ci',
                'prefix' => '',
                'strict' => true,
                'engine' => null,
            ]);

            $db->setAsGlobal();
            $db->bootEloquent();
        }else if($sourceObject->datasourceType=="mssql"){
            //    print_r($sourceObject);
            $db->addConnection([
                'driver' => 'sqlsrv',
                'host' => $sourceObject->host,
                'port' => $sourceObject->port,
                'database' => $sourceObject->dbname,
                'username' => $sourceObject->username,
                'password' => $sourceObject->password,
                'charset' => 'utf8',
                'collation' => 'utf8_unicode_ci',
                'prefix' => '',
                'strict' => true,
                'engine' => null,
            ]);
            $db->setAsGlobal();
            $db->bootEloquent();
        }else if($sourceObject->datasourceType=="excel"){
            //    print_r($sourceObject);
            $db->addConnection([
                'driver' => 'mysql',
                'host' => $sourceObject->host,
                'port' => $sourceObject->port,
                'database' => $sourceObject->dbname,
                'username' => $sourceObject->username,
                'password' => $sourceObject->password,
                'charset' => 'utf8mb4',
                'collation' => 'utf8mb4_unicode_ci',
                'prefix' => '',
                'strict' => true,
                'engine' => null,
            ]);
            $db->setAsGlobal();
            $db->bootEloquent();

        }else if($sourceObject->datasourceType=="mongodb"){
            $conn = new Client("mongodb://$sourceObject->username:$sourceObject->password@$sourceObject->host:$sourceObject->port");
            return $conn;
        }else if($sourceObject->datasourceType=="postgres"){
            if($sourceObject->dbname==''){
                $sourceObject->dbname='postgres';
            }
            $db->addConnection([
                'driver' => 'pgsql',
                'host' => $sourceObject->host,
                'port' => $sourceObject->port,
                'database' => $sourceObject->dbname,
                'username' => $sourceObject->username,
                'password' => $sourceObject->password,
                'charset' => 'utf8',
                'collation' => 'utf8_unicode_ci',
                'prefix' => '',
                'strict' => true,
                'engine' => null,
            ]);
            $db->setAsGlobal();
            $db->bootEloquent();
        }else if($sourceObject->datasourceType=="oracle"){
            /*$mydb="(DESCRIPTION = (ADDRESS = (PROTOCOL = TCP)(HOST = $sourceObject->host)(PORT = $sourceObject->port)) (CONNECT_DATA = (SERVER = DEDICATED)(SERVICE_NAME = orcl)))";
            $conn_username = "$sourceObject->username";
            $conn_password = "$sourceObject->password";
            $conn = new DB("oci:dbname=".$mydb, $conn_username, $conn_password);
            return $conn;*/
            $dbArr=explode("//@",$sourceObject->dbname);
            if($dbArr[0]=="sid"){
                $db->addConnection([
                    'driver'        => 'oracle',
                    'tns'           => "(DESCRIPTION = (ADDRESS_LIST = (ADDRESS = (PROTOCOL = TCP)(HOST = $sourceObject->host)(PORT = $sourceObject->port))) (CONNECT_DATA = (SERVICE_NAME = $dbArr[1])))",
                    'host'          => $sourceObject->host,
                    'port'          => $sourceObject->port,
                    'database'      => $dbArr[1],
                    'username'      => $sourceObject->username,
                    'password'      => $sourceObject->password,
                    'charset'       => env('DB_CHARSET', 'AL32UTF8'),
                    'prefix'        => env('DB_PREFIX', ''),
                    'prefix_schema' => env('DB_SCHEMA_PREFIX', ''),
                ]);
            }else{
                $db->addConnection([
                    'driver'        => 'oracle',
                    'host'          => $sourceObject->host,
                    'port'          => $sourceObject->port,
                    'database'      => $sourceObject->dbname,
                    'service_name'  => $dbArr[1],
                    'username'      => $sourceObject->username,
                    'password'      => $sourceObject->password,
                    'charset'       => env('DB_CHARSET', 'AL32UTF8'),
                    'prefix'        => env('DB_PREFIX', ''),
                    'prefix_schema' => env('DB_SCHEMA_PREFIX', ''),
                ]);
            }

            $db->setAsGlobal();
            $db->bootEloquent();
            //Working code
            /*$mydb="(DESCRIPTION =
                    (ADDRESS = (PROTOCOL = TCP)(HOST = 13.235.191.76)(PORT = 1521))
                    (CONNECT_DATA =
                      (SERVER = DEDICATED)
                      (SERVICE_NAME = orcl)
                    )
                  )";
            $conn_username = "remoteallow";
            $conn_password = "hello@thinklayer";
            $opt = [
                PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
                PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_NUM,

            ];
            try{
                $conn = new PDO("oci:dbname=".$mydb, $conn_username, $conn_password, $opt);
            }catch(PDOException $e){
                echo ($e->getMessage());
            }
            return $conn;*/
        }

        return "";
    }
    public static function createDB($dbName,$plan_id){
        $charset = config("database.connections.mysql.charset",'utf8mb4');
        $collation = config("database.connections.mysql.collation",'utf8mb4_unicode_ci');
        $query = "CREATE DATABASE IF NOT EXISTS $dbName CHARACTER SET $charset COLLATE $collation;";
        DB::statement($query);
        $sql_dump = File::get('/var/www/html/demo2_thinklytics_io/think_db.sql');
        Config::set('database.connections.'.$dbName,  array(
            'driver' => 'mysql',
            'host' => env('DB_HOST', '127.0.0.1'),
            'port' => env('DB_PORT', '3306'),
            'database' => $dbName,
            'username' => env('DB_USERNAME', 'forge'),
            'password' => env('DB_PASSWORD', ''),
            'charset' => 'utf8mb4',
            'collation' => 'utf8mb4_unicode_ci',
            'prefix' => '',
            'strict' => true,
            'engine' => null,
        ));
        DB::connection($dbName)->getPdo()->exec($sql_dump);
        /*
         * Seeds permission
         */
        if($plan_id=="PT102"){
            $permissions=[
                [
                    "order"=>0,
                    "name"=>"datasource-add",
                    "display_name"=>"Datasource Add",
                    "description"=>"Datasource Add"
                ],
                [
                    "order"=>0,
                    "name"=>"datasource-list",
                    "display_name"=>"Datasource list",
                    "description"=>"Datasource list"
                ],
                [
                    "order"=>0,
                    "name"=>"datasource-edit",
                    "display_name"=>"Datasource Edit",
                    "description"=>"Datasource Edit"
                ],
                [
                    "order"=>0,
                    "name"=>"datasource-view",
                    "display_name"=>"Datasource View",
                    "description"=>"Datasource View"
                ],
                [
                    "order"=>0,
                    "name"=>"datasource-delete",
                    "display_name"=>"Datasource Delete",
                    "description"=>"Datasource Delete"
                ],
                [
                    "order"=>1,
                    "name"=>"metadata-add",
                    "display_name"=>"Metadata Add",
                    "description"=>"Metadata Add"
                ],
                [
                    "order"=>1,
                    "name"=>"metadata-list",
                    "display_name"=>"Metadata list",
                    "description"=>"Metadata list"
                ],
                [
                    "order"=>1,
                    "name"=>"metadata-edit",
                    "display_name"=>"Metadata Edit",
                    "description"=>"Metadata Edit"
                ],
                [
                    "order"=>1,
                    "name"=>"metadata-view",
                    "display_name"=>"Metadata View",
                    "description"=>"Metadata View"
                ],
                [
                    "order"=>1,
                    "name"=>"metadata-delete",
                    "display_name"=>"Metadata Delete",
                    "description"=>"Metadata Delete"
                ],
                [
                    "order"=>2,
                    "name"=>"ml-add",
                    "display_name"=>"M/L Model Add",
                    "description"=>"M/L Model Add"
                ],[
                    "order"=>2,
                    "name"=>"ml-list",
                    "display_name"=>"M/L Model List",
                    "description"=>"M/L Model List"
                ],[
                    "order"=>2,
                    "name"=>"ml-edit",
                    "display_name"=>"M/L Model Edit",
                    "description"=>"M/L Model edit"
                ],[
                    "order"=>2,
                    "name"=>"ml-view",
                    "display_name"=>"M/L Model View",
                    "description"=>"M/L Model View"
                ],[
                    "order"=>2,
                    "name"=>"ml-delete",
                    "display_name"=>"M/L Model Delete",
                    "description"=>"M/L Model delete"
                ],
                [
                    "order"=>3,
                    "name"=>"dashboard-add",
                    "display_name"=>"Dashboard Add",
                    "description"=>"Dashboard Add"
                ],
                [
                    "order"=>3,
                    "name"=>"dashboard-list",
                    "display_name"=>"Dashboard list",
                    "description"=>"Dashboard list"
                ],
                [
                    "order"=>3,
                    "name"=>"dashboard-edit",
                    "display_name"=>"Dashboard Edit",
                    "description"=>"Dashboard Edit"
                ],
                [
                    "order"=>3,
                    "name"=>"dashboard-view",
                    "display_name"=>"Dashboard View",
                    "description"=>"Dashboard View"
                ],
                [
                    "order"=>3,
                    "name"=>"dashboard-delete",
                    "display_name"=>"Dashboard Delete",
                    "description"=>"Dashboard Delete"
                ],
                [
                    "order"=>4,
                    "name"=>"sharedview-add",
                    "display_name"=>"Sharedview Add",
                    "description"=>"Sharedview Add"
                ],[
                    "order"=>4,
                    "name"=>"sharedview-list",
                    "display_name"=>"Sharedview list",
                    "description"=>"Sharedview list"
                ],
                [
                    "order"=>4,
                    "name"=>"sharedview-edit",
                    "display_name"=>"Sharedview Edit",
                    "description"=>"Sharedview Edit"
                ],
                [
                    "order"=>4,
                    "name"=>"sharedview-view",
                    "display_name"=>"Sharedview View",
                    "description"=>"Sharedview View"
                ],
                [
                    "order"=>4,
                    "name"=>"sharedview-delete",
                    "display_name"=>"Sharedview Delete",
                    "description"=>"Sharedview Delete"
                ],
                [
                    "order"=>5,
                    "name"=>"setting",
                    "display_name"=>"Setting",
                    "description"=>"Setting"
                ]
            ];
        }else{
            $permissions=[
                [
                    "order"=>0,
                    "name"=>"datasource-add",
                    "display_name"=>"Datasource Add",
                    "description"=>"Datasource Add"
                ],
                [
                    "order"=>0,
                    "name"=>"datasource-list",
                    "display_name"=>"Datasource list",
                    "description"=>"Datasource list"
                ],
                [
                    "order"=>0,
                    "name"=>"datasource-edit",
                    "display_name"=>"Datasource Edit",
                    "description"=>"Datasource Edit"
                ],
                [
                    "order"=>0,
                    "name"=>"datasource-view",
                    "display_name"=>"Datasource View",
                    "description"=>"Datasource View"
                ],
                [
                    "order"=>0,
                    "name"=>"datasource-delete",
                    "display_name"=>"Datasource Delete",
                    "description"=>"Datasource Delete"
                ],
                [
                    "order"=>1,
                    "name"=>"metadata-add",
                    "display_name"=>"Metadata Add",
                    "description"=>"Metadata Add"
                ],
                [
                    "order"=>1,
                    "name"=>"metadata-list",
                    "display_name"=>"Metadata list",
                    "description"=>"Metadata list"
                ],
                [
                    "order"=>1,
                    "name"=>"metadata-edit",
                    "display_name"=>"Metadata Edit",
                    "description"=>"Metadata Edit"
                ],
                [
                    "order"=>1,
                    "name"=>"metadata-view",
                    "display_name"=>"Metadata View",
                    "description"=>"Metadata View"
                ],
                [
                    "order"=>1,
                    "name"=>"metadata-delete",
                    "display_name"=>"Metadata Delete",
                    "description"=>"Metadata Delete"
                ],
                [
                    "order"=>3,
                    "name"=>"dashboard-add",
                    "display_name"=>"Dashboard Add",
                    "description"=>"Dashboard Add"
                ],
                [
                    "order"=>3,
                    "name"=>"dashboard-list",
                    "display_name"=>"Dashboard list",
                    "description"=>"Dashboard list"
                ],
                [
                    "order"=>3,
                    "name"=>"dashboard-edit",
                    "display_name"=>"Dashboard Edit",
                    "description"=>"Dashboard Edit"
                ],
                [
                    "order"=>3,
                    "name"=>"dashboard-view",
                    "display_name"=>"Dashboard View",
                    "description"=>"Dashboard View"
                ],
                [
                    "order"=>3,
                    "name"=>"dashboard-delete",
                    "display_name"=>"Dashboard Delete",
                    "description"=>"Dashboard Delete"
                ],
                [
                    "order"=>4,
                    "name"=>"sharedview-add",
                    "display_name"=>"Sharedview Add",
                    "description"=>"Sharedview Add"
                ],[
                    "order"=>4,
                    "name"=>"sharedview-list",
                    "display_name"=>"Sharedview list",
                    "description"=>"Sharedview list"
                ],
                [
                    "order"=>4,
                    "name"=>"sharedview-edit",
                    "display_name"=>"Sharedview Edit",
                    "description"=>"Sharedview Edit"
                ],
                [
                    "order"=>4,
                    "name"=>"sharedview-view",
                    "display_name"=>"Sharedview View",
                    "description"=>"Sharedview View"
                ],
                [
                    "order"=>4,
                    "name"=>"sharedview-delete",
                    "display_name"=>"Sharedview Delete",
                    "description"=>"Sharedview Delete"
                ],
                [
                    "order"=>5,
                    "name"=>"setting",
                    "display_name"=>"Setting",
                    "description"=>"Setting"
                ]
            ];
        }
        foreach($permissions as $permissionObj){
            $permission= Permission::on($dbName)->create([
                "order"=>$permissionObj['order'],
                "name"=>$permissionObj['name'],
                "display_name"=>$permissionObj['display_name'],
                "description"=>$permissionObj['description'],
            ]);
            $permission->save();
        }
        if($plan_id=="PT101"){
            $roles=[
                [
                    "name"=>"Super Admin",
                    "display_name"=>"Super Admin",
                    "description"=>"Super Admin",
                    "landing_page"=>"datasource-list",
                    "permission"=>["datasource-add","datasource-list","datasource-edit","datasource-view","datasource-delete",
                        "metadata-add","metadata-list","metadata-edit","metadata-view","metadata-delete",
                        "dashboard-add","dashboard-list","dashboard-edit","dashboard-view","dashboard-delete",
                        "sharedview-add","sharedview-list","sharedview-edit","sharedview-view","sharedview-delete",
                        "setting"]
                ],
                [
                    "name"=>"Admin",
                    "display_name"=>"Admin",
                    "description"=>"Admin",
                    "landing_page"=>"datasource-list",
                    "permission"=>["datasource-add","datasource-list","datasource-edit","datasource-view","datasource-delete",
                        "metadata-add","metadata-list","metadata-edit","metadata-view","metadata-delete",
                        "dashboard-add","dashboard-list","dashboard-edit","dashboard-view","dashboard-delete",
                        "sharedview-add","sharedview-list","sharedview-edit","sharedview-view","sharedview-delete",
                        "setting"]
                ]
            ];
        }else{
            $roles=[
                [
                    "name"=>"Super Admin",
                    "display_name"=>"Super Admin",
                    "description"=>"Super Admin",
                    "landing_page"=>"datasource-list",
                    "permission"=>["datasource-add","datasource-list","datasource-edit","datasource-view","datasource-delete",
                        "metadata-add","metadata-list","metadata-edit","metadata-view","metadata-delete",
                        "ml-add","ml-list","ml-edit","ml-view","ml-delete",
                        "dashboard-add","dashboard-list","dashboard-edit","dashboard-view","dashboard-delete",
                        "sharedview-add","sharedview-list","sharedview-edit","sharedview-view","sharedview-delete",
                        "setting"]
                ],
                [
                    "name"=>"Admin",
                    "display_name"=>"Admin",
                    "description"=>"Admin",
                    "landing_page"=>"datasource-list",
                    "permission"=>["datasource-add","datasource-list","datasource-edit","datasource-view","datasource-delete",
                        "metadata-add","metadata-list","metadata-edit","metadata-view","metadata-delete",
                        "ml-add","ml-list","ml-edit","ml-view","ml-delete",
                        "dashboard-add","dashboard-list","dashboard-edit","dashboard-view","dashboard-delete",
                        "sharedview-add","sharedview-list","sharedview-edit","sharedview-view","sharedview-delete",
                        "setting"]
                ]
            ];
        }

        /*,
        [
            "name"=>"IT Admin",
            "display_name"=>"IT Admin",
            "description"=>"IT Admin",
            "landing_page"=>"datasource-list",
            "permission"=>["datasource-add","datasource-list","datasource-edit","datasource-view","datasource-delete",
                "metadata-add","metadata-list","metadata-edit","metadata-view","metadata-delete",
                "ml-add","ml-list","ml-edit","ml-view","ml-delete",
                "dashboard-add","dashboard-list","dashboard-edit","dashboard-view","dashboard-delete",
                "sharedview-add","sharedview-list","sharedview-edit","sharedview-view","sharedview-delete",
                "setting"]
        ],
            [
                "name"=>"HR Admin",
                "display_name"=>"HR Admin",
                "description"=>"HR Admin",
                "landing_page"=>"datasource-list"
            ],
            [
                "name"=>"Report Designer",
                "display_name"=>"Report Designer",
                "description"=>"Report Designer",
                "landing_page"=>"datasource-list"
            ],
            [
                "name"=>"Report Viewer",
                "display_name"=>"Report Viewer",
                "description"=>"Report Viewer",
                "landing_page"=>"sharedview-list"
            ]*/
        foreach($roles as $role){
            $roleObj= Role::on($dbName)->create([
                "name"=>$role['name'],
                "display_name"=>$role['display_name'],
                "description"=>$role['description'],
                "landing_page"=>$role['landing_page']
            ]);
            $roleObj->save();
            foreach ($role['permission'] as $permission){
                $permissionObj=Permission::on($dbName)->where('name',$permission)->first();
                if($permissionObj){
                    PermissionRole::on($dbName)->create([
                        "permission_id"=>$permissionObj->id,
                        "role_id"=>$roleObj->id
                    ]);
                }
            }
        }
        /*
         * Mongo db create new database
         */
        $sourceObject=[];
        $sourceObject['datasourceType']=env('MDB_CONNECTION');
        $sourceObject['dbname']=$dbName;
        $sourceObject['host']=env('MDB_HOST');
        $sourceObject['port']=env('MDB_PORT');
        $sourceObject['username']=env('MDB_USERNAME');
        $sourceObject['password']=env('MDB_PASSWORD');
        $sourceObject=(Object)$sourceObject;
        $conn = new Client("mongodb://$sourceObject->username:$sourceObject->password@$sourceObject->host:$sourceObject->port");
        $db=$conn->$dbName;
        $collection = $db->createCollection("testCollection");
    }
} 