<?php
namespace App\Http\Contract;
/**
 * Class DatasourceInterface
 * @property int errorCode
 *
 * @property string message
 * @property object result
 * @package App\Http\Contract

 */
    abstract class DatasourceInterface {
        public abstract function connect($connectQuery);
        public abstract function listDatabase();
        public abstract function listTables();
        public abstract function getData($metadataObject);
        public abstract function getColumnName($tableName);
    }
?>