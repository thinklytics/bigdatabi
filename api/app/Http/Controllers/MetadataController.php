<?php

namespace App\Http\Controllers;

use App\Http\Core\MongoDB\MongoDB;
use App\Http\Core\MsSQL\MsSQL;
use App\Jobs\BlendingData;
use App\Model\Datasource;
use App\Model\Dashboard;
use App\Model\CompanyDetail;
use App\Model\Metadata;
use App\Http\Core\MySQL\MySQL;
use App\Http\Core\ExcelDs\ExcelDs;
use App\Http\Core\PostGres\PostGres;
use App\Http\Core\Oracle\Oracle;
use App\Model\MetadataReportGroup;
use App\Model\ReportGroup;
use App\Model\ReportUserGroup;
use App\Model\RoleLevelSecurity;
use App\Model\RoleLevelSecurityUser;
use App\Model\RoleLevelSecurityUserGroup;
use App\Model\UserGroup;
use App\Model\UserGroupUser;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Database\Capsule\Manager as DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Response;
use Mockery\Exception;
use Psy\Util\Json;
use App\Model\Email;
use App\Model\EmailNotification;
use App\Model\PublicView;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redis;
use File;
class MetadataController extends Controller
{
    /*
     *******************************Metadata for other database************************************************************
     */
    /*
     * Table List
     */
    public function getTableList()
    {
        $metadata=null;
        $sourceObject=json_decode(Input::get('sourceObject'));
        if(Controller::datasourceCheck($sourceObject->datasource_id)){
            $sourceObject=Controller::datasourceCheck($sourceObject->datasource_id);
        }
        switch ($sourceObject->datasourceType) {
            case "mysql":
                $metadata = (new MySQL())->connect($sourceObject);
                break;
            case "mongodb":
                $metadata = (new MongoDB())->connect($sourceObject);
                break;
            case "mssql":
                $metadata = (new MsSQL())->connect($sourceObject);
                break;
            case "excel":
                $metadata = (new ExcelDs())->connect($sourceObject);
                break;
            case "postgres":
                $metadata = (new PostGres())->connect($sourceObject);
                break;
            case "oracle":
                $metadata = (new Oracle())->connect($sourceObject);
                break;
        }
        return Response::json($metadata->listTables());
    }
    /*
     * Make query and get table data
     */
    public function getQueryData()
    {
        $metadata="";
        $metadataObject=json_decode(Input::get('matadataObject'));
        $connection=$metadataObject->connectionObject;
        if(Controller::datasourceCheck($connection->datasource_id)){
            $connection=Controller::datasourceCheck($connection->datasource_id);
        }
        switch ($connection->datasourceType) {
            case "mysql":
                $metadata = (new MySQL())->connect($connection);
                break;
            case "mongodb":
                $metadata = (new MongoDB())->connect($connection);
                break;
            case "mssql":
                $metadata = (new MsSQL())->connect($connection);
                break;
            case "excel":
                $metadata = (new ExcelDs())->connect($connection);
                break;
            case "postgres":
                $metadata = (new PostGres())->connect($connection);
                break;
            case "oracle":
                $metadata = (new Oracle())->connect($connection);
                break;
        }
        return Response::json($metadata->getData($metadataObject));
    }
    /*
     * Get query to fetch data copy metadata for dashboard
     */

    public function getQueryDataDashboard()
    {
        $mongoDb=CompanyDetail::where('uid',Auth::user()->company_id)->first();
        $metadata="";
        $metadataObject=json_decode(Input::get('matadataObject'));
        if(isset($metadataObject->connObject->connectionObject)){
            $connection=$metadataObject->connObject->connectionObject;
            $connection=Controller::datasourceCheck($connection->datasource_id);
        }
        $roleCondition=[];
        $user=$this->switchUser();
        if(isset($metadataObject->sharedViewId)  && isset($user->id)){
            $rlsCount=RoleLevelSecurity::on($this->switchDB())->where('public_view_id',$metadataObject->sharedViewId)->count();
            if($rlsCount){
                $users=User::on($this->switchDB())->where('id',$user->id)->with('group')->first();
                foreach ($users->group as $group){
                    if(isset($group->group_id)){
                        $rlsUserGroup=RoleLevelSecurityUserGroup::on($this->switchDB())->where('user_group_id',$group->group_id)->first();
                        if(isset($rlsUserGroup->rls_id)){
                            $rls=RoleLevelSecurity::on($this->switchDB())->where('id',$rlsUserGroup->rls_id)->where('public_view_id',$metadataObject->sharedViewId)->first();
                            if(isset($rls->conditionObj)){
                                array_push($roleCondition,$rls->conditionObj);
                            }
                        }
                    }
                }
                if(isset($roleCondition)){
                    $condition=RoleLevelSecurity::on($this->switchDB())->where('public_view_id',$metadataObject->sharedViewId)->with(['rls_user'=>function($query) use ($user){
                        $query->where('user_id',$user->id);
                    }])->get();
                    foreach ($condition as $where){
                        if(isset($where->rls_user)){
                            array_push($roleCondition,$where->conditionObj);
                        }
                    }
                }
            }
            $metadataObject->connObject->exCondition=$roleCondition;
        }
        if(!isset($connection->datasourceType) && !isset($metadataObject->connObject->type)){
            $connection->datasourceType='mysql';
        }
        //For BLending metadata
        if(isset($metadataObject->connObject->type)){

            $connection=[];
            $connection['datasourceType']=env('MDB_CONNECTION');
            $connection['dbname']=$mongoDb->db_name;
            $connection['host']=env('MDB_HOST');
            $connection['port']=env('MDB_PORT');
            $connection['username']=env('MDB_USERNAME');
            $connection['password']=env('MDB_PASSWORD');
            $connection=(Object)$connection;
        }
        switch ($connection->datasourceType) {
            case "mysql":
                $metadata = (new MySQL())->connect($connection);
                break;
            case "mongodb":
                $metadata = (new MongoDB())->connect($connection);
                break;

            case "mssql":
                $metadata = (new MsSQL())->connect($connection);
                break;
            case "excel":
                $metadata = (new ExcelDs())->connect($connection);
                break;
            case "postgres":
                $metadata = (new PostGres())->connect($connection);
                break;
            case "oracle":
                $metadata = (new Oracle())->connect($connection);
                break;
        }
        if(isset($metadataObject->connObject) && isset($metadataObject->metadataId))
            $metadataObject->connObject->metadataId=$metadataObject->metadataId;
        return Response::json($metadata->getDataDashboard($metadataObject->connObject));
    }
    /*
     * Get column Name
     */
    public function getColumn(){
        $metadata="";
        $datasourceObject=json_decode(Input::get('connObject'));
        if(Controller::datasourceCheck($datasourceObject->datasource_id)){
            $datasourceObject=Controller::datasourceCheck($datasourceObject->datasource_id);
        }
        switch ($datasourceObject->datasourceType) {
            case "mysql":
                $metadata = (new MySQL())->connect($datasourceObject);
                break;
            case "mongodb":
                $metadata = (new MongoDB())->connect($datasourceObject);
                break;
            case "mssql":
                $metadata = (new MsSQL())->connect($datasourceObject);
                break;
            case "excel":
                $metadata = (new ExcelDs())->connect($datasourceObject);
                break;
            case "postgres":
                $metadata = (new PostGres())->connect($datasourceObject);
                break;
            case "oracle":
                $metadata = (new Oracle())->connect($datasourceObject);
                break;
        }
        return Response::json($metadata->getColumnName(Input::get('tableName')));
    }
    /*
     *********************************core functions Metadata***************************************************************
     */
    //Get all Datasource name
    public function metadataList()
    {
        try{
            $temp=array();
            $user=User::on($this->switchDB())->find(Auth::user()->user_id);
            if($user->hasRole([ 'Admin']) || $user->hasRole(["Super Admin"])) {
                //.report.group.userGroup
                $dList1 = UserGroup::on($this->switchDB())->with("reportGroup.reportGroup.metadataGroup.metadataDetails")->get();
                foreach ($dList1 as $metadata) {
                    foreach ($metadata->reportGroup as $reportGrp) {
                        foreach ($reportGrp->reportGroup->metadataGroup as $sharedviewReport) {
                            if (isset($sharedviewReport->metadataDetails[0]) && $sharedviewReport->user_group_id==$metadata->id) {
                                if (!isset($metadata->name)) {
                                    $temp[$metadata->name] = [];
                                }
                                if (!isset($temp[$metadata->name][$reportGrp->reportGroup->name])) {
                                    $temp[$metadata->name][$reportGrp->reportGroup->name] = [];
                                }
                                array_push($temp[$metadata->name][$reportGrp->reportGroup->name], $sharedviewReport->metadataDetails[0]);
                            }
                        }
                    }
                }
                //Public View
                //->with("reportGroup.group.group.userGroup")
                $dList2 = Metadata::on($this->switchDB())->where('group_type','public_group')->Wherenull('type')->orWhere('type','')->get();
                foreach($dList2 as $publicview){
                    if (!isset($temp['Public View'])) {
                        $temp['Public View']=[];
                    }
                    array_push($temp['Public View'],$publicview);
                }
                $data=$temp;
            }else{
                $dList1 = Metadata::on($this->switchDB())->WhereNull('type')->orWhere('type','')->with(["reportGroup.group.group.userGroup"=> function($query) use ($user){
                    $query->where("user_id", $user->id);
                }])->get();
                foreach ($dList1 as $metadata) {
                    if (isset($metadata->reportGroup[0]) && isset($metadata->reportGroup[0]->group)) {
                        foreach ($metadata->reportGroup as $reportGrp) {
                            foreach ($reportGrp->group->group as $grpArr) {
                                if (isset($grpArr->userGroup[0])) {
                                    if (isset($reportGrp->group->name) && $grpArr->userGroup[0]->userGroup->id==$reportGrp->user_group_id) {
                                        if (!isset($temp[$grpArr->userGroup[0]->userGroup->name])) {
                                            $temp[$grpArr->userGroup[0]->userGroup->name] = [];
                                        }
                                        if (!isset($temp[$grpArr->userGroup[0]->userGroup->name][$reportGrp->group->name])) {
                                            $temp[$grpArr->userGroup[0]->userGroup->name][$reportGrp->group->name] = [];
                                        }
                                        array_push($temp[$grpArr->userGroup[0]->userGroup->name][$reportGrp->group->name], $metadata);
                                    }
                                }
                            }
                        }
                    }
                }
                //Public View
                $dList2 = Metadata::on($this->switchDB())->where('group_type','public_group')->Wherenull('type')->orWhere('type','')->get();
                foreach($dList2 as $publicview){
                    if (!isset($temp['Public View'])) {
                        $temp['Public View']=[];
                    }
                    array_push($temp['Public View'],$publicview);
                }
                $data=$temp;
            }
            return Response::json([
                'errorCode' => 1,
                'message'=>'Get successfully',
                'result'=> $data
            ]);
        }catch (Exception $e){
            return Response::json([
                'errorCOde' => 0,
                'message'=>$e->getMessage(),
                'result'=> ""
            ]);
        }

    }
    /*
     * Dashboard metadata list and blending
     */
    public function dashboardMetadataList()
    {
        try{
            $metadata = Metadata::on($this->switchDB())->select('id','metadata_id','name')->orderBy('id', 'desc')->get();
            $metadataList = [];
            foreach($metadata as $ro){
                $array = array(
                    "name" => $ro->name,
                    "metadataId" => $ro->metadata_id
                );
                array_push($metadataList, $array);
            }
            return Response::json([
                'errorCode' => 1,
                'message'=>'Get successfully',
                'result'=> json_encode($metadataList)
            ]);
        }catch (Exception $e){
            return Response::json([
                'errorCOde' => 0,
                'message'=>$e->getMessage(),
                'result'=> ""
            ]);
        }
    }
    /*
     * Save metadata object to database
     */
    public function save(){
        try{
            $response=[];
            if(isset(Input::get('reportGroup')['name']) && Controller::folderValidation(Input::get('reportGroup')['name'])){
                return Response::json([
                    'errorCode' => 0,
                    'message'=>'Folder already exist',
                    'result'=> "",
                ]);
            }
            $metadataCount=Metadata::on($this->switchDB())->where("name",Input::get('metadataName'))->count();
            if($metadataCount==0){
                $ref_id="MD".self::generateRandomString();
                $metadata=Metadata::on($this->switchDB())->create(
                    [
                        "name" => Input::get('metadataName'),
                        "metadata_id"=>$ref_id,
                        "userid" => Auth::user()->id,
                        "metadataObject" =>Input::get('metadataObject'),
                        "group" => Input::get('groupObject'),
                        "group_type"=>Input::get('publicViewType')
                    ]
                );
                if(Input::get('publicViewType')!='public_group') {
                    if (Input::get('reportGroup')['grpType'] == "both") {
                        foreach (Input::get('reportGroup')['reportGroup'] as $reportGrpObj) {
                            $reportGrpObj = json_decode($reportGrpObj);
                            MetadataReportGroup::on($this->switchDB())->create([
                                "report_group_id" => $reportGrpObj->ReportGrpid,
                                "metadata_id" => $metadata->id,
                                "user_group_id"=>$reportGrpObj->id
                            ]);
                        }

                        $reportGroup = ReportGroup::on($this->switchDB())->create([
                            "name" => Input::get('reportGroup')['name'],
                            "description" => Input::get('reportGroup')['name'],
                        ]);
                        /*
                         * Report user group
                         */
                        foreach (Input::get('reportGroup')['userGroup'] as $group) {
                            ReportUserGroup::on($this->switchDB())->create([
                                "report_group_id" => $reportGroup->id,
                                "user_group_id" => $group
                            ]);
                            MetadataReportGroup::on($this->switchDB())->create([
                                "report_group_id" => $reportGroup->id,
                                "metadata_id" => $metadata->id,
                                "user_group_id"=>$group
                            ]);
                        }

                    }else if(Input::get('reportGroup')['grpType'] == "selectGrp"){
                        foreach (Input::get('reportGroup')['reportGroup'] as $reportGrpObj) {
                            $reportGrpObj = json_decode($reportGrpObj);
                            MetadataReportGroup::on($this->switchDB())->create([
                                "report_group_id" => $reportGrpObj->ReportGrpid,
                                "metadata_id" => $metadata->id,
                                "user_group_id"=>$reportGrpObj->id
                            ]);
                        }
                    }else{
                        $reportGroup = ReportGroup::on($this->switchDB())->create([
                            "name" => Input::get('reportGroup')['name'],
                            "description" => Input::get('reportGroup')['name'],
                        ]);
                        /*
                         * Report user group
                         */
                        foreach (Input::get('reportGroup')['userGroup'] as $group) {
                            ReportUserGroup::on($this->switchDB())->create([
                                "report_group_id" => $reportGroup->id,
                                "user_group_id" => $group
                            ]);
                            MetadataReportGroup::on($this->switchDB())->create([
                                "report_group_id" => $reportGroup->id,
                                "metadata_id" => $metadata->id,
                                "user_group_id"=>$group
                            ]);
                        }
                    }
                }
                $response['errorCode']=1;
                $response['message']="Metadata Save Successfuly";
                $response['data']=$metadata->metadata_id;
            }else{
                $response['errorCode']=0;
                $response['message']="Metadata name already exist";
                $response['data']="";
            }
            return $response;
        }catch (Exception $e){
            $statusCode=500;
            $response['errorCode']=0;
            $response['message']="Check your connection";
            $response['data']="";
            return $response;
        }
    }
    /*
     * Metadata update
     */
    public function update(){
        $metadataCount=Metadata::on($this->switchDB())->where('name',Input::get('metadataName'))->count();
        $metadataData=Metadata::on($this->switchDB())->where('name',Input::get('metadataName'))->first();
        if(((isset($metadataData) && $metadataData->metadata_id==Input::get('id'))) || $metadataCount==0){
            $metadata=Metadata::on($this->switchDB())->where('metadata_id',Input::get('id'))->first();
            $metadata->name=Input::get('metadataName');
            $metadata->metadataObject=Input::get('metadataObject');
            $metadata->group = Input::get('groupObject');
            if($metadata->save()){
                return Response::json([
                    'errorCode' => 1,
                    'message'=>'Metadata update successfully',
                    'result'=> ''
                ]);
            }
        }
        return Response::json([
            'error' => 0,
            'message'=>'Metadata name already exist',
            'result'=> ''
        ]);
    }
    /*
     * Metadata delete using id
     */
    public function metadataDelete(){
        $metadata=Metadata::on($this->switchDB())->where('metadata_id',Input::get('id'))->first();
        if($metadata->delete()){
            return Response::json([
                'errorCode' => 1,
                'message'=>'Metadata delete successfully',
                'result'=> ""
            ]);
        }
        return Response::json([
            'errorCode' => 0,
            'message'=>'Check your connection',
            'result'=> ""
        ]);
    }
    /*
     * All Metadata for list
     */
    public function getMetadataObject(){
        $metadata=Metadata::on($this->switchDB())->where('metadata_id',Input::get('metadataId'))->first();
        if($metadata){
            return Response::json([
                'errorCode' => 1,
                'message'=>'Metadata object get successfully',
                'result'=> $metadata
            ]);
        }
        return Response::json([
            'errorCode' => 0,
            'message'=>'Check your connection',
            'result'=> ""
        ]);
    }
    /*
     * Table index
     */
    public function tableIndexs(){
        $metadata=null;
        $sourceObject=json_decode(Input::get('connObject'));
        if(Controller::datasourceCheck($sourceObject->datasource_id)){
            $sourceObject=Controller::datasourceCheck($sourceObject->datasource_id);
        }
        switch ($sourceObject->datasourceType) {
            case "mysql":
                $metadata = (new MySQL())->connect($sourceObject);
                break;
            case "Mongodb":
                echo "i equals 1";
                break;
            case "mssql":
                $metadata = (new MsSQL())->connect($sourceObject);
                break;
            case "excel":
                $metadata = (new ExcelDs())->connect($sourceObject);
                break;
            case "postgres":
                $metadata = (new PostGres())->connect($sourceObject);
                break;
            case "oracle":
                $metadata = (new Oracle())->connect($sourceObject);
                break;

        }
        return Response::json($metadata->tableIndexsList(Input::get('tableName')));
    }
    public function tableIndexsDelete(){
        $metadata=null;
        $sourceObject=json_decode(Input::get('connObject'));
        if(Controller::datasourceCheck($sourceObject->datasource_id)){
            $sourceObject=Controller::datasourceCheck($sourceObject->datasource_id);
        }
        switch ($sourceObject->datasourceType) {
            case "mysql":
                $metadata = (new MySQL())->connect($sourceObject);
                break;
            case "Mongodb":
                echo "i equals 1";
                break;
            case "mssql":
                $metadata = (new MsSQL())->connect($sourceObject);
                break;
            case "excel":
                $metadata = (new ExcelDs())->connect($sourceObject);
                break;
            case "postgres":
                $metadata = (new PostGres())->connect($sourceObject);
                break;
            case "oracle":
                $metadata = (new Oracle())->connect($sourceObject);
                break;
        }
        return Response::json($metadata->tableIndexsListDelete(Input::get('indexColumn')));
    }
    public function tableIndexsCreate(){
        $metadata=null;
        $sourceObject=json_decode(Input::get('connObject'));
        if(Controller::datasourceCheck($sourceObject->datasource_id)){
            $sourceObject=Controller::datasourceCheck($sourceObject->datasource_id);
        }
        switch ($sourceObject->datasourceType) {
            case "mysql":
                $metadata = (new MySQL())->connect($sourceObject);
                break;
            case "Mongodb":
                echo "i equals 1";
                break;
            case "mssql":
                $metadata = (new MsSQL())->connect($sourceObject);
                break;
            case "excel":
                $metadata = (new ExcelDs())->connect($sourceObject);
                break;
            case "postgres":
                $metadata = (new PostGres())->connect($sourceObject);
                break;
            case "oracle":
                $metadata = (new Oracle())->connect($sourceObject);
                break;
        }
        return Response::json($metadata->tableIndexsListCreate(Input::get('tableIndex'),Input::get('tableName')));
    }
    public function getMultiColumn(){
        $metadata="";
        $datasourceObject=json_decode(Input::get('connObject'));

        if(Controller::datasourceCheck($datasourceObject->datasource_id)){
            $datasourceObject=Controller::datasourceCheck($datasourceObject->datasource_id);
        }

        switch ($datasourceObject->datasourceType) {
            case "mysql":
                $metadata = (new MySQL())->connect($datasourceObject);
                break;
            case "mongodb":
                $metadata = (new MongoDB())->connect($datasourceObject);
                break;
            case "mssql":
                $metadata = (new MsSQL())->connect($datasourceObject);
                break;
            case "excel":
                $metadata = (new ExcelDs())->connect($datasourceObject);
                break;
            case "postgres":
                $metadata = (new PostGres())->connect($datasourceObject);
                break;
            case "oracle":
                $metadata = (new Oracle())->connect($datasourceObject);
                break;
        }

        return Response::json($metadata->getColumnMultiTable(Input::get('tableName')));
    }
    /*
     * Get Primary key
     */
    public function getPrimarykey(){
        $metadata=null;
        $sourceObject=json_decode(Input::get('connObject'));
        if(Controller::datasourceCheck($sourceObject->datasource_id)){
            $sourceObject=Controller::datasourceCheck($sourceObject->datasource_id);
        }
        switch ($sourceObject->datasourceType) {
            case "mysql":
                $metadata = (new MySQL())->connect($sourceObject);
                break;
            case "Mongodb":
                echo "i equals 1";
                break;
            case "mssql":
                $metadata = (new MsSQL())->connect($sourceObject);
                break;
            case "excel":
                $metadata = (new ExcelDs())->connect($sourceObject);
                break;
            case "postgres":
                $metadata = (new PostGres())->connect($sourceObject);
                break;
            case "oracle":
                $metadata = (new Oracle())->connect($sourceObject);
                break;
        }
        return Response::json($metadata->getPrimarykey(Input::get('tableName')));
    }
    public function createPrimarykey(){
        $metadata=null;
        $sourceObject=json_decode(Input::get('connObject'));
        if(Controller::datasourceCheck($sourceObject->datasource_id)){
            $sourceObject=Controller::datasourceCheck($sourceObject->datasource_id);
        }
        switch ($sourceObject->datasourceType) {
            case "mysql":
                $metadata = (new MySQL())->connect($sourceObject);
                break;
            case "Mongodb":
                echo "i equals 1";
                break;
            case "mssql":
                $metadata = (new MsSQL())->connect($sourceObject);
                break;
            case "excel":
                $metadata = (new ExcelDs())->connect($sourceObject);
                break;
            case "postgres":
                $metadata = (new PostGres())->connect($sourceObject);
                break;
            case "oracle":
                $metadata = (new Oracle())->connect($sourceObject);
                break;
        }
        return Response::json($metadata->createPrimarykey(Input::get('tableName'),Input::get('columns')));
    }
    public function deletePrimarykey(){
        $metadata=null;
        $sourceObject=json_decode(Input::get('connObject'));
        if(Controller::datasourceCheck($sourceObject->datasource_id)){
            $sourceObject=Controller::datasourceCheck($sourceObject->datasource_id);
        }
        switch ($sourceObject->datasourceType) {
            case "mysql":
                $metadata = (new MySQL())->connect($sourceObject);
                break;
            case "Mongodb":
                echo "i equals 1";
                break;
            case "mssql":
                $metadata = (new MsSQL())->connect($sourceObject);
                break;
            case "excel":
                $metadata = (new ExcelDs())->connect($sourceObject);
                break;
            case "postgres":
                $metadata = (new PostGres())->connect($sourceObject);
                break;
            case "oracle":
                $metadata = (new Oracle())->connect($sourceObject);
                break;
        }
        return Response::json($metadata->deletePrimarykey(Input::get('tableName')));
    }
    /*
     * Blending multiple column
     */
    function getBlendingMultiColumn(){
        $metadataId=Input::get('metadataId');
        $metadata=null;
        $metadataColumn=[];
        try{
            $tempMetadata=[];
            foreach ($metadataId as $id) {
                $tempMetaDataGet = Metadata::on($this->switchDB())->select("metadataObject", "name")->where('metadata_id',$id)->first();
                $metadataObj=json_decode($tempMetaDataGet->metadataObject);
                /*
                 * Datasource details change check
                 */
                $sourceObject=$metadataObj->connectionObject;
                if(Controller::datasourceCheck($sourceObject->datasource_id)){
                    $sourceObject=Controller::datasourceCheck($sourceObject->datasource_id);
                }
                $metadataObj->connectionObject=$sourceObject;
                $tempMetaDataGet->metadataObject=json_encode($metadataObj);
                array_push($tempMetadata, $tempMetaDataGet);
            }
            foreach ($tempMetadata as $metaDataGet){
                $metadataObj=json_decode($metaDataGet->metadataObject);
                $tableArray=[];
                array_push($tableArray,$metadataObj->rootTable);
                foreach ($metadataObj->joinsDetails as $key=>$value){
                    array_push($tableArray,$key);
                }
                $sourceObject=$metadataObj->connectionObject;
                switch ($sourceObject->datasourceType) {
                    case "mysql":
                        $metadata = (new MySQL())->connect($sourceObject);
                        break;
                    case "mongodb":
                        $metadata = (new MongoDB())->connect($sourceObject);
                        break;
                    case "mssql":
                        $metadata = (new MsSQL())->connect($sourceObject);
                        break;
                    case "excel":
                        $metadata = (new ExcelDs())->connect($sourceObject);
                        break;
                    case "postgres":
                        $metadata = (new PostGres())->connect($sourceObject);
                        break;
                    case "oracle":
                        $metadata = (new Oracle())->connect($sourceObject);
                        break;
                }
                $metadataColumn[$metaDataGet->name]=[];
                $metadataColumn[$metaDataGet->name]['columns']=($metadata->getColumnMultiTable($tableArray))->result;
                $metadataColumn[$metaDataGet->name]['type']=$sourceObject->datasourceType;
            }
            return Response::json([
                "errorCode"=>1,
                "result"=>$metadataColumn,
                "message"=>"",
            ]);
        }catch (Exception $e){
            return Response::json([
                "errorCode"=>0,
                "result"=>"",
                "message"=>"Check your connection"
            ]);
        }
    }
    public function getBlendingQueryData(){
        $metadataId=Input::get('metadataId');
        $metadataArray=[];
        $tempMetadataArray=[];
        foreach ($metadataId as $id) {
            $metadata = "";
            $metadataObjTemp = Metadata::on($this->switchDB())->select("metadataObject", "name")->where('metadata_id', $id)->first();
            $metadataObject=json_decode($metadataObjTemp->metadataObject);
            $connection = $metadataObject->connectionObject;
            if(Controller::datasourceCheck($connection->datasource_id)){
                $connection=Controller::datasourceCheck($connection->datasource_id);
            }
            $metadataObject->connectionObject=$connection;
            $metadataObjTemp->metadataObject=json_encode($metadataObject);
            array_push($tempMetadataArray,$metadataObjTemp);
        }
        foreach ($tempMetadataArray as $metadataObj){
            $metadataObject=json_decode($metadataObj->metadataObject);
            $connection = $metadataObject->connectionObject;
            switch ($connection->datasourceType) {
                case "mysql":
                    $metadata = (new MySQL())->connect($connection);
                    break;
                case "mongodb":
                    $metadata = (new MongoDB())->connect($connection);
                    break;
                case "mssql":
                    $metadata = (new MsSQL())->connect($connection);
                    break;
                case "excel":
                    $metadata = (new ExcelDs())->connect($connection);
                    break;
                case "postgres":
                    $metadata = (new PostGres())->connect($connection);
                    break;
                case "oracle":
                    $metadata = (new Oracle())->connect($connection);
                    break;
            }
            $metadataObject->blending=true;
            $metadataArray[$metadataObj->name]=($metadata->getData($metadataObject))->result;
        }
        return Response::json([
            "errorCode"=>"1",
            "result"=>$metadataArray,
            "message"=>""
        ]);
    }
    public function blendingSaveData(){
        $mongoDb=CompanyDetail::where('uid',Auth::user()->company_id)->first();
        $metadataResult=Metadata::on($this->switchDB())->find(Input::get('metadataId'));
        $metadataGetObj=json_decode($metadataResult->metadataObject);
        $metadataId=$metadataGetObj->selectedMetadata;
        $metadataArray=[];
        $tempMetadataArray=[];
        foreach ($metadataId as $id) {
            $metadata = "";
            $metadataObjTemp = Metadata::on($this->switchDB())->select("metadataObject", "name")->where('metadata_id',$id)->first();
            $metadataObject=json_decode($metadataObjTemp->metadataObject);
            $connection = $metadataObject->connectionObject;
            if(Controller::datasourceCheck($connection->datasource_id)){
                $connection=Controller::datasourceCheck($connection->datasource_id);
            }
            $metadataObject->connectionObject=$connection;
            $metadataObjTemp->metadataObject=json_encode($metadataObject);
            array_push($tempMetadataArray,$metadataObjTemp);
        }
        foreach ($tempMetadataArray as $metadataObj){
            $metadataObject=json_decode($metadataObj->metadataObject);
            $connection = $metadataObject->connectionObject;
            switch ($connection->datasourceType) {
                case "mysql":
                    $metadata = (new MySQL())->connect($connection);
                    break;
                case "mongodb":
                    $metadata = (new MongoDB())->connect($connection);
                    break;
                case "mssql":
                    $metadata = (new MsSQL())->connect($connection);
                    break;
                case "excel":
                    $metadata = (new ExcelDs())->connect($connection);
                    break;
                case "postgres":
                    $metadata = (new PostGres())->connect($connection);
                    break;
                case "oracle":
                    $metadata = (new Oracle())->connect($connection);
                    break;
            }
            $metadataArray[$metadataObj->name]=($metadata->getDataDashboard($metadataObject))->result;
        }

        foreach($metadataArray as $key=>$value){
            $connection=[];
            $connection['datasourceType']=env('MDB_CONNECTION');
            $connection['dbname']=$mongoDb->db_name;
            $connection['host']=env('MDB_HOST');
            $connection['port']=env('MDB_PORT');
            $connection['username']=env('MDB_USERNAME');
            $connection['password']=env('MDB_PASSWORD');
            $connection=(Object)$connection;
            $metadata = (new MongoDB())->connect($connection);
            $metadata->saveData($key,(Object)$value,Input::get('metadataId'),$metadataGetObj->dataTypeCheck);
        }
        return Response::json([
            "errorCode"=>"1",
            "result"=>"",
            "message"=>"Successfully"
        ]);
    }
    public function blendingSave(){
        try{
            if(isset(Input::get('reportGroup')['name']) && Controller::folderValidation(Input::get('reportGroup')['name'])){
                return Response::json([
                    'errorCode' => 0,
                    'message'=>'Folder already exist',
                    'result'=> "",
                ]);
            }
            $response=[];
            $metadataCount=Metadata::on($this->switchDB())->where("name",Input::get('metadataName'))->count();
            if($metadataCount==0){
                $ref_id="MD".self::generateRandomString();
                $metadata=Metadata::on($this->switchDB())->create(
                    [
                        "name" => Input::get('metadataName'),
                        "metadata_id"=>$ref_id,
                        "type" => "blending",
                        "group_type"=>Input::get('publicViewType'),
                        "userid" => Auth::user()->id,
                        "metadataObject" =>Input::get('metadataObject')
                    ]
                );
                $metadata->save();
                if(Input::get('publicViewType')!='public_group') {
                    if (isset(Input::get('reportGroup')['reportGroup'])) {
                        MetadataReportGroup::on($this->switchDB())->create([
                            "report_group_id" => Input::get('reportGroup')['reportGroup'],
                            "metadata_id" => $metadata->id
                        ]);
                    } else {
                        $reportGroup = ReportGroup::on($this->switchDB())->create([
                            "name" => Input::get('reportGroup')['name'],
                            "description" => Input::get('reportGroup')['name'],
                        ]);
                        /*
                         * Report user group
                         */
                        foreach (Input::get('reportGroup')['userGroup'] as $group) {
                            ReportUserGroup::on($this->switchDB())->create([
                                "report_group_id" => $reportGroup->id,
                                "user_group_id" => $group
                            ]);
                        }
                        MetadataReportGroup::on($this->switchDB())->create([
                            "report_group_id" => $reportGroup->id,
                            "metadata_id" => $metadata->id
                        ]);
                    }
                }
                $response['errorCode']=1;
                $response['message']="Metadata Save Successfuly";
                $response['result']=$metadata->id;
            }else{
                $response['errorCode']=0;
                $response['message']="Metadata name already exist";
                $response['result']="";
            }
        }catch (Exception $e){
            $statusCode=500;
            $response['errorCode']=0;
            $response['message']="Check your connection";
            $response['result']="";
        }
        return Response::json($response);
    }
    public function blendingList(){
        try{
            $temp=array();
            $user=$this->switchUser();
            if($user->hasRole([ 'Admin']) || $user->hasRole(["Super Admin"])) {
                //.report.group.userGroup
                $dList1 = Metadata::on($this->switchDB())->where('type','blending')->with("reportGroup.group.group.userGroup")->get();
                foreach($dList1 as $publicview){
                    if(isset($publicview->reportGroup[0]) && isset($publicview->reportGroup[0]->group)){
                        foreach($publicview->reportGroup as $reportGrp){
                            if(isset($reportGrp->group->name)){
                                if (!isset($temp[$reportGrp->group->name])) {
                                    $temp[$reportGrp->group->name]=[];
                                }
                                if(isset($temp[$reportGrp->group->name]))
                                    array_push($temp[$reportGrp->group->name],$publicview);
                            }
                        }
                    }
                }
                //Public View
                $dList2 = Metadata::on($this->switchDB())->where('group_type','public_group')->where('type','blending')->with("reportGroup.group.group.userGroup")->get();
                foreach($dList2 as $publicview){
                    if (!isset($temp['public_group'])) {
                        $temp['public_group']=[];
                    }
                    array_push($temp['public_group'],$publicview);
                }
                $data=$temp;
            }else{
                $dList1 = Metadata::on($this->switchDB())->where('type','blending')->with(["reportGroup.group.group.userGroup"=> function($query) use ($user){
                    $query->where("user_id", $user->id);
                }])->get();
                foreach($dList1 as $publicview){
                    if(isset($publicview->reportGroup[0]) && isset($publicview->reportGroup[0]->group)){
                        foreach($publicview->reportGroup as $reportGrp){
                            if(isset($reportGrp->group->name)){
                                if (!isset($temp[$reportGrp->group->name]) && isset($reportGrp->group->group[0]->userGroup) && count($reportGrp->group->group[0]->userGroup)) {
                                    $temp[$reportGrp->group->name]=[];
                                }
                                if(isset($temp[$reportGrp->group->name]))
                                    array_push($temp[$reportGrp->group->name],$publicview);
                            }
                        }
                    }
                }
                //Public View
                $dList2 = Metadata::on($this->switchDB())->where('type','blending')->where('group_type','public_group')->with("reportGroup.group.group.userGroup")->get();
                foreach($dList2 as $publicview){
                    if (!isset($temp['public_group'])) {
                        $temp['public_group']=[];
                    }
                    array_push($temp['public_group'],$publicview);
                }
                $data=$temp;
            }
            return Response::json([
                'errorCode' => 1,
                'message'=>'Get successfully',
                'result'=> $data
            ]);
        }catch (Exception $e){
            return Response::json([
                'errorCOde' => 0,
                'message'=>$e->getMessage(),
                'result'=> ""
            ]);
        }
    }
    public function blendingUpdate(){
        $metadataData=Metadata::on($this->switchDB())->where('name',Input::get('metadataName'))->first();
        $metadataCount=Metadata::on($this->switchDB())->where('name',Input::get('metadataName'))->count();
        if(((isset($metadataData) && $metadataData->metadata_id==Input::get('id'))) || $metadataCount==0){
            $metadata=Metadata::on($this->switchDB())->where('metadata_id',Input::get('id'))->first();
            $metadata->name=Input::get('metadataName');
            $metadata->metadataObject=Input::get('metadataObject');
            if($metadata->save()){
                return Response::json([
                    'errorCode' => 1,
                    'message'=>'Metadata update successfully',
                    'result'=> ''
                ]);
            }
        }
        return Response::json([
            'error' => 0,
            'message'=>'Metadata name already exist',
            'result'=> ''
        ]);
    }
    /*
     * Update metadata
     */
    public function updateDataType(){
        $tableObj=Input::get('tableObj');
        foreach ($tableObj as $key=>$value){
            $query="Alter TABLE ".$key;
            $i=0;
            foreach ($value as $colObj){
                $columnName=explode("(",$colObj['Field']);
                if($i==0){
                    $query .=" MODIFY COLUMN  `".$columnName[0]."` ".$colObj['Type'];
                }else{
                    $query .=" , MODIFY COLUMN  `".$columnName[0]."` ".$colObj['Type'];
                }
                $i++;
            }
            $connection=[];
            $connection['datasourceType']=env('EXCEL_CONNECTION');
            $connection['dbname']=env('EXCEL_DB');
            $connection['host']=env('EXCEL_HOST');
            $connection['port']=env('EXCEL_PORT');
            $connection['username']=env('EXCEL_USERNAME');
            $connection['password']=env('EXCEL_PASSWORD');
            $connection=(Object)$connection;
            $metadata = (new ExcelDs())->connect($connection);
            return Response::json($metadata->dataTypeChange($query));
        }
    }

    public function syncBlendingData(){
        $id=Input::get('id');
        $mongoDb=CompanyDetail::where('uid',Auth::user()->company_id)->first();
        $metadata=Metadata::on($this->switchDB())->where('metadata_id',$id)->first();
        $selectedMetadata=json_decode($metadata->metadataObject)->selectedMetadata;
        $tempMetadataArray=[];
        foreach ($selectedMetadata as $id) {
            $metadata = "";
            $metadataObjTemp = Metadata::on($this->switchDB())->select("metadataObject", "name")->where('metadata_id',$id)->first();
            array_push($tempMetadataArray,$metadataObjTemp);
        }
        foreach ($tempMetadataArray as $metadataObj){
            $metadataObject=json_decode($metadataObj->metadataObject);
            $connection = $metadataObject->connectionObject;
            if(Controller::datasourceCheck($connection->datasource_id)){
                $connection=Controller::datasourceCheck($connection->datasource_id);
            }
            switch ($connection->datasourceType) {
                case "mysql":
                    $metadata = (new MySQL())->connect($connection);
                    break;
                case "mongodb":
                    $metadata = (new MongoDB())->connect($connection);
                    break;
                case "mssql":
                    $metadata = (new MsSQL())->connect($connection);
                    break;
                case "excel":
                    $metadata = (new ExcelDs())->connect($connection);
                    break;
                case "postgres":
                    $metadata = (new PostGres())->connect($connection);
                    break;
                case "oracle":
                    $metadata = (new Oracle())->connect($connection);
                    break;
            }
            $metadataArray[$metadataObj->name]=($metadata->getDataDashboard($metadataObject))->result;
        }
        foreach($metadataArray as $key=>$value){
            $connection=[];
            $connection['datasourceType']=env('MDB_CONNECTION','mongodb');
            $connection['dbname']=$mongoDb->db_name;
            $connection['host']=env('MDB_HOST','101.53.130.66');
            $connection['port']=env('MDB_PORT','27017');
            $connection['username']=env('MDB_USERNAME','admin');
            $connection['password']=env('MDB_PASSWORD','hellothinklayer1');
            $connection=(Object)$connection;
            $metadata = (new MongoDB())->connect($connection);
            $metadata->syncData($key,(Object)$value,Input::get('metadataId'),"");
        }
        //BlendingData::dispatch(Input::get('metadataId'),$connection);
        return Response::json([
            "errorCode"=>"1",
            "result"=>"",
            "message"=>"Successfully"
        ]);
    }


    public function renameData(){
        $metadataId=Input::get('metadataId');
        $metadataObject=Input::get('metadataObject');
        $metadataObj=Metadata::on($this->switchDB())->where('metadata_id',$metadataId)->first();
        if($metadataObj){
            $metadataObj->metadataObject=$metadataObject;
            $metadataObj->save();
        }
        return Response::json([
            "errorCode"=>"1",
            "result"=>"",
            "message"=>"Column Renamed Successfully"
        ]);
    }






    /*
     * ******************************************************Server Side api Code *********************************************************************
     */
    /*
     *  Dashboard get column from query
     */
    /*
      * Get query to fetch data copy metadata for dashboard
      */
    public function dataCacheToRedis(Request $request){
        $token = $request->bearerToken();
        $metadata="";
        $metadataObject=json_decode(Input::get('matadataObject'));
        /*
         * Get department id
         */
        $department_name=Input::get('departmentName');
        $userGroupObj=UserGroup::on($this->switchDB())->where('name',$department_name)->first();
        // Current limit Apply
        $sharedViewId="";
        if(isset($metadataObject->sharedViewId)){
            $sharedViewId=$metadataObject->sharedViewId;
        }
        $metaCurr=Metadata::on($this->switchDB())->where('metadata_id',$metadataObject->metadataId)->first();
        if($metaCurr){
            $metadataList=json_decode(json_decode($metaCurr)->metadataObject);
            $customColumn=[];
            $temp=[];
            foreach($metadataObject->connObject->column as $column){
                if(!isset($temp[$column->columnName])){
                    if($column->type=='custom'){
                        array_push($customColumn,$column);
                    }
                    $temp[$column->columnName]=true;
                }
            }
            if(isset($customColumn)){
                foreach ($customColumn as $custColumn){
                    array_push($metadataList->column,$custColumn);
                }
            }
            $temp=[];
            $temp['connObject']=$metadataList;
            $temp['name']=$metadataObject->name;
            $temp['metadataId']=$metadataObject->metadataId;
            $metadataObject=(Object)$temp;
            if(isset($metadataObject->connObject->connectionObject)){
                $connection=$metadataObject->connObject->connectionObject;
                if(Controller::datasourceCheck($connection->datasource_id)){
                    $connection=Controller::datasourceCheck($connection->datasource_id);
                }
                $metadataObject->connObject->connectionObject=$connection;
            }

            $roleCondition=[];
            $user=$this->switchUser();
            $userGroup=UserGroupUser::on($this->switchDB())->where('user_id',$user->id)->with('userGroup')->first();
            $department="";
            if($userGroup){
                $department=$userGroup->userGroup['name'];
            }
            $userType=false;
            if($sharedViewId!="" && isset($user->id) && !$user->hasRole([ 'Admin']) && !$user->hasRole([ 'Super Admin']) ){
                $rlsCount=RoleLevelSecurity::on($this->switchDB())->where('public_view_id',$sharedViewId)->count();
                if($rlsCount){
                    /*
                     * User wise role level security
                     */
                    if(isset($roleCondition)){
                        $condition=RoleLevelSecurity::on($this->switchDB())->where('public_view_id',$sharedViewId)->with(['rls_user'=>function($query) use ($user){
                            $query->where('user_id',$user->id);
                        }])->get();
                        foreach ($condition as $where){
                            if(isset($where->rls_user) && isset($where->rls_user) && count($where->rls_user)){
                                array_push($roleCondition,$where->conditionObj);
                            }
                        }
                    }
                    if(count($roleCondition)){
                        $userType=true;
                    }
                    if(count($roleCondition)==0){
                        /*
                         * Department wise role level security if multiple department then both condition with or mode
                         */
                        //user_group_id from front end which folder selected
                        $rlsUserGroups=RoleLevelSecurityUserGroup::on($this->switchDB())->where('user_group_id',$userGroupObj->id)->get();
                        foreach ($rlsUserGroups as $rlsUserGroup){
                            if(isset($rlsUserGroup->rls_id)){
                                $rls=RoleLevelSecurity::on($this->switchDB())->where('id',$rlsUserGroup->rls_id)->where('public_view_id',$sharedViewId)->first();
                                if(isset($rls->conditionObj)){
                                    array_push($roleCondition,$rls->conditionObj);
                                }
                            }
                        }
                    }
                    /*
                     * Role level user wise or department wise
                     */
                    $metadataObject->connObject->userType=$userType;
                    $metadataObject->connObject->exCondition=$roleCondition;
                }
            }
            if($sharedViewId){
                $publicView=PublicView::on($this->switchDB())->find($sharedViewId);
                if(isset(json_decode($publicView->reportObject)->realtime) && json_decode($publicView->reportObject)->realtime->flag){
                    $metadataObject->connObject->realtime=json_decode($publicView->reportObject)->realtime;
                }
            }
            if(!isset($connection->datasourceType) && !isset($metadataObject->connObject->type)){
                $connection->datasourceType='mysql';
            }
            $mongoDb=CompanyDetail::where('uid',Auth::user()->company_id)->first();
            if(isset($metadataObject->connObject->type)){
                $connection=[];
                $connection['datasourceType']=env('MDB_CONNECTION','mongodb');
                $connection['dbname']=$mongoDb->db_name;
                $connection['host']=env('MDB_HOST','101.53.130.66');
                $connection['port']=env('MDB_PORT','27017');
                $connection['username']=env('MDB_USERNAME','admin');
                $connection['password']=env('MDB_PASSWORD','hellothinklayer1');
                $connection=(Object)$connection;
            }
            switch ($connection->datasourceType) {
                case "mysql":
                    $metadata = (new MySQL())->connect($connection);
                    break;
                case "mongodb":
                    $metadata = (new MongoDB())->connect($connection);
                    break;
                case "mssql":
                    $metadata = (new MsSQL())->connect($connection);
                    break;
                case "excel":
                    $metadata = (new ExcelDs())->connect($connection);
                    break;
                case "postgres":
                    $metadata = (new PostGres())->connect($connection);
                    break;
                case "oracle":
                    $metadata = (new Oracle())->connect($connection);
                    break;
            }
            $metadataObject->connObject->metadataId=$metadataObject->metadataId;
            return Response::json($metadata->queryDataToredis($metadataObject->connObject,$metaCurr->incrementObj,$token,$department));
        }else{
            return Response::json([
                'errorCode' => 0,
                'message'=>"Metadata doesn't exists",
                'result'=> ""
            ]);
        }
    }
    /*
     * Datacache for public api
     */
    public function dataCacheToRedisPublic()
    {
        $metadata="";
        $uid=Input::get('comId');
        $token=Input::get('accessToken');
        $companyDetails=CompanyDetail::where('uid',$uid)->first();
        $metadataObject=json_decode(Input::get('matadataObject'));
        $metaCurr=Metadata::on(Controller::switchDBPublicview($companyDetails->db_name))->where('metadata_id',$metadataObject->metadataId)->first();
        if($metaCurr){
            $metadataList=json_decode(json_decode($metaCurr)->metadataObject);
            $customColumn=[];
            $temp=[];
            foreach($metadataObject->connObject->column as $column){
                if(!isset($temp[$column->columnName])){
                    if($column->type=='custom'){
                        array_push($customColumn,$column);
                    }
                    $temp[$column->columnName]=true;
                }
            }
            if(isset($customColumn)){
                foreach ($customColumn as $custColumn){
                    array_push($metadataList->column,$custColumn);
                }
            }
            $temp=[];
            $temp['connObject']=$metadataList;
            $temp['name']=$metadataObject->name;
            $temp['metadataId']=$metadataObject->metadataId;
            $metadataObject=(Object)$temp;
            if(isset($metadataObject->connObject->connectionObject))
                $connection=$metadataObject->connObject->connectionObject;
        }
        if(isset($metadataObject->connObject->connectionObject)){
            $connection=$metadataObject->connObject->connectionObject;
            if(Controller::datasourceCheckPublic($connection->datasource_id,$companyDetails->db_name)){
                $connection=Controller::datasourceCheckPublic($connection->datasource_id,$companyDetails->db_name);
            }
            $metadataObject->connObject->connectionObject=$connection;
        }
        if(!isset($connection->datasourceType) && !isset($metadataObject->connObject->type)){
            $connection->datasourceType='mysql';
        }
        if(isset($metadataObject->connObject->type)){
            $connection=[];
            $connection['datasourceType']=env('MDB_CONNECTION','mongodb');
            $connection['dbname']=$companyDetails->db_name;
            $connection['host']=env('MDB_HOST','101.53.130.66');
            $connection['port']=env('MDB_PORT','27017');
            $connection['username']=env('MDB_USERNAME','admin');
            $connection['password']=env('MDB_PASSWORD','hellothinklayer1');
            $connection=(Object)$connection;
        }
        switch ($connection->datasourceType) {
            case "mysql":
                $metadata = (new MySQL())->connect($connection);
                break;
            case "mongodb":
                $metadata = (new MongoDB())->connect($connection);
                break;
            case "mssql":
                $metadata = (new MsSQL())->connect($connection);
                break;
            case "excel":
                $metadata = (new ExcelDs())->connect($connection);
                break;
            case "postgres":
                $metadata = (new PostGres())->connect($connection);
                break;
            case "oracle":
                $metadata = (new Oracle())->connect($connection);
                break;
        }
        $metadataObject->connObject->metadataId=$metadataObject->metadataId;
        return Response::json($metadata->queryDataToredisPublic($metadataObject->connObject,$metaCurr->incrementObj,$uid,$token));
    }

    /*
     * Query to get data length
     */
    public function getDataLength()
    {
        $metadata="";
        $metadataObject=json_decode(Input::get('matadataObject'));
        /*
         * Get department id
         */
        $department_name=Input::get('departmentName');
        $userGroupObj=UserGroup::on($this->switchDB())->where('name',$department_name)->first();
        // Current limit Apply
        $sharedViewId="";
        if(isset($metadataObject->sharedViewId)){
            $sharedViewId=$metadataObject->sharedViewId;
        }
        $metaCurr=Metadata::on($this->switchDB())->where('metadata_id',$metadataObject->metadataId)->first();
        if($metaCurr){
            $metadataList=json_decode(json_decode($metaCurr)->metadataObject);
            $customColumn=[];
            $temp=[];
            foreach($metadataObject->connObject->column as $column){
                if(!isset($temp[$column->columnName])){
                    if($column->type=='custom'){
                        array_push($customColumn,$column);
                    }
                    $temp[$column->columnName]=true;
                }
            }
            if(isset($customColumn)){
                foreach ($customColumn as $custColumn){
                    array_push($metadataList->column,$custColumn);
                }
            }
            $temp=[];
            $temp['connObject']=$metadataList;
            $temp['name']=$metadataObject->name;
            $temp['metadataId']=$metadataObject->metadataId;
            $metadataObject=(Object)$temp;
            if(isset($metadataObject->connObject->connectionObject)){
                $connection=$metadataObject->connObject->connectionObject;
                if(Controller::datasourceCheck($connection->datasource_id)){
                    $connection=Controller::datasourceCheck($connection->datasource_id);
                }
                $metadataObject->connObject->connectionObject=$connection;
            }

            $roleCondition=[];
            $user=$this->switchUser();
            $userGroup=UserGroupUser::on($this->switchDB())->where('user_id',$user->id)->with('userGroup')->first();
            $department="";
            if($userGroup){
                $department=$userGroup->userGroup['name'];
            }
            $userType=false;
            if($sharedViewId!="" && isset($user->id) && !$user->hasRole([ 'Admin']) && !$user->hasRole([ 'Super Admin']) ){
                $rlsCount=RoleLevelSecurity::on($this->switchDB())->where('public_view_id',$sharedViewId)->count();
                if($rlsCount){
                    /*
                     * User wise role level security
                     */
                    if(isset($roleCondition)){
                        $condition=RoleLevelSecurity::on($this->switchDB())->where('public_view_id',$sharedViewId)->with(['rls_user'=>function($query) use ($user){
                            $query->where('user_id',$user->id);
                        }])->get();
                        foreach ($condition as $where){
                            if(isset($where->rls_user) && isset($where->rls_user) && count($where->rls_user)){
                                array_push($roleCondition,$where->conditionObj);
                            }
                        }
                    }
                    if(count($roleCondition)){
                        $userType=true;
                    }
                    if(count($roleCondition)==0){
                        /*
                         * Department wise role level security if multiple department then both condition with or mode
                         */
                        //user_group_id from front end which folder selected
                        $rlsUserGroups=RoleLevelSecurityUserGroup::on($this->switchDB())->where('user_group_id',$userGroupObj->id)->get();
                        foreach ($rlsUserGroups as $rlsUserGroup){
                            if(isset($rlsUserGroup->rls_id)){
                                $rls=RoleLevelSecurity::on($this->switchDB())->where('id',$rlsUserGroup->rls_id)->where('public_view_id',$sharedViewId)->first();
                                if(isset($rls->conditionObj)){
                                    array_push($roleCondition,$rls->conditionObj);
                                }
                            }
                        }
                    }
                    /*
                     * Role level user wise or department wise
                     */
                    $metadataObject->connObject->userType=$userType;
                    $metadataObject->connObject->exCondition=$roleCondition;
                }
            }
            if($sharedViewId){
                $publicView=PublicView::on($this->switchDB())->find($sharedViewId);
                if(isset(json_decode($publicView->reportObject)->realtime) && json_decode($publicView->reportObject)->realtime->flag){
                    $metadataObject->connObject->realtime=json_decode($publicView->reportObject)->realtime;
                }
            }
            if(!isset($connection->datasourceType) && !isset($metadataObject->connObject->type)){
                $connection->datasourceType='mysql';
            }
            $mongoDb=CompanyDetail::where('uid',Auth::user()->company_id)->first();
            if(isset($metadataObject->connObject->type)){
                $connection=[];
                $connection['datasourceType']=env('MDB_CONNECTION','mongodb');
                $connection['dbname']=$mongoDb->db_name;
                $connection['host']=env('MDB_HOST','101.53.130.66');
                $connection['port']=env('MDB_PORT','27017');
                $connection['username']=env('MDB_USERNAME','admin');
                $connection['password']=env('MDB_PASSWORD','hellothinklayer1');
                $connection=(Object)$connection;
            }
            switch ($connection->datasourceType) {
                case "mysql":
                    $metadata = (new MySQL())->connect($connection);
                    break;
                case "mongodb":
                    $metadata = (new MongoDB())->connect($connection);
                    break;
                case "mssql":
                    $metadata = (new MsSQL())->connect($connection);
                    break;
                case "excel":
                    $metadata = (new ExcelDs())->connect($connection);
                    break;
                case "postgres":
                    $metadata = (new PostGres())->connect($connection);
                    break;
                case "oracle":
                    $metadata = (new Oracle())->connect($connection);
                    break;
            }
            $metadataObject->connObject->metadataId=$metadataObject->metadataId;
            return Response::json($metadata->getDataLength($metadataObject->connObject));
        }else{
            return Response::json([
                'errorCode' => 0,
                'message'=>"Metadata doesn't exists",
                'result'=> ""
            ]);
        }
    }
    /*
     * Query to get data length
     */
    public function getDataLengthPublicView()
    {
        $metadata="";
        $metadataObject=json_decode(Input::get('matadataObject'));
        /*
         * Get department id
         */
        $department_name=Input::get('departmentName');
        $company_id=Input::get('companyId');
        $companyObj = CompanyDetail::where('uid', $company_id)->first();
        // Current limit Apply
        $sharedViewId="";
        if(isset($metadataObject->sharedViewId)){
            $sharedViewId=$metadataObject->sharedViewId;
        }
        $metaCurr=Metadata::on(Controller::switchDBPublicview($companyObj->db_name))->where('metadata_id',$metadataObject->metadataId)->first();
        if($metaCurr){
            $metadataList=json_decode(json_decode($metaCurr)->metadataObject);
            $customColumn=[];
            $temp=[];
            foreach($metadataObject->connObject->column as $column){
                if(!isset($temp[$column->columnName])){
                    if($column->type=='custom'){
                        array_push($customColumn,$column);
                    }
                    $temp[$column->columnName]=true;
                }
            }
            if(isset($customColumn)){
                foreach ($customColumn as $custColumn){
                    array_push($metadataList->column,$custColumn);
                }
            }
            $temp=[];
            $temp['connObject']=$metadataList;
            $temp['name']=$metadataObject->name;
            $temp['metadataId']=$metadataObject->metadataId;
            $metadataObject=(Object)$temp;
            if(isset($metadataObject->connObject->connectionObject)){
                $connection=$metadataObject->connObject->connectionObject;
                if(Controller::datasourceCheckPublic($connection->datasource_id,$companyObj->db_name)){
                    $connection=Controller::datasourceCheckPublic($connection->datasource_id,$companyObj->db_name);
                }
                $metadataObject->connObject->connectionObject=$connection;
            }
            $roleCondition=[];
            $department="";
            $userType=false;
            if($sharedViewId){
                $publicView=PublicView::on(Controller::switchDBPublicview($companyObj->db_name))->find($sharedViewId);
                if(isset(json_decode($publicView->reportObject)->realtime) && json_decode($publicView->reportObject)->realtime->flag){
                    $metadataObject->connObject->realtime=json_decode($publicView->reportObject)->realtime;
                }
            }
            if(!isset($connection->datasourceType) && !isset($metadataObject->connObject->type)){
                $connection->datasourceType='mysql';
            }
            $mongoDb=CompanyDetail::where('uid',$company_id)->first();
            if(isset($metadataObject->connObject->type)){
                $connection=[];
                $connection['datasourceType']=env('MDB_CONNECTION','mongodb');
                $connection['dbname']=$mongoDb->db_name;
                $connection['host']=env('MDB_HOST','101.53.130.66');
                $connection['port']=env('MDB_PORT','27017');
                $connection['username']=env('MDB_USERNAME','admin');
                $connection['password']=env('MDB_PASSWORD','hellothinklayer1');
                $connection=(Object)$connection;
            }
            switch ($connection->datasourceType) {
                case "mysql":
                    $metadata = (new MySQL())->connect($connection);
                    break;
                case "mongodb":
                    $metadata = (new MongoDB())->connect($connection);
                    break;
                case "mssql":
                    $metadata = (new MsSQL())->connect($connection);
                    break;
                case "excel":
                    $metadata = (new ExcelDs())->connect($connection);
                    break;
                case "postgres":
                    $metadata = (new PostGres())->connect($connection);
                    break;
                case "oracle":
                    $metadata = (new Oracle())->connect($connection);
                    break;
            }
            $metadataObject->connObject->metadataId=$metadataObject->metadataId;
            return Response::json($metadata->getDataLength($metadataObject->connObject));
        }else{
            return Response::json([
                'errorCode' => 0,
                'message'=>"Metadata doesn't exists",
                'result'=> ""
            ]);
        }
    }



    /*
     * Metadata cache clear
     */
    public function redisCacheClear(){
        $metadataId=Input::get('id');
        $clientId=Auth::user()->id;
        $redis = Redis::connection();
        $allKeys = $redis->keys('*');
        $response=0;
        $company_id=Auth::user()->company_id;
        $companyDetails=CompanyDetail::where('uid',$company_id)->first();
        $response=Redis::del($metadataId.":".$company_id);
        if(Redis::exists("$metadataId:$company_id-realtime")){
            Redis::del("$metadataId:$company_id-realtime");
        }
        $metadataDetails=Redis::hgetall("metadataDetails");
        $i=0;
        Redis::del("metadataDetails");
        foreach ($metadataDetails as $metadataKey=>$metadataDetail){
            $keyArr=explode(":",$metadataKey);
            if($keyArr[1]==$metadataId || $keyArr[0]==$metadataId){
                array_splice($metadataDetails, $i, 1);
            }else{
                Redis::hset("metadataDetails",$metadataKey,$metadataDetail);
            }
            $i++;
        }
        /*
         * Redis clear metadata
         */

        /*
         * Node clear metadata
         */
        $ip = env("BASE_IP");
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,"$ip:$companyDetails->port/metadataDelete");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS,"metadataId=$metadataId");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $server_output = curl_exec($ch);
        if(json_decode($server_output)->status || $response==1){
            return Response::json([
                "errorCode"=>1,
                "result"=>"",
                "message"=>"Cache Cleared Successfully"
            ]);
        }else{
            return Response::json([
                "errorCode"=>0,
                "result"=>"",
                "message"=>"Cache does not exist"
            ]);
        }
    }
    /*
     * Get condition
     */
    public function RoleLevelcondition(){
        $metadataId=Input::get('metadataId');
        $data=RoleLevelSecurity::on($this->switchDB())->where('metadata_id',$metadataId)->first();
        return Response::json([
            "errorCode"=>"1",
            "result"=>$data,
            "message"=>"Cache not exist"
        ]);
    }
    public function columnCustomGet(){
        $metadataId=Input::get('metadataId');
        //$columns=Input::get('columns');
        $dashboardId=Input::get('dashboardId');
        $dashboard=Dashboard::on($this->switchDB())->where('dashboard_id',$dashboardId)->first();
        //dd(json_decode($dashboard->reportObject)); 
        $columns=json_decode($dashboard->reportObject)->metadataObject->connObject->column;
        if(isset(json_decode($dashboard->reportObject)->columnDeleted)){
            $columnDeleted=json_decode($dashboard->reportObject)->columnDeleted;
        }
        $metadata=Metadata::on($this->switchDB())->where('metadata_id',$metadataId)->first();
        $metadataColumns=json_decode($metadata->metadataObject)->column;
        /*
         * Deleted column delete from metadatacolumn
         */
        $metadataObj=[];
        foreach ($metadataColumns as $key=>$metdatacolumn){
            $metadataObj[$metdatacolumn->columnName]=$metdatacolumn;
        }
        if(isset($columnDeleted)){
            foreach ($columnDeleted as $column){
                if(isset($metadataObj[$column->columnName])){
                    unset($metadataObj[$column->columnName]);
                }
            }
        }
        /*
         * end deleted column
         */
        /*
         *   Changed column from dashboard column
         */
        $dashboardCustomColumns=[];
        foreach($columns as $column){
            if(isset($column->type) && $column->type=="custom"){
                $metadataObj[$column->columnName]=$column;
            }else{
                $dashboardCustomColumns[$column->columnName]=$column;
            }
            if(isset($column->columType) && isset($metadataObj[$column->columnName]) && $column->columType!=$metadataObj[$column->columnName]->columType){
                $metadataObj[$column->columnName]=$column;
            }
        }

        //dd($dashboardCustomColumns);
        foreach ($metadataObj as $key=>$metdatacolumn){
            //print_r($dashboardCustomColumns[$metdatacolumn->columnName]->columnName);
            if(isset($dashboardCustomColumns[$metdatacolumn->columnName]) && ($dashboardCustomColumns[$metdatacolumn->columnName]->columnName!=$dashboardCustomColumns[$metdatacolumn->columnName]->reName || $dashboardCustomColumns[$metdatacolumn->columnName]->dataKey!= $metdatacolumn->dataKey)){
                $metadataObj[$metdatacolumn->columnName]=$dashboardCustomColumns[$metdatacolumn->columnName];
                //unset($metadataObj[$metdatacolumn->columnName]);
            }
        }
        /*
         * Metadata key column name convert to index
         */
        $metadataArray=[];
        foreach ($metadataObj as $key=>$metdatacolumn){
            array_push($metadataArray,$metdatacolumn);
        }
        return Response::json([
            "errorCode"=>"1",
            "result"=>$metadataArray,
            "message"=>"Custom column"
        ]);
    }

    public function incrementDataObjSave(){
        $metadataId=Input::get('metadataId');
        $incrementObj=Input::get('incrementObj');
        $metadataObj=Metadata::on($this->switchDB())->where('metadata_id',$metadataId)->first();
        $metadataObj->incrementObj=$incrementObj;
        if($metadataObj->save()){
            return Response::json([
                "errorCode"=>1,
                "result"=>"",
                "message"=>"Increment data save successfully"
            ]);
        }
        return Response::json([
            "errorCode"=>0,
            "result"=>"",
            "message"=>"Check your connection"
        ]);
    }
    /*
     * node to call api realtime data check function call
     */
    public static function redisAddDataNode()
    {
        $client = Input::get('client');
        $sessionId = Input::get('sessionId');
        $clientId = explode(":", $client)[0];
        if ($clientId) {
            $companyId = Input::get('companyId');
            $metadataId = explode(":", $client)[1];
            $sharedViewId = Input::get('sharedviewId');
            $redis = Redis::connection();
            /*
             * Get All Data
             */
            //$allKeys=$redis->keys("*");
            $metadataObj = [];
            /*
             * Get all metadata
             */
            //foreach ($allKeys as $value){
            //  if($value!="metadataDetails"){;
            if ($metadataId && $companyId && $sharedViewId) {
                $companyObj = CompanyDetail::where('uid', $companyId)->first();
                $metadata = Metadata::on(Controller::switchDBPublicview($companyObj->db_name))->where('metadata_id', $metadataId)->first();
                $metadataObject = json_decode($metadata->metadataObject);
                if (Controller::datasourceCheckPublic($metadataObject->connectionObject->datasource_id, $companyObj->db_name)) {
                    $metadataObject->connectionObject = Controller::datasourceCheckPublic($metadataObject->connectionObject->datasource_id, $companyObj->db_name);
                }
                $datasourceType = $metadataObject->connectionObject->datasourceType;
                $incrementObj = $metadata->incrementObj;
                $publicView = PublicView::on(Controller::switchDBPublicview($companyObj->db_name))->find($sharedViewId);

                $roleCondition = [];
                $userType=false;
                if ($sharedViewId != "" && isset($clientId)) {
                    $rlsCount = RoleLevelSecurity::on(Controller::switchDBPublicview($companyObj->db_name))->where('public_view_id', $sharedViewId)->count();
                    if ($rlsCount) {
                        $userM = User::where('id', $clientId)->first();
                        $users = User::on(Controller::switchDBPublicview($companyObj->db_name))->where('id', $userM->user_id)->with('group')->first();
                        /*
                         * User wise role level security
                         */
                        if (isset($roleCondition)) {
                            $condition = RoleLevelSecurity::on(Controller::switchDBPublicview($companyObj->db_name))->where('public_view_id', $sharedViewId)->with(['rls_user' => function ($query) use ($users) {
                                $query->where('user_id', $users->id);
                            }])->get();
                            foreach ($condition as $where) {
                                if (isset($where->rls_user) && isset($where->rls_user) && count($where->rls_user)) {
                                    array_push($roleCondition, $where->conditionObj);
                                }
                            }
                        }
                        if (count($roleCondition)) {
                            $userType = true;
                        }
                        if (count($roleCondition) == 0) {
                            /*
                             * Department wise role level security if multiple department then both condition with or mode
                             */
                            //user_group_id from front end which folder selected
                            $rlsUserGroups = RoleLevelSecurityUserGroup::on(Controller::switchDBPublicview($companyObj->db_name))->where('user_group_id',2)->get();
                            foreach ($rlsUserGroups as $rlsUserGroup) {
                                if (isset($rlsUserGroup->rls_id)) {
                                    $rls = RoleLevelSecurity::on(Controller::switchDBPublicview($companyObj->db_name))->where('id', $rlsUserGroup->rls_id)->where('public_view_id', $sharedViewId)->first();
                                    if (isset($rls->conditionObj)) {
                                        array_push($roleCondition, $rls->conditionObj);
                                    }
                                }
                            }
                        }
                        if (!($users->hasRole(['Admin']) || $users->hasRole(['Super Admin']))){
                            $metadataObject->userType = $userType;
                            $metadataObject->exCondition = $roleCondition;
                        }
                    }
                    /*
                     * Role level user wise or department wise
                     */

                    if (isset(json_decode($publicView->reportObject)->realtime)) {
                        $metadataObject->realtime = json_decode($publicView->reportObject)->realtime;
                    }
                    //dd(json_decode($publicView->reportObject)->realtime);
                    //dd($metadataObject);
                    $metadataObj[$client] = $metadataObject;
                    /*
                     * Check length and get data
                     */
                    /*
                     * Check length and get data
                     */

                    switch ($datasourceType) {
                        case "mysql":
                            $responseObj = MySQL::nodeToRedis($metadataObj, $redis, $incrementObj, $sessionId, $metadataId, $companyId);
                            break;
                        case "mongodb":
                            $responseObj = MongoDB::nodeToRedis($metadataObj, $redis, $incrementObj, $sessionId, $metadataId, $companyId);
                            break;
                        case "mssql":
                            $responseObj = MsSQL::nodeToRedis($metadataObj, $redis, $incrementObj, $sessionId, $metadataId, $companyId);
                            break;
                        case "excel":
                            $responseObj = ExcelDs::nodeToRedis($metadataObj, $redis, $incrementObj, $sessionId, $metadataId, $companyId);
                            break;
                        case "postgres":
                            $responseObj = PostGres::nodeToRedis($metadataObj, $redis, $incrementObj, $sessionId, $metadataId, $companyId);
                            break;
                        case "oracle":
                            $responseObj = Oracle::nodeToRedis($metadataObj, $redis, $incrementObj, $sessionId, $metadataId, $companyId);
                            break;
                    }
                    return Response::json($responseObj);
                }
                return false;
            }
            return false;
        }
    }
    /*
     * Export metadatasource
     */
    public function export(){
        //Key
        $metadataId=Input::get('metadataId');
        $metadata=Metadata::on($this->switchDB())->where('metadata_id',$metadataId)->first();
        $metadata->company_id=Auth::user()->company_id;
        $encrypted = encrypt($metadata);
        $fileName=str_replace(' ', '', $metadata->name);
        $folder=env('FOLDER_PATH');
        $fileName=self::clean($fileName);
        $store= file_put_contents("/var/www/html/$folder/bi/export/".$fileName.".think",$encrypted);
        if($store){
            return Response::json([
                'errorCode' => 1,
                'message'=>'upload successfully',
                'result'=> ["url"=>"export/$fileName.think","fileName"=>$metadata->name.".think"]
            ]);
        }else{
            return Response::json([
                'errorCode' => 0,
                'message'=>'Excel uploading failed',
                'result'=> "file export failed"
            ]);
        }
    }
    /*
     * Import
     */
    public static function is_JSON() {
        call_user_func_array('json_decode',func_get_args());
        return (json_last_error()===JSON_ERROR_NONE);
    }
    public function checkValidation(){
        $input = Input::all();
        $file = File::get($input['upload']);
        $metadataObj=decrypt($file);
        if($metadataObj->company_id!=Auth::user()->company_id){
            return Response::json([
                'errorCode' => 0,
                'message'=> 'File corrupted',
                'result'=> ""
            ]);
        }else{
            unset($metadataObj->company_id);
        }
        if (self::is_JSON($metadataObj)) {
            /*
             * Datasource check
             */
            if(isset(json_decode($metadataObj->metadataObject)->connectionObject)){
                $datasource=json_decode($metadataObj->metadataObject)->connectionObject;
                $datasourceCount=Datasource::on($this->switchDB())->where('datasource_id',$datasource->datasource_id)->count();
            }else{
                $datasourceCount=0;
            }

            $metadataCount=Metadata::on($this->switchDB())->where('metadata_id',$metadataObj['metadata_id'])->count();
            if($metadataCount){
                return Response::json([
                    'errorCode' => 1,
                    'message'=> 'If you move '.$metadataObj['name'].' from '.$metadataObj['group_type'].' to another folder then it will be deleted from '.$metadataObj['group_type'],
//                    'message'=> 'Metadata already exist you want to overwrite '.$metadataObj['name'].' metadata save',
                    'result'=> ["datasource"=>$datasourceCount,"metadata"=>$metadataCount]
                ]);
            }
            return Response::json([
                'errorCode' => 1,
                'message'=> 'Valid File',
                'result'=> ["datasource"=>1,"metadata"=>1]
            ]);
        } else {
            return Response::json([
                'errorCode' => 0,
                'message'=> 'File corrupted',
                'result'=> ""
            ]);
        }
    }
    public function importSave(){
        $input = Input::all();
        $file = File::get($input['upload']);
        $metadataObj=decrypt($file);
        $reportGroupObj=json_decode(Input::get('reportGroup'));
        if(isset($reportGroupObj->name) && Controller::folderValidation($reportGroupObj->name)){
            return Response::json([
                'errorCode' => 0,
                'message'=>'Folder already exist',
                'result'=> "",
            ]);
        }
        if (self::is_JSON($metadataObj)) {
            $metadataObj=(object)$metadataObj;
            $type="";
            if(!isset(json_decode($input['overwriteStatus'])->blending)){
                $datasourceObj=json_decode($metadataObj->metadataObject)->connectionObject;
                if(isset(json_decode($input['overwriteStatus'])->overwrite->datasource)){
                    $datasourceUpdate=Datasource::on($this->switchDB())->where('datasource_id',$datasourceObj->datasource_id)->first();
                    if($datasourceUpdate){
                        $datasourceUpdate->name=$datasourceObj->name;
                        $datasourceUpdate->dbname=$datasourceObj->dbname;
                        $datasourceUpdate->host=$datasourceObj->host;
                        $datasourceUpdate->port=$datasourceObj->port;
                        $datasourceUpdate->username=$datasourceObj->username;
                        $datasourceUpdate->password=$datasourceObj->password;
                        $datasourceUpdate->save();
                    }else{
                        Datasource::on($this->switchDB())->create([
                            "datasource_id"=>$datasourceObj->datasource_id,
                            "login_id"=>$datasourceObj->login_id,
                            "datasourceType"=>$datasourceObj->datasourceType,
                            "group_type"=>"public_group",
                            "name"=>$datasourceObj->name,
                            "dbname"=>$datasourceObj->dbname,
                            "host"=>$datasourceObj->host,
                            "port"=>$datasourceObj->port,
                            "username"=>$datasourceObj->username,
                            "password"=>$datasourceObj->password
                        ]);
                    }
                }
            }else{
                $type="blending";
            }
            $metadata=Metadata::on($this->switchDB())->where('metadata_id',$metadataObj->metadata_id)->first();
            if($metadata){
                $metadata->name=$metadataObj->name;
                $metadata->userid=$metadataObj->userid;
                $metadata->metadataObject=$metadataObj->metadataObject;
                $metadata->group=$metadataObj->group;
                $metadata->group_type=Input::get('publicViewType');
                $metadata->save();
            }else{
                $metadata=Metadata::on($this->switchDB())->create(
                    [
                        "name" => $metadataObj->name,
                        "metadata_id"=>$metadataObj->metadata_id,
                        "userid" => $metadataObj->userid,
                        "metadataObject" =>$metadataObj->metadataObject,
                        "group" => $metadataObj->group,
                        "type" => $type,
                        "group_type"=>Input::get('publicViewType')
                    ]
                );
            }
            if(Input::get('publicViewType')!='public_group') {
                $metadataReport=MetadataReportGroup::on($this->switchDB())->where("metadata_id",$metadata->id)->first();
                if($metadataReport){
                    $metadataReport->delete();
                }
                if (isset($reportGroupObj->reportGroup)) {
                    MetadataReportGroup::on($this->switchDB())->create([
                        "report_group_id" => $reportGroupObj->reportGroup,
                        "metadata_id" => $metadata->id
                    ]);
                } else {
                    $reportGroup = ReportGroup::on($this->switchDB())->create([
                        "name" => $reportGroupObj->name,
                        "description" => $reportGroupObj->name,
                    ]);
                    /*
                     * Report user group
                     */
                    foreach ($reportGroupObj->userGroup as $group) {
                        ReportUserGroup::on($this->switchDB())->create([
                            "report_group_id" => $reportGroup->id,
                            "user_group_id" => $group
                        ]);
                    }
                    MetadataReportGroup::on($this->switchDB())->create([
                        "report_group_id" => $reportGroup->id,
                        "metadata_id" => $metadata->id
                    ]);
                }
            }
            if($metadata){
                return Response::json([
                    'errorCode' => 1,
                    'message'=> 'Metadata save successfully',
                    'result'=> ""
                ]);
            }
        } else {
            return Response::json([
                'errorCode' => 0,
                'message'=> 'File corrupted',
                'result'=> ""
            ]);
        }
    }

}