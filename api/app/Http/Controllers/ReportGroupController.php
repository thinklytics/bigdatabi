<?php

namespace App\Http\Controllers;

use App\Model\Dashboard;
use App\Model\DashboardReportGroup;
use App\Model\Datasource;
use App\Model\DatasourceReportGroup;
use App\Model\Metadata;
use App\Model\MetadataReportGroup;
use App\Model\Permission;
use App\Model\PermissionRole;
use App\Model\PublicView;
use App\Model\ReportGroup;
use App\Model\ReportUserGroup;
use App\Model\Role;
use App\Model\RoleUser;
use App\Model\SharedviewReportGroup;
use App\Model\UserGroup;
use App\Model\UserGroupUser;
use App\Model\UserProfile;
use App\User;
use function array_push;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Image;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Mockery\Exception;
use stdClass;

class ReportGroupController extends Controller
{
    public function add(){
        try{
            $reportCount=ReportGroup::on($this->switchDB())->where("name",Input::get('group')['name'])->count();
            if($reportCount==0){
                $reportGroup=ReportGroup::on($this->switchDB())->create([
                    "name"=>Input::get('group')['name'],
                    "description"=>Input::get('group')['description']
                ]);
                foreach (Input::get('group')['userGroup'] as $userGroup){
                    ReportUserGroup::on($this->switchDB())->create([
                        "report_group_id"=>$reportGroup->id,
                        "user_group_id"=>$userGroup
                    ]);
                }
                $type=Input::get('type'); 
                if($type=="datasource"){
                    foreach (Input::get('group')['selectedlist'] as $id){
                        $datasource=Datasource::on($this->switchDB())->find($id);
                        $datasource->group_type="specific_group";
                        $datasource->save();
                        DatasourceReportGroup::on($this->switchDB())->create([
                            "report_group_id"=>$reportGroup->id,
                            "datasource_id"=>$id
                        ]); 
                    }
                }else if($type=="metadata"){
                    foreach (Input::get('group')['selectedlist'] as $id){
                        $metadata=Metadata::on($this->switchDB())->find($id);
                        $metadata->type="specific_group";
                        $metadata->save();
                        MetadataReportGroup::on($this->switchDB())->create([
                            "report_group_id"=>$reportGroup->id,
                            "metadata_id"=>$id
                        ]);
                    }
                }else if($type=="dashboard"){
                    foreach (Input::get('group')['selectedlist'] as $id){
                        $dashboard=Dashboard::on($this->switchDB())->find($id);
                        $dashboard->group_type="specific_group";
                        $dashboard->save();
                        DashboardReportGroup::on($this->switchDB())->create([
                            "report_group_id"=>$reportGroup->id,
                            "dashboard_id"=>$id
                        ]);
                    }
                }else if($type=="sharedview"){
                    foreach (Input::get('group')['selectedlist'] as $id){
                        $sharedView=PublicView::on($this->switchDB())->find($id);
                        $sharedView->type="sharedview";
                        $sharedView->save();
                        SharedviewReportGroup::on($this->switchDB())->create([
                            "report_group_id"=>$reportGroup->id,
                            "sharedview_id"=>$id
                        ]);
                    }
                }
                return Response::json([
                    "errorCode"=>1,
                    "message"=>"successfully",
                    "reuslt"=>""
                ]);
            }
            return Response::json([
                "errorCode"=>0,
                "message"=>"Report Group name is already defined",
                "reuslt"=>""
            ]);
        }catch(Exception $e){
            return Response::json([
                "errorCode"=>0,
                "message"=>"Check your connection",
                "reuslt"=>$e->getMessage()
            ]);
        }

    }
    /*
     * Report Group group
     */
    public function ReportGroupList(){
        try{
            $reportGroupList=[];
            foreach (Input::get('usrGrp') as $usergrpId){
                $reps=ReportUserGroup::on($this->switchDB())->where('user_group_id',$usergrpId)->with('reportGroup')->get();
                $userGroupName=UserGroup::on($this->switchDB())->find($usergrpId);
                foreach ($reps as $rep){
                    $tempObj=new stdClass();
                    $tempObj->id=$usergrpId;
                    $tempObj->ReportGrpid=$rep->reportGroup->id;
                    $tempObj->name=$rep->reportGroup->name."(".$userGroupName->name.")";
                    array_push($reportGroupList,$tempObj);
                }
            }
            return Response::json([
                "errorCode"=>1,
                "message"=>"report group list",
                "result"=>$reportGroupList
            ]);
        }catch(Exception $e){
            return Response::json([
                "errorCode"=>0,
                "message"=>"Check your connection",
                "result"=>$e->getMessage()
            ]);
        }
    }
    /*
    * Report group list datasource
    */
    public function ReportGroupDatasourceList(){
        try{
            $reportGroupList=ReportGroup::on($this->switchDB())->with("group.userGroupDetails")
                ->with(["datasourceGroup.datasourceDetails"=>function($query){
                    $query->select("id","name");
                }])->get();
            return Response::json([
                "errorCode"=>1,
                "message"=>"report group list",
                "result"=>$reportGroupList
            ]);
        }catch(Exception $e){
            return Response::json([
                "errorCode"=>0,
                "message"=>"Check your connection",
                "result"=>$e->getMessage()
            ]);
        }
    }
    /*
     * Report group list Metadata
     */
    public function ReportGroupMetadataList(){
        try{
            $reportGroupList=ReportGroup::on($this->switchDB())->with("group.userGroupDetails")
                ->with(["metadataGroup.metadataDetails"=>function($query){
                    $query->select("id","name");
                }])->get();
            return Response::json([
                "errorCode"=>1,
                "message"=>"report group list",
                "result"=>$reportGroupList
            ]);
        }catch(Exception $e){
            return Response::json([
                "errorCode"=>0,
                "message"=>"Check your connection",
                "result"=>$e->getMessage()
            ]);
        }
    }
    /*
     * Report group list dashbard
     */
    public function ReportGroupdashboardList(){
        try{
            $reportGroupList=ReportGroup::on($this->switchDB())->with("group.userGroupDetails")
                ->with(["dashboardGroup.dashboardDetails"=>function($query){
                    $query->select("id","name");
                }])->get();
            return Response::json([
                "errorCode"=>1,
                "message"=>"report group list",
                "result"=>$reportGroupList
            ]);
        }catch(Exception $e){
            return Response::json([
                "errorCode"=>0,
                "message"=>"Check your connection",
                "result"=>$e->getMessage()
            ]);
        }
    }
    /*
     * Report group list Public view
     */
    public function ReportGroupPublicviewList(){
        try{
            $reportGroupList=ReportGroup::on($this->switchDB())->with("group.userGroupDetails")
                ->with(["sharedGroup.sharedViewDetails"=>function($query){
                $query->select("id","name");
            }])->get();
            /*$tempArray=array();
            $tempArray['name']="Public view";
            $tempArray['Description']="Public view";
            $tempArray['created_at']="";
            $tempArray['updated_at']="";
            $tempArray['id']="publicview";
            $tempArray['shared_group']="publicview";
            $tempArray['group']="publicview";
            array_push($reportGroupList->toArray(),$tempArray);*/
            return Response::json([
                "errorCode"=>1,
                "message"=>"report group list",
                "result"=>$reportGroupList
             ]);
        }catch(Exception $e){
            return Response::json([
                "errorCode"=>0,
                "message"=>"Check your connection",
                "result"=>$e->getMessage()
            ]);
        }
    }
    public function deleteReportGroup(){
        try{
            //Report group
            /*$reportGroup=ReportGroup::find(Input::get('id'));
            $reportGroup->delete();*/
            //Report user group
            /*$reportGroupUser=ReportUserGroup::where('report_group_id',Input::get('id'))->first();
            $reportGroupUser->delete();*/
            //Shared view delete
            $type=Input::get('type');
            if($type=="datasource"){
                $sharedViewGroup=DatasourceReportGroup::on($this->switchDB())->where('report_group_id',Input::get('id'))->first();
            }else if($type=="metadata"){
                $sharedViewGroup=MetadataReportGroup::on($this->switchDB())->where('report_group_id',Input::get('id'))->first();
            }else if($type=="dashboard"){
                $sharedViewGroup=DashboardReportGroup::on($this->switchDB())->where('report_group_id',Input::get('id'))->first();
            }else if($type=="sharedview"){
                $sharedViewGroup=SharedviewReportGroup::on($this->switchDB())->where('report_group_id',Input::get('id'))->first();
            }
            $sharedViewGroup->delete();
            return Response::json([
                "errorCode"=>1,
                "message"=>"Delete successfully",
                "result"=>""
            ]);
        }catch(Exception $e){
            return Response::json([
                "errorCode"=>0,
                "message"=>"Check your connection",
                "result"=>$e->getMessage()
            ]);
        }
    }
    public function reportGroupDataById(){
        try{

            if(Input::get('type')=='datasource'){
                $reportData = ReportGroup::on($this->switchDB())->where('id', Input::get('id'))->with('group')->with('datasourceGroup')->get();
            }else if(Input::get('type')=='metadata'){
                $reportData = ReportGroup::on($this->switchDB())->where('id', Input::get('id'))->with('group')->with('metadataGroup')->get();
            }else if(Input::get('type')=='dashboard'){
                $reportData = ReportGroup::on($this->switchDB())->where('id', Input::get('id'))->with('group')->with('dashboardGroup')->get();
            }else if(Input::get('type')=='sharedview' || Input::get('type')=='shardview'){
                $reportData = ReportGroup::on($this->switchDB())->where('id', Input::get('id'))->with('group')->with('sharedGroup')->get();
            }
            return Response::json([
                'errorCode' => 1,
                'message' => 'Report Group Data Success',
                'result' => $reportData
            ]);
        }catch(Exception $e){
            return Response::json([
               'errorCode' => 0,
               'message' => 'Check Your Connection',
               'result' => $e->getMessage()
            ]);
        }
    }
    public function reportGroupUpdate(){
        try{
            $reportGroupData = ReportGroup::on($this->switchDB())->find(Input::get('id'));
            $reportGroupData->name = Input::get('name');
            $reportGroupData->description = Input::get('description');
            $reportGroupData->save();

            if(ReportUserGroup::on($this->switchDB())->where('report_group_id', Input::get('id'))->count()){
                $group = ReportUserGroup::on($this->switchDB())->where('report_group_id', Input::get('id'))->delete();
            }

            foreach (Input::get('group') as $groupId) {
                $group = ReportUserGroup::on($this->switchDB())->create([
                    'report_group_id' => Input::get('id'),
                    'user_group_id' => $groupId
                ]);
                $group->save();
            }

            if(Input::get('type')=='datasource'){
                if(DatasourceReportGroup::on($this->switchDB())->where('report_group_id', Input::get('id'))->count()){
                    DatasourceReportGroup::on($this->switchDB())->where('report_group_id', Input::get('id'))->delete();
                }
                foreach (Input::get('sharedView') as $shared){
                    $shared = DatasourceReportGroup::on($this->switchDB())->create([
                        'report_group_id' => Input::get('id'),
                        'datasource_id' => $shared
                    ]);
                }
            }else if(Input::get('type')=='metadata'){
                if(MetadataReportGroup::on($this->switchDB())->where('report_group_id', Input::get('id'))->count()){
                    MetadataReportGroup::on($this->switchDB())->where('report_group_id', Input::get('id'))->delete();
                }
                foreach (Input::get('sharedView') as $shared){
                    $shared = MetadataReportGroup::on($this->switchDB())->create([
                        'report_group_id' => Input::get('id'),
                        'metadata_id' => $shared
                    ]);
                }
            }else if(Input::get('type')=='dashboard'){
                if(DashboardReportGroup::on($this->switchDB())->where('report_group_id', Input::get('id'))->count()){
                    DashboardReportGroup::on($this->switchDB())->where('report_group_id', Input::get('id'))->delete();
                }
                foreach (Input::get('sharedView') as $shared){
                    $shared = DashboardReportGroup::on($this->switchDB())->create([
                        'report_group_id' => Input::get('id'),
                        'dashboard_id' => $shared
                    ]);
                }
            }else if(Input::get('type')=='sharedview'){
                if(SharedviewReportGroup::on($this->switchDB())->where('report_group_id', Input::get('id'))->count()){
                     SharedviewReportGroup::on($this->switchDB())->where('report_group_id', Input::get('id'))->delete();
                }
                foreach (Input::get('sharedView') as $shared){
                    $shared = SharedviewReportGroup::on($this->switchDB())->create([
                        'report_group_id' => Input::get('id'),
                        'sharedview_id' => $shared
                    ]);
                }
            }
            $shared->save();
            return Response::json([
                'errorCode' => 1,
                'message' => 'Report Group Data Updated Successfully',
                'result' => ''
            ]);

        }catch(Exception $e){
            return Response::json([
                'errorCode' => 0,
                'message' => 'Check Your Connection',
                'result' => $e->getMessage()
            ]);
        }
    }
}