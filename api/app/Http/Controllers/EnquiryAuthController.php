<?php

namespace App\Http\Controllers;

use App\Http\Utils\DBHelper;
use Faker\Provider\Enquiry;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use App\Model\EnquiryAuth;
use App\Model\EnquiryDetail;
use App\User;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Mail;

class EnquiryAuthController extends Controller{

    public static function generateRandomPassword() {
        $length = 10;
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ!@#$%^&*_=+-/.?<>)';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public function add(){
        $password=$this->generateRandomPassword();
        $email=Input::get('email');
        $enquiryObj=EnquiryDetail::where('email_id', $email)->first();
        if(!$enquiryObj){
            return Response::json([
                'errorCode' => 1,
                'message'=>"Email ID not found",
                'result'=> "",
            ]);
        }
        $now = time();
        $extra_min = $now + (10 * 60);
        $valid_time = date('Y-m-d H:i:s', $extra_min);
        $enquiryAuthCount=EnquiryAuth::where('enquiry_id', $enquiryObj['id'])->where('valid_time','>=',$valid_time)->count();
        if($enquiryAuthCount == 0){
            $enquiry=EnquiryAuth::create([
                "enquiry_id"=>$enquiryObj['id'],
                "password"=>$password,
                "valid_time"=>$valid_time,
                "status"=>0,
            ]);
            Mail::send([],[], function($message) use ($password, $email){
                $subject = 'Thinklytics Activation';
                $msg = 'Dear ' . $email . ' ,<br><br>Welcome to Thinklytics.<br>Your Password is ' . $password ;
                $message->from('thinklytics@gmail.com','Thinklytics')->to($email)->subject($subject)->setBody($msg, 'text/html');
            });
            return Response::json([
                'errorCode' => 1,
                'message'=>"Enquiry Added Successfully & Password send to registered mail successfully",
                'result'=> $enquiryObj,
            ]);
        }else{
            return Response::json([
                'errorCode' => 0,
                'message'=>"Email ID Already exists.",
                'result'=> "",
            ]);
        }
    }

    public function auth(){
        $input = Input::get();
        return Response::json([
            'errorCode' => 1,
            'message'=>"Success",
            'result'=> $input,
        ]);




        $currentDate = date('Y-m-d H:i:s');
        $input=(object)Input::get('enquiry_auth');
        $enquiryDetailObj=EnquiryDetail::where('email_id', $input->email)->first();
        $enquiryAuthObj=EnquiryAuth::where('enquiry_id', $enquiryDetailObj['id'])->where('password', $input->password)->where('status', 0)->where('valid_time', ">=", $currentDate)->first();
        if($enquiryAuthObj){
            $enquiryAuthObj->status=1;
            $enquiryAuthObj->save();
            return Response::json([
                'errorCode' => 1,
                'message'=>"Authenticated Successfully",
                'result'=> "",
            ]);
        }else{
            return Response::json([
                'errorCode' => 0,
                'message'=>"Authentication Failed",
                'result'=> "",
            ]);
        }
    }





















//    public function auth(){
//        $currentDate = date('Y-m-d H:i:s');
//        $input=(object)Input::get('enquiry_auth');
//        $enquiryDetailObj=EnquiryDetail::where('email_id', $input->email)->first();
//        $enquiryAuthObj=EnquiryAuth::where('enquiry_id', $enquiryDetailObj['id'])->where('password', $input->password)->where('status', 0)->where('valid_time', ">=", $currentDate)->first();
//        if($enquiryAuthObj){
//            $enquiryAuthObj->status=1;
//            $enquiryAuthObj->save();
//            return Response::json([
//                'errorCode' => 1,
//                'message'=>"Authenticated Successfully",
//                'result'=> "",
//            ]);
//        }else{
//            return Response::json([
//                'errorCode' => 0,
//                'message'=>"Authentication Failed",
//                'result'=> "",
//            ]);
//        }
//    }







}
