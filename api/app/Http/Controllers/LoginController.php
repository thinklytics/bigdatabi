<?php

namespace App\Http\Controllers;
use App\Model\Login;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;
use Illuminate\Validation\Rules\In;
class LoginController extends Controller
{
    //
    public function loginCheck(){
        $login= new Login();
        $result=$login->get()->where('username', Input::get('username'))->where('password',Input::get('password'))->first();
        if(count($result)){
            return Response::json([
                'error' => false,
                'message'=>'Connection successfully',
                'result'=>$result->id
            ]);
        }
        return Response::json([
            'error' => true,
            'message'=>'Connection failed',
            'result'=>""
        ]);
    }
    public function signUp(){
    	$userData=json_decode(Input::get('userData'));
    	$signup=new Login([
    		"firstName" =>$userData->firstname,
    		"lastName" => $userData->lastname,
    		"email"    => $userData->email,
    		"username" => $userData->email,
    		"password" => $userData->password,
    	]); 
    	$p =$signup->save();
    	if($p){
    		return Response::json([
                'error' => false,
                'message'=>'Signup successfully',
                'result'=>""
            ]);
    	}else{
    		return Response::json([
                'error' => true,
                'message'=>'Check your connection',
                'result'=>""
            ]);
    	}
    }
}
