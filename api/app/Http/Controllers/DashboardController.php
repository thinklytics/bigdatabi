<?php

namespace App\Http\Controllers;

use App\Model\Dashboard;
use App\Model\DashboardReportGroup;
use App\Model\Datasource;
use App\Model\ReportGroup;
use App\Model\ReportUserGroup;
use App\Model\Metadata;
use App\Model\UserGroup;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Image;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Auth;
use App\Model\EmailNotification;
use Mockery\Exception;

class DashboardController extends Controller
{
    /*
     * ******************************************Dashboard common function**************************************************
     */
    /*
     * Save dashboard
     */
    public function save()
    {
        try {
            //dd(Input::get());
            $p = Dashboard::on($this->switchDB())->where("name", Input::get('name'))->count();
            if (isset(Input::get('reportGroup')['name']) && Controller::folderValidation(Input::get('reportGroup')['name'])) {
                return Response::json([
                    'errorCode' => 0,
                    'message' => 'Folder already exist',
                    'result' => "",
                ]);
            }
            if ($p == 0) {
                $dashboardCount = Dashboard::on($this->switchDB())->count();
                $ref_id = "DB" . self::generateRandomString();
                $dashboard=Dashboard::on($this->switchDB())->create(array(
                    "userid" => Auth::user()->id,
                    "dashboard_id"=>$ref_id,
                    "name" => Input::get('name'),
                    "description" => Input::get('dashboardDesc'),
                    "reportObject" => Input::get('repotsObject'),
                    "image" => Input::get('image'),
                    "filterBy"=> Input::get('filterBy'),
                    "group"=>Input::get('group'),
                    "group_type"=>Input::get('publicViewType')
                ));
                if (Input::get('publicViewType') != 'public_group') {
                    if (Input::get('reportGroup')['grpType'] == "both") {
                        /*
                         * Create group assign to report
                         */
                        foreach (Input::get('reportGroup')['reportGroup'] as $reportGrpObj) {
                            $reportGrpObj = json_decode($reportGrpObj);
                            DashboardReportGroup::on($this->switchDB())->create([
                                "report_group_id" => $reportGrpObj->ReportGrpid,
                                "dashboard_id" => $dashboard->id,
                                "user_group_id" => $reportGrpObj->id
                            ]);
                        }
                        /*
                         * Create folder then assign to folder
                         */
                        $reportGroup = ReportGroup::on($this->switchDB())->create([
                            "name" => Input::get('reportGroup')['name'],
                            "description" => Input::get('reportGroup')['name'],
                        ]);
                        /*
                         * Report user group
                         */
                        foreach (Input::get('reportGroup')['userGroup'] as $group) {
                            ReportUserGroup::on($this->switchDB())->create([
                                "report_group_id" => $reportGroup->id,
                                "user_group_id" => $group
                            ]);
                            DashboardReportGroup::on($this->switchDB())->create([
                                "report_group_id" => $reportGroup->id,
                                "dashboard_id" => $dashboard->id,
                                "user_group_id" => $group
                            ]);
                        }
                    } else if (Input::get('reportGroup')['grpType'] == "selectGrp") {
                        foreach (Input::get('reportGroup')['reportGroup'] as $reportGrpObj) {
                            $reportGrpObj = json_decode($reportGrpObj);
                            DashboardReportGroup::on($this->switchDB())->create([
                                "report_group_id" => $reportGrpObj->ReportGrpid,
                                "dashboard_id" => $dashboard->id,
                                "user_group_id" => $reportGrpObj->id
                            ]);
                        }
                    } else {
                        $reportGroup = ReportGroup::on($this->switchDB())->create([
                            "name" => Input::get('reportGroup')['name'],
                            "description" => Input::get('reportGroup')['name'],
                        ]);
                        /*
                         * Report user group
                         */
                        foreach (Input::get('reportGroup')['userGroup'] as $group) {
                            ReportUserGroup::on($this->switchDB())->create([
                                "report_group_id" => $reportGroup->id,
                                "user_group_id" => $group
                            ]);
                            DashboardReportGroup::on($this->switchDB())->create([
                                "report_group_id" => $reportGroup->id,
                                "dashboard_id" => $dashboard->id,
                                "user_group_id" => $group
                            ]);
                        }
                    }
                }
                /*
                 Email Notification save
                */
                if (Input::get('notificationObj')) {
                    $emailObj = Input::get('notificationObj');
                    foreach ($emailObj as $key => $value) {
                        $value = (object)$value;
                        $emailNotification = EmailNotification::on($this->switchDB())->create([
                            "notificationObj" => $value->emailObj,
                            "reportObj" => $value->connection,
                            "dashboardId" => $ref_id,
                            "reportId" => $key
                        ]);
                    }
                }
                return Response::json([
                    'errorCode' => 1,
                    'message' => 'Dashboard save successfully',
                    'result' => $dashboard->dashboard_id
                ]);
            }
            return Response::json([
                'errorCode' => 0,
                'message' => 'Dashboard name already exists',
                'result' => ""
            ]);
        } catch (Exception $e) {
            return Response::json(['errorCode' => 0,
                'message' => 'Check your connection',
                'result' => ""]);

        }
    }

    /*
     * Update dashboard
     */
    public
    function update()
    {
        try {
            $dashboardData = Dashboard::on($this->switchDB())->where("name", Input::get('name'))->get();
            if ((count($dashboardData) == 1 && $dashboardData[0]->dashboard_id == Input::get('dashboardId')) || (count($dashboardData) == 0)) {
                $dashboard = Dashboard::on($this->switchDB())->where('dashboard_id', Input::get('dashboardId'))->first();
                $dashboard->name = Input::get('name');
                $dashboard->description = Input::get('dashboardDesc');
                $dashboard->reportObject = Input::get('repotsObject');
                $dashboard->image = Input::get('image');
                $dashboard->filterBy = Input::get('filterBy');
                $dashboard->group = Input::get('group');
                $dashboard->save();
                return Response::json([
                    'errorCode' => 1,
                    'message' => 'Dashboard update successfully',
                    'result' => $dashboard->dashboard_id
                ]);
            }
            return Response::json([
                'errorCode' => 0,
                'message' => 'Dashboard name already exist',
                'result' => ""
            ]);
        } catch (Exception $e) {
            return Response::json([
                'errorCode' => 0,
                'message' => 'Check Your Connection',
                'result' => ""
            ]);
        }

    }

    /*
     * Dashboard list
     */
    public
    function dashboardList()
    {
        try {
            $temp = array();
            $user = $this->switchUser();
            if ($user->hasRole(['Admin']) || $user->hasRole(["Super Admin"])) {
                //.report.group.userGroup
                $usergroup = UserGroup::on($this->switchDB())->get();
                $dashboards = UserGroup::on($this->switchDB())->with('reportGroup.reportGroup.dashboardGroup.dashboardDetails')->get();
                foreach ($dashboards as $dashboard) {
                    foreach ($dashboard->reportGroup as $reportGrp) {
                        foreach ($reportGrp->reportGroup->dashboardGroup as $sharedviewReport) {
                            if (isset($sharedviewReport->dashboardDetails[0]) && $sharedviewReport->user_group_id==$dashboard->id) {
                                if (!isset($dashboard->name)) {
                                    $temp[$dashboard->name] = [];
                                }
                                if (!isset($temp[$dashboard->name][$reportGrp->reportGroup->name])) {
                                    $temp[$dashboard->name][$reportGrp->reportGroup->name] = [];
                                }
                                array_push($temp[$dashboard->name][$reportGrp->reportGroup->name], $sharedviewReport->dashboardDetails[0]);
                            }
                        }
                    }
                }
                //Public View
                $dList2 = Dashboard::on($this->switchDB())->select('id', 'dashboard_id', 'name', 'image')->where('group_type', 'public_group')->with("reportGroup.group.group.userGroup")->get();
                foreach ($dList2 as $publicview) {
                    if (!isset($temp['Public View'])) {
                        $temp['Public View'] = [];
                    }
                    array_push($temp['Public View'], $publicview);
                }
                $data = $temp;
            } else {
                $dList1 = Dashboard::on($this->switchDB())->select('id', 'dashboard_id', 'name', 'image')->with(["reportGroup.group.group.userGroup" => function ($query) use ($user) {
                    $query->where("user_id", $user->id);
                }])->get();
                foreach ($dList1 as $publicview) {
                    if (isset($publicview->reportGroup[0]) && isset($publicview->reportGroup[0]->group)) {
                        foreach ($publicview->reportGroup as $reportGrp) {
                            foreach ($reportGrp->group->group as $grpArr) {
                                if (isset($grpArr->userGroup[0])) {
                                    if (isset($reportGrp->group->name) && $grpArr->userGroup[0]->userGroup->id==$reportGrp->user_group_id) {
                                        if (!isset($temp[$grpArr->userGroup[0]->userGroup->name])) {
                                            $temp[$grpArr->userGroup[0]->userGroup->name] = [];
                                        }
                                        if (!isset($temp[$grpArr->userGroup[0]->userGroup->name][$reportGrp->group->name])) {
                                            $temp[$grpArr->userGroup[0]->userGroup->name][$reportGrp->group->name] = [];
                                        }
                                        array_push($temp[$grpArr->userGroup[0]->userGroup->name][$reportGrp->group->name], $publicview);
                                    }
                                }
                            }
                        }
                    }
                }
                //Public View
                $dList2 = Dashboard::on($this->switchDB())->select('id', 'dashboard_id', 'name', 'image')->where('group_type', 'public_group')->with("reportGroup.group.group.userGroup")->get();
                foreach ($dList2 as $publicview) {
                    if (!isset($temp['Public View'])) {
                        $temp['Public View'] = [];
                    }
                    array_push($temp['Public View'], $publicview);
                }
                $data = $temp;
            }
            return Response::json([
                'errorCode' => 1,
                'message' => 'Dashboard List',
                'result' => $data
            ]);
        } catch (Exception $e) {
            return Response::json([
                'errorCode' => 0,
                'message' => 'Check your connection',
                'result' => ""
            ]);
        }
    }

    /*
     * Dashboard shared view list
     */
    public
    function dashboardSharedViewList()
    {
        try {
            $temp = array();
            $user = $this->switchUser();
            if ($user->hasRole(['Admin']) || $user->hasRole(["Super Admin"])) {
                //.report.group.userGroup
                $dList1 = Dashboard::on($this->switchDB())->select('id', 'dashboard_id', 'name', 'group_type', 'group', 'filterBy')->with("reportGroup.group.group.userGroup")->get();
                foreach ($dList1 as $publicview) {
                    if (isset($publicview->reportGroup[0]) && isset($publicview->reportGroup[0]->group)) {
                        foreach ($publicview->reportGroup as $reportGrp) {
                            if (isset($reportGrp->group->name)) {
                                if (!isset($temp[$reportGrp->group->name])) {
                                    $temp[$reportGrp->group->name] = [];
                                }
                                array_push($temp[$reportGrp->group->name], $publicview);
                            }
                        }
                    }
                }
                //Public View
                $dList2 = Dashboard::on($this->switchDB())->select('id', 'dashboard_id', 'name', 'group_type', 'group', 'filterBy')->where('group_type', 'public_group')->with("reportGroup.group.group.userGroup")->get();
                foreach ($dList2 as $publicview) {
                    if (!isset($temp['public_group'])) {
                        $temp['public_group'] = [];
                    }
                    array_push($temp['public_group'], $publicview);
                }
                $data = $temp;
            } else {
                $dList1 = Dashboard::on($this->switchDB())->select('id', 'dashboard_id', 'name', 'group_type', 'group', 'filterBy')->with(["reportGroup.group.group.userGroup" => function ($query) use ($user) {
                    $query->where("user_id", $user);
                }])->get();
                foreach ($dList1 as $publicview) {
                    if (isset($publicview->reportGroup[0]) && isset($publicview->reportGroup[0]->group)) {
                        foreach ($publicview->reportGroup as $reportGrp) {
                            if (isset($reportGrp->group->name) && isset($reportGrp->group->group[0]->userGroup[0])) {
                                if (!isset($temp[$reportGrp->group->name])) {
                                    $temp[$reportGrp->group->name] = [];
                                }
                                array_push($temp[$reportGrp->group->name], $publicview);
                            }
                        }
                    }
                }
                //Public View
                $dList2 = Dashboard::on($this->switchDB())->select('id', 'dashboard_id', 'name', 'group_type', 'group', 'filterBy')->where('group_type', 'public_group')->with("reportGroup.group.group.userGroup")->get();
                foreach ($dList2 as $publicview) {
                    if (!isset($temp['public_group'])) {
                        $temp['public_group'] = [];
                    }
                    array_push($temp['public_group'], $publicview);
                }
                $data = $temp;
            }
            return Response::json([
                'errorCode' => 1,
                'message' => 'Dashboard List',
                'result' => $data
            ]);
        } catch (Exception $e) {
            return Response::json([
                'errorCode' => 0,
                'message' => 'Check your connection',
                'result' => ""
            ]);
        }
    }

    /*
     * One dashboard object get
     */
    public
    function dashboardIdGetObject()
    {
        try {
            $dashboard = Dashboard::on($this->switchDB())->where('dashboard_id', Input::get('id'))->first();
            //dd($dashboard);
            return Response::json([
                'errorCode' => 1,
                'message' => 'Dashboard  object',
                'result' => $dashboard
            ]);
        } catch (Exception $e) {
            return Response::json([
                'error' => 0,
                'message' => 'Check your connection',
                'result' => ""
            ]);
        }
    }

    /*
     * Metadata object update
     */
    public
    function updateMetadata()
    {
        try {
            $dashboard = Dashboard::on($this->switchDB())->find(Input::get('id'));
            $dashboard->reportObject = Input::get('dashboardObj');
            $dashboard->filterBy = Input::get('filterObj');
            $dashboard->save();
            return Response::json([
                'errorCode' => 1,
                'message' => 'Dashboard metadata update successfully',
                'result' => $dashboard->id
            ]);
        } catch (Exception $e) {
            return Response::json([
                'errorCode' => 0,
                'message' => 'Check Your Connection',
                'result' => ""
            ]);
        }
    }

    /*
     * Metadata object update
     */
    public
    function updateMetadataBlending()
    {
        try {
            $dashboard = Dashboard::on($this->switchDB())->find(Input::get('id'));
            $metadataObject = json_decode($dashboard['reportObject']);
            $metadataObject->metadataObject->name = Input::get('metadataName');
            $metadataObject->metadataObject->connObject = json_decode(Input::get('metadataObject'));
            $dashboard['reportObject'] = json_encode($metadataObject);
            $dashboard->save();
            return Response::json([
                'errorCode' => 1,
                'message' => 'Dashboard metadata update successfully',
                'result' => $dashboard->id
            ]);
        } catch (Exception $e) {
            return Response::json([
                'errorCode' => 0,
                'message' => 'Check Your Connection',
                'result' => ""
            ]);
        }
    }

    /*
     * Dashboard list
     */
    public
    function dashboardDelete()
    {
        $dashboard = Dashboard::on($this->switchDB())->where('dashboard_id', Input::get('id'))->first();
        if ($dashboard->delete()) {
            return Response::json([
                'errorCode' => 1,
                'message' => 'Dashboard delete successfully',
                'result' => ""
            ]);
        }
        return Response::json([
            'error' => 0,
            'message' => 'Check your connection',
            'result' => ""
        ]);
    }

    /*
     * Delete dashboard
     */
    public
    function imageSave()
    {
        try {
            $data = Input::get('image');
            list($type, $data) = explode(';', $data);
            list(, $data) = explode(',', $data);
            $image = base64_decode($data);
            $image_name = Input::get('image_name') . ".png";
            Storage::disk('local')->put("dashboardImage/" . $image_name, $image);
            return Response::json([
                'errorCode' => 1,
                'message' => 'Image save successfully',
                'result' => $image_name
            ]);
        } catch (Exception $e) {
            return Response::json([
                'errorCode' => 0,
                'message' => 'Error',
                'result' => $e->getMessage()
            ]);
        }
    }

    /*
     * Export dashboard
     */
    public
    function export()
    {
        $dashboardId = Input::get('dashboard_id');
        $dashboardObj = Dashboard::on($this->switchDB())->where('dashboard_id', $dashboardId)->first();
        $dashboardObj->company_id = Auth::user()->company_id;
        $encrypted = encrypt($dashboardObj);
        $fileName = str_replace(' ', '', $dashboardObj->name);
        $folder = env('FOLDER_PATH');
        $fileName = self::clean($fileName);
        $store = file_put_contents("/var/www/html/$folder/bi/export/" . $fileName . ".think", $encrypted);
        if ($store) {
            return Response::json([
                'errorCode' => 1,
                'message' => 'upload successfully',
                'result' => ["url" => "export/$fileName.think", "fileName" => $dashboardObj->name . ".think"]
            ]);
        } else {
            return Response::json([
                'errorCode' => 0,
                'message' => 'Excel uploading failed',
                'result' => "file export failed"
            ]);
        }
    }

    /*
     * Import dashboard
     */
    public
    function checkValidation()
    {
        $input = Input::all();
        $file = File::get($input['upload']);
        $dashboardObj = decrypt($file);
        if ($dashboardObj->company_id != Auth::user()->company_id) {
            return Response::json([
                'errorCode' => 0,
                'message' => 'File corrupted',
                'result' => ""
            ]);
        } else {
            unset($dashboardObj->company_id);
        }
        if (self::is_JSON($dashboardObj)) {
            /*
             * Datasource check
             */
            $metadata = json_decode($dashboardObj->reportObject)->metadataObject;
            if (!(isset($metadata->connObject->type) && $metadata->connObject->type == "blending")) {
                $datasource = $metadata->connObject->connectionObject;
                $datasourceCount = Datasource::on($this->switchDB())->where('datasource_id', $datasource->datasource_id)->count();
            } else {
                $datasourceCount = 0;
            }
            $metadataCount = Metadata::on($this->switchDB())->where('metadata_id', $metadata->metadataId)->count();
            $dashboardCount = Dashboard::on($this->switchDB())->where('dashboard_id', $dashboardObj->dashboard_id)->count();
            if ($dashboardCount) {
                return Response::json([
                    'errorCode' => 1,
                    'message' => 'If you move ' . $dashboardObj['name'] . ' from ' . $dashboardObj['group_type'] . ' to another folder then it will be deleted from ' . $dashboardObj['group_type'],
//                    'message'=> 'Dashboard already exist you want to overwrite '.$dashboardObj['name'].' dashboard save',
                    'result' => ["datasource" => $datasourceCount, "metadata" => $metadataCount, "dashboard" => $dashboardCount]
                ]);
            }
            return Response::json([
                'errorCode' => 1,
                'message' => 'Valid json',
                'result' => ["datasource" => $datasourceCount, "metadata" => $metadataCount, "dashboard" => 1]
            ]);
        } else {
            return Response::json([
                'errorCode' => 0,
                'message' => 'File corrupted',
                'result' => ""
            ]);
        }
    }

    public
    function importSave()
    {
        $input = Input::all();
        $file = File::get($input['upload']);
        $dashboardObj = decrypt($file);
        $reportGroupObj = json_decode(Input::get('reportGroup'));
        if (isset($reportGroupObj->name) && Controller::folderValidation($reportGroupObj->name)) {
            return Response::json([
                'errorCode' => 0,
                'message' => 'Folder already exist',
                'result' => "",
            ]);
        }
        if (self::is_JSON($dashboardObj)) {
            $metadata = json_decode($dashboardObj->reportObject)->metadataObject;
            /*
             * Datasource save or update
             */
            if (!(isset($metadata->connObject->type) && $metadata->connObject->type == "blending")) {
                $datasourceObj = $metadata->connObject->connectionObject;
                if (isset(json_decode($input['overwriteStatus'])->overwrite->datasource)) {
                    $datasourceUpdate = Datasource::on($this->switchDB())->where('datasource_id', $datasourceObj->datasource_id)->first();
                    if ($datasourceUpdate) {
                        $datasourceUpdate->name = $datasourceObj->name;
                        $datasourceUpdate->dbname = $datasourceObj->dbname;
                        $datasourceUpdate->host = $datasourceObj->host;
                        $datasourceUpdate->port = $datasourceObj->port;
                        $datasourceUpdate->username = $datasourceObj->username;
                        $datasourceUpdate->password = $datasourceObj->password;
                        $datasourceUpdate->save();
                    } else {
                        Datasource::on($this->switchDB())->create([
                            "datasource_id" => $datasourceObj->datasource_id,
                            "login_id" => $datasourceObj->login_id,
                            "datasourceType" => $datasourceObj->datasourceType,
                            "group_type" => "public_group",
                            "name" => $datasourceObj->name,
                            "dbname" => $datasourceObj->dbname,
                            "host" => $datasourceObj->host,
                            "port" => $datasourceObj->port,
                            "username" => $datasourceObj->username,
                            "password" => $datasourceObj->password
                        ]);
                    }
                }
            }
            /*
             * Metadata save or update
             */
            if (isset(json_decode($input['overwriteStatus'])->overwrite->metadata)) {
                $metadataUpdate = Metadata::on($this->switchDB())->where('metadata_id', $metadata->metadataId)->first();
                if ($metadataUpdate) {
                    $metadataUpdate->userid = Auth::user()->user_id;
                    $metadataUpdate->name = $metadata->name;
                    $metadataUpdate->metadataObject = json_encode($metadata->connObject);
                    $metadataUpdate->save();
                } else {
                    Metadata::on($this->switchDB())->create([
                        "metadata_id" => $metadata->metadataId,
                        "userid" => Auth::user()->user_id,
                        "name" => $metadata->name,
                        "group_type" => "public_group",
                        "metadataObject" => $metadata->connObject,
                        "group" => "",
                        "incrementObj" => ""
                    ]);
                }
            }
            /*
             * Dashboard update or create
             */
            $dashboard = Dashboard::on($this->switchDB())->where('dashboard_id', $dashboardObj->dashboard_id)->first();
            if ($dashboard) {
                $dashboard->userid = Auth::user()->user_id;
                $dashboard->name = $dashboardObj->name;
                $dashboard->group = $dashboardObj->group;
                $dashboard->description = $dashboardObj->description;
                $dashboard->reportObject = $dashboardObj->reportObject;
                $dashboard->group = $dashboardObj->group;
                $dashboard->image = $dashboardObj->image;
                $dashboard->filterBy = $dashboardObj->filterBy;
                $dashboard->group_type = Input::get('publicViewType');
                $dashboard->save();
            } else {
                $dashboard = Dashboard::on($this->switchDB())->create(
                    [
                        "dashboard_id" => $dashboardObj->dashboard_id,
                        "name" => $dashboardObj->name,
                        "userid" => Auth::user()->user_id,
                        "group_type" => Input::get('publicViewType'),
                        "reportObject" => $dashboardObj->reportObject,
                        "group" => $dashboardObj->group,
                        "image" => $dashboardObj->image,
                        "filterBy" => $dashboardObj->filterBy
                    ]
                );
            }
            if (Input::get('publicViewType') != 'public_group') {
                $dashboardReport = DashboardReportGroup::on($this->switchDB())->where("dashboard_id", $dashboard->id)->first();
                if ($dashboardReport) {
                    $dashboardReport->delete();
                }
                if (isset(json_decode($input['reportGroup'])->reportGroup)) {
                    DashboardReportGroup::on($this->switchDB())->create([
                        "report_group_id" => json_decode($input['reportGroup'])->reportGroup,
                        "dashboard_id" => $dashboard->id
                    ]);
                } else {
                    $reportGrp = json_decode(Input::get('reportGroup'));
                    $reportGroup = ReportGroup::on($this->switchDB())->create([
                        "name" => $reportGrp->name,
                        "description" => $reportGrp->name,
                    ]);
                    /*
                     * Report user group
                     */
                    foreach ($reportGrp->userGroup as $group) {
                        ReportUserGroup::on($this->switchDB())->create([
                            "report_group_id" => $reportGroup->id,
                            "user_group_id" => $group
                        ]);
                    }
                    DashboardReportGroup::on($this->switchDB())->create([
                        "report_group_id" => $reportGroup->id,
                        "dashboard_id" => $dashboard->id
                    ]);
                }
            }
            if ($dashboard) {
                return Response::json([
                    'errorCode' => 1,
                    'message' => 'Dashboard save successfully',
                    'result' => ""
                ]);
            }
        } else {
            return Response::json([
                'errorCode' => 0,
                'message' => 'File corrupted',
                'result' => ""
            ]);
        }
    }

}
