<?php

namespace App\Http\Controllers;

use App\Model\CompanyProfile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Input;

class CompanyProfileController extends Controller
{
    //
    public function save(){
        if(CompanyProfile::on($this->switchDB())->count()){
            $profile=CompanyProfile::on($this->switchDB())->first();
            $profile->company_name=Input::get('company_name');
            $profile->contact_no=Input::get('contact_no');
            $profile->address=Input::get('address');
            $profile->city=Input::get('city');
            $profile->country=Input::get('country');
            $profile->email=Input::get('email');
            $profile->save();
        }else{
            $profile=CompanyProfile::create(Input::get());
        }
        return Response::json([
            "errorCode"=>1,
            "message"=>"successfully save",
            "result"=>""
        ]);
    }
    public function getProfile(){
        $profile=CompanyProfile::on($this->switchDB())->first();
        return Response::json([
            "errorCode"=>1,
            "message"=>"successfully save",
            "result"=>$profile
        ]);
    }
}
