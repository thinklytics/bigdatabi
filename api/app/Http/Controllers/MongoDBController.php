<?php

namespace App\Http\Controllers;
use App\Model\Datasource;
use Illuminate\Http\Request;
use Illuminate\Database\Capsule\Manager as DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;


use MongoDB;



class MongoDBController extends Controller
{
    public function checkConnection()
      {
          try
          {

              $m = new MongoDB\Client("mongodb://admin:admin123@192.168.0.17:27017/");
              echo "Connection to database successfull. <br>";
              $db=$m->ecommerce;


  // Where Condition in Aggregate

       //$product = $m->selectDatabase("ecommerce")->selectCollection("products");

 // Left Join From two Collection
//              $c = $m->selectDatabase("ecommerce")->selectCollection("products");
//              $pipeline = array(
//                  array(
//                      '$lookup' => array(
//                          'from' => 'customers_basket',
//                          'localField' => 'products_id',
//                          'foreignField' => 'products_id',
//                          'as' => 'pro_code',
//                      ))
//
//              );
//              $out = $c->aggregate($pipeline);

              $c = $m->selectDatabase("ecommerce")->selectCollection("customers_basket_attributes");
              $pipeline = array(
                  array(
                      '$lookup' => array(
                          'from' => 'customers',
                          'localField' => 'customers_id',
                          'foreignField' => 'customers_id',
                          'as' => 'customers',
                      )),
                  array(
                      '$unwind'=>'$customers'
                  ),
                  array(
                      '$lookup' => array(
                          'from' => 'products',
                          'localField' => 'products_id',
                          'foreignField' => 'products_id',
                          'as' => 'products',
                      )),
                  array(
                      '$unwind'=>'$products'
                  ),
                  array(
                      '$lookup' => array(
                          'from' => 'products_options',
                          'localField' => 'products_options_id',
                          'foreignField' => 'products_options_id',
                          'as' => 'products_options',
                      )),
                  array(
                      '$unwind'=>'$products_options'
                  ),
                  array(
                      '$lookup' => array(
                          'from' => 'products_options_values',
                          'localField' => 'products_options_values_id',
                          'foreignField' => 'products_options_values_id',
                          'as' => 'products_options_values',
                      )),
                  array(
                      '$unwind'=>'$products_options_values'
                  )

              );
              $out = $c->aggregate($pipeline);

              foreach($out as $output)
              {
                  echo json_encode($output);
              }

//              foreach($out as $row)
//              {
//                  foreach($row as $key => $value)
//                  {
//                      echo $key . ': ' . $value;
//                     $val=gettype($value);
//                  if($val=="integer" || $val=="decimal" || $val == "double" || $val == "float")
//                  {
//                      echo $val;echo "Measure";
//                  }
//                  else
//                  {
//                      echo $val;echo "Dimension";
//                  }
//                      echo '<br>';
//                  }
//              }


              $data=$db->selectCollection('customers');
              $list=$data->find();

              foreach ($list as $lists)
              {

                  foreach($lists as $key => $value)
                    {
                      $val = gettype($value);
                      echo $key . ': ' . $value.':'.$val;
                      if ($val == "integer" || $val == "decimal" || $val == "double" || $val == "float")
                      {
                          echo $val;
                          echo "Measure";
                      }
                      else
                      {
                          echo $val;
                          echo "Dimension";
                      }
                      echo '<br>';
                  }
              }
//List Of Collection (Table)
//              $list = $db->listCollections();
//              foreach ($list as $collection) {
//                 echo $collection->getName();echo "<br/>";
//              }

 //How to get Database Name

//              $database=$m->listDatabases();
//              $num = 0;
//              foreach ($database as $databases)
//              {
//                  $num++;
//                  $dbname = $databases->getName();
//                  echo "<br> $num. $dbname <br>";
//
//              }


 // Get Database Name and table name
//              $database=$m->listDatabases();
//              $num = 0;
//              foreach ($database as $databases)
//              {
//                  $num++;
//                  $dbname = $databases->getName();
//                  echo "<br> $num. $dbname <br>";
//                  $db1=$m->$dbname;
//                  $list = $db1->listCollections();
//                  foreach ($list as $collection)
//                  {
//                      echo $collection->getName();echo "<br/>";
//                  }
//
//              }

          }
          catch ( MongoConnectionException $e )
          {
              echo '<p>Couldn\'t connect to mongodb, is the "mongo" process running?</p>';
              exit();
          }
      }
}
