<?php

namespace App\Http\Controllers;

use App\Model\Role;
use App\Model\CompanyDetail;
use App\Model\RoleUser;
use App\Model\UserProfile;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;

class UserController extends Controller
{
    //user create
    public function userCreate(){
        try{
            /*if($this->validator(Input::get())->fails()){
                return Response::json([
                    "errorCode"=>0,
                    "message" => "Email ID already exist",
                    "data"=>""
                ]);
            }*/
            $userAdmin =User::create([
                'company_id'=>Input::get('company'),
                'email' => Input::get('email'),
                'password' => bcrypt(Input::get('password')),
                "role"=>"NA"
            ]);
            $companyObj=CompanyDetail::where('uid',Input::get('company'))->first();
            Auth::user()['db_name']=$companyObj['db_name'];
            $user =User::on($this->switchDB())->create([
                'name' => Input::get('name'),
                'email' => Input::get('email'),
                'password' => bcrypt(Input::get('password')),
            ]);
            $userAdmin->user_id=$user->id;
            $userAdmin->save();
            UserProfile::on($this->switchDB())->create([
                'user_id' => $user->id
            ]);
            $roleObj=Role::on($this->switchDB())->first();
            $role=RoleUser::on($this->switchDB())->create([
                'user_id' => $user->id,
                'role_id' => $roleObj->id,
            ]);
            return Response::json([
                'errorCode' => 1,
                'message'=>'User successfully',
                'result'=> ""
            ]);
        }catch(Exception $e){
            return Response::json([
                'errorCode' => 500,
                'message'=>$e->getMessage(),
                'result'=> ""
            ]);
        }
    }
    public function userDelete(){
        $user_id=Input::get('user_id');
        $user=User::find($user_id);
        $companyObj=CompanyDetail::where('uid',$user->company_id)->first();
        Auth::user()['db_name']=$companyObj['db_name'];
        $subUser=User::on($this->switchDB())->find($user->user_id);
        $subUser->delete();
        $user->delete();
        return Response::json([
            'errorCode' => 1,
            'message'=>"User delete succesfully",
            'result'=> ""
        ]);
    }
    public function userObj(){
        $user_id=Input::get('user_id');
        $user=User::find($user_id);
        $companyObj=CompanyDetail::where('uid',$user->company_id)->first();
        Auth::user()['db_name']=$companyObj['db_name'];
        $userObj=User::on($this->switchDB())->find($user->user_id);
        $user->name=$userObj->name;
        return Response::json([
            'errorCode' => 1,
            'message'=>"User Object",
            'result'=> $user
        ]);
    }
    public function userList(){
        $userList=User::where('role',"!=","SA")->get();
        return Response::json([
            'errorCode' => 1,
            'message'=>"User List",
            'result'=> $userList
        ]);
    }
    public function userUpdate(){
        $user=User::find(Input::get('id'));
        $user->company_id=Input::get('company');
        $user->email=Input::get('email');
        if(Input::get('password')){
            $user->password=bcrypt(Input::get('password'));
        }
        $user->save();
        $companyObj=CompanyDetail::where('uid',$user->company_id)->first();
        Auth::user()['db_name']=$companyObj['db_name'];
        $userSwitch =User::on($this->switchDB())->find($user->user_id);
        $userSwitch->name=Input::get('name');
        if(Input::get('password')){
            $userSwitch->password=bcrypt(Input::get('password'));
        }
        $userSwitch->save();
        return Response::json([
            'errorCode' => 1,
            'message'=>"User Update successfully",
            'result'=> ""
        ]);

    }
}
