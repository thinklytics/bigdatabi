<?php
namespace App\Http\Controllers;


use App\Http\Core\MsSQL\MsSQL;
use App\Http\Core\PostGres\PostGres;
use App\Http\Core\Oracle\Oracle;
use App\Http\Core\MySQL\MySQL;
use App\Http\Core\ExcelDs\ExcelDs;
use App\Model\Datasource;
use App\Model\DatasourceReportGroup;
use App\Model\Metadata;
use App\Model\ReportGroup;
use App\Model\ReportUserGroup;
use App\Model\UserGroup;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Database\Capsule\Manager as DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;
use Mockery\Exception;
use PDO;
use PDOException;
use App\Http\Core\MongoDB\MongoDB;
use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Maatwebsite\Excel\Classes\PHPExcel;
use File;

class DatasourceController extends Controller
{

    /*
     * Datasource Check connection get list
     */


    public function connectionCheck(){
        try{
            $dataForm=json_decode(Input::get('dataForm'));
            if(!isset($dataForm->dbname))
            $dataForm->dbname="";//For get all database name
            $datasource = null;
            //Check connection type and switch class
            switch ($dataForm->datasourceType) {
                case "mysql":
                    $datasource = (new MySQL())->connect($dataForm);
                    break;
                case "mongodb":
                    $datasource = (new MongoDB())->connect($dataForm);
                    break;
                case "mssql":
                    $datasource = (new MsSQL())->connect($dataForm);
                    break;
                case "postgres":
                    $datasource = (new PostGres())->connect($dataForm);
                    break;
                case "oracle":
                    $datasource = (new Oracle())->connect($dataForm);
                    break;
            }
            if($dataForm->datasourceType=="oracle"){
                return Response::json([
                    'errorCode' => 1,
                    'message'=>"Successfully",
                    'result'=> "oracle",
                ]);
            }


            return Response::json(
                $datasource->listDatabase()
            );
        }catch (Exception $e){
            return Response::json([
                'errorCode' => 0,
                'message'=>$e->getMessage(),
                'result'=> "",
            ]);
        }
    }
    public function loadExcel(){
        (new ExcelDs())->loadExcelToMysql();

    }
    public function idToGetTablelist(){
        $datasource=new Datasource();
        $result=$datasource::on($this->switchDB())->find(Input::get('id'));
        $this->switchDataBase($result);
        $tables=DB::select('SHOW TABLES');
        return Response::json([
            'error' => false,
            'message'=>'Table list get successfully',
            'result'=> array(
                "dbName"=> $result->dbname,
                "tableList"=>$tables
            )
        ]);
    }
    public function idToGetTableData(){
        $datasource=new Datasource();
        $result=$datasource::on($this->switchDB())->find(Input::get('conId'));
        $this->switchDataBase($result);
        $tableName=Input::get('tableName'); //Table name
        //Table Columns
        $tableColumnQuery="show columns from ".$tableName;
        $tableColumnObject=DB::select($tableColumnQuery);
        foreach ($tableColumnObject as $ro){
            if($ro->Type=="int(11)"){
                $ro->dataType="Measure";
            }else{
                $ro->dataType="Dimension";
            }
            $ro->tableName=$tableName;
        }
        //Table Data
        $tableData=DB::on($this->switchDB())->select('select * from '.$tableName);
        return Response::json([
            'error' => false,
            'message'=>'Get Successfully',
            'result'=>array(
                "tableColumn" => $tableColumnObject,
                "tableData"   => $tableData
            )
        ]);
    }
    /*
     * ******************************Datasource common functions for core database*******************************************
     */
    /*
     * Id to get datasource list
     */

    public function datasourceList(){
        try{
            $temp=array();
            $user=$this->switchUser();
            if($user->hasRole([ 'Admin']) || $user->hasRole(["Super Admin"])) {
                //.report.group.userGroup
                $usergroup = UserGroup::on($this->switchDB())->get();
                $dList1 = UserGroup::on($this->switchDB())->with("reportGroup.reportGroup.datasourceGroup.datasourceDetails")->get();
                foreach ($dList1 as $datasource) {
                    foreach ($datasource->reportGroup as $reportGrp) {
                        foreach ($reportGrp->reportGroup->datasourceGroup as $sharedviewReport) {
                            if (isset($sharedviewReport->datasourceDetails[0]) && $sharedviewReport->user_group_id==$datasource->id) {
                                if (!isset($datasource->name)) {
                                    $temp[$datasource->name] = [];
                                }
                                if (!isset($temp[$datasource->name][$reportGrp->reportGroup->name])) {
                                    $temp[$datasource->name][$reportGrp->reportGroup->name] = [];
                                }
                                array_push($temp[$datasource->name][$reportGrp->reportGroup->name], $sharedviewReport->datasourceDetails[0]);
                            }
                        }
                    }
                }
                //Public View
                $dList2 = Datasource::on($this->switchDB())->select("id","datasource_id","login_id","datasourceType","group_type","name","dbname","host","port","username","password","Attributes")->where('group_type','public_group')->with("reportGroup.group.group.userGroup")->get();
                foreach($dList2 as $publicview){
                    if (!isset($temp['Public View'])) {
                        $temp['Public View']=[];
                    }
                    array_push($temp['Public View'],$publicview);
                }
                $data=$temp;
            }else{
                //reportGroup.group.group.userGroup
                $dList1 = Datasource::on($this->switchDB())->select("id","datasource_id","login_id","datasourceType","group_type","name","dbname","host","port","username","password","Attributes")->with(["reportGroup.group.group.userGroup"=> function($query) use ($user){
                    $query->where("user_id", $user->id);
                }])->get();
                foreach ($dList1 as $publicview) {
                    if (isset($publicview->reportGroup[0]) && isset($publicview->reportGroup[0]->group)) {
                        foreach ($publicview->reportGroup as $reportGrp) {
                            foreach ($reportGrp->group->group as $grpArr) {
                                if (isset($grpArr->userGroup[0])) {
                                    if (isset($reportGrp->group->name) && $grpArr->userGroup[0]->userGroup->id==$reportGrp->user_group_id) {
                                        if (!isset($temp[$grpArr->userGroup[0]->userGroup->name])) {
                                            $temp[$grpArr->userGroup[0]->userGroup->name] = [];
                                        }
                                        if (!isset($temp[$grpArr->userGroup[0]->userGroup->name][$reportGrp->group->name])) {
                                            $temp[$grpArr->userGroup[0]->userGroup->name][$reportGrp->group->name] = [];
                                        }
                                        array_push($temp[$grpArr->userGroup[0]->userGroup->name][$reportGrp->group->name], $publicview);
                                    }
                                }
                            }
                        }
                    }
                }
                /*foreach($dList1 as $publicview){
                    if(isset($publicview->reportGroup[0]) && isset($publicview->reportGroup[0]->group)){
                        foreach($publicview->reportGroup as $reportGrp){
                            if(isset($reportGrp->group->name) && isset($reportGrp->group->group[0]->userGroup[0])){
                                dd($reportGrp);
                                if (!isset($temp[$reportGrp->group->name]) && $reportGrp->userGroup[0]->userGroup->id==$reportGrp->user_group_id) {
                                    $temp[$reportGrp->group->name]=[];
                                }
                                array_push($temp[$reportGrp->group->name],$publicview);
                            }
                        }
                    }
                }*/
                //Public View
                $dList2 = Datasource::on($this->switchDB())->select("id","datasource_id","login_id","datasourceType","group_type","name","dbname","host","port","username","password","Attributes")->where('group_type','public_group')->with("reportGroup.group.group.userGroup")->get();
                foreach($dList2 as $publicview){
                    if (!isset($temp['Public View'])) {
                        $temp['Public View']=[];
                    }
                    array_push($temp['Public View'],$publicview);
                }
                $data=$temp;
            }
            return Response::json([
                'errorCode' => 1,
                'message'=>'DataSource List successfully',
                'result'=> $data
            ]);
        }catch (Exception $e){
            return Response::json([
                'errorCode' => 0,
                'message'=>$e->getMessage(),
                'result'=> "",
            ]);
        }
    }


    /*
     * Datasource save
     */
    public function save(){
        try{
            /*
             * Folder Validation
             */
            if(isset(Input::get('reportGroup')['name']) && Controller::folderValidation(Input::get('reportGroup')['name'])){
                return Response::json([
                    'errorCode' => 0,
                    'message'=>'Folder already exist',
                    'result'=> "",
                ]);
            }
            $dataForm=json_decode(Input::get('dataSourceForm'));
            $reportGroup=Input::get('reportGroup');
            $publicViewType=Input::get('publicViewType');
            $datasourceCount=Datasource::on($this->switchDB())->where('name',$dataForm->connectionName)->count();
            if($datasourceCount==0){
                if(!isset($dataForm->Attributes)){
                    $dataForm->Attributes=null;
                }else{
                    $dataForm->dbName = env('EXCEL_DB');
                    $dataForm->datasourceType="excel";
                    $dataForm->host=env('EXCEL_HOST');
                    $dataForm->username=env('EXCEL_USERNAME');
                    $dataForm->password=env('EXCEL_PASSWORD');
                    $dataForm->port=env('EXCEL_PORT');
                }
                $ref_id="DS".self::generateRandomString();
                $datasource=Datasource::on($this->switchDB())->create([
                    "login_id"=>Auth::user()->id,
                    "datasource_id"=>$ref_id,
                    "name"=>$dataForm->connectionName,
                    "group_type"=>$publicViewType,
                    "host"=>$dataForm->host,
                    "username"=>$dataForm->username,
                    "password"=>$dataForm->password,
                    "port"=>$dataForm->port,
                    "datasourceType"=>$dataForm->datasourceType,
                    "dbname"=>$dataForm->dbName,
                    "Attributes"=>$dataForm->Attributes
                ]);
                //$datasource->save();


                if(Input::get('publicViewType')=="specific_group"){
                    if (Input::get('reportGroup')['grpType'] == "both") {
                        foreach (Input::get('reportGroup')['reportGroup'] as $reportGrpObj) {
                            $reportGrpObj = json_decode($reportGrpObj);
                            DatasourceReportGroup::on($this->switchDB())->create([
                                "report_group_id" => $reportGrpObj->ReportGrpid,
                                "datasource_id" => $datasource->id,
                                "user_group_id" => $reportGrpObj->id
                            ]);
                        }
                        $reportGroup=ReportGroup::on($this->switchDB())->create([
                            "name"=>Input::get('reportGroup')['name'],
                            "description"=>Input::get('reportGroup')['name'],
                        ]);
                        /*
                         * Report user group
                         */
                        foreach (Input::get('reportGroup')['userGroup'] as $group){
                            ReportUserGroup::on($this->switchDB())->create([
                                "report_group_id"=>$reportGroup->id,
                                "user_group_id"=> $group
                            ]);
                            DatasourceReportGroup::on($this->switchDB())->create([
                                "report_group_id"=>$reportGroup->id,
                                "datasource_id"=> $datasource->id,
                                "user_group_id" => $group
                            ]);
                        }

                    }else if(Input::get('reportGroup')['grpType'] == "selectGrp"){
                        foreach (Input::get('reportGroup')['reportGroup'] as $reportGrpObj) {
                            $reportGrpObj = json_decode($reportGrpObj);
                            DatasourceReportGroup::on($this->switchDB())->create([
                                "report_group_id"=>$reportGrpObj->ReportGrpid,
                                "datasource_id"=> $datasource->id,
                                "user_group_id" => $reportGrpObj->id
                            ]);
                        }

                    }else{
                        $reportGroup=ReportGroup::on($this->switchDB())->create([
                            "name"=>Input::get('reportGroup')['name'],
                            "description"=>Input::get('reportGroup')['name'],
                        ]);
                        /*
                         * Report user group
                         */
                        foreach (Input::get('reportGroup')['userGroup'] as $group){
                            ReportUserGroup::on($this->switchDB())->create([
                                "report_group_id"=>$reportGroup->id,
                                "user_group_id"=> $group
                            ]);
                            DatasourceReportGroup::on($this->switchDB())->create([
                                "report_group_id"=>$reportGroup->id,
                                "datasource_id"=> $datasource->id,
                                "user_group_id" => $group
                            ]);
                        }
                    }
                }
                return Response::json([
                    'errorCode' => 1,
                    'message'=>'Datasource Save Successfully',
                    'result'=> $datasource,
                ]);
            }else{
                return Response::json([
                    'errorCode' => 0,
                    'message'=>'Datasource name already exist',
                    'result'=> "",
                ]);
            }
        }catch (Exception $e){
            return Response::json([
                'errorCode' => 500,
                'message'=>$e->getMessage(),
                'result'=> "",
            ]);
        }
    }
    /*
     * Datasource delete by id
     */
    public function datasourceDelete(){
    	try{
            $statusCode=200;
    		$response=[];
    		$datasource=Datasource::on($this->switchDB())->where('datasource_id',Input::get('id'))->first();
	        if($datasource->delete()){
                return Response::json([
                    'errorCode' =>1,
                    'message'=>'Delete successfully',
                    'result'=> "",
                ]);
            }else{
                return Response::json([
                    'errorCode' => 0,
                    'message'=>'Delete successfully',
                    'result'=> "",
                ]);
            }
    	}catch (Exception $e){
            return Response::json([
                'errorCode' => 0,
                'message'=>$e->getMessage(),
                'result'=> "",
            ]);
        }
    }
    /*
     * Datasource update by id
     */
	public function update(){
	    try{
            $dataForm=json_decode(Input::get('dataForm'));
            $dataSource=Datasource::on($this->switchDB())->where('datasource_id',$dataForm->datasource_id)->first();
            $dataSource->name=$dataForm->connectionName;
            $dataSource->host=$dataForm->host;
            $dataSource->username=$dataForm->username;
            $dataSource->password=$dataForm->password;
            $dataSource->port=$dataForm->port;
            $dataSource->dbname=$dataForm->dbName;
            if($dataSource->save()){
                return Response::json([
                    'errorCode' => 1,
                    'message'=>'Save successfully',
                    'result'=> $dataSource
                ]);
            }
            return Response::json([
                'errorCode' => 0,
                'message'=>"check your connection",
                'result'=>""
            ]);
        }catch (Exception $e){
            return Response::json([
                'errorCode' => 0,
                'message'=>$e->getMessage(),
                'result'=>""
            ]);
        }
    }



    /*
    * ExcelDs Uploading Function
    */

    public function setExcelDsParams(){
        try{
            $folder=env('FOLDER_PATH');
            $timestamp=strtotime(Carbon::now());
            $file=pathinfo(Input::file('upload')->getClientOriginalName(), PATHINFO_FILENAME);
            $file=str_replace(" ", "_", $file);
            $file=str_replace("-", "_", $file);
            $pathCheck="/var/www/html/$folder/api/storage/app/ExcelFilesNew/$file.tsv";
            if(File::exists($pathCheck)) {
                return Response::json([
                    'error' => 0,
                    'message'=>'File name already exist',
                    'result'=> ""
                ]);
            }
            //Input::file("upload")->getClientOriginalName();
            $url =  Storage::disk('local')->putFileAs("ExcelFilesNew",  Input::file("upload"),"$file.tsv");
            $host=env('EXCEL_HOST');
            $port=env('EXCEL_PORT');
            $username=env('EXCEL_USERNAME');
            $password=env('EXCEL_PASSWORD');
            $dbname=env('EXCEL_DB');
            $fileName="/var/www/html/$folder/api/storage/app/$url";
            //$tableName="dynamic_test";
            try
            {
                $inputFileType = \PHPExcel_IOFactory::identify($fileName);
                $objReader = \PHPExcel_IOFactory::createReader($inputFileType);
                $objPHPExcel = $objReader->load($fileName);
            }
            catch(Exception $e)
            {
                File::delete($pathCheck);
                File::delete($fileName);
                die('Error loading file "'.pathinfo($fileName,PATHINFO_BASENAME).'": '.$e->getMessage());
            }
            $sheets=$objPHPExcel->getSheetNames();
            $excelTable=array();
            for($i=0;$i<count($sheets);$i++) {
                // Export to tsv file.
                $sheet=str_replace(" ", "_", $sheets[$i]);
                $sheet=str_replace("-", "_", $sheet);
                $fileNameExcel="/var/www/html/$folder/api/storage/app/ExcelFilesNew/$sheet"."_$file".".tsv";
                $tableName=strtolower($sheet."_".$file);
                array_push($excelTable,$tableName);
                $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'CSV');
                $objWriter->setSheetIndex($i);   // Select which sheet.
                $objWriter->setDelimiter(chr(9));  // Define delimiter
                $objWriter->save($fileNameExcel);
                //Upload to sql
               // echo"sh /var/www/html/generic_excel_load_0.1/generic_excel_load/generic_excel_load_run_context.sh $host $port $username $password $dbname $fileNameExcel $tableName";
                $errorLogFile="/var/www/html/$folder/error.json";
                if (file_exists($errorLogFile)){
                    unlink($errorLogFile);
                }
                $process = new Process("sh /var/www/html/generic_excel_load_0.1/generic_excel_load/generic_excel_load_run_context.sh $host $port $username $password $dbname $fileNameExcel $tableName $errorLogFile");
                $process->run();
                //Check mysql error
                if (file_exists($errorLogFile) || !$process->isSuccessful()) {
                    if(file_exists($errorLogFile)){
                        $json = json_decode(file_get_contents($errorLogFile), true);
                        if ($json['data'][0]['code']) {
                            $sh_message = $json['data'][0]['message'];
                            $replaceStr = ['com.mysql.jdbc.exceptions.jdbc4.MySQLSyntaxErrorException:', 'mysql', 'SQL', 'MySQL'];
                            foreach ($replaceStr as $str) {
                                $sh_message = str_replace($str, "", $sh_message);
                            }
                            File::delete($pathCheck);
                            File::delete($fileName);
                            File::delete($fileNameExcel);
                            return Response::json([
                                'errorCode' => 0,
                                'message' => $sh_message,
                                'result' => ""
                            ]);
                        }
                    }else{
                        File::delete($pathCheck);
                        File::delete($fileName);
                        File::delete($fileNameExcel);
                        return Response::json([
                            'errorCode' => 1,
                            'message' => "Check again insert Row of excel file should be Header,There should be no column empty Header",
                            'result' => ""
                        ]);
                    }
                }
                // executes after the command finishes
                if (!$process->isSuccessful()) {
                    //dd((new ProcessFailedException($process))->getMessage());
                    $message=(new ProcessFailedException($process))->getMessage();
                    return Response::json([
                        'errorCode' => 0,
                        'message'=> $message,
                        'result'=> $excelTable
                    ]);
                }
                $process->getOutput();
            }

            return Response::json([
                'error' => 1,
                'message'=>'Excel uploaded successfully',
                'result'=> $excelTable
            ]);
        }catch (Exception $e){
            return Response::json([
                'error' => 0,
                'message'=>'Excel uploading failed',
                'result'=> $e->getMessage()
            ]);
        }
    }
    /*
     * Append excel data
     */
    public function appendExcelData(){
        try{

            /*$folder=env('FOLDER_PATH');
            $timestamp=strtotime(Carbon::now());
            $file=pathinfo(Input::file('upload')->getClientOriginalName(), PATHINFO_FILENAME);
            $file=str_replace(" ", "_", $file);
            $file=str_replace("-", "_", $file);
            $pathCheck="/var/www/html/$folder/api/storage/app/ExcelFilesNew/$file.tsv";
            if(File::exists($pathCheck)) {
                return Response::json([
                    'error' => 0,
                    'message'=>'File name already exist',
                    'result'=> ""
                ]);
            }
            //Input::file("upload")->getClientOriginalName();
            $url =  Storage::disk('local')->putFileAs("ExcelFilesNew",  Input::file("upload"),"$file.tsv");
            $host=env('EXCEL_HOST');
            $port=env('EXCEL_PORT');
            $username=env('EXCEL_USERNAME');
            $password=env('EXCEL_PASSWORD');
            $dbname=env('EXCEL_DB');
            $fileName="/var/www/html/$folder/api/storage/app/$url";
            //$tableName="dynamic_test";
            try
            {
                $inputFileType = \PHPExcel_IOFactory::identify($fileName);
                $objReader = \PHPExcel_IOFactory::createReader($inputFileType);
                $objPHPExcel = $objReader->load($fileName);
            }
            catch(Exception $e)
            {
                File::delete($pathCheck);
                File::delete($fileName);
                die('Error loading file "'.pathinfo($fileName,PATHINFO_BASENAME).'": '.$e->getMessage());
            }
            $sheets=$objPHPExcel->getSheetNames();
            $excelTable=array();
            // Export to tsv file.
            $sheet=str_replace(" ", "_", $sheets[0]);
            $sheet=str_replace("-", "_", $sheet);
            $fileNameExcel="/var/www/html/$folder/api/storage/app/ExcelFilesNew/$sheet"."_$file".".tsv";
            $tableName=strtolower($sheet."_".$file);
            array_push($excelTable,$tableName);
            $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'CSV');
            $objWriter->setSheetIndex(0);   // Select which sheet.
            $objWriter->setDelimiter(chr(9));  // Define delimiter
            $objWriter->save($fileNameExcel);
            //Upload to sql
            // echo"sh /var/www/html/generic_excel_load_0.1/generic_excel_load/generic_excel_load_run_context.sh $host $port $username $password $dbname $fileNameExcel $tableName";
            $errorLogFile="/var/www/html/$folder/error.json";
            if (file_exists($errorLogFile)){
                unlink($errorLogFile);
            }
            $tableName = Input::get('tableName');
            $process = new Process("sh /var/www/html/append_generic_excel_load_0.1/append_generic_excel_load/append_excel_load_run_context.sh $host $port $username $password $dbname $fileNameExcel $tableName $errorLogFile");
            $process->run();
            //Check mysql error
            if (file_exists($errorLogFile)) {
                $json = json_decode(file_get_contents($errorLogFile), true);
                if ($json['data'][0]['code']) {
                    $sh_message = $json['data'][0]['message'];
                    $replaceStr = ['com.mysql.jdbc.exceptions.jdbc4.MySQLSyntaxErrorException:', 'mysql', 'SQL', 'MySQL'];
                    foreach ($replaceStr as $str) {
                        $sh_message = str_replace($str, "", $sh_message);
                    }
                    File::delete($pathCheck);
                    File::delete($fileName);
                    return Response::json([
                        'errorCode' => 0,
                        'message' => $sh_message,
                        'result' => ""
                    ]);
                }
            }
            // executes after the command finishes
            if (!$process->isSuccessful()) {
                //dd((new ProcessFailedException($process))->getMessage());
                File::delete($pathCheck);
                File::delete($fileName);
                $message=(new ProcessFailedException($process))->getMessage();
                return Response::json([
                    'errorCode' => 0,
                    'message'=> $message,
                    'result'=> $excelTable
                ]);
            }
            $process->getOutput();*/
            /*
             * Clear cache for all related metadata
             */
            $datasourceId=Input::get('datasourceId');
            $metadataArr=Metadata::on($this->switchDB())->select("metadata_id","metadataObject")->get();
            foreach ($metadataArr as $metadata){
                $metadataId=$metadata['metadata_id'];
                if(isset(json_decode($metadata['metadataObject'])->connectionObject) && json_decode($metadata['metadataObject'])->connectionObject->datasource_id==$datasourceId){
                    $this->metadataCacheClear($metadataId);
                }
            }
            return Response::json([
                'errorCode' => 1,
                'message'=>'Excel data append successfully',
                'result'=> ""
            ]);
        }catch (Exception $e){
            return Response::json([
                'errorCode' => 0,
                'message'=>'Excel uploading failed',
                'result'=> $e->getMessage()
            ]);
        }
    }
    /*
     *********************************8 Export datasource *****************************************
     */
    public function datasourceExport(){
        //Key
        $datasourceId=Input::get('datasourceId');
        $datasource=Datasource::on($this->switchDB())->where('datasource_id',$datasourceId)->first();
        $datasource->company_id=Auth::user()->company_id;
        $encrypted = encrypt($datasource);
        $fileName=str_replace(' ', '', $datasource->name);
        $folder=env('FOLDER_PATH');
        $fileName=self::clean($fileName);
        $store= file_put_contents("/var/www/html/$folder/bi/export/".$fileName.".think",$encrypted);
        if($store){
            return Response::json([
                'errorCode' => 1,
                'message'=>'upload successfully',
                'result'=> ["url"=>"export/$fileName.think","fileName"=>$datasource->name.".think"]
            ]);
        }else{
            return Response::json([
                'errorCode' => 0,
                'message'=>'Excel uploading failed',
                'result'=> "file export failed"
            ]);
        }
    }
    public static function is_JSON() {
        call_user_func_array('json_decode',func_get_args());
        return (json_last_error()===JSON_ERROR_NONE);
    }
    public function checkValidation(){
        $input = Input::all();
        $file = File::get($input['upload']);
        $datasourceObj=decrypt($file);
        if($datasourceObj->company_id!=Auth::user()->company_id){
            return Response::json([
                'errorCode' => 0,
                'message'=> 'File corrupted',
                'result'=> ""
            ]);
        }else{
            unset($datasourceObj->company_id);
        }
        if (self::is_JSON($datasourceObj)) {
            $datasourceCount=Datasource::on($this->switchDB())->where('datasource_id',$datasourceObj['datasource_id'])->count();
            if($datasourceCount){
                return Response::json([
                    'errorCode' => 1,
                    'message'=> 'If you move '.$datasourceObj['name'].' from '.$datasourceObj['group_type'].' to another folder then it will be deleted from '.$datasourceObj['group_type'],
//                    'message'=> 'Datasource already exist you want to overwrite '.$datasourceObj['name'].' datasource save',
                    'result'=> ["datasourceName"=>$datasourceObj['name']]
                ]);
            }
            return Response::json([
                'errorCode' => 1,
                'message'=> 'Valid File',
                'result'=> ["datasourceName"=>$datasourceObj['name']]
            ]);
        } else {
            return Response::json([
                'errorCode' => 0,
                'message'=> 'File corrupted',
                'result'=> ""
            ]);
        }
    }
    public function importSave(){
        $input = Input::all();
        $file = File::get($input['upload']);
        $datasourceObj=decrypt($file);
        $reportGroup=json_decode(Input::get('reportGroup'));
        if(isset($reportGroup->name) && Controller::folderValidation($reportGroup->name)){
            return Response::json([
                'errorCode' => 0,
                'message'=>'Folder already exist',
                'result'=> "",
            ]);
        }
        if (self::is_JSON($datasourceObj)) {
            $datasourceObj=(object)$datasourceObj;
            $datasource=Datasource::on($this->switchDB())->where('datasource_id',$datasourceObj->datasource_id)->first();
            if($datasource){
                $datasource->login_id=$datasourceObj->login_id;
                $datasource->datasource_id=$datasourceObj->datasource_id;
                $datasource->datasourceType=$datasourceObj->datasourceType;
                $datasource->group_type=Input::get('publicViewType');
                $datasource->name=$datasourceObj->name;
                $datasource->dbname=$datasourceObj->dbname;
                $datasource->host=$datasourceObj->host;
                $datasource->port=$datasourceObj->port;
                $datasource->username=$datasourceObj->username;
                $datasource->password=$datasourceObj->password;
                $datasource->Attributes=$datasourceObj->Attributes;
                $datasource->save();
            }else{
                $datasource=Datasource::on($this->switchDB())->create([
                    "login_id"=>$datasourceObj->login_id,
                    "datasource_id"=>$datasourceObj->datasource_id,
                    "name"=>$datasourceObj->name,
                    "group_type"=>Input::get('publicViewType'),
                    "host"=>$datasourceObj->host,
                    "username"=>$datasourceObj->username,
                    "password"=>$datasourceObj->password,
                    "port"=>$datasourceObj->port,
                    "datasourceType"=>$datasourceObj->datasourceType,
                    "dbname"=>$datasourceObj->dbname,
                    "Attributes"=>$datasourceObj->Attributes
                ]);
                $datasource->save();
            }
            if(Input::get('publicViewType')=="specific_group"){
                $datasourceReport=DatasourceReportGroup::on($this->switchDB())->where("datasource_id",$datasource->id)->first();
                if($datasourceReport){
                    $datasourceReport->delete();
                }
                if(isset($reportGroup->reportGroup)) {
                    DatasourceReportGroup::on($this->switchDB())->create([
                        "report_group_id"=>$reportGroup->reportGroup,
                        "datasource_id"=> $datasource->id
                    ]);
                }else{
                    if(Controller::folderValidation($reportGroup->name)){
                        $reportGroup=ReportGroup::on($this->switchDB())->create([
                            "name"=>$reportGroup->name,
                            "description"=>$reportGroup->name,
                        ]);
                        /*
                         * Report user group
                         */
                        foreach ($reportGroup->userGroup as $group){
                            ReportUserGroup::on($this->switchDB())->create([
                                "report_group_id"=>$reportGroup->id,
                                "user_group_id"=> $group
                            ]);
                        }
                        DatasourceReportGroup::on($this->switchDB())->create([
                            "report_group_id"=>$reportGroup->id,
                            "datasource_id"=> $datasource->id
                        ]);
                    }else{
                        return Response::json([
                            'errorCode' => 0,
                            'message'=> 'Folder already exist',
                            'result'=> ""
                        ]);
                    }
                }
            }
            if($datasource){
                return Response::json([
                    'errorCode' => 1,
                    'message'=> 'Datasource save successfully',
                    'result'=> ""
                ]);
            }
        } else {
            return Response::json([
                'errorCode' => 0,
                'message'=> 'File corrupted',
                'result'=> ""
            ]);
        }
    }
} 