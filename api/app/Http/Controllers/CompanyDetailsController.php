<?php

namespace App\Http\Controllers;

use App\Http\Utils\DBHelper;
use Faker\Provider\Company;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use App\Model\CompanyDetail;
use App\User;
use Illuminate\Support\Facades\Response;

class CompanyDetailsController extends Controller
{
    //
    public function companyList(){
        $companyObj=CompanyDetail::get();
        return Response::json([
            'errorCode' => 1,
            'message'=>"company list",
            'result'=> $companyObj,
        ]);
    }
    public function delete(){
        $company_id=Input::get('company_id');
        $companyObj=CompanyDetail::find($company_id);
        $dbName=$companyObj->db_name;
        $delete=$companyObj->delete();
        if($delete){
            $users=User::where('company_id',$companyObj->uid)->delete();
            $query = "SELECT SCHEMA_NAME FROM INFORMATION_SCHEMA.SCHEMATA WHERE SCHEMA_NAME =  ?";
            $db = DB::select($query, [$dbName]);
            if($db){
                $query = "Drop database $dbName;";
                DB::statement($query);
            }
            return Response::json([
                'errorCode' => 1,
                'message'=>"company delete succesfully",
                'result'=> "",
            ]);
        }else{
            return Response::json([
                'errorCode' => 0,
                'message'=>"Check you connection",
                'result'=> "",
            ]);
        }
    }
    public function save(){
        $input=(object)Input::get('profile');
        $count=CompanyDetail::count();
        /*
         * Check database name exist or not
         */
        $query = "SELECT SCHEMA_NAME FROM INFORMATION_SCHEMA.SCHEMATA WHERE SCHEMA_NAME =  ?";
        $db = DB::select($query, [$input->dbName]);
        if($db){
            return Response::json([
                'errorCode' => 0,
                'message'=>"Database already exist",
                'result'=> "",
            ]);
        }
        if($count==0){
            $uid=101;
            $port=3002;
        }else{
            $companyObj=CompanyDetail::orderBy('created_at', 'desc')->first();
            $uid = (int) filter_var($companyObj->uid, FILTER_SANITIZE_NUMBER_INT);
            $uid=$uid+1;
            $port=$companyObj->port+1;
        }
        $company=CompanyDetail::create([
            "name"=>$input->name,
            "description"=>$input->description,
            "phone"=>$input->phone,
            "start_date"=>date('Y-m-d',strtotime($input->startdate)),
            "end_date"=>date('Y-m-d',strtotime("+365 day",strtotime($input->startdate))),
            "status_id"=>"ST101",
            "address"=>$input->address,
            "pincode"=>$input->pincode,
            "city"=>$input->city,
            "state"=>$input->state,
            "country"=>$input->country,
            "email_id"=>$input->email,
            "uid"=>"COM".$uid,
            "db_name"=>$input->dbName,
            "plan_id"=>$input->plan_id,
            "port"=>$port
        ]);
        if($company){
            $createdb=DBHelper::createDB($input->dbName,$input->plan_id);
            \SSH::run(array(
                "iptables -I INPUT 7 -p tcp --dport $port -m state --state NEW -j ACCEPT",
                'cd /var/www/html/demo2_thinklytics_io/NodeLayer',
                'node app '.$port,

            ));
            return Response::json([
                'errorCode' => 1,
                'message'=>"company and database create successfully",
                'result'=> "",
            ]);
        }else{
            return Response::json([
                'errorCode' => 0,
                'message'=>"",
                'result'=> "",
            ]);
        }
    }
    public function comObj(){
        $company_id=Input::get('company_id');
        $companyObj=CompanyDetail::find($company_id);
        $companyObj->start_date=date('d-m-Y',strtotime($companyObj->start_date));
        return Response::json([
            'errorCode' => 1,
            'message'=>"company Object",
            'result'=> $companyObj,
        ]);
    }
    public function update(){
        $input=(object)Input::get('profile');
        $companyObj=CompanyDetail::find($input->id);
        $companyObj->name=$input->name;
        $companyObj->description=$input->description;
        $companyObj->phone=$input->phone;
        $companyObj->start_date=date('Y-m-d',strtotime($input->startdate));
        $companyObj->end_date=date('Y-m-d',strtotime("+365 day",strtotime($input->startdate)));
        $companyObj->address=$input->address;
        $companyObj->pincode=$input->pincode;
        $companyObj->city=$input->city;
        $companyObj->status_id=$input->status_id;
        $companyObj->country=$input->country;
        $companyObj->email_id=$input->email;
        $companyObj->save();
        return Response::json([
            'errorCode' => 1,
            'message'=>"company Object update successfully",
            'result'=> "",
        ]);
    }
}
