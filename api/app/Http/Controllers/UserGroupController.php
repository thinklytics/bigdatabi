<?php

namespace App\Http\Controllers;

use App\Model\UserGroup;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;
use Mockery\Exception;
use App\Model\UserGroupUser;
use App\User;

class UserGroupController extends Controller
{
    //
    /*
     * User group create
     */
    public function save(){
        try{
            $userGroup=UserGroup::on($this->switchDB())->create(
                [
                    "name"=>Input::get('name')
                ]
            );
            return Response::json([
                'errorCode' => 1,
                'message'=>'User group created successfully',
                'result'=> $userGroup
            ]);
        }catch (Exception $e){
            return Response::json([
                'errorCode' => 0,
                'message'=>$e->getMessage(),
                'result'=> "",
            ]);
        }

    }
    public function update(){
        try{
            $userGroup=UserGroup::on($this->switchDB())->find(Input::get('id'));
            $userGroup->name=Input::get('name');
            $userGroup->save();
            return Response::json([
                'errorCode' => 1,
                'message'=>'User group created successfully',
                'result'=> $userGroup
            ]);
        }catch (Exception $e){
            return Response::json([
                'errorCode' => 0,
                'message'=>$e->getMessage(),
                'result'=> "",
            ]);
        }
    }
    /*
     * User group List
     */
    public function UsergroupList(){
        try{
            $user=$this->switchUser();
            
            if($user->hasRole([ 'Admin']) || $user->hasRole(["Super Admin"])) {
                $list=UserGroup::on($this->switchDB())->with('reportGroup.reportGroup')->get();
            }else{
                $Usergroup=UserGroupUser::on($this->switchDB())->where('user_id',$user->id)->first();
                $groupId=$Usergroup->group_id;
          	$user=User::on($this->switchDB())->find(Auth::user()->user_id);
                //$list=UserGroup::on($this->switchDB())->with(['reportGroup.reportGroup'=>function($query) use ($groupId){
                //    $query->where('user_group_id',$groupId);
               // }])->get();
               $list=UserGroup::on($this->switchDB())->with('reportGroup.reportGroup')->get();
            }  
            return Response::json([
                'errorCode' => 1,
                'message'=>'User group List successfully',
                'result'=> $list
            ]);
        }catch (Exception $e){
            return Response::json([
                'errorCode' => 0,
                'message'=>$e->getMessage(),
                'result'=> "",
            ]);
        }
    } 
    public function UsergroupDelete(){
        try{
            $role=UserGroup::on($this->switchDB())->find(Input::get('id'));
            $role->delete();
            return Response::json([
                'errorCode' => 1,
                'message'=>'User group List successfully',
                'result'=> ""
            ]);
        }catch (Exception $e){
            return Response::json([
                'errorCode' => 0,
                'message'=>$e->getMessage(),
                'result'=> "",
            ]);
        }
    }
}
