<?php

namespace App\Http\Controllers;

use App\Model\UserProfile;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Storage;
use Mockery\Exception;

class UserProfileController extends Controller
{
    //
    public function save(){
        if(UserProfile::on($this->switchDB())->count()){
            $profile=UserProfile::on($this->switchDB())->where('user_id',Auth::user()->id)->first();
            $profile->contact_no=Input::get('contact_no');
            $profile->address=Input::get('address');
            $profile->city=Input::get('city');
            $profile->country=Input::get('country');
            $profile->save();
        }else{
            $profile=UserProfile::on($this->switchDB())->create([
                "contact_no"=>Input::get('contact_no'),
                "address"=>Input::get('address'),
                "city"=>Input::get('city'),
                "country"=>Input::get('country'),
            ]);
        }
        return Response::json([
            "errorCode"=>1,
            "message"=>"successfully save",
            "result"=>""
        ]);
    }
    public function getProfile(){
        $user=$this->switchUser();
        $user=User::on($this->switchDB())->find($user->id);
        $profile=UserProfile::where('user_id',$user->id)->first();
        $profile->email=$user->email;
        $profile->name=$user->name;
        return Response::json([
            "errorCode"=>1,
            "message"=>"successfully save",
            "result"=>$profile
        ]);
    }
    public function changePassword(){
        try{
            if(Auth::user()->role=="SA"){
                $user=User::find(Auth::user()->id);
                $user->password=bcrypt(Input::get('password'));
                $user->save();
            }else{
                $user=User::find(Auth::user()->id);
                $user->password=bcrypt(Input::get('password'));
                $user->save();
                $user=User::on($this->switchDB())->find(Auth::user()->user_id);
                $user->password=bcrypt(Input::get('password'));
                $user->save();
            }
            return Response::json([
                "errorCode"=>1,
                "message"=>"password change sucessfully",
                "result"=>""
            ]);
        }catch (Exception $e){
            return Response::json([
                "errorCode"=>0,
                "message"=>"Check your connection",
                "result"=>""
            ]);
        }
    }
    public function logSave(){
       $date = date('d-m-Y', time());
       $dateTime = date('d-m-Y h:i:s a', time());
       $pageName=Input::get('pageName');
       if($pageName){$pageName="common";}
       //$p= Storage::disk('local')->put("logs/$date/$pageName/$dateTime.txt", Input::get('message'));
       return Response::json([
           "errorCode"=>1,
           "message"=>"",
           "data"=>""
       ]);
    }
}
