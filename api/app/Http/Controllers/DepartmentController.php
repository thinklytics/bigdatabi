<?php

namespace App\Http\Controllers;
use App\Http\Core\MySQL\MySQL;
use App\Model\Department;
use App\Model\Metadata;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;
use Illuminate\Validation\Rules\In;
use Illuminate\Support\Facades\Redis;
class DepartmentController extends Controller
{
    //
    public function save(){
        $department=Department::on($this->switchDB())->create(array(
            "name" => Input::get('name')
        ));
        return Response::json([
            'error' => 0,
            'message'=>'Get successfully',
            'result'=> $department->id,
            'status'=>1
        ]);
    }
    public function getList(){
        $departmentList=Department::on($this->switchDB())->get();
        return Response::json([
            'error' => 0,
            'message'=>'Get department list',
            'result'=> $departmentList
        ]);
    }
    public function delete(){
        $departmentList=Department::on($this->switchDB())->find(Input::get('id'));
        $departmentList->delete();
        return Response::json([
            'error' => 0,
            'message'=>'Department delete successfully',
            'result'=> "",
            'status'=>1
        ]);
    }
    //public static function redisAddData($client,$metadata){
    /*
     * For data add by browser
     */
    public static function redisAddData($client,$metadata,$incrementObj){
        $redis = Redis::connection();
        /*
         * Get All Data
         */
        //$allKeys=$redis->keys("*");
        $metadataObj=[];
        /*
         * Get all metadata
         */
        $metadataObj[$client]=$metadata;
        /*
         * Check length and get data
         */
        foreach ($metadataObj as $key=>$metadataVal){
            $lengthObj=$redis->hgetAll("metadataDetails");
            $metadataDetailsObj=json_decode($lengthObj[$key]);
            $length=json_decode($lengthObj[$key])->length;
            $dataLength=MySQL::checkLength($metadataVal,$metadataDetailsObj,$incrementObj);
            if($incrementObj && $dataLength){
                $data=MySQL::getLimitData($metadataVal,$metadataDetailsObj,$dataLength,$incrementObj);
                $updatedKey=[];
                $incrementObj=json_decode($incrementObj);
                $tempArr=[];
                $lastData="";
                foreach ($data as $dt){
                    $selectedKeys=[];
                    foreach ($incrementObj->selected_Key as $keyObj){
                        $keyObj=json_decode($keyObj);
                        array_push($selectedKeys,$keyObj->Field."(".$keyObj->tableName.")");
                    }
                    $setKey="";
                    foreach ($selectedKeys as $selectedKey){
                        $setKey .= $dt->$selectedKey;
                    }
                    array_push($updatedKey,$setKey);
                    $lastData=(array)$dt;
                    foreach($incrementObj->table as $table){
                        if(isset($table->key)){
                            if(!isset($tempArr[$table->name])){
                                $tempArr[$table->name]=$lastData[$table->key."(".$table->name.")"];
                            }else if(strtotime($lastData[$table->key."(".$table->name.")"])>strtotime($tempArr[$table->name])){
                                $tempArr[$table->name]=$lastData[$table->key."(".$table->name.")"];
                            }
                        }

                    }
                    $redis->hset($key, $setKey, json_encode($dt));
                }
                $metadataDetailsObj->length=$dataLength;
                $metadataDetailsObj->updated_at=$tempArr;
                $metadataDetailsObj->updated_key=$updatedKey;
                $redis->hset("metadataDetails", $key, json_encode($metadataDetailsObj));
            }else if($dataLength>$length){
                $data=MySQL::getLimitData($metadataVal,$metadataDetailsObj,$dataLength,"");
                $i=0;
                foreach ($data as $dt){
                    $redis->hset($key, $length+$i, json_encode($dt));
                    $i++;
                }
                $metadataDetailsObj->length=$dataLength;
                //$metadataDetailsObj->updated_at=date('Y-m-d H:i:s');
                $redis->hset("metadataDetails", $key, json_encode($metadataDetailsObj));
            }
        }
    }
    /*
     * For data add for node request
     */
    public static function redisAddDataNode(){
        $client=Input::get('client');
        $metadataId=Input::get('metadataId');
        $redis = Redis::connection();
        /*
         * Get All Data
         */
        //$allKeys=$redis->keys("*");
        $metadataObj=[];
        /*
         * Get all metadata
         */
        //foreach ($allKeys as $value){
          //  if($value!="metadataDetails"){
        $metadata=Metadata::on(Controller::switchDB())->find($metadataId);
        $incrementObj=$metadata->incrementObj;
        $metadataObj[$client]=json_decode($metadata->metadataObject);
            //}
        //}
        /*
         * Check length and get data
         */
        /*
         * Check length and get data
         */
        foreach ($metadataObj as $key=>$metadataVal){
            $lengthObj=$redis->hgetAll("metadataDetails");
            $metadataDetailsObj=json_decode($lengthObj[$key]);
            $length=json_decode($lengthObj[$key])->length;
            $dataLength=MySQL::checkLength($metadataVal,$metadataDetailsObj,$incrementObj);
            if($incrementObj && $dataLength){
                $data=MySQL::getLimitData($metadataVal,$metadataDetailsObj,$dataLength,$incrementObj);
                $updatedKey=[];
                $tempArr=[];
                $incrementObj=json_decode($incrementObj);
                foreach ($data as $dt){
                    $selectedKeys=[];
                    foreach ($incrementObj->selected_Key as $keyObj){
                        $keyObj=json_decode($keyObj);
                        array_push($selectedKeys,$keyObj->Field."(".$keyObj->tableName.")");
                    }
                    $setKey="";
                    foreach ($selectedKeys as $selectedKey){
                        $setKey .= $dt->$selectedKey;
                    }
                    array_push($updatedKey,$setKey);
                    $lastData=(array)$dt;
                    foreach($incrementObj->table as $table){
                        if(isset($table->key)){
                            if(!isset($tempArr[$table->name])){
                                $tempArr[$table->name]=$lastData[$table->key."(".$table->name.")"];
                            }else if(strtotime($lastData[$table->key."(".$table->name.")"])>strtotime($tempArr[$table->name])){
                                $tempArr[$table->name]=$lastData[$table->key."(".$table->name.")"];
                            }
                        }
                    }
                    $redis->hset($key, $setKey, json_encode($dt));
                }
                $metadataDetailsObj->length=$dataLength;
                $metadataDetailsObj->updated_at=$tempArr;
                $metadataDetailsObj->updated_key=$updatedKey;
                $redis->hset("metadataDetails", $key, json_encode($metadataDetailsObj));
                $incrementObj=$metadataDetailsObj;
                $responseArr=array(
                    "incrementObjFlag" => true,
                    "flag" => true,
                    "incrementObj" => $incrementObj
                );
                return Response::json($responseArr);
            }else if($dataLength>$length){
                $data=MySQL::getLimitData($metadataVal,$metadataDetailsObj,$dataLength,"");
                $i=0;
                foreach ($data as $dt){
                    $redis->hset($key, $length+$i, json_encode($dt));
                    $i++;
                }
                $metadataDetailsObj->length=$dataLength;
                $redis->hset("metadataDetails", $key, json_encode($metadataDetailsObj));
                $responseArr=array(
                    "incrementObj" => false,
                    "flag" => true
                );
                return Response::json(true);
            }else{
                $metadataDetailsObj->updated_key=[];
                $redis->hset("metadataDetails", $key, json_encode($metadataDetailsObj));
            }
            $responseArr=array(
                "incrementObj" => false,
                "flag" => false
            );
            return Response::json($responseArr);
        }
    }
}
