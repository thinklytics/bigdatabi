<?php
namespace App\Http\Controllers;
use App\Model\Datasource;
use App\User;
use App\Model\ReportGroup;
use Faker\Provider\Company;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
/*use Illuminate\Support\Facades\DB;*/
use Illuminate\Database\Capsule\Manager as DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redis;
use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Illuminate\Support\Facades\Response;
use App\Model\CompanyDetail;
class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    protected function switchDataBase($sourceObject){
        $db = new DB();
             $db->addConnection([
            'driver' => 'mysql',
            'host' => $sourceObject->host,
            'port' => $sourceObject->port,
            'database' => $sourceObject->dbname,
            'username' => $sourceObject->username,
            'password' => $sourceObject->password,
            'charset' => 'utf8mb4',
            'collation' => 'utf8mb4_unicode_ci',
            'prefix' => '',
            'strict' => true,
            'engine' => null,
        ]);

        $db->setAsGlobal();
        $db->bootEloquent();
        return "";
    }
    public function nodeRestart(){
       /* $process = new Process("sh /var/www/html/demo1_thinklytics_io/NodeLayer/nodeStop.sh");
        if(!$process->run()){*/
            //$process=new Process("sh /var/www/html/demo1_thinklytics_io/NodeLayer/nodeStart.sh");
            //dd($process->run());
            //$port pending
            $companyObj=CompanyDetail::where('uid',Auth::user()->company_id)->first();
            \SSH::run([
                'cd /var/www/html/demo2_thinklytics_io/NodeLayer',
                "kill $(fuser -n tcp $companyObj->port 2> /dev/null)",
                "nohup nodejs app $companyObj->port > /dev/null 2>&1 &"
            ],function ($e){

            });
            return Response::json([
                'errorCode' => 1,
                'message' => "Cache server restart successfully",
                'result' => ""
            ]);
    }
    public function redisClear(){
        //$process = new Process("sh /var/www/html/demo2_thinklytics_io/redisClear.sh");
        /*
         * Company id dynamic
         */
        $company_id=Auth::user()->company_id;
        $redis = Redis::connection();
        $allKeys=$redis->keys("*");
        foreach($allKeys as $keys){
            $key=explode(":",$keys);
            if(count($key) && isset($key[2]) && $key[2]==$company_id){
                Redis::del($keys);
            }
        }
        /*
         * Inside metadatadetails data details
         */
        $allMetadataKeys=$redis->hgetAll("metadataDetails");
        foreach ($allMetadataKeys as $keys=>$value){
            $key=explode(":",$keys);
            if(count($key) && isset($key[2]) && $key[2]==$company_id){
                unset($allMetadataKeys[$keys]);
            }
        }
        if(count($allMetadataKeys)){
            Redis::del("metadataDetails");
            foreach ($allMetadataKeys as $keys=>$value) {
                $redis->hset("metadataDetails", $key, json_encode($value));
            }
        }else{
            Redis::del("metadataDetails");
        }
        return Response::json([
            'errorCode' => 1,
            'message' => "Cache clear successfully",
            'result' => ""
        ]);

    }
    public function deleteExportFile(){
        $url=Input::get('url');
        if(unlink("../../bi/".$url))
            echo "File Deleted.";
    }
    public static function switchDB(){
        $user=Auth::user();
        if($user->role!="SA"){
            $companyDetails=CompanyDetail::where('uid',$user->company_id)->first();
            $dbName=$companyDetails->db_name;
        }else{
            $dbName=$user->db_name;
        }
        $connName = $dbName;
        Config::set('database.connections.'.$connName,  array(
            'driver' => 'mysql',
            'host' => env('DB_HOST', '127.0.0.1'),
            'port' => env('DB_PORT', '3306'),
            'database' => $dbName,
            'username' => env('DB_USERNAME', 'forge'),
            'password' => env('DB_PASSWORD', ''),
            'charset' => 'utf8mb4',
            'collation' => 'utf8mb4_unicode_ci',
            'prefix' => '',
            'strict' => true,
            'engine' => null,
        ));
        return $connName;
    }
    public static function switchDBPublicview($dbName){
        Config::set('database.connections.'.$dbName,  array(
            'driver' => 'mysql',
            'host' => env('DB_HOST', '127.0.0.1'),
            'port' => env('DB_PORT', '3306'),
            'database' => $dbName,
            'username' => env('DB_USERNAME', 'forge'),
            'password' => env('DB_PASSWORD', ''),
            'charset' => 'utf8mb4',
            'collation' => 'utf8mb4_unicode_ci',
            'prefix' => '',
            'strict' => true,
            'engine' => null,
        ));
        return $dbName;
    }
    public static function switchUser(){
        return User::on(self::switchDB())->find(Auth::user()->user_id);
    }
    public static function checkNodeStatus(){
        $companyArr=CompanyDetail::where('status_id','ST101')->get();
        foreach($companyArr as $company){
            $port=$company->port;
            $connection = @fsockopen('localhost', $port);
            if (!is_resource($connection))
            {
                \SSH::run([
                    'cd /var/www/html/demo1-rahul/NodeLayer',
                    "nohup nodejs app $port > /dev/null 2>&1 &"
                ],function ($e){

                });
            }
        }
        return Response::json([
            'errorCode' => 1,
            'message' => "server start successfully",
            'result' => ""
        ]);
    }
    public static function is_JSON() {
        call_user_func_array('json_decode',func_get_args());
        return (json_last_error()===JSON_ERROR_NONE);
    }
    public static function clean($string) {
        $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.
        $string = preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
        return preg_replace('/-+/', '-', $string); // Replaces multiple hyphens with single one.
    }
    public static function generateRandomString() {
        $length = 8;
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
   public static function folderValidation($folderName){
        $reportFolderCount=ReportGroup::on(self::switchDB())->where('name',$folderName)->count();
        if($reportFolderCount){
            return true;
        }
        return false;
    }
    /*
     * metadata Cache clear
     */
    public function metadataCacheClear($metadataId){
        $clientId=Auth::user()->id;
        $redis = Redis::connection();
        /*
         * Redis clear metadata
         */
        $allKeys = $redis->keys('*');
        $response=0;
        $company_id=Auth::user()->company_id;
        $companyDetails=CompanyDetail::where('uid',$company_id)->first();
        foreach ($allKeys as $key){
            $redisMetadataId=explode(":",$key);
            if(isset($redisMetadataId[1]) && $redisMetadataId[1]==$metadataId){
                $response=Redis::del($key);
            }
        }
        /*
         * Node clear metadata
         */
        $ip = env("BASE_IP");
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,"$ip:$companyDetails->port/metadataDelete");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS,"metadataId=$metadataId");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $server_output = curl_exec($ch);
        if(json_decode($server_output)->status || $response==1){
            return Response::json([
                "errorCode"=>"1",
                "result"=>"",
                "message"=>"Cache Cleared Successfully"
            ]);
        }else{
            return Response::json([
                "errorCode"=>"0",
                "result"=>"",
                "message"=>"Cache does not exist"
            ]);
        }
    }
    public static function datasourceCheck($datasourceId){
        $dataSource=Datasource::on(self::switchDB())->where('datasource_id',$datasourceId)->first();
        return $dataSource;
    }
    public static function datasourceCheckPublic($datasourceId,$db_name){
        $dataSource=Datasource::on(self::switchDBPublicview($db_name))->where('datasource_id',$datasourceId)->first();
        return $dataSource;
    }
}
?>