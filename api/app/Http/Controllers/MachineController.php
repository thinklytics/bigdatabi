<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Model\Machine;
use App\Model\MachineReportGroup;
use App\Model\ReportGroup;
use App\Model\ReportUserGroup;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Image;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Auth;
use Mockery\Exception;


class MachineController extends Controller
{



// Save Machine Model
    public function save(){
        try{
            $ref_id="ML".self::generateRandomString();
            $p=Machine::on($this->switchDB())->where("name",Input::get('name'))->count();
            if($p==0){
                $machine=Machine::on($this->switchDB())->create(array(
                    "userid" => Auth::user()->id,
                    "machine_id" =>$ref_id,
                    "name" => Input::get('name'),
                    "group_type"=>Input::get('publicViewType'),
                    "description" => Input::get('machineDesc'),
                    "machineObject" => json_encode(Input::get('machineObj')),
                    "dbtype" => Input::get('dbtype'),
                ));
                $machine->save();
                if(Input::get('publicViewType')!='public_group'){
                    if(isset(Input::get('reportGroup')['reportGroup'])){
                        MachineReportGroup::on($this->switchDB())->create([
                            "report_group_id"=>Input::get('reportGroup')['reportGroup'],
                            "machine_id"=> $machine->id,
                        ]);
                    }else{
                        $reportGroup=ReportGroup::on($this->switchDB())->create([
                            "name"=>Input::get('reportGroup')['name'],
                            "description"=>Input::get('reportGroup')['name'],
                        ]);
                        /*
                         * Report user group
                         */
                        foreach (Input::get('reportGroup')['userGroup'] as $group){
                            ReportUserGroup::on($this->switchDB())->create([
                                "report_group_id"=>$reportGroup->id,
                                "user_group_id"=> $group,
                            ]);
                        }
                        MachineReportGroup::on($this->switchDB())->create([
                            "report_group_id"=>$reportGroup->id,
                            "machine_id"=> $machine->id,
                        ]);
                    }
                }

                return Response::json([
                    'errorCode' => 1,
                    'message'=>'Machine Model save successfully',
                    'result'=> $machine->machine_id,
                ]);
            }
            return Response::json([
                'errorCode' => 0,
                'message'=>'Machine Model Name already exists',
                'result'=> "",
            ]);
        }catch (Exception $e){
            return Response::json([
                'errorCode' => 0,
                'message'=>'Check your connection',
                'result'=> "",
            ]);
        }
    }



// Get Machine Model List
    public function machineList(){
        try{
            $temp=array();
            $user=$this->switchUser();
            if($user->hasRole([ 'Admin']) || $user->hasRole(["Super Admin"])) {
                $dList1 = Machine::on($this->switchDB())->select('id','machine_id','name','dbtype')->with("reportGroup.group.group.userGroup")->get();
                foreach($dList1 as $publicview){
                    if(isset($publicview->reportGroup[0]) && isset($publicview->reportGroup[0]->group)){
                        foreach($publicview->reportGroup as $reportGrp){
                            if(isset($reportGrp->group->name) && isset($reportGrp->group->group[0]->userGroup[0])){
                                if (!isset($temp[$reportGrp->group->name])) {
                                    $temp[$reportGrp->group->name]=[];
                                }
                                array_push($temp[$reportGrp->group->name],$publicview);
                            }
                        }
                    }
                }
                //Public View
                $dList2 = Machine::on($this->switchDB())->select('id','machine_id','name','dbtype')->where('group_type','public_group')->with("reportGroup.group.group.userGroup")->get();
                foreach($dList2 as $publicview){
                    if (!isset($temp['public_group'])) {
                        $temp['public_group']=[];
                    }
                    array_push($temp['public_group'],$publicview);
                }
                $data=$temp;
            }else{
                $dList1 = Machine::on($this->switchDB())->select('id','machine_id','name','dbtype')->with(["reportGroup.group.group.userGroup"=> function($query){
                    $query->where("user_id", Auth::user()->id);
                }])->get();
                foreach($dList1 as $publicview){
                    if(isset($publicview->reportGroup[0]) && isset($publicview->reportGroup[0]->group)){
                        foreach($publicview->reportGroup as $reportGrp){
                            if(isset($reportGrp->group->name) && isset($reportGrp->group->group[0]->userGroup[0])){
                                if (!isset($temp[$reportGrp->group->name])) {
                                    $temp[$reportGrp->group->name]=[];
                                }
                                array_push($temp[$reportGrp->group->name],$publicview);
                            }
                        }
                    }
                }
                //Public View
                $dList2 = Machine::on($this->switchDB())->select('id','machine_id','name','dbtype')->where('group_type','public_group')->with("reportGroup.group.group.userGroup")->get();
                foreach($dList2 as $publicview){
                    if (!isset($temp['public_group'])) {
                        $temp['public_group']=[];
                    }
                    array_push($temp['public_group'],$publicview);
                }
                $data=$temp;
            }
            return Response::json([
                'errorCode' => 1,
                'message' => 'Machine Model List',
                'result' => $data
            ]);
        }catch(Exception $e){
            return Response::json([
                'errorCode' => 0,
                'message' => 'Check your connection',
                'result' => ""
            ]);
        }
    }



// Delete Machine Model
    public function machineDelete(){
        $machine = Machine::on($this->switchDB())->where('machine_id',Input::get('id'))->first();
        if($machine->delete()){
            return Response::json([
                'errorCode' => 1,
                'message'=>'Mcahine Model deleted successfully',
                'result'=> ""
            ]);
        }
        return Response::json([
            'error' => 0,
            'message'=>'Check your connection',
            'result'=> ""
        ]);
    }



// Get Machine Model Object
    public function getMachineObject(){
        $machine = Machine::on($this->switchDB())->where('machine_id',Input::get('machineId'))->first();
        if($machine){
            return Response::json([
                'errorCode' => 1,
                'message'=>'Machine Object get successfully',
                'result'=> $machine
            ]);
        }
        return Response::json([
            'errorCode' => 0,
            'message'=>'Check your connection',
            'result'=> ""
        ]);
    }




// Update
    public function update(){
        try{
            $machineData=Machine::on($this->switchDB())->where("name",Input::get('name'))->get();
            if((count($machineData)==1 && $machineData[0]->machine_id==Input::get('machineId')) || (count($machineData)==0)){
                $machine=Machine::on($this->switchDB())->where('machine_id',Input::get('machineId'))->first();
                $machine->name=Input::get('name');
                $machine->description=Input::get('machineDesc');
                $machine->machineObject=json_encode(Input::get('machineObj'));
                $machine->save();
                return Response::json([
                    'errorCode' => 1,
                    'message'=>'Machine Model updated successfully',
                    'result'=> $machine->machine_id,
                ]);
            }
            return Response::json([
                'errorCode' => 0,
                'message'=>'Machine Model name already exist',
                'result'=> ""
            ]);
        }catch (Exception $e){
            return Response::json([
                'errorCode' => 0,
                'message'=>'Check Your Connection',
                'result'=> ""
            ]);
        }

    }



}
