<?php

namespace App\Http\Controllers;

use App\Http\Utils\DBHelper;
use Faker\Provider\Enquiry;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use App\Model\EnquiryDetail;
use App\User;
use Illuminate\Support\Facades\Response;

class EnquiryDetailsController extends Controller{

    public function save(){
        $input=(object)Input::get('enquiry');
        $enquiry=EnquiryDetail::create([
            "name"=>$input->name,
            "email_id"=>$input->email,
            "phone"=>$input->phone,
            "description"=>$input->description,
            "address"=>$input->address,
            "pincode"=>$input->pincode,
            "city"=>$input->city,
            "state"=>$input->state,
            "country"=>$input->country,
            "enquiry_date"=>date('Y-m-d',strtotime($input->enquirydate)),
        ]);
        if($enquiry){
            return Response::json([
                'errorCode' => 1,
                'message'=>"Enquiry Added successfully",
                'result'=> "",
            ]);
        }else{
            return Response::json([
                'errorCode' => 0,
                'message'=>"",
                'result'=> "",
            ]);
        }
    }

    public function enquiryList(){
        $enquiryObj=EnquiryDetail::get();
        return Response::json([
            'errorCode' => 1,
            'message'=>"Enquiry list",
            'result'=> $enquiryObj,
        ]);
    }

    public function delete(){
        $enquiry_id=Input::get('enquiry_id');
        $enquiry=EnquiryDetail::find($enquiry_id);
        if($enquiry->delete()){
            return Response::json([
                'errorCode' => 1,
                'message'=>"Enquiry Deleted Successfully",
                'result'=> "",
            ]);
        }else{
            return Response::json([
                'errorCode' => 0,
                'message'=>"Check you connection",
                'result'=> "",
            ]);
        }
    }

    public function enquiryObj(){
        $enquiry_id=Input::get('enquiry_id');
        $enquiryObj=EnquiryDetail::find($enquiry_id);
        $enquiryObj->enquiry_date=date('d-m-Y',strtotime($enquiryObj->enquiry_date));
        return Response::json([
            'errorCode' => 1,
            'message'=>"Enquiry Object",
            'result'=> $enquiryObj,
        ]);
    }

    public function update(){
        $input=(object)Input::get('enquiry');
        $enquiryObj=EnquiryDetail::find($input->id);
        $enquiryObj->name=$input->name;
        $enquiryObj->email_id=$input->email;
        $enquiryObj->phone=$input->phone;
        $enquiryObj->description=$input->description;
        $enquiryObj->address=$input->address;
        $enquiryObj->pincode=$input->pincode;
        $enquiryObj->city=$input->city;
        $enquiryObj->country=$input->country;
        $enquiryObj->enquiry_date=date('Y-m-d',strtotime($input->enquirydate));
        $enquiryObj->save();
        return Response::json([
            'errorCode' => 1,
            'message'=>"Enquiry Details Updated Successfully",
            'result'=> "",
        ]);
    }

}
