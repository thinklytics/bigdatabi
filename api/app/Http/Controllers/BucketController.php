<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Storage;

class BucketController extends Controller
{
    //
    function listJs(){
      $files = Storage::disk('s3')->files('');
      $allFile = [];
      foreach ($files as $file){
         $allFile[$file] = Storage::disk('s3')->temporaryUrl(
              $file, now()->addMinutes(10)
          );
      }
     return  Response::json($allFile);
    }
}
