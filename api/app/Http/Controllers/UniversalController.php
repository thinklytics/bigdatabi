<?php
namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Input;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class UniversalController extends Controller{
    //Add Machine Details
    public function machineAdd(){
        $file= Input::file('machine_File');
        if(Input::get('uniqueId')){
            $unix_timestamp = now()->timestamp;
            $getData=DB::connection($this->switchDB())->table('universal_mac')->where('id', Input::get('uniqueId'))->first();
            if($file){
                Storage::delete($getData->image);
                $name = "portal/".Input::get('name').$unix_timestamp.'.'.$file->getClientOriginalExtension();
                $md=DB::connection($this->switchDB())->table('universal_mac')->where('id', Input::get('uniqueId'))->update(array('machine_id'=>Input::get('id'), 'name'=>Input::get('name'), 'description'=>Input::get('desc'),  'image'=>$name ));
                $file->move(public_path('../storage/app/portal'), $name);
            }else {
                $md = DB::connection($this->switchDB())->table('universal_mac')->where('id', Input::get('uniqueId'))->update(array('machine_id' => Input::get('id'), 'name' => Input::get('name'), 'description' => Input::get('desc')));
            }
            return Response::json([
                'errorCode' => 1,
                'message'=> 'Machine Details Updated Successfully',
                'result'=> ""
            ]);
        }else{
            $name="";
            if($file){
                $data=Input::get();
                $unix_timestamp = now()->timestamp;
                $name = "portal/".$data['name'].$unix_timestamp.'.'.$file->getClientOriginalExtension();
                $file->move(public_path('../storage/app/portal'), $name);
            }
            $md = DB::connection($this->switchDB())->insert('insert into universal_mac (machine_id,name,description,image) values(?,?,?,?)',[$data['id'],$data['name'],$data['desc'],$name]);

            return Response::json([
                'errorCode' => 1,
                'message'=> 'Machine Details Added Successfully',
                'result'=> ""
            ]);
        }

    }


    public function machineAddedList(){
        $mList = DB::connection($this->switchDB())->table('universal_mac')->get();
        $mStatusList = DB::connection($this->switchDB())->table('universal_machine')->get();
        $tempArray=[];
        foreach ($mStatusList as $list){
            $tempArray[$list->machine_id]=$list->status;
        }
        foreach ($mList as $listM){
            if(isset($tempArray[$listM->machine_id])){
                $listM->status=$tempArray[$listM->machine_id];
            }
        }
        return Response::json([
            'errorCode' => 1,
            'message'=> 'Machine List',
            'result'=> $mList
        ]);
    }


    public function getMachine(){
        $machineData = DB::connection($this->switchDB())->table('universal_mac')->where('id',Input::get('id'))->first();
        return Response::json([
            'errorCode' => 1,
            'message'=> 'Machine List',
            'result'=> $machineData
        ]);
    }

    public function machineEdit(){
        $file= Input::file('machine_File');
        $data = Input::get();
        $unix_timestamp = now()->timestamp;
        $name = "portal/".$data['name'].$unix_timestamp.'.'.$file->getClientOriginalExtension();
        $md=DB::connection($this->switchDB())->table('universal_mac')->where('id', Input::get('id'))->update(array('machine_id'=>Input::get('machineId'), 'name'=>Input::get('name'), 'description'=>Input::get('desc'),  'image'=>$name ));
        $file->move(public_path('../storage/app/portal'), $name);

        return Response::json([
            'errorCode' => 1,
            'message'=> 'Machine Details Updated Successfully',
            'result'=> ""
        ]);
    }



//    public function machineEdit(){
//        $md=DB::connection($this->switchDB())->table('universal_mac')->where('id', Input::get('id'))->update(array('machine_id'=>Input::get('machineId'),'name'=>Input::get('name'),'description'=>Input::get('desc')));
//        if($md == 1){
//            return Response::json([
//                'errorCode' => 1,
//                'message'=> 'Machine Details Updated Successfully',
//                'result'=> ""
//            ]);
//        }else{
//            return Response::json([
//                'errorCode' => 0,
//                'message'=> 'Check Your Connection',
//                'result'=> ""
//            ]);
//        }
//    }


    public function machineDelete(){
        $m = DB::connection($this->switchDB())->table('universal_mac')->where('id', '=', Input::get('id'))->delete();
        if($m == 0){
            return Response::json([
                'errorCode' => 1,
                'message'=> 'Deleted Successfully',
                'result'=> ''
            ]);
        }else{
            return Response::json([
                'errorCode' => 0,
                'message'=> 'Check Your Connection',
                'result'=> ''
            ]);
        }
    }

    /*
     * Api for machine list
    */
    public function machineList(){
        $users = DB::connection($this->switchDB())->table('universal_machine')->get();
        return Response::json([
            'errorCode' => 1,
            'message'=> '',
            'result'=> $users
        ]);
    }
    public function machineSharedviewsave(){
        $machine_id=Input::get('machineId');
        $sharedView=Input::get('sharedViewArr');
        $sharedviewCount=DB::connection($this->switchDB())->table('universal_machine_sharedview')->where('machine_id', '=', $machine_id)->get();
        if(count($sharedviewCount)==0){
            $ms=DB::connection($this->switchDB())->insert('insert into universal_machine_sharedview (machine_id,sharedview) values(?,?)',[$machine_id,$sharedView]);
            return Response::json([
                'errorCode' => 1,
                'message'=> 'Save successfully',
                'result'=> ""
            ]);
        }else{
            DB::connection($this->switchDB())->table('universal_machine_sharedview')->where('machine_id', $machine_id)->update(array('sharedview' => $sharedView));
            return Response::json([
                'errorCode' => 1,
                'message'=> 'Updated successfully',
                'result'=> ""
            ]);
        }
    }
    public function machineSharedviewList(){
        $sharedview=DB::connection($this->switchDB())->table('universal_machine_sharedview')->get();
        return Response::json([
            'errorCode' => 1,
            'message'=> 'List',
            'result'=> $sharedview
        ]);
    }
    public function roleCheck(){
        $user=$this->switchUser();
        if($user->hasRole([ 'Admin']) || $user->hasRole(["Super Admin"])) {
            return Response::json([
                'errorCode' => 1,
                'message'=> 'role',
                'result'=> true
            ]);
        }else{
            return Response::json([
                'errorCode' => 1,
                'message'=> 'role',
                'result'=> false
            ]);
        }
    }
}
?>