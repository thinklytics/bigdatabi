<?php

namespace App\Http\Controllers;

use App\Http\Utils\DBHelper;
use App\Model\Permission;
use App\Model\PermissionRole;
use App\Model\Role;
use App\Model\RoleUser;
use App\Model\UserGroupUser;
use App\Model\UserProfile;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Image;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;
use Mockery\Exception;

class RoleController extends Controller
{
    //
    public function save(){
        try{
            $role=Role::on($this->switchDB())->create([
                "name" => Input::get('name'),
                "display_name" => Input::get('name'),
                "description" => Input::get('description'),
                "landing_page" => Input::get('landingPage')
            ]);
            $role->save();
            foreach (Input::get('pages') as $pageId){
                $permissionRole=PermissionRole::on($this->switchDB())->create(array(
                    "permission_id" => $pageId,
                    "role_id" => $role->id
                ));
            }
            return Response::json([
                'errorCode' => 1,
                'message'=>'Role created successfully',
                'result'=> ""
            ]);
        }catch(Exception $e){
            return Response::json([
                'errorCode' => 500,
                'message'=>$e->getMessage(),
                'result'=> ""
            ]);
        }
    }

    public function listRoles(){
        $roles=Role::on($this->switchDB())->get();
        return Response::json([
            'errorCode' => 1,
            'message'=>'Role created successfully',
            'result'=> $roles
        ]);
    }

    public function roleDelete(){
        $role=Role::on($this->switchDB())->whereId(Input::get('id'));
        $role->delete();
        return Response::json([
            'errorCode' => 1,
            'message'=>'Role delete successfully',
            'result'=> ""
        ]);
    }

    public function permissionList(){
        try{
            $list=Permission::on($this->switchDB())->get();
            return Response::json([
                'errorCode' => 1,
                'message'=>'Permission list',
                'result'=> $list
            ]);
        }catch(Exception $e){
            return Response::json([
                'errorCode' => 500,
                'message'=>$e->getMessage(),
                'result'=> "",
            ]);
        }

    }
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6',
        ]);
    }
    public function assgin(){
        try{
            if($this->validator(Input::get())->fails()){
                return Response::json([
                    "errorCode"=>0,
                    "message" => "Email ID already exist",
                    "data"=>""
                ]);
            }
            $user =User::on($this->switchDB())->create([
                'name' => Input::get('name'),
                'email' => Input::get('email'),
                'password' => bcrypt(Input::get('password')),
            ]);
            $userWithlogin=Auth::user();
            User::create([
                'email' =>  Input::get('email'),
                'password' => bcrypt(Input::get('password')),
                'role' => 'NA',
                'company_id'=>$userWithlogin->company_id,
                'user_id'=>(int)$user->id
            ]);
            if(Input::get('group')){
                foreach(Input::get('group') as $group){
                    $group=UserGroupUser::on($this->switchDB())->create([
                        "group_id"=>$group,
                        'user_id' => $user->id,
                    ]);
                }
            }
            $role=RoleUser::on($this->switchDB())->create([
                'user_id' => $user->id,
                'role_id' => Input::get('role'),
            ]);
            UserProfile::on($this->switchDB())->create([
                'user_id' => $user->id
            ]);
            return Response::json([
                'errorCode' => 1,
                'message'=>'Role created successfully',
                'result'=> ""
            ]);
        }catch(Exception $e){
            return Response::json([
                'errorCode' => 500,
                'message'=>$e->getMessage(),
                'result'=> ""
            ]);
        }
    }
    //User role assign list
    public function userList(){
        try{
            $user=User::on($this->switchDB())->with("roles")->whereHas("roles", function($query){
                $query->where("name","!=", "Super Admin");
            })->with('group.userGroup')->get();
            return Response::json([
                'errorCode' => 1,
                'message'=>"Succes",
                'result'=> $user
            ]);
        }catch(Exception $e){
            return Response::json([
                'errorCode' => 500,
                'message'=>$e->getMessage(),
                'result'=> ""
            ]);
        }

    }
    //logged in user roles
    public function loginUserRole(){
        try {
            if(Auth::user()->role=="SA"){
                return Response::json([
                    'errorCode' => 2,
                    'message'=>"successfully",
                    'result'=> ""
                ]);
            }else{
                $userRole = User::on($this->switchDB())->where('id', Auth::user()->user_id)->with("roles")->with("roles.perms")->get();
                return Response::json([
                    'errorCode' => 1,
                    'message'=>"successfully",
                    'result'=> $userRole
                ]);
            }

        }catch(Exception $e){
            return Response::json([
                'errorCode' => 0,
                'message'=>$e->getMessage(),
                'result'=> ""
            ]);
        }
    }
    public function getRole(){
        try{
            if(Auth::user()->role=="SA"){
                return Response::json([
                    'errorCode' => 2,
                    'message'=>"User role",
                    'result'=> "company.details"
                ]);
            }else{
                //$db=DBHelper::switchDB(Auth::user());
                //on(DBHelper::switchDB(Auth::user()))->
                $userRole = User::on($this->switchDB(Auth::user()))->where('id', Auth::user()->user_id)->with("roles")->first();
                return Response::json([
                    'errorCode' => 1,
                    'message'=>"User role",
                    'result'=> $userRole->roles
                ]);
            }
        }catch(Exception $e){
            return Response::json([
                'errorCode' => 0,
                'message'=>"Check your connection",
                'result'=> ""
            ]);
        }
    }

    public function listRoleId(){
        try{
            $user=Role::on($this->switchDB())->where("id","=",Input::get('id'))->with("perms")->get();
            return Response::json([
                'errorcode' => 1,
                'message' => 'listRoles',
                'result' => $user
            ]);
        }catch(Exception $e) {
            return Response::json([
                'errorcode' => 0,
                'message' => 'Check your connection',
                'result' => $e->getMessage()
            ]);
        }
    }


    public function updateRole(){
        try{
            $RoleId = Role::on($this->switchDB())->find(Input::get('id'));
            $RoleId->name=Input::get('name');
            $RoleId->description=Input::get('description');
            $RoleId->landing_page=Input::get('landingPage');
            $RoleId->save();
            $permissionRole = PermissionRole::on($this->switchDB())->where("role_id",$RoleId->id)->delete();
            foreach (Input::get('pages') as $pageId){
                $permissionRole=PermissionRole::on($this->switchDB())->create(array(
                    "permission_id" => $pageId,
                    "role_id" => $RoleId->id
                ));
            }
            return Response::json([
                'errorCode' => 1,
                'message'=>'Role Updated successfully',
                'result'=> $RoleId->id
            ]);
        }catch (Exception $e){
            return Response::json([
                'errorCode' => 0,
                'message' => 'Data Not Saved',
                'result' => $e->getMessage()
            ]);
        }
    }

    public function getRoleAssign()
    {
        try {
            $user = User::on($this->switchDB())->where('id',Input::get('id'))->with('roles')->with('group')->get();
            return Response::json([
                'errorCode' => 1,
                'message' => 'Role Assign Data Success',
                'result' => $user
            ]);
        } catch (Exception $e) {
            return Response::json([
                'errorCode' => 0,
                'message' => 'Check Your Connection',
                'result' => $e->getMessage()
            ]);
        }
    }
    public function updateRoleAssign(){
        try{
            $user=User::on($this->switchDB())->find(Input::get('id'));
            $user->name=Input::get('name');
            if(Input::get('password'))
                $user->password=bcrypt(Input::get('password'));
            $user->save();
            if(UserGroupUser::on($this->switchDB())->where("user_id", Input::get('id'))->count()){
                $group = UserGroupUser::on($this->switchDB())->where("user_id", Input::get('id'))->delete();
            }
            if(RoleUser::on($this->switchDB())->where("user_id", Input::get('id'))->count()) {
                $role = RoleUser::on($this->switchDB())->where("user_id", Input::get('id'))->delete();
            }
            foreach (Input::get('group') as $group){
                $group=UserGroupUser::on($this->switchDB())->create([
                    "user_id"=>Input::get('id'),
                    "group_id"=> $group
                ]);
            }
            $role=RoleUser::on($this->switchDB())->create([
                'user_id' => Input::get('id'),
                'role_id' => Input::get('role')
            ]);
            return Response::json([
                'errorCode' => 1,
                'message'=>'Role Assign Updated successfully',
                'result'=>''
            ]);
        }catch(Exception $e){
            return Response::json([
                'errorCode' =>0,
                'message'=>'Check Your Connection',
                'result'=>$e->getMessage()
            ]);
        }
    }
    public function roleAssignDelete(){
        try {
            /*
             * Main dataase
             */
            $userM = DB::table('users')->where('user_id',Input::get('id'))->delete();
            $user = User::on($this->switchDB())->find(Input::get('id'));
            if($user->delete()){
                return Response::json([
                    'errorCode' => 1,
                    'message' => 'Delete successfully',
                    'result' => ""
                ]);
            }else{
                return Response::json([
                    'errorCode' => 0,
                    'message' => 'Check your connection',
                    'result' => ""
                ]);
            }
        } catch (Exception $e) { 
            return Response::json([
                'errorCode' => 0,
                'message' => 'Check Your Connection',
                'result' => $e->getMessage()
            ]);
        }
    }
}

