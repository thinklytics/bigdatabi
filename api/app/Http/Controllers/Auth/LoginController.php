<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Model\CompanyDetail;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }


    public function login(Request $request){
        $this->validateLogin($request);

        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);
            return $this->sendLockoutResponse($request);
        }
        if ($this->attemptLogin($request)) {
            $user = Auth::user();
            $user->access_token = $user->createToken("MyToken")->accessToken;
            if($user->company_id=="0"){
                return Response::json([
                    "errorCode"=>1,
                    "message"=>"true",
                    "result"=>$user
                ]);
            }
            $companyCount=CompanyDetail::where('uid',$user->company_id)->where('status_id','ST101')->count();
            if($companyCount){
                $companyObj=CompanyDetail::where('uid',$user->company_id)->where('status_id','ST101')->first();
                $user->port=$companyObj->port;
                return Response::json([
                    "errorCode"=>1,
                    "message"=>"true",
                    "result"=>$user
                ]);
            }else{
                $tempObj=new \stdClass();
                $tempObj->email="Your subscription expired";
                return Response::json([
                    "errorCode"=>0,
                    "message"=>"true",
                    "result"=>$tempObj
                ]);
            }
        }
        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        $this->incrementLoginAttempts($request);
        $errors = [$this->username() => trans('Check username password')];
        return  Response::json([
            "errorCode"=>0,
            "message"=>"true",
            "result"=>$errors
        ]);
    }
}
