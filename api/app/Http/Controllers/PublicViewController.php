<?php
namespace App\Http\Controllers;
use App\Http\Core\ExcelDs\ExcelDs;
use App\Http\Core\MsSQL\MsSQL;
use App\Http\Core\MySQL\MySQL;
use App\Http\Core\Oracle\Oracle;
use App\Model\Datasource;
use App\Model\Metadata;
use App\Model\ReportGroup;
use App\Model\ReportUserGroup;
use App\Model\SharedviewReportGroup;
use App\Model\CompanyDetail;
use App\Model\UserGroup;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;
use App\Model\PublicView;
use App\Model\Dashboard;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Auth;
use Mockery\Exception;
use function print_r;
use App\Http\Core\MongoDB\MongoDB;
use App\Http\Core\PostGres\PostGres;

class PublicViewController extends Controller
{
    //Public dashboard save
    public function save(){
        if(isset(Input::get('reportGroup')['name']) && Controller::folderValidation(Input::get('reportGroup')['name'])){
            return Response::json([
                'errorCode' => 0,
                'message'=>'Folder already exist',
                'result'=> "",
            ]);
        }
        $p=PublicView::on(Controller::switchDB())->where("name",Input::get('name'))->count();
        if($p==0){
            $length=32;
            $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
            $charactersLength = strlen($characters);
            $randomString = '';
            for ($i = 0; $i < $length; $i++) {
                $randomString .= $characters[rand(0, $charactersLength - 1)];
            }
            // str_random(128);

            $authKey=$randomString;
            $publicviewCount=PublicView::on($this->switchDB())->count();
            $ref_id="SV".self::generateRandomString();
            try{
                $type="publicview";
                if(Input::get('publicViewType')!="public_group"){
                    $type="sharedview";
                }
                $sharedview=PublicView::on($this->switchDB())->create(array(
                    "userid" => Auth::user()->id,
                    "sharedview_id"=>$ref_id,
                    "dashboard_id"=>Input::get('dashboardId'),
                    "auth_key"=>$authKey,
                    "name" => Input::get('name'),
                    "description" => Input::get('dashboardDesc'),
                    "reportObject" => Input::get('repotsObject'),
                    "image" => Input::get('image'),
                    "logo" => Input::get('logo'),
                    "filterBy"=> Input::get('filterBy'),
                    "group"=>Input::get('group'),
                    "type"=>$type
                ));
                /*
                 * Report group Create
                 */
                if($type=="sharedview"){
                    if (Input::get('reportGroup')['grpType'] == "both") {
                        foreach (Input::get('reportGroup')['reportGroup'] as $reportGrpObj){
                            $reportGrpObj = json_decode($reportGrpObj);
                            SharedviewReportGroup::on($this->switchDB())->create([
                                "report_group_id"=>$reportGrpObj->ReportGrpid,
                                "sharedview_id"=> $sharedview->id,
                                "user_group_id"=>$reportGrpObj->id,
                            ]);
                        }
                        $reportGroup=ReportGroup::on($this->switchDB())->create([
                            "name"=>Input::get('reportGroup')['name'],
                            "description"=>Input::get('reportGroup')['name'],
                        ]);
                        /*
                         * Report user group
                         */
                        foreach (Input::get('reportGroup')['userGroup'] as $group){
                            ReportUserGroup::on($this->switchDB())->create([
                                "report_group_id"=>$reportGroup->id,
                                "user_group_id"=> $group
                            ]);
                            SharedviewReportGroup::on($this->switchDB())->create([
                                "report_group_id"=>$reportGroup->id,
                                "sharedview_id"=> $sharedview->id,
                                "user_group_id"=>$group,
                            ]);
                        }

                    }else if(Input::get('reportGroup')['grpType'] == "selectGrp"){
                        foreach (Input::get('reportGroup')['reportGroup'] as $reportGrpObj){
                            $reportGrpObj = json_decode($reportGrpObj);
                            SharedviewReportGroup::on($this->switchDB())->create([
                                "report_group_id"=>$reportGrpObj->ReportGrpid,
                                "sharedview_id"=> $sharedview->id,
                                "user_group_id"=>$reportGrpObj->id,
                            ]);
                        }
                    }else{
                        $reportGroup=ReportGroup::on($this->switchDB())->create([
                            "name"=>Input::get('reportGroup')['name'],
                            "description"=>Input::get('reportGroup')['name'],
                        ]);
                        /*
                         * Report user group
                         */
                        foreach (Input::get('reportGroup')['userGroup'] as $group){
                            ReportUserGroup::on($this->switchDB())->create([
                                "report_group_id"=>$reportGroup->id,
                                "user_group_id"=> $group
                            ]);
                            SharedviewReportGroup::on($this->switchDB())->create([
                                "report_group_id"=>$reportGroup->id,
                                "sharedview_id"=> $sharedview->id,
                                "user_group_id"=>$group,
                            ]);
                        }

                    }
                }
                return Response::json([
                    'errorCode' => 1,
                    'message'=>'Save successfully',
                    'result'=> $sharedview->sharedview_id
                ]);
            }catch(Exception $e) {
                return Response::json([
                    'errorCode' => 0,
                    'message' => 'Check your connection',
                    'result' => ""
                ]);
            }
        }else{
            return Response::json([
                'errorCode' => 0,
                'message' => 'Publicview name already exist ',
                'result' => ""
            ]);
        }
    }
    public function update(){
        try{
            $dashboardObj=PublicView::on($this->switchDB())->where("sharedview_id",Input::get('auth_key'))->first();
            $dashboardObj->name=Input::get('name');
            $dashboardObj->description=Input::get('dashboardDesc');
            $dashboardObj->reportObject=Input::get('repotsObject');
            if(Input::get('image'))
                $dashboardObj->image=Input::get('image');
            if(Input::get('logo'))
                $dashboardObj->logo=Input::get('logo');
            $dashboardObj->filterBy=Input::get('filterBy');
            $dashboardObj->group=Input::get('group');
            $dashboardObj->save();
            return Response::json([
                'errorCode' => 1,
                'message'=>'Update successfully',
                'result'=> $dashboardObj->id
            ]);
        }catch(Exception $e){
            return Response::json([
                'errorCode' => 0,
                'message'=>'Check you connection',
                'result'=> ""
            ]);
        }

    }
    public function checkPermission(){
        try{
            $userRole = User::on($this->switchDB())->where('id', Auth::user()->user_id)->with("roles")->first();
            $role=$userRole->roles[0]->name;
            $publicCheck = PublicView::on($this->switchDB())->where("auth_key", Input::get('auth_key'))->where("type","publicview")->count();
            $count=1;
            if($publicCheck==0 && $role!="Super Admin" && $role!="Admin"){
                $publicView = PublicView::on($this->switchDB())->where("auth_key", Input::get('auth_key'))->select(["id", "name","auth_key","type"])
                    ->with(["reportGroup.report.group.userGroup"=>function($query){
                        $query->where("user_id", Auth::user()->id);
                    }])
                    ->first();
                $count=count($publicView->reportGroup);
            }
            if($count || $role=="Super Admin" || $role=="Admin"){
                return Response::json([
                    'errorCode' => 1,
                    'message'=>'successfully',
                    'result'=> ""
                ]);
            }else{
                return Response::json([
                    'errorCode' => 0,
                    'message'=>'Unauthorized',
                    'result'=> ""
                ]);
            }
        }catch (Exception $e){
            return Response::json([
                'errorCode' => 0,
                'message'=>'Check your connection',
                'result'=> ""
            ]);
        }
    }
    public function getPublicViewObject(){
        try{
            $user=$this->switchUser();
            $userRole = User::on($this->switchDB())->where('id', $user->id)->with("roles")->first();
            $role=$userRole->roles[0]->name;
            $publicCheck = PublicView::on($this->switchDB())->where("auth_key", Input::get('auth_key'))->where("type","publicview")->count();
            $count=1;
            if($publicCheck==0 && $role!="Super Admin" && $role!="Admin"){
                    $publicView = PublicView::on($this->switchDB())->where("auth_key", Input::get('auth_key'))->select(["id", "name","auth_key","type"])
                    ->with(["reportGroup.report.group.userGroup"=>function($query) use ($user){
                        $query->where("user_id", $user->id);
                    }])
                    ->first();
                $count=count($publicView->reportGroup);
            }
            if($count || $role=="Super Admin" || $role=="Admin"){
                $pvObj=PublicView::on($this->switchDB())->where("auth_key",Input::get('auth_key'))->first();

                return Response::json([
                    'errorCode' => 1,
                    'message'=>'successfully',
                    'result'=> $pvObj
                ]);
            }else{
                return Response::json([
                    'errorCode' => 401,
                    'message'=>'Unauthorized login again',
                    'result'=> ""
                ]);
            }
        }catch (Exception $e){
            return Response::json([
                'errorCode' => 0,
                'message'=>'Check your connection',
                'result'=> ""
            ]);
        }
    }
    public function getPublicViewObjectEdit(){
        try{
            $user=$this->switchUser();
            $userRole = User::on($this->switchDB())->where('id', $user->id)->with("roles")->first();
            $role=$userRole->roles[0]->name;
            $publicCheck = PublicView::on($this->switchDB())->where("sharedview_id", Input::get('auth_key'))->where("type","publicview")->count();
            $count=1;
            if($publicCheck==0 && $role!="Super Admin" && $role!="Admin"){
                $publicView = PublicView::on($this->switchDB())->where("sharedview_id", Input::get('auth_key'))->select(["id", "name","auth_key","type"])
                    ->with(["reportGroup.report.group.userGroup"=>function($query) use ($user){
                        $query->where("user_id", $user->id);
                    }])
                    ->first();
                $count=count($publicView->reportGroup);

            }
            if($count || $role=="Super Admin" || $role=="Admin"){
                $pvObj=PublicView::on($this->switchDB())->where("sharedview_id",Input::get('auth_key'))->first();
                if($pvObj->group=='{}'){
                    $dashboard=Dashboard::on($this->switchDB())->where("dashboard_id",$pvObj->dashboard_id)->first();
                    $pvObj->group=$dashboard->group;
                }
                return Response::json([
                    'errorCode' => 1,
                    'message'=>'successfully',
                    'result'=> $pvObj
                ]);
            }else{
                return Response::json([
                    'errorCode' => 401,
                    'message'=>'Unauthorized login again',
                    'result'=> ""
                ]);
            }
        }catch (Exception $e){
            return Response::json([
                'errorCode' => 0,
                'message'=>'Check your connection',
                'result'=> ""
            ]);
        }
    }

    public function getSharedviewList(){
        $user=$this->switchUser();
        if($user->hasRole([ 'Admin']) || $user->hasRole(["Super Admin"])) {
            $data = PublicView::on($this->switchDB())->select(["id","sharedview_id", "name","auth_key","type","image"])->get();
        }else{
            $publicview1 = PublicView::on($this->switchDB())->select(["id","sharedview_id", "name","auth_key","type","image"])->with("reportGroup.report.group.userGroup")
                ->orWhereHas("reportGroup.report.group.userGroup", function($query) use ($user){
                    $query->where("user_id", $user->id);
                })->get();
            //Public View
            $publicview2 = PublicView::on($this->switchDB())->where('type','publicview')->select(["id","sharedview_id", "name","auth_key","type","image"])->with("reportGroup.report.group.userGroup")->get();
            $data=array_merge($publicview1->toArray(),$publicview2->toArray());
        }

        try{
            $pvObj=PublicView::on($this->switchDB())->orderBy('id','desc')->get();
            return Response::json([
                'errorCode' => 1,
                'message'=>'Public View Object',
                'result'=> $data
            ]);
        }catch(Exception $e){
            return Response::json([
                'errorCode' => 0,
                'message'=>'Check your connection',
                'result'=> ""
            ]);
        }
    }
    public function getSharedviewListObj(){
        try{
            $data = PublicView::on($this->switchDB())->select(["id","sharedview_id", "name","auth_key","type","image","reportObject"])->get();
            return Response::json([
                'errorCode' => 1,
                'message'=>'Public View Object',
                'result'=> $data
            ]);
        }catch(Exception $e){
            return Response::json([
                'errorCode' => 0,
                'message'=>'Check your connection',
                'result'=> ""
            ]);
        }
    }
    public function getSharedviewFolderList(){
        $temp=array();
        $user=$this->switchUser();
        if($user->hasRole([ 'Admin']) || $user->hasRole(["Super Admin"])) {
            $dList1=UserGroup::on($this->switchDB())->with('reportGroup.reportGroup.sharedGroup.sharedViewDetails')->get();
            foreach($dList1 as $publicview){
                foreach ($publicview->reportGroup as $reportGrp){
                    foreach ($reportGrp->reportGroup->sharedGroup as $sharedviewReport){
                        if(isset($sharedviewReport->sharedViewDetails[0]) && $sharedviewReport->user_group_id==$publicview->id){
                            if(!isset($publicview->name)){
                                $temp[$publicview->name]=[];
                            }
                            if(!isset($temp[$publicview->name][$reportGrp->reportGroup->name])){
                                $temp[$publicview->name][$reportGrp->reportGroup->name]=[];
                            }
                            array_push($temp[$publicview->name][$reportGrp->reportGroup->name],$sharedviewReport->sharedViewDetails[0]);
                        }
                    }
                }
            }
            //Public View
            //->select(["id", "name","auth_key","type","image"])
            $publicview2 = PublicView::on($this->switchDB())->select("id","sharedview_id","auth_key","userid","type","name","image","logo")->where('type','publicview')->with("reportGroup.report.group.userGroup")->get();
            foreach($publicview2 as $publicview){
                if (!isset($temp['Public View'])) {
                    $temp['Public View']=[];
                }
                array_push($temp['Public View'],$publicview);
            }
            $data=$temp;
        }else{
            $publicview1 = PublicView::on($this->switchDB())->select("id","sharedview_id","auth_key","userid","type","name","image","logo")->with(["reportGroup.report.group.userGroup"=> function($query) use ($user){
                $query->where("user_id", $user->id);
            }])->get(); 
            foreach($publicview1 as $publicview){
                if(isset($publicview->reportGroup[0]) && isset($publicview->reportGroup[0]->report)){
                    foreach($publicview->reportGroup as $reportGrp){
                        if(isset($reportGrp->report->group))
                            foreach($reportGrp->report->group as $grpArr){
                                if(isset($grpArr->userGroup[0]->userGroup)){
                                    if(isset($reportGrp->report->name) && $grpArr->userGroup[0]->userGroup->id==$reportGrp->user_group_id){
                                        if(!isset($temp[$grpArr->userGroup[0]->userGroup->name])){
                                            $temp[$grpArr->userGroup[0]->userGroup->name]=[];
                                        }
                                        if (!isset($temp[$grpArr->userGroup[0]->userGroup->name][$reportGrp->report->name])) {
                                            $temp[$grpArr->userGroup[0]->userGroup->name][$reportGrp->report->name]=[];
                                        }
                                        array_push($temp[$grpArr->userGroup[0]->userGroup->name][$reportGrp->report->name],$publicview);
                                    }
                                }
                            }
                    }
                }
            }
            //Public View
            $publicview2 = PublicView::on($this->switchDB())->where('type','publicview')->select("id","sharedview_id","auth_key","userid","type","name","image","logo")->with("reportGroup.report.group.userGroup")->get();
            foreach($publicview2 as $publicview){
                if (!isset($temp['Public View'])) {
                    $temp['Public View']=[];
                }
                array_push($temp['Public View'],$publicview);
            }
            $data=$temp;
        }
        try{
            $pvObj=PublicView::on($this->switchDB())->orderBy('id','desc')->get();
            return Response::json([
                'errorCode' => 1,
                'message'=>'Public View Object',
                'result'=> $data
            ]);
        }catch(Exception $e){
            return Response::json([
                'errorCode' => 0,
                'message'=>'Check your connection',
                'result'=> ""
            ]);
        }
    }
    public function delete(){
        $dashboard=PublicView::on($this->switchDB())->where('sharedview_id',Input::get('id'))->first();
        if($dashboard->delete()){
            return Response::json([
                'errorCode' => 1,
                'message'=>'Public view dashboard delete successfully',
                'result'=> ""
            ]);
        }
        return Response::json([
            'errorCode' => 0,
            'message'=>'Check your connection',
            'result'=> ""
        ]);
    }
    public function publicViewImageSave(){
        try{
            $data=Input::get('image');
            list($type, $data) = explode(';', $data);
            list(, $data)      = explode(',', $data);
            $image = base64_decode($data);
            $image_name=Input::get('image_name').".png";
            Storage::disk('local')->put("dashboardImage/".$image_name, $image);
            $data_logo=Input::get('logo');
            if($data_logo){
                list($type, $data_logo) = explode(';', $data_logo);
                list(, $data_logo)      = explode(',', $data_logo);
                $image_logo = base64_decode($data_logo);
                $image_name=Input::get('image_name').".png";
                Storage::disk('local')->put("dashboardImage/logo_".$image_name, $image_logo);
            }
            return Response::json([
                'errorCode' => 1,
                'message'=>'Image save successfully',
                'result'=> $image_name
            ]);
        }catch (Exception $e){
            return Response::json([
                'errorCode' => 0,
                'message'=>'Check your connection',
                'result'=> $e->getMessage()
            ]);
        }

    }

    public function getPublicData(){
        try{
            $companyDetails=CompanyDetail::where('uid',Input::get('comId'))->first();
            if(PublicView::on($this->switchDBPublicview($companyDetails->db_name))->where("auth_key",Input::get('auth_key'))->where("type","publicview")->count()){
                $pvObj=PublicView::on($this->switchDBPublicview($companyDetails->db_name))->where("auth_key",Input::get('auth_key'))->where("type","publicview")->first();
                return Response::json([
                    'errorCode' => 1,
                    'message'=>'successfully',
                    'result'=> $pvObj
                ]);
            }else{
                return Response::json([
                    'errorCode' => 401,
                    'message'=>'Unauthenticated',
                    'result'=> ""
                ]);
            }
        }catch (Exception $e){
            return Response::json([
                'errorCode' => 0,
                'message'=>'Check your connection',
                'result'=> ""
            ]);
        }
    }
    public function getTableColumn(){
        try{
            $id=Input::get('id');
            $pvObj=PublicView::on($this->switchDB())->where("id",$id)->first();
            $metadataObj=json_decode($pvObj->reportObject)->metadataObject->connObject;
            if(!isset($metadataObj->type)){
                if(isset($metadataObj->connectionObject->datasourceType)){
                    $datasourceType=$metadataObj->connectionObject->datasourceType;
                }else{
                    $datasourceType="mysql";
                    $metadataObj->connectionObject->datasourceType="mysql";
                }
                $metadataObj->connectionObject=Controller::datasourceCheck($metadataObj->connectionObject->datasource_id);
                switch ($datasourceType) {
                    case "mysql":
                        $metadata = (new MySQL())->connect($metadataObj->connectionObject);
                        break;
                    case "mongodb":
                        $metadata = (new MongoDB())->connect($metadataObj->connectionObject);
                        break;
                    case "oracle":
                        $metadata = (new Oracle())->connect($metadataObj->connectionObject);
                        break;
                    case "mssql":
                        $metadata = (new MsSQL())->connect($metadataObj->connectionObject);
                        break;
                    case "excel":
                        $metadata = (new ExcelDs())->connect($metadataObj->connectionObject);
                        break;
                    case "postgres":
                	$metadata = (new PostGres())->connect($metadataObj->connectionObject);
                	break;
                }
            }else{
                $mongoDb=CompanyDetail::where('uid',Auth::user()->company_id)->first();
                $connection=[];
                $connection['datasourceType']=env('MDB_CONNECTION');
                $connection['dbname']=$mongoDb->db_name;
                $connection['host']=env('MDB_HOST');
                $connection['port']=env('MDB_PORT');
                $connection['username']=env('MDB_USERNAME');
                $connection['password']=env('MDB_PASSWORD');
                $connection=(Object)$connection;
                $metadata = (new MongoDB())->connect($connection);
                $tableColumn=$metadata->blendingDataColumn($metadataObj);
                return Response::json([
                    "errorCode"=>1,
                    "result"=>$tableColumn->result['tableColumn'],
                    "message"=>"column"
                ]);
            }
            return Response::json([
                "errorCode"=>1,
                "result"=>$metadata->getData($metadataObj)->result['tableColumn'],
                "message"=>"column"
            ]);
        }catch (Exception $e){

        }
    }
    public function tempImageSave(){
        $data = Input::all();
        dd($data);
        $imageArray=[];
        $png_url = "product-".time().".png";
        $path = public_path().'sharedview/'.$png_url;
        Image::make(file_get_contents($data->base64_image))->save($path);
        array_push($imageArray,$path);

        /*
         * For pdf
         */
        $p="";
        $p .="<span><img src='http://101.53.130.66/demo1_thinklytics_io/api/storage/app/snapImage/".$publicViewName.".png' style='width:1090px;height:750px'></span>";
        $html="<html>
                            <head> 
                              <style>
                                body{margin: 0px;background-color: #eeeeee;}
                                @page { margin: 15px 15px 15px 15px; }
                                span { page-break-after: always; }
                              
                                span:last-child { page-break-after: never; }
                              </style>
                            </head>
                            <body>
                              <main>
                                $p
                              </main>
                            </body>
                           </html>";
        $response = array(
            'status' => 'success',
        );
        return Response::json($response);
    }
    /*
     * Export dashboard
     */
    public function export(){
        $sharedview_id=Input::get('sharedview_id');
        $sharedviewObj=PublicView::on($this->switchDB())->where('sharedview_id',$sharedview_id)->first();
        $sharedviewObj->company_id=Auth::user()->company_id;
        $dashboardObj=Dashboard::on($this->switchDB())->where('dashboard_id',$sharedviewObj->dashboard_id)->first();
        $tempArr=[];
        $tempArr['sharedview']=$sharedviewObj;
        $tempArr['dashboard']=$dashboardObj;
        $encrypted = encrypt($tempArr);
        $fileName=str_replace(' ', '', $sharedviewObj->name);
        $folder=env('FOLDER_PATH');
        $fileName=self::clean($fileName);
        $store= file_put_contents("/var/www/html/$folder/bi/export/".$fileName.".think",$encrypted);
        if($store){
            return Response::json([
                'errorCode' => 1,
                'message'=>'upload successfully',
                'result'=> ["url"=>"export/$fileName.think","fileName"=>$sharedviewObj->name.".think"]
            ]);
        }else{
            return Response::json([
                'errorCode' => 0,
                'message'=>'Excel uploading failed',
                'result'=> "file export failed"
            ]);
        }
    }
    /*
     * Import dashboard
     */
    public function checkValidation(){
        $input = Input::all();
        $file = File::get($input['upload']);
        $sharedviewObj=decrypt($file);
        $sharedviewJson=json_encode($sharedviewObj);
        $sharedView=$sharedviewObj['sharedview'];
        if($sharedView->company_id!=Auth::user()->company_id){
            return Response::json([
                'errorCode' => 0,
                'message'=> 'File corrupted',
                'result'=> ""
            ]);
        }else{
            unset($sharedView->company_id);
        }
        if (self::is_JSON($sharedviewJson)) {
            /*
             * Datasource check
             */
            $dashboardObj=(object)$sharedviewObj['dashboard'];
            $metadata=json_decode($dashboardObj->reportObject)->metadataObject;
            if(!(isset($metadata->connObject->type) && $metadata->connObject->type=="blending")){
                $datasource=$metadata->connObject->connectionObject;
                $datasourceCount=Datasource::on($this->switchDB())->where('datasource_id',$datasource->datasource_id)->count();
            }else{
                $datasourceCount=0;
            }
            $metadataCount=Metadata::on($this->switchDB())->where('metadata_id',$metadata->metadataId)->count();
            $dashboardCount=Dashboard::on($this->switchDB())->where('dashboard_id',$dashboardObj->dashboard_id)->count();
            $sharedViewCount=PublicView::on($this->switchDB())->where('sharedview_id',$sharedView->sharedview_id)->count();
            if($sharedViewCount){
                return Response::json([
                    'errorCode' => 1,
                    'message'=> 'If you move '.$sharedView['name'].' from '.$sharedView['group_type'].' to another folder then it will be deleted from '.$sharedView['group_type'],
//                    'message'=> 'Shared view already exist you want to overwrite '.$sharedView['name'].' shared view save',
                    'result'=> ["datasource"=>$datasourceCount,"metadata"=>$metadataCount,"dashboard"=>$dashboardCount,"sharedview"=>$sharedViewCount]
                ]);
            }
            return Response::json([
                'errorCode' => 1,
                'message'=> 'Shared view already exist you want to overwrite '.$sharedView['name'].' shared view save',
                'result'=> ["datasource"=>$datasourceCount,"metadata"=>$metadataCount,"dashboard"=>$dashboardCount,"sharedview"=>1]
            ]);
        } else {
            return Response::json([
                'errorCode' => 0,
                'message'=> 'File corrupted',
                'result'=> ""
            ]);
        }
    }
    public function importSave(){
        $input = Input::all(); 
        $file = File::get($input['upload']);
        $reportGroupObj=json_decode(Input::get('reportGroup'));
        if(isset($reportGroupObj->name) && Controller::folderValidation($reportGroupObj->name)){
            return Response::json([
                'errorCode' => 0,
                'message'=>'Folder already exist',
                'result'=> "",
            ]);
        }
        $sharedviewObj=decrypt($file);
        $sharedviewJson=json_encode($sharedviewObj);
        if (self::is_JSON($sharedviewJson)) {
            $sharedView=$sharedviewObj['sharedview'];
            $dashboardObj=(object)$sharedviewObj['dashboard'];
            $metadata=json_decode($dashboardObj->reportObject)->metadataObject;
            /*
             * Datasource save or update
             */
            if(!(isset($metadata->connObject->type) && $metadata->connObject->type=="blending")){
                $datasourceObj=$metadata->connObject->connectionObject;
                if(isset(json_decode($input['overwriteStatus'])->overwrite->datasource)){
                    $datasourceUpdate=Datasource::on($this->switchDB())->where('datasource_id',$datasourceObj->datasource_id)->first();
                    if($datasourceUpdate){
                        $datasourceUpdate->name=$datasourceObj->name;
                        $datasourceUpdate->dbname=$datasourceObj->dbname;
                        $datasourceUpdate->host=$datasourceObj->host;
                        $datasourceUpdate->port=$datasourceObj->port;
                        $datasourceUpdate->username=$datasourceObj->username;
                        $datasourceUpdate->password=$datasourceObj->password;
                        $datasourceUpdate->save();
                    }else{
                        Datasource::on($this->switchDB())->create([
                            "datasource_id"=>$datasourceObj->datasource_id,
                            "login_id"=>$datasourceObj->login_id,
                            "datasourceType"=>$datasourceObj->datasourceType,
                            "group_type"=>"public_group",
                            "name"=>$datasourceObj->name,
                            "dbname"=>$datasourceObj->dbname,
                            "host"=>$datasourceObj->host,
                            "port"=>$datasourceObj->port,
                            "username"=>$datasourceObj->username,
                            "password"=>$datasourceObj->password
                        ]);
                    }
                }
            }
            /*
             * Metadata save or update
             */
            if(isset(json_decode($input['overwriteStatus'])->overwrite->metadata)){
                $metadataUpdate=Metadata::on($this->switchDB())->where('metadata_id',$metadata->metadataId)->first();
                if($metadataUpdate){
                    $metadataUpdate->userid=Auth::user()->user_id;
                    $metadataUpdate->name=$metadata->name;
                    $metadataUpdate->metadataObject=json_encode($metadata->connObject);
                    $metadataUpdate->save();
                }else{
                    Metadata::on($this->switchDB())->create([
                        "metadata_id"=>$metadata->metadataId,
                        "userid"=>Auth::user()->user_id,
                        "name"=>$metadata->name,
                        "group_type"=>"public_group",
                        "metadataObject"=>json_encode($metadata->connObject),
                        "group"=>"",
                        "incrementObj"=>""
                    ]);
                }

            }

            if(isset(json_decode($input['overwriteStatus'])->overwrite->dashboard)){
                $dashboard=Dashboard::on($this->switchDB())->where('dashboard_id',$dashboardObj->dashboard_id)->first();
                if($metadata){
                    $dashboard->userid=Auth::user()->user_id;
                    $dashboard->name=$dashboardObj->name;
                    $dashboard->group=$dashboardObj->group;
                    $dashboard->description=$dashboardObj->description;
                    $dashboard->reportObject=$dashboardObj->reportObject;
                    $dashboard->group=$dashboardObj->group;
                    $dashboard->image=$dashboardObj->image;
                    $dashboard->filterBy=$dashboardObj->filterBy;
                    $dashboard->group_type="public_group";
                    $dashboard->save();
                }else{
                    $dashboard=Dashboard::on($this->switchDB())->create(
                        [
                            "dashboard_id"=>$dashboardObj->dashboard_id,
                            "name" => $dashboardObj->name,
                            "group_type"=>"public_group",
                            "description"=>$dashboardObj->description,
                            "userid" => $dashboardObj->userid,
                            "group_type"=>"public_group",
                            "reportObject" =>$dashboardObj->reportObject,
                            "group" => $dashboardObj->group,
                            "image" =>$dashboardObj->image,
                            "image" =>$dashboardObj->filterBy
                        ]
                    );
                }
            }
            /*
             * public view update or create
             */
            $sharedviewSave=PublicView::on($this->switchDB())->where('sharedview_id',$sharedView->sharedview_id)->first();
            if($sharedviewSave){
                $sharedviewSave->type=Input::get('publicViewType');
                $sharedviewSave->name=$sharedView->name;
                $sharedviewSave->description=$sharedView->description;
                $sharedviewSave->reportObject=$sharedView->reportObject;
                $sharedviewSave->group=$sharedView->group;
                $sharedviewSave->logo=$sharedView->logo;
                $sharedviewSave->image=$sharedView->image;
                $sharedviewSave->filterBy=$sharedView->filterBy;
                $sharedviewSave->save();
            }else{
                $sharedviewSave=PublicView::on($this->switchDB())->create(
                    [
                        "sharedview_id"=>$sharedView->sharedview_id,
                        "type" => Input::get('publicViewType'),
                        "auth_key" => $sharedView->auth_key,
                        "userid"=>Auth::user()->user_id,
                        "dashboard_id" =>$sharedView->dashboard_id,
                        "name" => $sharedView->name,
                        "description" =>$sharedView->description,
                        "reportObject" =>$sharedView->reportObject,
                        "group" => $sharedView->group,
                        "logo" => $sharedView->logo,
                        "image" => $sharedView->image,
                        "filterBy"=>$sharedView->filterBy
                    ]
                );
            }
            if(Input::get('publicViewType')=="sharedview"){
                $publicviewReport=SharedviewReportGroup::on($this->switchDB())->where("sharedview_id",$sharedviewSave->id)->first();
                if($publicviewReport){
                    $publicviewReport->delete();
                }
                if (isset($reportGroupObj->reportGroup)) {
                    SharedviewReportGroup::on($this->switchDB())->create([ 
                        "report_group_id"=>$reportGroupObj->reportGroup,
                        "sharedview_id"=> $sharedviewSave->id
                    ]);
                }else{
                    $reportGroup = ReportGroup::on($this->switchDB())->create([
                        "name" => $reportGroupObj->name,
                        "description" => $reportGroupObj->name,
                    ]);
                    /*
                     * Report user group
                     */
                    foreach ($reportGroupObj->userGroup as $group){
                        ReportUserGroup::on($this->switchDB())->create([
                            "report_group_id"=>$reportGroup->id,
                            "user_group_id"=> $group
                        ]);
                    }
                    SharedviewReportGroup::on($this->switchDB())->create([
                        "report_group_id"=>$reportGroup->id,
                        "sharedview_id"=> $sharedviewSave->id
                    ]);
                }
            }
            if($dashboard){
                return Response::json([
                    'errorCode' => 1,
                    'message'=> 'Public view save successfully',
                    'result'=> "" 
                ]);
            }
        } else {
            return Response::json([
                'errorCode' => 0,
                'message'=> 'File corrupted',
                'result'=> ""
            ]);
        }
    }
}
