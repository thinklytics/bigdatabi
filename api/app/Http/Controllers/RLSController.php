<?php

namespace App\Http\Controllers;
use App\Model\PublicView;
use App\Model\ReportUserGroup;
use App\Model\RoleLevelSecurityUser;
use App\Model\RoleLevelSecurityUserGroup;
use App\Model\UserGroup;
use App\User;
use Illuminate\Support\Facades\Response;
use App\Model\RoleLevelSecurity;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Mockery\Exception;

class RLSController extends Controller
{
    //
    public function roleLevelCheck($rlsObj){
        $roleLevelObj=RoleLevelSecurity::on($this->switchDB())->where('public_view_id',$rlsObj->publicviewId)->where('user_group_id',$rlsObj->userGrp)->get();
        if($rlsObj->usersObj=="ALL"){
            if(count($roleLevelObj)){
                return true;
            }
        }else{
            $flag=false;
            foreach ($roleLevelObj as $roleLevel){
                $roleLevelUsers=RoleLevelSecurityUser::on($this->switchDB())->where('rls_id',$roleLevel->id)->get();
                foreach ($roleLevelUsers as $rlsUser){
                    if(in_array($rlsUser->user_id ,$rlsObj->usersObj)){
                        $flag=true;
                    }
                }
            }
            return $flag;
        }
    }
    public function save(){
        try{
            $rlsObj=(object)Input::get('rlsObj');
            if(!$this->roleLevelCheck($rlsObj)){
                $roleLevelSave=RoleLevelSecurity::on($this->switchDB())->create([
                    "public_view_id"=>$rlsObj->publicviewId,
                    "user_group_id"=>$rlsObj->userGrp,
                    "name"=>"",
                    "conditionObj"=>Input::get('filterObj')
                ]);
                if($rlsObj->usersObj=="ALL"){
                    /*
                     * User role give to department wise
                     */
                    RoleLevelSecurityUserGroup::on($this->switchDB())->create([
                        "rls_id" => $roleLevelSave->id,
                        "user_group_id" => $rlsObj->userGrp
                    ]);
                }else{
                    /*
                     * User wise role assign
                     */
                    foreach (Input::get('rlsObj')['usersObj'] as $userid) {
                        RoleLevelSecurityUser::on($this->switchDB())->create([
                            "rls_id" => $roleLevelSave->id,
                            "user_id" => $userid
                        ]);
                    }
                }
                return Response::json([
                    'errorCode' => 1,
                    'message'=>'Role Level Save Successfully',
                    'result'=> "",
                ]);
            }
            return Response::json([
                'errorCode' => 0,
                'message'=>'Role already exist',
                'result'=> "",
            ]);

        }catch(Exception $e){
            return Response::json([
                'errorCode' => 0,
                'message'=>'Check your connection',
                'result'=> $e->getMessage(),
            ]);
        }
    }
    public function update(){
        try{
            $roleLevel=RoleLevelSecurity::on($this->switchDB())->where('id',Input::get('roleId'))->first();
            $roleLevel->name=Input::get('name');
            $roleLevel->conditionObj=Input::get('filterObj');
            $this->metadataCacheClear(Input::get('metadataId'));
            $roleLevel->save();
            return Response::json([
                'errorCode' => 1,
                'message'=>'Role Level update Successfully',
                'result'=> "",
            ]);
        }catch(Exception $e){
            return Response::json([
                'errorCode' => 0,
                'message'=>'Check your connection',
                'result'=> $e->getMessage(),
            ]);
        }
    }
    public function rlsList(){
        try{
            $userGroupId=Input::get('userGroupId');
            $userList=User::on($this->switchDB())->with(['group'=>function($query) use ($userGroupId) {
                $query->where('group_id',$userGroupId);
            }])->get();
            $sharedviewList=PublicView::on($this->switchDB())->select(["id","sharedview_id", "name","auth_key","type","image","reportObject"])->with(['reportGroup.report.group'=>function($q) use ($userGroupId){
                $q->where('user_group_id',$userGroupId);
            }])->get();
            $sharedviewWithGrp=[];
            foreach ($sharedviewList as $sharedview){
                if(isset($sharedview->reportGroup)){
                    foreach ($sharedview->reportGroup as $sharedviewReportGrp){
                        if(isset($sharedviewReportGrp->report) && isset($sharedviewReportGrp->report->group) && count($sharedviewReportGrp->report->group)){
                            array_push($sharedviewWithGrp,$sharedview);
                        }
                    }
                }
            }
            $userListWithoutRole=[];
            foreach ($userList as $list){
                if(count($list->group)){
                    array_push($userListWithoutRole,$list);
                }
            }
            return Response::json([
                'errorCode' => 1,
                'message'=>'Role Level Security List',
                'result'=> [
                    "userList"=>$userListWithoutRole,
                    "sharedViewList"=>$sharedviewWithGrp
                ],
            ]);
        }catch(Exception $e){
            return Response::json([
                'errorCode' => 0,
                'message'=>'Check your connection',
                'result'=> $e->getMessage(),
            ]);
        }
    }
    public function roleLevelList(){
        $rlsList=RoleLevelSecurity::on($this->switchDB())->with('rls_user.user')->with('rls_user_group.userGroup')->with(['sharedView'=>function($query){
            $query->select('id','name');
        }])->get();
        $rlsListArr=[];
        foreach ($rlsList as $rls){
            $tempArr=[];
            $tempArr['rlsId']=$rls->id;
            $tempArr['condition']=$rls->conditionObj;
            foreach ($rls->rls_user as $rlsUser){
                $tempArr['department_user']="";
                if(isset($rlsUser->user->name)){
                    $tempArr['department_user']=$rlsUser->user->name."(".$rlsUser->user->email.")";
                }

            }
            foreach ($rls->rls_user_group as $rlsUser){
                $tempArr['department_user']=$rlsUser->userGroup->name;
            }

            $tempArr['sharedview_name']=$rls->sharedView->name;
            array_push($rlsListArr,$tempArr);
        }
        return Response::json([
            'errorCode' => 1,
            'message'=>'Role Level Security List',
            'result'=> [
                "rlsList"=>$rlsListArr
            ],
        ]);
    }


    public function rlsListWithGroup(){
        try{
            $userGroup=UserGroup::on($this->switchDB())->get();
            return Response::json([
                'errorCode' => 1,
                'message'=>'Role Level Security List',
                'result'=> [
                    "userGroup"=>$userGroup
                ],
            ]);
        }catch(Exception $e){
            return Response::json([
                'errorCode' => 0,
                'message'=>'Check your connection',
                'result'=> $e->getMessage(),
            ]);
        }
    }
    public function userRLSSave(){
        try {
            foreach (Input::get('rlsObj')['userid'] as $userid) {
                $user =RoleLevelSecurityUser::on($this->switchDB())->create([
                    "rls_id" => Input::get('rlsObj')['sharedviewid'],
                    "user_id" => $userid
                ]);
            }
            return Response::json([
                'errorCode' => 1,
                'message'=>'Save Successfully',
                'result'=> ""
            ]);
        }catch(Exception $e){
            return Response::json([
                'errorCode' => 0,
                'message'=>'Check your connection',
                'result'=> $e->getMessage(),
            ]);
        }
    }
    public function rlsDelete(){
        $rlsList=RoleLevelSecurity::on($this->switchDB())->with('rls_user.user')->with('rls_user_group.userGroup')->find(Input::get('id'));
        if($rlsList->delete()){
            return Response::json([
                'errorCode' => 1,
                'message'=>'Security role delete successfully',
                'result'=> ""
            ]);
        }
        return Response::json([
            'error' => 0,
            'message'=>'Check your connection',
            'result'=> ""
        ]);
    }
    public function rlsGetDataById(){
        try{
            $id=Input::get('id');
            $rlsList=RoleLevelSecurity::on($this->switchDB())->with('rls_user.user')->with('rls_user_group.userGroup')->with('sharedview')->where('id',$id)->get();
            return Response::json([
                'errorCode' => 1,
                'message'=>'Role Level Security List',
                'result'=> [
                    "rlsList"=>$rlsList
                ],
            ]);
        }catch(Exception $e){
            return Response::json([
                'errorCode' => 0,
                'message'=>'Check your connection',
                'result'=> $e->getMessage(),
            ]);
        }
    }
    public function rlsUserGetDataById(){
        try{
            $rls_user=RoleLevelSecurityUser::on($this->switchDB())->where('rls_id',Input::get('id'))->get();
            $rlsUserArray=array();
            foreach ($rls_user as $user){
                if(!isset($rlsUserArray[$user->rls_id])){
                    $rlsUserArray[$user->rls_id]['user']=[];
                    array_push($rlsUserArray[$user->rls_id]['user'],$user->user_id);
                }else{
                    array_push($rlsUserArray[$user->rls_id]['user'],$user->user_id);
                }
            }
            return Response::json([
                'errorCode' => 1,
                'message'=>'Role Level Security List',
                'result'=>$rlsUserArray
            ]);
        }catch(Exception $e){
            return Response::json([
                'errorCode' => 0,
                'message'=>'Check your connection',
                'result'=> $e->getMessage(),
            ]);
        }
    }
    public function rlsUserGroupGetDataById(){
        try{
            $roleLevelUser=RoleLevelSecurityUserGroup::on($this->switchDB())->where('rls_id',Input::get('id'))->with('userGroup')->with('rls')->get();
            $roleLevelUserGroup=array();
            foreach($roleLevelUser as $user){
                if(!isset($roleLevelUserGroup[$user->rls_id])){
                    $roleLevelUserGroup[$user->rls_id]=array();
                    $roleLevelUserGroup[$user->rls_id]['rlsName']=$user->rls->name;
                    $roleLevelUserGroup[$user->rls_id]['userGroup']=[];
                    array_push($roleLevelUserGroup[$user->rls_id]['userGroup'],$user->userGroup->id);
                }else{
                    array_push($roleLevelUserGroup[$user->rls_id]['userGroup'],$user->userGroup->id);
                }
            }
            return Response::json([
                'errorCode' => 1,
                'message'=>'Save Successfully',
                'result'=> $roleLevelUserGroup
            ]);
        }catch(Exception $e){
            return Response::json([
                'errorCode' => 0,
                'message'=>'Check your connection',
                'result'=> $e->getMessage(),
            ]);
        }
    }
    public function reportGrpList(){
        try{
            $groupId=[2,3];
            foreach ($groupId as $id){
                $reportGrps=ReportUserGroup::where('user_group_id',$id)->with('reportGroup')->get();
                dd($reportGrps);
            }
            return Response::json([
                'errorCode' => 1,
                'message'=>'Save Successfully',
                'result'=> $roleLevelUserGroup
            ]);
        }catch(Exception $e){
            return Response::json([
                'errorCode' => 0,
                'message'=>'Check your connection',
                'result'=> $e->getMessage(),
            ]);
        }
    }
}
