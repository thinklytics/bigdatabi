<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class MetadataReportGroup extends Model
{
    //
    protected $fillable=['report_group_id','metadata_id','user_group_id'];
    public $timestamps=false;
    protected $primaryKey = 'metadata_id';
    public function group(){
        return $this->hasOne(ReportGroup::class, "id", "report_group_id");
    }
    public function metadataDetails(){
        return $this->hasMany(Metadata::class, "id", "metadata_id");
    }
}
