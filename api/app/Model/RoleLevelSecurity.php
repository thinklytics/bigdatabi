<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class RoleLevelSecurity extends Model
{
    //
    protected $table="role_level_security";
    protected $guarded=['id'];
    public function rls_user(){
        return $this->hasMany(RoleLevelSecurityUser::class,"rls_id", "id");
    }
    public function rls_user_group(){
        return $this->hasMany(RoleLevelSecurityUserGroup::class,"rls_id", "id");
    }
    public function sharedView(){
        return $this->belongsTo(PublicView::class,"public_view_id", "id");
    }
}
