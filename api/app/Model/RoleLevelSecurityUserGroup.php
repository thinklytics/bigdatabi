<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class RoleLevelSecurityUserGroup extends Model
{
    //
    protected $table="role_level_security_user_group";
    protected $guarded=['id'];
    public function userGroup(){
        return $this->belongsTo(UserGroup::class,"user_group_id", "id");
    }
    public function rls(){
        return $this->belongsTo(RoleLevelSecurity::class,"rls_id", "id");
    }
}
