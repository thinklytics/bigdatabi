<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Jenssegers\Mongodb\Eloquent\HybridRelations;

class TestMongo extends Eloquent
{
    use HybridRelations;
    protected $connection = 'mongodb';
}
