<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Datasource extends Model
{
    //
    protected $guarded=['id'];
    public function metadatas(){
        return $this->hasMany(Metadata::class);
    }
    public function reportGroup(){
        return $this->hasMany(DatasourceReportGroup::class, "datasource_id", "id");
    }
}
