<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use SebastianBergmann\CodeCoverage\Report\Xml\Report;

class ReportUserGroup extends Model
{
    //
    protected $fillable=['user_group_id','report_group_id'];
    public $timestamps = false;


    public function reportGroup(){
        return $this->belongsTo(ReportGroup::class, "report_group_id", "id");
    }

    public function userGroup(){
        return $this->hasMany(UserGroupUser::class, "group_id", "user_group_id");
    }
    public function userGroupDetails(){
        return $this->hasMany(UserGroup::class, "id", "user_group_id");
    }
}