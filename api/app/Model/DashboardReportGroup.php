<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class DashboardReportGroup extends Model
{
    protected $fillable=['report_group_id','dashboard_id','user_group_id'];
    public $timestamps=false;
    protected $primaryKey = 'dashboard_id';
    public function group(){
        return $this->hasOne(ReportGroup::class, "id", "report_group_id");
    }
    public function dashboardDetails(){
        return $this->hasMany(Dashboard::class, "id", "dashboard_id");
    }
}
