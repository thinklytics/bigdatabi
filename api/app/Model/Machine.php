<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Machine extends Model
{
    protected $guarded=['id'];
    public function reportGroup(){
        return $this->hasMany(MachineReportGroup::class, "machine_id", "id");
    }
}
