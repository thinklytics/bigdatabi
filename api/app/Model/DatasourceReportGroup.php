<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class DatasourceReportGroup extends Model
{
    //

    protected $fillable=['report_group_id','datasource_id','user_group_id'];
    public $timestamps=false;
    protected $primaryKey = 'datasource_id';
    public function group(){
        return $this->hasOne(ReportGroup::class, "id", "report_group_id");
    }
    public function datasourceDetails(){
        return $this->hasMany(Datasource::class, "id", "datasource_id");
    }
}
