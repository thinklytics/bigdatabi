<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class EnquiryDetail extends Model
{
    protected $guarded = ['id'];
}
