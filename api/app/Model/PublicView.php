<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class PublicView extends Model
{
   protected $guarded=['id'];
   public function reportGroup(){
       return $this->hasMany(SharedviewReportGroup::class, "sharedview_id", "id");
   }
}
