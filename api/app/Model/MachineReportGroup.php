<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class MachineReportGroup extends Model
{
    protected $fillable=['report_group_id','machine_id'];
    public $timestamps=false;
    protected $primaryKey = 'machine_id';
    public function group(){
        return $this->hasOne(ReportGroup::class, "id", "report_group_id");
    }
    public function machineDetails(){
        return $this->hasMany(Machine::class, "id", "machine_id");
    }
}
