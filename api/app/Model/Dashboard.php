<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Dashboard extends Model
{
   
    protected $guarded=['id'];
    public function reportGroup(){
        return $this->hasMany(DashboardReportGroup::class, "dashboard_id", "id");
    }
}
