<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Metadata extends Model
{
    //
    protected $table = "metadatas";
    protected $guarded=['id'];
    public function datasource(){
        return $this->belongsTo(Datasource::class);
    }
    public function reportGroup(){
        return $this->hasMany(MetadataReportGroup::class, "metadata_id", "id");
    }
}
