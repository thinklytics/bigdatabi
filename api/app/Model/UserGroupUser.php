<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class UserGroupUser extends Model
{
    //
    protected $guarded=['id'];
    public $timestamps = false;


    public function report(){
        return $this->belongsTo(ReportUserGroup::class, "group_id", "user_group_id");
    }

public function userGroup(){
        return $this->belongsTo(UserGroup::class, "group_id", "id");
    }
}
