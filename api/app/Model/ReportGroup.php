<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ReportGroup extends Model
{
    //
    protected $guarded=['id'];

    public function sharedView(){
        return $this->belongsTo(PublicView::class,"sharedview_id", "id");
    }
    public function group(){
        return $this->hasMany(ReportUserGroup::class, "report_group_id", "id");
    }
    public function sharedGroup(){
        return $this->hasMany(SharedviewReportGroup::class, "report_group_id", "id");
    }
    public function datasourceGroup(){
        return $this->hasMany(DatasourceReportGroup::class, "report_group_id", "id");
    }
    public function metadataGroup(){
        return $this->hasMany(MetadataReportGroup::class, "report_group_id", "id");
    }
    public function dashboardGroup(){
        return $this->hasMany(DashboardReportGroup::class, "report_group_id", "id");
    }
}
