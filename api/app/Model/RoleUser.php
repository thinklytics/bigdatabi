<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class RoleUser extends Model
{
    //
    protected $table="role_user";
    protected $guarded=['id'];
    public $timestamps = false;
}
