<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class UserGroup extends Model
{
    //
    protected $guarded=['id'];
    public function reportGroup(){
        return $this->hasMany(ReportUserGroup::class, "user_group_id", "id");
    }
}
