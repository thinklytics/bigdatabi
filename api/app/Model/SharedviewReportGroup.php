<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class SharedviewReportGroup extends Model
{
    //
    protected $fillable=['report_group_id','sharedview_id','user_group_id'];
    public $timestamps=false;
    protected $primaryKey = 'sharedview_id';
    public function sharedViewDetails(){
        return $this->hasMany(PublicView::class, "id", "sharedview_id");
    }
    public function report(){
        return $this->belongsTo(ReportGroup::class, "report_group_id", "id");
    }
}
