<?php

namespace App\Model;

use App\User;
use Illuminate\Database\Eloquent\Model;

class RoleLevelSecurityUser extends Model
{
    //
    protected $table="role_level_security_user";
    protected $guarded=['id'];
    public function user(){
        return $this->hasOne(User::class,"id", "user_id");
    }
    public function rls(){
        return $this->hasOne(RoleLevelSecurity::class,"id", "rls_id");
    }
}
