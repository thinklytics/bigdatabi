<?php

use Illuminate\Database\Seeder;

class Permission extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public static function run()
    {
        //
        $permissions=[
            [
                "order"=>0,
                "name"=>"datasource-add",
                "display_name"=>"Datasource Add",
                "description"=>"Datasource Add"
            ],
            [
                "order"=>0,
                "name"=>"datasource-list",
                "display_name"=>"Datasource list",
                "description"=>"Datasource list"
            ],
            [
                "order"=>0,
                "name"=>"datasource-edit",
                "display_name"=>"Datasource Edit",
                "description"=>"Datasource Edit"
            ],
            [
                "order"=>0,
                "name"=>"datasource-view",
                "display_name"=>"Datasource View",
                "description"=>"Datasource View"
            ],
            [
                "order"=>0,
                "name"=>"datasource-delete",
                "display_name"=>"Datasource Delete",
                "description"=>"Datasource Delete"
            ],
            [
                "order"=>1,
                "name"=>"metadata-add",
                "display_name"=>"Metadata Add",
                "description"=>"Metadata Add"
            ],
            [
                "order"=>1,
                "name"=>"metadata-list",
                "display_name"=>"Metadata list",
                "description"=>"Metadata list"
            ],
            [
                "order"=>1,
                "name"=>"metadata-edit",
                "display_name"=>"Metadata Edit",
                "description"=>"Metadata Edit"
            ],
            [
                "order"=>1,
                "name"=>"metadata-view",
                "display_name"=>"Metadata View",
                "description"=>"Metadata View"
            ],
            [
                "order"=>1,
                "name"=>"metadata-delete",
                "display_name"=>"Metadata Delete",
                "description"=>"Metadata Delete"
            ],
            [
                "order"=>2,
                "name"=>"ml-add",
                "display_name"=>"M/L Model Add",
                "description"=>"M/L Model Add"
            ],[
                "order"=>2,
                "name"=>"ml-list",
                "display_name"=>"M/L Model List",
                "description"=>"M/L Model List"
            ],[
                "order"=>2,
                "name"=>"ml-edit",
                "display_name"=>"M/L Model Edit",
                "description"=>"M/L Model edit"
            ],[
                "order"=>2,
                "name"=>"ml-view",
                "display_name"=>"M/L Model View",
                "description"=>"M/L Model View"
            ],[
                "order"=>2,
                "name"=>"ml-delete",
                "display_name"=>"M/L Model Delete",
                "description"=>"M/L Model delete"
            ],
            [
                "order"=>3,
                "name"=>"dashboard-add",
                "display_name"=>"Dashboard Add",
                "description"=>"Dashboard Add"
            ],
            [
                "order"=>3,
                "name"=>"dashboard-list",
                "display_name"=>"Dashboard list",
                "description"=>"Dashboard list"
            ],
            [
                "order"=>3,
                "name"=>"dashboard-edit",
                "display_name"=>"Dashboard Edit",
                "description"=>"Dashboard Edit"
            ],
            [
                "order"=>3,
                "name"=>"dashboard-view",
                "display_name"=>"Dashboard View",
                "description"=>"Dashboard View"
            ],
            [
                "order"=>3,
                "name"=>"dashboard-delete",
                "display_name"=>"Dashboard Delete",
                "description"=>"Dashboard Delete"
            ],
            [
                "order"=>4,
                "name"=>"sharedview-add",
                "display_name"=>"Sharedview Add",
                "description"=>"Sharedview Add"
            ],[
                "order"=>4,
                "name"=>"sharedview-list",
                "display_name"=>"Sharedview list",
                "description"=>"Sharedview list"
            ],
            [
                "order"=>4,
                "name"=>"sharedview-edit",
                "display_name"=>"Sharedview Edit",
                "description"=>"Sharedview Edit"
            ],
            [
                "order"=>4,
                "name"=>"sharedview-view",
                "display_name"=>"Sharedview View",
                "description"=>"Sharedview View"
            ],
            [
                "order"=>4,
                "name"=>"sharedview-delete",
                "display_name"=>"Sharedview Delete",
                "description"=>"Sharedview Delete"
            ],
            [
                "order"=>5,
                "name"=>"setting",
                "display_name"=>"Setting",
                "description"=>"Setting"
            ]
        ];
        foreach($permissions as $permission){
           $permission= new \App\Model\Permission([
                "name"=>$permission['name'],
                "display_name"=>$permission['display_name'],
                "description"=>$permission['description'],
            ]);
            $permission->save();
        }
    }
}
