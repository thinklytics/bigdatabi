<?php

use Illuminate\Database\Seeder;

class Role extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $roles=[
            [
                "name"=>"Super Admin",
                "display_name"=>"Super Admin",
                "description"=>"Super Admin",
                "landing_page"=>"datasource-list"
            ],
            [
                "name"=>"Admin",
                "display_name"=>"Admin",
                "description"=>"Admin",
                "landing_page"=>"datasource-list"
            ],[
                "name"=>"IT Admin",
                "display_name"=>"IT Admin",
                "description"=>"IT Admin",
                "landing_page"=>"datasource-list"
            ],[
                "name"=>"HR Admin",
                "display_name"=>"HR Admin",
                "description"=>"HR Admin",
                "landing_page"=>"datasource-list"
            ],
        ];
        foreach($roles as $role){
            $roleObj= new \App\Model\Role([
                "name"=>$role['name'],
                "display_name"=>$role['display_name'],
                "description"=>$role['description'],
                "landing_page"=>$role['landing_page']
            ]);
            $roleObj->save();
        }
    }
}
