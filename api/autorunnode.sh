#!/bin/bash

NAME="app.js" # nodejs script's name here
RUN=`pgrep -f $NAME`

if [ "$RUN" == "" ]; then
 echo "Script is not running"
 php http://demo1api.thinklytics.io/public/api/email/send 
else
 echo "Script is running" 
fi

